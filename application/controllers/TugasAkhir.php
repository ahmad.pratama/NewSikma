<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\Writer\Word2007;
use PhpOffice\PhpWord\IOFactory;
use Mpdf\Mpdf;

class TugasAkhir extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->library('form_validation');
        if (!$this->session->userdata('Id')){
            redirect('auth');
        }
    }

    public function index(){
        $data['title'] = 'Tugas Akhir';
        $Id = $this->session->userdata['Id'];
        $RoleId = $this->session->userdata['RoleId'];
        $query = "select * from Users where Id = $Id";
        $User = $this->db->query($query)->row_array();
        $data['User'] = $User;

        $mhs = [2,3,4];
        if (in_array($RoleId, $mhs)){
            /* Available Regis Sempro */
            $queryCekBerkas = "
                            select BerkasTa.ValidFormulir, BerkasTA.ValidKHSReguler, BerkasTA.ValidKHSPendek, BerkasTA.ValidKRS, BerkasTA.ValidFoto, BerkasTA.ValidTranskrip
                            from BerkasPendaftaranTA BerkasTA
                            join TugasAkhirs ta on BerkasTA.TaId = ta.Id
                            Join Users usr on ta.MhsId = usr.Id
                            where usr.Id = $Id
                            ";
    
            $CekBerkas = $this->db->query($queryCekBerkas)->row_array();
            $data['Sempro'] = false;
            $data['Semju'] = false;
            $data['Skripsi'] = false;

            if ($CekBerkas != null) {
                if ($CekBerkas["ValidFormulir"] == 0 || $CekBerkas["ValidKHSReguler"] == 0 || $CekBerkas["ValidKHSPendek"] == 0 || $CekBerkas["ValidKRS"] == 0 || $CekBerkas["ValidFoto"] == 0 || $CekBerkas["ValidTranskrip"] == 0){
                    $tampilSempro = false;
                }else{
                    $tampilSempro = true;
                }
        
                $data['Sempro'] = $tampilSempro;

                /* Avaliable Regis Semju */
                $queryCekNilaiSempro = "
                                        select Dsn1Nas4, Dsn2Nas4, Dsn3Nas4, NilaiAkhir
                                        from NilaiSeminarProposalTA NilaiSempro
                                        join TugasAkhirs ta on NilaiSempro.TaId = ta.Id
                                        Join Users usr on ta.MhsId = usr.Id
                                        where usr.Id = $Id
                                        ";
                $NilaiSempro = $this->db->query($queryCekNilaiSempro)->row_array();

                if ($NilaiSempro != null){
                    if ($NilaiSempro['Dsn1Nas4'] > 0 && $NilaiSempro['Dsn2Nas4'] > 0 && $NilaiSempro['Dsn3Nas4'] > 0 && $NilaiSempro['NilaiAkhir'] > 0){
                        $tampilSemju = true;
                    }else{
                        $tampilSemju = false;
                    }

                    $data['Semju'] = $tampilSemju;
                }

                /* Avaliable Regis Skripsi */
                $queryCekNilaiSemju = "
                                        select Dsn1Ber4, Dsn2Ber4, Dsn3Ber4, NilaiAkhir
                                        from NilaiSeminarKemajuanTA NilaiSemju
                                        join TugasAkhirs ta on NilaiSemju.TaId = ta.Id
                                        Join Users usr on ta.MhsId = usr.Id
                                        where usr.Id = $Id
                                        ";
                $NilaiSemju = $this->db->query($queryCekNilaiSemju)->row_array();

                if ($NilaiSemju != null){
                    if ($NilaiSemju['Dsn1Ber4'] > 0 && $NilaiSemju['Dsn2Ber4'] > 0 && $NilaiSemju['Dsn3Ber4'] > 0 && $NilaiSemju['NilaiAkhir'] > 0){
                        $tampilSkripsi = true;
                    }else{
                        $tampilSkripsi = false;
                    }
                    
                    $data['Skripsi'] = $tampilSkripsi;
                }
            }
        }
        
        /* Setting kembali */
        $data['url_kembali'] = get_referer_url();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('tugasakhir/index', $data);
        $this->load->view('templates/footer');
    }

    /* Pendaftaran TA */
    public function PendaftaranTA($id = null){
        $data['title'] = 'Pendaftaran Tugas Akhir';
        if ($id == null){
            $id = $this->session->userdata['Id'];
        }

        $roleId = $this->session->userdata['RoleId'];

        $kadep = $this->session->userdata['Kadep'];
        $sekdep = $this->session->userdata['Sekdep'];
        $kaprodiS1 = $this->session->userdata['KaprodiS1'];
        $kaprodiS2 = $this->session->userdata['KaprodiS2'];
        $kaprodiS3 = $this->session->userdata['KaprodiS3'];

        $querySemesterId = "select Id from AkademikSemester order by Id Desc limit 1";
        $LastSemesterId = $this->db->query($querySemesterId)->row_array();

        $mhsRole = [2,3,4];
        $dsnRole = [1];
        if (in_array($roleId, $mhsRole)){
            $query = "
                        select mhs.*, 
                        usr.Id, usr.Name, usr.Username, usr.Telpon, usr.Email, usr.RoleId, usr.CreatedAt, usr.Status, usr.ImageProfile, usr.AlamatMalang, usr.Alamat
                        from Users usr
                        join UserMahasiswa mhs on usr.Id = mhs.UserId
                        where usr.Id = '$id' 
                        ";

            /* Ambil Data Ta berdasarkan Mahasiswa */
            $queryTa = "
                        select ta.*,
                        usr.Name, usr.Username
                        from TugasAkhirs ta
                        join Users usr on usr.Id = ta.MhsId
                        where ta.MhsId = $id and ta.Status = 1
                        Order By ta.CreatedAt Desc
                        ";
            $TA = $this->db->query($queryTa)->result_array();
        }else if(in_array($roleId, $dsnRole) && ($kadep == false && $sekdep == false && $kaprodiS1 == false && $kaprodiS2 == false && $kaprodiS3 == false)){
            $query = "
                        select dsn.*, 
                        usr.Id, usr.Name, usr.Username, usr.Telpon, usr.Email, usr.RoleId, usr.CreatedAt, usr.Status, usr.ImageProfile, usr.AlamatMalang, usr.Alamat
                        from Users usr
                        join UserTenagaKependidikan dsn on usr.Id = dsn.UserId
                        where usr.Id = '$id'
                        ";

            /* Ambil Data Ta berdasarkan Mahasiswa */
            $queryTa = "
                        select ta.*,
                        usr.Name, usr.Username
                        from TugasAkhirs ta
                        join Users usr on usr.Id = ta.MhsId
                        where (ta.Dosen1Id = $id or ta.Dosen2Id = $id) and ta.Status = 1
                        ";

            if($this->input->post('jenjang') != NULL){
                $jenjang = $this->input->post('jenjang');
                $queryTa = $queryTa . " AND RoleId = '$jenjang'";
            }else{
                $jenjang = 2;
                $queryTa = $queryTa . " AND RoleId = '$jenjang'";
            }

            if($this->input->post('semester') != NULL){
                $semesterId = $this->input->post('semester');
                $queryTa = $queryTa . " AND semesterId = $semesterId"; 
            }else{
                $semesterId = $LastSemesterId['Id'] - 1;
                $queryTa = $queryTa . " AND semesterId = $semesterId"; 
            }

            if($this->input->post('search') != NULL){
                $search = $this->input->post('search');
                $queryTa = $queryTa . " AND usr.Name LIKE '%$search%' OR usr.Username LIKE '%$search%'"; 
            }

            $queryTa = $queryTa . " Order By ta.CreatedAt Desc";

            if($this->input->post('limit') != NULL){
                $limit = $this->input->post('limit');
                $queryTa = $queryTa . " limit $limit"; 
            }

            $TA = $this->db->query($queryTa)->result_array();
        }else{
            $query = "select * from Users where Id = '$id'";
            
            /* Ambil Data Ta berdasarkan Mahasiswa */
            $queryTa = "
                        select ta.*,
                        usr.Name, usr.Username
                        from TugasAkhirs ta
                        join Users usr on usr.Id = ta.MhsId
                        ";

            if($this->input->post('jenjang') != NULL){
                $jenjang = $this->input->post('jenjang');
                $queryTa = $queryTa . " AND RoleId = '$jenjang'";
            }else{
                $jenjang = 2;
                $queryTa = $queryTa . " AND RoleId = '$jenjang'";
            }

            if($this->input->post('semester') != NULL){
                $semesterId = $this->input->post('semester');
                $queryTa = $queryTa . " AND semesterId = $semesterId"; 
            }else{
                $semesterId = $LastSemesterId['Id'] - 1;
                $queryTa = $queryTa . " AND semesterId = $semesterId"; 
            }

            if($this->input->post('search') != NULL){
                $search = $this->input->post('search');
                $queryTa = $queryTa . " AND usr.Name LIKE '%$search%' OR usr.Username LIKE '%$search%'"; 
            }

            $queryTa = $queryTa . " Order By ta.CreatedAt Desc";

            if($this->input->post('limit') != NULL){
                $limit = $this->input->post('limit');
                $queryTa = $queryTa . " limit $limit"; 
            }
            
            $TA = $this->db->query($queryTa)->result_array();

        }

        $User = $this->db->query($query)->row_array();
        $data['User'] = $User;

        /* Ambil Data Tahun Akademik */
        $queryAkademik = "
                        select * from AkademikSemester
                        where Status = 1 
                        Order by id Desc
                        ";

        $ListSemester = $this->db->query($queryAkademik)->result_array();
        $data['Semester'] = $ListSemester;
                        
        $queryAkademik = $queryAkademik . " limit 1";
        $Akademik = $this->db->query($queryAkademik)->row_array();
        $data['Akademik'] = $Akademik;
        
        /* Ambil List Dosen */
        $queyrDosen = "select *
                        from Users
                        where RoleId in ('1')
                        ";

        $Dosen = $this->db->query($queyrDosen)->result_array();
        $data['Dosen'] = $Dosen;

        /* Ambil Data lab */
        $queryLab = "
                    select * 
                    from AkademikLaboratorium 
                    where Status = 1 AND IsDeleted = 0 AND Id NOT IN ('6','7')";
        $Lab = $this->db->query($queryLab)->result_array();
        $data['Lab'] = $Lab;

        $data['PendaftaranTA'] = $TA;

        /* Setting kembali */
        $data['url_kembali'] = get_referer_url();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('tugasakhir/PendaftaranTA', $data);
        $this->load->view('templates/footer');
    }

    public function DaftarTA(){
        $datetime = new DateTime();
        $timezone = new DateTimeZone('Asia/Jakarta'); 

        $datetime->setTimezone($timezone);
        $MhsId = htmlspecialchars($this->input->post('mhsId'));

        $Semester = htmlspecialchars($this->input->post('semester'));
        $TahunAkademik = htmlspecialchars($this->input->post('tahunAkademik'));

        $queryCekTA = "
                        select * 
                        from TugasAkhirs 
                        where Status = 1 
                        AND MhsId = $MhsId 
                        AND TahunAkademik = '$TahunAkademik' 
                        AND Semester = '$Semester'";

        $AvailableTA = $this->db->query($queryCekTA)->result_array();

        if (count($AvailableTA) > 0){
            redirect('TugasAkhir/PendaftaranTA');
        }
        
        $requestData = [
            'MhsId' => $MhsId,
            'Dosen1Id' => htmlspecialchars($this->input->post('Dosen1')),
            'Dosen2Id' => htmlspecialchars($this->input->post('Dosen2')),
            'Semester' => htmlspecialchars($this->input->post('semester')),
            'TahunAkademik' => htmlspecialchars($this->input->post('tahunAkademik')),
            'SemesterId' => htmlspecialchars($this->input->post('semesterId')),
            'JumlahSks' => htmlspecialchars($this->input->post('jumlahSKS')),
            'IPK' => htmlspecialchars($this->input->post('IPK')),
            'BidangMinat' => htmlspecialchars($this->input->post('bidangMinat')),
            'JudulIdn' => htmlspecialchars($this->input->post('judulIdn')),
            'JudulEng' => htmlspecialchars($this->input->post('judulEng')),
            'CreatedAt' => $datetime->format("Y-m-d H:i:s"),
            'Status' => 1,
            'Tahapan' => 1,
        ];

        $this->db->insert('TugasAkhirs', $requestData);

        $queryTA = "select Id from TugasAkhirs where MhsId = $MhsId order by Id Desc limit 1";

        $TaId = $this->db->query($queryTA)->row_array();

        $requestBerkas = [
            'TaId' => $TaId['Id'],
        ];

        $this->db->insert('BerkasPendaftaranTA', $requestBerkas);

        redirect('TugasAkhir/PendaftaranTA');
    }

    public function DetailTA(){
        $TaId = $this->input->post('TaId');
        // $Dosen1Id = $this->input->post('Dosen1Id');
        // $Dosen2Id = $this->input->post('Dosen2Id');

        // if ($Dosen2Id != "0" || $Dosen2Id != "" || $Dosen2Id != null){
        //     $query = "
        //             select ta.*,
        //             Dosen1.Name as Dosen1Name, Dosen1.Id as Dosen1Id,
        //             Dosen2.Name as Dosen2Name, Dosen2.Id as Dosen2Id,
        //             (
        //                 select MaxDateSempro 
        //                 from AkademikSkripsi 
        //                 limit 1) as MaxDateSempro,
        //             (
        //                 select MaxDateSemju 
        //                 from AkademikSkripsi 
        //                 limit 1) as MaxDateSemju,
        //             (
        //                 select MaxDateSkripsi 
        //                 from AkademikSkripsi 
        //                 limit 1) as MaxDateSkripsi
        //             from TugasAkhirs ta 
        //             join Users Dosen1 on ta.Dosen1Id = Dosen1.Id
        //             join Users Dosen2 on ta.Dosen2Id = Dosen2.Id
        //             where ta.Id = $TaId
        //             ";
        // }else{
            $query = "
                    select ta.*,
                    Dosen1.Name as Dosen1Name, Dosen1.Id as Dosen1Id,
                    (
                        select MaxDateSempro 
                        from AkademikSkripsi 
                        limit 1) as MaxDateSempro,
                    (CASE WHEN ta.Dosen2Id IS NOT NULL OR ta.Dosen2Id != 0
                        THEN (SELECT Dosen2.Name
                                FROM Users Dosen2
                                WHERE Id = ta.Dosen2Id)
                        ELSE NULL END) AS Dosen2Name
                    from TugasAkhirs ta 
                    join Users Dosen1 on ta.Dosen1Id = Dosen1.Id
                    where ta.Id = $TaId
                    ";
        // }
        
        $DetailTA = $this->db->query($query)->row_array();

        switch ($DetailTA['BidangMinat']){
            case 0:
                $DetailTA['Bidang'] = "Belum Ada";
                break;
            case 1:
                $DetailTA['Bidang'] = "Biokimia";
                break;
            case 2:
                $DetailTA['Bidang'] = "Kimia Analitik";
                break;
            case 3:
                $DetailTA['Bidang'] = "Kimia Anorganik";
                break;
            case 4:
                $DetailTA['Bidang'] = "Kimia Fisik";
                break;
            case 5:
                $DetailTA['Bidang'] = "Kimia Organik";
                break;

            default:
                $DetailTA['Bidang'] = "-";
        }

        if ($DetailTA['Dosen2Id'] == null){
            $DetailTA['Dosen2Name'] = "-";
        }

        // if ($this->input->post('view') == "true"){
        // /* Return View Modal Content */
        //     echo json_encode($DetailTA);
        // }else{
            echo json_encode($DetailTA);
        // }
    }

    public function EditTA(){
        $requestData = [
            'JumlahSKS' => htmlspecialchars($this->input->post('jumlahSKS')),
            'IPK' => htmlspecialchars($this->input->post('IPK')),
            'BidangMinat' => htmlspecialchars($this->input->post('bidangMinat')),
            'Dosen1Id' => htmlspecialchars($this->input->post('Dosen1')),
            'Dosen2Id' => $this->input->post('Dosen2'),
            'JudulIdn' => htmlspecialchars($this->input->post('judulIdn')),
            'JudulEng' => htmlspecialchars($this->input->post('judulEng')),
        ];

        $this->db->where('Id', htmlspecialchars($this->input->post('TaId')))->update('TugasAkhirs', $requestData);

        $this->PendaftaranTA();
    }

    public function DownloadTemplatePendaftaranTA($TaId = null){
        require_once 'vendor/autoload.php';
        $phpWord = new \PhpOffice\PhpWord\PhpWord();

        $document = new \PhpOffice\PhpWord\TemplateProcessor('assets/templates/word/FormulirTA.docx');

        /* Get Data User */
        $query = "
                select 
                usr.Name, usr.Username, usr.AlamatMalang, usr.Telpon,
                mhs.Angkatan,
                ta.Semester, ta.TahunAkademik, ta.JumlahSks as SKS, ta.IPK, ta.BidangMinat, ta.JudulIdn, ta.CreatedAt as TanggalDaftar,
                Dosen1.Name as Dosen1Name, 
                (CASE WHEN ta.BidangMinat IS NOT NULL OR ta.BidangMinat != 0
                    THEN (SELECT lab.Nama
                            FROM AkademikLaboratorium lab
                            WHERE Id = ta.BidangMinat)
                    ELSE 'Belum Ada' END) AS Bidang,
                (CASE WHEN ta.BidangMinat IS NOT NULL OR ta.BidangMinat != 0
                    THEN (SELECT kalab.Name
                            FROM Users kalab
                            join AkademikLaboratorium lab on lab.Id = ta.BidangMinat
                            WHERE kalab.Id = lab.KalabId)
                    ELSE NULL END) AS NamaKalab,
                (CASE WHEN ta.Dosen2Id IS NOT NULL OR ta.Dosen2Id != 0
                    THEN (SELECT Dosen2.Name
                            FROM Users Dosen2
                            WHERE Id = ta.Dosen2Id)
                    ELSE NULL END) AS Dosen2Name
                from TugasAkhirs ta
                join Users usr on ta.MhsId = usr.Id
                join UserMahasiswa mhs on ta.MhsId = mhs.UserId
                join Users Dosen1 on ta.Dosen1Id = Dosen1.Id
                where ta.Id = $TaId
                ";

        $DataTA = $this->db->query($query)->row_array();

        if ($DataTA['Dosen2Name'] == null){
            $Dosen2Name = "-";
        }else{
            $Dosen2Name = $DataTA["Dosen2Name"];
        };

        /* Set Value */
        $document->setValue('namaMahasiswa', $DataTA['Name']);
        $document->setValue('NIM', $DataTA['Username']);
        $document->setValue('tahunMasuk', $DataTA['Angkatan']);
        $document->setValue('alamatMalang', $DataTA['AlamatMalang']);
        $document->setValue('telpon', $DataTA['Telpon']);
        $document->setValue('semester', $DataTA['Semester']);
        $document->setValue('tahunAkademik', $DataTA['TahunAkademik']);
        $document->setValue('SKS', $DataTA['SKS']);
        $document->setValue('IPK', $DataTA['IPK']);
        $document->setValue('bidangMInat', $DataTA['Bidang']);
        $document->setValue('dospem1', $DataTA['Dosen1Name']);
        $document->setValue('dospem2', $Dosen2Name);
        $document->setValue('judulIdn', $DataTA['JudulIdn']);
        $document->setValue('tglDaftar', date("d-m-Y", strtotime($DataTA['TanggalDaftar'])));
        $document->setValue('namaKalab', $DataTA['NamaKalab']);
        $document->setValue('fotoMhs', "foto 3x4");

        /* Change Filename to userId-FormulirTA.pdf */
        $filename = $TaId.'-FormulirTA.docx';
        $directory = './assets/templates/temp/';
        $fullPath = $directory . $filename;
        $document->saveAs($fullPath);

        // Offer the document as a download link
        $this->load->helper('download');
        force_download($fullPath, NULL);

        sleep(5);
        
        /* Remove the file */
        unlink($fullPath);
    }

    public function DataBerkas(){
        $TaId = $this->input->post('TaId');
        $query = "
                select * from BerkasPendaftaranTA 
                where TaId = $TaId
                ";

        $Berkas = $this->db->query($query)->row_array();

        echo json_encode($Berkas);
    }

    public function UploadFormulirTA(){
        $uploadPath = './assets/uploads/pendaftaran_ta/';
        $config['upload_path'] = $uploadPath;
        $config['allowed_types'] = 'pdf|doc|docx';

        $TaId = $this->input->post('TaId');

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('formulirTA')) {
            $TA = $this->db->get_where('BerkasPendaftaranTA', ['Id' => $TaId])->row_array();

            // File upload success
            $data = $this->upload->data();
            $uploaded_filename = $data['file_name'];
            $extention = pathinfo($uploaded_filename, PATHINFO_EXTENSION);
            $source_path = './assets/uploads/pendaftaran_ta/' . $uploaded_filename;
            $new_filename = $TaId.'-FormulirTA.'.$extention; // Change this to your desired new name
            $new_path = './assets/uploads/pendaftaran_ta/' . $new_filename;

            if (rename($source_path, $new_path)) {
                if ($TA['FileFormulir'] != "" && $TA['FileFormulir'] != $new_filename && $TA['FileFormulir'] != null){
                    unlink('./assets/uploads/pendaftaran_ta/'. $TA['FileFormulir']);
                }

                $datetime = new DateTime;
                $timezone = new DateTimeZone('Asia/Jakarta'); 
        
                $datetime->setTimezone($timezone);

                $requestUploadFormulirTA = [
                    'UploadFormulir' => $datetime->format("Y-m-d H:i:s"),
                    'FileFormulir' => $new_filename
                ];

                $this->db->update('BerkasPendaftaranTA', $requestUploadFormulirTA, "Id = $TaId");

                $UserId = $this->session->userdata('UserId');

                $this->PendaftaranTA($UserId);
            }

            // $response = 'File uploaded successfully: ' . $data['file_name'];
        } else {
            // File upload failed
            $response = 'File upload failed: ' . $this->upload->display_errors();
        }
    }

    public function UploadKHSReguler(){
        $uploadPath = './assets/uploads/pendaftaran_ta/';
        $config['upload_path'] = $uploadPath;
        $config['allowed_types'] = 'pdf|doc|docx';

        $TaId = $this->input->post('TaId');

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('KHSReguler')) {
            $TA = $this->db->get_where('BerkasPendaftaranTA', ['Id' => $TaId])->row_array();

            // File upload success
            $data = $this->upload->data();
            $uploaded_filename = $data['file_name'];
            $extention = pathinfo($uploaded_filename, PATHINFO_EXTENSION);
            $source_path = './assets/uploads/pendaftaran_ta/' . $uploaded_filename;
            $new_filename = $TaId.'-KHSReguler.'.$extention; // Change this to your desired new name
            $new_path = './assets/uploads/pendaftaran_ta/' . $new_filename;

            if (rename($source_path, $new_path)) {
                if ($TA['FileKHSReguler'] != "" && $TA['FileKHSReguler'] != $new_filename && $TA['FileKHSReguler'] != null){
                    unlink('./assets/uploads/pendaftaran_ta/'. $TA['FileKHSReguler']);
                }

                $datetime = new DateTime;
                $timezone = new DateTimeZone('Asia/Jakarta'); 
        
                $datetime->setTimezone($timezone);

                $requestUploadKHSReguler = [
                    'UploadKHSReguler' => $datetime->format("Y-m-d H:i:s"),
                    'FileKHSReguler' => $new_filename
                ];

                $this->db->update('BerkasPendaftaranTA', $requestUploadKHSReguler, "Id = $TaId");

                $UserId = $this->session->userdata('UserId');

                $this->PendaftaranTA($UserId);
            }

            // $response = 'File uploaded successfully: ' . $data['file_name'];
        } else {
            // File upload failed
            $response = 'File upload failed: ' . $this->upload->display_errors();
        }
    }

    public function UploadKHSPendek(){
        $uploadPath = './assets/uploads/pendaftaran_ta/';
        $config['upload_path'] = $uploadPath;
        $config['allowed_types'] = 'pdf|doc|docx';

        $TaId = $this->input->post('TaId');

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('KHSPendek')) {
            $TA = $this->db->get_where('BerkasPendaftaranTA', ['Id' => $TaId])->row_array();

            // File upload success
            $data = $this->upload->data();
            $uploaded_filename = $data['file_name'];
            $extention = pathinfo($uploaded_filename, PATHINFO_EXTENSION);
            $source_path = './assets/uploads/pendaftaran_ta/' . $uploaded_filename;
            $new_filename = $TaId.'-KHSPendek.'.$extention; // Change this to your desired new name
            $new_path = './assets/uploads/pendaftaran_ta/' . $new_filename;

            if (rename($source_path, $new_path)) {
                if ($TA['FileKHSPendek'] != "" && $TA['FileKHSPendek'] != $new_filename && $TA['FileKHSPendek'] != null){
                    unlink('./assets/uploads/pendaftaran_ta/'. $TA['FileKHSPendek']);
                }

                $datetime = new DateTime;
                $timezone = new DateTimeZone('Asia/Jakarta'); 
        
                $datetime->setTimezone($timezone);

                $requestUploadKHSPendek = [
                    'UploadKHSPendek' => $datetime->format("Y-m-d H:i:s"),
                    'FileKHSPendek' => $new_filename
                ];

                $this->db->update('BerkasPendaftaranTA', $requestUploadKHSPendek, "Id = $TaId");

                $UserId = $this->session->userdata('UserId');

                $this->PendaftaranTA($UserId);
            }

            // $response = 'File uploaded successfully: ' . $data['file_name'];
        } else {
            // File upload failed
            $response = 'File upload failed: ' . $this->upload->display_errors();
        }
    }

    public function UploadKRS(){
        $uploadPath = './assets/uploads/pendaftaran_ta/';
        $config['upload_path'] = $uploadPath;
        $config['allowed_types'] = 'pdf|doc|docx';

        $TaId = $this->input->post('TaId');

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('KRS')) {
            $TA = $this->db->get_where('BerkasPendaftaranTA', ['Id' => $TaId])->row_array();

            // File upload success
            $data = $this->upload->data();
            $uploaded_filename = $data['file_name'];
            $extention = pathinfo($uploaded_filename, PATHINFO_EXTENSION);
            $source_path = './assets/uploads/pendaftaran_ta/' . $uploaded_filename;
            $new_filename = $TaId.'-KRS.'.$extention; // Change this to your desired new name
            $new_path = './assets/uploads/pendaftaran_ta/' . $new_filename;

            if (rename($source_path, $new_path)) {
                if ($TA['FileKRS'] != "" && $TA['FileKRS'] != $new_filename && $TA['FileKRS'] != null){
                    unlink('./assets/uploads/pendaftaran_ta/'. $TA['FileKRS']);
                }

                $datetime = new DateTime;
                $timezone = new DateTimeZone('Asia/Jakarta'); 
        
                $datetime->setTimezone($timezone);

                $requestUploadKRS = [
                    'UploadKRS' => $datetime->format("Y-m-d H:i:s"),
                    'FileKRS' => $new_filename
                ];

                $this->db->update('BerkasPendaftaranTA', $requestUploadKRS, "Id = $TaId");

                $UserId = $this->session->userdata('UserId');

                $this->PendaftaranTA($UserId);
            }

            // $response = 'File uploaded successfully: ' . $data['file_name'];
        } else {
            // File upload failed
            $response = 'File upload failed: ' . $this->upload->display_errors();
        }
    }

    public function UploadFoto(){
        $uploadPath = './assets/uploads/pendaftaran_ta/';
        $config['upload_path'] = $uploadPath;
        $config['allowed_types'] = 'jpg|jpeg';

        $TaId = $this->input->post('TaId');

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('Foto')) {
            $TA = $this->db->get_where('BerkasPendaftaranTA', ['Id' => $TaId])->row_array();

            // File upload success
            $data = $this->upload->data();
            $uploaded_filename = $data['file_name'];
            $extention = pathinfo($uploaded_filename, PATHINFO_EXTENSION);
            $source_path = './assets/uploads/pendaftaran_ta/' . $uploaded_filename;
            $new_filename = $TaId.'-Foto.'.$extention; // Change this to your desired new name
            $new_path = './assets/uploads/pendaftaran_ta/' . $new_filename;

            if (rename($source_path, $new_path)) {
                if ($TA['FileFoto'] != "" && $TA['FileFoto'] != $new_filename && $TA['FileFoto'] != null){
                    unlink('./assets/uploads/pendaftaran_ta/'. $TA['FileFoto']);
                }

                $datetime = new DateTime;
                $timezone = new DateTimeZone('Asia/Jakarta'); 
        
                $datetime->setTimezone($timezone);

                $requestUploadFoto = [
                    'UploadFoto' => $datetime->format("Y-m-d H:i:s"),
                    'FileFoto' => $new_filename
                ];

                $this->db->update('BerkasPendaftaranTA', $requestUploadFoto, "Id = $TaId");

                $UserId = $this->session->userdata('UserId');

                $this->PendaftaranTA($UserId);
            }

            // $response = 'File uploaded successfully: ' . $data['file_name'];
        } else {
            // File upload failed
            $response = 'File upload failed: ' . $this->upload->display_errors();
            var_dump($response);
        }
    }

    public function UploadKartuSeminar(){
        $uploadPath = './assets/uploads/pendaftaran_ta/';
        $config['upload_path'] = $uploadPath;
        $config['allowed_types'] = 'pdf|doc|docx';

        $TaId = $this->input->post('TaId');

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('KartuSeminar')) {
            $TA = $this->db->get_where('BerkasPendaftaranTA', ['Id' => $TaId])->row_array();

            // File upload success
            $data = $this->upload->data();
            $uploaded_filename = $data['file_name'];
            $extention = pathinfo($uploaded_filename, PATHINFO_EXTENSION);
            $source_path = './assets/uploads/pendaftaran_ta/' . $uploaded_filename;
            $new_filename = $TaId.'-KartuSeminar.'.$extention; // Change this to your desired new name
            $new_path = './assets/uploads/pendaftaran_ta/' . $new_filename;

            if (rename($source_path, $new_path)) {
                if ($TA['FileKartuSeminar'] != "" && $TA['FileKartuSeminar'] != $new_filename && $TA['FileKartuSeminar'] != null){
                    unlink('./assets/uploads/pendaftaran_ta/'. $TA['FileKartuSeminar']);
                }

                $datetime = new DateTime;
                $timezone = new DateTimeZone('Asia/Jakarta'); 
        
                $datetime->setTimezone($timezone);

                $requestUploadKartuSeminar = [
                    'UploadKartuSeminar' => $datetime->format("Y-m-d H:i:s"),
                    'FileKartuSeminar' => $new_filename
                ];

                $this->db->update('BerkasPendaftaranTA', $requestUploadKartuSeminar, "Id = $TaId");

                $UserId = $this->session->userdata('UserId');

                $this->PendaftaranTA($UserId);
            }

            // $response = 'File uploaded successfully: ' . $data['file_name'];
        } else {
            // File upload failed
            $response = 'File upload failed: ' . $this->upload->display_errors();
        }
    }
    
    public function UploadFIDK(){
        $uploadPath = './assets/uploads/pendaftaran_ta/';
        $config['upload_path'] = $uploadPath;
        $config['allowed_types'] = 'pdf|doc|docx';

        $TaId = $this->input->post('TaId');

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('FIDK')) {
            $TA = $this->db->get_where('BerkasPendaftaranTA', ['Id' => $TaId])->row_array();

            // File upload success
            $data = $this->upload->data();
            $uploaded_filename = $data['file_name'];
            $extention = pathinfo($uploaded_filename, PATHINFO_EXTENSION);
            $source_path = './assets/uploads/pendaftaran_ta/' . $uploaded_filename;
            $new_filename = $TaId.'-FIDK.'.$extention; // Change this to your desired new name
            $new_path = './assets/uploads/pendaftaran_ta/' . $new_filename;

            if (rename($source_path, $new_path)) {
                if ($TA['FileFIDK'] != "" && $TA['FileFIDK'] != $new_filename && $TA['FileFIDK'] != null){
                    unlink('./assets/uploads/pendaftaran_ta/'. $TA['FileFIDK']);
                }

                $datetime = new DateTime;
                $timezone = new DateTimeZone('Asia/Jakarta'); 
        
                $datetime->setTimezone($timezone);

                $requestUploadFIDK = [
                    'UploadFIDK' => $datetime->format("Y-m-d H:i:s"),
                    'FileFIDK' => $new_filename
                ];

                $this->db->update('BerkasPendaftaranTA', $requestUploadFIDK, "Id = $TaId");

                $UserId = $this->session->userdata('UserId');

                $this->PendaftaranTA($UserId);
            }

            // $response = 'File uploaded successfully: ' . $data['file_name'];
        } else {
            // File upload failed
            $response = 'File upload failed: ' . $this->upload->display_errors();
        }
    }

    public function UploadTranskrip(){
        $uploadPath = './assets/uploads/pendaftaran_ta/';
        $config['upload_path'] = $uploadPath;
        $config['allowed_types'] = 'pdf|doc|docx';

        $TaId = $this->input->post('TaId');

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('Transkrip')) {
            $TA = $this->db->get_where('BerkasPendaftaranTA', ['Id' => $TaId])->row_array();

            // File upload success
            $data = $this->upload->data();
            $uploaded_filename = $data['file_name'];
            $extention = pathinfo($uploaded_filename, PATHINFO_EXTENSION);
            $source_path = './assets/uploads/pendaftaran_ta/' . $uploaded_filename;
            $new_filename = $TaId.'-Transkrip.'.$extention; // Change this to your desired new name
            $new_path = './assets/uploads/pendaftaran_ta/' . $new_filename;

            if (rename($source_path, $new_path)) {
                if ($TA['FileTranskrip'] != "" && $TA['FileTranskrip'] != $new_filename && $TA['FileTranskrip'] != null){
                    unlink('./assets/uploads/pendaftaran_ta/'. $TA['FileTranskrip']);
                }

                $datetime = new DateTime;
                $timezone = new DateTimeZone('Asia/Jakarta'); 
        
                $datetime->setTimezone($timezone);

                $requestUploadTranskrip = [
                    'UploadTranskrip' => $datetime->format("Y-m-d H:i:s"),
                    'FileTranskrip' => $new_filename
                ];

                $this->db->update('BerkasPendaftaranTA', $requestUploadTranskrip, "Id = $TaId");

                $UserId = $this->session->userdata('UserId');

                $this->PendaftaranTA($UserId);
            }

            // $response = 'File uploaded successfully: ' . $data['file_name'];
        } else {
            // File upload failed
            $response = 'File upload failed: ' . $this->upload->display_errors();
        }
    }

    public function ValidFormulir(){
        $TaId = $this->input->post('TaId');
        $requestData = [
            'ValidFormulir' => $this->input->post('ValidFormulir'),
        ];

        $this->db->update('BerkasPendaftaranTA', $requestData, "TaId = $TaId");

        redirect('TugasAkhir/PendaftaranTA');
    }

    public function ValidKHSReguler(){
        $TaId = $this->input->post('TaId');
        $requestData = [
            'ValidKHSReguler' => $this->input->post('ValidKHSReguler'),
        ];

        $this->db->update('BerkasPendaftaranTA', $requestData, "TaId = $TaId");

        redirect('TugasAkhir/PendaftaranTA');
    }

    public function ValidKHSPendek(){
        $TaId = $this->input->post('TaId');
        $requestData = [
            'ValidKHSPendek' => $this->input->post('ValidKHSPendek'),
        ];

        $this->db->update('BerkasPendaftaranTA', $requestData, "TaId = $TaId");

        redirect('TugasAkhir/PendaftaranTA');
    }

    public function ValidKRS(){
        $TaId = $this->input->post('TaId');
        $requestData = [
            'ValidKRS' => $this->input->post('ValidKRS'),
        ];

        $this->db->update('BerkasPendaftaranTA', $requestData, "TaId = $TaId");

        redirect('TugasAkhir/PendaftaranTA');
    }

    public function ValidFoto(){
        $TaId = $this->input->post('TaId');
        $requestData = [
            'ValidFoto' => $this->input->post('ValidFoto'),
        ];

        $this->db->update('BerkasPendaftaranTA', $requestData, "TaId = $TaId");

        redirect('TugasAkhir/PendaftaranTA');
    }

    public function ValidKartuSeminar(){
        $TaId = $this->input->post('TaId');
        $requestData = [
            'ValidKartuSeminar' => $this->input->post('ValidKartuSeminar'),
        ];

        $this->db->update('BerkasPendaftaranTA', $requestData, "TaId = $TaId");

        redirect('TugasAkhir/PendaftaranTA');
    }

    public function ValidFIDK(){
        $TaId = $this->input->post('TaId');
        $requestData = [
            'ValidFIDK' => $this->input->post('ValidFIDK'),
        ];

        $this->db->update('BerkasPendaftaranTA', $requestData, "TaId = $TaId");

        redirect('TugasAkhir/PendaftaranTA');
    }

    public function ValidTranskrip(){
        $TaId = $this->input->post('TaId');
        $requestData = [
            'ValidTranskrip' => $this->input->post('ValidTranskrip'),
        ];

        $this->db->update('BerkasPendaftaranTA', $requestData, "TaId = $TaId");

        redirect('TugasAkhir/PendaftaranTA');
    }

    public function DownloadExcelMahasiswaTA(){
        /* Ambil data Last Semester */
        $querySemesterId = "select * from AkademikSemester order by Id Desc limit 1";
        $LastSemesterId = $this->db->query($querySemesterId)->row_array();
        $SemesterId = $LastSemesterId['Id'];

        /* Ambil Data Pendaftaran Tugas Akhir */
        $queryTA = "
                    select
                    ta.JudulIdn, ta.BidangMinat,
                    mhs.Name NamaMhs, mhs.Username NIM,
                    dsn.Name NamaPembimbing
                    from TugasAkhirs ta
                    join Users mhs on mhs.Id = ta.MhsId
                    join Users dsn on dsn.Id = ta.Dosen1Id
                    where ta.SemesterId = $SemesterId
                    ";
        $DataTA = $this->db->query($queryTA)->result_array();

        require_once 'vendor/autoload.php';

        // Create a new PhpSpreadsheet object
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();

        // Set the worksheet title
        $spreadsheet->getActiveSheet()->setTitle('Tugas Akhir Data');

        // Add column headers
        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Nama Mahasiswa')
            ->setCellValue('B1', 'NIM')
            ->setCellValue('C1', 'Bidang Minat')
            ->setCellValue('D1', 'Judul Tugas Akhir')
            ->setCellValue('E1', 'Nama Pembimbing');

        // Add data from the array to the Excel file
        $row = 2;
        foreach ($DataTA as $data) {
            $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue('A' . $row, $data['NamaMhs'])
                ->setCellValue('B' . $row, $data['NIM'])
                ->setCellValue('C' . $row, $data['BidangMinat'])
                ->setCellValue('D' . $row, $data['JudulIdn'])
                ->setCellValue('E' . $row, $data['NamaPembimbing']);

            // Set the column format to text for columns B and D (NIM and Judul Tugas Akhir)
            $spreadsheet->getActiveSheet()->getStyle('A' . $row)->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_TEXT);
            $spreadsheet->getActiveSheet()->getStyle('B' . $row)->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_TEXT);
            $spreadsheet->getActiveSheet()->getStyle('C' . $row)->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_TEXT);
            $spreadsheet->getActiveSheet()->getStyle('D' . $row)->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_TEXT);
            $spreadsheet->getActiveSheet()->getStyle('E' . $row)->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_TEXT);

            $row++;
        }
        
        $columnIndex = 'A';
        foreach (range('A', 'E') as $column) {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnIndex)->setAutoSize(true);
            $columnIndex++;
        }

        // Set the header for the Excel file
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="TugasAkhir-' . $LastSemesterId['Semester'] . '-' . $LastSemesterId['TahunAkademik'] . '.xlsx"');
        header('Cache-Control: max-age=0');

        // Create a Writer object for Excel
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');

        // Save the Excel file to php://output (output to browser)
        $writer->save('php://output');
    }

    /* Seminar Proposal */
    public function SeminarProposal($id = null){
        $data['title'] = 'Seminar Proposal';
        if ($id == null){
            $id = $this->session->userdata['Id'];
        }

        $roleId = $this->session->userdata['RoleId'];

        $kadep = $this->session->userdata['Kadep'];
        $sekdep = $this->session->userdata['Sekdep'];
        $kaprodiS1 = $this->session->userdata['KaprodiS1'];
        $kaprodiS2 = $this->session->userdata['KaprodiS2'];
        $kaprodiS3 = $this->session->userdata['KaprodiS3'];

        $querySemesterId = "select Id from AkademikSemester order by Id Desc limit 1";
        $LastSemesterId = $this->db->query($querySemesterId)->row_array();
        $mhsRole = [2,3,4];
        $dsnRole = [1];
        if (in_array($roleId, $mhsRole)){
            $query = "
                        select mhs.*, 
                        usr.Id, usr.Name, usr.Username, usr.Telpon, usr.Email, usr.RoleId, usr.CreatedAt, usr.Status, usr.ImageProfile, usr.AlamatMalang, usr.Alamat
                        from Users usr
                        join UserMahasiswa mhs on usr.Id = mhs.UserId
                        where usr.Id = '$id' 
                        ";

            /* Ambil Data Ta berdasarkan Mahasiswa */
            $queryTa = "
                        select ta.*, 
                        usr.Name, usr.Username
                        from TugasAkhirs ta
                        join Users usr on usr.Id = ta.MhsId
                        where ta.MhsId = $id and ta.Status = 1
                        Order By ta.Id Desc
                        ";
            $TA = $this->db->query($queryTa)->result_array();

            if ($TA != null){
                /* Menggabungkan TaId */
                $TaIds = [];
                foreach ($TA as $item) {
                    $TaIds[] = $item["Id"];
                }
                $values = array_map('strval', $TaIds);
                $encodedString = "(" . implode(",", $values) . ")";

                /* Ambil Data Sempro */
                $querySempro = "
                                select sempro.TaId, sempro.TanggalUjian, sempro.JamMulai, sempro.JamSelesai, sempro.RuangId, sempro.LinkZoom, sempro.MakalahSempro,
                                (CASE WHEN sempro.RuangId IS NOT NULL
                                THEN (SELECT Nama
                                        FROM AkademikRuangan
                                        WHERE Id = sempro.RuangId)
                                ELSE NULL END) AS RuangNama,
                                (CASE WHEN sempro.Dosen1Id IS NOT NULL
                                    THEN (SELECT Dosen1.Name
                                            FROM Users Dosen1
                                            WHERE Id = sempro.Dosen1Id)
                                    ELSE NULL END) AS Dosen1Name,
                                (CASE WHEN sempro.Dosen2Id IS NOT NULL
                                    THEN (SELECT Dosen2.Name
                                            FROM Users Dosen2
                                            WHERE Id = sempro.Dosen2Id)
                                    ELSE NULL END) AS Dosen2Name,
                                (CASE WHEN sempro.Dosen3Id IS NOT NULL
                                    THEN (SELECT Dosen3.Name
                                            FROM Users Dosen3
                                            WHERE Id = sempro.Dosen3Id)
                                    ELSE NULL END) AS Dosen3Name
                                from SeminarProposalTA sempro
                                where TaId in $encodedString
                                ";

                $Sempro = $this->db->query($querySempro)->result_array();
                if (count($Sempro) > 0) {
                    /* Menggabungkan Data Sempro Dengan Data TA */
                    foreach ($TA as $key => $dataTA) {
                        foreach ($Sempro as $dataSempro) {
                            if ($dataTA['Id'] == $dataSempro['TaId']) {
                                $TA[$key] = array_merge($dataTA, $dataSempro);
                                break;
                            }
                        }
                    }
                }

                /* Ambil Nilai Sempro */
                $queryNilaiSempro = "
                                    select 
                                    Dsn1TotalPres, 
                                    Dsn1TotalNas, 
                                    Dsn2TotalPres, 
                                    Dsn2TotalNas, 
                                    Dsn3TotalPres, 
                                    Dsn3TotalNas
                                    from NilaiSeminarProposalTA
                                    where TaId in $encodedString
                                    ";
                $NilaiSempro = $this->db->query($queryNilaiSempro)->result_array();
                if (count($NilaiSempro) > 0){
                    foreach ($TA as $key => $dataTA) {
                        foreach ($NilaiSempro as $dataNilaiSempro) {
                            if ($dataNilaiSempro['Dsn1TotalPres'] > 0 && $dataNilaiSempro['Dsn1TotalNas'] > 0 && $dataNilaiSempro['Dsn2TotalPres'] > 0 && $dataNilaiSempro['Dsn2TotalNas'] > 0 && $dataNilaiSempro['Dsn3TotalPres'] > 0 && $dataNilaiSempro['Dsn3TotalNas'] > 0){
                                $TA[$key]['BA'] = true;
                                break;
                            }
                        }
                    }
                }
            }

        }else if(in_array($roleId, $dsnRole) && ($kadep == false && $sekdep == false && $kaprodiS1 == false && $kaprodiS2 == false && $kaprodiS3 == false)){
            $query = "
                        select dsn.*, 
                        usr.Id, usr.Name, usr.Username, usr.Telpon, usr.Email, usr.RoleId, usr.CreatedAt, usr.Status, usr.ImageProfile, usr.AlamatMalang, usr.Alamat
                        from Users usr
                        join UserTenagaKependidikan dsn on usr.Id = dsn.UserId
                        where usr.Id = '$id'
                        ";

            /* Ambil Data Ta berdasarkan Mahasiswa */
            $queryTa = "
                        select ta.*, 
                        usr.Name, usr.Username,
                        (CASE WHEN sempro.Dosen1Id IS NOT NULL
                            THEN (SELECT Dosen1.Name
                                    FROM Users Dosen1
                                    WHERE Id = sempro.Dosen1Id)
                            ELSE NULL END) AS Dosen1Name,
                        (CASE WHEN sempro.Dosen2Id IS NOT NULL
                            THEN (SELECT Dosen2.Name
                                    FROM Users Dosen2
                                    WHERE Id = sempro.Dosen2Id)
                            ELSE NULL END) AS Dosen2Name,
                        (CASE WHEN sempro.Dosen3Id IS NOT NULL
                            THEN (SELECT Dosen3.Name
                                    FROM Users Dosen3
                                    WHERE Id = sempro.Dosen3Id)
                            ELSE NULL END) AS Dosen3Name
                        from TugasAkhirs ta
                        join Users usr on usr.Id = ta.MhsId
                        join SeminarProposalTA sempro on ta.Id = sempro.TaId
                        where (sempro.Dosen1Id = $id or sempro.Dosen2Id = $id or sempro.Dosen3Id = $id) and ta.Status = 1
                        ";

            if($this->input->post('jenjang') != NULL){
                $jenjang = $this->input->post('jenjang');
                $queryTa = $queryTa . " AND usr.RoleId = '$jenjang'";
            }else{
                $jenjang = 2;
                $queryTa = $queryTa . " AND usr.RoleId = '$jenjang'";
            }

            if($this->input->post('semester') != NULL){
                $semesterId = $this->input->post('semester');
                $queryTa = $queryTa . " AND semesterId = $semesterId"; 
            }else{
                $semesterId = $LastSemesterId['Id'] - 1;
                $queryTa = $queryTa . " AND semesterId = $semesterId"; 
            }

            if($this->input->post('search') != NULL){
                $search = $this->input->post('search');
                $queryTa = $queryTa . " AND usr.Name LIKE '%$search%' OR usr.Username LIKE '%$search%'"; 
            }

            $queryTa = $queryTa . " Order By ta.Id Desc";

            if($this->input->post('limit') != NULL){
                $limit = $this->input->post('limit');
                $queryTa = $queryTa . " limit $limit"; 
            }

            $TA = $this->db->query($queryTa)->result_array();

            if ($TA != null){
                /* Menggabungkan TaId */
                $TaIds = [];
                foreach ($TA as $item) {
                    $TaIds[] = $item["Id"];
                }
                $values = array_map('strval', $TaIds);
                $encodedString = "(" . implode(",", $values) . ")";

                /* Ambil Data Sempro */
                $querySempro = "
                                select sempro.TaId, sempro.TanggalUjian, sempro.JamMulai, sempro.JamSelesai, sempro.RuangId, sempro.LinkZoom, sempro.MakalahSempro,
                                (CASE WHEN sempro.RuangId IS NOT NULL
                                THEN (SELECT Nama
                                        FROM AkademikRuangan
                                        WHERE Id = sempro.RuangId)
                                ELSE NULL END) AS RuangNama,
                                (CASE WHEN sempro.Dosen1Id IS NOT NULL
                                    THEN (SELECT Dosen1.Name
                                            FROM Users Dosen1
                                            WHERE Id = sempro.Dosen1Id)
                                    ELSE NULL END) AS Dosen1Name,
                                (CASE WHEN sempro.Dosen2Id IS NOT NULL
                                    THEN (SELECT Dosen2.Name
                                            FROM Users Dosen2
                                            WHERE Id = sempro.Dosen2Id)
                                    ELSE NULL END) AS Dosen2Name,
                                (CASE WHEN sempro.Dosen3Id IS NOT NULL
                                    THEN (SELECT Dosen3.Name
                                            FROM Users Dosen3
                                            WHERE Id = sempro.Dosen3Id)
                                    ELSE NULL END) AS Dosen3Name
                                from SeminarProposalTA sempro
                                where TaId in $encodedString
                                ";


                $Sempro = $this->db->query($querySempro)->result_array();
                if (count($Sempro) > 0) {
                    /* Menggabungkan Data Sempro Dengan Data TA */
                    foreach ($TA as $key => $dataTA) {
                        foreach ($Sempro as $dataSempro) {
                            if ($dataTA['Id'] == $dataSempro['TaId']) {
                                $TA[$key] = array_merge($dataTA, $dataSempro);
                                break;
                            }
                        }
                    }
                }

                /* Ambil Nilai Sempro */
                $queryNilaiSempro = "
                                    select 
                                    Dsn1TotalPres, 
                                    Dsn1TotalNas, 
                                    Dsn2TotalPres, 
                                    Dsn2TotalNas, 
                                    Dsn3TotalPres, 
                                    Dsn3TotalNas
                                    from NilaiSeminarProposalTA
                                    where TaId in $encodedString
                                    ";
                $NilaiSempro = $this->db->query($queryNilaiSempro)->result_array();
                if (count($NilaiSempro) > 0){
                    foreach ($TA as $key => $dataTA) {
                        foreach ($NilaiSempro as $dataNilaiSempro) {
                            if ($dataNilaiSempro['Dsn1TotalPres'] > 0 && $dataNilaiSempro['Dsn1TotalNas'] > 0 && $dataNilaiSempro['Dsn2TotalPres'] > 0 && $dataNilaiSempro['Dsn2TotalNas'] > 0 && $dataNilaiSempro['Dsn3TotalPres'] > 0 && $dataNilaiSempro['Dsn3TotalNas'] > 0){
                                $TA[$key]['BA'] = true;
                                break;
                            }
                        }
                    }
                }
            }
        }else{
            $query = "select * from Users where Id = '$id'";
            
            /* Ambil Data Ta berdasarkan Mahasiswa */
            $queryTa = "
                        select ta.*, 
                        usr.Name, usr.Username
                        from TugasAkhirs ta
                        join Users usr on usr.Id = ta.MhsId
                        where ta.Status = 1
                        ";

            if($this->input->post('jenjang') != NULL){
                $jenjang = $this->input->post('jenjang');
                $queryTa = $queryTa . " AND usr.RoleId = '$jenjang'";
            }else{
                $jenjang = 2;
                $queryTa = $queryTa . " AND usr.RoleId = '$jenjang'";
            }

            if($this->input->post('semester') != NULL){
                $semesterId = $this->input->post('semester');
                $queryTa = $queryTa . " AND semesterId = $semesterId"; 
            }else{
                $semesterId = $LastSemesterId['Id'] - 1;
                $queryTa = $queryTa . " AND semesterId = $semesterId"; 
            }

            if($this->input->post('search') != NULL){
                $search = $this->input->post('search');
                $queryTa = $queryTa . " AND usr.Name LIKE '%$search%' OR usr.Username LIKE '%$search%'"; 
            }

            $queryTa = $queryTa . " Order By ta.Id Desc";
            
            if($this->input->post('limit') != NULL){
                $limit = $this->input->post('limit');
                $queryTa = $queryTa . " limit $limit"; 
            }

            $TA = $this->db->query($queryTa)->result_array();

            if ($TA != null){
                /* Menggabungkan TaId */
                $TaIds = [];
                foreach ($TA as $item) {
                    $TaIds[] = $item["Id"];
                }
                $values = array_map('strval', $TaIds);
                $encodedString = "(" . implode(",", $values) . ")";
    
                /* Ambil Data Sempro */
                $querySempro = "
                                select sempro.TaId, sempro.TanggalUjian, sempro.JamMulai, sempro.JamSelesai, sempro.RuangId, sempro.LinkZoom, sempro.MakalahSempro,
                                (CASE WHEN sempro.RuangId IS NOT NULL
                                THEN (SELECT Nama
                                        FROM AkademikRuangan
                                        WHERE Id = sempro.RuangId)
                                ELSE NULL END) AS RuangNama,
                                (CASE WHEN sempro.Dosen1Id IS NOT NULL
                                    THEN (SELECT Dosen1.Name
                                            FROM Users Dosen1
                                            WHERE Id = sempro.Dosen1Id)
                                    ELSE NULL END) AS Dosen1Name,
                                (CASE WHEN sempro.Dosen2Id IS NOT NULL
                                    THEN (SELECT Dosen2.Name
                                            FROM Users Dosen2
                                            WHERE Id = sempro.Dosen2Id)
                                    ELSE NULL END) AS Dosen2Name,
                                (CASE WHEN sempro.Dosen3Id IS NOT NULL
                                    THEN (SELECT Dosen3.Name
                                            FROM Users Dosen3
                                            WHERE Id = sempro.Dosen3Id)
                                    ELSE NULL END) AS Dosen3Name
                                from SeminarProposalTA sempro
                                where TaId in $encodedString
                                ";
    
                $Sempro = $this->db->query($querySempro)->result_array();
                if (count($Sempro) > 0) {
                    /* Menggabungkan Data Sempro Dengan Data TA */
                    foreach ($TA as $key => $dataTA) {
                        foreach ($Sempro as $dataSempro) {
                            if ($dataTA['Id'] == $dataSempro['TaId']) {
                                $TA[$key] = array_merge($dataTA, $dataSempro);
                                break;
                            }
                        }
                    }
                }

                /* Ambil Nilai Sempro */
                $queryNilaiSempro = "
                                    select 
                                    Dsn1TotalPres, 
                                    Dsn1TotalNas, 
                                    Dsn2TotalPres, 
                                    Dsn2TotalNas, 
                                    Dsn3TotalPres, 
                                    Dsn3TotalNas
                                    from NilaiSeminarProposalTA
                                    where TaId in $encodedString
                                    ";
                $NilaiSempro = $this->db->query($queryNilaiSempro)->result_array();
                if (count($NilaiSempro) > 0){
                    foreach ($TA as $key => $dataTA) {
                        foreach ($NilaiSempro as $dataNilaiSempro) {
                            if ($dataNilaiSempro['Dsn1TotalPres'] > 0 && $dataNilaiSempro['Dsn1TotalNas'] > 0 && $dataNilaiSempro['Dsn2TotalPres'] > 0 && $dataNilaiSempro['Dsn2TotalNas'] > 0 && $dataNilaiSempro['Dsn3TotalPres'] > 0 && $dataNilaiSempro['Dsn3TotalNas'] > 0){
                                $TA[$key]['BA'] = true;
                                break;
                            }
                        }
                    }
                }
            }
        }

        foreach ($TA as $key => $ta){
            switch ($ta['BidangMinat']){
                case 0:
                    $TA[$key]['Bidang'] = "Belum Ada";
                    break;
                case 1:
                    $TA[$key]['Bidang'] = "Biokimia";
                    break;
                case 2:
                    $TA[$key]['Bidang'] = "Kimia Analitik";
                    break;
                case 3:
                    $TA[$key]['Bidang'] = "Kimia Anorganik";
                    break;
                case 4:
                    $TA[$key]['Bidang'] = "Kimia Fisik";
                    break;
                case 5:
                    $TA[$key]['Bidang'] = "Kimia Organk";
                    break;

                default:
                    $TA[$key]['Bidang'] = '-';
                    break;
            }
        }

        $User = $this->db->query($query)->row_array();
        $data['User'] = $User;

        /* Ambil Data Tahun Akademik */
        $queryAkademik = "
                        select * from AkademikSemester
                        where Status = 1 
                        Order by id Desc
                        ";

        $ListSemester = $this->db->query($queryAkademik)->result_array();
        $data['Semester'] = $ListSemester;

        /* Ambil List Dosen */
        $queyrDosen = "select *
                        from Users
                        where RoleId in ('1')
                        ";

        $Dosen = $this->db->query($queyrDosen)->result_array();
        $data['Dosen'] = $Dosen;

        /* Ambil List Ruangan */
        $queryRuangan = "select *
                        from AkademikRuangan
                        where Status = 1
                        ";
        $Ruangan = $this->db->query($queryRuangan)->result_array();
        $data['Ruangan'] = $Ruangan;

        $data['SeminarProposal'] = $TA;

        /* Setting kembali */
        $data['url_kembali'] = get_referer_url();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('tugasakhir/SeminarProposal', $data);
        $this->load->view('templates/footer');
    }

    public function DaftarSempro(){
        $TaId = htmlspecialchars($this->input->post("TaId"));
        $dosen1 = htmlspecialchars($this->input->post("dosen1"));
        $dosen2 = htmlspecialchars($this->input->post("dosen2"));
        $dosen3 = htmlspecialchars($this->input->post("dosen3"));
        $datetime = new DateTime();
        $timezone = new DateTimeZone('Asia/Jakarta'); 

        $datetime->setTimezone($timezone);

        $requestSempro = [
            "TaId" => $TaId,
            "Dosen1Id" => $dosen1,
            "Dosen2Id" => $dosen2,
            "Dosen3Id" => $dosen3,
            "TanggalUjian" => htmlspecialchars($this->input->post("tanggalUjian")),
            "JamMulai" => htmlspecialchars($this->input->post("jamMulai")),
            "JamSelesai" => htmlspecialchars($this->input->post("jamSelesai")),
            "RuangId" => htmlspecialchars($this->input->post("ruangId")),
            "LinkZoom" => htmlspecialchars($this->input->post("linkZoom")),
            "CreatedAt" => $datetime->format("Y-m-d H:i:s"),
        ];

        if (isset($dosen3)){
            $requestSempro['Dosen3Id'] = $dosen3;
        }


        $this->db->insert('SeminarProposalTA', $requestSempro);

        $requestTA = [
            "Dosen1Id" => $dosen1,
            "JenisPenilaian" => $this->input->post("jenisPenilaian"),
            "Tahapan" => 2
        ];
        $this->db->update('TugasAkhirs', $requestTA, "Id = $TaId");

        // if (isset($dosen3)){
        //     $requestTA = [
        //         "Dosen2Id" => $dosen2,
        //         "Dosen3Id" => $dosen3,
        //     ];

        //     $this->db->update('TugasAkhirs', $requestTA, "Id = $TaId");
        // }

        redirect('TugasAkhir/SeminarProposal');
    }

    public function UploadMakalahSempro(){
        $uploadPath = './assets/uploads/sempro_ta/';
        $config['upload_path'] = $uploadPath;
        $config['allowed_types'] = 'pdf';

        $TaId = $this->input->post('TaId');

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('makalahSempro')) {
            $TA = $this->db->get_where('SeminarProposalTA', ['TaId' => $TaId])->row_array();

            // File upload success
            $data = $this->upload->data();
            $uploaded_filename = $data['file_name'];
            $extention = pathinfo($uploaded_filename, PATHINFO_EXTENSION);
            $source_path = './assets/uploads/sempro_ta/' . $uploaded_filename;
            $new_filename = $TaId.'-MakalahSempro.'.$extention; // Change this to your desired new name
            $new_path = './assets/uploads/sempro_ta/' . $new_filename;

            if (rename($source_path, $new_path)) {
                if ($TA['MakalahSempro'] != "" && $TA['MakalahSempro'] != $new_filename && $TA['MakalahSempro'] != null){
                    unlink('./assets/uploads/sempro_ta/'. $TA['MakalahSempro']);
                }

                $datetime = new DateTime;
                $timezone = new DateTimeZone('Asia/Jakarta'); 
        
                $datetime->setTimezone($timezone);

                $requestUploadMakalahSempro = [
                    'UploadMakalah' => $datetime->format("Y-m-d H:i:s"),
                    'MakalahSempro' => $new_filename
                ];

                $this->db->update('SeminarProposalTA', $requestUploadMakalahSempro, "TaId = $TaId");

                redirect('TugasAkhir/SeminarProposal');
            }

        } else {
            // File upload failed
            $response = 'File upload failed: ' . $this->upload->display_errors();
        }
    }

    public function DetailSempro(){
        $TaId = $this->input->post('TaId');
        /* Ambil Detail Sempro */
        $queryDetailSempro = "
                            select 
                            usr.Name, usr.Username, 
                            ta.BidangMinat, ta.JudulIdn, ta.JudulEng, ta.Id as TaId, sempro.Dosen1Id, sempro.Dosen2Id, sempro.Dosen3Id, ta.JenisPenilaian,
                            (CASE WHEN sempro.Dosen1Id IS NOT NULL OR sempro.Dosen1Id != 0
                                THEN (SELECT Dosen1.Name
                                        FROM Users Dosen1
                                        WHERE Id = sempro.Dosen1Id)
                                ELSE NULL END) AS Dosen1Nama,
                            (CASE WHEN sempro.Dosen2Id IS NOT NULL OR sempro.Dosen2Id != 0
                                THEN (SELECT Dosen2.Name
                                        FROM Users Dosen2
                                        WHERE Id = sempro.Dosen2Id)
                                ELSE NULL END) AS Dosen2Nama,
                            (CASE WHEN sempro.Dosen3Id IS NOT NULL OR sempro.Dosen3Id != 0
                                THEN (SELECT Dosen3.Name
                                        FROM Users Dosen3
                                        WHERE Id = sempro.Dosen3Id)
                                ELSE NULL END) AS Dosen3Nama,
                            sempro.TanggalUjian, sempro.JamMulai, sempro.JamSelesai, sempro.RuangId, sempro.LinkZoom,
                            (CASE WHEN sempro.RuangId IS NOT NULL
                            THEN (SELECT Nama
                                    FROM AkademikRuangan
                                    WHERE Id = sempro.RuangId)
                            ELSE NULL END) AS RuangNama,
                            (
                                select MaxDateSempro 
                                from AkademikSkripsi 
                                limit 1) as MaxDateSempro
                            from Users usr
                            join TugasAkhirs ta on usr.Id = ta.MhsId
                            join SeminarProposalTA sempro on ta.Id = sempro.TaId
                            where ta.Id = $TaId
                            ";

        $DetailSempro = $this->db->query($queryDetailSempro)->row_array();

        switch ($DetailSempro['BidangMinat']){
            case 0:
                $DetailSempro['Bidang'] = "Belum Ada";
                break;
            case 1:
                $DetailSempro['Bidang'] = "Biokimia";
                break;
            case 2:
                $DetailSempro['Bidang'] = "Kimia Analitik";
                break;
            case 3:
                $DetailSempro['Bidang'] = "Kimia Anorganik";
                break;
            case 4:
                $DetailSempro['Bidang'] = "Kimia Fisik";
                break;
            case 5:
                $DetailSempro['Bidang'] = "Kimia Organik";
                break;

            default:
                $DetailSempro['Bidang'] = "-";
        }

        $DetailSempro['Waktu'] = $DetailSempro['JamMulai'] . " - " . $DetailSempro['JamSelesai'];

        echo json_encode($DetailSempro);
    }

    public function EditSempro(){

        $TaId = htmlspecialchars($this->input->post('TaId'));

        $RuangId = htmlspecialchars($this->input->post('ruangId'));
        $LinkZoom = htmlspecialchars($this->input->post('linkZoom'));
        
        $requestSempro = [
            "TanggalUjian" => htmlspecialchars($this->input->post('tanggalUjian')),
            "JamMulai" => htmlspecialchars($this->input->post('jamMulai')),
            "JamSelesai" => htmlspecialchars($this->input->post('jamSelesai')),
            "RuangId" => $RuangId,
            "LinkZoom" => $LinkZoom,
            "Dosen1Id" => $this->input->post("dosen1"),
            "Dosen2Id" => $this->input->post("dosen2"),
            "Dosen3Id" => $this->input->post("dosen3")
        ];

        $this->db->update('SeminarProposalTA', $requestSempro, "TaId = $TaId");

        $requestTA = [
            "JenisPenilaian" => $this->input->post('jenisPenilaianSempro'),
        ];

        $this->db->update('TugasAkhirs', $requestTA, "Id = $TaId");

        redirect('TugasAkhir/SeminarProposal');
    }

    public function NilaiSemproTA($TaId = null){
        $data['title'] = 'Nilai Seminar Proposal';

        $Userid = $this->session->userdata['Id'];

        $roleId = $this->session->userdata['RoleId'];
        $mhsRole = [2,3,4];
        $dsnRole = [1,6];

        /* Cek Dosen */
        $queryTa = "
                    select ta.JenisPenilaian,
                    mhs.Name as NamaMahasiswa,
                    sempro.Dosen1Id, sempro.Dosen2Id, sempro.Dosen3Id,
                    (CASE WHEN sempro.Dosen1Id IS NOT NULL OR sempro.Dosen1Id != 0
                        THEN (SELECT Dosen1.Name
                                FROM Users Dosen1
                                WHERE Id = sempro.Dosen1Id)
                        ELSE '-' END) AS Dosen1Name,
                    (CASE WHEN sempro.Dosen2Id IS NOT NULL OR sempro.Dosen2Id != 0
                        THEN (SELECT Dosen2.Name
                                FROM Users Dosen2
                                WHERE Id = sempro.Dosen2Id)
                        ELSE '-' END) AS Dosen2Name,
                    (CASE WHEN sempro.Dosen3Id IS NOT NULL OR sempro.Dosen3Id != 0
                        THEN (SELECT Dosen3.Name
                                FROM Users Dosen3
                                WHERE Id = sempro.Dosen3Id)
                        ELSE '-' END) AS Dosen3Name
                    FROM SeminarProposalTA sempro 
                    JOIN TugasAkhirs ta ON sempro.TaId = ta.Id 
                    JOIN Users mhs ON ta.MhsId = mhs.Id
                    where ta.Id = $TaId
                    ";

        $TA = $this->db->query($queryTa)->row_array();

        if (in_array($roleId, $mhsRole)){
            $query = "
                        select mhs.*, 
                        usr.Id, usr.Name, usr.Username, usr.Telpon, usr.Email, usr.RoleId, usr.CreatedAt, usr.Status, usr.ImageProfile, usr.AlamatMalang, usr.Alamat
                        from Users usr
                        join UserMahasiswa mhs on usr.Id = mhs.UserId
                        where usr.Id = '$Userid' 
                        ";

        }else if(in_array($roleId, $dsnRole)){
            $query = "
                        select dsn.*, 
                        usr.Id, usr.Name, usr.Username, usr.Telpon, usr.Email, usr.RoleId, usr.CreatedAt, usr.Status, usr.ImageProfile, usr.AlamatMalang, usr.Alamat
                        from Users usr
                        join UserTenagaKependidikan dsn on usr.Id = dsn.UserId
                        where usr.Id = '$Userid'
                        ";

        }else{
            $query = "select * from Users where Id = '$Userid'";
        }

        /* Perhitungan Nilai Seminar Proposal */
        /* Ambil Nilai Sempro */
        $queryNilaiSemproTA = "
                            select nilai.*,
                            sempro.Dosen1Id, sempro.Dosen2Id, sempro.Dosen3Id,
                            (CASE WHEN sempro.Dosen1Id IS NOT NULL
                            THEN (SELECT Dosen.Name
                                    FROM Users Dosen
                                    WHERE Id = sempro.Dosen1Id)
                            ELSE NULL END) AS Dosen1Name,
                            (CASE WHEN sempro.Dosen2Id IS NOT NULL
                            THEN (SELECT Dosen.Name
                                    FROM Users Dosen
                                    WHERE Id = sempro.Dosen2Id)
                            ELSE NULL END) AS Dosen2Name,
                            (CASE WHEN sempro.Dosen3Id IS NOT NULL
                            THEN (SELECT Dosen.Name
                                    FROM Users Dosen
                                    WHERE Id = sempro.Dosen3Id)
                            ELSE NULL END) AS Dosen3Name
                            from NilaiSeminarProposalTA nilai
                            join SeminarProposalTA sempro on nilai.TaId = sempro.TaId
                            where nilai.TaId = $TaId
                            ";
        
        $NilaiSemproTA = $this->db->query($queryNilaiSemproTA)->row_array();

        if($NilaiSemproTA != null){
            /* Dosen 1 */
            if ($NilaiSemproTA['Dsn1Pres1'] != null){
                $totalNilaiPresentasiDsn1 = ($NilaiSemproTA['Dsn1Pres1'] * 2) + ($NilaiSemproTA['Dsn1Pres2'] * 2) + ($NilaiSemproTA['Dsn1Pres3'] * 2.5) + ($NilaiSemproTA['Dsn1Pres4'] * 2.5);
            }else{
                $totalNilaiPresentasiDsn1 = 0;
            }
            if ($NilaiSemproTA['Dsn1Nas1'] != null){
                $totalNilaiNaskahDsn1 = ($NilaiSemproTA['Dsn1Nas1'] * 3) + ($NilaiSemproTA['Dsn1Nas2'] * 3) + ($NilaiSemproTA['Dsn1Nas3'] * 2.5) + ($NilaiSemproTA['Dsn1Nas4'] * 2.5);
            }else{
                $totalNilaiNaskahDsn1 = 0;
            }
            $totalNilaiSemproDsn1TA = ($totalNilaiPresentasiDsn1 + $totalNilaiNaskahDsn1)/100;

            /* Dosen 2 */
            if ($NilaiSemproTA['Dsn2Pres1'] != null){
                $totalNilaiPresentasiDsn2 = ($NilaiSemproTA['Dsn2Pres1'] * 2) + ($NilaiSemproTA['Dsn2Pres2'] * 2) + ($NilaiSemproTA['Dsn2Pres3'] * 2.5) + ($NilaiSemproTA['Dsn2Pres4'] * 2.5);
            }else{
                $totalNilaiPresentasiDsn2 = 0;
            }
            if ($NilaiSemproTA['Dsn2Nas1'] != null){
                $totalNilaiNaskahDsn2 = ($NilaiSemproTA['Dsn2Nas1'] * 3) + ($NilaiSemproTA['Dsn2Nas2'] * 3) + ($NilaiSemproTA['Dsn2Nas3'] * 2.5) + ($NilaiSemproTA['Dsn2Nas4'] * 2.5);
            }else{
                $totalNilaiNaskahDsn2 = 0;
            }
            $totalNilaiSemproDsn2TA = ($totalNilaiPresentasiDsn2 + $totalNilaiNaskahDsn2)/100;

            /* Dosen 3 */
            if ($NilaiSemproTA['Dsn3Pres1'] != null){
                $totalNilaiPresentasiDsn3 = ($NilaiSemproTA['Dsn3Pres1'] * 2) + ($NilaiSemproTA['Dsn3Pres2'] * 2) + ($NilaiSemproTA['Dsn3Pres3'] * 2.5) + ($NilaiSemproTA['Dsn3Pres4'] * 2.5);
            }else{
                $totalNilaiPresentasiDsn3 = 0;
            }
            if ($NilaiSemproTA['Dsn3Nas1'] != null){
                $totalNilaiNaskahDsn3 = ($NilaiSemproTA['Dsn3Nas1'] * 3) + ($NilaiSemproTA['Dsn3Nas2'] * 3) + ($NilaiSemproTA['Dsn3Nas3'] * 2.5) + ($NilaiSemproTA['Dsn3Nas4'] * 2.5);
            }else{
                $totalNilaiNaskahDsn3 = 0;
            }
            $totalNilaiSemproDsn3TA = ($totalNilaiPresentasiDsn3 + $totalNilaiNaskahDsn3)/100;

            /* Perhitungan Nilai Rata-rata */
            $averagePresentasi = ($totalNilaiPresentasiDsn1 + $totalNilaiPresentasiDsn2 + $totalNilaiPresentasiDsn3)/3;
            $averageNaskah = ($totalNilaiNaskahDsn1 + $totalNilaiNaskahDsn2 + $totalNilaiNaskahDsn3)/3;
            // $averageTotalNilai = ($totalNilaiSemproDsn1TA + $totalNilaiSemproDsn2TA + $totalNilaiSemproDsn1TA)/3;

            $data['averagePresentasi'] = number_format($averagePresentasi,2);
            $data['averageNaskah'] = number_format($averageNaskah,2);
            // $data['averageTotalNilai'] = number_format($averageTotalNilai,2);

            /* Saran */
            $data['Dsn1Saran'] = $NilaiSemproTA['Dsn1Saran'];
            $data['Dsn2Saran'] = $NilaiSemproTA['Dsn2Saran'];
            $data['Dsn3Saran'] = $NilaiSemproTA['Dsn3Saran'];

            // Jenis Penilaian (1) = 2 Pembimbing
            // Jenis Penilaian (2) = 1 Pembimbing
            if($TA['JenisPenilaian'] == 1){
                $NilaiAkhirSemproTA = ((($totalNilaiPresentasiDsn1 + $totalNilaiPresentasiDsn2) * 0.35) + ($totalNilaiPresentasiDsn3 * 0.3) + (($totalNilaiNaskahDsn1 + $totalNilaiNaskahDsn2) * 0.4) + ($totalNilaiNaskahDsn3 * 0.2)) / 100;
            }else{
                $NilaiAkhirSemproTA = (($totalNilaiPresentasiDsn1 * 0.5) + (($totalNilaiPresentasiDsn2 + $totalNilaiPresentasiDsn3) * 0.25) + ($totalNilaiNaskahDsn1 * 0.6) + (($totalNilaiNaskahDsn2 + $totalNilaiNaskahDsn3) * 0.2)) / 100;
            }
            $NilaiAkhirSemproTA = number_format($NilaiAkhirSemproTA, 2);
            $data['NilaiAkhirSemproTA'] = $NilaiAkhirSemproTA;

            if ($NilaiSemproTA['NilaiAkhir'] == null || $NilaiSemproTA['NilaiAkhir'] != $NilaiAkhirSemproTA){
                $requestNilaiAkhir = [
                    'Dsn1TotalPres' => $totalNilaiPresentasiDsn1,
                    'Dsn2TotalPres' => $totalNilaiPresentasiDsn2,
                    'Dsn3TotalPres' => $totalNilaiPresentasiDsn3,
                    'Dsn1TotalNas' => $totalNilaiNaskahDsn1,
                    'Dsn2TotalNas' => $totalNilaiNaskahDsn2,
                    'Dsn3TotalNas' => $totalNilaiNaskahDsn3,
                    'NilaiAkhir' => $NilaiAkhirSemproTA
                ];
                $this->db->update('NilaiSeminarProposalTA', $requestNilaiAkhir, "TaId = $TaId");
            }
        }else{
            $totalNilaiPresentasiDsn1 = 0;
            $totalNilaiNaskahDsn1 = 0;
            $totalNilaiSemproDsn1TA = 0;

            $totalNilaiPresentasiDsn2 = 0;
            $totalNilaiNaskahDsn2 = 0;
            $totalNilaiSemproDsn2TA = 0;

            $totalNilaiPresentasiDsn3 = 0;
            $totalNilaiNaskahDsn3 = 0;
            $totalNilaiSemproDsn3TA = 0;

            $data['NilaiAkhirSemproTA'] = 0;
            $data['averagePresentasi'] = 0;
            $data['averageNaskah'] = 0;

            $data['Dsn1Saran'] = "-";
            $data['Dsn2Saran'] = "-";
            $data['Dsn3Saran'] = "-";
        }

        if ($TA['Dosen1Id'] == $Userid){
            $data['TipeDosen'] = 1;
            $data['totalNilaiPresentasiDsn1'] = number_format(($totalNilaiPresentasiDsn1 / 9),2);
            $data['totalNilaiNaskahDsn1'] = number_format(($totalNilaiNaskahDsn1 / 11),2);
            $data['totalNilaiSemproDsn1TA'] = number_format($totalNilaiSemproDsn1TA, 2);
        }else if ($TA['Dosen2Id'] == $Userid){
            $data['TipeDosen'] = 2;
            $data['totalNilaiPresentasiDsn2'] = number_format(($totalNilaiPresentasiDsn2 / 9), 2);
            $data['totalNilaiNaskahDsn2'] = number_format(($totalNilaiNaskahDsn2 / 11),2);
            $data['totalNilaiSemproDsn2TA'] = number_format($totalNilaiSemproDsn2TA,2);
        }else if ($TA['Dosen3Id'] == $Userid){
            $data['TipeDosen'] = 3;
            $data['totalNilaiPresentasiDsn3'] = number_format(($totalNilaiPresentasiDsn3 / 9),2);
            $data['totalNilaiNaskahDsn3'] = number_format(($totalNilaiNaskahDsn3 / 11),2);
            $data['totalNilaiSemproDsn3TA'] = number_format($totalNilaiSemproDsn3TA,2);
        }else{
            $data['TipeDosen'] = 0;
            $data['totalNilaiPresentasiDsn1'] = number_format(($totalNilaiPresentasiDsn1 / 9),2);
            $data['totalNilaiNaskahDsn1'] = number_format(($totalNilaiNaskahDsn1 / 11),2);
            $data['totalNilaiSemproDsn1TA'] = number_format($totalNilaiSemproDsn1TA, 2);

            $data['totalNilaiPresentasiDsn2'] = number_format(($totalNilaiPresentasiDsn2 / 9), 2);
            $data['totalNilaiNaskahDsn2'] = number_format(($totalNilaiNaskahDsn2 / 11),2);
            $data['totalNilaiSemproDsn2TA'] = number_format($totalNilaiSemproDsn2TA,2);

            $data['totalNilaiPresentasiDsn3'] = number_format(($totalNilaiPresentasiDsn3 / 9),2);
            $data['totalNilaiNaskahDsn3'] = number_format(($totalNilaiNaskahDsn3 / 11),2);
            $data['totalNilaiSemproDsn3TA'] = number_format($totalNilaiSemproDsn3TA,2);
        }

        $User = $this->db->query($query)->row_array();
        $data['User'] = $User;
        $data['TaId'] = $TaId;
        $data['DosenId'] = $TA;
        $data['Nilai'] = $NilaiSemproTA;

        /* Setting kembali */
        $data['url_kembali'] = get_referer_url();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('tugasAkhir/NilaiSeminarProposal', $data);
        $this->load->view('templates/footer');
    }

    public function DetailNilaiSempro(){
        $TaId = $this->input->post('TaId');

        $queryNilaiSempro = "
                            select * from NilaiSeminarProposalTA where TaId = $TaId
                            ";

        $NilaiSempro = $this->db->query($queryNilaiSempro)->row_array();

        echo json_encode($NilaiSempro);
    }

    public function SubmitNilaiSempro($id = null){

        $Userid = $this->session->userdata['Id'];

        /* Cek Dosen */
        $queryDsn = "
                    select Dosen1Id, Dosen2Id, Dosen3Id
                    from SeminarProposalTA
                    where TaId = $id
                    ";
        
        $JenisDosen = $this->db->query($queryDsn)->row_array();

        /* Cek Data Nilai */
        $queryNilaiSempro = "
                            select * from NilaiSeminarProposalTA
                            where TaId = $id
                            ";
        
        $NilaiSemproTA = $this->db->query($queryNilaiSempro)->row_array();

        $UpdateNilaiTA = false;
        if ($NilaiSemproTA != null){
            $UpdateNilaiTA = true;
        }

        if ($JenisDosen['Dosen1Id'] == $Userid){
            $requestNilaiSemproTA = [
                "Dsn1Pres1" => htmlspecialchars($this->input->post('tataBahasa_9C')),
                "Dsn1Pres2" => htmlspecialchars($this->input->post('kosaKata_9C')),
                "Dsn1Pres3" => htmlspecialchars($this->input->post('pengucapan_9C')),
                "Dsn1Pres4" => htmlspecialchars($this->input->post('penyampaian_6')),
                "Dsn1Nas1" => htmlspecialchars($this->input->post('ideHasilPekerjaan_9C')),
                "Dsn1Nas2" => htmlspecialchars($this->input->post('masalahTujuan_9C')),
                "Dsn1Nas3" => htmlspecialchars($this->input->post('langkahLangkah_6')),
                "Dsn1Nas4" => htmlspecialchars($this->input->post('cakupan_6')),
            ];
            if (!$UpdateNilaiTA){
                /* Insert Nilai Sempro Dosen 1 */
                $requestNilaiSemproTA['TaId'] = $id;
                $this->db->insert('NilaiSeminarProposalTA', $requestNilaiSemproTA);
            }else{
                /* Update Nilai Sempro Dosen 1 */
                $this->db->update('NilaiSeminarProposalTA', $requestNilaiSemproTA, ['TaId'=> $id]);
            }
        }else if($JenisDosen['Dosen2Id'] == $Userid){
            $requestNilaiSemproTA = [
                "Dsn2Pres1" => htmlspecialchars($this->input->post('tataBahasa_9C')),
                "Dsn2Pres2" => htmlspecialchars($this->input->post('kosaKata_9C')),
                "Dsn2Pres3" => htmlspecialchars($this->input->post('pengucapan_9C')),
                "Dsn2Pres4" => htmlspecialchars($this->input->post('penyampaian_6')),
                "Dsn2Nas1" => htmlspecialchars($this->input->post('ideHasilPekerjaan_9C')),
                "Dsn2Nas2" => htmlspecialchars($this->input->post('masalahTujuan_9C')),
                "Dsn2Nas3" => htmlspecialchars($this->input->post('langkahLangkah_6')),
                "Dsn2Nas4" => htmlspecialchars($this->input->post('cakupan_6')),
            ];
            if (!$UpdateNilaiTA){
                /* Insert Nilai Sempro Dosen 2 */
                $requestNilaiSemproTA['TaId'] = $id;
                $this->db->insert('NilaiSeminarProposalTA', $requestNilaiSemproTA);
            }else{
                /* Update Nilai Sempro Dosen 2 */
                $this->db->update('NilaiSeminarProposalTA', $requestNilaiSemproTA, ['TaId'=> $id]);
            }
        }else if($JenisDosen['Dosen3Id'] == $Userid){
            $requestNilaiSemproTA = [
                "Dsn3Pres1" => htmlspecialchars($this->input->post('tataBahasa_9C')),
                "Dsn3Pres2" => htmlspecialchars($this->input->post('kosaKata_9C')),
                "Dsn3Pres3" => htmlspecialchars($this->input->post('pengucapan_9C')),
                "Dsn3Pres4" => htmlspecialchars($this->input->post('penyampaian_6')),
                "Dsn3Nas1" => htmlspecialchars($this->input->post('ideHasilPekerjaan_9C')),
                "Dsn3Nas2" => htmlspecialchars($this->input->post('masalahTujuan_9C')),
                "Dsn3Nas3" => htmlspecialchars($this->input->post('langkahLangkah_6')),
                "Dsn3Nas4" => htmlspecialchars($this->input->post('cakupan_6')),
            ];
            if (!$UpdateNilaiTA){
                /* Insert Nilai Sempro Dosen 3 */
                $requestNilaiSemproTA['TaId'] = $id;
                $this->db->insert('NilaiSeminarProposalTA', $requestNilaiSemproTA);
            }else{
                /* Update Nilai Sempro Dosen 3 */
                $this->db->update('NilaiSeminarProposalTA', $requestNilaiSemproTA, ['TaId'=> $id]);
            }
        }else if($this->input->post('tipeDosen') != null){
            switch ($this->input->post('tipeDosen')){
                case 1:
                    $requestNilaiSemproTA = [
                        "Dsn1Pres1" => htmlspecialchars($this->input->post('tataBahasa_9C')),
                        "Dsn1Pres2" => htmlspecialchars($this->input->post('kosaKata_9C')),
                        "Dsn1Pres3" => htmlspecialchars($this->input->post('pengucapan_9C')),
                        "Dsn1Pres4" => htmlspecialchars($this->input->post('penyampaian_6')),
                        "Dsn1Nas1" => htmlspecialchars($this->input->post('ideHasilPekerjaan_9C')),
                        "Dsn1Nas2" => htmlspecialchars($this->input->post('masalahTujuan_9C')),
                        "Dsn1Nas3" => htmlspecialchars($this->input->post('langkahLangkah_6')),
                        "Dsn1Nas4" => htmlspecialchars($this->input->post('cakupan_6')),
                    ];
                    break;
                case 2:
                    $requestNilaiSemproTA = [
                        "Dsn2Pres1" => htmlspecialchars($this->input->post('tataBahasa_9C')),
                        "Dsn2Pres2" => htmlspecialchars($this->input->post('kosaKata_9C')),
                        "Dsn2Pres3" => htmlspecialchars($this->input->post('pengucapan_9C')),
                        "Dsn2Pres4" => htmlspecialchars($this->input->post('penyampaian_6')),
                        "Dsn2Nas1" => htmlspecialchars($this->input->post('ideHasilPekerjaan_9C')),
                        "Dsn2Nas2" => htmlspecialchars($this->input->post('masalahTujuan_9C')),
                        "Dsn2Nas3" => htmlspecialchars($this->input->post('langkahLangkah_6')),
                        "Dsn2Nas4" => htmlspecialchars($this->input->post('cakupan_6')),
                    ];
                    break;
                case 3:
                    $requestNilaiSemproTA = [
                        "Dsn3Pres1" => htmlspecialchars($this->input->post('tataBahasa_9C')),
                        "Dsn3Pres2" => htmlspecialchars($this->input->post('kosaKata_9C')),
                        "Dsn3Pres3" => htmlspecialchars($this->input->post('pengucapan_9C')),
                        "Dsn3Pres4" => htmlspecialchars($this->input->post('penyampaian_6')),
                        "Dsn3Nas1" => htmlspecialchars($this->input->post('ideHasilPekerjaan_9C')),
                        "Dsn3Nas2" => htmlspecialchars($this->input->post('masalahTujuan_9C')),
                        "Dsn3Nas3" => htmlspecialchars($this->input->post('langkahLangkah_6')),
                        "Dsn3Nas4" => htmlspecialchars($this->input->post('cakupan_6')),
                    ];
                    break;
            }

            if (!$UpdateNilaiTA){
                /* Insert Nilai Sempro Dosen 1 */
                $requestNilaiSemproTA['TaId'] = $id;
                $this->db->insert('NilaiSeminarProposalTA', $requestNilaiSemproTA);
            }else{
                /* Update Nilai Sempro Dosen 1 */
                $this->db->update('NilaiSeminarProposalTA', $requestNilaiSemproTA, ['TaId'=> $id]);
            }
        }

        redirect('TugasAkhir/NilaiSemproTA/'. $id);
    }

    public function SubmitSaranSempro($id = null){

        $Userid = $this->session->userdata['Id'];

        /* Cek Dosen */
        $queryDsn = "
                    select Dosen1Id, Dosen2Id, Dosen3Id
                    from SeminarProposalTA
                    where TaId = $id
                    ";
        
        $JenisDosen = $this->db->query($queryDsn)->row_array();

        /* Cek Data Saran */
        $querySaranSempro = "
                            select * from NilaiSeminarProposalTA
                            where TaId = $id
                            ";
        
        $SaranSemproTA = $this->db->query($querySaranSempro)->row_array();

        $UpdateSaranTA = false;
        if ($SaranSemproTA != null){
            $UpdateSaranTA = true;
        }

        if ($JenisDosen['Dosen1Id'] == $Userid){
            $requestSaranSemproTA = [
                "Dsn1Saran" => $this->input->post('saran'),
            ];

            if (!$UpdateSaranTA){
                /* Insert Saran Sempro Dosen 1 */
                $requestSaranSemproTA['TaId'] = $id;
                $this->db->insert('NilaiSeminarProposalTA', $requestSaranSemproTA);
            }else{
                /* Update Saran Sempro Dosen 1 */
                $this->db->update('NilaiSeminarProposalTA', $requestSaranSemproTA, ['TaId'=> $id]);
            }
        }else if($JenisDosen['Dosen2Id'] == $Userid){
            $requestSaranSemproTA = [
                "Dsn2Saran" => $this->input->post('saran'),
            ];

            if (!$UpdateSaranTA){
                /* Insert Saran Sempro Dosen 2 */
                $requestSaranSemproTA['TaId'] = $id;
                $this->db->insert('NilaiSeminarProposalTA', $requestSaranSemproTA);
            }else{
                /* Update Saran Sempro Dosen 2 */
                $this->db->update('NilaiSeminarProposalTA', $requestSaranSemproTA, ['TaId'=> $id]);
            }
        }else if($JenisDosen['Dosen3Id'] == $Userid){
            $requestSaranSemproTA = [
                "Dsn3Saran" => $this->input->post('saran'),
            ];

            if (!$UpdateSaranTA){
                /* Insert Saran Sempro Dosen 3 */
                $requestSaranSemproTA['TaId'] = $id;
                $this->db->insert('NilaiSeminarProposalTA', $requestSaranSemproTA);
            }else{
                /* Update Saran Sempro Dosen 3 */
                $this->db->update('NilaiSeminarProposalTA', $requestSaranSemproTA, ['TaId'=> $id]);
            }
        }

        redirect('TugasAkhir/NilaiSemproTA/'. $id);
    }

    /* Seminar Kemajuan */
    public function SeminarKemajuan($id = null){
        $data['title'] = 'Seminar Kemajuan';
        if ($id == null){
            $id = $this->session->userdata['Id'];
        }

        $roleId = $this->session->userdata['RoleId'];

        $kadep = $this->session->userdata['Kadep'];
        $sekdep = $this->session->userdata['Sekdep'];
        $kaprodiS1 = $this->session->userdata['KaprodiS1'];
        $kaprodiS2 = $this->session->userdata['KaprodiS2'];
        $kaprodiS3 = $this->session->userdata['KaprodiS3'];

        $querySemesterId = "select Id from AkademikSemester order by Id Desc limit 1";
        $LastSemesterId = $this->db->query($querySemesterId)->row_array();

        $mhsRole = [2,3,4];
        $dsnRole = [1,6];
        if (in_array($roleId, $mhsRole)){
            $query = "
                        select mhs.*, 
                        usr.Id, usr.Name, usr.Username, usr.Telpon, usr.Email, usr.RoleId, usr.CreatedAt, usr.Status, usr.ImageProfile, usr.AlamatMalang, usr.Alamat
                        from Users usr
                        join UserMahasiswa mhs on usr.Id = mhs.UserId
                        where usr.Id = '$id' 
                        ";

            /* Ambil Data Ta berdasarkan Mahasiswa */
            $queryTa = "
                        select ta.*, 
                        usr.Name, usr.Username
                        from TugasAkhirs ta
                        join Users usr on usr.Id = ta.MhsId
                        where ta.MhsId = $id and ta.Status = 1
                        Order By ta.Id Desc
                        ";
            $TA = $this->db->query($queryTa)->result_array();

            if ($TA != null){
                /* Menggabungkan TaId */
                $TaIds = [];
                foreach ($TA as $item) {
                    $TaIds[] = $item["Id"];
                }
                $values = array_map('strval', $TaIds);
                $encodedString = "(" . implode(",", $values) . ")";

                /* Ambil Data Semju */
                $querySemju = "
                                select semju.TaId, semju.TanggalUjian, semju.JamMulai, semju.JamSelesai, semju.RuangId, semju.LinkZoom, semju.MakalahSemju,
                                (CASE WHEN semju.RuangId IS NOT NULL
                                THEN (SELECT Nama
                                        FROM AkademikRuangan
                                        WHERE Id = semju.RuangId)
                                ELSE NULL END) AS RuangNama,
                                (CASE WHEN semju.Dosen1Id IS NOT NULL
                                    THEN (SELECT Dosen1.Name
                                            FROM Users Dosen1
                                            WHERE Id = semju.Dosen1Id)
                                    ELSE '-' END) AS Dosen1Name,
                                (CASE WHEN semju.Dosen2Id IS NOT NULL
                                    THEN (SELECT Dosen2.Name
                                            FROM Users Dosen2
                                            WHERE Id = semju.Dosen2Id)
                                    ELSE '-' END) AS Dosen2Name,
                                (CASE WHEN semju.Dosen3Id IS NOT NULL
                                    THEN (SELECT Dosen3.Name
                                            FROM Users Dosen3
                                            WHERE Id = semju.Dosen3Id)
                                    ELSE '-' END) AS Dosen3Name
                                from SeminarKemajuanTA semju
                                where TaId in $encodedString
                                ";

                $Semju = $this->db->query($querySemju)->result_array();
                if (count($Semju) > 0) {
                    /* Menggabungkan Data Semju Dengan Data TA */
                    foreach ($TA as $key => $dataTA) {
                        foreach ($Semju as $dataSemju) {
                            if ($dataTA['Id'] == $dataSemju['TaId']) {
                                $TA[$key] = array_merge($dataTA, $dataSemju);
                                break;
                            }
                        }
                    }
                }
            }

        }else if(in_array($roleId, $dsnRole) && ($kadep == false && $sekdep == false && $kaprodiS1 == false && $kaprodiS2 == false && $kaprodiS3 == false)){
            $query = "
                        select dsn.*, 
                        usr.Id, usr.Name, usr.Username, usr.Telpon, usr.Email, usr.RoleId, usr.CreatedAt, usr.Status, usr.ImageProfile, usr.AlamatMalang, usr.Alamat
                        from Users usr
                        join UserTenagaKependidikan dsn on usr.Id = dsn.UserId
                        where usr.Id = '$id'
                        ";

            /* Ambil Data Ta berdasarkan Mahasiswa */
            $queryTa = "
                        select ta.*, 
                        usr.Name, usr.Username,
                        (CASE WHEN semju.Dosen1Id IS NOT NULL
                            THEN (SELECT Dosen1.Name
                                    FROM Users Dosen1
                                    WHERE Id = semju.Dosen1Id)
                            ELSE NULL END) AS Dosen1Name,
                        (CASE WHEN semju.Dosen2Id IS NOT NULL
                            THEN (SELECT Dosen2.Name
                                    FROM Users Dosen2
                                    WHERE Id = semju.Dosen2Id)
                            ELSE NULL END) AS Dosen2Name,
                        (CASE WHEN semju.Dosen3Id IS NOT NULL
                            THEN (SELECT Dosen3.Name
                                    FROM Users Dosen3
                                    WHERE Id = semju.Dosen3Id)
                            ELSE NULL END) AS Dosen3Name
                        from TugasAkhirs ta
                        join Users usr on usr.Id = ta.MhsId
                        join SeminarKemajuanTA semju on ta.Id = semju.TaId
                        where (semju.Dosen1Id = $id or semju.Dosen2Id = $id or semju.Dosen3Id = $id) and ta.Status = 1
                        ";

            if($this->input->post('jenjang') != NULL){
                $jenjang = $this->input->post('jenjang');
                $queryTa = $queryTa . " AND usr.RoleId = '$jenjang'";
            }else{
                $jenjang = 2;
                $queryTa = $queryTa . " AND usr.RoleId = '$jenjang'";
            }

            if($this->input->post('semester') != NULL){
                $semesterId = $this->input->post('semester');
                $queryTa = $queryTa . " AND semesterId = $semesterId"; 
            }else{
                $semesterId = $LastSemesterId['Id'] - 1;
                $queryTa = $queryTa . " AND semesterId = $semesterId"; 
            }

            if($this->input->post('search') != NULL){
                $search = $this->input->post('search');
                $queryTa = $queryTa . " AND usr.Name LIKE '%$search%' OR usr.Username LIKE '%$search%'"; 
            }

            $queryTa = $queryTa . " Order By ta.Id Desc";

            if($this->input->post('limit') != NULL){
                $limit = $this->input->post('limit');
                $queryTa = $queryTa . " limit $limit"; 
            }

            $TA = $this->db->query($queryTa)->result_array();

            if ($TA != null){
                /* Menggabungkan TaId */
                $TaIds = [];
                foreach ($TA as $item) {
                    $TaIds[] = $item["Id"];
                }
                $values = array_map('strval', $TaIds);
                $encodedString = "(" . implode(",", $values) . ")";

                /* Ambil Data Semju */
                $querySemju = "
                                select semju.TaId, semju.TanggalUjian, semju.JamMulai, semju.JamSelesai, semju.RuangId, semju.LinkZoom, semju.MakalahSemju,
                                (CASE WHEN semju.RuangId IS NOT NULL
                                THEN (SELECT Nama
                                        FROM AkademikRuangan
                                        WHERE Id = semju.RuangId)
                                ELSE NULL END) AS RuangNama,
                                (CASE WHEN semju.Dosen1Id IS NOT NULL
                                    THEN (SELECT Dosen1.Name
                                            FROM Users Dosen1
                                            WHERE Id = semju.Dosen1Id)
                                    ELSE NULL END) AS Dosen1Name,
                                (CASE WHEN semju.Dosen2Id IS NOT NULL
                                    THEN (SELECT Dosen2.Name
                                            FROM Users Dosen2
                                            WHERE Id = semju.Dosen2Id)
                                    ELSE NULL END) AS Dosen2Name,
                                (CASE WHEN semju.Dosen3Id IS NOT NULL
                                    THEN (SELECT Dosen3.Name
                                            FROM Users Dosen3
                                            WHERE Id = semju.Dosen3Id)
                                    ELSE NULL END) AS Dosen3Name
                                from SeminarKemajuanTA semju
                                where TaId in $encodedString
                                ";

                $Semju = $this->db->query($querySemju)->result_array();
                if (count($Semju) > 0) {
                    /* Menggabungkan Data Semju Dengan Data TA */
                    foreach ($TA as $key => $dataTA) {
                        foreach ($Semju as $dataSemju) {
                            if ($dataTA['Id'] == $dataSemju['TaId']) {
                                $TA[$key] = array_merge($dataTA, $dataSemju);
                                break;
                            }
                        }
                    }
                }
            }
        }else{
            $query = "select * from Users where Id = '$id'";
            
            /* Ambil Data Ta berdasarkan Mahasiswa */
            $queryTa = "
                        select ta.*, 
                        usr.Name, usr.Username
                        from TugasAkhirs ta
                        join Users usr on usr.Id = ta.MhsId
                        where ta.Status = 1
                        ";

            if($this->input->post('jenjang') != NULL){
                $jenjang = $this->input->post('jenjang');
                $queryTa = $queryTa . " AND usr.RoleId = '$jenjang'";
            }else{
                $jenjang = 2;
                $queryTa = $queryTa . " AND usr.RoleId = '$jenjang'";
            }

            if($this->input->post('semester') != NULL){
                $semesterId = $this->input->post('semester');
                $queryTa = $queryTa . " AND semesterId = $semesterId"; 
            }else{
                $semesterId = $LastSemesterId['Id'] - 1;
                $queryTa = $queryTa . " AND semesterId = $semesterId"; 
            }

            if($this->input->post('search') != NULL){
                $search = $this->input->post('search');
                $queryTa = $queryTa . " AND usr.Name LIKE '%$search%' OR usr.Username LIKE '%$search%'"; 
            }

            $queryTa = $queryTa . " Order By ta.Id Desc";
            
            if($this->input->post('limit') != NULL){
                $limit = $this->input->post('limit');
                $queryTa = $queryTa . " limit $limit"; 
            }

            $TA = $this->db->query($queryTa)->result_array();

            if ($TA != null){
                /* Menggabungkan TaId */
                $TaIds = [];
                foreach ($TA as $item) {
                    $TaIds[] = $item["Id"];
                }
                $values = array_map('strval', $TaIds);
                $encodedString = "(" . implode(",", $values) . ")";

                /* Ambil Data Semju */
                $querySemju = "
                                select semju.TaId, semju.TanggalUjian, semju.JamMulai, semju.JamSelesai, semju.RuangId, semju.LinkZoom, semju.MakalahSemju,
                                (CASE WHEN semju.RuangId IS NOT NULL
                                THEN (SELECT Nama
                                        FROM AkademikRuangan
                                        WHERE Id = semju.RuangId)
                                ELSE NULL END) AS RuangNama,
                                (CASE WHEN semju.Dosen1Id IS NOT NULL
                                    THEN (SELECT Dosen1.Name
                                            FROM Users Dosen1
                                            WHERE Id = semju.Dosen1Id)
                                    ELSE NULL END) AS Dosen1Name,
                                (CASE WHEN semju.Dosen2Id IS NOT NULL
                                    THEN (SELECT Dosen2.Name
                                            FROM Users Dosen2
                                            WHERE Id = semju.Dosen2Id)
                                    ELSE NULL END) AS Dosen2Name,
                                (CASE WHEN semju.Dosen3Id IS NOT NULL
                                    THEN (SELECT Dosen3.Name
                                            FROM Users Dosen3
                                            WHERE Id = semju.Dosen3Id)
                                    ELSE NULL END) AS Dosen3Name
                                from SeminarKemajuanTA semju
                                where TaId in $encodedString
                                ";

                $Semju = $this->db->query($querySemju)->result_array();
                if (count($Semju) > 0) {
                    /* Menggabungkan Data Semju Dengan Data TA */
                    foreach ($TA as $key => $dataTA) {
                        foreach ($Semju as $dataSemju) {
                            if ($dataTA['Id'] == $dataSemju['TaId']) {
                                $TA[$key] = array_merge($dataTA, $dataSemju);
                                break;
                            }
                        }
                    }
                }
            }
        }

        foreach ($TA as $key => $ta){
            switch ($ta['BidangMinat']){
                case 0:
                    $TA[$key]['Bidang'] = "Belum Ada";
                    break;
                case 1:
                    $TA[$key]['Bidang'] = "Biokimia";
                    break;
                case 2:
                    $TA[$key]['Bidang'] = "Kimia Analitik";
                    break;
                case 3:
                    $TA[$key]['Bidang'] = "Kimia Anorganik";
                    break;
                case 4:
                    $TA[$key]['Bidang'] = "Kimia Fisik";
                    break;
                case 5:
                    $TA[$key]['Bidang'] = "Kimia Organk";
                    break;

                default:
                    $TA[$key]['Bidang'] = '-';
                    break;
            }
        }

        $User = $this->db->query($query)->row_array();
        $data['User'] = $User;

        /* Ambil Data Tahun Akademik */
        $queryAkademik = "
                        select * from AkademikSemester
                        where Status = 1 
                        Order by id Desc
                        ";

        $ListSemester = $this->db->query($queryAkademik)->result_array();
        $data['Semester'] = $ListSemester;
                  
        /* Ambil List Dosen */
        $queyrDosen = "select *
                        from Users
                        where RoleId in ('1')
                        ";

        $Dosen = $this->db->query($queyrDosen)->result_array();
        $data['Dosen'] = $Dosen;

        /* Ambil List Ruangan */
        $queryRuangan = "select *
                        from AkademikRuangan
                        where Status = 1
                        ";
        $Ruangan = $this->db->query($queryRuangan)->result_array();
        $data['Ruangan'] = $Ruangan;

        $data['SeminarKemajuan'] = $TA;

        /* Setting kembali */
        $data['url_kembali'] = get_referer_url();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('tugasakhir/SeminarKemajuan', $data);
        $this->load->view('templates/footer');
    }

    public function DaftarSemju(){
        $TaId = htmlspecialchars($this->input->post("TaId"));
        $datetime = new DateTime();
        $timezone = new DateTimeZone('Asia/Jakarta'); 

        $datetime->setTimezone($timezone);

        $dosen1 = htmlspecialchars($this->input->post("dosen1"));
        $dosen2 = htmlspecialchars($this->input->post("dosen2"));
        $dosen3 = htmlspecialchars($this->input->post("dosen3"));

        $requestSemju = [
            "TaId" => $TaId,
            "TanggalUjian" => htmlspecialchars($this->input->post("tanggalUjian")),
            "JamMulai" => htmlspecialchars($this->input->post("jamMulai")),
            "JamSelesai" => htmlspecialchars($this->input->post("jamSelesai")),
            "RuangId" => htmlspecialchars($this->input->post("ruangId")),
            "LinkZoom" => htmlspecialchars($this->input->post("linkZoom")),
            "CreatedAt" => $datetime->format("Y-m-d H:i:s"),
            "Dosen1Id" => $dosen1,
            "Dosen2Id" => $dosen2,
            "Dosen3Id" => $dosen3,
        ];

        $this->db->insert('SeminarKemajuanTA', $requestSemju);

        /* Update Tahapan */
        $requestTA = [
            "Dosen1Id" => $dosen1,
            "JenisPenilaian" => htmlspecialchars($this->input->post("jenisPenilaian")),
            "Tahapan" => 3
        ];
        $this->db->update('TugasAkhirs', $requestTA, "Id = $TaId");

        redirect('TugasAkhir/SeminarKemajuan');
    }

    public function UploadMakalahSemju(){
        $uploadPath = './assets/uploads/semju_ta/';
        $config['upload_path'] = $uploadPath;
        $config['allowed_types'] = 'pdf';

        $TaId = $this->input->post('TaId');

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('makalahSemju')) {
            $TA = $this->db->get_where('SeminarKemajuanTA', ['TaId' => $TaId])->row_array();

            // File upload success
            $data = $this->upload->data();
            $uploaded_filename = $data['file_name'];
            $extention = pathinfo($uploaded_filename, PATHINFO_EXTENSION);
            $source_path = './assets/uploads/semju_ta/' . $uploaded_filename;
            $new_filename = $TaId.'-MakalahSemju.'.$extention; // Change this to your desired new name
            $new_path = './assets/uploads/semju_ta/' . $new_filename;

            if (rename($source_path, $new_path)) {
                if ($TA['MakalahSemju'] != "" && $TA['MakalahSemju'] != $new_filename && $TA['MakalahSemju'] != null){
                    unlink('./assets/uploads/semju_ta/'. $TA['MakalahSemju']);
                }

                $datetime = new DateTime;
                $timezone = new DateTimeZone('Asia/Jakarta'); 
        
                $datetime->setTimezone($timezone);

                $requestUploadMakalahSemju = [
                    'UploadMakalah' => $datetime->format("Y-m-d H:i:s"),
                    'MakalahSemju' => $new_filename
                ];

                $this->db->update('SeminarKemajuanTA', $requestUploadMakalahSemju, "TaId = $TaId");

                redirect('TugasAkhir/SeminarKemajuan');
            }

        } else {
            // File upload failed
            $response = 'File upload failed: ' . $this->upload->display_errors();
        }
    }

    public function DetailSemju(){
        $TaId = $this->input->post('TaId');
        /* Ambil Detail Semju */
        $queryDetailSemju = "
                            select 
                            usr.Name, usr.Username, 
                            ta.BidangMinat, ta.JudulIdn, ta.JudulEng, ta.Id as TaId, semju.Dosen1Id, semju.Dosen2Id, semju.Dosen3Id, ta.JenisPenilaian,
                            (CASE WHEN semju.Dosen1Id IS NOT NULL OR semju.Dosen1Id != 0
                                THEN (SELECT Dosen1.Name
                                        FROM Users Dosen1
                                        WHERE Id = semju.Dosen1Id)
                                ELSE NULL END) AS Dosen1Nama,
                            (CASE WHEN semju.Dosen2Id IS NOT NULL OR semju.Dosen2Id != 0
                                THEN (SELECT Dosen2.Name
                                        FROM Users Dosen2
                                        WHERE Id = semju.Dosen2Id)
                                ELSE NULL END) AS Dosen2Nama,
                            (CASE WHEN semju.Dosen3Id IS NOT NULL OR semju.Dosen3Id != 0
                                THEN (SELECT Dosen3.Name
                                        FROM Users Dosen3
                                        WHERE Id = semju.Dosen3Id)
                                ELSE NULL END) AS Dosen3Nama,
                            semju.TanggalUjian, semju.JamMulai, semju.JamSelesai, semju.RuangId, semju.LinkZoom,
                            (CASE WHEN semju.RuangId IS NOT NULL
                            THEN (SELECT Nama
                                    FROM AkademikRuangan
                                    WHERE Id = semju.RuangId)
                            ELSE NULL END) AS RuangNama,
                            (
                                select MaxDateSemju 
                                from AkademikSkripsi 
                                limit 1) as MaxDateSemju
                            from Users usr
                            join TugasAkhirs ta on usr.Id = ta.MhsId
                            join SeminarKemajuanTA semju on ta.Id = semju.TaId
                            where ta.Id = $TaId
                            ";

        $DetailSemju = $this->db->query($queryDetailSemju)->row_array();

        switch ($DetailSemju['BidangMinat']){
            case 0:
                $DetailSemju['Bidang'] = "Belum Ada";
                break;
            case 1:
                $DetailSemju['Bidang'] = "Biokimia";
                break;
            case 2:
                $DetailSemju['Bidang'] = "Kimia Analitik";
                break;
            case 3:
                $DetailSemju['Bidang'] = "Kimia Anorganik";
                break;
            case 4:
                $DetailSemju['Bidang'] = "Kimia Fisik";
                break;
            case 5:
                $DetailSemju['Bidang'] = "Kimia Organik";
                break;

            default:
                $DetailSemju['Bidang'] = "-";
        }

        $DetailSemju['Waktu'] = $DetailSemju['JamMulai'] . " - " . $DetailSemju['JamSelesai'];

        echo json_encode($DetailSemju);
    }

    public function EditSemju(){

        $TaId = htmlspecialchars($this->input->post('TaId'));

        $RuangId = htmlspecialchars($this->input->post('ruangId'));
        $LinkZoom = htmlspecialchars($this->input->post('linkZoom'));
        
        $requestSemju = [
            "TanggalUjian" => htmlspecialchars($this->input->post('tanggalUjian')),
            "JamMulai" => htmlspecialchars($this->input->post('jamMulai')),
            "JamSelesai" => htmlspecialchars($this->input->post('jamSelesai')),
            "RuangId" => $RuangId,
            "LinkZoom" => $LinkZoom,
            "Dosen1Id" => htmlspecialchars($this->input->post("dosen1")),
            "Dosen2Id" => htmlspecialchars($this->input->post("dosen2")),
            "Dosen3Id" => htmlspecialchars($this->input->post("dosen3"))
        ];

        $this->db->update('SeminarKemajuanTA', $requestSemju, "TaId = $TaId");

        $requestTA = [
            "JenisPenilaian" => htmlspecialchars($this->input->post('jenisPenilaianSemju')),
        ];

        $this->db->update('TugasAkhirs', $requestTA, "Id = $TaId");

        redirect('TugasAkhir/SeminarKemajuan');
    }

    public function NilaiSemjuTA($TaId = null){
        $data['title'] = 'Nilai Seminar Kemajuan';

        $Userid = $this->session->userdata['Id'];

        $roleId = $this->session->userdata['RoleId'];
        $mhsRole = [2,3,4];
        $dsnRole = [1,6];

        /* Cek Dosen */
        $queryTa = "
                    select ta.JenisPenilaian,
                    mhs.Name as NamaMahasiswa,
                    semju.Dosen1Id, semju.Dosen2Id, semju.Dosen3Id,
                    (CASE WHEN semju.Dosen1Id IS NOT NULL OR semju.Dosen1Id != 0
                        THEN (SELECT Dosen1.Name
                                FROM Users Dosen1
                                WHERE Id = semju.Dosen1Id)
                        ELSE '-' END) AS Dosen1Name,
                    (CASE WHEN semju.Dosen2Id IS NOT NULL OR semju.Dosen2Id != 0
                        THEN (SELECT Dosen2.Name
                                FROM Users Dosen2
                                WHERE Id = semju.Dosen2Id)
                        ELSE '-' END) AS Dosen2Name,
                    (CASE WHEN semju.Dosen3Id IS NOT NULL OR semju.Dosen3Id != 0
                        THEN (SELECT Dosen3.Name
                                FROM Users Dosen3
                                WHERE Id = semju.Dosen3Id)
                        ELSE '-' END) AS Dosen3Name
                    FROM SeminarKemajuanTA semju 
                    JOIN TugasAkhirs ta ON semju.TaId = ta.Id 
                    JOIN Users mhs ON ta.MhsId = mhs.Id
                    where ta.Id = $TaId
                    ";

        $TA = $this->db->query($queryTa)->row_array();

        if (in_array($roleId, $mhsRole)){
            $query = "
                        select mhs.*, 
                        usr.Id, usr.Name, usr.Username, usr.Telpon, usr.Email, usr.RoleId, usr.CreatedAt, usr.Status, usr.ImageProfile, usr.AlamatMalang, usr.Alamat
                        from Users usr
                        join UserMahasiswa mhs on usr.Id = mhs.UserId
                        where usr.Id = '$Userid' 
                        ";

        }else if(in_array($roleId, $dsnRole)){
            $query = "
                        select dsn.*, 
                        usr.Id, usr.Name, usr.Username, usr.Telpon, usr.Email, usr.RoleId, usr.CreatedAt, usr.Status, usr.ImageProfile, usr.AlamatMalang, usr.Alamat
                        from Users usr
                        join UserTenagaKependidikan dsn on usr.Id = dsn.UserId
                        where usr.Id = '$Userid'
                        ";

        }else{
            $query = "select * from Users where Id = '$Userid'";
        }

        /* Perhitungan Nilai Seminar Kemajuan */
        /* Ambil Nilai Semju */
        $queryNilaiSemjuTA = "
                            select nilai.*,
                            semju.Dosen1Id, semju.Dosen2Id, semju.Dosen3Id,
                            (CASE WHEN semju.Dosen1Id IS NOT NULL
                            THEN (SELECT Dosen.Name
                                    FROM Users Dosen
                                    WHERE Id = semju.Dosen1Id)
                            ELSE NULL END) AS Dosen1Name,
                            (CASE WHEN semju.Dosen2Id IS NOT NULL
                            THEN (SELECT Dosen.Name
                                    FROM Users Dosen
                                    WHERE Id = semju.Dosen2Id)
                            ELSE NULL END) AS Dosen2Name,
                            (CASE WHEN semju.Dosen3Id IS NOT NULL
                            THEN (SELECT Dosen.Name
                                    FROM Users Dosen
                                    WHERE Id = semju.Dosen3Id)
                            ELSE NULL END) AS Dosen3Name
                            from NilaiSeminarKemajuanTA nilai
                            join SeminarKemajuanTA semju on nilai.TaId = semju.TaId
                            where nilai.TaId = $TaId
                            ";
        
        $NilaiSemjuTA = $this->db->query($queryNilaiSemjuTA)->row_array();

        if ($NilaiSemjuTA != null){
            /* Dosen 1 */
            if ($NilaiSemjuTA['Dsn1Pres1'] != null){
                $totalNilaiPresentasiDsn1 = ($NilaiSemjuTA['Dsn1Pres1'] * 2.5) + ($NilaiSemjuTA['Dsn1Pres2'] * 2.5) + ($NilaiSemjuTA['Dsn1Pres3'] * 2.5) + ($NilaiSemjuTA['Dsn1Pres4'] * 2.5);
            }else{
                $totalNilaiPresentasiDsn1 = 0;
            }
            if ($NilaiSemjuTA['Dsn1Ber1'] != null){
                $totalNilaiBerkasDsn1 = ($NilaiSemjuTA['Dsn1Ber1'] * 2.5) + ($NilaiSemjuTA['Dsn1Ber2'] * 2.5) + ($NilaiSemjuTA['Dsn1Ber3'] * 2.5) + ($NilaiSemjuTA['Dsn1Ber4'] * 2.5);
            }else{
                $totalNilaiBerkasDsn1 = 0;
            }
            $totalNilaiSemjuDsn1TA = ($totalNilaiPresentasiDsn1 + $totalNilaiBerkasDsn1)/100;

            /* Dosen 2 */
            if ($NilaiSemjuTA['Dsn2Pres1'] != null){
                $totalNilaiPresentasiDsn2 = ($NilaiSemjuTA['Dsn2Pres1'] * 2.5) + ($NilaiSemjuTA['Dsn2Pres2'] * 2.5) + ($NilaiSemjuTA['Dsn2Pres3'] * 2.5) + ($NilaiSemjuTA['Dsn2Pres4'] * 2.5);
            }else{
                $totalNilaiPresentasiDsn2 = 0;
            }
            if ($NilaiSemjuTA['Dsn2Ber1'] != null){
                $totalNilaiBerkasDsn2 = ($NilaiSemjuTA['Dsn2Ber1'] * 2.5) + ($NilaiSemjuTA['Dsn2Ber2'] * 2.5) + ($NilaiSemjuTA['Dsn2Ber3'] * 2.5) + ($NilaiSemjuTA['Dsn2Ber4'] * 2.5);
            }else{
                $totalNilaiBerkasDsn2 = 0;
            }
            $totalNilaiSemjuDsn2TA = ($totalNilaiPresentasiDsn2 + $totalNilaiBerkasDsn2)/100;

            /* Dosen 3 */
            if ($NilaiSemjuTA['Dsn3Pres1'] != null){
                $totalNilaiPresentasiDsn3 = ($NilaiSemjuTA['Dsn3Pres1'] * 2.5) + ($NilaiSemjuTA['Dsn3Pres2'] * 2.5) + ($NilaiSemjuTA['Dsn3Pres3'] * 2.5) + ($NilaiSemjuTA['Dsn3Pres4'] * 2.5);
            }else{
                $totalNilaiPresentasiDsn3 = 0;
            }
            if ($NilaiSemjuTA['Dsn3Ber1'] != null){
                $totalNilaiBerkasDsn3 = ($NilaiSemjuTA['Dsn3Ber1'] * 2.5) + ($NilaiSemjuTA['Dsn3Ber2'] * 2.5) + ($NilaiSemjuTA['Dsn3Ber3'] * 2.5) + ($NilaiSemjuTA['Dsn3Ber4'] * 2.5);
            }else{
                $totalNilaiBerkasDsn3 = 0;
            }
            $totalNilaiSemjuDsn3TA = ($totalNilaiPresentasiDsn3 + $totalNilaiBerkasDsn3)/100;

            /* Perhitungan Nilai Rata-rata */
            $averagePresentasi = ($totalNilaiPresentasiDsn1 + $totalNilaiPresentasiDsn2 + $totalNilaiPresentasiDsn3)/3;
            $averageBerkas = ($totalNilaiBerkasDsn1 + $totalNilaiBerkasDsn2 + $totalNilaiBerkasDsn3)/3;
            // $averageTotalNilai = ($totalNilaiSemproDsn1TA + $totalNilaiSemproDsn2TA + $totalNilaiSemproDsn1TA)/3;

            $data['averagePresentasi'] = number_format($averagePresentasi,2);
            $data['averageBerkas'] = number_format($averageBerkas,2);
            // $data['averageTotalNilai'] = number_format($averageTotalNilai,2);

            /* Saran */
            $data['Dsn1Saran'] = $NilaiSemjuTA['Dsn1Saran'];
            $data['Dsn2Saran'] = $NilaiSemjuTA['Dsn2Saran'];
            $data['Dsn3Saran'] = $NilaiSemjuTA['Dsn3Saran'];

            if($TA['JenisPenilaian'] == 1){
                $NilaiAkhirSemjuTA = ((($totalNilaiPresentasiDsn1 + $totalNilaiPresentasiDsn2) * 0.35) + ($totalNilaiPresentasiDsn3 * 0.3) + (($totalNilaiBerkasDsn1 + $totalNilaiBerkasDsn2) * 0.4) + ($totalNilaiBerkasDsn3 * 0.2)) / 100;
            }else{
                $NilaiAkhirSemjuTA = (($totalNilaiPresentasiDsn1 * 0.5) + (($totalNilaiPresentasiDsn2 + $totalNilaiPresentasiDsn3) * 0.25) + ($totalNilaiBerkasDsn1 * 0.6) + (($totalNilaiBerkasDsn2 + $totalNilaiBerkasDsn3) * 0.2)) / 100;
            }
            $NilaiAkhirSemjuTA = number_format($NilaiAkhirSemjuTA, 2);
            $data['NilaiAkhirSemjuTA'] = $NilaiAkhirSemjuTA;

            if ($NilaiSemjuTA['NilaiAkhir'] == null || $NilaiSemjuTA['NilaiAkhir'] != $NilaiAkhirSemjuTA){
                $requestNilaiAkhir = [
                    'Dsn1TotalPres' => $totalNilaiPresentasiDsn1,
                    'Dsn2TotalPres' => $totalNilaiPresentasiDsn2,
                    'Dsn3TotalPres' => $totalNilaiPresentasiDsn3,
                    'Dsn1TotalBer' => $totalNilaiBerkasDsn1,
                    'Dsn2TotalBer' => $totalNilaiBerkasDsn2,
                    'Dsn3TotalBer' => $totalNilaiBerkasDsn3,
                    'NilaiAkhir' => $NilaiAkhirSemjuTA
                ];
                $this->db->update('NilaiSeminarKemajuanTA', $requestNilaiAkhir, "TaId = $TaId");
            }
        }else{
            $totalNilaiPresentasiDsn1 = 0;
            $totalNilaiBerkasDsn1 = 0;
            $totalNilaiSemjuDsn1TA = 0;

            $totalNilaiPresentasiDsn2 = 0;
            $totalNilaiBerkasDsn2 = 0;
            $totalNilaiSemjuDsn2TA = 0;

            $totalNilaiPresentasiDsn3 = 0;
            $totalNilaiBerkasDsn3 = 0;
            $totalNilaiSemjuDsn3TA = 0;

            $data['NilaiAkhirSemjuTA'] = 0;
            $data['averagePresentasi'] = 0;
            $data['averageBerkas'] = 0;

            $data['Dsn1Saran'] = "-";
            $data['Dsn2Saran'] = "-";
            $data['Dsn3Saran'] = "-";
        }

        if ($TA['Dosen1Id'] == $Userid){
            $data['TipeDosen'] = 1;
            $data['totalNilaiPresentasiDsn1'] = number_format(($totalNilaiPresentasiDsn1 / 10),2);
            $data['totalNilaiBerkasDsn1'] = number_format(($totalNilaiBerkasDsn1 / 10),2);
            $data['totalNilaiSemjuDsn1TA'] = number_format($totalNilaiSemjuDsn1TA,2);
        }else if ($TA['Dosen2Id'] == $Userid){
            $data['TipeDosen'] = 2;
            $data['totalNilaiPresentasiDsn2'] = number_format(($totalNilaiPresentasiDsn2 / 10),2);
            $data['totalNilaiBerkasDsn2'] = number_format(($totalNilaiBerkasDsn2 / 10),2);
            $data['totalNilaiSemjuDsn2TA'] = number_format($totalNilaiSemjuDsn2TA,2);
        }else if ($TA['Dosen3Id'] == $Userid){
            $data['TipeDosen'] = 3;
            $data['totalNilaiPresentasiDsn3'] = number_format(($totalNilaiPresentasiDsn3 / 10),2);
            $data['totalNilaiBerkasDsn3'] = number_format(($totalNilaiBerkasDsn3 / 10),2);
            $data['totalNilaiSemjuDsn3TA'] = number_format($totalNilaiSemjuDsn3TA,2);
        }else{
            $data['TipeDosen'] = 0;
            $data['totalNilaiPresentasiDsn1'] = number_format(($totalNilaiPresentasiDsn1 / 10),2);
            $data['totalNilaiBerkasDsn1'] = number_format(($totalNilaiBerkasDsn1 / 10),2);
            $data['totalNilaiSemjuDsn1TA'] = number_format($totalNilaiSemjuDsn1TA,2);

            $data['totalNilaiPresentasiDsn2'] = number_format(($totalNilaiPresentasiDsn2 / 10),2);
            $data['totalNilaiBerkasDsn2'] = number_format(($totalNilaiBerkasDsn2 / 10),2);
            $data['totalNilaiSemjuDsn2TA'] = number_format($totalNilaiSemjuDsn2TA,2);

            $data['totalNilaiPresentasiDsn3'] = number_format(($totalNilaiPresentasiDsn3 / 10),2);
            $data['totalNilaiBerkasDsn3'] = number_format(($totalNilaiBerkasDsn3 / 10),2);
            $data['totalNilaiSemjuDsn3TA'] = number_format($totalNilaiSemjuDsn3TA,2);
        }

        $User = $this->db->query($query)->row_array();
        $data['User'] = $User;
        $data['TaId'] = $TaId;
        $data['DosenId'] = $TA;
        $data['Nilai'] = $NilaiSemjuTA;

        /* Setting kembali */
        $data['url_kembali'] = get_referer_url();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('tugasAkhir/NilaiSeminarKemajuan', $data);
        $this->load->view('templates/footer');
    }

    public function DetailNilaiSemju(){
        $TaId = $this->input->post('TaId');

        $queryNilaiSemju = "
                            select * from NilaiSeminarKemajuanTA where TaId = $TaId
                            ";

        $NilaiSemju = $this->db->query($queryNilaiSemju)->row_array();

        echo json_encode($NilaiSemju);
    }

    public function SubmitNilaiSemju($id = null){

        $Userid = $this->session->userdata['Id'];

        /* Cek Dosen */
        $queryDsn = "
                    select Dosen1Id, Dosen2Id, Dosen3Id
                    from SeminarKemajuanTA
                    where TaId = $id
                    ";
        
        $JenisDosen = $this->db->query($queryDsn)->row_array();

        /* Cek Data Nilai */
        $queryNilaiSemju = "
                            select * from NilaiSeminarKemajuanTA
                            where TaId = $id
                            ";
        
        $NilaiSemjuTA = $this->db->query($queryNilaiSemju)->row_array();

        $UpdateNilaiTA = false;
        if ($NilaiSemjuTA != null){
            $UpdateNilaiTA = true;
        }

        if ($JenisDosen['Dosen1Id'] == $Userid){
            $requestNilaiSemjuTA = [
                "Dsn1Pres1" => htmlspecialchars($this->input->post('tataBahasa_9C')),
                "Dsn1Pres2" => htmlspecialchars($this->input->post('kosaKata_9C')),
                "Dsn1Pres3" => htmlspecialchars($this->input->post('pengucapan_9C')),
                "Dsn1Pres4" => htmlspecialchars($this->input->post('penyampaian_9B')),

                "Dsn1Ber1" => htmlspecialchars($this->input->post('langkahLangkah_8A')),
                "Dsn1Ber2" => htmlspecialchars($this->input->post('data_8A')),
                "Dsn1Ber3" => htmlspecialchars($this->input->post('mengolahData_8B')),
                "Dsn1Ber4" => htmlspecialchars($this->input->post('kesimpulan_8C')),
            ];
            if (!$UpdateNilaiTA){
                /* Insert Nilai Semju Dosen 1 */
                $requestNilaiSemjuTA['TaId'] = $id;
                $this->db->insert('NilaiSeminarKemajuanTA', $requestNilaiSemjuTA);
            }else{
                /* Update Nilai Semju Dosen 1 */
                $this->db->update('NilaiSeminarKemajuanTA', $requestNilaiSemjuTA, ['TaId'=> $id]);
            }
        }else if($JenisDosen['Dosen2Id'] == $Userid){
            $requestNilaiSemjuTA = [
                "Dsn2Pres1" => htmlspecialchars($this->input->post('tataBahasa_9C')),
                "Dsn2Pres2" => htmlspecialchars($this->input->post('kosaKata_9C')),
                "Dsn2Pres3" => htmlspecialchars($this->input->post('pengucapan_9C')),
                "Dsn2Pres4" => htmlspecialchars($this->input->post('penyampaian_9B')),

                "Dsn2Ber1" => htmlspecialchars($this->input->post('langkahLangkah_8A')),
                "Dsn2Ber2" => htmlspecialchars($this->input->post('data_8A')),
                "Dsn2Ber3" => htmlspecialchars($this->input->post('mengolahData_8B')),
                "Dsn2Ber4" => htmlspecialchars($this->input->post('kesimpulan_8C')),
            ];
            if (!$UpdateNilaiTA){
                /* Insert Nilai Semju Dosen 2 */
                $requestNilaiSemjuTA['TaId'] = $id;
                $this->db->insert('NilaiSeminarKemajuanTA', $requestNilaiSemjuTA);
            }else{
                /* Update Nilai Semju Dosen 2 */
                $this->db->update('NilaiSeminarKemajuanTA', $requestNilaiSemjuTA, ['TaId'=> $id]);
            }
        }else if($JenisDosen['Dosen3Id'] == $Userid){
            $requestNilaiSemjuTA = [
                "Dsn3Pres1" => htmlspecialchars($this->input->post('tataBahasa_9C')),
                "Dsn3Pres2" => htmlspecialchars($this->input->post('kosaKata_9C')),
                "Dsn3Pres3" => htmlspecialchars($this->input->post('pengucapan_9C')),
                "Dsn3Pres4" => htmlspecialchars($this->input->post('penyampaian_9B')),

                "Dsn3Ber1" => htmlspecialchars($this->input->post('langkahLangkah_8A')),
                "Dsn3Ber2" => htmlspecialchars($this->input->post('data_8A')),
                "Dsn3Ber3" => htmlspecialchars($this->input->post('mengolahData_8B')),
                "Dsn3Ber4" => htmlspecialchars($this->input->post('kesimpulan_8C')),
            ];
            if (!$UpdateNilaiTA){
                /* Insert Nilai Semju Dosen 3 */
                $requestNilaiSemjuTA['TaId'] = $id;
                $this->db->insert('NilaiSeminarKemajuanTA', $requestNilaiSemjuTA);
            }else{
                /* Update Nilai Semju Dosen 3 */
                $this->db->update('NilaiSeminarKemajuanTA', $requestNilaiSemjuTA, ['TaId'=> $id]);
            }
        }else if(htmlspecialchars($this->input->post('tipeDosen')) != null){
            switch ($this->input->post('tipeDosen')){
                case 1:
                    $requestNilaiSemjuTA = [
                        "Dsn1Pres1" => htmlspecialchars($this->input->post('tataBahasa_9C')),
                        "Dsn1Pres2" => htmlspecialchars($this->input->post('kosaKata_9C')),
                        "Dsn1Pres3" => htmlspecialchars($this->input->post('pengucapan_9C')),
                        "Dsn1Pres4" => htmlspecialchars($this->input->post('penyampaian_9B')),

                        "Dsn1Ber1" => htmlspecialchars($this->input->post('langkahLangkah_8A')),
                        "Dsn1Ber2" => htmlspecialchars($this->input->post('data_8A')),
                        "Dsn1Ber3" => htmlspecialchars($this->input->post('mengolahData_8B')),
                        "Dsn1Ber4" => htmlspecialchars($this->input->post('kesimpulan_8C')),
                    ];
                    break;
                case 2:
                    $requestNilaiSemjuTA = [
                        "Dsn2Pres1" => htmlspecialchars($this->input->post('tataBahasa_9C')),
                        "Dsn2Pres2" => htmlspecialchars($this->input->post('kosaKata_9C')),
                        "Dsn2Pres3" => htmlspecialchars($this->input->post('pengucapan_9C')),
                        "Dsn2Pres4" => htmlspecialchars($this->input->post('penyampaian_9B')),
        
                        "Dsn2Ber1" => htmlspecialchars($this->input->post('langkahLangkah_8A')),
                        "Dsn2Ber2" => htmlspecialchars($this->input->post('data_8A')),
                        "Dsn2Ber3" => htmlspecialchars($this->input->post('mengolahData_8B')),
                        "Dsn2Ber4" => htmlspecialchars($this->input->post('kesimpulan_8C')),
                    ];
                    break;
                case 3:
                    $requestNilaiSemjuTA = [
                        "Dsn3Pres1" => htmlspecialchars($this->input->post('tataBahasa_9C')),
                        "Dsn3Pres2" => htmlspecialchars($this->input->post('kosaKata_9C')),
                        "Dsn3Pres3" => htmlspecialchars($this->input->post('pengucapan_9C')),
                        "Dsn3Pres4" => htmlspecialchars($this->input->post('penyampaian_9B')),
        
                        "Dsn3Ber1" => htmlspecialchars($this->input->post('langkahLangkah_8A')),
                        "Dsn3Ber2" => htmlspecialchars($this->input->post('data_8A')),
                        "Dsn3Ber3" => htmlspecialchars($this->input->post('mengolahData_8B')),
                        "Dsn3Ber4" => htmlspecialchars($this->input->post('kesimpulan_8C')),
                    ];
                    break;
            }

            if (!$UpdateNilaiTA){
                /* Insert Nilai Semju Dosen 1 */
                $requestNilaiSemjuTA['TaId'] = $id;
                $this->db->insert('NilaiSeminarKemajuanTA', $requestNilaiSemjuTA);
            }else{
                /* Update Nilai Semju Dosen 1 */
                $this->db->update('NilaiSeminarKemajuanTA', $requestNilaiSemjuTA, ['TaId'=> $id]);
            }
        }

        redirect('TugasAkhir/NilaiSemjuTA/'. $id);
    }

    public function SubmitSaranSemju($id = null){

        $Userid = $this->session->userdata['Id'];

        /* Cek Dosen */
        $queryDsn = "
                    select Dosen1Id, Dosen2Id, Dosen3Id
                    from SeminarKemajuanTA
                    where TaId = $id
                    ";
        
        $JenisDosen = $this->db->query($queryDsn)->row_array();

        /* Cek Data Saran */
        $querySaranSemju = "
                            select * from NilaiSeminarKemajuanTA
                            where TaId = $id
                            ";
        
        $SaranSemjuTA = $this->db->query($querySaranSemju)->row_array();

        $UpdateSaranTA = false;
        if ($SaranSemjuTA != null){
            $UpdateSaranTA = true;
        }

        if ($JenisDosen['Dosen1Id'] == $Userid){
            $requestSaranSemjuTA = [
                "Dsn1Saran" => $this->input->post('saran'),
            ];

            if (!$UpdateSaranTA){
                /* Insert Saran Semju Dosen 1 */
                $requestSaranSemjuTA['TaId'] = $id;
                $this->db->insert('NilaiSeminarKemajuanTA', $requestSaranSemjuTA);
            }else{
                /* Update Saran Semju Dosen 1 */
                $this->db->update('NilaiSeminarKemajuanTA', $requestSaranSemjuTA, ['TaId'=> $id]);
            }
        }else if($JenisDosen['Dosen2Id'] == $Userid){
            $requestSaranSemjuTA = [
                "Dsn2Saran" => $this->input->post('saran'),
            ];

            if (!$UpdateSaranTA){
                /* Insert Saran Semju Dosen 2 */
                $requestSaranSemjuTA['TaId'] = $id;
                $this->db->insert('NilaiSeminarKemajuanTA', $requestSaranSemjuTA);
            }else{
                /* Update Saran Semju Dosen 2 */
                $this->db->update('NilaiSeminarKemajuanTA', $requestSaranSemjuTA, ['TaId'=> $id]);
            }
        }else if($JenisDosen['Dosen3Id'] == $Userid){
            $requestSaranSemjuTA = [
                "Dsn3Saran" => $this->input->post('saran'),
            ];

            if (!$UpdateSaranTA){
                /* Insert Saran Semju Dosen 3 */
                $requestSaranSemjuTA['TaId'] = $id;
                $this->db->insert('NilaiSeminarKemajuanTA', $requestSaranSemjuTA);
            }else{
                /* Update Saran Semju Dosen 3 */
                $this->db->update('NilaiSeminarKemajuanTA', $requestSaranSemjuTA, ['TaId'=> $id]);
            }
        }

        redirect('TugasAkhir/NilaiSemjuTA/'. $id);
    }

    /* Ujian Tugas Akhir (Skripsi) */
    public function SeminarSkripsi($id = null){
        $data['title'] = 'Seminar Skripsi';
        if ($id == null){
            $id = $this->session->userdata['Id'];
        }

        $roleId = $this->session->userdata['RoleId'];

        $kadep = $this->session->userdata['Kadep'];
        $sekdep = $this->session->userdata['Sekdep'];
        $kaprodiS1 = $this->session->userdata['KaprodiS1'];
        $kaprodiS2 = $this->session->userdata['KaprodiS2'];
        $kaprodiS3 = $this->session->userdata['KaprodiS3'];

        $querySemesterId = "select Id from AkademikSemester order by Id Desc limit 1";
        $LastSemesterId = $this->db->query($querySemesterId)->row_array();

        $mhsRole = [2,3,4];
        $dsnRole = [1];
        if (in_array($roleId, $mhsRole)){
            $query = "
                        select mhs.*, 
                        usr.Id, usr.Name, usr.Username, usr.Telpon, usr.Email, usr.RoleId, usr.CreatedAt, usr.Status, usr.ImageProfile, usr.AlamatMalang, usr.Alamat
                        from Users usr
                        join UserMahasiswa mhs on usr.Id = mhs.UserId
                        where usr.Id = '$id' 
                        ";

            /* Ambil Data Ta berdasarkan Mahasiswa */
            $queryTa = "
                        select ta.*, 
                        usr.Name, usr.Username
                        from TugasAkhirs ta
                        join Users usr on usr.Id = ta.MhsId
                        where ta.MhsId = $id and ta.Status = 1
                        Order By ta.Id Desc
                        ";
            $TA = $this->db->query($queryTa)->result_array();

            if ($TA != null){
                /* Menggabungkan TaId */
                $TaIds = [];
                foreach ($TA as $item) {
                    $TaIds[] = $item["Id"];
                }
                $values = array_map('strval', $TaIds);
                $encodedString = "(" . implode(",", $values) . ")";

                /* Ambil Data Skripsi */
                $querySkripsi = "
                                select skripsi.TaId, skripsi.TanggalUjian, skripsi.JamMulai, skripsi.JamSelesai, skripsi.RuangId, skripsi.LinkZoom, skripsi.MakalahSkripsi,
                                (CASE WHEN skripsi.RuangId IS NOT NULL
                                THEN (SELECT Nama
                                        FROM AkademikRuangan
                                        WHERE Id = skripsi.RuangId)
                                ELSE NULL END) AS RuangNama,
                                (CASE WHEN skripsi.Dosen1Id IS NOT NULL
                                    THEN (SELECT Dosen1.Name
                                            FROM Users Dosen1
                                            WHERE Id = skripsi.Dosen1Id)
                                    ELSE '-' END) AS Dosen1Name,
                                (CASE WHEN skripsi.Dosen2Id IS NOT NULL
                                    THEN (SELECT Dosen2.Name
                                            FROM Users Dosen2
                                            WHERE Id = skripsi.Dosen2Id)
                                    ELSE '-' END) AS Dosen2Name,
                                (CASE WHEN skripsi.Dosen3Id IS NOT NULL
                                    THEN (SELECT Dosen3.Name
                                            FROM Users Dosen3
                                            WHERE Id = skripsi.Dosen3Id)
                                    ELSE '-' END) AS Dosen3Name
                                from SeminarSkripsiTA skripsi
                                where TaId in $encodedString
                                ";

                $Skripsi = $this->db->query($querySkripsi)->result_array();
                if (count($Skripsi) > 0) {
                    /* Menggabungkan Data Skripsi Dengan Data TA */
                    foreach ($TA as $key => $dataTA) {
                        foreach ($Skripsi as $dataSkripsi) {
                            if ($dataTA['Id'] == $dataSkripsi['TaId']) {
                                $TA[$key] = array_merge($dataTA, $dataSkripsi);
                                break;
                            }
                        }
                    }
                }
            }

        }else if(in_array($roleId, $dsnRole) && ($kadep == false && $sekdep == false && $kaprodiS1 == false && $kaprodiS2 == false && $kaprodiS3 == false)){
            $query = "
                        select dsn.*, 
                        usr.Id, usr.Name, usr.Username, usr.Telpon, usr.Email, usr.RoleId, usr.CreatedAt, usr.Status, usr.ImageProfile, usr.AlamatMalang, usr.Alamat
                        from Users usr
                        join UserTenagaKependidikan dsn on usr.Id = dsn.UserId
                        where usr.Id = '$id'
                        ";

            /* Ambil Data Ta berdasarkan Mahasiswa */
            $queryTa = "
                        select ta.*, 
                        usr.Name, usr.Username,
                        (CASE WHEN skripsi.Dosen1Id IS NOT NULL
                            THEN (SELECT Dosen1.Name
                                    FROM Users Dosen1
                                    WHERE Id = skripsi.Dosen1Id)
                            ELSE NULL END) AS Dosen1Name,
                        (CASE WHEN skripsi.Dosen2Id IS NOT NULL
                            THEN (SELECT Dosen2.Name
                                    FROM Users Dosen2
                                    WHERE Id = skripsi.Dosen2Id)
                            ELSE NULL END) AS Dosen2Name,
                        (CASE WHEN skripsi.Dosen3Id IS NOT NULL
                            THEN (SELECT Dosen3.Name
                                    FROM Users Dosen3
                                    WHERE Id = skripsi.Dosen3Id)
                            ELSE NULL END) AS Dosen3Name
                        from TugasAkhirs ta
                        join Users usr on usr.Id = ta.MhsId
                        join SeminarSkripsiTA skripsi on ta.Id = skripsi.TaId
                        where (skripsi.Dosen1Id = $id or skripsi.Dosen2Id = $id or skripsi.Dosen3Id = $id) and ta.Status = 1
                        ";

            if($this->input->post('jenjang') != NULL){
                $jenjang = $this->input->post('jenjang');
                $queryTa = $queryTa . " AND usr.RoleId = '$jenjang'";
            }else{
                $jenjang = 2;
                $queryTa = $queryTa . " AND usr.RoleId = '$jenjang'";
            }

            if($this->input->post('semester') != NULL){
                $semesterId = $this->input->post('semester');
                $queryTa = $queryTa . " AND semesterId = $semesterId"; 
            }else{
                $semesterId = $LastSemesterId['Id'] - 1;
                $queryTa = $queryTa . " AND semesterId = $semesterId"; 
            }

            if($this->input->post('search') != NULL){
                $search = $this->input->post('search');
                $queryTa = $queryTa . " AND usr.Name LIKE '%$search%' OR usr.Username LIKE '%$search%'"; 
            }

            $queryTa = $queryTa . " Order By ta.Id Desc";

            if($this->input->post('limit') != NULL){
                $limit = $this->input->post('limit');
                $queryTa = $queryTa . " limit $limit"; 
            }

            $TA = $this->db->query($queryTa)->result_array();

            if ($TA != null){
                /* Menggabungkan TaId */
                $TaIds = [];
                foreach ($TA as $item) {
                    $TaIds[] = $item["Id"];
                }
                $values = array_map('strval', $TaIds);
                $encodedString = "(" . implode(",", $values) . ")";

                /* Ambil Data Skripsi */
                $querySkripsi = "
                                select skripsi.TaId, skripsi.TanggalUjian, skripsi.JamMulai, skripsi.JamSelesai, skripsi.RuangId, skripsi.LinkZoom, skripsi.MakalahSkripsi,
                                (CASE WHEN skripsi.RuangId IS NOT NULL
                                THEN (SELECT Nama
                                        FROM AkademikRuangan
                                        WHERE Id = skripsi.RuangId)
                                ELSE NULL END) AS RuangNama,
                                (CASE WHEN skripsi.Dosen1Id IS NOT NULL
                                    THEN (SELECT Dosen1.Name
                                            FROM Users Dosen1
                                            WHERE Id = skripsi.Dosen1Id)
                                    ELSE NULL END) AS Dosen1Name,
                                (CASE WHEN skripsi.Dosen2Id IS NOT NULL
                                    THEN (SELECT Dosen2.Name
                                            FROM Users Dosen2
                                            WHERE Id = skripsi.Dosen2Id)
                                    ELSE NULL END) AS Dosen2Name,
                                (CASE WHEN skripsi.Dosen3Id IS NOT NULL
                                    THEN (SELECT Dosen3.Name
                                            FROM Users Dosen3
                                            WHERE Id = skripsi.Dosen3Id)
                                    ELSE NULL END) AS Dosen3Name
                                from SeminarSkripsiTA skripsi
                                where TaId in $encodedString
                                ";

                $Skripsi = $this->db->query($querySkripsi)->result_array();
                if (count($Skripsi) > 0) {
                    /* Menggabungkan Data Skripsi Dengan Data TA */
                    foreach ($TA as $key => $dataTA) {
                        foreach ($Skripsi as $dataSkripsi) {
                            if ($dataTA['Id'] == $dataSkripsi['TaId']) {
                                $TA[$key] = array_merge($dataTA, $dataSkripsi);
                                break;
                            }
                        }
                    }
                }
            }
        }else{
            $query = "select * from Users where Id = '$id'";
            
            /* Ambil Data Ta berdasarkan Mahasiswa */
            $queryTa = "
                        select ta.*, 
                        usr.Name, usr.Username
                        from TugasAkhirs ta
                        join Users usr on usr.Id = ta.MhsId
                        where ta.Status = 1
                        ";

            if($this->input->post('jenjang') != NULL){
                $jenjang = $this->input->post('jenjang');
                $queryTa = $queryTa . " AND usr.RoleId = '$jenjang'";
            }else{
                $jenjang = 2;
                $queryTa = $queryTa . " AND usr.RoleId = '$jenjang'";
            }

            if($this->input->post('semester') != NULL){
                $semesterId = $this->input->post('semester');
                $queryTa = $queryTa . " AND semesterId = $semesterId"; 
            }else{
                $semesterId = $LastSemesterId['Id'] - 1;
                $queryTa = $queryTa . " AND semesterId = $semesterId"; 
            }

            if($this->input->post('search') != NULL){
                $search = $this->input->post('search');
                $queryTa = $queryTa . " AND usr.Name LIKE '%$search%' OR usr.Username LIKE '%$search%'"; 
            }

            $queryTa = $queryTa . " Order By ta.Id Desc";
            
            if($this->input->post('limit') != NULL){
                $limit = $this->input->post('limit');
                $queryTa = $queryTa . " limit $limit"; 
            }

            $TA = $this->db->query($queryTa)->result_array();

            if ($TA != null){
                /* Menggabungkan TaId */
                $TaIds = [];
                foreach ($TA as $item) {
                    $TaIds[] = $item["Id"];
                }
                $values = array_map('strval', $TaIds);
                $encodedString = "(" . implode(",", $values) . ")";

                /* Ambil Data Skripsi */
                $querySkripsi = "
                                select skripsi.TaId, skripsi.TanggalUjian, skripsi.JamMulai, skripsi.JamSelesai, skripsi.RuangId, skripsi.LinkZoom, skripsi.MakalahSkripsi,
                                (CASE WHEN skripsi.RuangId IS NOT NULL
                                THEN (SELECT Nama
                                        FROM AkademikRuangan
                                        WHERE Id = skripsi.RuangId)
                                ELSE NULL END) AS RuangNama,
                                (CASE WHEN skripsi.Dosen1Id IS NOT NULL
                                    THEN (SELECT Dosen1.Name
                                            FROM Users Dosen1
                                            WHERE Id = skripsi.Dosen1Id)
                                    ELSE NULL END) AS Dosen1Name,
                                (CASE WHEN skripsi.Dosen2Id IS NOT NULL
                                    THEN (SELECT Dosen2.Name
                                            FROM Users Dosen2
                                            WHERE Id = skripsi.Dosen2Id)
                                    ELSE NULL END) AS Dosen2Name,
                                (CASE WHEN skripsi.Dosen3Id IS NOT NULL
                                    THEN (SELECT Dosen3.Name
                                            FROM Users Dosen3
                                            WHERE Id = skripsi.Dosen3Id)
                                    ELSE NULL END) AS Dosen3Name
                                from SeminarSkripsiTA skripsi
                                where TaId in $encodedString
                                ";

                $Skripsi = $this->db->query($querySkripsi)->result_array();
                if (count($Skripsi) > 0) {
                    /* Menggabungkan Data Skripsi Dengan Data TA */
                    foreach ($TA as $key => $dataTA) {
                        foreach ($Skripsi as $dataSkripsi) {
                            if ($dataTA['Id'] == $dataSkripsi['TaId']) {
                                $TA[$key] = array_merge($dataTA, $dataSkripsi);
                                break;
                            }
                        }
                    }
                }
            }
        }

        foreach ($TA as $key => $ta){
            switch ($ta['BidangMinat']){
                case 0:
                    $TA[$key]['Bidang'] = "Belum Ada";
                    break;
                case 1:
                    $TA[$key]['Bidang'] = "Biokimia";
                    break;
                case 2:
                    $TA[$key]['Bidang'] = "Kimia Analitik";
                    break;
                case 3:
                    $TA[$key]['Bidang'] = "Kimia Anorganik";
                    break;
                case 4:
                    $TA[$key]['Bidang'] = "Kimia Fisik";
                    break;
                case 5:
                    $TA[$key]['Bidang'] = "Kimia Organk";
                    break;

                default:
                    $TA[$key]['Bidang'] = '-';
                    break;
            }
        }

        $User = $this->db->query($query)->row_array();
        $data['User'] = $User;

        /* Ambil Data Tahun Akademik */
        $queryAkademik = "
                        select * from AkademikSemester
                        where Status = 1 
                        Order by id Desc
                        ";

        $ListSemester = $this->db->query($queryAkademik)->result_array();
        $data['Semester'] = $ListSemester;
                   
        /* Ambil List Dosen */
        $queyrDosen = "select *
                        from Users
                        where RoleId in ('1')
                        ";

        $Dosen = $this->db->query($queyrDosen)->result_array();
        $data['Dosen'] = $Dosen;

        /* Ambil List Ruangan */
        $queryRuangan = "select *
                        from AkademikRuangan
                        where Status = 1
                        ";
        $Ruangan = $this->db->query($queryRuangan)->result_array();
        $data['Ruangan'] = $Ruangan;

        $data['SeminarSkripsi'] = $TA;

        /* Setting kembali */
        $data['url_kembali'] = get_referer_url();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('tugasakhir/SeminarSkripsi', $data);
        $this->load->view('templates/footer');
    }

    public function DaftarSkripsi(){
        $TaId = htmlspecialchars($this->input->post("TaId"));
        $datetime = new DateTime();
        $timezone = new DateTimeZone('Asia/Jakarta'); 

        $datetime->setTimezone($timezone);

        $dosen1 = htmlspecialchars($this->input->post("dosen1"));
        $dosen2 = htmlspecialchars($this->input->post("dosen2"));
        $dosen3 = htmlspecialchars($this->input->post("dosen3"));

        $requestSkripsi = [
            "TaId" => $TaId,
            "TanggalUjian" => htmlspecialchars($this->input->post("tanggalUjian")),
            "JamMulai" => htmlspecialchars($this->input->post("jamMulai")),
            "JamSelesai" => htmlspecialchars($this->input->post("jamSelesai")),
            "RuangId" => htmlspecialchars($this->input->post("ruangId")),
            "LinkZoom" => htmlspecialchars($this->input->post("linkZoom")),
            "CreatedAt" => $datetime->format("Y-m-d H:i:s"),
            "Dosen1Id" => $dosen1,
            "Dosen2Id" => $dosen2,
            "Dosen3Id" => $dosen3,
        ];

        $this->db->insert('SeminarSkripsiTA', $requestSkripsi);

        /* Update Tahapan */
        $requestTA = [
            "Dosen1Id" => $dosen1,
            "JenisPenilaian" => htmlspecialchars($this->input->post("jenisPenilaian")),
            "Tahapan" => 4
        ];
        $this->db->update('TugasAkhirs', $requestTA, "Id = $TaId");

        redirect('TugasAkhir/SeminarSkripsi');
    }

    public function UploadMakalahSkripsi(){
        $uploadPath = './assets/uploads/skripsi_ta/';
        $config['upload_path'] = $uploadPath;
        $config['allowed_types'] = 'pdf';

        $TaId = $this->input->post('TaId');

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('makalahSkripsi')) {
            $TA = $this->db->get_where('SeminarSkripsiTA', ['TaId' => $TaId])->row_array();

            // File upload success
            $data = $this->upload->data();
            $uploaded_filename = $data['file_name'];
            $extention = pathinfo($uploaded_filename, PATHINFO_EXTENSION);
            $source_path = './assets/uploads/skripsi_ta/' . $uploaded_filename;
            $new_filename = $TaId.'-MakalahSkripsi.'.$extention; // Change this to your desired new name
            $new_path = './assets/uploads/skripsi_ta/' . $new_filename;

            if (rename($source_path, $new_path)) {
                if ($TA['MakalahSkripsi'] != "" && $TA['MakalahSkripsi'] != $new_filename && $TA['MakalahSkripsi'] != null){
                    unlink('./assets/uploads/skripsi_ta/'. $TA['MakalahSkripsi']);
                }

                $datetime = new DateTime;
                $timezone = new DateTimeZone('Asia/Jakarta'); 
        
                $datetime->setTimezone($timezone);

                $requestUploadMakalahSkripsi = [
                    'UploadMakalah' => $datetime->format("Y-m-d H:i:s"),
                    'MakalahSkripsi' => $new_filename
                ];

                $this->db->update('SeminarSkripsiTA', $requestUploadMakalahSkripsi, "TaId = $TaId");

                redirect('TugasAkhir/SeminarSkripsi');
            }

        } else {
            // File upload failed
            $response = 'File upload failed: ' . $this->upload->display_errors();
        }
    }

    public function DetailSkripsi(){
        $TaId = $this->input->post('TaId');
        /* Ambil Detail Skripsi */
        $queryDetailSkripsi = "
                            select 
                            usr.Name, usr.Username, 
                            ta.BidangMinat, ta.JudulIdn, ta.JudulEng, ta.Id as TaId, skripsi.Dosen1Id, skripsi.Dosen2Id, skripsi.Dosen3Id, ta.JenisPenilaian,
                            (CASE WHEN skripsi.Dosen1Id IS NOT NULL OR skripsi.Dosen1Id != 0
                                THEN (SELECT Dosen1.Name
                                        FROM Users Dosen1
                                        WHERE Id = skripsi.Dosen1Id)
                                ELSE NULL END) AS Dosen1Nama,
                            (CASE WHEN skripsi.Dosen2Id IS NOT NULL OR skripsi.Dosen2Id != 0
                                THEN (SELECT Dosen2.Name
                                        FROM Users Dosen2
                                        WHERE Id = skripsi.Dosen2Id)
                                ELSE NULL END) AS Dosen2Nama,
                            (CASE WHEN skripsi.Dosen3Id IS NOT NULL OR skripsi.Dosen3Id != 0
                                THEN (SELECT Dosen3.Name
                                        FROM Users Dosen3
                                        WHERE Id = skripsi.Dosen3Id)
                                ELSE NULL END) AS Dosen3Nama,
                            skripsi.TanggalUjian, skripsi.JamMulai, skripsi.JamSelesai, skripsi.RuangId, skripsi.LinkZoom,
                            (CASE WHEN skripsi.RuangId IS NOT NULL
                            THEN (SELECT Nama
                                    FROM AkademikRuangan
                                    WHERE Id = skripsi.RuangId)
                            ELSE NULL END) AS RuangNama,
                            (
                                select MaxDateskripsi 
                                from AkademikSkripsi 
                                limit 1) as MaxDateskripsi
                            from Users usr
                            join TugasAkhirs ta on usr.Id = ta.MhsId
                            join SeminarSkripsiTA skripsi on ta.Id = skripsi.TaId
                            where ta.Id = $TaId
                            ";

        $DetailSkripsi = $this->db->query($queryDetailSkripsi)->row_array();

        switch ($DetailSkripsi['BidangMinat']){
            case 0:
                $DetailSkripsi['Bidang'] = "Belum Ada";
                break;
            case 1:
                $DetailSkripsi['Bidang'] = "Biokimia";
                break;
            case 2:
                $DetailSkripsi['Bidang'] = "Kimia Analitik";
                break;
            case 3:
                $DetailSkripsi['Bidang'] = "Kimia Anorganik";
                break;
            case 4:
                $DetailSkripsi['Bidang'] = "Kimia Fisik";
                break;
            case 5:
                $DetailSkripsi['Bidang'] = "Kimia Organik";
                break;

            default:
                $DetailSkripsi['Bidang'] = "-";
        }

        $DetailSkripsi['Waktu'] = $DetailSkripsi['JamMulai'] . " - " . $DetailSkripsi['JamSelesai'];

        echo json_encode($DetailSkripsi);
    }

    public function EditSkripsi(){

        $TaId = htmlspecialchars($this->input->post('TaId'));

        $RuangId = htmlspecialchars($this->input->post('ruangId'));
        $LinkZoom = htmlspecialchars($this->input->post('linkZoom'));
        
        $requestSkripsi = [
            "TanggalUjian" => htmlspecialchars($this->input->post('tanggalUjian')),
            "JamMulai" => htmlspecialchars($this->input->post('jamMulai')),
            "JamSelesai" => htmlspecialchars($this->input->post('jamSelesai')),
            "RuangId" => $RuangId,
            "LinkZoom" => $LinkZoom,
            "Dosen1Id" => htmlspecialchars($this->input->post("dosen1")),
            "Dosen2Id" => htmlspecialchars($this->input->post("dosen2")),
            "Dosen3Id" => htmlspecialchars($this->input->post("dosen3"))
        ];

        $this->db->update('SeminarSkripsiTA', $requestSkripsi, "TaId = $TaId");

        $requestTA = [
            "JenisPenilaian" => htmlspecialchars($this->input->post('jenisPenilaianSkripsi')),
        ];

        $this->db->update('TugasAkhirs', $requestTA, "Id = $TaId");

        redirect('TugasAkhir/SeminarSkripsi');
    }

    public function NilaiSkripsiTA($TaId = null){
        $data['title'] = 'Nilai Seminar Ujian AKhir (Skripsi)';

        $Userid = $this->session->userdata['Id'];

        $roleId = $this->session->userdata['RoleId'];
        $mhsRole = [2,3,4];
        $dsnRole = [1,6];

        /* Cek Dosen */
        $queryTa = "
                    select ta.JenisPenilaian,
                    mhs.Name as NamaMahasiswa,
                    skripsi.Dosen1Id, skripsi.Dosen2Id, skripsi.Dosen3Id,
                    (CASE WHEN skripsi.Dosen1Id IS NOT NULL OR skripsi.Dosen1Id != 0
                        THEN (SELECT Dosen1.Name
                                FROM Users Dosen1
                                WHERE Id = skripsi.Dosen1Id)
                        ELSE '-' END) AS Dosen1Name,
                    (CASE WHEN skripsi.Dosen2Id IS NOT NULL OR skripsi.Dosen2Id != 0
                        THEN (SELECT Dosen2.Name
                                FROM Users Dosen2
                                WHERE Id = skripsi.Dosen2Id)
                        ELSE '-' END) AS Dosen2Name,
                    (CASE WHEN skripsi.Dosen3Id IS NOT NULL OR skripsi.Dosen3Id != 0
                        THEN (SELECT Dosen3.Name
                                FROM Users Dosen3
                                WHERE Id = skripsi.Dosen3Id)
                        ELSE '-' END) AS Dosen3Name
                    FROM SeminarSkripsiTA skripsi 
                    JOIN TugasAkhirs ta ON skripsi.TaId = ta.Id 
                    JOIN Users mhs ON ta.MhsId = mhs.Id
                    where ta.Id = $TaId
                    ";

        $TA = $this->db->query($queryTa)->row_array();
        
        if (in_array($roleId, $mhsRole)){
            $query = "
                        select mhs.*, 
                        usr.Id, usr.Name, usr.Username, usr.Telpon, usr.Email, usr.RoleId, usr.CreatedAt, usr.Status, usr.ImageProfile, usr.AlamatMalang, usr.Alamat
                        from Users usr
                        join UserMahasiswa mhs on usr.Id = mhs.UserId
                        where usr.Id = '$Userid' 
                        ";

        }else if(in_array($roleId, $dsnRole)){
            $query = "
                        select dsn.*, 
                        usr.Id, usr.Name, usr.Username, usr.Telpon, usr.Email, usr.RoleId, usr.CreatedAt, usr.Status, usr.ImageProfile, usr.AlamatMalang, usr.Alamat
                        from Users usr
                        join UserTenagaKependidikan dsn on usr.Id = dsn.UserId
                        where usr.Id = '$Userid'
                        ";

        }else{
            $query = "select * from Users where Id = '$Userid'";
        }

        /* Perhitungan Nilai Seminar Ujian */
        /* Ambil Nilai Ujian */
        $queryNilaiUjianTA = "
                            select nilai.*,
                            skripsi.Dosen1Id, skripsi.Dosen2Id, skripsi.Dosen3Id,
                            (CASE WHEN skripsi.Dosen1Id IS NOT NULL
                            THEN (SELECT Dosen.Name
                                    FROM Users Dosen
                                    WHERE Id = skripsi.Dosen1Id)
                            ELSE NULL END) AS Dosen1Name,
                            (CASE WHEN skripsi.Dosen2Id IS NOT NULL
                            THEN (SELECT Dosen.Name
                                    FROM Users Dosen
                                    WHERE Id = skripsi.Dosen2Id)
                            ELSE NULL END) AS Dosen2Name,
                            (CASE WHEN skripsi.Dosen3Id IS NOT NULL
                            THEN (SELECT Dosen.Name
                                    FROM Users Dosen
                                    WHERE Id = skripsi.Dosen3Id)
                            ELSE NULL END) AS Dosen3Name
                            from NilaiSeminarSkripsiTA nilai
                            join SeminarSkripsiTA skripsi on nilai.TaId = skripsi.TaId
                            where nilai.TaId = $TaId
                            ";
        
        $NilaiUjianTA = $this->db->query($queryNilaiUjianTA)->row_array();

        if ($NilaiUjianTA != null){
            /* Dosen 1 */
            if ($NilaiUjianTA['Dsn1Pres1'] != null){
                $totalNilaiPresentasiDsn1 = ($NilaiUjianTA['Dsn1Pres1'] * 3.5) + ($NilaiUjianTA['Dsn1Pres2'] * 3.5) + ($NilaiUjianTA['Dsn1Pres3'] * 4) + ($NilaiUjianTA['Dsn1Pres4'] * 3.5);
            }else{
                $totalNilaiPresentasiDsn1 = 0;
            }
            if ($NilaiUjianTA['Dsn1Ber1'] != null){
                $totalNilaiBerkasDsn1 = ($NilaiUjianTA['Dsn1Ber1'] * 5) + ($NilaiUjianTA['Dsn1Ber2'] * 5.5) + ($NilaiUjianTA['Dsn1Ber4'] * 5);
                // $totalNilaiBerkasDsn1 = ($NilaiUjianTA['Dsn1Ber1'] * 5) + ($NilaiUjianTA['Dsn1Ber2'] * 5.5) + ($NilaiUjianTA['Dsn1Ber3'] * 5.5) + ($NilaiUjianTA['Dsn1Ber4'] * 5);
            }else{
                $totalNilaiBerkasDsn1 = 0;
            }
            $totalNilaiUjianDsn1TA = ($totalNilaiPresentasiDsn1 + $totalNilaiBerkasDsn1)/100;

            /* Dosen 2 */
            if ($NilaiUjianTA['Dsn2Pres1'] != null){
                $totalNilaiPresentasiDsn2 = ($NilaiUjianTA['Dsn2Pres1'] * 3.5) + ($NilaiUjianTA['Dsn2Pres2'] * 3.5) + ($NilaiUjianTA['Dsn2Pres3'] * 4) + ($NilaiUjianTA['Dsn2Pres4'] * 3.5);
            }else{
                $totalNilaiPresentasiDsn2 = 0;
            }
            if ($NilaiUjianTA['Dsn2Ber1'] != null){
                $totalNilaiBerkasDsn2 = ($NilaiUjianTA['Dsn2Ber1'] * 5) + ($NilaiUjianTA['Dsn2Ber2'] * 5.5) + ($NilaiUjianTA['Dsn2Ber4'] * 5);
                // $totalNilaiBerkasDsn2 = ($NilaiUjianTA['Dsn2Ber1'] * 5) + ($NilaiUjianTA['Dsn2Ber2'] * 5.5) + ($NilaiUjianTA['Dsn2Ber3'] * 5.5) + ($NilaiUjianTA['Dsn2Ber4'] * 5);
            }else{
                $totalNilaiBerkasDsn2 = 0;
            }
            $totalNilaiUjianDsn2TA = ($totalNilaiPresentasiDsn2 + $totalNilaiBerkasDsn2)/100;
            
            /* Dosen 3 */
            if ($NilaiUjianTA['Dsn3Pres1'] != null){
                $totalNilaiPresentasiDsn3 = ($NilaiUjianTA['Dsn3Pres1'] * 3.5) + ($NilaiUjianTA['Dsn3Pres2'] * 3.5) + ($NilaiUjianTA['Dsn3Pres3'] * 4) + ($NilaiUjianTA['Dsn3Pres4'] * 3.5);
            }else{
                $totalNilaiPresentasiDsn3 = 0;
            }
            if ($NilaiUjianTA['Dsn3Ber1'] != null){
                $totalNilaiBerkasDsn3 = ($NilaiUjianTA['Dsn3Ber1'] * 5) + ($NilaiUjianTA['Dsn3Ber2'] * 5.5) + ($NilaiUjianTA['Dsn3Ber4'] * 5);
                // $totalNilaiBerkasDsn3 = ($NilaiUjianTA['Dsn3Ber1'] * 5) + ($NilaiUjianTA['Dsn3Ber2'] * 5.5) + ($NilaiUjianTA['Dsn3Ber3'] * 5.5) + ($NilaiUjianTA['Dsn3Ber4'] * 5);
            }else{
                $totalNilaiBerkasDsn3 = 0;
            }
            $totalNilaiUjianDsn3TA = ($totalNilaiPresentasiDsn3 + $totalNilaiBerkasDsn3)/100;

            /* Perhitungan Nilai Rata-rata */
            $averagePresentasi = ($totalNilaiPresentasiDsn1 + $totalNilaiPresentasiDsn2 + $totalNilaiPresentasiDsn3)/3;
            $averageBerkas = ($totalNilaiBerkasDsn1 + $totalNilaiBerkasDsn2 + $totalNilaiBerkasDsn3)/3;
            // $averageTotalNilai = ($totalNilaiSemproDsn1TA + $totalNilaiSemproDsn2TA + $totalNilaiSemproDsn1TA)/3;

            $data['averagePresentasi'] = number_format($averagePresentasi,2);
            $data['averageBerkas'] = number_format($averageBerkas,2);
            // $data['averageTotalNilai'] = number_format($averageTotalNilai,2);

            /* Saran */
            $data['Dsn1Saran'] = $NilaiUjianTA['Dsn1Saran'];
            $data['Dsn2Saran'] = $NilaiUjianTA['Dsn2Saran'];
            $data['Dsn3Saran'] = $NilaiUjianTA['Dsn3Saran'];

            /* Perhitungan Total Nilai berdasarkan Jumlah Pembimbing */
            if($TA['JenisPenilaian'] == 1){
                /* Perhitungan Total Nilai Skripsi */
                /* Dosen 1 */
                $NilaiSkripsiDosen1 = (($NilaiUjianTA['Dsn1Skripsi1'] * 6) + ($NilaiUjianTA['Dsn1Skripsi2'] * 6) + ($NilaiUjianTA['Dsn1Skripsi3'] * 6) + ($NilaiUjianTA['Dsn1Skripsi4'] * 6) + ($NilaiUjianTA['Dsn1Skripsi5'] * 6)) / 100;
                
                /* Dosen 2 */
                $NilaiSkripsiDosen2 = (($NilaiUjianTA['Dsn2Skripsi1'] * 6) + ($NilaiUjianTA['Dsn2Skripsi2'] * 6) + ($NilaiUjianTA['Dsn2Skripsi3'] * 6) + ($NilaiUjianTA['Dsn2Skripsi4'] * 6) + ($NilaiUjianTA['Dsn2Skripsi5'] * 6)) / 100;

                /* Total Nilai Skripsi */
                $TotalNilaiSkripsi = (($NilaiSkripsiDosen1 + $NilaiSkripsiDosen2) * 0.5);

                $data['NilaiSkripsiDosen1'] = $NilaiSkripsiDosen1;
                $data['NilaiSkripsiDosen2'] = $NilaiSkripsiDosen2;
                $data['TotalNilaiSkripsi'] = $TotalNilaiSkripsi;

                /* Perhitungan Total Nilai Skripsi */
                $NilaiAkhirUjianTA = ((($totalNilaiPresentasiDsn1 + $totalNilaiPresentasiDsn2) * 0.35) + ($totalNilaiPresentasiDsn3 * 0.3) + (($totalNilaiBerkasDsn1 + $totalNilaiBerkasDsn2) * 0.4) + ($totalNilaiBerkasDsn3 * 0.2)) / 100;
            }else{
                /* Perhitungan Total Nilai Skripsi */
                /* Dosen 1 */
                $NilaiSkripsiDosen1 = (($NilaiUjianTA['Dsn1Skripsi1'] * 6) + ($NilaiUjianTA['Dsn1Skripsi2'] * 6) + ($NilaiUjianTA['Dsn1Skripsi3'] * 6) + ($NilaiUjianTA['Dsn1Skripsi4'] * 6) + ($NilaiUjianTA['Dsn1Skripsi5'] * 6)) / 100;
        
                /* Total Nilai Skripsi */
                $TotalNilaiSkripsi = ($NilaiSkripsiDosen1 * 1);

                $data['NilaiSkripsiDosen1'] = $NilaiSkripsiDosen1;
                $data['NilaiSkripsiDosen2'] = "-";
                $data['TotalNilaiSkripsi'] = $TotalNilaiSkripsi;

                /* Perhitungan Total Nilai Skripsi */
                $NilaiAkhirUjianTA = (($totalNilaiPresentasiDsn1 * 0.5) + (($totalNilaiPresentasiDsn2 + $totalNilaiPresentasiDsn3) * 0.25) + ($totalNilaiBerkasDsn1 * 0.6) + (($totalNilaiBerkasDsn2 + $totalNilaiBerkasDsn3) * 0.2)) / 100;
            }
            $NilaiAkhirUjianTA = number_format($NilaiAkhirUjianTA, 2);
            $data['NilaiAkhirUjianTA'] = $NilaiAkhirUjianTA;

            if ($NilaiUjianTA['NilaiAkhir'] == null || $NilaiUjianTA['NilaiAkhir'] != $NilaiAkhirUjianTA || $NilaiUjianTA['Dsn1TotalSkripsi'] != $NilaiSkripsiDosen1){
                $requestNilaiAkhir = [
                    'Dsn1TotalPres' => $totalNilaiPresentasiDsn1,
                    'Dsn2TotalPres' => $totalNilaiPresentasiDsn2,
                    'Dsn3TotalPres' => $totalNilaiPresentasiDsn3,

                    'Dsn1TotalBer' => $totalNilaiBerkasDsn1,
                    'Dsn2TotalBer' => $totalNilaiBerkasDsn2,
                    'Dsn3TotalBer' => $totalNilaiBerkasDsn3,

                    'NilaiAkhir' => $NilaiAkhirUjianTA
                ];

                if($TA['JenisPenilaian'] == 1){
                    $requestNilaiAkhir['Dsn1TotalSkripsi'] = $NilaiSkripsiDosen1;
                    $requestNilaiAkhir['Dsn2TotalSkripsi'] = $NilaiSkripsiDosen2;
                }else{
                    $requestNilaiAkhir['Dsn1TotalSkripsi'] = $NilaiSkripsiDosen1;
                }

                $this->db->update('NilaiSeminarSkripsiTA', $requestNilaiAkhir, "TaId = $TaId");
            }
        }else{
            $totalNilaiPresentasiDsn1 = 0;
            $totalNilaiBerkasDsn1 = 0;
            $totalNilaiUjianDsn1TA = 0;

            $totalNilaiPresentasiDsn2 = 0;
            $totalNilaiBerkasDsn2 = 0;
            $totalNilaiUjianDsn2TA = 0;

            $totalNilaiPresentasiDsn3 = 0;
            $totalNilaiBerkasDsn3 = 0;
            $totalNilaiUjianDsn3TA = 0;

            $data['NilaiAkhirUjianTA'] = 0;
            $data['NilaiSkripsiDosen1'] = 0;
            $data['NilaiSkripsiDosen2'] = 0;
            $data['TotalNilaiSkripsi'] = 0;

            $data['averagePresentasi'] = 0;
            $data['averageBerkas'] = 0;

            $data['Dsn1Saran'] = "-";
            $data['Dsn2Saran'] = "-";
            $data['Dsn3Saran'] = "-";
        }

        if ($TA['Dosen1Id'] == $Userid){
            $data['TipeDosen'] = 1;
            $data['totalNilaiPresentasiDsn1'] = number_format(($totalNilaiPresentasiDsn1 / 14.5),2);
            $data['totalNilaiBerkasDsn1'] = number_format(($totalNilaiBerkasDsn1 / 15.5),2);
            $data['totalNilaiUjianDsn1TA'] = number_format($totalNilaiUjianDsn1TA,2);
        }else if ($TA['Dosen2Id'] == $Userid){
            $data['TipeDosen'] = 2;
            $data['totalNilaiPresentasiDsn2'] = number_format(($totalNilaiPresentasiDsn2 / 14.5),2);
            $data['totalNilaiBerkasDsn2'] = number_format(($totalNilaiBerkasDsn2 / 15.5),2);
            $data['totalNilaiUjianDsn2TA'] = number_format($totalNilaiUjianDsn2TA,2);
        }else if ($TA['Dosen3Id'] == $Userid){
            $data['TipeDosen'] = 3;
            $data['totalNilaiPresentasiDsn3'] = number_format(($totalNilaiPresentasiDsn3 / 14.5),2);
            $data['totalNilaiBerkasDsn3'] = number_format(($totalNilaiBerkasDsn3 / 15.5),2);
            $data['totalNilaiUjianDsn3TA'] = number_format($totalNilaiUjianDsn3TA,2);
        }else{
            $data['TipeDosen'] = 0;
            $data['totalNilaiPresentasiDsn1'] = number_format(($totalNilaiPresentasiDsn1 / 14.5),2);
            $data['totalNilaiBerkasDsn1'] = number_format(($totalNilaiBerkasDsn1 / 15.5),2);
            $data['totalNilaiUjianDsn1TA'] = number_format($totalNilaiUjianDsn1TA,2);

            $data['totalNilaiPresentasiDsn2'] = number_format(($totalNilaiPresentasiDsn2 / 14.5),2);
            $data['totalNilaiBerkasDsn2'] = number_format(($totalNilaiBerkasDsn2 / 15.5),2);
            $data['totalNilaiUjianDsn2TA'] = number_format($totalNilaiUjianDsn2TA,2);

            $data['totalNilaiPresentasiDsn3'] = number_format(($totalNilaiPresentasiDsn3 / 14.5),2);
            $data['totalNilaiBerkasDsn3'] = number_format(($totalNilaiBerkasDsn3 / 15.5),2);
            $data['totalNilaiUjianDsn3TA'] = number_format($totalNilaiUjianDsn3TA,2);
        }

        $User = $this->db->query($query)->row_array();
        $data['User'] = $User;
        $data['TaId'] = $TaId;
        $data['TA'] = $TA;
        $data['DosenId'] = $TA;
        $data['NilaiUjianTA'] = $NilaiUjianTA;

        /* Query Nilai Sempro, Semju, Skripsi */
        $queryNilai = "
                        select 
                        ta.Id AS TaId, ta.JenisPenilaian,
                        sempro.NilaiAkhir AS NilaiAkhirSempro,
                        semju.NilaiAkhir AS NilaiAkhirSemju,
                        skripsi.NilaiAkhir AS NilaiAkhirSkripsi,
                        skripsi.Dsn1TotalSkripsi, skripsi.Dsn2TotalSkripsi
                        from TugasAkhirs ta
                        join NilaiSeminarProposalTA sempro on ta.Id = sempro.TaId
                        join NilaiSeminarKemajuanTA semju on ta.Id = semju.TaId
                        join NilaiSeminarSkripsiTA skripsi on ta.Id = skripsi.TaId
                        where ta.Id = $TaId
                        ";

        $Nilai = $this->db->query($queryNilai)->row_array();

        if ($Nilai != null){
            if ($Nilai['JenisPenilaian'] == 1){
                $Nilai['NilaiNaskah'] = number_format((($Nilai['Dsn1TotalSkripsi'] + $Nilai['Dsn2TotalSkripsi']) * 0.5), 2);
            }else{
                $Nilai['NilaiNaskah'] = number_format($Nilai['Dsn1TotalSkripsi'], 2);
            }

            $Nilai['TotalNilaiAkhir'] = number_format((number_format($Nilai['NilaiAkhirSempro'],2) + number_format($Nilai['NilaiAkhirSemju'],2) + number_format($Nilai['NilaiAkhirSkripsi'],2) + number_format($Nilai['NilaiNaskah'],2)),2) ;
        }else{
            $Nilai['NilaiAkhirSempro'] = 0;
            $Nilai['NilaiAkhirSemju'] = 0;
            $Nilai['NilaiAkhirSkripsi'] = 0;
            $Nilai['NilaiNaskah'] = 0;
            $Nilai['TotalNilaiAkhir'] = 0;
        }
        
        if ($Nilai['TotalNilaiAkhir'] < 0.75) {
            $Nilai['HurufMutu'] = "E";
        }elseif($Nilai['TotalNilaiAkhir'] >= 0.75 && $Nilai['TotalNilaiAkhir'] <= 1.24){
            $Nilai['HurufMutu'] = "D";
        }elseif($Nilai['TotalNilaiAkhir'] >= 1.25 && $Nilai['TotalNilaiAkhir'] <= 1.74){
            $Nilai['HurufMutu'] = "D+";
        }elseif($Nilai['TotalNilaiAkhir'] >= 1.75 && $Nilai['TotalNilaiAkhir'] <= 2.24){
            $Nilai['HurufMutu'] = "C";
        }elseif($Nilai['TotalNilaiAkhir'] >= 2.25 && $Nilai['TotalNilaiAkhir'] <= 2.74){
            $Nilai['HurufMutu'] = "C+";
        }elseif($Nilai['TotalNilaiAkhir'] >= 2.75 && $Nilai['TotalNilaiAkhir'] <= 3.24){
            $Nilai['HurufMutu'] = "B";
        }elseif($Nilai['TotalNilaiAkhir'] >= 3.25 && $Nilai['TotalNilaiAkhir'] <= 3.74){
            $Nilai['HurufMutu'] = "B+";
        }elseif($Nilai['TotalNilaiAkhir'] >= 3.75){
            $Nilai['HurufMutu'] = "A";
        }

        $data['Nilai'] = $Nilai;

        /* Setting kembali */
        $data['url_kembali'] = get_referer_url();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('tugasAkhir/NilaiSeminarSkripsi', $data);
        $this->load->view('templates/footer');
    }

    public function DetailNilaiSkripsi(){
        $TaId = $this->input->post('TaId');

        $queryNilaiSkripsi = "
                            select * from NilaiSeminarSkripsiTA where TaId = $TaId
                            ";

        $NilaiSkripsi = $this->db->query($queryNilaiSkripsi)->row_array();

        echo json_encode($NilaiSkripsi);
    }

    public function SubmitNilaiSkripsi($id = null){

        $Userid = $this->session->userdata['Id'];

        /* Cek Dosen */
        $queryTA = "
                    select skripsi.Dosen1Id, skripsi.Dosen2Id, skripsi.Dosen3Id, ta.JenisPenilaian
                    from SeminarSkripsiTA skripsi
                    join TugasAkhirs ta on skripsi.TaId = ta.Id
                    where skripsi.TaId = $id
                    ";
        
        $TA = $this->db->query($queryTA)->row_array();

        /* Cek Data Nilai */
        $queryNilaiSkripsi = "
                            select * from NilaiSeminarSkripsiTA
                            where TaId = $id
                            ";
        
        $NilaiSkripsiTA = $this->db->query($queryNilaiSkripsi)->row_array();

        $UpdateNilaiTA = false;
        if ($NilaiSkripsiTA != null){
            $UpdateNilaiTA = true;
        }

        if ($TA['Dosen1Id'] == $Userid){
            $requestNilaiSkripsiTA = [
                "Dsn1Pres1" => htmlspecialchars($this->input->post('langkahLangkah_8A')),
                "Dsn1Pres2" => htmlspecialchars($this->input->post('pengukuran_8B')),
                "Dsn1Pres3" => htmlspecialchars($this->input->post('pengukuran_9C')),
                "Dsn1Pres4" => htmlspecialchars($this->input->post('kesimpulan_8C')),

                "Dsn1Ber1" => htmlspecialchars($this->input->post('aturanPenulisan_9A')),
                "Dsn1Ber2" => htmlspecialchars($this->input->post('tampilan_9A')),
                // "Dsn1Ber3" => htmlspecialchars($this->input->post('tampilan_9B')),
                "Dsn1Ber4" => htmlspecialchars($this->input->post('organisasi_9B')),

                "Dsn1Skripsi1" => htmlspecialchars($this->input->post('mengolahData_8B')),
                "Dsn1Skripsi2" => htmlspecialchars($this->input->post('kesimpulan_8C')),
                "Dsn1Skripsi3" => htmlspecialchars($this->input->post('tataLetak_9A')),
                "Dsn1Skripsi4" => htmlspecialchars($this->input->post('konsep_9A')),
                "Dsn1Skripsi5" => htmlspecialchars($this->input->post('pembahasan_9B')),
            ];
            if (!$UpdateNilaiTA){
                /* Insert Nilai Skripsi Dosen 1 */
                $requestNilaiSkripsiTA['TaId'] = $id;
                $this->db->insert('NilaiSeminarSkripsiTA', $requestNilaiSkripsiTA);
            }else{
                /* Update Nilai Skripsi Dosen 1 */
                $this->db->update('NilaiSeminarSkripsiTA', $requestNilaiSkripsiTA, ['TaId'=> $id]);
            }
        }else if($TA['Dosen2Id'] == $Userid){
            $requestNilaiSkripsiTA = [
                "Dsn2Pres1" => htmlspecialchars($this->input->post('langkahLangkah_8A')),
                "Dsn2Pres2" => htmlspecialchars($this->input->post('pengukuran_8B')),
                "Dsn2Pres3" => htmlspecialchars($this->input->post('pengukuran_9C')),
                "Dsn2Pres4" => htmlspecialchars($this->input->post('kesimpulan_8C')),

                "Dsn2Ber1" => htmlspecialchars($this->input->post('aturanPenulisan_9A')),
                "Dsn2Ber2" => htmlspecialchars($this->input->post('tampilan_9A')),
                // "Dsn2Ber3" => htmlspecialchars($this->input->post('tampilan_9B')),
                "Dsn2Ber4" => htmlspecialchars($this->input->post('organisasi_9B')),
            ];

            if ($TA['JenisPenilaian'] == 1){
                $requestNilaiSkripsiTA["Dsn2Skripsi1"] = htmlspecialchars($this->input->post('mengolahData_8B'));
                $requestNilaiSkripsiTA["Dsn2Skripsi2"] = htmlspecialchars($this->input->post('kesimpulan_8C'));
                $requestNilaiSkripsiTA["Dsn2Skripsi3"] = htmlspecialchars($this->input->post('tataLetak_9A'));
                $requestNilaiSkripsiTA["Dsn2Skripsi4"] = htmlspecialchars($this->input->post('konsep_9A'));
                $requestNilaiSkripsiTA["Dsn2Skripsi5"] = htmlspecialchars($this->input->post('pembahasan_9B'));
            }
            if (!$UpdateNilaiTA){
                /* Insert Nilai Skripsi Dosen 2 */
                $requestNilaiSkripsiTA['TaId'] = $id;
                $this->db->insert('NilaiSeminarSkripsiTA', $requestNilaiSkripsiTA);
            }else{
                /* Update Nilai Skripsi Dosen 2 */
                $this->db->update('NilaiSeminarSkripsiTA', $requestNilaiSkripsiTA, ['TaId'=> $id]);
            }
        }else if($TA['Dosen3Id'] == $Userid){
            $requestNilaiSkripsiTA = [
                "Dsn3Pres1" => htmlspecialchars($this->input->post('langkahLangkah_8A')),
                "Dsn3Pres2" => htmlspecialchars($this->input->post('pengukuran_8B')),
                "Dsn3Pres3" => htmlspecialchars($this->input->post('pengukuran_9C')),
                "Dsn3Pres4" => htmlspecialchars($this->input->post('kesimpulan_8C')),

                "Dsn3Ber1" => htmlspecialchars($this->input->post('aturanPenulisan_9A')),
                "Dsn3Ber2" => htmlspecialchars($this->input->post('tampilan_9A')),
                // "Dsn3Ber3" => htmlspecialchars($this->input->post('tampilan_9B')),
                "Dsn3Ber4" => htmlspecialchars($this->input->post('organisasi_9B')),
            ];
            if (!$UpdateNilaiTA){
                /* Insert Nilai Skripsi Dosen 3 */
                $requestNilaiSkripsiTA['TaId'] = $id;
                $this->db->insert('NilaiSeminarSkripsiTA', $requestNilaiSkripsiTA);
            }else{
                /* Update Nilai Skripsi Dosen 3 */
                $this->db->update('NilaiSeminarSkripsiTA', $requestNilaiSkripsiTA, ['TaId'=> $id]);
            }
        }else if($this->input->post('tipeDosen') != null){
            switch ($this->input->post('tipeDosen')){
                case 1:
                    $requestNilaiSkripsiTA = [
                        "Dsn1Pres1" => htmlspecialchars($this->input->post('langkahLangkah_8A')),
                        "Dsn1Pres2" => htmlspecialchars($this->input->post('pengukuran_8B')),
                        "Dsn1Pres3" => htmlspecialchars($this->input->post('pengukuran_9C')),
                        "Dsn1Pres4" => htmlspecialchars($this->input->post('kesimpulan_8C')),

                        "Dsn1Ber1" => htmlspecialchars($this->input->post('aturanPenulisan_9A')),
                        "Dsn1Ber2" => htmlspecialchars($this->input->post('tampilan_9A')),
                        // "Dsn1Ber3" => htmlspecialchars($this->input->post('tampilan_9B')),
                        "Dsn1Ber4" => htmlspecialchars($this->input->post('organisasi_9B')),

                        "Dsn1Skripsi1" => htmlspecialchars($this->input->post('mengolahData_8B')),
                        "Dsn1Skripsi2" => htmlspecialchars($this->input->post('kesimpulan_8C')),
                        "Dsn1Skripsi3" => htmlspecialchars($this->input->post('tataLetak_9A')),
                        "Dsn1Skripsi4" => htmlspecialchars($this->input->post('konsep_9A')),
                        "Dsn1Skripsi5" => htmlspecialchars($this->input->post('pembahasan_9B')),
                    ];
                    break;
                case 2:
                    $requestNilaiSkripsiTA = [
                        "Dsn2Pres1" => htmlspecialchars($this->input->post('langkahLangkah_8A')),
                        "Dsn2Pres2" => htmlspecialchars($this->input->post('pengukuran_8B')),
                        "Dsn2Pres3" => htmlspecialchars($this->input->post('pengukuran_9C')),
                        "Dsn2Pres4" => htmlspecialchars($this->input->post('kesimpulan_8C')),
        
                        "Dsn2Ber1" => htmlspecialchars($this->input->post('aturanPenulisan_9A')),
                        "Dsn2Ber2" => htmlspecialchars($this->input->post('tampilan_9A')),
                        // "Dsn2Ber3" => htmlspecialchars($this->input->post('tampilan_9B')),
                        "Dsn2Ber4" => htmlspecialchars($this->input->post('organisasi_9B')),
                    ];
        
                    if ($TA['JenisPenilaian'] == 1){
                        $requestNilaiSkripsiTA["Dsn2Skripsi1"] = htmlspecialchars($this->input->post('mengolahData_8B'));
                        $requestNilaiSkripsiTA["Dsn2Skripsi2"] = htmlspecialchars($this->input->post('kesimpulan_8C'));
                        $requestNilaiSkripsiTA["Dsn2Skripsi3"] = htmlspecialchars($this->input->post('tataLetak_9A'));
                        $requestNilaiSkripsiTA["Dsn2Skripsi4"] = htmlspecialchars($this->input->post('konsep_9A'));
                        $requestNilaiSkripsiTA["Dsn2Skripsi5"] = htmlspecialchars($this->input->post('pembahasan_9B'));
                    }
                    break;
                case 3:
                    $requestNilaiSkripsiTA = [
                        "Dsn3Pres1" => htmlspecialchars($this->input->post('langkahLangkah_8A')),
                        "Dsn3Pres2" => htmlspecialchars($this->input->post('pengukuran_8B')),
                        "Dsn3Pres3" => htmlspecialchars($this->input->post('pengukuran_9C')),
                        "Dsn3Pres4" => htmlspecialchars($this->input->post('kesimpulan_8C')),
        
                        "Dsn3Ber1" => htmlspecialchars($this->input->post('aturanPenulisan_9A')),
                        "Dsn3Ber2" => htmlspecialchars($this->input->post('tampilan_9A')),
                        // "Dsn3Ber3" => htmlspecialchars($this->input->post('tampilan_9B')),
                        "Dsn3Ber4" => htmlspecialchars($this->input->post('organisasi_9B')),
                    ];
                    break;
            }

            if (!$UpdateNilaiTA){
                /* Insert Nilai Skripsi Dosen 1 */
                $requestNilaiSkripsiTA['TaId'] = $id;
                $this->db->insert('NilaiSeminarSkripsiTA', $requestNilaiSkripsiTA);
            }else{
                /* Update Nilai Skripsi Dosen 1 */
                $this->db->update('NilaiSeminarSkripsiTA', $requestNilaiSkripsiTA, ['TaId'=> $id]);
            }
        }

        redirect('TugasAkhir/NilaiSkripsiTA/'. $id);
    }

    public function SubmitSaranSkripsi($id = null){

        $Userid = $this->session->userdata['Id'];

        /* Cek Dosen */
        $queryDsn = "
                    select Dosen1Id, Dosen2Id, Dosen3Id
                    from SeminarSkripsiTA
                    where TaId = $id
                    ";
        
        $JenisDosen = $this->db->query($queryDsn)->row_array();

        /* Cek Data Saran */
        $querySaranSkripsi = "
                            select * from NilaiSeminarSkripsiTA
                            where TaId = $id
                            ";
        
        $SaranSkripsiTA = $this->db->query($querySaranSkripsi)->row_array();

        $UpdateSaranTA = false;
        if ($SaranSkripsiTA != null){
            $UpdateSaranTA = true;
        }

        if ($JenisDosen['Dosen1Id'] == $Userid){
            $requestSaranSkripsiTA = [
                "Dsn1Saran" => $this->input->post('saran'),
            ];

            if (!$UpdateSaranTA){
                /* Insert Saran Skripsi Dosen 1 */
                $requestSaranSkripsiTA['TaId'] = $id;
                $this->db->insert('NilaiSeminarSkripsiTA', $requestSaranSkripsiTA);
            }else{
                /* Update Saran Skripsi Dosen 1 */
                $this->db->update('NilaiSeminarSkripsiTA', $requestSaranSkripsiTA, ['TaId'=> $id]);
            }
        }else if($JenisDosen['Dosen2Id'] == $Userid){
            $requestSaranSkripsiTA = [
                "Dsn2Saran" => $this->input->post('saran'),
            ];

            if (!$UpdateSaranTA){
                /* Insert Saran Skripsi Dosen 2 */
                $requestSaranSkripsiTA['TaId'] = $id;
                $this->db->insert('NilaiSeminarSkripsiTA', $requestSaranSkripsiTA);
            }else{
                /* Update Saran Skripsi Dosen 2 */
                $this->db->update('NilaiSeminarSkripsiTA', $requestSaranSkripsiTA, ['TaId'=> $id]);
            }
        }else if($JenisDosen['Dosen3Id'] == $Userid){
            $requestSaranSkripsiTA = [
                "Dsn3Saran" => $this->input->post('saran'),
            ];

            if (!$UpdateSaranTA){
                /* Insert Saran Skripsi Dosen 3 */
                $requestSaranSkripsiTA['TaId'] = $id;
                $this->db->insert('NilaiSeminarSkripsiTA', $requestSaranSkripsiTA);
            }else{
                /* Update Saran Skripsi Dosen 3 */
                $this->db->update('NilaiSeminarSkripsiTA', $requestSaranSkripsiTA, ['TaId'=> $id]);
            }
        }

        redirect('TugasAkhir/NilaiSkripsiTA/'. $id);
    }

    public function DownloadBASkripsi($TaId = null){
        require_once 'vendor/autoload.php';
        $phpWord = new \PhpOffice\PhpWord\PhpWord();

        $document = new \PhpOffice\PhpWord\TemplateProcessor('assets/templates/word/TemplateBA.docx');

        /* Get Data User */
        $query = "
                select 
                ujian.TanggalUjian, ujian.JamMulai, ujian.JamSelesai,
                ta.BidangMinat, ta.JudulIdn,
                ruangan.Nama as NamaRuangan,
                mhs.Name as NamaMahasiswa, mhs.Username as NIM,
                dsn1.Name as NamaDosen1,
                dsn2.Name as NamaDosen2,
                dsn3.Name as NamaDosen3
                from SeminarSkripsiTA ujian
                join TugasAkhirs ta on ujian.TaId = ta.Id
                join AkademikRuangan ruangan on ujian.RuangId = ruangan.Id
                join Users mhs on ta.MhsId = mhs.Id
                join Users dsn1 on ujian.Dosen1Id = dsn1.Id
                join Users dsn2 on ujian.Dosen2Id = dsn2.Id
                join Users dsn3 on ujian.Dosen3Id = dsn3.Id
                where ta.Id =  $TaId
                ";

        $DataTA = $this->db->query($query)->row_array();

        $Waktu = $DataTA['JamMulai'] . " - " . $DataTA['JamSelesai'];

        switch ($DataTA['BidangMinat']){
            case 0:
                $DataTA['Bidang'] = "Belum Ada";
                break;
            case 1:
                $DataTA['Bidang'] = "Biokimia";
                break;
            case 2:
                $DataTA['Bidang'] = "Kimia Analitik";
                break;
            case 3:
                $DataTA['Bidang'] = "Kimia Anorganik";
                break;
            case 4:
                $DataTA['Bidang'] = "Kimia Fisik";
                break;
            case 5:
                $DataTA['Bidang'] = "Kimia Organik";
                break;

            default:
                $DataTA['Bidang'] = "-";
        }

        /* Set Value */
        $document->setValue('tanggalUjian', date("d-m-Y", strtotime($DataTA['TanggalUjian'])));
        $document->setValue('waktu', $Waktu);
        $document->setValue('ruangan', $DataTA['NamaRuangan']);
        $document->setValue('namaMahasiswa', $DataTA['NamaMahasiswa']);
        $document->setValue('NIM', $DataTA['NIM']);
        $document->setValue('bidangMinat', $DataTA['Bidang']);
        $document->setValue('judul', $DataTA['JudulIdn']);
        $document->setValue('dosen1', $DataTA['NamaDosen1']);
        $document->setValue('dosen2', $DataTA['NamaDosen2']);
        $document->setValue('dosen3', $DataTA['NamaDosen3']);

        /* Change Filename to TaId-BeritaAcara.pdf */
        $filename = $TaId.'-BeritaAcara.docx';
        $directory = './assets/templates/temp/';
        $fullPath = $directory . $filename;
        $document->saveAs($fullPath);

        // Offer the document as a download link
        $this->load->helper('download');
        force_download($fullPath, NULL);

        sleep(5);
        
        /* Remove the file */
        unlink($fullPath);
    }

    /* General */
    public function NilaiTugasAkhirMahasiswa(){
        $data['title'] = 'Nilai Tugas Akhir Mahasiswa';

        $Userid = $this->session->userdata['Id'];
        $roleId = $this->session->userdata['RoleId'];
        $query = "select * from Users where Id = '$Userid'";

        $User = $this->db->query($query)->row_array();
        $data['User'] = $User;

        $querySemesterId = "select Id from AkademikSemester order by Id Desc limit 1";
        $LastSemesterId = $this->db->query($querySemesterId)->row_array();

        /* Query Nilai Sempro, Semju, Skripsi */
        $queryNilai = "
                        SELECT 
                        ta.Id AS TaId,
                        sempro.NilaiAkhir AS NilaiAkhirSempro,
                        semju.NilaiAkhir AS NilaiAkhirSemju,
                        skripsi.NilaiAkhir AS NilaiAkhirSkripsi,
                        skripsi.Dsn1TotalSkripsi, skripsi.Dsn2TotalSkripsi,
                        users.Name,
                        users.Username
                        from TugasAkhirs ta
                        join NilaiSeminarProposalTA sempro on ta.Id = sempro.TaId
                        join NilaiSeminarKemajuanTA semju on ta.Id = semju.TaId
                        join NilaiSeminarSkripsiTA skripsi on ta.Id = skripsi.TaId
                        JOIN Users users ON users.Id = ta.MhsId
                        WHERE ta.Tahapan = 4
                        ";

        if($this->input->post('semester') != NULL){
            $semesterId = $this->input->post('semester');
            $queryNilai = $queryNilai . " AND semesterId = $semesterId"; 
        }else{
            $semesterId = $LastSemesterId['Id'] - 1;
            $queryNilai = $queryNilai . " AND semesterId = $semesterId"; 
        }

        if($this->input->post('search') != NULL){
            $search = $this->input->post('search');
            $queryNilai = $queryNilai . " AND users.Name LIKE '%$search%' OR users.Username LIKE '%$search%'"; 
        }

        $queryNilai = $queryNilai . " Order By ta.Id Desc";
        
        if($this->input->post('limit') != NULL){
            $limit = $this->input->post('limit');
            $queryNilai = $queryNilai . " limit $limit"; 
        }

        $Nilai = $this->db->query($queryNilai)->result_array();

        if ($Nilai != null){
            foreach ($Nilai as $key => $nilai){
                $Nilai[$key]['NilaiNaskah'] = number_format((($Nilai[$key]['Dsn1TotalSkripsi'] + $Nilai[$key]['Dsn2TotalSkripsi']) * 0.5), 2);

                $Nilai[$key]['TotalNilaiAkhir'] = number_format((number_format($Nilai[$key]['NilaiAkhirSempro'],2) + number_format($Nilai[$key]['NilaiAkhirSemju'],2) + number_format($Nilai[$key]['NilaiAkhirSkripsi'],2) + number_format($Nilai[$key]['NilaiNaskah'],2)),2) ;

                if ($Nilai[$key]['TotalNilaiAkhir'] < 0.75) {
                    $Nilai[$key]['HurufMutu'] = "E";
                }elseif($Nilai[$key]['TotalNilaiAkhir'] >= 0.75 && $Nilai[$key]['TotalNilaiAkhir'] <= 1.24){
                    $Nilai[$key]['HurufMutu'] = "D";
                }elseif($Nilai[$key]['TotalNilaiAkhir'] >= 1.25 && $Nilai[$key]['TotalNilaiAkhir'] <= 1.74){
                    $Nilai[$key]['HurufMutu'] = "D+";
                }elseif($Nilai[$key]['TotalNilaiAkhir'] >= 1.75 && $Nilai[$key]['TotalNilaiAkhir'] <= 2.24){
                    $Nilai[$key]['HurufMutu'] = "C";
                }elseif($Nilai[$key]['TotalNilaiAkhir'] >= 2.25 && $Nilai[$key]['TotalNilaiAkhir'] <= 2.74){
                    $Nilai[$key]['HurufMutu'] = "C+";
                }elseif($Nilai[$key]['TotalNilaiAkhir'] >= 2.75 && $Nilai[$key]['TotalNilaiAkhir'] <= 3.24){
                    $Nilai[$key]['HurufMutu'] = "B";
                }elseif($Nilai[$key]['TotalNilaiAkhir'] >= 3.25 && $Nilai[$key]['TotalNilaiAkhir'] <= 3.74){
                    $Nilai[$key]['HurufMutu'] = "B+";
                }elseif($Nilai[$key]['TotalNilaiAkhir'] >= 3.75){
                    $Nilai[$key]['HurufMutu'] = "A";
                }
            }
        }

        $data['Nilai'] = $Nilai;

        /* Ambil Data Tahun Akademik */
        $queryAkademik = "
                        select * from AkademikSemester
                        where Status = 1 
                        Order by id Desc
                        ";

        $ListSemester = $this->db->query($queryAkademik)->result_array();
        $data['Semester'] = $ListSemester;

        /* Setting kembali */
        $data['url_kembali'] = get_referer_url();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('tugasAkhir/NilaiMahasiswa', $data);
        $this->load->view('templates/footer');
    }
}