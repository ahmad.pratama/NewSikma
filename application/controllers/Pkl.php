<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pkl extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->library('form_validation');
        if (!$this->session->userdata('Id')){
            redirect('auth');
        }
    }
    
    public function index(){
        $data['title'] = 'Prakter Kerja Lapangan';
        $Id = $this->session->userdata['Id'];
        $RoleId = $this->session->userdata['RoleId'];
        $query = "select * from Users where Id = $Id";
        $User = $this->db->query($query)->row_array();
        $data['User'] = $User;

        $mhs = [2,3,4];
        if (in_array($RoleId, $mhs)){
            /* Available Regis ujian PKL */
            $queryPKL = "
                        select nilai.LP_PKL_A
                        from PraktekKerjaLapangan pkl
                        join NilaiPembimbingLapangPKL nilai on pkl.Id = nilai.PklId
                        where pkl.MhsId = $Id
            ";

            $PKL = $this->db->query($queryPKL)->row_array();
            
            if ($PKL != null){
                $data['UjianPKL'] = $PKL;
            }else{
                $data['UjianPKL'] = false;
            };
        }
        
        /* Setting kembali */
        $data['url_kembali'] = get_referer_url();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('Pkl/index', $data);
        $this->load->view('templates/footer');
    }

    /* Pendaftaran PKL */
    public function PendaftaranPKL($id = null){
        $data['title'] = 'Pendaftaran Praktek Kerja Lapangan';
        if ($id == null){
            $id = $this->session->userdata['Id'];
        }

        $roleId = $this->session->userdata['RoleId'];

        $kadep = $this->session->userdata['Kadep'];
        $sekdep = $this->session->userdata['Sekdep'];
        $kaprodiS1 = $this->session->userdata['KaprodiS1'];
        $kaprodiS2 = $this->session->userdata['KaprodiS2'];
        $kaprodiS3 = $this->session->userdata['KaprodiS3'];

        $mhsRole = [2,3,4];
        $dsnRole = [1,6];
        if (in_array($roleId, $mhsRole)){
            $query = "
                        select mhs.*, 
                        usr.Id, usr.Name, usr.Username, usr.Telpon, usr.Email, usr.RoleId, usr.CreatedAt, usr.Status, usr.ImageProfile, usr.AlamatMalang, usr.Alamat
                        from Users usr
                        join UserMahasiswa mhs on usr.Id = mhs.UserId
                        where usr.Id = '$id' 
                        ";

            /* Ambil Data PKL Berdasarkan Mahasiswa */
            $queryPKL = "
                        select pkl.*,
                        usr.Name, usr.Username,
                        instansi.Nama as NamaInstansi, 
                        instansi.Alamat as AlamatInstansi
                        from PraktekKerjaLapangan pkl
                        join Users usr on usr.Id = pkl.MhsId
                        join AkademikInstansi instansi on instansi.Id = pkl.InstansiId
                        where pkl.MhsId = $id
                        Order By pkl.CreatedAt Desc
                        ";

            $PKL = $this->db->query($queryPKL)->result_array();
        }else if(in_array($roleId, $dsnRole) && ($kadep == false && $sekdep == false && $kaprodiS1 == false && $kaprodiS2 == false && $kaprodiS3 == false)){
            $query = "
                        select dsn.*, 
                        usr.Id, usr.Name, usr.Username, usr.Telpon, usr.Email, usr.RoleId, usr.CreatedAt, usr.Status, usr.ImageProfile, usr.AlamatMalang, usr.Alamat
                        from Users usr
                        join UserTenagaKependidikan dsn on usr.Id = dsn.UserId
                        where usr.Id = '$id'
                        ";

            /* Ambil Data PKL berdasarkan Mahasiswa */
            $queryPKL = "
                        select pkl.*,
                        usr.Name, usr.Username,
                        instansi.Nama as NamaInstansi, 
                        instansi.Alamat as AlamatInstansi
                        from PraktekKerjaLapangan pkl
                        join Users usr on usr.Id = pkl.MhsId
                        join AkademikInstansi instansi on instansi.Id = pkl.InstansiId
                        where pkl.DosenId = $id 
                        ";

            if($this->input->post('jenjang') != NULL){
                $jenjang = $this->input->post('jenjang');
                $queryPKL = $queryPKL . " AND RoleId = '$jenjang'";
            }else{
                $jenjang = 2;
                $queryPKL = $queryPKL . " AND RoleId = '$jenjang'";
            }

            if($this->input->post('search') != NULL){
                $search = $this->input->post('search');
                $queryPKL = $queryPKL . " AND usr.Name LIKE '%$search%' OR usr.Username LIKE '%$search%'"; 
            }

            if($this->input->post('limit') != NULL){
                $limit = $this->input->post('limit');
                $queryPKL = $queryPKL . " limit $limit"; 
            }

            $queryPKL = $queryPKL . " Order By pkl.CreatedAt Desc";

            $PKL = $this->db->query($queryPKL)->result_array();
        }else{
            $query = "select * from Users where Id = '$id'";
            
            /* Ambil Data PKL berdasarkan Mahasiswa */
            $queryPKL = "
                        select pkl.*,
                        usr.Name, usr.Username,
                        instansi.Nama as NamaInstansi, 
                        instansi.Alamat as AlamatInstansi
                        from PraktekKerjaLapangan pkl
                        join Users usr on usr.Id = pkl.MhsId
                        join AkademikInstansi instansi on instansi.Id = pkl.InstansiId
                        ";

            if($this->input->post('jenjang') != NULL){
                $jenjang = $this->input->post('jenjang');
                $queryPKL = $queryPKL . " AND RoleId = '$jenjang'";
            }else{
                $jenjang = 2;
                $queryPKL = $queryPKL . " AND RoleId = '$jenjang'";
            }

            if($this->input->post('search') != NULL){
                $search = $this->input->post('search');
                $queryPKL = $queryPKL . " AND usr.Name LIKE '%$search%' OR usr.Username LIKE '%$search%'"; 
            }

            if($this->input->post('limit') != NULL){
                $limit = $this->input->post('limit');
                $queryPKL = $queryPKL . " limit $limit"; 
            }

            $queryPKL = $queryPKL . " Order By pkl.CreatedAt Desc";
            
            $PKL = $this->db->query($queryPKL)->result_array();
        }

        $User = $this->db->query($query)->row_array();
        $data['User'] = $User;

        /* Ambil List Dosen */
        $queyrDosen = "select *
                        from Users
                        where RoleId = 1
                        ";

        $Dosen = $this->db->query($queyrDosen)->result_array();
        $data['Dosen'] = $Dosen;

        /* Ambil Data Tahun Akademik */
        $queryAkademik = "
                        select * from AkademikSemester
                        where Status = 1 
                        Order by id Desc
                        limit 1
                        ";

        $Akademik = $this->db->query($queryAkademik)->row_array();
        $data['Akademik'] = $Akademik;
        
        /* Ambil List Instansi */
        $queyrInstansi = "select *
                        from AkademikInstansi
                        where Status = 1 AND IsDeleted = 0
                        ";

        $Instansi = $this->db->query($queyrInstansi)->result_array();
        $data['Instansi'] = $Instansi;

        /* Ambil List Semester */
        $queyrSemester = "select *
                        from AkademikSemester
                        where Status = 1
                        ";

        $Semester = $this->db->query($queyrSemester)->result_array();

        $data['Semester'] = $Semester;

        foreach ($PKL as $key => $pkl) {
            switch ($pkl['Status']) {
                case 0:
                    $PKL[$key]['Status'] = 'Ditolak';
                    break;
                case 1:
                    $PKL[$key]['Status'] = 'Diterima';
                    break;
                case 2:
                    $PKL[$key]['Status'] = 'Diproses';
                    break;

                default:
                    $PKL[$key]['Status'] = 'Diproses';
                    break;
            }

            $PKL[$key]['TanggalMulai'] = date("d-m-Y", strtotime($pkl['TanggalMulai']));
            $PKL[$key]['TanggalSelesai'] = date("d-m-Y", strtotime($pkl['TanggalSelesai']));
        }

        $data['PendaftaranPKL'] = $PKL;

        /* Setting kembali */
        $data['url_kembali'] = get_referer_url();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('pkl/PendaftaranPKL', $data);
        $this->load->view('templates/footer');
    }

    public function DaftarPKL(){
        $datetime = new DateTime();
        $timezone = new DateTimeZone('Asia/Jakarta'); 

        $datetime->setTimezone($timezone);
        $MhsId = htmlspecialchars($this->input->post('mhsId'));

        $Semester = htmlspecialchars($this->input->post('semesterId'));

        /* Cek pendaftaran PKL */
        $queryCekPKL = "
                        select * 
                        from PraktekKerjaLapangan
                        where MhsId = $MhsId 
                        AND SemesterId = $Semester";

        $AvailablePKL = $this->db->query($queryCekPKL)->result_array();

        if (count($AvailablePKL) > 0){
            redirect('pkl/PendaftaranPKL');
        }

        $Instansi = htmlspecialchars($this->input->post('instansi'));
        if ($Instansi == 'lainnya'){
            $Nama = htmlspecialchars($this->input->post('nama'));
            $requestInstansi = [
                'Nama' => $Nama,
                'Alamat' => htmlspecialchars($this->input->post('alamat')),
                'Email' => htmlspecialchars($this->input->post('email')),
                'Telpon' => htmlspecialchars($this->input->post('telpon')),
                'Pimpinan' => htmlspecialchars($this->input->post('pimpinan')),
                'Status' => 1
            ];

            $this->db->insert('AkademikInstansi', $requestInstansi);
            
            $queryDataInstansiByName = "
                                        select * from AkademikInstansi
                                        where Nama = '$Nama' AND Status = 1
                                        ";
            $DataInstansi = $this->db->query($queryDataInstansiByName)->row_array();

            $Instansi = $DataInstansi["Id"];
        }

        $requestPKL = [
            'MhsId' => $MhsId,
            'DosenId' => htmlspecialchars($this->input->post('dosen1')),
            'SemesterId' => $Semester,
            'TanggalMulai' => htmlspecialchars($this->input->post('tanggalMulai')),
            'TanggalSelesai' => htmlspecialchars($this->input->post('tanggalSelesai')),
            'Status' => 2,
            'InstansiId' => $Instansi,
            'PembimbingLapangan' => htmlspecialchars($this->input->post('pembimbingLapang')),
            'CreatedAt' => $datetime->format("Y-m-d H:i:s")
        ];

        $this->db->insert('PraktekKerjaLapangan', $requestPKL);

        redirect('pkl/PendaftaranPKL');
    }

    public function UploadSuratInstansi(){
        $uploadPath = './assets/uploads/suratInstansi_pkl/';
        $config['upload_path'] = $uploadPath;
        $config['allowed_types'] = 'pdf|doc|docx';

        $PklId = $this->input->post('PklId');

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('suratInstansi')) {
            $PKL = $this->db->get_where('PraktekKerjaLapangan', ['Id' => $PklId])->row_array();

            // File upload success
            $data = $this->upload->data();
            $uploaded_filename = $data['file_name'];
            $extention = pathinfo($uploaded_filename, PATHINFO_EXTENSION);
            $source_path = './assets/uploads/suratInstansi_pkl/' . $uploaded_filename;
            $new_filename = $PklId.'-SuratInstansi-PKL.'.$extention; // Change this to your desired new name
            $new_path = './assets/uploads/suratInstansi_pkl/' . $new_filename;

            if (rename($source_path, $new_path)) {
                if ($PKL['FileSuratInstansi'] != "" && $PKL['FileSuratInstansi'] != $new_filename && $PKL['FileSuratInstansi'] != null){
                    unlink('./assets/uploads/suratInstansi_pkl/'. $PKL['FileSuratInstansi']);
                }

                $datetime = new DateTime;
                $timezone = new DateTimeZone('Asia/Jakarta'); 
        
                $datetime->setTimezone($timezone);

                $requestUploadSuratInstansi = [
                    'UploadSuratInstansi' => $datetime->format("Y-m-d H:i:s"),
                    'FileSuratInstansi' => $new_filename
                ];

                $this->db->update('PraktekKerjaLapangan', $requestUploadSuratInstansi, "Id = $PklId");

                redirect('pkl/PendaftaranPKL');
            }
        } else {
            // File upload failed
            $response = 'File upload failed: ' . $this->upload->display_errors();
        }
    }

    public function UploadSuratLamaran(){
        $uploadPath = './assets/uploads/berkasPendaftaran_pkl/';
        $config['upload_path'] = $uploadPath;
        $config['allowed_types'] = 'pdf|doc|docx';

        $PklId = $this->input->post('PklId');

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('lamaranPkl')) {
            $PKL = $this->db->get_where('PraktekKerjaLapangan', ['Id' => $PklId])->row_array();

            // File upload success
            $data = $this->upload->data();
            $uploaded_filename = $data['file_name'];
            $extention = pathinfo($uploaded_filename, PATHINFO_EXTENSION);
            $source_path = './assets/uploads/berkasPendaftaran_pkl/' . $uploaded_filename;
            $new_filename = $PklId.'-SuratLamaran-PKL.'.$extention; // Change this to your desired new name
            $new_path = './assets/uploads/berkasPendaftaran_pkl/' . $new_filename;

            if (rename($source_path, $new_path)) {
                if ($PKL['FileSuratLamaran'] != "" && $PKL['FileSuratLamaran'] != $new_filename && $PKL['FileSuratLamaran'] != null){
                    unlink('./assets/uploads/berkasPendaftaran_pkl/'. $PKL['FileSuratLamaran']);
                }

                $datetime = new DateTime;
                $timezone = new DateTimeZone('Asia/Jakarta'); 
        
                $datetime->setTimezone($timezone);

                $requestUploadSuratLamaran = [
                    'UploadSuratLamaran' => $datetime->format("Y-m-d H:i:s"),
                    'FileSuratLamaran' => $new_filename
                ];

                $this->db->update('PraktekKerjaLapangan', $requestUploadSuratLamaran, "Id = $PklId");

                redirect('pkl/PendaftaranPKL');
            }
        } else {
            // File upload failed
            $response = 'File upload failed: ' . $this->upload->display_errors();
        }
    }
    
    public function UploadCV(){
        $uploadPath = './assets/uploads/berkasPendaftaran_pkl/';
        $config['upload_path'] = $uploadPath;
        $config['allowed_types'] = 'pdf|doc|docx';

        $PklId = $this->input->post('PklId');

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('cvPkl')) {
            $PKL = $this->db->get_where('PraktekKerjaLapangan', ['Id' => $PklId])->row_array();

            // File upload success
            $data = $this->upload->data();
            $uploaded_filename = $data['file_name'];
            $extention = pathinfo($uploaded_filename, PATHINFO_EXTENSION);
            $source_path = './assets/uploads/berkasPendaftaran_pkl/' . $uploaded_filename;
            $new_filename = $PklId.'-CV-PKL.'.$extention; // Change this to your desired new name
            $new_path = './assets/uploads/berkasPendaftaran_pkl/' . $new_filename;

            if (rename($source_path, $new_path)) {
                if ($PKL['FileCVMahasiswa'] != "" && $PKL['FileCVMahasiswa'] != $new_filename && $PKL['FileCVMahasiswa'] != null){
                    unlink('./assets/uploads/berkasPendaftaran_pkl/'. $PKL['FileCVMahasiswa']);
                }

                $datetime = new DateTime;
                $timezone = new DateTimeZone('Asia/Jakarta'); 
        
                $datetime->setTimezone($timezone);

                $requestUploadCVMahasiswa = [
                    'UploadCVMahasiswa' => $datetime->format("Y-m-d H:i:s"),
                    'FileCVMahasiswa' => $new_filename
                ];

                $this->db->update('PraktekKerjaLapangan', $requestUploadCVMahasiswa, "Id = $PklId");

                redirect('pkl/PendaftaranPKL');
            }
        } else {
            // File upload failed
            $response = 'File upload failed: ' . $this->upload->display_errors();
        }
    }

    public function UploadProposal(){
        $uploadPath = './assets/uploads/berkasPendaftaran_pkl/';
        $config['upload_path'] = $uploadPath;
        $config['allowed_types'] = 'pdf|doc|docx';

        $PklId = $this->input->post('PklId');

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('proposalPkl')) {
            $PKL = $this->db->get_where('PraktekKerjaLapangan', ['Id' => $PklId])->row_array();

            // File upload success
            $data = $this->upload->data();
            $uploaded_filename = $data['file_name'];
            $extention = pathinfo($uploaded_filename, PATHINFO_EXTENSION);
            $source_path = './assets/uploads/berkasPendaftaran_pkl/' . $uploaded_filename;
            $new_filename = $PklId.'-Proposal-PKL.'.$extention; // Change this to your desired new name
            $new_path = './assets/uploads/berkasPendaftaran_pkl/' . $new_filename;

            if (rename($source_path, $new_path)) {
                if ($PKL['FileProposal'] != "" && $PKL['FileProposal'] != $new_filename && $PKL['FileProposal'] != null){
                    unlink('./assets/uploads/berkasPendaftaran_pkl/'. $PKL['FileProposal']);
                }

                $datetime = new DateTime;
                $timezone = new DateTimeZone('Asia/Jakarta'); 
        
                $datetime->setTimezone($timezone);

                $requestUploadProposal = [
                    'UploadProposal' => $datetime->format("Y-m-d H:i:s"),
                    'FileProposal' => $new_filename
                ];

                $this->db->update('PraktekKerjaLapangan', $requestUploadProposal, "Id = $PklId");

                redirect('pkl/PendaftaranPKL');
            }
        } else {
            // File upload failed
            $response = 'File upload failed: ' . $this->upload->display_errors();
        }
    }

    public function UploadSuratPengantar(){
        $uploadPath = './assets/uploads/berkasPendaftaran_pkl/';
        $config['upload_path'] = $uploadPath;
        $config['allowed_types'] = 'pdf|doc|docx';

        $PklId = $this->input->post('PklId');

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('pengantarPkl')) {
            $PKL = $this->db->get_where('PraktekKerjaLapangan', ['Id' => $PklId])->row_array();

            // File upload success
            $data = $this->upload->data();
            $uploaded_filename = $data['file_name'];
            $extention = pathinfo($uploaded_filename, PATHINFO_EXTENSION);
            $source_path = './assets/uploads/berkasPendaftaran_pkl/' . $uploaded_filename;
            $new_filename = $PklId.'-SuratPengantar-PKL.'.$extention; // Change this to your desired new name
            $new_path = './assets/uploads/berkasPendaftaran_pkl/' . $new_filename;

            if (rename($source_path, $new_path)) {
                if ($PKL['FileSuratPengantar'] != "" && $PKL['FileSuratPengantar'] != $new_filename && $PKL['FileSuratPengantar'] != null){
                    unlink('./assets/uploads/berkasPendaftaran_pkl/'. $PKL['FileSuratPengantar']);
                }

                $datetime = new DateTime;
                $timezone = new DateTimeZone('Asia/Jakarta'); 
        
                $datetime->setTimezone($timezone);

                $requestUploadSuratPengantar = [
                    'UploadSuratPengantar' => $datetime->format("Y-m-d H:i:s"),
                    'FileSuratPengantar' => $new_filename
                ];

                $this->db->update('PraktekKerjaLapangan', $requestUploadSuratPengantar, "Id = $PklId");

                redirect('pkl/PendaftaranPKL');
            }
        } else {
            // File upload failed
            $response = 'File upload failed: ' . $this->upload->display_errors();
        }
    }

    public function UploadTranskrip(){
        $uploadPath = './assets/uploads/berkasPendaftaran_pkl/';
        $config['upload_path'] = $uploadPath;
        $config['allowed_types'] = 'pdf|doc|docx';

        $PklId = $this->input->post('PklId');

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('TranskripPkl')) {
            $PKL = $this->db->get_where('PraktekKerjaLapangan', ['Id' => $PklId])->row_array();

            // File upload success
            $data = $this->upload->data();
            $uploaded_filename = $data['file_name'];
            $extention = pathinfo($uploaded_filename, PATHINFO_EXTENSION);
            $source_path = './assets/uploads/berkasPendaftaran_pkl/' . $uploaded_filename;
            $new_filename = $PklId.'-Transkrip-PKL.'.$extention; // Change this to your desired new name
            $new_path = './assets/uploads/berkasPendaftaran_pkl/' . $new_filename;

            if (rename($source_path, $new_path)) {
                if ($PKL['FileTranskrip'] != "" && $PKL['FileTranskrip'] != $new_filename && $PKL['FileTranskrip'] != null){
                    unlink('./assets/uploads/berkasPendaftaran_pkl/'. $PKL['FileTranskrip']);
                }

                $datetime = new DateTime;
                $timezone = new DateTimeZone('Asia/Jakarta'); 
        
                $datetime->setTimezone($timezone);

                $requestUploadTranskrip = [
                    'UploadTranskrip' => $datetime->format("Y-m-d H:i:s"),
                    'FileTranskrip' => $new_filename
                ];

                $this->db->update('PraktekKerjaLapangan', $requestUploadTranskrip, "Id = $PklId");

                redirect('pkl/PendaftaranPKL');
            }
        } else {
            // File upload failed
            $response = 'File upload failed: ' . $this->upload->display_errors();
        }
    }

    public function DetailPkl(){
        $PklId = $this->input->post('PklId');

        $queryPkl = "
                    select pkl.*,
                    smstr.Semester, smstr.TahunAkademik,
                    instansi.Nama AS NamaInstansi, instansi.Alamat AS AlamatInstansi, instansi.Email AS EmailInstansi, instansi.Telpon AS TelponInstansi,
                    mhs.Name AS NamaMahasiswa, mhs.Username,
                    (CASE WHEN pkl.DosenId IS NOT NULL OR pkl.DosenId != 0
                                THEN (SELECT Dosen.Name
                                        FROM Users Dosen
                                        WHERE Id = pkl.DosenId)
                                ELSE NULL END) AS DosenNama
                    from PraktekKerjaLapangan pkl
                    join Users mhs on mhs.Id = pkl.MhsId
                    join AkademikSemester smstr on smstr.Id = pkl.SemesterId
                    join AkademikInstansi instansi on instansi.Id = pkl.InstansiId
                    where pkl.Id = $PklId
                    ";
        $Pkl = $this->db->query($queryPkl)->row_array();

        switch ($Pkl["Status"]) {
            case 1:
                $Pkl["StatusTxt"] = "Diterima";
                break;
            case 0:
                $Pkl["StatusTxt"] = "Ditolak";
                break;
            case 2:
                $Pkl["StatusTxt"] = "Diproses";
                break;
            
            default:
                $Pkl["StatusTxt"] = "Diproses";
                break;
        }

        $Pkl['TanggalMulaiFormat'] = date("d-m-Y", strtotime($Pkl['TanggalMulai']));
        $Pkl['TanggalSelesaiFormat'] = date("d-m-Y", strtotime($Pkl['TanggalSelesai']));

        echo json_encode($Pkl);
    }

    public function EditPkl(){ 
        $PklId = htmlspecialchars($this->input->post('PklId'));

        $requestPKL = [
            'DosenId' => htmlspecialchars($this->input->post('dosenPkl')),
            'TanggalMulai' => htmlspecialchars($this->input->post('tanggalMulai')),
            'TanggalSelesai' => htmlspecialchars($this->input->post('tanggalSelesai')),
            'Status' => htmlspecialchars($this->input->post('status')),
        ];

        $this->db->update('PraktekKerjaLapangan', $requestPKL, "Id = $PklId");

        redirect('pkl/PendaftaranPKL');
    }

    /* Nilai Pembimbing Lapang */
    public function NilaiPembimbingLapang($PklId = null){
        $data['title'] = 'Nilai Pembimbing Lapang';

        $id = $this->session->userdata['Id'];
        $roleId = $this->session->userdata['RoleId'];

        $mhsRole = [2,3,4];
        $dsnRole = [1,6];
        if (in_array($roleId, $mhsRole)){
            $query = "
                        select mhs.*, 
                        usr.Id, usr.Name, usr.Username, usr.Telpon, usr.Email, usr.RoleId, usr.CreatedAt, usr.Status, usr.ImageProfile, usr.AlamatMalang, usr.Alamat
                        from Users usr
                        join UserMahasiswa mhs on usr.Id = mhs.UserId
                        where usr.Id = '$id' 
                        ";

        }else if(in_array($roleId, $dsnRole)){
            $query = "
                        select dsn.*, 
                        usr.Id, usr.Name, usr.Username, usr.Telpon, usr.Email, usr.RoleId, usr.CreatedAt, usr.Status, usr.ImageProfile, usr.AlamatMalang, usr.Alamat
                        from Users usr
                        join UserTenagaKependidikan dsn on usr.Id = dsn.UserId
                        where usr.Id = '$id'
                        ";
        }else{
            $query = "select * from Users where Id = '$id'";
        }

        $User = $this->db->query($query)->row_array();
        $data['User'] = $User;

        $queryPkl = "
                    select pkl.* 
                    from PraktekKerjaLapangan pkl
                    where pkl.Id = $PklId
                    ";
        $Pkl = $this->db->query($queryPkl)->row_array();

        $queryNilai = "
                    select nilai.PklId, nilai.LP_PKL_A, nilai.LP_PKL_B, nilai.LP_MK1, nilai.LP_MK2, nilai.LP_MK3 
                    from NilaiPembimbingLapangPKL nilai 
                    where PklId = $PklId";

        $NilaiPKL = $this->db->query($queryNilai)->row_array();

        if ($NilaiPKL != null){
            if ($Pkl['Id'] == $NilaiPKL['PklId']) {
                $Pkl = array_merge($Pkl, $NilaiPKL);
            }
        }else{
            $Pkl['LP_PKL_A'] = null;
            $Pkl['LP_PKL_B'] = null;
            $Pkl['LP_MK1'] = null;
            $Pkl['LP_MK2'] = null;
            $Pkl['LP_MK3'] = null;
        }

        switch($Pkl['MataKuliah1']){
            case 1:
                $Pkl['MataKuliah1Text'] = "Sumber Daya Alam";
                break;
            case 2:
                $Pkl['MataKuliah1Text'] = "Keselamatan dan Lingkungan";
                break;
            case 3:
                $Pkl['MataKuliah1Text'] = "Metode dan Terapan";
                break;
            case 4:
                $Pkl['MataKuliah1Text'] = "Analisis, Penilaian, dan Tindakan";
                break;
            case 5:
                $Pkl['MataKuliah1Text'] = "Kemandirian Pengayaan Pengetahuan";
                break;

            default :
                $Pkl['MataKuliah1Text'] = "-";
                break;
        }

        switch($Pkl['MataKuliah2']){
            case 1:
                $Pkl['MataKuliah2Text'] = "Sumber Daya Alam";
                break;
            case 2:
                $Pkl['MataKuliah2Text'] = "Keselamatan dan Lingkungan";
                break;
            case 3:
                $Pkl['MataKuliah2Text'] = "Metode dan Terapan";
                break;
            case 4:
                $Pkl['MataKuliah2Text'] = "Analisis, Penilaian, dan Tindakan";
                break;
            case 5:
                $Pkl['MataKuliah2Text'] = "Kemandirian Pengayaan Pengetahuan";
                break;

            default :
                $Pkl['MataKuliah2Text'] = "-";
                break;
        }

        switch($Pkl['MataKuliah3']){
            case 1:
                $Pkl['MataKuliah3Text'] = "Sumber Daya Alam";
                break;
            case 2:
                $Pkl['MataKuliah3Text'] = "Keselamatan dan Lingkungan";
                break;
            case 3:
                $Pkl['MataKuliah3Text'] = "Metode dan Terapan";
                break;
            case 4:
                $Pkl['MataKuliah3Text'] = "Analisis, Penilaian, dan Tindakan";
                break;
            case 5:
                $Pkl['MataKuliah3Text'] = "Kemandirian Pengayaan Pengetahuan";
                break;

            default :
                $Pkl['MataKuliah3Text'] = "-";
                break;
        }

        $data['PKL'] = $Pkl;

        /* Setting kembali */
        $data['url_kembali'] = get_referer_url();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('pkl/PembimbingLapangPKL', $data);
        $this->load->view('templates/footer');
    }

    public function UpdateJenisSks(){
        $PklId = $this->input->post('PklId');

        $requestData = [
            'JenisSks' => $this->input->post('jenisSks'),
            'MataKuliah1' => $this->input->post('mataKuliah1'),
            'MataKuliah2' => $this->input->post('mataKuliah2'),
            'MataKuliah3' => $this->input->post('mataKuliah3')
        ];

        switch($this->input->post('jenisSks')){
            case 4:
                $requestData['MataKuliah1'] = 0;
                $requestData['MataKuliah2'] = 0;
                $requestData['MataKuliah3'] = 0;
                break;
            case 7:
                $requestData['MataKuliah2'] = 0;
                $requestData['MataKuliah3'] = 0;
                break;
            case 10:
                $requestData['MataKuliah3'] = 0;
                break;
        }

        $this->db->update('PraktekKerjaLapangan', $requestData, "Id = $PklId");

        redirect('pkl/NilaiPembimbingLapang/'. $PklId);
    }

    public function NilaiPembimbingLapangLPA(){
        $PklId = $this->input->post('PklId');


        $requestLaporanAkhir = [
            '12_1_1' => $this->input->post("12_1_1"),
            '12_1_2' => $this->input->post("12_1_2"),
            '12_1_3' => $this->input->post("12_1_3"),
            '12_1_4' => $this->input->post("12_1_4"),

            '12_2_1' => $this->input->post("12_2_1"),
            '12_2_2' => $this->input->post("12_2_2"),
            '12_2_3' => $this->input->post("12_2_3"),
            '12_2_4' => $this->input->post("12_2_4"),

            '12_3_1' => $this->input->post("12_3_1"),
            '12_3_2' => $this->input->post("12_3_2"),

            '12_5_1' => $this->input->post("12_5_1"),
            '12_5_2' => $this->input->post("12_5_2"),
            
            '15_1_1' => $this->input->post("15_1_1"),
            '15_1_2' => $this->input->post("15_1_2"),

            '15_2_1' => $this->input->post("15_2_1"),
            '15_2_2' => $this->input->post("15_2_2"),

            '15_3_1' => $this->input->post("15_3_1"),
            '15_3_2' => $this->input->post("15_3_2"),

            '15_4_1' => $this->input->post("15_4_1"),
            '15_4_2' => $this->input->post("15_4_2"),

            '15_5_1' => $this->input->post("15_5_1"),
            '15_5_2' => $this->input->post("15_5_2"),
        ];
        
        $jsonNilai = json_encode($requestLaporanAkhir);

        $requestData = [
            'LP_PKL_A' => $jsonNilai
        ];

        /* cek data nilai */
        $queryCekData = "select * from NilaiPembimbingLapangPKL where PklId = $PklId";
        $dataLPA = $this->db->query($queryCekData)->row_array();

        if($dataLPA!=null){
            $this->db->update('NilaiPembimbingLapangPKL', $requestData, "PklId = $PklId");
        }else{
            $requestData['PklId'] = $PklId;
            $this->db->insert('NilaiPembimbingLapangPKL', $requestData);
        }

        redirect('pkl/NilaiPembimbingLapang/'. $PklId);
    }

    public function NilaiPembimbingLapangLPB(){
        $PklId = $this->input->post('PklId');

        $requestLaporanTengah = [
            '12_1_1' => $this->input->post("12_1_1"),
            '12_1_2' => $this->input->post("12_1_2"),
            '12_1_3' => $this->input->post("12_1_3"),
            '12_1_4' => $this->input->post("12_1_4"),

            '12_2_1' => $this->input->post("12_2_1"),
            '12_2_2' => $this->input->post("12_2_2"),
            '12_2_3' => $this->input->post("12_2_3"),
            '12_2_4' => $this->input->post("12_2_4"),

            '12_3_1' => $this->input->post("12_3_1"),
            '12_3_2' => $this->input->post("12_3_2"),

            '12_5_1' => $this->input->post("12_5_1"),
            '12_5_2' => $this->input->post("12_5_2"),
            
            '15_1_1' => $this->input->post("15_1_1"),
            '15_1_2' => $this->input->post("15_1_2"),

            '15_2_1' => $this->input->post("15_2_1"),
            '15_2_2' => $this->input->post("15_2_2"),

            '15_3_1' => $this->input->post("15_3_1"),
            '15_3_2' => $this->input->post("15_3_2"),

            '15_4_1' => $this->input->post("15_4_1"),
            '15_4_2' => $this->input->post("15_4_2"),

            '15_5_1' => $this->input->post("15_5_1"),
            '15_5_2' => $this->input->post("15_5_2"),
        ];
        
        $jsonNilai = json_encode($requestLaporanTengah);

        $requestData = [
            'LP_PKL_B' => $jsonNilai
        ];

        /* cek data nilai */
        $queryCekData = "select * from NilaiPembimbingLapangPKL where PklId = $PklId";
        $dataLPA = $this->db->query($queryCekData)->row_array();

        if($dataLPA!=null){
            $this->db->update('NilaiPembimbingLapangPKL', $requestData, "PklId = $PklId");
        }else{
            $requestData['PklId'] = $PklId;
            $this->db->insert('NilaiPembimbingLapangPKL', $requestData);
        }

        redirect('pkl/NilaiPembimbingLapang/'. $PklId);
    }

    public function NilaiPembimbingLapangMK1(){
        $PklId = $this->input->post('PklId');

        $requestMK1 = [
            '1' => $this->input->post("1"),
            '2' => $this->input->post("2"),
            '3' => $this->input->post("3"),
        ];
        
        $jsonNilai = json_encode($requestMK1);

        $requestData = [
            'LP_MK1' => $jsonNilai
        ];

        /* cek data nilai */
        $queryCekData = "select * from NilaiPembimbingLapangPKL where PklId = $PklId";
        $dataLPA = $this->db->query($queryCekData)->row_array();

        if($dataLPA!=null){
            $this->db->update('NilaiPembimbingLapangPKL', $requestData, "PklId = $PklId");
        }else{
            $requestData['PklId'] = $PklId;
            $this->db->insert('NilaiPembimbingLapangPKL', $requestData);
        }

        redirect('pkl/NilaiPembimbingLapang/'. $PklId);
    }

    public function NilaiPembimbingLapangMK2(){
        $PklId = $this->input->post('PklId');

        $requestMK1 = [
            '1' => $this->input->post("1"),
            '2' => $this->input->post("2"),
            '3' => $this->input->post("3"),
        ];
        
        $jsonNilai = json_encode($requestMK1);

        $requestData = [
            'LP_MK2' => $jsonNilai
        ];

        /* cek data nilai */
        $queryCekData = "select * from NilaiPembimbingLapangPKL where PklId = $PklId";
        $dataLPA = $this->db->query($queryCekData)->row_array();

        if($dataLPA!=null){
            $this->db->update('NilaiPembimbingLapangPKL', $requestData, "PklId = $PklId");
        }else{
            $requestData['PklId'] = $PklId;
            $this->db->insert('NilaiPembimbingLapangPKL', $requestData);
        }

        redirect('pkl/NilaiPembimbingLapang/'. $PklId);
    }

    public function NilaiPembimbingLapangMK3(){
        $PklId = $this->input->post('PklId');

        $requestMK1 = [
            '1' => $this->input->post("1"),
            '2' => $this->input->post("2"),
            '3' => $this->input->post("3"),
        ];
        
        $jsonNilai = json_encode($requestMK1);

        $requestData = [
            'LP_MK3' => $jsonNilai
        ];

        /* cek data nilai */
        $queryCekData = "select * from NilaiPembimbingLapangPKL where PklId = $PklId";
        $dataLPA = $this->db->query($queryCekData)->row_array();

        if($dataLPA!=null){
            $this->db->update('NilaiPembimbingLapangPKL', $requestData, "PklId = $PklId");
        }else{
            $requestData['PklId'] = $PklId;
            $this->db->insert('NilaiPembimbingLapangPKL', $requestData);
        }

        redirect('pkl/NilaiPembimbingLapang/'. $PklId);
    }

    public function ViewNilaiPembimbingLapangLPA(){
        $PklId = $this->input->post('PklId');

        $query = "
                select LP_PKL_A from NilaiPembimbingLapangPKL where PklId = $PklId
                ";
        
        $LPA = $this->db->query($query)->row_array();

        echo json_encode($LPA);
    }

    public function ViewNilaiPembimbingLapangLPB(){
        $PklId = $this->input->post('PklId');

        $query = "
                select LP_PKL_B from NilaiPembimbingLapangPKL where PklId = $PklId
                ";
        
        $LPA = $this->db->query($query)->row_array();

        echo json_encode($LPA);
    }

    public function ViewNilaiPembimbingLapangMK1(){
        $PklId = $this->input->post('PklId');

        $query = "
                select nilai.LP_MK1, pkl.MataKuliah1
                from NilaiPembimbingLapangPKL nilai
                join PraktekKerjaLapangan pkl on nilai.PklId = pkl.Id
                where PklId = $PklId
                ";
        
        $MK1 = $this->db->query($query)->row_array();

        echo json_encode($MK1);
    }

    public function ViewNilaiPembimbingLapangMK2(){
        $PklId = $this->input->post('PklId');

        $query = "
                select nilai.LP_MK2, pkl.MataKuliah2
                from NilaiPembimbingLapangPKL nilai
                join PraktekKerjaLapangan pkl on nilai.PklId = pkl.Id
                where PklId = $PklId
                ";
        
        $MK2 = $this->db->query($query)->row_array();

        echo json_encode($MK2);
    }

    /* Ujian PKL */
    public function PendaftaranUjianPKL($id = null){
        $data['title'] = 'Pendaftaran Ujian Praktek Kerja Lapangan';
        if ($id == null){
            $id = $this->session->userdata['Id'];
        }

        $roleId = $this->session->userdata['RoleId'];

        $kadep = $this->session->userdata['Kadep'];
        $sekdep = $this->session->userdata['Sekdep'];
        $kaprodiS1 = $this->session->userdata['KaprodiS1'];
        $kaprodiS2 = $this->session->userdata['KaprodiS2'];
        $kaprodiS3 = $this->session->userdata['KaprodiS3'];

        $mhsRole = [2,3,4];
        $dsnRole = [1,6];
        if (in_array($roleId, $mhsRole)){
            $query = "
                        select mhs.*, 
                        usr.Id, usr.Name, usr.Username, usr.Telpon, usr.Email, usr.RoleId, usr.CreatedAt, usr.Status, usr.ImageProfile, usr.AlamatMalang, usr.Alamat
                        from Users usr
                        join UserMahasiswa mhs on usr.Id = mhs.UserId
                        where usr.Id = '$id'
                        ";

            /* Ambil Data PKL Berdasarkan Mahasiswa */
            $queryPKL = "
                        select pkl.*,
                        usr.Name, usr.Username,
                        dsn.Name as NamaDosen
                        from PraktekKerjaLapangan pkl
                        join Users usr on usr.Id = pkl.MhsId
                        join Users dsn on dsn.Id = pkl.DosenId
                        where pkl.MhsId = $id and pkl.Status = 1
                        Order By pkl.CreatedAt Desc
                        ";

            $PKL = $this->db->query($queryPKL)->result_array();

            if ($PKL != null) {
                $PKLIds = [];
                foreach ($PKL as $item) {
                    $PKLIds[] = $item["Id"];
                }
                $values = array_map('strval', $PKLIds);
                $encodedString = "(" . implode(",", $values) . ")";

                $queryUjianPKL = "
                            select ujianpkl.PklId, ujianpkl.TanggalUjian, ujianpkl.JamMulai, ujianpkl.JamSelesai, ujianpkl.RuangId, ujianpkl.LinkZoom, ujianpkl.FileDaftarHadir, ujianpkl.UploadDaftarHadir, ujianpkl.FileKartuBimbingan, ujianpkl.UploadKartuBimbingan, ujianpkl.FileLembarPersetujuan, ujianpkl.UploadLembarPersetujuan, ujianpkl.FilePenilaianPembimbing, ujianpkl.UploadPenilaianPembimbing, ujianpkl.FilePresentasi, ujianpkl.UploadPresentasi, ujianpkl.FileLaporanPKL, ujianpkl.UploadLaporanPKL,
                            (CASE WHEN ujianpkl.RuangId IS NOT NULL
                            THEN (SELECT Nama
                                    FROM AkademikRuangan
                                    WHERE Id = ujianpkl.RuangId)
                            ELSE NULL END) AS RuangNama
                            from UjianPKL ujianpkl
                            where PklId in $encodedString
                        ";

                $UjianPKL = $this->db->query($queryUjianPKL)->result_array();
                if (count($UjianPKL) > 0) {
                    /* Menggabungkan Data UjianPKL Dengan Data TA */
                    foreach ($PKL as $key => $dataPKL) {
                        foreach ($UjianPKL as $dataUjianPKL) {
                            if ($dataPKL['Id'] == $dataUjianPKL['PklId']) {
                                if ($dataUjianPKL['FileDaftarHadir'] != null && $dataUjianPKL['FileKartuBimbingan'] != null && $dataUjianPKL['FileLembarPersetujuan'] != null && $dataUjianPKL['FilePenilaianPembimbing'] != null && $dataUjianPKL['FilePresentasi'] != null && $dataUjianPKL['FileLaporanPKL'] != null ){
                                    $dataUjianPKL['Uploaded'] = true;
                                    if ($dataUjianPKL["TanggalUjian"] != null && $dataUjianPKL["JamMulai"] != null && $dataUjianPKL["JamSelesai"] != null){
                                        $dataUjianPKL["Registered"] = true;
                                    }else{
                                        $dataUjianPKL["Registered"] = false;
                                    }
                                }else{
                                    $dataUjianPKL['Uploaded'] = false;
                                }
                                $PKL[$key] = array_merge($dataPKL, $dataUjianPKL);
                                break;
                            }
                        }
                    }
                }else{
                    foreach ($PKL as $key => $dataPKL) {
                        $PKL[$key]["Uploaded"] = false;
                    }
                }
            }
        }else if(in_array($roleId, $dsnRole) && ($kadep == false && $sekdep == false && $kaprodiS1 == false && $kaprodiS2 == false && $kaprodiS3 == false)){
            $query = "
                        select dsn.*, 
                        usr.Id, usr.Name, usr.Username, usr.Telpon, usr.Email, usr.RoleId, usr.CreatedAt, usr.Status, usr.ImageProfile, usr.AlamatMalang, usr.Alamat
                        from Users usr
                        join UserTenagaKependidikan dsn on usr.Id = dsn.UserId
                        where usr.Id = '$id'
                        ";

            /* Ambil Data PKL berdasarkan Mahasiswa */
            $queryPKL = "
                        select pkl.*,
                        usr.Name, usr.Username,
                        instansi.Nama as NamaInstansi, 
                        instansi.Alamat as AlamatInstansi,
                        (CASE WHEN pkl.DosenId IS NOT NULL OR pkl.DosenId != 0
                        THEN (SELECT Dosen.Name
                                FROM Users Dosen
                                WHERE Id = pkl.DosenId)
                        ELSE NULL END) AS NamaDosen
                        from PraktekKerjaLapangan pkl
                        join Users usr on usr.Id = pkl.MhsId
                        join AkademikInstansi instansi on instansi.Id = pkl.InstansiId
                        where pkl.DosenId = $id AND pkl.Status = 1
                        ";

            if($this->input->post('jenjang') != NULL){
                $jenjang = $this->input->post('jenjang');
                $queryPKL = $queryPKL . " AND RoleId = '$jenjang'";
            }else{
                $jenjang = 2;
                $queryPKL = $queryPKL . " AND RoleId = '$jenjang'";
            }

            if($this->input->post('search') != NULL){
                $search = $this->input->post('search');
                $queryPKL = $queryPKL . " AND usr.Name LIKE '%$search%' OR usr.Username LIKE '%$search%'"; 
            }

            if($this->input->post('limit') != NULL){
                $limit = $this->input->post('limit');
                $queryPKL = $queryPKL . " limit $limit"; 
            }

            $queryPKL = $queryPKL . " Order By pkl.CreatedAt Desc";

            $PKL = $this->db->query($queryPKL)->result_array();
            if ($PKL != null) {
                $PKLIds = [];
                foreach ($PKL as $item) {
                    $PKLIds[] = $item["Id"];
                }
                $values = array_map('strval', $PKLIds);
                $encodedString = "(" . implode(",", $values) . ")";

                $queryUjianPKL = "
                            select  ujianpkl.PklId, ujianpkl.TanggalUjian, ujianpkl.JamMulai, ujianpkl.JamSelesai, ujianpkl.RuangId, ujianpkl.LinkZoom, ujianpkl.FileDaftarHadir, ujianpkl.UploadDaftarHadir, ujianpkl.FileKartuBimbingan, ujianpkl.UploadKartuBimbingan, ujianpkl.FileLembarPersetujuan, ujianpkl.UploadLembarPersetujuan, ujianpkl.FilePenilaianPembimbing, ujianpkl.UploadPenilaianPembimbing, ujianpkl.FilePresentasi, ujianpkl.UploadPresentasi, ujianpkl.FileLaporanPKL, ujianpkl.UploadLaporanPKL,
                            (CASE WHEN ujianpkl.RuangId IS NOT NULL
                            THEN (SELECT Nama
                                    FROM AkademikRuangan
                                    WHERE Id = ujianpkl.RuangId)
                            ELSE NULL END) AS RuangNama
                            from UjianPKL ujianpkl
                            where PklId in $encodedString
                        ";

                $UjianPKL = $this->db->query($queryUjianPKL)->result_array();
                if (count($UjianPKL) > 0) {
                    /* Menggabungkan Data UjianPKL Dengan Data TA */
                    foreach ($PKL as $key => $dataPKL) {
                        foreach ($UjianPKL as $dataUjianPKL) {
                            if ($dataPKL['Id'] == $dataUjianPKL['PklId']) {
                                $PKL[$key] = array_merge($dataPKL, $dataUjianPKL);
                                break;
                            }
                        }
                    }
                }
            }
        }else{
            $query = "select * from Users where Id = '$id'";
            
            /* Ambil Data PKL berdasarkan Mahasiswa */
            $queryPKL = "
                        select pkl.*,
                        usr.Name, usr.Username,
                        instansi.Nama as NamaInstansi, 
                        instansi.Alamat as AlamatInstansi,
                        dsn.Name as NamaDosen
                        from PraktekKerjaLapangan pkl
                        join Users usr on usr.Id = pkl.MhsId
                        join Users dsn on dsn.Id = pkl.DosenId
                        join AkademikInstansi instansi on instansi.Id = pkl.InstansiId
                        where pkl.Status = 1
                        ";

            if($this->input->post('jenjang') != NULL){
                $jenjang = $this->input->post('jenjang');
                $queryPKL = $queryPKL . " AND usr.RoleId = '$jenjang'";
            }else{
                $jenjang = 2;
                $queryPKL = $queryPKL . " AND usr.RoleId = '$jenjang'";
            }

            if($this->input->post('search') != NULL){
                $search = $this->input->post('search');
                $queryPKL = $queryPKL . " AND usr.Name LIKE '%$search%' OR usr.Username LIKE '%$search%'"; 
            }

            if($this->input->post('limit') != NULL){
                $limit = $this->input->post('limit');
                $queryPKL = $queryPKL . " limit $limit"; 
            }

            $queryPKL = $queryPKL . " Order By pkl.CreatedAt Desc";
            
            $PKL = $this->db->query($queryPKL)->result_array();

            if ($PKL != null) {
                $PKLIds = [];
                foreach ($PKL as $item) {
                    $PKLIds[] = $item["Id"];
                }
                $values = array_map('strval', $PKLIds);
                $encodedString = "(" . implode(",", $values) . ")";

                $queryUjianPKL = "
                            select  ujianpkl.PklId, ujianpkl.TanggalUjian, ujianpkl.JamMulai, ujianpkl.JamSelesai, ujianpkl.RuangId, ujianpkl.LinkZoom, ujianpkl.FileDaftarHadir, ujianpkl.UploadDaftarHadir, ujianpkl.FileKartuBimbingan, ujianpkl.UploadKartuBimbingan, ujianpkl.FileLembarPersetujuan, ujianpkl.UploadLembarPersetujuan, ujianpkl.FilePenilaianPembimbing, ujianpkl.UploadPenilaianPembimbing, ujianpkl.FilePresentasi, ujianpkl.UploadPresentasi, ujianpkl.FileLaporanPKL, ujianpkl.UploadLaporanPKL,
                            (CASE WHEN ujianpkl.RuangId IS NOT NULL
                            THEN (SELECT Nama
                                    FROM AkademikRuangan
                                    WHERE Id = ujianpkl.RuangId)
                            ELSE NULL END) AS RuangNama
                            from UjianPKL ujianpkl
                            where PklId in $encodedString
                        ";

                $UjianPKL = $this->db->query($queryUjianPKL)->result_array();
                if (count($UjianPKL) > 0) {
                    /* Menggabungkan Data UjianPKL Dengan Data TA */
                    foreach ($PKL as $key => $dataPKL) {
                        foreach ($UjianPKL as $dataUjianPKL) {
                            if ($dataPKL['Id'] == $dataUjianPKL['PklId']) {
                                if ($dataUjianPKL['FileDaftarHadir'] != null && $dataUjianPKL['FileKartuBimbingan'] != null && $dataUjianPKL['FileLembarPersetujuan'] != null && $dataUjianPKL['FilePenilaianPembimbing'] != null && $dataUjianPKL['FilePresentasi'] != null && $dataUjianPKL['FileLaporanPKL'] != null ){
                                    $dataUjianPKL['Uploaded'] = true;
                                    if ($dataUjianPKL["TanggalUjian"] != null && $dataUjianPKL["JamMulai"] != null && $dataUjianPKL["JamSelesai"] != null){
                                        $dataUjianPKL["Registered"] = true;
                                    }else{
                                        $dataUjianPKL["Registered"] = false;
                                    }
                                }else{
                                    $dataUjianPKL['Uploaded'] = false;
                                }
                                $PKL[$key] = array_merge($dataPKL, $dataUjianPKL);
                                break;
                            }
                        }
                    }
                }else{
                    foreach ($PKL as $key => $dataPKL) {
                        $PKL[$key]["Uploaded"] = false;
                    }
                }
            }
        }

        $User = $this->db->query($query)->row_array();
        $data['User'] = $User;

        /* Ambil List Dosen */
        $queyrDosen = "select *
                        from Users
                        where RoleId in ('1')
                        ";

        $Dosen = $this->db->query($queyrDosen)->result_array();
        $data['Dosen'] = $Dosen;
        
        /* Ambil List Ruangan */
        $queryRuangan = "select *
                        from AkademikRuangan
                        where Status = 1 AND IsDeleted = 0
                        ";
        $Ruangan = $this->db->query($queryRuangan)->result_array();
        $data['Ruangan'] = $Ruangan;

        $data['PendaftaranPKL'] = $PKL;

        /* Setting kembali */
        $data['url_kembali'] = get_referer_url();


        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('pkl/PendaftaranUjianPKL', $data);
        $this->load->view('templates/footer');
    }

    public function DaftarUjianPKL(){
        $PklId = htmlspecialchars($this->input->post("PklId"));
        $datetime = new DateTime();
        $timezone = new DateTimeZone('Asia/Jakarta'); 

        $datetime->setTimezone($timezone);

        $requestUjianPKL = [
            "PklId" => $PklId,
            "TanggalUjian" => htmlspecialchars($this->input->post("tanggalUjian")),
            "JamMulai" => htmlspecialchars($this->input->post("jamMulai")),
            "JamSelesai" => htmlspecialchars($this->input->post("jamSelesai")),
            "RuangId" => htmlspecialchars($this->input->post("ruangId")),
            "LinkZoom" => htmlspecialchars($this->input->post("linkZoom")),
            "CreatedAt" => $datetime->format("Y-m-d H:i:s"),
        ];

        /* Cek Data PKL */
        $queryPKL = "select * from UjianPKL where PklId = $PklId";

        $PKL = $this->db->query($queryPKL)->row_array();
        if ($PKL != null){
            $this->db->update('UjianPKL', $requestUjianPKL, "PklId = $PklId");
        }else{
            $this->db->insert('UjianPKL', $requestUjianPKL);
        }

        redirect('pkl/PendaftaranUjianPKL');

    }

    public function DetailUjianPKL(){

        $PklId = $this->input->post('PklId');

        $queryUjianPkl = "
                    select *
                    from UjianPKL
                    where PklId = $PklId
                    ";
        $UjianPkl = $this->db->query($queryUjianPkl)->row_array();

        echo json_encode($UjianPkl);
    }

    public function UploadDaftarHadirUjianPKL(){
        $uploadPath = './assets/uploads/berkasUjian_pkl/';
        $config['upload_path'] = $uploadPath;
        $config['allowed_types'] = 'pdf|doc|docx';

        $PklId = $this->input->post('PklId');

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('daftarHadirUjianPKL')) {
            $PKL = $this->db->get_where('UjianPKL', ['PklId' => $PklId])->row_array();

            // File upload success
            $data = $this->upload->data();
            $uploaded_filename = $data['file_name'];
            $extention = pathinfo($uploaded_filename, PATHINFO_EXTENSION);
            $source_path = './assets/uploads/berkasUjian_pkl/' . $uploaded_filename;
            $new_filename = $PklId.'-DaftarHadirUjianPKL.'.$extention; // Change this to your desired new name
            $new_path = './assets/uploads/berkasUjian_pkl/' . $new_filename;

            if (rename($source_path, $new_path)) {
                if ($PKL['FileDaftarHadir'] != "" && $PKL['FileDaftarHadir'] != $new_filename && $PKL['FileDaftarHadir'] != null){
                    unlink('./assets/uploads/berkasUjian_pkl/'. $PKL['FileDaftarHadir']);
                }

                $datetime = new DateTime;
                $timezone = new DateTimeZone('Asia/Jakarta'); 
        
                $datetime->setTimezone($timezone);

                $requestUploadDaftarHadirUjianPKL = [
                    'UploadDaftarHadir' => $datetime->format("Y-m-d H:i:s"),
                    'FileDaftarHadir' => $new_filename
                ];

                if ($PKL != null){
                    $this->db->update('UjianPKL', $requestUploadDaftarHadirUjianPKL, "PklId = $PklId");
                }else{
                    $requestUploadDaftarHadirUjianPKL["PklId"] = $PklId;
                    $this->db->insert('UjianPKL',$requestUploadDaftarHadirUjianPKL);
                }

                $UserId = $this->session->userdata('UserId');

                redirect('pkl/PendaftaranUjianPKL/'.$UserId);
            }

            // $response = 'File uploaded successfully: ' . $data['file_name'];
        } else {
            // File upload failed
            $response = 'File upload failed: ' . $this->upload->display_errors();
        }
    }

    public function UploadKartuBimbinganUjianPKL(){
        $uploadPath = './assets/uploads/berkasUjian_pkl/';
        $config['upload_path'] = $uploadPath;
        $config['allowed_types'] = 'pdf|doc|docx';

        $PklId = $this->input->post('PklId');

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('kartuBimbinganUjianPKL')) {
            $PKL = $this->db->get_where('UjianPKL', ['PklId' => $PklId])->row_array();

            // File upload success
            $data = $this->upload->data();
            $uploaded_filename = $data['file_name'];
            $extention = pathinfo($uploaded_filename, PATHINFO_EXTENSION);
            $source_path = './assets/uploads/berkasUjian_pkl/' . $uploaded_filename;
            $new_filename = $PklId.'-KartuBimbinganUjianPKL.'.$extention; // Change this to your desired new name
            $new_path = './assets/uploads/berkasUjian_pkl/' . $new_filename;

            if (rename($source_path, $new_path)) {
                if ($PKL['FileKartuBimbingan'] != "" && $PKL['FileKartuBimbingan'] != $new_filename && $PKL['FileKartuBimbingan'] != null){
                    unlink('./assets/uploads/berkasUjian_pkl/'. $PKL['FileKartuBimbingan']);
                }

                $datetime = new DateTime;
                $timezone = new DateTimeZone('Asia/Jakarta'); 
        
                $datetime->setTimezone($timezone);

                $requestUploadKartuBimbinganUjianPKL = [
                    'UploadKartuBimbingan' => $datetime->format("Y-m-d H:i:s"),
                    'FileKartuBimbingan' => $new_filename
                ];

                if ($PKL != null){
                    $this->db->update('UjianPKL', $requestUploadKartuBimbinganUjianPKL, "PklId = $PklId");
                }else{
                    $requestUploadKartuBimbinganUjianPKL["PklId"] = $PklId;
                    $this->db->insert('UjianPKL',$requestUploadKartuBimbinganUjianPKL);
                }
                $UserId = $this->session->userdata('UserId');

                $this->PendaftaranUjianPKL($UserId);
            }

            // $response = 'File uploaded successfully: ' . $data['file_name'];
        } else {
            // File upload failed
            $response = 'File upload failed: ' . $this->upload->display_errors();
        }
    }

    public function UploadLembarPersetujuanUjianPKL(){
        $uploadPath = './assets/uploads/berkasUjian_pkl/';
        $config['upload_path'] = $uploadPath;
        $config['allowed_types'] = 'pdf|doc|docx';

        $PklId = $this->input->post('PklId');

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('lembarPersetujuanUjianPKL')) {
            $PKL = $this->db->get_where('UjianPKL', ['PklId' => $PklId])->row_array();

            // File upload success
            $data = $this->upload->data();
            $uploaded_filename = $data['file_name'];
            $extention = pathinfo($uploaded_filename, PATHINFO_EXTENSION);
            $source_path = './assets/uploads/berkasUjian_pkl/' . $uploaded_filename;
            $new_filename = $PklId.'-LembarPersetujuanUjianPKL.'.$extention; // Change this to your desired new name
            $new_path = './assets/uploads/berkasUjian_pkl/' . $new_filename;

            if (rename($source_path, $new_path)) {
                if ($PKL['FileLembarPersetujuan'] != "" && $PKL['FileLembarPersetujuan'] != $new_filename && $PKL['FileLembarPersetujuan'] != null){
                    unlink('./assets/uploads/berkasUjian_pkl/'. $PKL['FileLembarPersetujuan']);
                }

                $datetime = new DateTime;
                $timezone = new DateTimeZone('Asia/Jakarta'); 
        
                $datetime->setTimezone($timezone);

                $requestUploadLembarPersetujuanUjianPKL = [
                    'UploadLembarPersetujuan' => $datetime->format("Y-m-d H:i:s"),
                    'FileLembarPersetujuan' => $new_filename
                ];

                if ($PKL != null){
                    $this->db->update('UjianPKL', $requestUploadLembarPersetujuanUjianPKL, "PklId = $PklId");
                }else{
                    $requestUploadLembarPersetujuanUjianPKL["PklId"] = $PklId;
                    $this->db->insert('UjianPKL',$requestUploadLembarPersetujuanUjianPKL);
                }
                $UserId = $this->session->userdata('UserId');

                $this->PendaftaranUjianPKL($UserId);
            }

            // $response = 'File uploaded successfully: ' . $data['file_name'];
        } else {
            // File upload failed
            $response = 'File upload failed: ' . $this->upload->display_errors();
        }
    }

    public function UploadPenilaianPembimbingUjianPKL(){
        $uploadPath = './assets/uploads/berkasUjian_pkl/';
        $config['upload_path'] = $uploadPath;
        $config['allowed_types'] = 'pdf|doc|docx';

        $PklId = $this->input->post('PklId');

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('penilaianPembimbingUjianPKL')) {
            $PKL = $this->db->get_where('UjianPKL', ['PklId' => $PklId])->row_array();

            // File upload success
            $data = $this->upload->data();
            $uploaded_filename = $data['file_name'];
            $extention = pathinfo($uploaded_filename, PATHINFO_EXTENSION);
            $source_path = './assets/uploads/berkasUjian_pkl/' . $uploaded_filename;
            $new_filename = $PklId.'-PenilaianPembimbingUjianPKL.'.$extention; // Change this to your desired new name
            $new_path = './assets/uploads/berkasUjian_pkl/' . $new_filename;

            if (rename($source_path, $new_path)) {
                if ($PKL['FilePenilaianPembimbing'] != "" && $PKL['FilePenilaianPembimbing'] != $new_filename && $PKL['FilePenilaianPembimbing'] != null){
                    unlink('./assets/uploads/berkasUjian_pkl/'. $PKL['FilePenilaianPembimbing']);
                }

                $datetime = new DateTime;
                $timezone = new DateTimeZone('Asia/Jakarta'); 
        
                $datetime->setTimezone($timezone);

                $requestUploadPenilaianPembimbingUjianPKL = [
                    'UploadPenilaianPembimbing' => $datetime->format("Y-m-d H:i:s"),
                    'FilePenilaianPembimbing' => $new_filename
                ];

                if ($PKL != null){
                    $this->db->update('UjianPKL', $requestUploadPenilaianPembimbingUjianPKL, "PklId = $PklId");
                }else{
                    $requestUploadPenilaianPembimbingUjianPKL["PklId"] = $PklId;
                    $this->db->insert('UjianPKL',$requestUploadPenilaianPembimbingUjianPKL);
                }

                $UserId = $this->session->userdata('UserId');

                $this->PendaftaranUjianPKL($UserId);
            }

            // $response = 'File uploaded successfully: ' . $data['file_name'];
        } else {
            // File upload failed
            $response = 'File upload failed: ' . $this->upload->display_errors();
        }
    }

    public function UploadPresentasiUjianPKL(){
        $uploadPath = './assets/uploads/berkasUjian_pkl/';
        $config['upload_path'] = $uploadPath;
        $config['allowed_types'] = 'ppt|pptx';

        $PklId = $this->input->post('PklId');

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('presentasiUjianPKL')) {
            $PKL = $this->db->get_where('UjianPKL', ['PklId' => $PklId])->row_array();

            // File upload success
            $data = $this->upload->data();
            $uploaded_filename = $data['file_name'];
            $extention = pathinfo($uploaded_filename, PATHINFO_EXTENSION);
            $source_path = './assets/uploads/berkasUjian_pkl/' . $uploaded_filename;
            $new_filename = $PklId.'-PresentasiUjianPKL.'.$extention; // Change this to your desired new name
            $new_path = './assets/uploads/berkasUjian_pkl/' . $new_filename;

            if (rename($source_path, $new_path)) {
                if ($PKL['FilePresentasi'] != "" && $PKL['FilePresentasi'] != $new_filename && $PKL['FilePresentasi'] != null){
                    unlink('./assets/uploads/berkasUjian_pkl/'. $PKL['FilePresentasi']);
                }

                $datetime = new DateTime;
                $timezone = new DateTimeZone('Asia/Jakarta'); 
        
                $datetime->setTimezone($timezone);

                $requestUploadPresentasiUjianPKL = [
                    'UploadPresentasi' => $datetime->format("Y-m-d H:i:s"),
                    'FilePresentasi' => $new_filename
                ];

                if ($PKL != null){
                    $this->db->update('UjianPKL', $requestUploadPresentasiUjianPKL, "PklId = $PklId");
                }else{
                    $requestUploadPresentasiUjianPKL["PklId"] = $PklId;
                    $this->db->insert('UjianPKL',$requestUploadPresentasiUjianPKL);
                }
                $UserId = $this->session->userdata('UserId');

                $this->PendaftaranUjianPKL($UserId);
            }

            // $response = 'File uploaded successfully: ' . $data['file_name'];
        } else {
            // File upload failed
            $response = 'File upload failed: ' . $this->upload->display_errors();
        }
    }

    public function UploadLaporanPKLUjianPKL(){
        $uploadPath = './assets/uploads/berkasUjian_pkl/';
        $config['upload_path'] = $uploadPath;
        $config['allowed_types'] = 'pdf|doc|docx';

        $PklId = $this->input->post('PklId');

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('laporanPKLUjianPKL')) {
            $PKL = $this->db->get_where('UjianPKL', ['PklId' => $PklId])->row_array();

            // File upload success
            $data = $this->upload->data();
            $uploaded_filename = $data['file_name'];
            $extention = pathinfo($uploaded_filename, PATHINFO_EXTENSION);
            $source_path = './assets/uploads/berkasUjian_pkl/' . $uploaded_filename;
            $new_filename = $PklId.'-LaporanPKLUjianPKL.'.$extention; // Change this to your desired new name
            $new_path = './assets/uploads/berkasUjian_pkl/' . $new_filename;

            if (rename($source_path, $new_path)) {
                if ($PKL['FileLaporanPKL'] != "" && $PKL['FileLaporanPKL'] != $new_filename && $PKL['FileLaporanPKL'] != null){
                    unlink('./assets/uploads/berkasUjian_pkl/'. $PKL['FileLaporanPKL']);
                }

                $datetime = new DateTime;
                $timezone = new DateTimeZone('Asia/Jakarta'); 
        
                $datetime->setTimezone($timezone);

                $requestUploadLaporanPKLUjianPKL = [
                    'UploadLaporanPKL' => $datetime->format("Y-m-d H:i:s"),
                    'FileLaporanPKL' => $new_filename
                ];

                if ($PKL != null){
                    $this->db->update('UjianPKL', $requestUploadLaporanPKLUjianPKL, "PklId = $PklId");
                }else{
                    $requestUploadLaporanPKLUjianPKL["PklId"] = $PklId;
                    $this->db->insert('UjianPKL',$requestUploadLaporanPKLUjianPKL);
                }
                $UserId = $this->session->userdata('UserId');

                $this->PendaftaranUjianPKL($UserId);
            }

            // $response = 'File uploaded successfully: ' . $data['file_name'];
        } else {
            // File upload failed
            $response = 'File upload failed: ' . $this->upload->display_errors();
        }
    }

    /* Nilai Dosen Pembimbing */
    public function NilaiDosenPembimbing($PklId = null){
        $data['title'] = 'Nilai Pembimbing Lapang';

        $id = $this->session->userdata['Id'];
        $roleId = $this->session->userdata['RoleId'];

        $mhsRole = [2,3,4];
        $dsnRole = [1,6];
        if (in_array($roleId, $mhsRole)){
            $query = "
                        select mhs.*, 
                        usr.Id, usr.Name, usr.Username, usr.Telpon, usr.Email, usr.RoleId, usr.CreatedAt, usr.Status, usr.ImageProfile, usr.AlamatMalang, usr.Alamat
                        from Users usr
                        join UserMahasiswa mhs on usr.Id = mhs.UserId
                        where usr.Id = '$id' 
                        ";

        }else if(in_array($roleId, $dsnRole)){
            $query = "
                        select dsn.*, 
                        usr.Id, usr.Name, usr.Username, usr.Telpon, usr.Email, usr.RoleId, usr.CreatedAt, usr.Status, usr.ImageProfile, usr.AlamatMalang, usr.Alamat
                        from Users usr
                        join UserTenagaKependidikan dsn on usr.Id = dsn.UserId
                        where usr.Id = '$id'
                        ";
        }else{
            $query = "select * from Users where Id = '$id'";
        }

        $User = $this->db->query($query)->row_array();
        $data['User'] = $User;

        $queryPkl = "
                    select pkl.*
                    from PraktekKerjaLapangan pkl
                    where pkl.Id = $PklId
                    ";
        $Pkl = $this->db->query($queryPkl)->row_array();

        $queryNilai = "
                    select nilai.PklId, nilai.LP_PKL_A, nilai.LP_PKL_B, nilai.LP_MK1, nilai.LP_MK2, nilai.LP_MK3 
                    from NilaiDosenPembimbingPKL nilai 
                    where PklId = $PklId";

        $NilaiPKL = $this->db->query($queryNilai)->row_array();

        if ($NilaiPKL != null){
            if ($Pkl['Id'] == $NilaiPKL['PklId']) {
                $Pkl = array_merge($Pkl, $NilaiPKL);
            }
        }else{
            $Pkl['LP_PKL_A'] = null;
            $Pkl['LP_PKL_B'] = null;
            $Pkl['LP_MK1'] = null;
            $Pkl['LP_MK2'] = null;
            $Pkl['LP_MK3'] = null;
        }

        switch($Pkl['MataKuliah1']){
            case 1:
                $Pkl['MataKuliah1Text'] = "Sumber Daya Alam";
                break;
            case 2:
                $Pkl['MataKuliah1Text'] = "Keselamatan dan Lingkungan";
                break;
            case 3:
                $Pkl['MataKuliah1Text'] = "Metode dan Terapan";
                break;
            case 4:
                $Pkl['MataKuliah1Text'] = "Analisis, Penilaian, dan Tindakan";
                break;
            case 5:
                $Pkl['MataKuliah1Text'] = "Kemandirian Pengayaan Pengetahuan";
                break;

            default :
                $Pkl['MataKuliah1Text'] = "-";
                break;
        }

        switch($Pkl['MataKuliah2']){
            case 1:
                $Pkl['MataKuliah2Text'] = "Sumber Daya Alam";
                break;
            case 2:
                $Pkl['MataKuliah2Text'] = "Keselamatan dan Lingkungan";
                break;
            case 3:
                $Pkl['MataKuliah2Text'] = "Metode dan Terapan";
                break;
            case 4:
                $Pkl['MataKuliah2Text'] = "Analisis, Penilaian, dan Tindakan";
                break;
            case 5:
                $Pkl['MataKuliah2Text'] = "Kemandirian Pengayaan Pengetahuan";
                break;

            default :
                $Pkl['MataKuliah2Text'] = "-";
                break;
        }

        switch($Pkl['MataKuliah3']){
            case 1:
                $Pkl['MataKuliah3Text'] = "Sumber Daya Alam";
                break;
            case 2:
                $Pkl['MataKuliah3Text'] = "Keselamatan dan Lingkungan";
                break;
            case 3:
                $Pkl['MataKuliah3Text'] = "Metode dan Terapan";
                break;
            case 4:
                $Pkl['MataKuliah3Text'] = "Analisis, Penilaian, dan Tindakan";
                break;
            case 5:
                $Pkl['MataKuliah3Text'] = "Kemandirian Pengayaan Pengetahuan";
                break;

            default :
                $Pkl['MataKuliah3Text'] = "-";
                break;
        }

        $data['PKL'] = $Pkl;

        /* Setting kembali */
        $data['url_kembali'] = get_referer_url();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('pkl/DosenPembimbingPKL', $data);
        $this->load->view('templates/footer');
    }

    public function NilaiDosenPembimbingLPA(){
        $PklId = $this->input->post('PklId');

        $requestLaporanAkhir = [
            '12_1_1' => $this->input->post("12_1_1"),
            '12_1_2' => $this->input->post("12_1_2"),
            '12_1_3' => $this->input->post("12_1_3"),

            '12_2_1' => $this->input->post("12_2_1"),
            '12_2_2' => $this->input->post("12_2_2"),
            '12_2_3' => $this->input->post("12_2_3"),
            '12_2_4' => $this->input->post("12_2_4"),

            '12_3_1' => $this->input->post("12_3_1"),
            '12_3_2' => $this->input->post("12_3_2"),

            '12_5_1' => $this->input->post("12_5_1"),
            '12_5_2' => $this->input->post("12_5_2"),
            
            '15_1_1' => $this->input->post("15_1_1"),
            '15_1_2' => $this->input->post("15_1_2"),

            '15_2_1' => $this->input->post("15_2_1"),
            '15_2_2' => $this->input->post("15_2_2"),

            '15_3_1' => $this->input->post("15_3_1"),
            '15_3_2' => $this->input->post("15_3_2"),

            '15_4_1' => $this->input->post("15_4_1"),
            '15_4_2' => $this->input->post("15_4_2"),

            '15_5_1' => $this->input->post("15_5_1"),
            '15_5_2' => $this->input->post("15_5_2"),
        ];
        
        $jsonNilai = json_encode($requestLaporanAkhir);

        $requestData = [
            'LP_PKL_A' => $jsonNilai
        ];

        /* cek data nilai */
        $queryCekData = "select * from NilaiDosenPembimbingPKL where PklId = $PklId";
        $dataLPA = $this->db->query($queryCekData)->row_array();

        if($dataLPA!=null){
            $this->db->update('NilaiDosenPembimbingPKL', $requestData, "PklId = $PklId");
        }else{
            $requestData['PklId'] = $PklId;
            $this->db->insert('NilaiDosenPembimbingPKL', $requestData);
        }

        redirect('pkl/NilaiDosenPembimbing/'. $PklId);
    }

    public function NilaiDosenPembimbingLPB(){
        $PklId = $this->input->post('PklId');

        $requestLaporanTengah = [
            '12_1_1' => $this->input->post("12_1_1"),
            '12_1_2' => $this->input->post("12_1_2"),
            '12_1_3' => $this->input->post("12_1_3"),

            '12_2_1' => $this->input->post("12_2_1"),
            '12_2_2' => $this->input->post("12_2_2"),
            '12_2_3' => $this->input->post("12_2_3"),
            '12_2_4' => $this->input->post("12_2_4"),

            '12_3_1' => $this->input->post("12_3_1"),
            '12_3_2' => $this->input->post("12_3_2"),

            '12_5_1' => $this->input->post("12_5_1"),
            '12_5_2' => $this->input->post("12_5_2"),
            
            '15_1_1' => $this->input->post("15_1_1"),
            '15_1_2' => $this->input->post("15_1_2"),

            '15_2_1' => $this->input->post("15_2_1"),
            '15_2_2' => $this->input->post("15_2_2"),

            '15_3_1' => $this->input->post("15_3_1"),
            '15_3_2' => $this->input->post("15_3_2"),

            '15_4_1' => $this->input->post("15_4_1"),
            '15_4_2' => $this->input->post("15_4_2"),

            '15_5_1' => $this->input->post("15_5_1"),
            '15_5_2' => $this->input->post("15_5_2"),
        ];
        
        $jsonNilai = json_encode($requestLaporanTengah);

        $requestData = [
            'LP_PKL_B' => $jsonNilai
        ];

        /* cek data nilai */
        $queryCekData = "select * from NilaiDosenPembimbingPKL where PklId = $PklId";
        $dataLPA = $this->db->query($queryCekData)->row_array();

        if($dataLPA!=null){
            $this->db->update('NilaiDosenPembimbingPKL', $requestData, "PklId = $PklId");
        }else{
            $requestData['PklId'] = $PklId;
            $this->db->insert('NilaiDosenPembimbingPKL', $requestData);
        }

        redirect('pkl/NilaiDosenPembimbing/'. $PklId);
    }

    public function NilaiDosenPembimbingMK1(){
        $PklId = $this->input->post('PklId');

        $requestMK1 = [
            '1' => $this->input->post("1"),
            '2' => $this->input->post("2"),
            '3' => $this->input->post("3"),
        ];
        
        $jsonNilai = json_encode($requestMK1);

        $requestData = [
            'LP_MK1' => $jsonNilai
        ];

        /* cek data nilai */
        $queryCekData = "select * from NilaiDosenPembimbingPKL where PklId = $PklId";
        $dataLPA = $this->db->query($queryCekData)->row_array();

        if($dataLPA!=null){
            $this->db->update('NilaiDosenPembimbingPKL', $requestData, "PklId = $PklId");
        }else{
            $requestData['PklId'] = $PklId;
            $this->db->insert('NilaiDosenPembimbingPKL', $requestData);
        }

        redirect('pkl/NilaiDosenPembimbing/'. $PklId);
    }

    public function NilaiDosenPembimbingMK2(){
        $PklId = $this->input->post('PklId');

        $requestMK1 = [
            '1' => $this->input->post("1"),
            '2' => $this->input->post("2"),
            '3' => $this->input->post("3"),
        ];
        
        $jsonNilai = json_encode($requestMK1);

        $requestData = [
            'LP_MK2' => $jsonNilai
        ];

        /* cek data nilai */
        $queryCekData = "select * from NilaiDosenPembimbingPKL where PklId = $PklId";
        $dataLPA = $this->db->query($queryCekData)->row_array();

        if($dataLPA!=null){
            $this->db->update('NilaiDosenPembimbingPKL', $requestData, "PklId = $PklId");
        }else{
            $requestData['PklId'] = $PklId;
            $this->db->insert('NilaiDosenPembimbingPKL', $requestData);
        }

        redirect('pkl/NilaiDosenPembimbing/'. $PklId);
    }

    public function NilaiDosenPembimbingMK3(){
        $PklId = $this->input->post('PklId');

        $requestMK1 = [
            '1' => $this->input->post("1"),
            '2' => $this->input->post("2"),
            '3' => $this->input->post("3"),
        ];
        
        $jsonNilai = json_encode($requestMK1);

        $requestData = [
            'LP_MK3' => $jsonNilai
        ];

        /* cek data nilai */
        $queryCekData = "select * from NilaiDosenPembimbingPKL where PklId = $PklId";
        $dataLPA = $this->db->query($queryCekData)->row_array();

        if($dataLPA!=null){
            $this->db->update('NilaiDosenPembimbingPKL', $requestData, "PklId = $PklId");
        }else{
            $requestData['PklId'] = $PklId;
            $this->db->insert('NilaiDosenPembimbingPKL', $requestData);
        }

        redirect('pkl/NilaiDosenPembimbing/'. $PklId);
    }

    public function ViewNilaiDosenPembimbingLPA(){
        $PklId = $this->input->post('PklId');

        $query = "
                select LP_PKL_A from NilaiDosenPembimbingPKL where PklId = $PklId
                ";
        
        $LPA = $this->db->query($query)->row_array();

        echo json_encode($LPA);
    }

    public function ViewNilaiDosenPembimbingLPB(){
        $PklId = $this->input->post('PklId');

        $query = "
                select LP_PKL_B from NilaiDosenPembimbingPKL where PklId = $PklId
                ";
        
        $LPA = $this->db->query($query)->row_array();

        echo json_encode($LPA);
    }

    public function ViewNilaiDosenPembimbingMK1(){
        $PklId = $this->input->post('PklId');

        $query = "
                select nilai.LP_MK1, pkl.MataKuliah1
                from NilaiDosenPembimbingPKL nilai
                join PraktekKerjaLapangan pkl on nilai.PklId = pkl.Id
                where PklId = $PklId
                ";
        
        $MK1 = $this->db->query($query)->row_array();

        echo json_encode($MK1);
    }

    public function ViewNilaiDosenPembimbingMK2(){
        $PklId = $this->input->post('PklId');

        $query = "
                select nilai.LP_MK2, pkl.MataKuliah2
                from NilaiDosenPembimbingPKL nilai
                join PraktekKerjaLapangan pkl on nilai.PklId = pkl.Id
                where PklId = $PklId
                ";
        
        $MK2 = $this->db->query($query)->row_array();

        echo json_encode($MK2);
    }
    
}