<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class Sdm extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->library('form_validation');
        if (!$this->session->userdata('Id')){
            redirect('auth');
        }
    }
    
    public function index(){
        $data['title'] = 'SDM';

        $Id = $this->session->userdata['Id'];
        $query = "select * from Users where Id = $Id";
        $User = $this->db->query($query)->row_array();
        $data['User'] = $User;
        
        /* Setting kembali */
        $data['url_kembali'] = get_referer_url();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('sdm/index', $data);
        $this->load->view('templates/footer');
    }

    public function TemplateExcelTendik(){
        $this->load->helper('download');
        $templateFilePath = './assets/templates//excel/Tenaga Kependidikan.xlsx'; 
        force_download($templateFilePath, NULL);
    }
    public function TemplateExcelMahasiswa(){
        $this->load->helper('download');
        $templateFilePath = './assets/templates//excel/Mahasiswa.xlsx';
        force_download($templateFilePath, NULL);
    }

    /* Mahasiswa */
    public function DataMahasiswa($Mahasiswa = null){
        $data['title'] = 'Data Mahasiswa';

        $Id = $this->session->userdata['Id'];
        $query = "select * from Users where Id = $Id";
        $User = $this->db->query($query)->row_array();
        $data['User'] = $User;

        /* Penyimpanan Request Data */
        $RequestData = [];

        /* Query Data */
        $queryMahasiswa = "select usr.Id, usr.Username, usr.Name, usr.Status, usr.RoleId, mhs.Status, usr.Telpon, usr.Email, mhs.Angkatan
                            from users usr
                            join UserMahasiswa mhs on usr.Id = mhs.UserId
                            where usr.Status != 2 AND IsKimia = 1";

        /* proses Filter ketika Pengambilan Data */
        if($this->input->post('angkatan') != NULL){
            $year = $this->input->post('angkatan');
            $queryMahasiswa = $queryMahasiswa . " AND Angkatan = '$year'";
            $RequestData['Angkatan'] = $year;
        }else if(isset($Mahasiswa['Angkatan'])){
            $year = $Mahasiswa['Angkatan'];
            $queryMahasiswa = $queryMahasiswa . " AND Angkatan = '$year'";
            $RequestData['Angkatan'] = $year;
        }else{
            $time = strtotime("-1 year", time());
            $year = date("Y", $time);
            $queryMahasiswa = $queryMahasiswa . " AND Angkatan = '$year'";
            $RequestData['Angkatan'] = $year;
        }

        if($this->input->post('jenjang') != NULL){
            $jenjang = $this->input->post('jenjang');
            $queryMahasiswa = $queryMahasiswa . " AND RoleId = '$jenjang'";
            $RequestData['Jenjang'] = $jenjang;
        }else{
            $jenjang = 2;
            $queryMahasiswa = $queryMahasiswa . " AND RoleId = '$jenjang'";
            $RequestData['Jenjang'] = $jenjang;
        }

        if($this->input->post('search') != NULL){
            $search = $this->input->post('search');
            $queryMahasiswa = $queryMahasiswa . " AND usr.Name LIKE '%$search%' OR usr.Username LIKE '%$search%'"; 
        }

        if($this->input->post('limit') != NULL){
            $limit = $this->input->post('limit');
            $queryMahasiswa = $queryMahasiswa . " limit $limit"; 
            $RequestData['Limit'] = $limit;
        }
        
        $data['RequestData'] = $RequestData;

        /* Proses Eksekusi Query */
        $data['Mahasiswa'] = $this->db->query($queryMahasiswa)->result_array();

        /* Mengambil Data Angkatan Mahasiswa */
        $queryAngkatan = "select Angkatan from UserMahasiswa where IsKimia = 1 group by Angkatan";
        $data['Year'] = $this->db->query($queryAngkatan)->result_array();
        
        /* Setting kembali */
        $data['url_kembali'] = get_referer_url();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('sdm/DataMahasiswa', $data);
        $this->load->view('templates/footer');
    }

    public function importMahasiswa(){ 
        if($_FILES["importexcel"]["name"] != ''){
            $allowed_extension = array('xls', 'xlsx', '.csv');
            $file_array = explode(".", $_FILES['importexcel']['name']);
            $file_extension = end($file_array);

            if(in_array($file_extension, $allowed_extension)){
                $file_name = time() . '.' . $file_extension; // rename file
                move_uploaded_file($_FILES['importexcel']['tmp_name'], $file_name); // save file in directory
                $file_type = \PhpOffice\PhpSpreadsheet\IOFactory::identify($file_name); 
                $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($file_type);
                $spreadsheet = $reader->load($file_name); // get data file
                unlink($file_name); // hapus file
                $data = $spreadsheet->getActiveSheet()->toArray(); // cek sheet
                $sheetcount=count($data); // menjumlah data sheet

                $datetime = new DateTime();
                $timezone = new DateTimeZone('Asia/Jakarta'); 
        
                $datetime->setTimezone($timezone);
                /* proses pengambilan data */
                for($i=2; $i<$sheetcount; $i++){

                    if ($data[$i][2] != null){
                        $username = "";
                        if ($data[$i][1] != null){
                            $username = $data[$i][1];
                        }

                        /* Cek Duplicate Data */
                        $queryCekData = "select * from Users where Username = '$username'";
                        $duplicateData = $this->db->query($queryCekData)->row_array();

                        if ($duplicateData == null){
                            $requestDataUser = array(
                                'Username'  => $data[$i][1],
                                'RoleId' => htmlspecialchars($this->input->post('role_id')),
                                'Name' => $data[$i][3],
                                'Password' => password_hash($data[$i][1], PASSWORD_DEFAULT),
                                'CreatedAt' => $datetime->format("Y-m-d H:i:s"),
                                'ChangedPassword' => 0,
                                'Status' => 1,
                                'ImageProfile' => 'default.jpg',
                                'Alamat' => $data[$i][6]
                            );

                            $this->db->insert('Users', $requestDataUser);

                            $query = "select * from Users where Username='$username'";
                            $User = $this->db->query($query)->row_array();

                            if ($data[$i][7]=="L"){
                                $jeniskelamin = 1;
                            }else{
                                $jeniskelamin = 0;
                            }

                            $requestDataMahasiswa = array(
                                'UserId' => $User['Id'],
                                'Angkatan' => $data[$i][2],
                                'TempatLahir' => $data[$i][4],
                                'TanggalLahir' => ($data[$i][5] != null && $data[$i][5] != "") ? str_replace("'"," ",$data[$i][5]) : "",
                                'JenisKelamin' => $jeniskelamin,
                                'Status' => $data[$i][8],
                                'Seleksi' => $data[$i][9],
                                'Fakultas' => $data[$i][10],
                                'ProgramStudi' => $data[$i][11],
                                'Tertanggal' => $data[$i][13],
                                'BidikMisi' => $data[$i][14],
                                'IsKimia' => 1,
                            );

                            $this->db->insert('UserMahasiswa', $requestDataMahasiswa);
                        }
                    }
                }
                $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
                    Data mahasiswa Berhasil Ditambahkan
                </div>');

                redirect('sdm/DataMahasiswa');
            }else{
                $message = '<div class="alert alert-danger">Only .xls or .xlsx file allowed</div>';
            }
        }else{
            $message = '<div class="alert alert-danger">Please Select File</div>';
        }
    }

    public function TambahMahasiswa(){
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('username', 'Username', 'required|is_unique[Users.Username]',[
            'is_unique' => 'This Username has already registered!'
        ]);
        $this->form_validation->set_rules('angkatan', 'Angkatan', 'required|max_length[4]',[
            'max_length' => 'Tahun angkatan tidak valid'
        ]);
        $this->form_validation->set_rules('role_id', 'RoleId', 'required');

        $datetime = new DateTime();
        $timezone = new DateTimeZone('Asia/Jakarta'); 

        $datetime->setTimezone($timezone);

        if ($this->form_validation->run() == false){
            $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
                Gagal Menambahkan Mahasiswa
            </div>');
            redirect('sdm/DataMahasiswa');
        }else{
            $requestData = [
                'Name' => htmlspecialchars($this->input->post('name')),
                'Username' => htmlspecialchars($this->input->post('username')),
                'Password' => password_hash($this->input->post('username'), PASSWORD_DEFAULT),
                'RoleId' => htmlspecialchars($this->input->post('role_id')),
                'CreatedAt' => $datetime->format("Y-m-d H:i:s"),
                'ChangedPassword' => 0,
                'Status' => 1,
                'ImageProfile' => 'default.jpg'
            ];

            $this->db->insert('Users', $requestData);

            $User = $this->db->get_where('Users', ['Username' => htmlspecialchars($this->input->post('username'))])->row_array();

            
            $requestDataMahasiswa = array(
                'UserId' => $User['Id'],
                'Angkatan' => htmlspecialchars($this->input->post('angkatan')),
                'Status' => 'Aktif',
                'Fakultas' => 'S1-FMIPA',
                'ProgramStudi' => 'Kimia',
                'IsKimia' => 1,
            );

            $this->db->insert('UserMahasiswa', $requestDataMahasiswa);

            $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
                Mahasiswa Berhasil Ditambahkan
            </div>');

            redirect('sdm/Datamahasiswa');
        }
    }

    public function DetailMahasiswa(){
        $UserId = $this->input->post('UserId');
        
        $query = "Select usr.Name, usr.Username, usr.Telpon, usr.Email, usr.AlamatMalang, usr.RoleId, usr.Id,
                    mhs.Angkatan, mhs.TanggalLahir, usr.Alamat, mhs.JenisKelamin, mhs.Status, mhs.Seleksi, 
                    mhs.Fakultas, mhs.ProgramStudi, mhs.Tertanggal, mhs.BidikMisi, mhs.TempatLahir
                    from Users usr
                    join UserMahasiswa mhs on usr.Id = mhs.UserId
                    where usr.Id = $UserId";
        
        $userData = $this->db->query($query)->row_array();

        $JenisKelamin = ($userData['JenisKelamin'] == 0) ? "Perempuan" : "Laki-Laki";

        if ($userData['JenisKelamin'] === 0){
            $JenisKelamin = "Perempuan";
        }else if($userData['JenisKelamin'] === 1){
            $JenisKelamin = "Laki-Laki";
        }else{
            $JenisKelamin = "-";
        }

        if ($userData['RoleId'] == 2){
            $Jenjang = 'Mahasiswa S1';
        }elseif ($userData['RoleId'] == 3){
            $Jenjang = 'Mahasiswa S2';
        }else{
            $Jenjang = 'Mahasiswa S3';
        }

        if ($this->input->post('view') == "true"){
        /* Return View Modal Content */
        echo $view = '
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Detail Mahasiswa</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
            <div class="card">
                <div class="card-body">
                <div class="form-group row">
                    <label for="inputName" class="col-sm-4 col-form-label">Nama Mahasiswa</label>
                    <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputName" name="name" value="'.$userData['Name'].'" disabled>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputUsername" class="col-sm-4 col-form-label">NIM</label>
                    <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputUsername" name="username" value="'.$userData['Username'].'" disabled>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputAngkatan" class="col-sm-4 col-form-label">Angkatan</label>
                    <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputAngkatan" name="angkatan" value="'.$userData['Angkatan'].'" disabled>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputJenisKelamin" class="col-sm-4 col-form-label">Jenis Kelamin</label>
                    <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputJenisKelamin" name="JenisKelamin" value="'.$JenisKelamin.'" disabled>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputTTL" class="col-sm-4 col-form-label">Tempat / Tanggal Lahir</label>
                    <div class="col-sm-4">
                    <div class="input-group">
                    <input type="text" class="form-control" id="inputTTL" name="TempatLahir" value="'.$userData['TempatLahir'].'" disabled>
                    <span class="input-group-text">/</span>
                    </div>
                    </div>
                    <div class="col-sm-4">
                    <input type="text" class="form-control" id="inputTTL" name="TanggalLahir" value="'.$userData['TanggalLahir'].'" disabled>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputTanggalMasuk" class="col-sm-4 col-form-label">Tanggal Masuk</label>
                    <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputTanggalMasuk" name="TanggalMasuk" value="'.$userData['Tertanggal'].'" disabled>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputAlamatMalang" class="col-sm-4 col-form-label">Alamat di malang</label>
                    <div class="col-sm-8">
                    <textarea class="form-control" id="inputAlamatMalang" name="AlamatMalang" rows="2" value="'.$userData['AlamatMalang'].'" disabled></textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputAlamat" class="col-sm-4 col-form-label">Alamat Asal</label>
                    <div class="col-sm-8">
                    <textarea class="form-control" id="inputAlamat" name="Alamat" rows="2" value="'.$userData['Alamat'].'" disabled></textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputTelponEmail" class="col-sm-4 col-form-label">Telpon</label>
                    <div class="col-sm-3">
                    <input type="text" class="form-control" id="inputTelponEmail" name="Telpon" value="'.$userData['Telpon'].'" disabled>
                    </div>
                    <div class="col-sm-5">
                    <div class="input-group">
                    <span class="input-group-text">Email</span>
                    <input type="text" class="form-control" id="inputTelponEmail" name="Email" value="'.$userData['Email'].'" disabled>
                    </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputJenjang" class="col-sm-4 col-form-label">Jenjang Pendidikan</label>
                    <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputJenjang" name="jenjang" value="'.$Jenjang.'" disabled>
                    </div>
                </div>
                </div>
            </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Tutup</button>
            </div>
        ';
        }else{
            echo $view = '
            <div class="card">
                <div class="card-body">
                <div class="form-group row">
                    <label for="inputName" class="col-sm-4 col-form-label">Nama Mahasiswa</label>
                    <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputId" name="Id" value="'.$userData['Id'].'" hidden>
                    <input type="text" class="form-control" id="inputName" name="Name" value="'.$userData['Name'].'">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputUsername" class="col-sm-4 col-form-label">NIM</label>
                    <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputUsername" name="Username" value="'.$userData['Username'].'">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputAngkatan" class="col-sm-4 col-form-label">Angkatan</label>
                    <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputAngkatan" name="Angkatan" value="'.$userData['Angkatan'].'">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputJenisKelamin" class="col-sm-4 col-form-label">Jenis Kelamin</label>
                    <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputJenisKelamin" name="JenisKelamin" value="'.$JenisKelamin.'">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputTTL" class="col-sm-4 col-form-label">Tempat / Tanggal Lahir</label>
                    <div class="col-sm-4">
                    <div class="input-group">
                    <input type="text" class="form-control" id="inputTTL" name="TempatLahir" value="'.$userData['TempatLahir'].'">
                    <span class="input-group-text">/</span>
                    </div>
                    </div>
                    <div class="col-sm-4">
                    <input type="date" class="form-control" id="inputTTL" name="TanggalLahir" value="'.(($userData['TanggalLahir'] != NULL) ? date('Y-m-d',strtotime($userData['TanggalLahir'])) : "").'">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputTanggalMasuk" class="col-sm-4 col-form-label">Tanggal Masuk</label>
                    <div class="col-sm-8">
                    <input type="date" class="form-control" id="inputTanggalMasuk" name="TanggalMasuk" value="'.(($userData['Tertanggal'] != NULL) ? date('Y-m-d',strtotime($userData['Tertanggal'])) : "").'">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputAlamatMalang" class="col-sm-4 col-form-label">Alamat di malang</label>
                    <div class="col-sm-8">
                    <textarea class="form-control" id="inputAlamatMalang" name="AlamatMalang" rows="2" value="'.$userData['AlamatMalang'].'"></textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputAlamat" class="col-sm-4 col-form-label">Alamat Asal</label>
                    <div class="col-sm-8">
                    <textarea class="form-control" id="inputAlamat" name="Alamat" rows="2" value="'.$userData['Alamat'].'"></textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputTelponEmail" class="col-sm-4 col-form-label">Telpon</label>
                    <div class="col-sm-3">
                    <input type="text" class="form-control" id="inputTelponEmail" name="Telpon" value="'.$userData['Telpon'].'">
                    </div>
                    <div class="col-sm-5">
                    <div class="input-group">
                    <span class="input-group-text">Email</span>
                    <input type="text" class="form-control" id="inputTelponEmail" name="Email" value="'.$userData['Email'].'">
                    </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputJenjang" class="col-sm-4 col-form-label">Jenjang Pendidikan</label>
                    <div class="col-sm-8">
                    <select id="inputJenjang" name="RoleId" class="form-control">
                        <option value="2" '.($userData['RoleId']== 2 ? "selected" : "").'>Mahasiswa S1</option>
                        <option value="3" '.($userData['RoleId']== 3 ? "selected" : "").'>Mahasiswa S2</option>
                        <option value="4" '.($userData['RoleId']== 4 ? "selected" : "").'>Mahasiswa S3</option>
                    </select>
                    </div>
                </div>
                </div>
            </div>
            </div>
        ';
        }
    }

    public function EditMahasiswa(){
        $UserId = htmlspecialchars($this->input->post('Id'));

        $UserRequest = [
            'Name' => htmlspecialchars($this->input->post('Name')),
            'Username' => htmlspecialchars($this->input->post('Username')),
            'Telpon' => htmlspecialchars($this->input->post('Telpon')),
            'Email' => htmlspecialchars($this->input->post('Email')),
            'AlamatMalang' => htmlspecialchars($this->input->post('AlamatMalang')),
            'RoleId' => htmlspecialchars($this->input->post('RoleId')),
            'Alamat' => htmlspecialchars($this->input->post('Alamat')),
        ];

        $this->db->where('Id', $UserId)->update('Users', $UserRequest);

        $MahasiswaRequest = [
            'Angkatan' => htmlspecialchars($this->input->post('Angkatan')),
            'TanggalLahir' => htmlspecialchars($this->input->post('TanggalLahir')),
            'TempatLahir' => htmlspecialchars($this->input->post('TempatLahir')),
            'JenisKelamin' => htmlspecialchars($this->input->post('JenisKelamin')),
            'Status' => htmlspecialchars($this->input->post('Status')),
            'Seleksi' => htmlspecialchars($this->input->post('Seleksi')),
            'Fakultas' => htmlspecialchars($this->input->post('Fakultas')),
            'ProgramStudi' => htmlspecialchars($this->input->post('ProgramStudi')),
            'Tertanggal' => htmlspecialchars($this->input->post('Tertanggal')),
            'BidikMisi' => htmlspecialchars($this->input->post('BidikMisi')),
        ];

        $this->db->where('Id', $UserId)->update('UserMahasiswa', $MahasiswaRequest);


        $Mahasiswa = $this->db->get_where('UserMahasiswa', array('UserId' => $UserId))->row_array();

        $this->DataMahasiswa($Mahasiswa);
    }

    public function DeleteMahasiswa($Id){
        $requestData = [
            'Status' => 2
        ];

        $this->db->where('Id', $Id)->update('Users', $requestData);

        $Mahasiswa = $this->db->get_where('UserMahasiswa', array('UserId' => $Id))->row_array();

        $this->DataMahasiswa($Mahasiswa);
    }

    public function ResetPasswordMahasiswa($Id){
        /* Get Data User By Id */
        $query = "select * from Users where Id = $Id";
        $UserData = $this->db->query($query)->row_array();

        $requestData = [
            'ChangedPassword' => 0,
            'Password' => password_hash($UserData['Username'], PASSWORD_DEFAULT),
        ];

        $this->db->where('Id', $Id)->update('Users', $requestData);
        
        $Mahasiswa = $this->db->get_where('UserMahasiswa', array('UserId' => $Id))->row_array();

        $this->DataMahasiswa($Mahasiswa);
    }

    /* Dosen */
    public function DataDosen($Dosen = null){
        $data['title'] = 'Data Dosen';
        $Id = $this->session->userdata['Id'];
        $query = "select * from Users where Id = $Id";
        $User = $this->db->query($query)->row_array();
        $data['User'] = $User;

        /* Query Data */
        $queryDosen = "select usr.Id, usr.Name, usr.ImageProfile, dsn.Laboratorium,
                        (CASE WHEN dsn.Laboratorium IS NOT NULL AND dsn.Laboratorium != ''
                        THEN (SELECT Nama
                                FROM AkademikLaboratorium
                                WHERE Id = Laboratorium)
                        ELSE '-' END) AS Lab
                        from Users usr
                        join UserTenagaKependidikan dsn on usr.Id = dsn.UserId
                        where usr.Status != 2 AND usr.RoleId IN ('1')";

        /* Penyimpanan Request Data */
        $RequestData = [];

        /* proses Filter ketika Pengambilan Data */
        if($this->input->post('ProgramStudi') != NULL){
            $programStudi = $this->input->post('ProgramStudi');
            $queryDosen = $queryDosen . " AND dsn.ProgramStudi = '$programStudi'";
            $RequestData['ProgramStudi'] = $programStudi;
        }

        if($this->input->post('laboratorium') != NULL){
            $Laboratorium = $this->input->post('laboratorium');
            $queryDosen = $queryDosen . " AND dsn.Laboratorium = $Laboratorium"; 
            $RequestData['laboratorium'] = $Laboratorium;
        }

        if($this->input->post('search') != NULL){
            $search = $this->input->post('search');
            $queryDosen = $queryDosen . " AND usr.Name LIKE '%$search%'"; 
        }

        if($this->input->post('limit') != NULL){
            $limit = $this->input->post('limit');
            $queryDosen = $queryDosen . " limit $limit"; 
            $RequestData['Limit'] = $limit;
        }
        
        $data['RequestData'] = $RequestData;

        /* Proses Eksekusi Query */
        $Dosen = $this->db->query($queryDosen)->result_array();

        $data['Dosen'] = $Dosen;

        /* Ambil Data lab */
        $queryLab = "
                    select * 
                    from AkademikLaboratorium 
                    where Status = 1 AND IsDeleted = 0";
        $Lab = $this->db->query($queryLab)->result_array();
        $data['Lab'] = $Lab;

        /* Setting kembali */
        $data['url_kembali'] = get_referer_url();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('sdm/DataDosen', $data);
        $this->load->view('templates/footer');
    }

    public function importDosen(){ 
        if($_FILES["importexcel"]["name"] != ''){
            $allowed_extension = array('xls', 'xlsx', '.csv');
            $file_array = explode(".", $_FILES['importexcel']['name']);
            $file_extension = end($file_array);

            if(in_array($file_extension, $allowed_extension)){
                $file_name = time() . '.' . $file_extension; // rename file
                move_uploaded_file($_FILES['importexcel']['tmp_name'], $file_name); // save file in directory
                $file_type = \PhpOffice\PhpSpreadsheet\IOFactory::identify($file_name); 
                $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($file_type);
                $spreadsheet = $reader->load($file_name); // get data file
                unlink($file_name); // hapus file
                $data = $spreadsheet->getActiveSheet()->toArray(); // cek sheet
                $sheetcount=count($data); // menjumlah data sheet

                $datetime = new DateTime();
                $timezone = new DateTimeZone('Asia/Jakarta'); 
        
                $datetime->setTimezone($timezone);
                /* proses pengambilan data */
                for($i=1; $i<$sheetcount; $i++){
                    /* cek data excel */
                    if ($data[$i][2] != null || $data[$i][3] != null){
                        $username = "";
                        $email = "";
                        if ($data[$i][2] != null){
                            $username = $data[$i][2];
                        }else{
                            $email = $data[$i][3];
                        }

                        /* Cek Duplicate Data */
                        $queryCekData = "select * from Users where Username = '$username' or Email = '$email'";
                        $duplicateData = $this->db->query($queryCekData)->row_array();

                        if ($duplicateData == null){
                            $requestDataUser = array(
                                'RoleId' => 1,
                                'Name' => $data[$i][1],
                                'CreatedAt' => $datetime->format("Y-m-d H:i:s"),
                                'ChangedPassword' => 0,
                                'Status' => 1,
                                'ImageProfile' => 'default.jpg',
                            );
                            
                            if ($data[$i][2] != null){
                                $requestDataUser['Email'] = $data[$i][3];
                            }
                            
                            if($username != null){
                                $requestDataUser['Username'] = $username;
                                $password = $username;
                            }else{
                                $requestDataUser['Email'] = $email;
                                $password = $email;
                            }
        
                            $requestDataUser['Password'] = password_hash($password, PASSWORD_DEFAULT);
        
                            $this->db->insert('Users', $requestDataUser);
        
                            $query = "select * from Users where Username='$username' or Email='$email'";
                            $User = $this->db->query($query)->row_array();
        
                            $requestDataDosen = array(
                                'UserId' => $User['Id'],
                                'ProgramStudi' => $data[$i][6],
                                'Bidang' => $data[$i][7],
                                'Golongan' => $data[$i][4],
                                'Pangkat' => $data[$i][5],
                                'Jabatan' => $data[$i][6],
                                'TugasTambahan' => $data[$i][9],
                                'Departemen' => $data[$i][8],
                                'IsStaff' => 0,
                            );
        
                            $this->db->insert('UserTenagaKependidikan', $requestDataDosen);
                        }
                    }
                }
                $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
                    Data Dosen Berhasil Ditambahkan
                </div>');

                unlink($file_name);

                redirect('sdm/DataDosen');
            }else{
                $message = '<div class="alert alert-danger">Only .xls or .xlsx file allowed</div>';
            }
        }else{
            $message = '<div class="alert alert-danger">Please Select File</div>';
        }
    }

    public function TambahDosen(){
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('laboratorium', 'Laboratorium', 'required');
        $this->form_validation->set_rules('bidang', 'Bidang', 'required');
        $this->form_validation->set_rules('level', 'Level', 'required');

        $datetime = new DateTime();
        $timezone = new DateTimeZone('Asia/Jakarta'); 

        $datetime->setTimezone($timezone);

        if ($this->form_validation->run() == false){
            $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
                Gagal Menambahkan Dosen
            </div>');
            redirect('sdm/DataDosen');
        }else{
            $requestData = [
                'Name' => htmlspecialchars($this->input->post('name')),
                'Email' => htmlspecialchars($this->input->post('email')),
                'RoleId' => 1,
                'CreatedAt' => $datetime->format("Y-m-d H:i:s"),
                'ChangedPassword' => 0,
                'Status' => 1,
                'ImageProfile' => 'default.jpg'
            ];

            if (htmlspecialchars($this->input->post('username')) != null){
                $password = password_hash($this->input->post('username'), PASSWORD_DEFAULT);
                $requestData['Username'] = htmlspecialchars($this->input->post('username'));
            }else{
                $password = password_hash($this->input->post('email'), PASSWORD_DEFAULT);
            }

            $requestData['Password'] = $password;

            $this->db->insert('Users', $requestData);

            $User = $this->db->get_where('Users', ['Username' => htmlspecialchars($this->input->post('username'))])->row_array();
            
            $requestDataDosen = array(
                'UserId' => $User['Id'],
                'Laboratorium' => htmlspecialchars($this->input->post('laboratorium')),
                'Bidang' => htmlspecialchars($this->input->post('bidang')),
                'ProgramStudi' => htmlspecialchars($this->input->post('programstudi')),
                'IsStaff' => 0,
                'IsKimia' => 1,
                'Institusi' => "-",
            );

            $this->db->insert('UserTenagaKependidikan', $requestDataDosen);

            $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
                Dosen Berhasil Ditambahkan
            </div>');

            redirect('sdm/DataDosen');
        }
    }

    public function TambahDosenLuar(){
        $this->form_validation->set_rules('name', 'Name', 'required');

        $datetime = new DateTime();
        $timezone = new DateTimeZone('Asia/Jakarta'); 

        $datetime->setTimezone($timezone);

        if ($this->form_validation->run() == false){
            $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
                Gagal Menambahkan Dosen Luar
            </div>');
            redirect('sdm/DataDosen');
        }else{
            $requestData = [
                'Name' => htmlspecialchars($this->input->post('name')),
                'Email' => htmlspecialchars($this->input->post('email')),
                'RoleId' => 1,
                'CreatedAt' => $datetime->format("Y-m-d H:i:s"),
                'ChangedPassword' => 1,
                'Status' => 1,
                'ImageProfile' => 'default.jpg'
            ];

            $password = password_hash("Kimia088.", PASSWORD_DEFAULT);
            $requestData['Username'] = htmlspecialchars($this->input->post('email'));

            $requestData['Password'] = $password;

            $this->db->insert('Users', $requestData);

            $User = $this->db->get_where('Users', ['Username' => htmlspecialchars($this->input->post('email'))])->row_array();
            
            $modifDate = new DateTime('now');
            $modifDate->modify('+6 month'); // or you can use '-90 day' for deduct
            $modifDate = $modifDate->format('Y-m-d h:i:s');

            $requestDataDosen = array(
                'UserId' => $User['Id'],
                'IsStaff' => 0,
                'IsKimia' => 0,
                'Institusi' => htmlspecialchars($this->input->post('institusi')),
                'ActiveDate' => $modifDate,
            );

            $this->db->insert('UserTenagaKependidikan', $requestDataDosen);

            $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
                Dosen Berhasil Ditambahkan
            </div>');

            redirect('sdm/DataDosen');
        }
    }

    public function DetailDosen($UserId = null){

        $query = "
                select usr.Id, usr.Name, usr.Username, usr.Telpon, usr.Email, usr.ImageProfile, usr.AlamatMalang, usr.Alamat, 
                stf.ProgramStudi, stf.Laboratorium, stf.Bidang, stf.Golongan, stf.Pangkat, stf.Jabatan, stf.PendidikanS1,
                stf.LulusS1, stf.PendidikanS2, stf.LulusS2, stf.PendidikanS3, stf.LulusS3, stf.PendidikanTerakhir, stf.TTDImage, 
                stf.TugasTambahan, stf.Departemen, stf.NIDN,
                (CASE WHEN stf.Laboratorium IS NOT NULL AND stf.Laboratorium != ''
                THEN (SELECT Nama
                        FROM AkademikLaboratorium
                        WHERE Id = Laboratorium)
                ELSE '-' END) AS Lab
                from Users usr
                join UserTenagaKependidikan stf on usr.Id = stf.UserId
                where usr.Id='$UserId'
                ";
        $Dosen = $this->db->query($query)->row_array();

        $data['title'] = $Dosen['Name'];
        $data['Dosen'] = $Dosen;

        $Id = $this->session->userdata['Id'];
        $query = "select * from Users where Id = $Id";
        $User = $this->db->query($query)->row_array();
        $data['User'] = $User;

        /* Ambil Data lab */
        $queryLab = "
                    select * 
                    from AkademikLaboratorium 
                    where Status = 1 AND IsDeleted = 0";
        $Lab = $this->db->query($queryLab)->result_array();
        $data['Lab'] = $Lab;

        /* Setting kembali */
        $data['url_kembali'] = get_referer_url();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('sdm/DetailDosen', $data);
        $this->load->view('templates/footer');
    }

    public function EditDosen(){
        $userId = htmlspecialchars($this->input->post('id'));
        $requestUser = [
            'Name' => htmlspecialchars($this->input->post('name')),
            'Telpon' => htmlspecialchars($this->input->post('telpon')),
            'Email' => htmlspecialchars($this->input->post('email')),
            'AlamatMalang' => htmlspecialchars($this->input->post('alamatMalang')),
            'Alamat' => htmlspecialchars($this->input->post('alamat')),
        ];

        if(htmlspecialchars($this->input->post('password'))){
            $requestUser['Password'] = password_hash(htmlspecialchars($this->input->post('password')), PASSWORD_DEFAULT);
        }
        $this->db->where('Id', $userId)->update('Users', $requestUser);

        $query = "
                    select dsn.*, 
                    usr.Id, usr.Name, usr.Username, usr.Telpon, usr.Email, usr.RoleId, usr.CreatedAt, usr.Status, usr.ImageProfile, usr.AlamatMalang, usr.Alamat
                    from Users usr
                    join UserTenagaKependidikan dsn on usr.Id = dsn.UserId
                    where usr.Id = '$userId'
                    ";

        $User = $this->db->query($query)->row_array();

        /* Update Dosen */
        $requestDosen = [
            'NIDN' => htmlspecialchars($this->input->post('nidn')),
            'Laboratorium' => htmlspecialchars($this->input->post('laboratorium')),
            'Bidang' => htmlspecialchars($this->input->post('bidang')),
            'Golongan' => htmlspecialchars($this->input->post('golongan')),
            'Pangkat' => htmlspecialchars($this->input->post('pangkat')),
            'Jabatan' => htmlspecialchars($this->input->post('jabatan')),
            'TugasTambahan' => htmlspecialchars($this->input->post('tugasTambahan')),
            'PendidikanS1' => htmlspecialchars($this->input->post('pendidikanS1')),
            'LulusS1' => htmlspecialchars($this->input->post('lulusS1')),
            'PendidikanS2' => htmlspecialchars($this->input->post('pendidikanS2')),
            'LulusS2' => htmlspecialchars($this->input->post('lulusS2')),
            'PendidikanS3' => htmlspecialchars($this->input->post('pendidikanS3')),
            'LulusS3' => htmlspecialchars($this->input->post('lulusS3')),
        ];

        $IjazahS1 = $_FILES['ijazahS1']['name'];
        if($IjazahS1){
            $config['upload_path']          = './assets/img/ijazah/';
            $config['allowed_types']        = 'jpg|png';
            $config['max_size']             = 5000;

            $this->load->library('upload', $config);

            if($this->upload->do_upload('ijazahS1')){
                /* unlink old Image */
                unlink('./assets/img/ijazah/'.$User['IjazahS1']);

                /* Upload New Image */
                $data = $this->upload->data();
                $uploaded_filename = $data['file_name'];
                $extention = pathinfo($uploaded_filename, PATHINFO_EXTENSION);
                $source_path = './assets/img/ijazah/' . $uploaded_filename;
                $new_filename = $userId.'-IjazahS1.'.$extention; // Change this to your desired new name
                $new_path = './assets/img/ijazah/' . $new_filename;

                if (rename($source_path, $new_path)) {
                    $requestDosen['IjazahS1'] = $new_filename;
                }
            }
        }

        $IjazahS2 = $_FILES['ijazahS2']['name'];
        if($IjazahS2){
            $config['upload_path']          = './assets/img/ijazah/';
            $config['allowed_types']        = 'jpg|png';
            $config['max_size']             = 5000;

            $this->load->library('upload', $config);

            if($this->upload->do_upload('ijazahS2')){
                /* unlink old Image */
                unlink('./assets/img/ijazah/'.$User['IjazahS2']);

                /* Upload New Image */
                $data = $this->upload->data();
                $uploaded_filename = $data['file_name'];
                $extention = pathinfo($uploaded_filename, PATHINFO_EXTENSION);
                $source_path = './assets/img/ijazah/' . $uploaded_filename;
                $new_filename = $userId.'-IjazahS2.'.$extention; // Change this to your desired new name
                $new_path = './assets/img/ijazah/' . $new_filename;

                if (rename($source_path, $new_path)) {
                    $requestDosen['IjazahS2'] = $new_filename;
                }
            }
        }

        $IjazahS3 = $_FILES['ijazahS3']['name'];
        if($IjazahS3){
            $config['upload_path']          = './assets/img/ijazah/';
            $config['allowed_types']        = 'jpg|png';
            $config['max_size']             = 5000;

            $this->load->library('upload', $config);

            if($this->upload->do_upload('ijazahS3')){
                /* unlink old Image */
                unlink('./assets/img/ijazah/'.$User['IjazahS3']);

                /* Upload New Image */
                $data = $this->upload->data();
                $uploaded_filename = $data['file_name'];
                $extention = pathinfo($uploaded_filename, PATHINFO_EXTENSION);
                $source_path = './assets/img/ijazah/' . $uploaded_filename;
                $new_filename = $userId.'-IjazahS3.'.$extention; // Change this to your desired new name
                $new_path = './assets/img/ijazah/' . $new_filename;

                if (rename($source_path, $new_path)) {
                    $requestDosen['IjazahS3'] = $new_filename;
                }
            }
        }

        $this->db->where('UserId', $userId)->update('UserTenagaKependidikan', $requestDosen);

        $this->DetailDosen($userId);
    }

    public function DataDosenByName($Name = null){
        if ($Name == null){
            $Name = $this->input->get('Name');
        }

        $query = "
                select * from Users
                where RoleId in ('1','5') AND Name like '%$Name%' or Id = $Name AND Status = 1
                ";

        $Dosen = $this->db->query($query)->result_array();

        return $Dosen;
    }

    public function DeleteDosen($UserId = null){
        $requestData = [
            'Status' => 2
        ];

        $this->db->where('Id', $UserId)->update('Users', $requestData);

        redirect('sdm/DataStaff');
    }

    public function ResetPasswordDosen($UserId = null){
        /* Get Data User By Id */
        $query = "select * from Users where Id = $UserId";
        $UserData = $this->db->query($query)->row_array();

        $requestData = [
            'ChangedPassword' => 0,
            'Password' => password_hash($UserData['Username'], PASSWORD_DEFAULT),
        ];

        $this->db->where('Id', $UserId)->update('Users', $requestData);
        
        redirect('sdm/DetailDosen/'.$UserId);
    }

    public function UploadProfilePhotoDosen(){
        $UserId = $this->input->post('userId');
        $upload_image = $_FILES['image']['name'];

        if($upload_image){
            $config['upload_path']          = './assets/img/profiles/';
            $config['allowed_types']        = 'jpg|png|jpeg';
            $config['max_size']             = 5000;

            $this->load->library('upload', $config);

            if($this->upload->do_upload('image')){
                $User = $this->db->get_where('Users', ['Id' => $UserId])->row_array();

                /* Upload New Image */
                $data = $this->upload->data();
                $uploaded_filename = $data['file_name'];
                $extention = pathinfo($uploaded_filename, PATHINFO_EXTENSION);
                $source_path = './assets/img/profiles/' . $uploaded_filename;
                $new_filename = $UserId.'-Profile.'.$extention; // Change this to your desired new name
                $new_path = './assets/img/profiles/' . $new_filename;

                if (rename($source_path, $new_path)) {
                    if ($User['ImageProfile'] != "default.jpg" && $User['ImageProfile'] != $new_filename){
                        unlink(FCPATH . 'assets/img/profiles/'. $User['ImageProfile']);
                    }

                    $this->db->set('ImageProfile', $new_filename)->where('Id', $UserId)->update('Users');

                    $this->DetailDosen($UserId);
                }
            }else{
                echo $this->upload->display_errors();
            }
        }
    }

    public function UploadTTDDosen(){
        $UserId = $this->input->post('userId');
        $upload_image = $_FILES['image']['name'];

        if($upload_image){
            $config['upload_path']          = './assets/img/signs/';
            $config['allowed_types']        = 'png';
            $config['max_size']             = 5000;

            $this->load->library('upload', $config);

            if($this->upload->do_upload('image')){
                $query = "
                            select dsn.*, 
                            usr.Id, usr.Name, usr.Username, usr.Telpon, usr.Email, usr.RoleId, usr.CreatedAt, usr.Status, usr.ImageProfile, usr.AlamatMalang, usr.Alamat
                            from Users usr
                            join UserTenagaKependidikan dsn on usr.Id = dsn.UserId
                            where usr.Id = '$UserId'
                            ";
    
                $User = $this->db->query($query)->row_array();

                /* Upload New Image */
                $data = $this->upload->data();
                $uploaded_filename = $data['file_name'];
                $extention = pathinfo($uploaded_filename, PATHINFO_EXTENSION);
                $source_path = './assets/img/signs/' . $uploaded_filename;
                $new_filename = $UserId.'-TTD.'.$extention; // Change this to your desired new name
                $new_path = './assets/img/signs/' . $new_filename;

                if (rename($source_path, $new_path)) {
                    if ($User['TTDImage'] != "default.jpg" && $User['TTDImage'] != $new_filename){
                        unlink(FCPATH . 'assets/img/signs/'. $User['TTDImage']);
                    }

                    $this->db->set('TTDImage', $new_filename)->where('Id', $UserId)->update('UserTenagaKependidikan');

                    $this->DetailDosen($UserId);
                }
            }else{
                
                $message = '<div class="alert alert-danger">Please Select File</div>';

                $this->DetailDosen($UserId);
            }
        }
    }

    /* Staff */
    public function DataStaff($Staff = null){
        $data['title'] = 'Data Tenaga Kependidikan';
        $Id = $this->session->userdata['Id'];
        $query = "select * from Users where Id = $Id";
        $User = $this->db->query($query)->row_array();
        $data['User'] = $User;
        
        /* Query Data */
        $queryStaff = "
                        select usr.Id, usr.Name, stf.Jabatan, stf.Bagian
                        from Users usr
                        join UserTenagaKependidikan stf on usr.Id = stf.UserId
                        where usr.Status != 2 AND usr.RoleId in ('5','6')
                        ";

        /* proses Filter ketika Pengambilan Data */
        if($this->input->post('search') != NULL){
            $search = $this->input->post('search');
            $queryStaff = $queryStaff . " AND usr.Name LIKE '%$search%'"; 
        }

        if($this->input->post('limit') != NULL){
            $limit = $this->input->post('limit');
            $queryStaff = $queryStaff . " limit $limit"; 
        }
        
        /* Proses Eksekusi Query */
        $Staff = $this->db->query($queryStaff)->result_array();

        $data['Staff'] = $Staff;

        /* Ambil Data lab */
        $queryLab = "
                    select * 
                    from AkademikLaboratorium 
                    where Status = 1 AND IsDeleted = 0";
        $Lab = $this->db->query($queryLab)->result_array();
        $data['Lab'] = $Lab;

        /* Setting kembali */
        $data['url_kembali'] = get_referer_url();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('sdm/DataStaff', $data);
        $this->load->view('templates/footer');
    }

    public function TambahStaff(){

        $datetime = new DateTime();
        $timezone = new DateTimeZone('Asia/Jakarta'); 

        $datetime->setTimezone($timezone);

        $NIP = htmlspecialchars($this->input->post('NIP'));
        $email = htmlspecialchars($this->input->post('email'));

        if ($NIP != null){
            $password = password_hash($NIP, PASSWORD_DEFAULT);
        }else{
            $password = password_hash($email, PASSWORD_DEFAULT);
        }

        if ($this->input->post('isPLP') == 1){
            $RoleId = 6;
            $Lab = htmlspecialchars($this->input->post('laboratorium'));
        }else{
            $RoleId = 5;
        }

        $requestData = [
            'Name' => htmlspecialchars($this->input->post('name')),
            'Username' => $NIP,
            'Email' => $email,
            'NIP' => $NIP,
            'RoleId' => $RoleId,
            'CreatedAt' => $datetime->format("Y-m-d H:i:s"),
            'ChangedPassword' => 0,
            'Status' => 1,
            'ImageProfile' => 'default.jpg',
            'Password' => $password,
        ];

        $this->db->insert('Users', $requestData);

        if($NIP != null){
            $User = $this->db->get_where('Users', ['Username' => $NIP])->row_array();
        }else{
            $User = $this->db->get_where('Users', ['Email' => $email])->row_array();
        }
        
        $requestDataDosen = array(
            'UserId' => $User['Id'],
            'Departemen' => "Kimia",
            'IsStaff' => 1,
            'Bagian' => htmlspecialchars($this->input->post('bagian')),
            'Jabatan' => htmlspecialchars($this->input->post('jabatan')),
            'Laboratorium' => $Lab,
            'IsKimia' => 1,
            'Institusi' => "-",
        );

        $this->db->insert('UserTenagaKependidikan', $requestDataDosen);

        $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
            Dosen Berhasil Ditambahkan
        </div>');

        redirect('sdm/DataStaff');
    }

    public function DetailStaff(){
        $UserId = $this->input->post('UserId');

        $queryTendik = "
                        select 
                        usr.*,
                        stf.Golongan, stf.Pangkat, stf.Jabatan, stf.Bagian, stf.PendidikanTerakhir, stf.Departemen, stf.Laboratorium,
                        (CASE WHEN stf.laboratorium IS NOT NULL OR stf.laboratorium != 0
                        THEN (SELECT lab.Nama
                                FROM AkademikLaboratorium lab
                                WHERE Id = stf.laboratorium)
                        ELSE NULL END) AS NamaLab
                        from Users usr
                        join UserTenagaKependidikan stf on usr.Id = stf.Userid
                        where usr.Id = $UserId
                        ";

        $Tendik = $this->db->query($queryTendik)->row_array();

        echo json_encode($Tendik);
    }

    public function EditStaff(){
        $UserId = htmlspecialchars($this->input->post('UserId'));

        $requestUser = [
            'Name' => htmlspecialchars($this->input->post('name')),
            'Email' => htmlspecialchars($this->input->post('email'))
        ];

        $this->db->update('Users', $requestUser, "Id = $UserId");

        $requestTendik = [
            'Golongan' => htmlspecialchars($this->input->post('golongan')),
            'Pangkat' => htmlspecialchars($this->input->post('pangkat')),
            'Jabatan' => htmlspecialchars($this->input->post('jabatan')),
            'Bagian' => htmlspecialchars($this->input->post('bagian')),
            'PendidikanTerakhir' => htmlspecialchars($this->input->post('pendidikan')),
            'Departemen' => htmlspecialchars($this->input->post('departemen')),
            'Laboratorium' => htmlspecialchars($this->input->post('laboratorium')),
        ];

        $this->db->update('UserTenagaKependidikan', $requestTendik, "UserId = $UserId");

        redirect('sdm/DataStaff');
    }

    public function DeleteStaff($UserId = null){
        $requestData = [
            'Status' => 2
        ];

        $this->db->where('Id', $UserId)->update('Users', $requestData);

        redirect('sdm/DataStaff');
    }

    public function ResetPasswordStaff($UserId = null){
        /* Get Data User By Id */
        $query = "select * from Users where Id = $UserId";
        $UserData = $this->db->query($query)->row_array();

        $requestData = [
            'ChangedPassword' => 0,
            'Password' => password_hash($UserData['Email'], PASSWORD_DEFAULT),
        ];

        $this->db->where('Id', $UserId)->update('Users', $requestData);
        
        redirect('sdm/DataStaff');
    }

    public function importStaff(){ 
        if($_FILES["importexcel"]["name"] != ''){
            $allowed_extension = array('xls', 'xlsx', '.csv');
            $file_array = explode(".", $_FILES['importexcel']['name']);
            $file_extension = end($file_array);

            if(in_array($file_extension, $allowed_extension)){
                $file_name = time() . '.' . $file_extension; // rename file
                move_uploaded_file($_FILES['importexcel']['tmp_name'], $file_name); // save file in directory
                $file_type = \PhpOffice\PhpSpreadsheet\IOFactory::identify($file_name); 
                $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($file_type);
                $spreadsheet = $reader->load($file_name); // get data file
                unlink($file_name); // hapus file
                $data = $spreadsheet->getActiveSheet()->toArray(); // cek sheet
                $sheetcount=count($data); // menjumlah data sheet

                $datetime = new DateTime();
                $timezone = new DateTimeZone('Asia/Jakarta'); 
        
                $datetime->setTimezone($timezone);
                /* proses pengambilan data */
                for($i=1; $i<$sheetcount; $i++){
                    /* cek data excel */
                    if ($data[$i][3] != null){
                        $email = $data[$i][3];

                        /* Cek Duplicate Data */
                        $queryCekData = "select * from Users where Email = '$email'";
                        $duplicateData = $this->db->query($queryCekData)->row_array();

                        if ($duplicateData == null){
                            $requestDataUser = array(
                                'RoleId' => 5,
                                'Name' => $data[$i][1],
                                'CreatedAt' => $datetime->format("Y-m-d H:i:s"),
                                'ChangedPassword' => 0,
                                'Status' => 1,
                                'ImageProfile' => 'default.jpg',
                                'Email' => $email,
                            );
                            
                            $password = $email;
        
                            $requestDataUser['Password'] = password_hash($password, PASSWORD_DEFAULT);
        
                            $this->db->insert('Users', $requestDataUser);

                            $query = "select * from Users where Email='$email'";
                            $User = $this->db->query($query)->row_array();

                            $requestDataStaff = array(
                                'UserId' => $User['Id'],
                                'Golongan' => $data[$i][4],
                                'Pangkat' => $data[$i][5],
                                'Jabatan' => $data[$i][6],
                                'Departemen' => $data[$i][8],
                                'PendidikanTerakhir' => $data[$i][7],
                                'IsStaff' => 1,
                                'Bagian' => "-",
                            );

                            $this->db->insert('UserTenagaKependidikan', $requestDataStaff);
                        }
                    }
                }
                $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
                    Data Dosen Berhasil Ditambahkan
                </div>');

                unlink($file_name);

                redirect('sdm/DataStaff');
            }else{
                $message = '<div class="alert alert-danger">Only .xls or .xlsx file allowed</div>';
            }
        }else{
            $message = '<div class="alert alert-danger">Please Select File</div>';
        }
    }

    /* Mahasiswa Non Kimia */
    public function DataMahasiswaEksternal($Mahasiswa = null){
        $data['title'] = 'Data Mahasiswa non Kimia';

        $Id = $this->session->userdata['Id'];
        $query = "select * from Users where Id = $Id";
        $User = $this->db->query($query)->row_array();
        $data['User'] = $User;

        /* Penyimpanan Request Data */
        $RequestData = [];

        /* Query Data */
        $queryMahasiswa = "select usr.Id, usr.Username, usr.Name, usr.Status, usr.RoleId, mhs.Status, usr.Telpon, usr.Email, 
                            mhs.Angkatan, mhs.Fakultas, mhs.ProgramStudi
                            from users usr
                            join UserMahasiswa mhs on usr.Id = mhs.UserId
                            where usr.Status != 2 AND mhs.IsKimia = 0";

        if($this->input->post('jenjang') != NULL){
            $jenjang = $this->input->post('jenjang');
            $queryMahasiswa = $queryMahasiswa . " AND RoleId = '$jenjang'";
            $RequestData['Jenjang'] = $jenjang;
        }else{
            $jenjang = 2;
            $queryMahasiswa = $queryMahasiswa . " AND RoleId = '$jenjang'";
            $RequestData['Jenjang'] = $jenjang;
        }

        if($this->input->post('search') != NULL){
            $search = $this->input->post('search');
            $queryMahasiswa = $queryMahasiswa . " AND usr.Name LIKE '%$search%' OR usr.Username LIKE '%$search%'"; 
        }

        if($this->input->post('limit') != NULL){
            $limit = $this->input->post('limit');
            $queryMahasiswa = $queryMahasiswa . " limit $limit"; 
            $RequestData['Limit'] = $limit;
        }
        
        $data['RequestData'] = $RequestData;

        /* Proses Eksekusi Query */
        $data['Mahasiswa'] = $this->db->query($queryMahasiswa)->result_array();

        /* Mengambil Data Angkatan Mahasiswa */
        $queryAngkatan = "select Angkatan from UserMahasiswa group by Angkatan";
        $data['Year'] = $this->db->query($queryAngkatan)->result_array();
        
        /* Setting kembali */
        $data['url_kembali'] = get_referer_url();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('sdm/DataMahasiswaEksternal', $data);
        $this->load->view('templates/footer');
    }

    public function TambahMahasiswaEksternal(){
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('username', 'Username', 'required|is_unique[Users.Username]',[
            'is_unique' => 'This Username has already registered!'
        ]);
        $this->form_validation->set_rules('angkatan', 'Angkatan', 'required|max_length[4]',[
            'max_length' => 'Tahun angkatan tidak valid'
        ]);
        $this->form_validation->set_rules('role_id', 'RoleId', 'required');

        $datetime = new DateTime();
        $timezone = new DateTimeZone('Asia/Jakarta'); 

        $datetime->setTimezone($timezone);

        if ($this->form_validation->run() == false){
            $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
                Gagal Menambahkan Mahasiswa
            </div>');
            redirect('sdm/DataMahasiswa');
        }else{
            $requestData = [
                'Name' => htmlspecialchars($this->input->post('name')),
                'Username' => htmlspecialchars($this->input->post('username')),
                'Password' => password_hash($this->input->post('username'), PASSWORD_DEFAULT),
                'RoleId' => htmlspecialchars($this->input->post('role_id')),
                'CreatedAt' => $datetime->format("Y-m-d H:i:s"),
                'ChangedPassword' => 0,
                'Status' => 1,
                'ImageProfile' => 'default.jpg'
            ];

            $this->db->insert('Users', $requestData);

            $User = $this->db->get_where('Users', ['Username' => htmlspecialchars($this->input->post('username'))])->row_array();
            
            $modifDate = new DateTime('now');
            $modifDate->modify('+6 month'); // or you can use '-90 day' for deduct
            $modifDate = $modifDate->format('Y-m-d h:i:s');
            
            $requestDataMahasiswa = array(
                'UserId' => $User['Id'],
                'Angkatan' => htmlspecialchars($this->input->post('angkatan')),
                'Status' => 'Aktif',
                'Fakultas' => ucwords(htmlspecialchars($this->input->post('fakultas'))),
                'ProgramStudi' => ucwords(htmlspecialchars($this->input->post('programStudi'))),
                'IsKimia' => 0,
                'ActiveDate' => $modifDate,
            );

            $this->db->insert('UserMahasiswa', $requestDataMahasiswa);

            $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
                Mahasiswa Berhasil Ditambahkan
            </div>');

            redirect('sdm/DatamahasiswaEksternal');
        }
    }
}