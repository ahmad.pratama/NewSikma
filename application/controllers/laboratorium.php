<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laboratorium extends CI_Controller {
    
    // public function __construct(){
    //     parent::__construct();
    //     $this->load->library('form_validation');
    //     if (!$this->session->userdata('Id')){
    //         redirect('auth');
    //     }
    // }

    public function DataAlat(){
        $UserId = $this->session->userdata['Id'];

        $roleId = $this->session->userdata['RoleId'];
        $tendik = [1,5,6];
        
        $query = "select * from Users where Id = $UserId";

        if(in_array($roleId,$tendik)){
            $query = "
                    select 
                    usr.*,
                    tendik.Laboratorium
                    from Users usr
                    join UserTenagaKependidikan tendik on usr.Id = tendik.UserId
                    where usr.Id = $UserId
                    ";
        }

        $User = $this->db->query($query)->row_array();
        $data['User'] = $User;

        /* Ambil data Lab */
        $queryLab = "
                    select * from AkademikLaboratorium
                    where Status = 1 AND IsDeleted = 0
                    ";
        
        $Laboratorium = $this->db->query($queryLab)->result_array();
        $data['Laboratorium'] = $Laboratorium;

        /* Setting kembali */
        $data['url_kembali'] = get_referer_url();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('laboratorium/DataAlat', $data);
        $this->load->view('templates/footer');
    }

    public function DataBahan(){
        $UserId = $this->session->userdata['Id'];

        $roleId = $this->session->userdata['RoleId'];
        $tendik = [1,5,6];
        
        $query = "select * from Users where Id = $UserId";

        if(in_array($roleId,$tendik)){
            $query = "
                    select 
                    usr.*,
                    tendik.Laboratorium
                    from Users usr
                    join UserTenagaKependidikan tendik on usr.Id = tendik.UserId
                    where usr.Id = $UserId
                    ";
        }

        $User = $this->db->query($query)->row_array();
        $data['User'] = $User;

        /* Ambil data Lab */
        $queryLab = "
                    select * from AkademikLaboratorium
                    where Status = 1 AND IsDeleted = 0
                    ";
        
        $Laboratorium = $this->db->query($queryLab)->result_array();
        $data['Laboratorium'] = $Laboratorium;

        /* Setting kembali */
        $data['url_kembali'] = get_referer_url();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('laboratorium/DataBahan', $data);
        $this->load->view('templates/footer');
    }

    /* Master Alat Kimia */
    public function ListMasterAlat(){
        $UserId = $this->session->userdata['Id'];

        $roleId = $this->session->userdata['RoleId'];
        $tendik = [1,5,6];
        
        $query = "select * from Users where Id = $UserId";

        if(in_array($roleId,$tendik)){
            $query = "
                    select 
                    usr.*,
                    tendik.Laboratorium
                    from Users usr
                    join UserTenagaKependidikan tendik on usr.Id = tendik.UserId
                    where usr.Id = $UserId
                    ";
        }

        $User = $this->db->query($query)->row_array();
        $data['User'] = $User;

        /* Ambil Data Master Alat */
        $queryListAlat = "
                        select * from MasterAlat
                        ";
        $ListAlat = $this->db->query($queryListAlat)->result_array();

        $data['ListAlat'] = $ListAlat;

        /* Setting kembali */
        $data['url_kembali'] = get_referer_url();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('laboratorium/ListAlat', $data);
        $this->load->view('templates/footer');
    }

    public function TambahMasterAlat(){
        $datetime = new DateTime();
        $timezone = new DateTimeZone('Asia/Jakarta'); 

        $datetime->setTimezone($timezone);

        $request = [
            'Nama' => $this->input->post('nama'),
            'Merk' => $this->input->post('merk'),
            'Status' => 1,
            'IsDeleted' => 0,
            'CreatedAt' => $datetime->format("Y-m-d H:i:s"),
        ];

        $this->db->insert('MasterAlat', $request);

        redirect('Laboratorium/ListMasterAlat');
    }

    public function DetailMasterAlat(){
        $AlatId = $this->input->post('AlatId');

        $queryMasterAlat = "
                            select * from MasterAlat
                            where Id = $AlatId
                            ";

        $MasterAlat = $this->db->query($queryMasterAlat)->row_array();

        echo json_encode($MasterAlat);
    }

    public function EditMasterAlat(){
        $AlatId = $this->input->post('alatId');

        $request = [
            'Nama' => $this->input->post('nama'),
            'Merk' => $this->input->post('merk'),
            'Status' => $this->input->post('status'),
        ];

        $this->db->update('MasterAlat', $request, ["Id" => $AlatId]);
        
        redirect('Laboratorium/ListMasterAlat');
    }

    /* Alat Masing-masing Lab Kimia */
    public function ListAlatLab($LabId = null){
        $data['title'] = 'Pendaftaran Tugas Akhir';
        
        $UserId = $this->session->userdata['Id'];

        $roleId = $this->session->userdata['RoleId'];

        $kadep = $this->session->userdata['Kadep'];
        $sekdep = $this->session->userdata['Sekdep'];
        $kaprodiS1 = $this->session->userdata['KaprodiS1'];
        $kaprodiS2 = $this->session->userdata['KaprodiS2'];
        $kaprodiS3 = $this->session->userdata['KaprodiS3'];

        $mhsRole = [2,3,4];
        $dsnRole = [1];
        if (in_array($roleId, $mhsRole)){
            $query = "
                        select mhs.*, 
                        usr.Id, usr.Name, usr.Username, usr.Telpon, usr.Email, usr.RoleId, usr.CreatedAt, usr.Status, usr.ImageProfile, usr.AlamatMalang, usr.Alamat
                        from Users usr
                        join UserMahasiswa mhs on usr.Id = mhs.UserId
                        where usr.Id = '$UserId' 
                        ";

            /* Ambil Data Alat Laboratorium */
            $queryAlatLab = "
                            select 
                            alatLab.*,
                            alat.Nama NamaAlat
                            from AlatLaboratorium alatLab
                            join MasterAlat alat on alatLab.AlatId = alat.Id
                            where alatLab.LabId = $LabId and alatLab.Status = 1
                            ";

            if($this->input->post('search') != NULL){
                $search = $this->input->post('search');
                $queryAlatLab = $queryAlatLab . " AND Nama LIKE '%$search%'"; 
            }

            if($this->input->post('limit') != NULL){
                $limit = $this->input->post('limit');
                $queryAlatLab = $queryAlatLab . " limit $limit"; 
            }

            $AlatLab = $this->db->query($queryAlatLab)->result_array();

        }else if(in_array($roleId, $dsnRole) && ($kadep == false && $sekdep == false && $kaprodiS1 == false && $kaprodiS2 == false && $kaprodiS3 == false)){
            $query = "
                        select dsn.*, 
                        usr.Id, usr.Name, usr.Username, usr.Telpon, usr.Email, usr.RoleId, usr.CreatedAt, usr.Status, usr.ImageProfile, usr.AlamatMalang, usr.Alamat
                        from Users usr
                        join UserTenagaKependidikan dsn on usr.Id = dsn.UserId
                        where usr.Id = '$UserId'
                        ";

            /* Ambil Data Alat Laboratorium */
            $queryAlatLab = "
                            select 
                            alatLab.*,
                            alat.Nama NamaAlat
                            from AlatLaboratorium alatLab
                            join MasterAlat alat on alatLab.AlatId = alat.Id
                            where alatLab.LabId = $LabId
                            ";

            if($this->input->post('search') != NULL){
                $search = $this->input->post('search');
                $queryAlatLab = $queryAlatLab . " AND Nama LIKE '%$search%'"; 
            }
                
            if($this->input->post('limit') != NULL){
                $limit = $this->input->post('limit');
                $queryAlatLab = $queryAlatLab . " limit $limit"; 
            }
            
            $AlatLab = $this->db->query($queryAlatLab)->result_array();
        }else if($roleId == 6){
            $query = "
                        select stf.*, 
                        usr.Id, usr.Name, usr.Username, usr.Telpon, usr.Email, usr.RoleId, usr.CreatedAt, usr.Status, usr.ImageProfile, usr.AlamatMalang, usr.Alamat
                        from Users usr
                        join UserTenagaKependidikan stf on usr.Id = stf.UserId
                        where usr.Id = '$UserId'
                        ";

            /* Ambil Data Alat Laboratorium */
            $queryAlatLab = "
                            select 
                            alatLab.*,
                            alat.Nama NamaAlat
                            from AlatLaboratorium alatLab
                            join MasterAlat alat on alatLab.AlatId = alat.Id
                            where alatLab.LabId = $LabId
                            ";

            if($this->input->post('search') != NULL){
                $search = $this->input->post('search');
                $queryAlatLab = $queryAlatLab . " AND Nama LIKE '%$search%'"; 
            }
                
            if($this->input->post('limit') != NULL){
                $limit = $this->input->post('limit');
                $queryAlatLab = $queryAlatLab . " limit $limit"; 
            }
            
            $AlatLab = $this->db->query($queryAlatLab)->result_array();
        }else{
            $query = "select * from Users where Id = '$UserId'";
            
            /* Ambil Data Alat Laboratorium */
            $queryAlatLab = "
                            select 
                            alatLab.*,
                            alat.Nama NamaAlat
                            from AlatLaboratorium alatLab
                            join MasterAlat alat on alatLab.AlatId = alat.Id
                            where alatLab.LabId = $LabId
                            ";

            if($this->input->post('search') != NULL){
                $search = $this->input->post('search');
                $queryAlatLab = $queryAlatLab . " AND Nama LIKE '%$search%'"; 
            }
                
            if($this->input->post('limit') != NULL){
                $limit = $this->input->post('limit');
                $queryAlatLab = $queryAlatLab . " limit $limit"; 
            }
            
            $AlatLab = $this->db->query($queryAlatLab)->result_array();
        }

        $User = $this->db->query($query)->row_array();
        $data['User'] = $User;

        // var_dump($User);
        $data['AlatLab'] = $AlatLab;
        $data['LabId'] = $LabId;

        $queryKalab = "
                        select * from AkademikLaboratorium 
                        where Status = 1 and IsDeleted = 0
                        ";
        $Kalab = $this->db->query($queryKalab)->result_array();
        $data['Kalab'] = $Kalab;

        /* Ambil Data Master Alat */
        $queryListAlat = "
                        select * from MasterAlat
                        where Status = 1 And IsDeleted = 0
                        ";
        $ListAlat = $this->db->query($queryListAlat)->result_array();

        $data['ListAlat'] = $ListAlat;

        /* Setting kembali */
        $data['url_kembali'] = get_referer_url();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('laboratorium/ListAlatLab', $data);
        $this->load->view('templates/footer');
    }

    public function TambahAlatLab(){
        $datetime = new DateTime();
        $timezone = new DateTimeZone('Asia/Jakarta'); 

        $datetime->setTimezone($timezone);

        $LabId = $this->input->post('labId');

        $request = [
            'LabId' => $LabId,
            'Alatid' => $this->input->post('alat'),
            'StokPermanen' => $this->input->post('stok'),
            'Stok' => $this->input->post('stok'),
            'Status' => 1,
            'CreatedAt' => $datetime->format("Y-m-d H:i:s"),
        ];

        $this->db->insert('AlatLaboratorium', $request);

        redirect('Laboratorium/ListAlatLab/' . $LabId);
    }

    public function DetailAlatLab(){
        $AlatLabId = $this->input->post('AlatLabId');

        $queryAlatLab = "
                            select alatLab.*,
                            alat.Nama NamaAlat
                            from AlatLaboratorium alatLab
                            join MasterAlat alat on alatLab.AlatId = alat.Id
                            where alatLab.Id = $AlatLabId
                            ";

        $AlatLab = $this->db->query($queryAlatLab)->row_array();

        echo json_encode($AlatLab);
    }

    public function EditAlatLab(){
        $alatLabId = $this->input->post('alatLabId');
        $labId = $this->input->post('labId');

        $request = [
            'Status' => $this->input->post('status'),
        ];

        $this->db->update('AlatLaboratorium', $request, ["Id" => $alatLabId]);
        
        redirect('Laboratorium/ListAlatLab/'.$labId);
    }

    /* Stok Masing-masing Alat Lab Kimia */
    public function StokAlatLab($AlatLabId = null){
        $data['title'] = 'Pendaftaran Tugas Akhir';
        
        $UserId = $this->session->userdata['Id'];

        $roleId = $this->session->userdata['RoleId'];

        $kadep = $this->session->userdata['Kadep'];
        $sekdep = $this->session->userdata['Sekdep'];
        $kaprodiS1 = $this->session->userdata['KaprodiS1'];
        $kaprodiS2 = $this->session->userdata['KaprodiS2'];
        $kaprodiS3 = $this->session->userdata['KaprodiS3'];

        $dsnRole = [1];
        if(in_array($roleId, $dsnRole) && ($kadep == false && $sekdep == false && $kaprodiS1 == false && $kaprodiS2 == false && $kaprodiS3 == false)){
            $query = "
                        select dsn.*, 
                        usr.Id, usr.Name, usr.Username, usr.Telpon, usr.Email, usr.RoleId, usr.CreatedAt, usr.Status, usr.ImageProfile, usr.AlamatMalang, usr.Alamat
                        from Users usr
                        join UserTenagaKependidikan dsn on usr.Id = dsn.UserId
                        where usr.Id = '$UserId'
                        ";

            /* Ambil Data Alat Laboratorium */
            $queryStokAlat = "
            select 
            alat.Nama NamaAlat,
            alatLab.LabId,
            stok.*,
            (CASE WHEN stok.PengadaanId IS NOT NULL
                THEN (SELECT pengadaan.Bukti
                        FROM PengadaanAlat pengadaan
                        WHERE pengadaan.Id = stok.PengadaanId)
                ELSE NULL END) AS Bukti
            from AlatLaboratorium alatLab
            join MasterAlat alat on alatLab.AlatId = alat.Id
            left join StokAlat stok on alatLab.Id = stok.AlatLabId
            where alatLab.Id = $AlatLabId
            ";
            
            $StokAlat = $this->db->query($queryStokAlat)->result_array();

            $queryLastIdStok = "
                            select Id from StokAlat
                            where AlatLabId = $AlatLabId
                            order by Id Desc Limit 1
                            ";
            $LastIdStok = $this->db->query($queryLastIdStok)->row_array();
        }else if($roleId == 6){
            $query = "
                        select dsn.*, 
                        usr.Id, usr.Name, usr.Username, usr.Telpon, usr.Email, usr.RoleId, usr.CreatedAt, usr.Status, usr.ImageProfile, usr.AlamatMalang, usr.Alamat
                        from Users usr
                        join UserTenagaKependidikan dsn on usr.Id = dsn.UserId
                        where usr.Id = '$UserId'
                        ";

            /* Ambil Data Alat Laboratorium */
            $queryStokAlat = "
            select 
            alat.Nama NamaAlat,
            alatLab.LabId,
            stok.*,
            (CASE WHEN stok.PengadaanId IS NOT NULL
                THEN (SELECT pengadaan.Bukti
                        FROM PengadaanAlat pengadaan
                        WHERE pengadaan.Id = stok.PengadaanId)
                ELSE NULL END) AS Bukti
            from AlatLaboratorium alatLab
            join MasterAlat alat on alatLab.AlatId = alat.Id
            left join StokAlat stok on alatLab.Id = stok.AlatLabId
            where alatLab.Id = $AlatLabId
            ";
            
            $StokAlat = $this->db->query($queryStokAlat)->result_array();

            $queryLastIdStok = "
                            select Id from StokAlat
                            where AlatLabId = $AlatLabId
                            order by Id Desc Limit 1
                            ";
            $LastIdStok = $this->db->query($queryLastIdStok)->row_array();
        }else{
            $query = "select * from Users where Id = '$UserId'";
            
            /* Ambil Data Alat Laboratorium */
            $queryStokAlat = "
                            select 
                            alat.Nama NamaAlat,
                            alatLab.LabId,
                            stok.*,
                            (CASE WHEN stok.PengadaanId IS NOT NULL
                                THEN (SELECT pengadaan.Bukti
                                        FROM PengadaanAlat pengadaan
                                        WHERE pengadaan.Id = stok.PengadaanId)
                                ELSE NULL END) AS Bukti
                            from AlatLaboratorium alatLab
                            join MasterAlat alat on alatLab.AlatId = alat.Id
                            left join StokAlat stok on alatLab.Id = stok.AlatLabId
                            where alatLab.Id = $AlatLabId
                            ";
                            
            $StokAlat = $this->db->query($queryStokAlat)->result_array();

            $queryLastIdStok = "
                                select Id from StokAlat
                                order by Id Desc Limit 1
                                ";
            $LastIdStok = $this->db->query($queryLastIdStok)->row_array();
        }

        foreach($StokAlat as $key => $alat){
            switch ($alat['Jenis']){
                case 1:
                    $StokAlat[$key]['JenisText'] = "Pengadaan";
                    break;
                case 2:
                    $StokAlat[$key]['JenisText'] = "Peminjaman";
                    break;
                case 3:
                    $StokAlat[$key]['JenisText'] = "Rusak";
                    break;


                default:
                    $StokAlat[$key]['JenisText'] = "";
                    break;

            }
        }

        $User = $this->db->query($query)->row_array();
        $data['User'] = $User;

        $data['StokAlat'] = $StokAlat;
        $data['LastIdStok'] = $LastIdStok;

        /* Ambil data alat */
        $queryAlat = "
                    select 
                    alat.Nama NamaAlat, alatLab.Id
                    from AlatLaboratorium alatLab
                    join MasterAlat alat on alatLab.AlatId = alat.Id
                    where alatLab.Id = $AlatLabId
                    ";
        $Alat = $this->db->query($queryAlat)->row_array();

        $data['Alat'] = $Alat;
        
        /* Setting kembali */
        $data['url_kembali'] = get_referer_url();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('laboratorium/StokAlat', $data);
        $this->load->view('templates/footer');
    }

    public function TambahStokAlatLab(){
        $datetime = new DateTime();
        $timezone = new DateTimeZone('Asia/Jakarta'); 
        $datetime->setTimezone($timezone);

        // $AlatId = $this->input->post('alatId');
        $AlatLabId = $this->input->post('alatLabId');
        $UserId = $this->session->userdata['Id'];

        /* insert Table pengadaan Alat */
        $request = [
            'AlatLabId' => $AlatLabId,
            'AdminId' => $UserId,
            'Keterangan' => $this->input->post('keterangan'),
            'Jumlah' => $this->input->post('jumlah'),
            'Tanggal' => $this->input->post('tanggal'),
            'CreatedAt' => $datetime->format("Y-m-d H:i:s"),
        ];

        $this->db->insert('PengadaanAlat', $request);

        /* Ambil Id Terakhir */
        $queryStokAlat = "select * from PengadaanAlat where AlatLabId = $AlatLabId order by Id Desc limit 1";
        $PengadaanAlat = $this->db->query($queryStokAlat)->row_array();

        /* Update Bukti Pengadaan */
        $BuktiPengadaanAlat = $_FILES['buktiPengadaanAlat']['name'];
        if($BuktiPengadaanAlat){
            $config['upload_path']          = './assets/uploads/buktiPengadaanalat_lab/';
            $config['allowed_types']        = 'jpg|png|pdf';
            $config['max_size']             = 5000;

            $this->load->library('upload', $config);

            if($this->upload->do_upload('buktiPengadaanAlat')){
                /* Upload New Image */
                $data = $this->upload->data();
                $uploaded_filename = $data['file_name'];
                $extention = pathinfo($uploaded_filename, PATHINFO_EXTENSION);
                $source_path = './assets/uploads/buktiPengadaanalat_lab/' . $uploaded_filename;
                $new_filename = $PengadaanAlat['Id'].'-BuktiPengadaanAlat.'.$extention; // Change this to your desired new name
                $new_path = './assets/uploads/buktiPengadaanalat_lab/' . $new_filename;

                if (rename($source_path, $new_path)) {
                    $requestUpdate = ['Bukti' => $new_filename];

                    $this->db->update('PengadaanAlat', $requestUpdate, ['Id' => $PengadaanAlat['Id']]);
                }
            }
        }

        /* Ambil data Alat Laboratorium */
        $queryAlatLab = "
                        select * from AlatLaboratorium where Id = $AlatLabId
                        ";
        $AlatLab = $this->db->query($queryAlatLab)->row_array();

        /* Input Pada Table Stok */
        $requestStok = [
            'AlatLabId' => $AlatLabId,
            'Jenis' => 1,
            'JumlahAwal' => $AlatLab['Stok'],
            'JumlahPerubah' => $this->input->post('jumlah'),
            'Keterangan' => $this->input->post('keterangan'),
            'Tanggal' => $this->input->post('tanggal'),
            'CreatedAt' => $datetime->format("Y-m-d H:i:s"),
            'PengadaanId' => $PengadaanAlat['Id']
        ];

        $this->db->insert('StokAlat', $requestStok);

        /* Ambil data Stok berdasarkan Id Terakhir */
        $queryStok = "select * from StokAlat where AlatLabId = $AlatLabId order by Id Desc limit 1";
        $Stok = $this->db->query($queryStok)->row_array();

        $newStok = $AlatLab['Stok'] + $Stok['JumlahPerubah'];

        $newStokPermanen = $AlatLab['StokPermanen'] + $Stok['JumlahPerubah'];

        $newStokBaik = $AlatLab['JumlahBaik'] + $Stok['JumlahPerubah'];

        $requestUpdateAlatLab = [
            'Stok' => $newStok,
            'StokPermanen' => $newStokPermanen,
            'JumlahBaik' => $newStokBaik
        ];

        $this->db->update('AlatLaboratorium', $requestUpdateAlatLab, ['Id' => $AlatLabId]);

        redirect('Laboratorium/StokAlatLab/' . $AlatLabId);
    }

    public function DetailStokAlatLab(){
        $StokId = $this->input->post('StokId');

        $queryJenisStok = "select * from StokAlat where Id = $StokId order by Id Desc limit 1";
        $JenisStok = $this->db->query($queryJenisStok)->row_array();

        if ($JenisStok['Jenis'] == 1){
            $queryStokAlat = "
                            select 
                            pengadaan.Keterangan, pengadaan.Bukti, pengadaan.Id PengadaanId, pengadaan.Tanggal,
                            stok.JumlahAwal, stok.JumlahPerubah, stok.Id StokId,
                            alatLab.Stok TotalStok,
                            admin.Name NamaAdmin,
                            alat.Nama NamaAlat
                            from StokAlat stok
                            join PengadaanAlat pengadaan on stok.PengadaanId = pengadaan.Id
                            join AlatLaboratorium alatLab on pengadaan.AlatLabId = alatLab.Id
                            join Users admin on pengadaan.AdminId = admin.Id
                            join MasterAlat alat on alatLab.AlatId = alat.Id
                            where stok.Id = $StokId
                                ";
        }else if ($JenisStok['Jenis'] == 2){
            $queryStokAlat = "
                            select 
                            peminjaman.Keterangan, peminjaman.Id PeminjamanId, peminjaman.Tanggal,
                            stok.JumlahAwal, stok.JumlahPerubah, Stok.Id StokId,
                            alatLab.Stok TotalStok,
                            mhs.Name NamaMhs,
                            alat.Nama NamaAlat
                            from StokAlat stok
                            join PeminjamanAlat peminjaman on stok.PeminjamanId = peminjaman.Id
                            join Users mhs on peminjaman.MhsId = mhs.Id
                            join AlatLaboratorium alatLab on stok.AlatLabId = alatLab.Id
                            join MasterAlat alat on alatLab.AlatId = alat.Id
                            where stok.Id = $StokId
                            ";
        }else{
            $queryStokAlat = "
                            select 
                            rusak.Id rusakId, rusak.Tanggal,
                            stok.JumlahAwal, stok.JumlahPerubah, Stok.Id StokId,
                            alatLab.Stok TotalStok,
                            alat.Nama NamaAlat
                            from StokAlat stok
                            join PelaporanRusakAlat rusak on stok.PelaporanId = rusak.Id
                            join AlatLaboratorium alatLab on stok.AlatLabId = alatLab.Id
                            join MasterAlat alat on alatLab.AlatId = alat.Id
                            where stok.Id = $StokId
                            ";
        }

        $StokAlat = $this->db->query($queryStokAlat)->row_array();

        echo json_encode($StokAlat);
    }

    public function EditStokAlatLab(){
        $datetime = new DateTime();
        $timezone = new DateTimeZone('Asia/Jakarta'); 
        $datetime->setTimezone($timezone);

        $UserId = $this->session->userdata['Id'];
        $StokId = $this->input->post('stokId');
        $PengadaanId = $this->input->post('pengadaanId');

        $JumlahPerubah = $this->input->post('jumlah');

        /* Ambil data Stok Alat */
        $queryStok = "
                    select * from StokAlat
                    where Id = $StokId
                    ";
        $StokAlat = $this->db->query($queryStok)->row_array();

        /* Update Table pengadaan Alat */
        $request = [
            'AdminId' => $UserId,
            'Keterangan' => $this->input->post('keterangan'),
            'Jumlah' => $JumlahPerubah,
            'Tanggal' => $this->input->post('tanggal'),
            'CreatedAt' => $datetime->format("Y-m-d H:i:s"),
        ];

        $this->db->update('PengadaanAlat', $request, ['Id' => $PengadaanId]);

        /* Update Bukti Pengadaan */
        $BuktiPengadaanAlat = $_FILES['buktiPengadaanAlat']['name'];
        if($BuktiPengadaanAlat){
            $config['upload_path']          = './assets/uploads/buktiPengadaanalat_lab/';
            $config['allowed_types']        = 'jpg|png|pdf';
            $config['max_size']             = 5000;

            $this->load->library('upload', $config);

            if($this->upload->do_upload('buktiPengadaanAlat')){
                /* Upload New Image */
                $data = $this->upload->data();
                $uploaded_filename = $data['file_name'];
                $extention = pathinfo($uploaded_filename, PATHINFO_EXTENSION);
                $source_path = './assets/uploads/buktiPengadaanalat_lab/' . $uploaded_filename;
                $new_filename = $PengadaanId.'-BuktiPengadaanAlat.'.$extention; // Change this to your desired new name
                $new_path = './assets/uploads/buktiPengadaanalat_lab/' . $new_filename;

                if (rename($source_path, $new_path)) {
                    $requestUpdate = ['Bukti' => $new_filename];

                    $this->db->update('PengadaanAlat', $requestUpdate, ['Id' => $PengadaanId]);
                }
            }
        }

        $AlatLabId = $StokAlat['AlatLabId'];

        $requestStok = [
            'JumlahPerubah' => $JumlahPerubah,
            'Keterangan' => $this->input->post('keterangan')
        ];

        $this->db->update('StokAlat', $requestStok, ['Id' => $StokId]);

        /* Ambil Alat Lab */
        $queryAlatLab = "
                        select * from AlatLaboratorium
                        where Id = $AlatLabId
                        ";
        $AlatLab = $this->db->query($queryAlatLab)->row_array();
        
        if($StokAlat['Jenis'] == 1){
            $newStok = $StokAlat['JumlahAwal'] + $JumlahPerubah;

            $newStokPermanen = $newStok + $AlatLab['JumlahPinjam'] + $AlatLab['JumlahRusak'];

            $requestAlatLab = [
                'Stok' => $newStok,
                'JumlahBaik' => $newStok,
                'StokPermanen' => $newStokPermanen
            ];
        }else{
            $newStok = $StokAlat['JumlahAwal'] - $JumlahPerubah;

            $queryArrayJumlahRusak = "
                                    select JumlahPerubah from StokAlat
                                    where Jenis = 3 AND AlatLabId = $AlatLabId
                                    ";
            $ArrayJumlahRusak = $this->db->query($queryArrayJumlahRusak)->result_array();

            $JumlahRusak = 0;
            foreach($ArrayJumlahRusak as $value){
                $JumlahRusak = $JumlahRusak + $value['JumlahPerubah'];
            }

            $requestAlatLab = [
                'Stok' => $newStok,
                'JumlahBaik' => $newStok,
                'JumlahRusak' => $JumlahRusak
            ];
        }

        $this->db->update('AlatLaboratorium', $requestAlatLab, ['Id' => $AlatLabId]);

        redirect('Laboratorium/StokAlatLab/' . $AlatLabId);
    }

    public function TambahAlatLabRusak(){
        $datetime = new DateTime();
        $timezone = new DateTimeZone('Asia/Jakarta'); 
        $datetime->setTimezone($timezone);

        // $AlatId = $this->input->post('alatId');
        $AlatLabId = $this->input->post('alatLabId');
        $UserId = $this->session->userdata['Id'];

        /* insert Table pengadaan Alat */
        $request = [
            'AlatLabId' => $AlatLabId,
            'AdminId' => $UserId,
            'Jumlah' => $this->input->post('jumlah'),
            'Tanggal' => $this->input->post('tanggal'),
            'CreatedAt' => $datetime->format("Y-m-d H:i:s"),
        ];

        $this->db->insert('PelaporanRusakAlat', $request);

        /* Ambil Id Terakhir */
        $queryStokAlat = "select * from PelaporanRusakAlat where AlatLabId = $AlatLabId order by Id Desc limit 1";
        $PelaporanRusakAlat = $this->db->query($queryStokAlat)->row_array();

        /* Update Bukti Pengadaan */
        $BuktiPelaporanAlatRusak = $_FILES['buktiPelaporanAlatRusak']['name'];
        if($BuktiPelaporanAlatRusak){
            $config['upload_path']          = './assets/uploads/buktiPelaporanAlatRusak_lab/';
            $config['allowed_types']        = 'jpg|png|pdf';
            $config['max_size']             = 5000;

            $this->load->library('upload', $config);

            if($this->upload->do_upload('buktiPelaporanAlatRusak')){
                /* Upload New Image */
                $data = $this->upload->data();
                $uploaded_filename = $data['file_name'];
                $extention = pathinfo($uploaded_filename, PATHINFO_EXTENSION);
                $source_path = './assets/uploads/buktiPelaporanAlatRusak_lab/' . $uploaded_filename;
                $new_filename = $PelaporanRusakAlat['Id'].'-BuktiPelaporanAlatRusak.'.$extention; // Change this to your desired new name
                $new_path = './assets/uploads/buktiPelaporanAlatRusak_lab/' . $new_filename;

                if (rename($source_path, $new_path)) {
                    $requestUpdate = ['Bukti' => $new_filename];

                    $this->db->update('PelaporanRusakAlat', $requestUpdate, ['Id' => $PelaporanRusakAlat['Id']]);
                }
            }
        }

        /* Ambil data Alat Laboratorium */
        $queryAlatLab = "
                        select * from AlatLaboratorium where Id = $AlatLabId
                        ";
        $AlatLab = $this->db->query($queryAlatLab)->row_array();

        /* Input Pada Table Stok */
        $requestStok = [
            'AlatLabId' => $AlatLabId,
            'Jenis' => 3,
            'JumlahAwal' => $AlatLab['Stok'],
            'JumlahPerubah' => $this->input->post('jumlah'),
            'Tanggal' => $this->input->post('tanggal'),
            'CreatedAt' => $datetime->format("Y-m-d H:i:s"),
            'PelaporanId' => $PelaporanRusakAlat['Id']
        ];

        $this->db->insert('StokAlat', $requestStok);

        /* Ambil data Stok berdasarkan Id Terakhir */
        $queryStok = "select * from StokAlat where AlatLabId = $AlatLabId order by Id Desc limit 1";
        $Stok = $this->db->query($queryStok)->row_array();

        $newStok = $Stok['JumlahAwal'] - $Stok['JumlahPerubah'];
        $JumlahRusak = $AlatLab['JumlahRusak'] + $Stok['JumlahPerubah'];

        $requestUpdateAlatLab = [
            'Stok' => $newStok,
            'JumlahBaik' => $newStok,
            'JumlahRusak' => $JumlahRusak
        ];

        $this->db->update('AlatLaboratorium', $requestUpdateAlatLab, ['Id' => $AlatLabId]);

        redirect('Laboratorium/StokAlatLab/' . $AlatLabId);
    }

    /* Bon Alat */
    public function PeminjamanAlat(){
        $data['title'] = 'Peminjaman Alat';
        
        $UserId = $this->session->userdata['Id'];

        $roleId = $this->session->userdata['RoleId'];

        $kadep = $this->session->userdata['Kadep'];
        $sekdep = $this->session->userdata['Sekdep'];
        $kaprodiS1 = $this->session->userdata['KaprodiS1'];
        $kaprodiS2 = $this->session->userdata['KaprodiS2'];
        $kaprodiS3 = $this->session->userdata['KaprodiS3'];

        $mhsRole = [2,3,4];
        $dsnRole = [1];
        if (in_array($roleId, $mhsRole)){
            $query = "
                        select mhs.*, 
                        usr.Id, usr.Name, usr.Username, usr.Telpon, usr.Email, usr.RoleId, usr.CreatedAt, usr.Status, usr.ImageProfile, usr.AlamatMalang, usr.Alamat
                        from Users usr
                        join UserMahasiswa mhs on usr.Id = mhs.UserId
                        where usr.Id = '$UserId' 
                        ";

            /* Ambil Data Mahasiswa Meminjam Alat */
            $queryPeminjamAlat = "
                                select 
                                peminjaman.*,
                                (CASE WHEN peminjaman.MhsId IS NOT NULL OR peminjaman.MhsId != 0
                                    THEN (SELECT mhs.Name
                                            FROM Users mhs
                                            WHERE Id = peminjaman.MhsId)
                                    ELSE null END) AS NamaMahasiswa
                                from PeminjamanAlat peminjaman
                                where peminjaman.MhsId = $UserId
                                ";

            $PeminjamAlat = $this->db->query($queryPeminjamAlat)->result_array();

        }else if(in_array($roleId, $dsnRole) && ($kadep == false && $sekdep == false && $kaprodiS1 == false && $kaprodiS2 == false && $kaprodiS3 == false)){
            $query = "
                        select dsn.*, 
                        usr.Id, usr.Name, usr.Username, usr.Telpon, usr.Email, usr.RoleId, usr.CreatedAt, usr.Status, usr.ImageProfile, usr.AlamatMalang, usr.Alamat
                        from Users usr
                        join UserTenagaKependidikan dsn on usr.Id = dsn.UserId
                        where usr.Id = '$UserId'
                        ";
                        
            /* Ambil Data Mahasiswa Meminjam Alat */
            $queryPeminjamAlat = "
                                select 
                                peminjaman.*,
                                (CASE WHEN peminjaman.MhsId IS NOT NULL OR peminjaman.MhsId != 0
                                    THEN (SELECT mhs.Name
                                            FROM Users mhs
                                            WHERE Id = peminjaman.MhsId)
                                    ELSE null END) AS NamaMahasiswa
                                from PeminjamanAlat peminjaman
                                    ";
            $PeminjamAlat = $this->db->query($queryPeminjamAlat)->result_array();
        }else if($roleId == 6){
            $query = "
                        select dsn.*, 
                        usr.Id, usr.Name, usr.Username, usr.Telpon, usr.Email, usr.RoleId, usr.CreatedAt, usr.Status, usr.ImageProfile, usr.AlamatMalang, usr.Alamat
                        from Users usr
                        join UserTenagaKependidikan dsn on usr.Id = dsn.UserId
                        where usr.Id = '$UserId'
                        ";
                        
            /* Ambil Data Mahasiswa Meminjam Alat */
            $queryPeminjamAlat = "
                                select 
                                peminjaman.*,
                                (CASE WHEN peminjaman.MhsId IS NOT NULL OR peminjaman.MhsId != 0
                                    THEN (SELECT mhs.Name
                                            FROM Users mhs
                                            WHERE Id = peminjaman.MhsId)
                                    ELSE null END) AS NamaMahasiswa
                                from PeminjamanAlat peminjaman
                                    ";
            $PeminjamAlat = $this->db->query($queryPeminjamAlat)->result_array();
        }else{
            $query = "select * from Users where Id = '$UserId'";
            
            /* Ambil Data Mahasiswa Meminjam Alat */
            $queryPeminjamAlat = "
                                select 
                                peminjaman.*,
                                (CASE WHEN peminjaman.MhsId IS NOT NULL OR peminjaman.MhsId != 0
                                    THEN (SELECT mhs.Name
                                            FROM Users mhs
                                            WHERE Id = peminjaman.MhsId)
                                    ELSE null END) AS NamaMahasiswa
                                from PeminjamanAlat peminjaman
                                    ";
            $PeminjamAlat = $this->db->query($queryPeminjamAlat)->result_array();
        }

        foreach($PeminjamAlat as $key => $peminjaman){
            switch ($peminjaman['Tujuan']){
                case 1:
                    $PeminjamAlat[$key]['Tujuan'] = "Penelitian Dosen";
                    break;
                case 2:
                    $PeminjamAlat[$key]['Tujuan'] = "Proyek Kimia";
                    break;
                case 3:
                    $PeminjamAlat[$key]['Tujuan'] = "Program Kreatifitas Mahasiswa";
                    break;
                case 4:
                    $PeminjamAlat[$key]['Tujuan'] = "Penelitian Skripsi";
                    break;
                case 5:
                    $PeminjamAlat[$key]['Tujuan'] = "Penelitian Tesis";
                    break;
                case 6:
                    $PeminjamAlat[$key]['Tujuan'] = "Penelitian Disertasi";
                    break;
                case 7:
                    $PeminjamAlat[$key]['Tujuan'] = "Analisis Sampel";
                    break;
                case 8:
                    $PeminjamAlat[$key]['Tujuan'] = $peminjaman['TujuanLainnya'];
                    break;
            }

        }

        $User = $this->db->query($query)->row_array();
        $data['User'] = $User;

        $data['PeminjamAlat'] = $PeminjamAlat;

        /* Ambil Data Master Alat */
        $queryListAlatLab = "
                            SELECT 
                            alat.Id AlatId, 
                            lab.Id LabId, 
                            alatLab.Id AlatLabId, 
                            alatLab.Stok, 
                            alat.Nama NamaAlat, 
                            lab.Nama NamaLab 
                        FROM 
                            AlatLaboratorium alatLab 
                            JOIN MasterAlat alat ON alatLab.AlatId = alat.Id 
                            JOIN AkademikLaboratorium lab ON alatLab.LabId = lab.Id 
                        WHERE 
                            alatLab.Status = 1 
                            AND (
                            alat.Status = 1 
                            AND alat.IsDeleted = 0
                            )
                        ";
        $ListAlatLab = $this->db->query($queryListAlatLab)->result_array();

        $data['ListAlatLab'] = $ListAlatLab;

        /* Ambil data Lab */
        $queryLab = "
                    select * from AkademikLaboratorium
                    where Status = 1 AND IsDeleted = 0
                    ";
        
        $Laboratorium = $this->db->query($queryLab)->result_array();
        $data['Laboratorium'] = $Laboratorium;

        /* Setting kembali */
        $data['url_kembali'] = get_referer_url();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('laboratorium/PeminjamanAlat', $data);
        $this->load->view('templates/footer');
    }

    public function TambahPeminjamanAlat(){
        $datetime = new DateTime();
        $timezone = new DateTimeZone('Asia/Jakarta'); 
        $datetime->setTimezone($timezone);

        $UserId = $this->session->userdata['Id'];

        /* insert table Peminjaman Alat */
        $requestPeminjamanAlat = [
            'MhsId' => $UserId,
            'Tanggal' => $this->input->post('tanggal'),
            'Keterangan' => $this->input->post('keterangan'),
            'Tujuan' => $this->input->post('tujuan'),
            'TujuanLainnya' => "-",
            'Status' => 0,
            'CreatedAt' => $datetime->format("Y-m-d H:i:s"),
        ];

        if($this->input->post('tujuan') == 8){
            $requestPeminjamanAlat['TujuanLainnya'] = $this->input->post('tujuanLainnya');
        }

        $this->db->insert('PeminjamanAlat', $requestPeminjamanAlat);

        /* Ambil Id Peminjaman Alat */
        $queryIdPeminjamanAlat = "
                                select * from PeminjamanAlat
                                where MhsId = $UserId
                                order by Id Desc
                                limit 1
                                ";
        $IdPeminjamanAlat = $this->db->query($queryIdPeminjamanAlat)->row_array();

        /* Atur Request Data */
        $countAlatLabId = count($this->input->post('alatLabId'));
        $requestPeminjamanListAlat = [];
        for ($i = 1; $i < $countAlatLabId; $i++) {
            $alatLabId = $this->input->post('alatLabId')[$i];
            $alat = $this->input->post('alat')[$i - 1];
            $lab = $this->input->post('lab')[$i - 1];
            $stok = $this->input->post('stok')[$i - 1];

            if ($alatLabId != "" && $alat != "" && $lab != "" && $stok != "") {
                $requestPeminjamanListAlat[] = [
                    'PeminjamanId' => $IdPeminjamanAlat['Id'],
                    'AlatLabId' => $alatLabId,
                    'AlatId' => $alat,
                    'LabId' => $lab,
                    'Jumlah' => $stok,
                    'Status' => 0
                ];
            }
        }

        foreach($requestPeminjamanListAlat as $req){
            $AlatLabId = $req['AlatLabId'];

            /* Ambil Data Alat Laboratorium  */
            $queryAlatLab = "
                        select * from AlatLaboratorium
                        where Id = $AlatLabId
                        ";

            $AlatLab = $this->db->query($queryAlatLab)->row_array();

            /* Update data Stok pada AlatLab */
            $newstok = $AlatLab['Stok'] - $req['Jumlah'];

            $JumlahPinjam = $AlatLab['JumlahPinjam'] + $req['Jumlah'];

            if ($newstok > 0){
                /* insert table Peminjaman List Alat */
                $this->db->insert('PeminjamanListAlat', $req);

                $requestAlatLab = [
                    'Stok' => $newstok,
                    'JumlahPinjam' => $JumlahPinjam,
                    'JumlahBaik' => $newstok,
                ];
    
                $this->db->update('AlatLaboratorium', $requestAlatLab, ['Id' => $AlatLabId]);
    
                /* Memasukan data Pada table Stok */
                $requestStok = [
                    'AlatLabId' => $AlatLabId,
                    'Jenis' => 2,
                    'JumlahAwal' => $AlatLab['Stok'],
                    'JumlahPerubah' => $req['Jumlah'],
                    'Keterangan' => $this->input->post('keterangan'),
                    'Tanggal' => $this->input->post('tanggal'),
                    'CreatedAt' => $datetime->format("Y-m-d H:i:s"),
                    'PeminjamanId' => $IdPeminjamanAlat['Id']
                ];
    
                $this->db->insert('StokAlat', $requestStok);
            }
        }

        redirect('Laboratorium/PeminjamanAlat');
    }

    /* List Peminjaman Alat */
    public function PeminjamanListAlat($PeminjamanId = null){
        $data['title'] = 'Peminjaman Alat';
        
        $UserId = $this->session->userdata['Id'];

        $roleId = $this->session->userdata['RoleId'];

        $kadep = $this->session->userdata['Kadep'];
        $sekdep = $this->session->userdata['Sekdep'];
        $kaprodiS1 = $this->session->userdata['KaprodiS1'];
        $kaprodiS2 = $this->session->userdata['KaprodiS2'];
        $kaprodiS3 = $this->session->userdata['KaprodiS3'];

        $mhsRole = [2,3,4];
        $dsnRole = [1];
        if (in_array($roleId, $mhsRole)){
            $query = "
                        select mhs.*, 
                        usr.Id, usr.Name, usr.Username, usr.Telpon, usr.Email, usr.RoleId, usr.CreatedAt, usr.Status, usr.ImageProfile, usr.AlamatMalang, usr.Alamat
                        from Users usr
                        join UserMahasiswa mhs on usr.Id = mhs.UserId
                        where usr.Id = '$UserId' 
                        ";

            /* Ambil Data List Peminjaman Alat */
            $queryPeminjamanListAlat = "
                            select 
                            peminjaman.Keterangan, peminjaman.Tujuan, peminjaman.TujuanLainnya,
                            listAlat.PeminjamanId PeminjamanId, listAlat.AlatId, listAlat.LabId, listAlat.Jumlah, listAlat.Status, listAlat.Id ListAlatId,
                            alat.Nama NamaAlat,
                            lab.Nama NamaLab
                            from PeminjamanAlat peminjaman
                            join PeminjamanListAlat listAlat on peminjaman.Id = listAlat.PeminjamanId
                            join MasterAlat alat on listAlat.AlatId = alat.Id
                            join AkademikLaboratorium lab on listAlat.LabId = lab.Id
                            where peminjaman.Id = $PeminjamanId
                            order by listAlat.LabId
                            ";

            $PeminjamanListAlat = $this->db->query($queryPeminjamanListAlat)->result_array();
            
        }else if(in_array($roleId, $dsnRole) && ($kadep == false && $sekdep == false && $kaprodiS1 == false && $kaprodiS2 == false && $kaprodiS3 == false)){
            $query = "
                        select dsn.*, 
                        usr.Id, usr.Name, usr.Username, usr.Telpon, usr.Email, usr.RoleId, usr.CreatedAt, usr.Status, usr.ImageProfile, usr.AlamatMalang, usr.Alamat
                        from Users usr
                        join UserTenagaKependidikan dsn on usr.Id = dsn.UserId
                        where usr.Id = '$UserId'
                        ";

            /* Ambil Data List Peminjaman Alat */
            $queryPeminjamanListAlat = "
                            select 
                            peminjaman.Keterangan, peminjaman.Tujuan, peminjaman.TujuanLainnya,
                            listAlat.PeminjamanId PeminjamanId, listAlat.AlatId, listAlat.LabId, listAlat.Jumlah, listAlat.Status, listAlat.Id ListAlatId,
                            alat.Nama NamaAlat,
                            lab.Nama NamaLab
                            from PeminjamanAlat peminjaman
                            join PeminjamanListAlat listAlat on peminjaman.Id = listAlat.PeminjamanId
                            join MasterAlat alat on listAlat.AlatId = alat.Id
                            join AkademikLaboratorium lab on listAlat.LabId = lab.Id
                            where peminjaman.Id = $PeminjamanId
                            order by listAlat.LabId
                            ";

            $PeminjamanListAlat = $this->db->query($queryPeminjamanListAlat)->result_array();
        }else if($roleId == 6){
            $query = "
                        select dsn.*, 
                        usr.Id, usr.Name, usr.Username, usr.Telpon, usr.Email, usr.RoleId, usr.CreatedAt, usr.Status, usr.ImageProfile, usr.AlamatMalang, usr.Alamat
                        from Users usr
                        join UserTenagaKependidikan dsn on usr.Id = dsn.UserId
                        where usr.Id = '$UserId'
                        ";

            /* Ambil Data List Peminjaman Alat */
            $queryPeminjamanListAlat = "
                            select 
                            peminjaman.Keterangan, peminjaman.Tujuan, peminjaman.TujuanLainnya,
                            listAlat.PeminjamanId PeminjamanId, listAlat.AlatId, listAlat.LabId, listAlat.Jumlah, listAlat.Status, listAlat.Id ListAlatId,
                            alat.Nama NamaAlat,
                            lab.Nama NamaLab
                            from PeminjamanAlat peminjaman
                            join PeminjamanListAlat listAlat on peminjaman.Id = listAlat.PeminjamanId
                            join MasterAlat alat on listAlat.AlatId = alat.Id
                            join AkademikLaboratorium lab on listAlat.LabId = lab.Id
                            where peminjaman.Id = $PeminjamanId
                            order by listAlat.LabId
                            ";

            $PeminjamanListAlat = $this->db->query($queryPeminjamanListAlat)->result_array();
        }else{
            $query = "select * from Users where Id = '$UserId'";

            /* Ambil Data List Peminjaman Alat */
            $queryPeminjamanListAlat = "
                            select 
                            peminjaman.Keterangan, peminjaman.Tujuan, peminjaman.TujuanLainnya,
                            listAlat.PeminjamanId PeminjamanId, listAlat.AlatId, listAlat.LabId, listAlat.Jumlah, listAlat.Status, listAlat.Id ListAlatId,
                            alat.Nama NamaAlat,
                            lab.Nama NamaLab
                            from PeminjamanAlat peminjaman
                            join PeminjamanListAlat listAlat on peminjaman.Id = listAlat.PeminjamanId
                            join MasterAlat alat on listAlat.AlatId = alat.Id
                            join AkademikLaboratorium lab on listAlat.LabId = lab.Id
                            where peminjaman.Id = $PeminjamanId
                            order by listAlat.LabId
                            ";

            $PeminjamanListAlat = $this->db->query($queryPeminjamanListAlat)->result_array();
        }

        $User = $this->db->query($query)->row_array();
        $data['User'] = $User;

        $data['PeminjamanListAlat'] = $PeminjamanListAlat;

        /* Ambil Data Master Alat */
        $queryListAlatLab = "
                        select 
                        alat.Id AlatId, lab.Id LabId,  alatLab.Id AlatLabId, alatLab.Stok, 
                        alat.Nama NamaAlat,
                        lab.Nama NamaLab
                        from AlatLaboratorium alatLab
                        join MasterAlat alat on alatlab.AlatId = alat.Id
                        join AkademikLaboratorium lab on alatLab.LabId = lab.Id
                        where alatLab.Status = 1 and (alat.Status = 1 and alat.IsDeleted = 0)
                        ";
        $ListAlatLab = $this->db->query($queryListAlatLab)->result_array();

        $data['ListAlatLab'] = $ListAlatLab;

        $data['PeminjamanId'] = $PeminjamanId;

        /* Setting kembali */
        $data['url_kembali'] = get_referer_url();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('laboratorium/PeminjamanListAlat', $data);
        $this->load->view('templates/footer');
    }

    public function DetailPeminjamanListAlat(){
        $ListAlatId = $this->input->post('ListAlatId');

        $queryDetailListAlat = "
                        select
                        peminjamanlist.Id, peminjamanList.PeminjamanId, peminjamanList.AlatLabId, peminjamanList.Jumlah, peminjamanList.Status,
                        alat.Nama NamaAlat,
                        lab.Nama NamaLab
                        from PeminjamanListAlat peminjamanList
                        join MasterAlat alat on peminjamanList.AlatId = alat.Id
                        join AkademikLaboratorium lab on peminjamanList.LabId = lab.Id
                        where peminjamanList.Id = $ListAlatId
                        ";

        $DetailListAlat = $this->db->query($queryDetailListAlat)->row_array();

        echo json_encode($DetailListAlat);
    }

    public function EditPeminjamanListAlat(){
        $PeminjamanListAlatId = $this->input->post('listAlatId');
        $PeminjamanId = $this->input->post('peminjamanId');
        $AlatLabId = $this->input->post('alatLabId');

        /* Ambil AlatLab */
        $queryAlatLab = "
                        select * from AlatLaboratorium
                        where Id = $AlatLabId
                        ";
        $AlatLab = $this->db->query($queryAlatLab)->row_array();


        /* Ambil Data table stokAlat */
        $queryStokAlat = "
                        select * from StokAlat
                        where PeminjamanId = $PeminjamanId AND AlatLabId = $AlatLabId
                        ";
        
        $StokAlat = $this->db->query($queryStokAlat)->row_array();

        $newStok = $StokAlat['JumlahAwal'] - $this->input->post('jumlah');

        if($newStok > 0){
            /* Ubah Jumlah Pada Table PeminjamanListAlat */
            $requestPeminjamanListAlat = [
                'Jumlah' => $this->input->post('jumlah')
            ];

            $this->db->update('PeminjamanListAlat', $requestPeminjamanListAlat, ['Id' => $PeminjamanListAlatId]);
    
            /* Update Jumlah Perubah Pada Table StokAlat */
            $requestStokAlat = [
                'JumlahPerubah' => $this->input->post('jumlah')
            ];
    
            $this->db->update('StokAlat', $requestStokAlat, ['Id' => $StokAlat['Id']]);
    
            $queryArrayJumlahPinjam = "
                                    select JumlahPerubah from StokAlat
                                    where Jenis = 2 AND AlatLabId = $AlatLabId
                                    ";
            $ArrayJumlahPinjam = $this->db->query($queryArrayJumlahPinjam)->result_array();

            $JumlahPinjam = 0;
            foreach($ArrayJumlahPinjam as $value){
                $JumlahPinjam = $JumlahPinjam + $value['JumlahPerubah'];
            }

            /* Update Stok pada table AlatLaboratorium */
            $requestAlatLab = [
                'Stok' => $newStok,
                'JumlahBaik' => $newStok,
                'JumlahPinjam' => $JumlahPinjam
            ];
    
            $this->db->update('AlatLaboratorium', $requestAlatLab, ['Id' => $AlatLabId]);
        }

        if($this->input->post('adminId') != null && $this->input->post('status') == 1){
            $requestSelesaiPeminjamanAlat = [
                'AdminId' => $this->input->post('adminId'),
                'Status' => $this->input->post('status')
            ];

            $this->db->update('PeminjamanListAlat', $requestSelesaiPeminjamanAlat, ['Id' => $PeminjamanListAlatId]);

            $newBaik = $AlatLab['JumlahBaik'] + $StokAlat['JumlahPerubah'];

            $JumlahPinjam = $AlatLab['JumlahPinjam'] - $StokAlat['JumlahPerubah'];

            $requestAlatLab = [
                'Stok' => $newBaik,
                'JumlahBaik' => $newBaik,
                'JumlahPinjam' => $JumlahPinjam
            ];

            $this->db->update('AlatLaboratorium', $requestAlatLab, ['Id' => $AlatLabId]);
        }

        redirect('Laboratorium/PeminjamanListAlat/'. $PeminjamanId);
    }

    public function TambahPeminjamanListAlat(){
        $datetime = new DateTime();
        $timezone = new DateTimeZone('Asia/Jakarta'); 
        $datetime->setTimezone($timezone);

        /* Atur Request Data */
        $PeminjamanId = $this->input->post('peminjamanId');

        $queryPeminjamanAlat = "
                                select * from PeminjamanAlat
                                where Id = $PeminjamanId
                                limit 1
                                ";
        $PeminjamanAlat = $this->db->query($queryPeminjamanAlat)->row_array();

        $countAlatLabId = count($this->input->post('alatLabId'));
        $requestPeminjamanListAlat = [];
        for ($i = 1; $i < $countAlatLabId; $i++) {
            $alatLabId = $this->input->post('alatLabId')[$i];
            $alat = $this->input->post('alat')[$i - 1];
            $lab = $this->input->post('lab')[$i - 1];
            $stok = $this->input->post('stok')[$i - 1];

            /* Cek Alat Agar Tidak Duplicate */
            $queryCekAlat = "
                            select * from PeminjamanListAlat
                            where PeminjamanId = $PeminjamanId AND 
                            AlatLabId = $alatLabId AND AlatId = $alat AND
                            LabId = $lab
                            ";

            $duplicateAlat = $this->db->query($queryCekAlat)->row_array();

            if ($alatLabId != "" && $alat != "" && $lab != "" && $stok != "" && $duplicateAlat == null) {
                $requestPeminjamanListAlat[] = [
                    'PeminjamanId' => $PeminjamanId,
                    'AlatLabId' => $alatLabId,
                    'AlatId' => $alat,
                    'LabId' => $lab,
                    'Jumlah' => $stok,
                    'Status' => 0,
                ];
            }
        }

        foreach($requestPeminjamanListAlat as $req){
            $AlatLabId = $req['AlatLabId'];

            /* Ambil Data Alat Laboratorium  */
            $queryAlatLab = "
                        select * from AlatLaboratorium
                        where Id = $AlatLabId
                        ";

            $AlatLab = $this->db->query($queryAlatLab)->row_array();

            /* Update data Stok pada AlatLab */
            $newstok = $AlatLab['Stok'] - $req['Jumlah'];

            $JumlahPinjam = $AlatLab['JumlahPinjam'] + $req['Jumlah'];

            if ($newstok > 0){
                /* insert table Peminjaman List Alat */
                $this->db->insert('PeminjamanListAlat', $req);

                $requestAlatLab = [
                    'Stok' => $newstok,
                    'JumlahPinjam' => $JumlahPinjam,
                    'JumlahBaik' => $newstok,
                ];
    
                $this->db->update('AlatLaboratorium', $requestAlatLab, ['Id' => $AlatLabId]);
    
                /* Memasukan data Pada table Stok */
                $requestStok = [
                    'AlatLabId' => $AlatLabId,
                    'Jenis' => 2,
                    'JumlahAwal' => $AlatLab['Stok'],
                    'JumlahPerubah' => $req['Jumlah'],
                    'Keterangan' => $PeminjamanAlat['Keterangan'],
                    'Tanggal' => $PeminjamanAlat['Tanggal'],
                    'CreatedAt' => $datetime->format("Y-m-d H:i:s"),
                    'PeminjamanId' => $PeminjamanId
                ];
    
                $this->db->insert('StokAlat', $requestStok);
            }
        }

        redirect('Laboratorium/PeminjamanListAlat/'. $PeminjamanId);
    }

    public function PengajuanAlatManual(){
        require_once 'vendor/autoload.php';
        $phpWord = new \PhpOffice\PhpWord\PhpWord();

        $datetime = new DateTime();
        $timezone = new DateTimeZone('Asia/Jakarta');
        $datetime->setTimezone($timezone);
        $formattedDate = $datetime->format('d F Y');
        
        $MhsId = $this->input->post('mhsId');
        $LabId = $this->input->post('lab');

        /* ambil data Mahasiswa : Nama, Nim, alamat di malang, Telpon, Email */
        $queryMahasiswa = "
                            select 
                            usr.Name NamaMhs, usr.Username NIM, usr.AlamatMalang, usr.Telpon, usr.Email
                            from Users usr
                            where Id = $MhsId
                            ";
        $Mahasiswa = $this->db->query($queryMahasiswa)->row_array();

        /* ambil data Laboratorium : NamaLab, NamaKalab, NIPKalab, NamaPLP, NIPPLP */
        $queryLab = "
                    select 
                    lab.Nama NamaLab,
                    kalab.Name NamaKalab, kalab.Username NIPKalab,
                    plp.Name NamaPLP, plp.Username NIPPLP
                    from AkademikLaboratorium lab
                    join Users kalab on lab.kalabId = kalab.Id
                    join Users plp on lab.PLPId = plp.Id
                    where lab.Id = $LabId
                    ";
        $Lab = $this->db->query($queryLab)->row_array();

        $ProgramStudi = $this->input->post('programStudi');
        $Status = $this->input->post('status');

        /* Ambil Template */
        $section = $phpWord->addSection();
        $document = new \PhpOffice\PhpWord\TemplateProcessor('assets/templates/word/TemplatePemakaianAlat.docx');
        
        /* Set Value */
        $document->setValue('namaMahasiswa', $Mahasiswa['NamaMhs']);
        $document->setValue('NIM', $Mahasiswa['NIM']);
        $document->setValue('alamatMalang', $Mahasiswa['AlamatMalang']);
        $document->setValue('telpon', $Mahasiswa['Telpon']);
        $document->setValue('email', $Mahasiswa['Email']);

        $document->setValue('status', $Status);
        $document->setValue('programStudi', $ProgramStudi);


        $document->setValue('namaLab', $Lab['NamaLab']);
        $document->setValue('namaKalab', $Lab['NamaKalab']);
        $document->setValue('NIPKalab', $Lab['NIPKalab']);
        $document->setValue('namaPLP', $Lab['NamaPLP']);
        $document->setValue('NIPPLP', $Lab['NIPPLP']);

        $document->setValue('tanggal', $formattedDate);

        /* Change Filename to userId-FormulirTA.pdf */
        $filename = $Mahasiswa['NIM'] . '-FormulirPemakaianAlat.docx';
        $directory = './assets/templates/temp/';
        $fullPath = $directory . $filename;
        $document->saveAs($fullPath);

        // Offer the document as a download link
        $this->load->helper('download');
        force_download($fullPath, NULL);

        sleep(5);

        /* Remove the file */
        unlink($fullPath);
    }

    public function DownloadFormulirBonAlat(){

        $datetime = new DateTime();
        $timezone = new DateTimeZone('Asia/Jakarta');
        $datetime->setTimezone($timezone);
        $formattedDate = $datetime->format('d F Y');

        require_once 'vendor/autoload.php';
        $mpdf = new \Mpdf\Mpdf();

        $labId = $this->input->post('LabId');
        $PeminjamanId = $this->input->post('PeminjamanId');

        $queryPeminjamanAlat = "
                                select 
                                listAlat.Jumlah JumlahPeminjaman,
                                peminjaman.Tujuan, peminjaman.TujuanLainnya,
                                alat.Nama NamaAlat, alat.Merk,
                                lab.Nama NamaLab,
                                mhs.Name NamaMhs, mhs.Username NIM, mhs.AlamatMalang, mhs.Telpon, mhs.Email, mhs.RoleId,
                                kalab.Name NamaKalab, kalab.Username NIPKalab,
                                plp.Name NamaPLP, plp.Username NIPPLP
                                from PeminjamanListAlat listAlat
                                join PeminjamanAlat peminjaman on listAlat.PeminjamanId = peminjaman.Id
                                join Users mhs on peminjaman.MhsId = mhs.Id
                                join MasterAlat alat on listAlat.AlatId = alat.Id
                                join AkademikLaboratorium lab on listAlat.LabId = lab.Id
                                join Users kalab on lab.KalabId = kalab.Id
                                join Users plp on lab.PLPId = plp.Id
                                where listAlat.LabId = $labId AND listAlat.PeminjamanId = $PeminjamanId
                                ";
        $PeminjamanAlat = $this->db->query($queryPeminjamanAlat)->result_array();

        switch ($PeminjamanAlat[0]['RoleId']){
            case 2:
                $Status = "Mahasiswa S1";
                break;
            case 3:
                $Status = "Mahasiswa S2";
                break;
            case 4:
                $Status = "Mahasiswa S3";
                break;
        };


        $html = '
        <!DOCTYPE html>
        <html lang="en">

        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Formulir Pemakaian Alat Laboratorium</title>
            <style>
            body {
                font-family: Arial, sans-serif;
                margin: 20px;
                font-size: 12px;
            }

            h1 {
                text-align: center;
                font-weight: bold;
            }

            main {
                margin: 0 auto;
                max-width: 600px;
            }

            label {
                display: inline-block;
                width: 200px;
                margin-bottom: 10px;
                font-size: 12px;
            }

            table {
                width: 100%;
                border-collapse: collapse;
                margin-top: 20px;
            }

            th, td {
                border: 1px solid #ddd;
                padding: 8px;
                text-align: left;
            }

            th {
                background-color: #f2f2f2;
            }

            .rangkasurat {
                width: 650px;
                margin: auto;
                background-color: #fff;
                /* height: 500px; */
            }
            
        </style>
        
        </head>

        <body>
        <header>
            <div class="rangkasurat">
            <table width="100%">
                <tr>
                        <td style="border: none;"><img src="/Users/rizqi/Documents/Sites/SikmaAdmin/assets/img/icons/ub.png" width="100px"> </td>
                        <td style="border: none;">
                        <h3>KEMENTERIAN PENDIDIKAN, KEBUDAYAAN, RISET, DAN TEKNOLOGi</h3>
                        <h3>UNIVERSITAS BRAWIJAYA</h3>
                        </td>
                        <td width="27%"  style="border: none;">
                        <h4>Fakultas Matematika Dan Ilmu Pengetahuan Alam</h4>
                        <h5>Jl. Veteran, Malang 65145, Indonesia Telp-fax : +62341 554403, 551611</h5>
                        </td>
                    </tr>
            </table>
            </div>
        </header>
            <h1 style="padding-top: 30px;">Formulir Pemakaian Alat Laboratorium '. $PeminjamanAlat[0]['NamaLab'] .'</h1>
            <main>
            <div class="dataDiri" style="padding-left:50px; padding-top:30px;">
                <table>
                <tr>
                    <td width="180px" style="border: none;">Nama</td>
                    <td style="border: none;">: '. $PeminjamanAlat[0]['NamaMhs'] .'</td>
                </tr>
                <tr>
                    <td width="180px" style="border: none;">NIM</td>
                    <td style="border: none;">: '. $PeminjamanAlat[0]['NIM'] .'</td>
                </tr>
                <tr>
                    <td width="180px" style="border: none;">Status</td>
                    <td style="border: none;">: '. $Status .'</td>
                </tr>
                <tr>
                    <td width="180px" style="border: none;">Departemen</td>
                    <td style="border: none;">: Kimia Fakultas MIPA Universitas Brawijaya</td>
                </tr>
                <tr>
                    <td width="180px" style="border: none;">Alamat di Malang</td>
                    <td style="border: none;">: '. $PeminjamanAlat[0]['AlamatMalang'] .'</td>
                </tr>
                <tr>
                    <td width="180px" style="border: none;">Telpon</td>
                    <td style="border: none;">: '. $PeminjamanAlat[0]['Telpon'] .'</td>
                </tr>
                <tr>
                    <td width="180px" style="border: none;">Email</td>
                    <td style="border: none;">: '. $PeminjamanAlat[0]['Email'] .'</td>
                </tr>
                </table>
            </div>

                <table>
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Alat</th>
                            <th>Merk</th>
                            <th>Alat</th>
                            <th>Keperluan</th>
                            <th>Harga</th>
                        </tr>
                    </thead>
                    <tbody>';

                    $i = 1;
                    foreach($PeminjamanAlat as $peminjaman){
                        switch ($peminjaman['Tujuan']){
                            case 1:
                                $tujuan = "Penelitian Dosen";
                                break;
                            case 2:
                                $tujuan = "Proyek Kimia";
                                break;
                            case 3:
                                $tujuan = "Program Kreatifitas Mahasiswa";
                                break;
                            case 4:
                                $tujuan = "Penelitian Skripsi";
                                break;
                            case 5:
                                $tujuan = "Penelitian Tesis";
                                break;
                            case 6:
                                $tujuan = "Penelitian Disertasi";
                                break;
                            case 7:
                                $tujuan = "Analisis Sampel";
                                break;
                            case 8:
                                $tujuan = $peminjaman['TujuanLainnya'];
                                break;
                        }

                        $html .= '
                            <tr>
                                <td>'.$i.'</td>
                                <td>'.$peminjaman['NamaAlat'].'</td>
                                <td>'.$peminjaman['Merk'].'</td>
                                <td>'.$peminjaman['JumlahPeminjaman'].'</td>
                                <td>'.$tujuan.'</td>
                                <td></td>
                            </tr>
                        ';
                    $i++;
                    };


$html .= '
                    </tbody>
                </table>

                <div class="rangkasurat" style="padding-left:50px; padding-top:30px;">
                <table width="100%">
                    <tr>
                    <td style="border: none;"></td>
                    <td style="border: none;"></td>
                    <td style="border: none;">Malang, '.$formattedDate.'</td>
                    </tr>
                    <tr>
                    <td style="border: none;">Kalab. '. $PeminjamanAlat[0]['NamaLab'] .'</td>
                    <td style="border: none;">PLP</td>
                    <td style="border: none;">Yang Mengajukan</td>
                    </tr>
                    <tr><td style="border: none;"></td></tr>
                    <tr><td style="border: none;"></td></tr>
                    <tr><td style="border: none;"></td></tr>
                    <tr><td style="border: none;"></td></tr>
                    <tr><td style="border: none;"></td></tr>
                    <tr>
                    <td style="border: none;">'. $PeminjamanAlat[0]['NamaKalab'] .'</td>
                    <td style="border: none;">'. $PeminjamanAlat[0]['NamaPLP'] .'</td>
                    <td style="border: none;">'. $PeminjamanAlat[0]['NamaMhs'] .'</td>
                    </tr>
                    <tr>
                    <td style="border: none;">'. $PeminjamanAlat[0]['NIPKalab'] .'</td>
                    <td style="border: none;">'. $PeminjamanAlat[0]['NIPPLP'] .'</td>
                    <td style="border: none;">'. $PeminjamanAlat[0]['NIM'] .'</td>
                    </tr>
                </table>
                </div>
            </main>
        </body>

        </html>
        ';

        $mpdf->WriteHTML($html);
        $mpdf->Output('Bon Alat-'.$PeminjamanAlat[0]['NamaMhs'].'.pdf', \Mpdf\Output\Destination::DOWNLOAD);

        die();
    }

    /* Master Bahan Kimia */
    public function ListMasterBahan(){
        $UserId = $this->session->userdata['Id'];

        $roleId = $this->session->userdata['RoleId'];
        $tendik = [1,5,6];
        
        $query = "select * from Users where Id = $UserId";

        if(in_array($roleId,$tendik)){
            $query = "
                    select 
                    usr.*,
                    tendik.Laboratorium
                    from Users usr
                    join UserTenagaKependidikan tendik on usr.Id = tendik.UserId
                    where usr.Id = $UserId
                    ";
        }

        $User = $this->db->query($query)->row_array();
        $data['User'] = $User;

        /* Ambil Data Master Bahan */
        $queryListBahan = "
                        select * from MasterBahan
                        ";
        $ListBahan = $this->db->query($queryListBahan)->result_array();

        $data['ListBahan'] = $ListBahan;

        /* Setting kembali */
        $data['url_kembali'] = get_referer_url();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('laboratorium/ListBahan', $data);
        $this->load->view('templates/footer');
    }

    public function TambahMasterBahan(){
        $datetime = new DateTime();
        $timezone = new DateTimeZone('Asia/Jakarta'); 

        $datetime->setTimezone($timezone);

        $request = [
            'Nama' => $this->input->post('nama'),
            'Merk' => $this->input->post('merk'),
            'Status' => 1,
            'IsDeleted' => 0,
            'CreatedAt' => $datetime->format("Y-m-d H:i:s"),
        ];

        $this->db->insert('MasterBahan', $request);

        redirect('Laboratorium/ListMasterBahan');
    }

    public function DetailMasterBahan(){
        $BahanId = $this->input->post('BahanId');

        $queryMasterBahan = "
                            select * from MasterBahan
                            where Id = $BahanId
                            ";

        $MasterBahan = $this->db->query($queryMasterBahan)->row_array();

        echo json_encode($MasterBahan);
    }

    public function EditMasterBahan(){
        $BahanId = $this->input->post('bahanId');

        $request = [
            'Nama' => $this->input->post('nama'),
            'Merk' => $this->input->post('merk'),
            'Status' => $this->input->post('status'),
        ];

        $this->db->update('MasterBahan', $request, ["Id" => $BahanId]);
        
        redirect('Laboratorium/ListMasterBahan');
    }

    /* Bahan Masing-masing Lab Kimia */
    public function ListBahanLab($LabId = null){
        $data['title'] = 'List Bahan Bahan Lab';
        
        $UserId = $this->session->userdata['Id'];

        $roleId = $this->session->userdata['RoleId'];

        $kadep = $this->session->userdata['Kadep'];
        $sekdep = $this->session->userdata['Sekdep'];
        $kaprodiS1 = $this->session->userdata['KaprodiS1'];
        $kaprodiS2 = $this->session->userdata['KaprodiS2'];
        $kaprodiS3 = $this->session->userdata['KaprodiS3'];

        $mhsRole = [2,3,4];
        $dsnRole = [1];
        if (in_array($roleId, $mhsRole)){
            $query = "
                        select mhs.*, 
                        usr.Id, usr.Name, usr.Username, usr.Telpon, usr.Email, usr.RoleId, usr.CreatedAt, usr.Status, usr.ImageProfile, usr.AlamatMalang, usr.Alamat
                        from Users usr
                        join UserMahasiswa mhs on usr.Id = mhs.UserId
                        where usr.Id = '$UserId' 
                        ";

            /* Ambil Data Bahan Laboratorium */
            $queryBahanLab = "
                            select 
                            bahanLab.*,
                            bahan.Nama NamaBahan
                            from BahanLaboratorium bahanLab
                            join MasterBahan bahan on bahanLab.BahanId = bahan.Id
                            where bahanLab.LabId = $LabId and bahanLab.Status = 1
                            ";

            if($this->input->post('search') != NULL){
                $search = $this->input->post('search');
                $queryBahanLab = $queryBahanLab . " AND Nama LIKE '%$search%'"; 
            }

            if($this->input->post('limit') != NULL){
                $limit = $this->input->post('limit');
                $queryBahanLab = $queryBahanLab . " limit $limit"; 
            }

            $BahanLab = $this->db->query($queryBahanLab)->result_array();

        }else if(in_array($roleId, $dsnRole) && ($kadep == false && $sekdep == false && $kaprodiS1 == false && $kaprodiS2 == false && $kaprodiS3 == false)){
            $query = "
                        select dsn.*, 
                        usr.Id, usr.Name, usr.Username, usr.Telpon, usr.Email, usr.RoleId, usr.CreatedAt, usr.Status, usr.ImageProfile, usr.AlamatMalang, usr.Alamat
                        from Users usr
                        join UserTenagaKependidikan dsn on usr.Id = dsn.UserId
                        where usr.Id = '$UserId'
                        ";

            /* Ambil Data Bahan Laboratorium */
            $queryBahanLab = "
                            select 
                            bahanLab.*,
                            bahan.Nama NamaBahan
                            from BahanLaboratorium bahanLab
                            join MasterBahan bahan on bahanLab.BahanId = bahan.Id
                            where bahanLab.LabId = $LabId
                            ";

            if($this->input->post('search') != NULL){
                $search = $this->input->post('search');
                $queryBahanLab = $queryBahanLab . " AND Nama LIKE '%$search%'"; 
            }
                
            if($this->input->post('limit') != NULL){
                $limit = $this->input->post('limit');
                $queryBahanLab = $queryBahanLab . " limit $limit"; 
            }
            
            $BahanLab = $this->db->query($queryBahanLab)->result_array();
        }else if($roleId == 6){
            $query = "
                        select stf.*, 
                        usr.Id, usr.Name, usr.Username, usr.Telpon, usr.Email, usr.RoleId, usr.CreatedAt, usr.Status, usr.ImageProfile, usr.AlamatMalang, usr.Alamat
                        from Users usr
                        join UserTenagaKependidikan stf on usr.Id = stf.UserId
                        where usr.Id = '$UserId'
                        ";

            /* Ambil Data Bahan Laboratorium */
            $queryBahanLab = "
                            select 
                            bahanLab.*,
                            bahan.Nama NamaBahan
                            from BahanLaboratorium bahanLab
                            join MasterBahan bahan on bahanLab.BahanId = bahan.Id
                            where bahanLab.LabId = $LabId
                            ";

            if($this->input->post('search') != NULL){
                $search = $this->input->post('search');
                $queryBahanLab = $queryBahanLab . " AND Nama LIKE '%$search%'"; 
            }
                
            if($this->input->post('limit') != NULL){
                $limit = $this->input->post('limit');
                $queryBahanLab = $queryBahanLab . " limit $limit"; 
            }
            
            $BahanLab = $this->db->query($queryBahanLab)->result_array();
        }else{
            $query = "select * from Users where Id = '$UserId'";
            
            /* Ambil Data Bahan Laboratorium */
            $queryBahanLab = "
                            select 
                            bahanLab.*,
                            bahan.Nama NamaBahan
                            from BahanLaboratorium bahanLab
                            join MasterBahan bahan on bahanLab.BahanId = bahan.Id
                            where bahanLab.LabId = $LabId
                            ";

            if($this->input->post('search') != NULL){
                $search = $this->input->post('search');
                $queryBahanLab = $queryBahanLab . " AND Nama LIKE '%$search%'"; 
            }
                
            if($this->input->post('limit') != NULL){
                $limit = $this->input->post('limit');
                $queryBahanLab = $queryBahanLab . " limit $limit"; 
            }
            
            $BahanLab = $this->db->query($queryBahanLab)->result_array();
        }

        $User = $this->db->query($query)->row_array();
        $data['User'] = $User;

        // var_dump($User);
        $data['BahanLab'] = $BahanLab;
        $data['LabId'] = $LabId;

        $queryKalab = "
                        select * from AkademikLaboratorium 
                        where Status = 1 and IsDeleted = 0
                        ";
        $Kalab = $this->db->query($queryKalab)->result_array();
        $data['Kalab'] = $Kalab;

        /* Ambil Data Master Bahan */
        $queryListBahan = "
                        select * from MasterBahan
                        where Status = 1 And IsDeleted = 0
                        ";
        $ListBahan = $this->db->query($queryListBahan)->result_array();

        $data['ListBahan'] = $ListBahan;

        /* Setting kembali */
        $data['url_kembali'] = get_referer_url();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('laboratorium/ListBahanLab', $data);
        $this->load->view('templates/footer');
    }

    public function TambahBahanLab(){
        $datetime = new DateTime();
        $timezone = new DateTimeZone('Asia/Jakarta'); 

        $datetime->setTimezone($timezone);

        $LabId = $this->input->post('labId');

        $request = [
            'LabId' => $LabId,
            'Bahanid' => $this->input->post('bahan'),
            'Stok' => $this->input->post('stok'),
            'Status' => 1,
            'CreatedAt' => $datetime->format("Y-m-d H:i:s"),
        ];

        $this->db->insert('BahanLaboratorium', $request);

        redirect('Laboratorium/ListBahanLab/' . $LabId);
    }

    public function DetailBahanLab(){
        $BahanLabId = $this->input->post('BahanLabId');

        $queryBahanLab = "
                            select bahanLab.*,
                            bahan.Nama NamaBahan
                            from BahanLaboratorium bahanLab
                            join MasterBahan bahan on bahanLab.BahanId = bahan.Id
                            where bahanLab.Id = $BahanLabId
                            ";

        $BahanLab = $this->db->query($queryBahanLab)->row_array();

        echo json_encode($BahanLab);
    }

    public function EditBahanLab(){
        $bahanLabId = $this->input->post('bahanLabId');
        $labId = $this->input->post('labId');

        $request = [
            'Status' => $this->input->post('status'),
        ];

        $this->db->update('BahanLaboratorium', $request, ["Id" => $bahanLabId]);
        
        redirect('Laboratorium/ListBahanLab/'.$labId);
    }

    /* Stok Masing-masing Bahan Lab Kimia */
    public function StokBahanLab($BahanLabId = null){
        $data['title'] = 'Pendaftaran Tugas Akhir';
        
        $UserId = $this->session->userdata['Id'];

        $roleId = $this->session->userdata['RoleId'];

        $kadep = $this->session->userdata['Kadep'];
        $sekdep = $this->session->userdata['Sekdep'];
        $kaprodiS1 = $this->session->userdata['KaprodiS1'];
        $kaprodiS2 = $this->session->userdata['KaprodiS2'];
        $kaprodiS3 = $this->session->userdata['KaprodiS3'];

        $dsnRole = [1];
        if(in_array($roleId, $dsnRole) && ($kadep == false && $sekdep == false && $kaprodiS1 == false && $kaprodiS2 == false && $kaprodiS3 == false)){
            $query = "
                        select dsn.*, 
                        usr.Id, usr.Name, usr.Username, usr.Telpon, usr.Email, usr.RoleId, usr.CreatedAt, usr.Status, usr.ImageProfile, usr.AlamatMalang, usr.Alamat
                        from Users usr
                        join UserTenagaKependidikan dsn on usr.Id = dsn.UserId
                        where usr.Id = '$UserId'
                        ";
        }else if($roleId == 6){
            $query = "
                        select dsn.*, 
                        usr.Id, usr.Name, usr.Username, usr.Telpon, usr.Email, usr.RoleId, usr.CreatedAt, usr.Status, usr.ImageProfile, usr.AlamatMalang, usr.Alamat
                        from Users usr
                        join UserTenagaKependidikan dsn on usr.Id = dsn.UserId
                        where usr.Id = '$UserId'
                        ";
        }else{
            $query = "select * from Users where Id = '$UserId'";
        }

        /* Ambil Data Bahan Laboratorium */
        $queryStokBahan = "
                        select 
                        bahan.Nama NamaBahan,
                        bahanLab.LabId,
                        stok.*,
                        (CASE WHEN stok.PengadaanId IS NOT NULL
                            THEN (SELECT pengadaan.Bukti
                                    FROM PengadaanBahan pengadaan
                                    WHERE pengadaan.Id = stok.PengadaanId)
                            ELSE NULL END) AS Bukti
                        from BahanLaboratorium bahanLab
                        join MasterBahan bahan on bahanLab.BahanId = bahan.Id
                        left join StokBahan stok on bahanLab.Id = stok.bahanLabId
                        where bahanLab.Id = $BahanLabId
                        ";
        
        $StokBahan = $this->db->query($queryStokBahan)->result_array();

        $queryLastIdStok = "
                        select Id from StokBahan
                        where BahanLabId = $BahanLabId
                        order by Id Desc Limit 1
                        ";
        $LastIdStok = $this->db->query($queryLastIdStok)->row_array();

        foreach($StokBahan as $key => $bahan){
            switch ($bahan['Jenis']){
                case 1:
                    $StokBahan[$key]['JenisText'] = "Pengadaan";
                    break;
                case 2:
                    $StokBahan[$key]['JenisText'] = "Pengeluaran";
                    break;


                default:
                    $StokBahan[$key]['JenisText'] = "";
                    break;

            }
        }

        $User = $this->db->query($query)->row_array();
        $data['User'] = $User;

        $data['StokBahan'] = $StokBahan;
        $data['LastIdStok'] = $LastIdStok;

        /* Ambil data Bahan */
        $queryBahan = "
                    select 
                    bahan.Nama NamaBahan, bahanLab.Id
                    from BahanLaboratorium bahanLab
                    join MasterBahan bahan on bahanLab.bahanId = bahan.Id
                    where bahanLab.Id = $BahanLabId
                    ";
        $Bahan = $this->db->query($queryBahan)->row_array();

        $data['Bahan'] = $Bahan;
        
        /* Setting kembali */
        $data['url_kembali'] = get_referer_url();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('laboratorium/StokBahan', $data);
        $this->load->view('templates/footer');
    }

    public function TambahStokBahanLab(){
        $datetime = new DateTime();
        $timezone = new DateTimeZone('Asia/Jakarta'); 
        $datetime->setTimezone($timezone);

        // $AlatId = $this->input->post('alatId');
        $BahanLabId = $this->input->post('bahanLabId');
        $UserId = $this->session->userdata['Id'];

        /* insert Table pengadaan Alat */
        $request = [
            'BahanLabId' => $BahanLabId,
            'AdminId' => $UserId,
            'Keterangan' => $this->input->post('keterangan'),
            'Jumlah' => $this->input->post('jumlah'),
            'Tanggal' => $this->input->post('tanggal'),
            'CreatedAt' => $datetime->format("Y-m-d H:i:s"),
        ];

        $this->db->insert('PengadaanBahan', $request);

        /* Ambil Id Terakhir */
        $queryStokBahan = "select * from PengadaanBahan where BahanLabId = $BahanLabId order by Id Desc limit 1";
        $PengadaanBahan = $this->db->query($queryStokBahan)->row_array();

        /* Update Bukti Pengadaan */
        $BuktiPengadaanBahan = $_FILES['buktiPengadaanBahan']['name'];
        if($BuktiPengadaanBahan){
            $config['upload_path']          = './assets/uploads/buktiPengadaanbahan_lab/';
            $config['allowed_types']        = 'jpg|png|pdf';
            $config['max_size']             = 5000;

            $this->load->library('upload', $config);

            if($this->upload->do_upload('buktiPengadaanBahan')){
                /* Upload New Image */
                $data = $this->upload->data();
                $uploaded_filename = $data['file_name'];
                $extention = pathinfo($uploaded_filename, PATHINFO_EXTENSION);
                $source_path = './assets/uploads/buktiPengadaanbahan_lab/' . $uploaded_filename;
                $new_filename = $PengadaanBahan['Id'].'-BuktiPengadaanBahan.'.$extention; // Change this to your desired new name
                $new_path = './assets/uploads/buktiPengadaanbahan_lab/' . $new_filename;

                if (rename($source_path, $new_path)) {
                    $requestUpdate = ['Bukti' => $new_filename];

                    $this->db->update('PengadaanBahan', $requestUpdate, ['Id' => $PengadaanBahan['Id']]);
                }
            }
        }

        /* Ambil data Bahan Laboratorium */
        $queryBahanLab = "
                        select * from BahanLaboratorium where Id = $BahanLabId
                        ";
        $BahanLab = $this->db->query($queryBahanLab)->row_array();

        /* Input Pada Table Stok */
        $requestStok = [
            'BahanLabId' => $BahanLabId,
            'Jenis' => 1,
            'JumlahAwal' => $BahanLab['Stok'],
            'JumlahPerubah' => $this->input->post('jumlah'),
            'Keterangan' => $this->input->post('keterangan'),
            'Tanggal' => $this->input->post('tanggal'),
            'CreatedAt' => $datetime->format("Y-m-d H:i:s"),
            'PengadaanId' => $PengadaanBahan['Id']
        ];

        $this->db->insert('StokBahan', $requestStok);

        /* Ambil data Stok berdasarkan Id Terakhir */
        $queryStok = "select * from StokBahan where BahanLabId = $BahanLabId order by Id Desc limit 1";
        $Stok = $this->db->query($queryStok)->row_array();

        $newStok = $BahanLab['Stok'] + $Stok['JumlahPerubah'];

        $requestUpdateBahanLab = [
            'Stok' => $newStok,
        ];

        $this->db->update('BahanLaboratorium', $requestUpdateBahanLab, ['Id' => $BahanLabId]);

        redirect('Laboratorium/StokBahanLab/' . $BahanLabId);
    }

    public function DetailStokBahanLab(){
        $StokId = $this->input->post('StokId');

        $queryJenisStok = "select * from StokBahan where Id = $StokId order by Id Desc limit 1";
        $JenisStok = $this->db->query($queryJenisStok)->row_array();

        if ($JenisStok['Jenis'] == 1){
            $queryStokBahan = "
                            select 
                            pengadaan.Keterangan, pengadaan.Bukti, pengadaan.Id PengadaanId, pengadaan.Tanggal,
                            stok.JumlahAwal, stok.JumlahPerubah, stok.Id StokId,
                            bahanLab.Stok TotalStok,
                            admin.Name NamaAdmin,
                            bahan.Nama NamaBahan
                            from StokBahan stok
                            join PengadaanBahan pengadaan on stok.PengadaanId = pengadaan.Id
                            join BahanLaboratorium bahanLab on pengadaan.BahanLabId = bahanLab.Id
                            join Users admin on pengadaan.AdminId = admin.Id
                            join MasterBahan bahan on bahanLab.BahanId = bahan.Id
                            where stok.Id = $StokId
                                ";
        }else{
            $queryStokBahan = "
                            select 
                            peminjaman.Keterangan, peminjaman.Id PeminjamanId, peminjaman.Tanggal,
                            stok.JumlahAwal, stok.JumlahPerubah, Stok.Id StokId,
                            bahanLab.Stok TotalStok,
                            mhs.Name NamaMhs,
                            bahan.Nama NamaBahan
                            from StokBahan stok
                            join PeminjamanBahan peminjaman on stok.PeminjamanId = peminjaman.Id
                            join Users mhs on peminjaman.MhsId = mhs.Id
                            join BahanLaboratorium bahanLab on stok.BahanLabId = bahanLab.Id
                            join MasterBahan bahan on bahanLab.BahanId = bahan.Id
                            where stok.Id = $StokId
                            ";
        }

        $StokBahan = $this->db->query($queryStokBahan)->row_array();

        echo json_encode($StokBahan);
    }

    public function EditStokBahanLab(){
        $datetime = new DateTime();
        $timezone = new DateTimeZone('Asia/Jakarta'); 
        $datetime->setTimezone($timezone);

        $UserId = $this->session->userdata['Id'];
        $StokId = $this->input->post('stokId');
        $PengadaanId = $this->input->post('pengadaanId');

        $JumlahPerubah = $this->input->post('jumlah');

        /* Ambil data Stok Bahan */
        $queryStok = "
                    select * from StokBahan
                    where Id = $StokId
                    ";
        $StokBahan = $this->db->query($queryStok)->row_array();

        /* Update Table pengadaan Bahan */
        $request = [
            'AdminId' => $UserId,
            'Keterangan' => $this->input->post('keterangan'),
            'Jumlah' => $JumlahPerubah,
            'Tanggal' => $this->input->post('tanggal'),
            'CreatedAt' => $datetime->format("Y-m-d H:i:s"),
        ];

        $this->db->update('PengadaanBahan', $request, ['Id' => $PengadaanId]);

        /* Update Bukti Pengadaan */
        $BuktiPengadaanBahan = $_FILES['buktiPengadaanBahan']['name'];
        if($BuktiPengadaanBahan){
            $config['upload_path']          = './assets/uploads/buktiPengadaanbahan_lab/';
            $config['allowed_types']        = 'jpg|png|pdf';
            $config['max_size']             = 5000;

            $this->load->library('upload', $config);

            if($this->upload->do_upload('buktiPengadaanBahan')){
                /* Upload New Image */
                $data = $this->upload->data();
                $uploaded_filename = $data['file_name'];
                $extention = pathinfo($uploaded_filename, PATHINFO_EXTENSION);
                $source_path = './assets/uploads/buktiPengadaanbahan_lab/' . $uploaded_filename;
                $new_filename = $PengadaanId.'-BuktiPengadaanBahan.'.$extention; // Change this to your desired new name
                $new_path = './assets/uploads/buktiPengadaanbahan_lab/' . $new_filename;

                if (rename($source_path, $new_path)) {
                    $requestUpdate = ['Bukti' => $new_filename];

                    $this->db->update('PengadaanBahan', $requestUpdate, ['Id' => $PengadaanId]);
                }
            }
        }

        $BahanLabId = $StokBahan['BahanLabId'];

        $requestStok = [
            'JumlahPerubah' => $JumlahPerubah,
            'Keterangan' => $this->input->post('keterangan')
        ];

        $this->db->update('StokBahan', $requestStok, ['Id' => $StokId]);

        /* Ambil Bahan Lab */
        $queryBahanLab = "
                        select * from BahanLaboratorium
                        where Id = $BahanLabId
                        ";
        $BahanLab = $this->db->query($queryBahanLab)->row_array();
        
        if($StokBahan['Jenis'] == 1){
            $newStok = $StokBahan['JumlahAwal'] + $JumlahPerubah;

            $requestBahanLab = [
                'Stok' => $newStok,
            ];
        }else{
            $newStok = $StokBahan['JumlahAwal'] - $JumlahPerubah;

            $requestBahanLab = [
                'Stok' => $newStok,
            ];
        }

        $this->db->update('BahanLaboratorium', $requestBahanLab, ['Id' => $BahanLabId]);

        redirect('Laboratorium/StokBahanLab/' . $BahanLabId);
    }

    /* Bon Bahan */
    public function PeminjamanBahan(){
        $data['title'] = 'Peminjaman Bahan';
        
        $UserId = $this->session->userdata['Id'];

        $roleId = $this->session->userdata['RoleId'];

        $kadep = $this->session->userdata['Kadep'];
        $sekdep = $this->session->userdata['Sekdep'];
        $kaprodiS1 = $this->session->userdata['KaprodiS1'];
        $kaprodiS2 = $this->session->userdata['KaprodiS2'];
        $kaprodiS3 = $this->session->userdata['KaprodiS3'];

        $mhsRole = [2,3,4];
        $dsnRole = [1];
        if (in_array($roleId, $mhsRole)){
            $query = "
                        select mhs.*, 
                        usr.Id, usr.Name, usr.Username, usr.Telpon, usr.Email, usr.RoleId, usr.CreatedAt, usr.Status, usr.ImageProfile, usr.AlamatMalang, usr.Alamat
                        from Users usr
                        join UserMahasiswa mhs on usr.Id = mhs.UserId
                        where usr.Id = '$UserId' 
                        ";

            /* Ambil Data Mahasiswa Meminjam Bahan */
            $queryPeminjamBahan = "
                                select 
                                peminjaman.*,
                                (CASE WHEN peminjaman.MhsId IS NOT NULL OR peminjaman.MhsId != 0
                                    THEN (SELECT mhs.Name
                                            FROM Users mhs
                                            WHERE Id = peminjaman.MhsId)
                                    ELSE null END) AS NamaMahasiswa
                                from PeminjamanBahan peminjaman
                                where peminjaman.MhsId = $UserId
                                ";
            $PeminjamBahan = $this->db->query($queryPeminjamBahan)->result_array();
        }else if(in_array($roleId, $dsnRole) && ($kadep == false && $sekdep == false && $kaprodiS1 == false && $kaprodiS2 == false && $kaprodiS3 == false)){
            $query = "
                        select dsn.*, 
                        usr.Id, usr.Name, usr.Username, usr.Telpon, usr.Email, usr.RoleId, usr.CreatedAt, usr.Status, usr.ImageProfile, usr.AlamatMalang, usr.Alamat
                        from Users usr
                        join UserTenagaKependidikan dsn on usr.Id = dsn.UserId
                        where usr.Id = '$UserId'
                        ";
                        
            /* Ambil Data Mahasiswa Meminjam Bahan */
            $queryPeminjamBahan = "
                                select 
                                peminjaman.*,
                                (CASE WHEN peminjaman.MhsId IS NOT NULL OR peminjaman.MhsId != 0
                                    THEN (SELECT mhs.Name
                                            FROM Users mhs
                                            WHERE Id = peminjaman.MhsId)
                                    ELSE null END) AS NamaMahasiswa
                                from PeminjamanBahan peminjaman
                                    ";
            $PeminjamBahan = $this->db->query($queryPeminjamBahan)->result_array();
        }else if($roleId == 6){
            $query = "
                        select dsn.*, 
                        usr.Id, usr.Name, usr.Username, usr.Telpon, usr.Email, usr.RoleId, usr.CreatedAt, usr.Status, usr.ImageProfile, usr.AlamatMalang, usr.Alamat
                        from Users usr
                        join UserTenagaKependidikan dsn on usr.Id = dsn.UserId
                        where usr.Id = '$UserId'
                        ";
            
            /* Ambil Data Mahasiswa Meminjam Bahan */
            $queryPeminjamBahan = "
                                select 
                                peminjaman.*,
                                (CASE WHEN peminjaman.MhsId IS NOT NULL OR peminjaman.MhsId != 0
                                    THEN (SELECT mhs.Name
                                            FROM Users mhs
                                            WHERE Id = peminjaman.MhsId)
                                    ELSE null END) AS NamaMahasiswa
                                from PeminjamanBahan peminjaman
                                    ";
            $PeminjamBahan = $this->db->query($queryPeminjamBahan)->result_array();
        }else{
            $query = "select * from Users where Id = '$UserId'";
            
            /* Ambil Data Mahasiswa Meminjam Bahan */
            $queryPeminjamBahan = "
                                select 
                                peminjaman.*,
                                (CASE WHEN peminjaman.MhsId IS NOT NULL OR peminjaman.MhsId != 0
                                    THEN (SELECT mhs.Name
                                            FROM Users mhs
                                            WHERE Id = peminjaman.MhsId)
                                    ELSE null END) AS NamaMahasiswa
                                from PeminjamanBahan peminjaman
                                    ";
            $PeminjamBahan = $this->db->query($queryPeminjamBahan)->result_array();
        }

        foreach($PeminjamBahan as $key => $peminjaman){
            switch ($peminjaman['Tujuan']){
                case 1:
                    $PeminjamBahan[$key]['Tujuan'] = "Penelitian Dosen";
                    break;
                case 2:
                    $PeminjamBahan[$key]['Tujuan'] = "Proyek Kimia";
                    break;
                case 3:
                    $PeminjamBahan[$key]['Tujuan'] = "Program Kreatifitas Mahasiswa";
                    break;
                case 4:
                    $PeminjamBahan[$key]['Tujuan'] = "Penelitian Skripsi";
                    break;
                case 5:
                    $PeminjamBahan[$key]['Tujuan'] = "Penelitian Tesis";
                    break;
                case 6:
                    $PeminjamBahan[$key]['Tujuan'] = "Penelitian Disertasi";
                    break;
                case 7:
                    $PeminjamBahan[$key]['Tujuan'] = "Analisis Sampel";
                    break;
                case 8:
                    $PeminjamBahan[$key]['Tujuan'] = $peminjaman['TujuanLainnya'];
                    break;
            }

        }

        $User = $this->db->query($query)->row_array();
        $data['User'] = $User;

        $data['PeminjamBahan'] = $PeminjamBahan;

        /* Ambil Data Master Bahan */
        $queryListBahanLab = "
                            SELECT 
                                bahan.Id AS BahanId, 
                                lab.Id AS LabId,  
                                bahanLab.Id AS BahanLabId, 
                                bahanLab.Stok, 
                                bahan.Nama AS NamaBahan, 
                                lab.Nama AS NamaLab
                            FROM 
                                BahanLaboratorium bahanLab
                                JOIN MasterBahan bahan ON bahanLab.BahanId = bahan.Id
                                JOIN AkademikLaboratorium lab ON bahanLab.LabId = lab.Id
                            WHERE 
                                bahanLab.Status = 1 
                                AND (
                                bahan.Status = 1 
                                AND bahan.IsDeleted = 0
                                )
      
                        ";
        $ListBahanLab = $this->db->query($queryListBahanLab)->result_array();

        $data['ListBahanLab'] = $ListBahanLab;

        /* Ambil data Lab */
        $queryLab = "
                    select * from AkademikLaboratorium
                    where Status = 1 AND IsDeleted = 0
                    ";
        
        $Laboratorium = $this->db->query($queryLab)->result_array();
        $data['Laboratorium'] = $Laboratorium;

        /* Setting kembali */
        $data['url_kembali'] = get_referer_url();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('laboratorium/PeminjamanBahan', $data);
        $this->load->view('templates/footer');
    }

    public function TambahPeminjamanBahan(){
        $datetime = new DateTime();
        $timezone = new DateTimeZone('Asia/Jakarta'); 
        $datetime->setTimezone($timezone);

        $UserId = $this->session->userdata['Id'];

        /* insert table Peminjaman Bahan */
        $requestPeminjamanBahan = [
            'MhsId' => $UserId,
            'Tanggal' => $this->input->post('tanggal'),
            'Keterangan' => $this->input->post('keterangan'),
            'Tujuan' => $this->input->post('tujuan'),
            'TujuanLainnya' => "-",
            'Status' => 0,
            'CreatedAt' => $datetime->format("Y-m-d H:i:s"),
        ];

        if($this->input->post('tujuan') == 8){
            $requestPeminjamanBahan['TujuanLainnya'] = $this->input->post('tujuanLainnya');
        }

        $this->db->insert('PeminjamanBahan', $requestPeminjamanBahan);

        /* Ambil Id Peminjaman Bahan */
        $queryIdPeminjamanBahan = "
                                select * from PeminjamanBahan
                                where MhsId = $UserId
                                order by Id Desc
                                limit 1
                                ";
        $IdPeminjamanBahan = $this->db->query($queryIdPeminjamanBahan)->row_array();

        /* Atur Request Data */
        $countBahanLabId = count($this->input->post('bahanLabId'));
        $requestPeminjamanListBahan = [];
        for ($i = 1; $i < $countBahanLabId; $i++) {
            $bahanLabId = $this->input->post('bahanLabId')[$i];
            $bahan = $this->input->post('bahan')[$i - 1];
            $lab = $this->input->post('lab')[$i - 1];
            $stok = $this->input->post('stok')[$i - 1];

            if ($bahanLabId != "" && $bahan != "" && $lab != "" && $stok != "") {
                $requestPeminjamanListBahan[] = [
                    'PeminjamanId' => $IdPeminjamanBahan['Id'],
                    'BahanLabId' => $bahanLabId,
                    'bahanId' => $bahan,
                    'LabId' => $lab,
                    'Jumlah' => $stok,
                    'Status' => 0
                ];
            }
        }

        foreach($requestPeminjamanListBahan as $req){
            $BahanLabId = $req['BahanLabId'];

            /* Ambil Data Bahan Laboratorium  */
            $queryBahanLab = "
                        select * from BahanLaboratorium
                        where Id = $BahanLabId
                        ";

            $BahanLab = $this->db->query($queryBahanLab)->row_array();

            /* Update data Stok pada BahanLab */
            $newstok = $BahanLab['Stok'] - $req['Jumlah'];

            if ($newstok > 0){
                /* insert table Peminjaman List Bahan */
                $this->db->insert('PeminjamanListBahan', $req);

                $requestBahanLab = [
                    'Stok' => $newstok,
                ];
    
                $this->db->update('BahanLaboratorium', $requestBahanLab, ['Id' => $BahanLabId]);
    
                /* Memasukan data Pada table Stok */
                $requestStok = [
                    'BahanLabId' => $BahanLabId,
                    'Jenis' => 2,
                    'JumlahAwal' => $BahanLab['Stok'],
                    'JumlahPerubah' => $req['Jumlah'],
                    'Keterangan' => $this->input->post('keterangan'),
                    'Tanggal' => $this->input->post('tanggal'),
                    'CreatedAt' => $datetime->format("Y-m-d H:i:s"),
                    'PeminjamanId' => $IdPeminjamanBahan['Id']
                ];
    
                $this->db->insert('StokBahan', $requestStok);
            }
        }

        redirect('Laboratorium/PeminjamanBahan');
    }

    /* List Peminjaman Bahan */
    public function PeminjamanListBahan($PeminjamanId = null){
        $data['title'] = 'Peminjaman Bahan';
        
        $UserId = $this->session->userdata['Id'];

        $roleId = $this->session->userdata['RoleId'];

        $kadep = $this->session->userdata['Kadep'];
        $sekdep = $this->session->userdata['Sekdep'];
        $kaprodiS1 = $this->session->userdata['KaprodiS1'];
        $kaprodiS2 = $this->session->userdata['KaprodiS2'];
        $kaprodiS3 = $this->session->userdata['KaprodiS3'];

        $mhsRole = [2,3,4];
        $dsnRole = [1];
        if (in_array($roleId, $mhsRole)){
            $query = "
                        select mhs.*, 
                        usr.Id, usr.Name, usr.Username, usr.Telpon, usr.Email, usr.RoleId, usr.CreatedAt, usr.Status, usr.ImageProfile, usr.AlamatMalang, usr.Alamat
                        from Users usr
                        join UserMahasiswa mhs on usr.Id = mhs.UserId
                        where usr.Id = '$UserId' 
                        ";

        }else if(in_array($roleId, $dsnRole) && ($kadep == false && $sekdep == false && $kaprodiS1 == false && $kaprodiS2 == false && $kaprodiS3 == false)){
            $query = "
                        select dsn.*, 
                        usr.Id, usr.Name, usr.Username, usr.Telpon, usr.Email, usr.RoleId, usr.CreatedAt, usr.Status, usr.ImageProfile, usr.AlamatMalang, usr.Alamat
                        from Users usr
                        join UserTenagaKependidikan dsn on usr.Id = dsn.UserId
                        where usr.Id = '$UserId'
                        ";

        }else if($roleId == 6){
            $query = "
                        select dsn.*, 
                        usr.Id, usr.Name, usr.Username, usr.Telpon, usr.Email, usr.RoleId, usr.CreatedAt, usr.Status, usr.ImageProfile, usr.AlamatMalang, usr.Alamat
                        from Users usr
                        join UserTenagaKependidikan dsn on usr.Id = dsn.UserId
                        where usr.Id = '$UserId'
                        ";

        }else{
            $query = "select * from Users where Id = '$UserId'";
        }

        /* Ambil Data List Peminjaman Bahan */
        $queryPeminjamanListBahan = "
                        select 
                        peminjaman.Keterangan, peminjaman.Tujuan, peminjaman.TujuanLainnya,
                        listBahan.PeminjamanId PeminjamanId, listBahan.BahanId, listBahan.LabId, listBahan.Jumlah, listBahan.Status, listBahan.Id ListBahanId,
                        bahan.Nama NamaBahan,
                        lab.Nama NamaLab
                        from PeminjamanBahan peminjaman
                        join PeminjamanListBahan listBahan on peminjaman.Id = listBahan.PeminjamanId
                        join MasterBahan bahan on listBahan.BahanId = bahan.Id
                        join AkademikLaboratorium lab on listBahan.LabId = lab.Id
                        where peminjaman.Id = $PeminjamanId
                        order by listBahan.LabId
                        ";

        $PeminjamanListBahan = $this->db->query($queryPeminjamanListBahan)->result_array();
        

        $User = $this->db->query($query)->row_array();
        $data['User'] = $User;

        $data['PeminjamanListBahan'] = $PeminjamanListBahan;

        /* Ambil Data Master Bahan */
        $queryListBahanLab = "
                        select 
                        bahan.Id BahanId, lab.Id LabId,  bahanLab.Id BahanLabId, bahanLab.Stok, 
                        bahan.Nama NamaBahan,
                        lab.Nama NamaLab
                        from BahanLaboratorium bahanLab
                        join MasterBahan bahan on bahanlab.BahanId = bahan.Id
                        join AkademikLaboratorium lab on bahanLab.LabId = lab.Id
                        where bahanLab.Status = 1 and (bahan.Status = 1 and bahan.IsDeleted = 0)
                        ";
        $ListBahanLab = $this->db->query($queryListBahanLab)->result_array();

        $data['ListBahanLab'] = $ListBahanLab;

        $data['PeminjamanId'] = $PeminjamanId;

        /* Setting kembali */
        $data['url_kembali'] = get_referer_url();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('laboratorium/PeminjamanListBahan', $data);
        $this->load->view('templates/footer');
    }

    public function DetailPeminjamanListBahan(){
        $ListBahanId = $this->input->post('ListBahanId');

        $queryDetailListBahan = "
                        select
                        peminjamanlist.Id, peminjamanList.PeminjamanId, peminjamanList.BahanLabId, peminjamanList.Jumlah, peminjamanList.Status,
                        bahan.Nama NamaBahan,
                        lab.Nama NamaLab
                        from PeminjamanListBahan peminjamanList
                        join MasterBahan bahan on peminjamanList.BahanId = bahan.Id
                        join AkademikLaboratorium lab on peminjamanList.LabId = lab.Id
                        where peminjamanList.Id = $ListBahanId
                        ";

        $DetailListBahan = $this->db->query($queryDetailListBahan)->row_array();

        echo json_encode($DetailListBahan);
    }

    public function EditPeminjamanListBahan(){
        $PeminjamanListBahanId = $this->input->post('listBahanId');
        $PeminjamanId = $this->input->post('peminjamanId');
        $BahanLabId = $this->input->post('bahanLabId');

        /* Ambil Data table stokBahan */
        $queryStokBahan = "
                        select * from StokBahan
                        where PeminjamanId = $PeminjamanId AND BahanLabId = $BahanLabId
                        ";
        
        $StokBahan = $this->db->query($queryStokBahan)->row_array();


        $newStok = $StokBahan['JumlahAwal'] - $this->input->post('jumlah');

        if($newStok > 0){
            /* Ubah Jumlah Pada Table PeminjamanListBahan */
            $requestPeminjamanListBahan = [
                'Jumlah' => $this->input->post('jumlah')
            ];

            $this->db->update('PeminjamanListBahan', $requestPeminjamanListBahan, ['Id' => $PeminjamanListBahanId]);
    
            /* Update Jumlah Perubah Pada Table StokBahan */
            $requestStokBahan = [
                'JumlahPerubah' => $this->input->post('jumlah')
            ];
    
            $this->db->update('StokBahan', $requestStokBahan, ['Id' => $StokBahan['Id']]);
    
            /* Update Stok pada table BahanLaboratorium */
            $requestBahanLab = [
                'Stok' => $newStok,
            ];
    
            $this->db->update('BahanLaboratorium', $requestBahanLab, ['Id' => $BahanLabId]);
        }

        if($this->input->post('adminId') != null && $this->input->post('status') == 1){
            $requestSelesaiPeminjamanBahan = [
                'AdminId' => $this->input->post('adminId'),
                'Status' => $this->input->post('status')
            ];

            $this->db->update('PeminjamanListBahan', $requestSelesaiPeminjamanBahan, ['Id' => $PeminjamanListBahanId]);
        }

        redirect('Laboratorium/PeminjamanListBahan/'. $PeminjamanId);
    }

    public function TambahPeminjamanListBahan(){
        $datetime = new DateTime();
        $timezone = new DateTimeZone('Asia/Jakarta'); 
        $datetime->setTimezone($timezone);

        /* Atur Request Data */
        $PeminjamanId = $this->input->post('peminjamanId');

        $queryPeminjamanBahan = "
                                select * from PeminjamanBahan
                                where Id = $PeminjamanId
                                limit 1
                                ";
        $PeminjamanBahan = $this->db->query($queryPeminjamanBahan)->row_array();

        $countBahanLabId = count($this->input->post('bahanLabId'));
        $requestPeminjamanListBahan = [];
        for ($i = 1; $i < $countBahanLabId; $i++) {
            $bahanLabId = $this->input->post('bahanLabId')[$i];
            $bahan = $this->input->post('bahan')[$i - 1];
            $lab = $this->input->post('lab')[$i - 1];
            $stok = $this->input->post('stok')[$i - 1];

            /* Cek Bahan Agar Tidak Duplicate */
            $queryCekBahan = "
                            select * from PeminjamanListBahan
                            where PeminjamanId = $PeminjamanId AND 
                            BahanLabId = $bahanLabId AND BahanId = $bahan AND
                            LabId = $lab
                            ";

            $duplicateBahan = $this->db->query($queryCekBahan)->row_array();

            if ($bahanLabId != "" && $bahan != "" && $lab != "" && $stok != "" && $duplicateBahan == null) {
                $requestPeminjamanListBahan[] = [
                    'PeminjamanId' => $PeminjamanId,
                    'BahanLabId' => $bahanLabId,
                    'BahanId' => $bahan,
                    'LabId' => $lab,
                    'Jumlah' => $stok,
                    'Status' => 0,
                ];
            }
        }

        foreach($requestPeminjamanListBahan as $req){
            $BahanLabId = $req['BahanLabId'];

            /* Ambil Data Bahan Laboratorium  */
            $queryBahanLab = "
                        select * from BahanLaboratorium
                        where Id = $BahanLabId
                        ";

            $BahanLab = $this->db->query($queryBahanLab)->row_array();

            /* Update data Stok pada BahanLab */
            $newstok = $BahanLab['Stok'] - $req['Jumlah'];

            if ($newstok > 0){
                /* insert table Peminjaman List Bahan */
                $this->db->insert('PeminjamanListBahan', $req);

                $requestBahanLab = [
                    'Stok' => $newstok,
                ];
    
                $this->db->update('BahanLaboratorium', $requestBahanLab, ['Id' => $BahanLabId]);
    
                /* Memasukan data Pada table Stok */
                $requestStok = [
                    'BahanLabId' => $BahanLabId,
                    'Jenis' => 2,
                    'JumlahAwal' => $BahanLab['Stok'],
                    'JumlahPerubah' => $req['Jumlah'],
                    'Keterangan' => $PeminjamanBahan['Keterangan'],
                    'Tanggal' => $PeminjamanBahan['Tanggal'],
                    'CreatedAt' => $datetime->format("Y-m-d H:i:s"),
                    'PeminjamanId' => $PeminjamanId
                ];
    
                $this->db->insert('StokBahan', $requestStok);
            }
        }

        redirect('Laboratorium/PeminjamanListBahan/'. $PeminjamanId);
    }

    public function PengajuanBahanManual(){
        require_once 'vendor/autoload.php';
        $phpWord = new \PhpOffice\PhpWord\PhpWord();

        $datetime = new DateTime();
        $timezone = new DateTimeZone('Asia/Jakarta');
        $datetime->setTimezone($timezone);
        $formattedDate = $datetime->format('d F Y');
        
        $MhsId = $this->input->post('mhsId');
        $LabId = $this->input->post('lab');

        /* ambil data Mahasiswa : Nama, Nim, alamat di malang, Telpon, Email */
        $queryMahasiswa = "
                            select 
                            usr.Name NamaMhs, usr.Username NIM, usr.AlamatMalang, usr.Telpon, usr.Email
                            from Users usr
                            where Id = $MhsId
                            ";
        $Mahasiswa = $this->db->query($queryMahasiswa)->row_array();

        /* ambil data Laboratorium : NamaLab, NamaKalab, NIPKalab, NamaPLP, NIPPLP */
        $queryLab = "
                    select 
                    lab.Nama NamaLab,
                    kalab.Name NamaKalab, kalab.Username NIPKalab,
                    plp.Name NamaPLP, plp.Username NIPPLP
                    from AkademikLaboratorium lab
                    join Users kalab on lab.kalabId = kalab.Id
                    join Users plp on lab.PLPId = plp.Id
                    where lab.Id = $LabId
                    ";
        $Lab = $this->db->query($queryLab)->row_array();

        $ProgramStudi = $this->input->post('programStudi');
        $Status = $this->input->post('status');

        /* Ambil Template */
        $section = $phpWord->addSection();
        $document = new \PhpOffice\PhpWord\TemplateProcessor('assets/templates/word/TemplatePemakaianAlat.docx');
        
        /* Set Value */
        $document->setValue('namaMahasiswa', $Mahasiswa['NamaMhs']);
        $document->setValue('NIM', $Mahasiswa['NIM']);
        $document->setValue('alamatMalang', $Mahasiswa['AlamatMalang']);
        $document->setValue('telpon', $Mahasiswa['Telpon']);
        $document->setValue('email', $Mahasiswa['Email']);

        $document->setValue('status', $Status);
        $document->setValue('programStudi', $ProgramStudi);


        $document->setValue('namaLab', $Lab['NamaLab']);
        $document->setValue('namaKalab', $Lab['NamaKalab']);
        $document->setValue('NIPKalab', $Lab['NIPKalab']);
        $document->setValue('namaPLP', $Lab['NamaPLP']);
        $document->setValue('NIPPLP', $Lab['NIPPLP']);

        $document->setValue('tanggal', $formattedDate);

        /* Change Filename to userId-FormulirTA.pdf */
        $filename = $Mahasiswa['NIM'] . '-FormulirPemakaianAlat.docx';
        $directory = './assets/templates/temp/';
        $fullPath = $directory . $filename;
        $document->saveAs($fullPath);

        // Offer the document as a download link
        $this->load->helper('download');
        force_download($fullPath, NULL);

        sleep(5);

        /* Remove the file */
        unlink($fullPath);
    }

//     public function DownloadFormulirBonBahan(){

//         $datetime = new DateTime();
//         $timezone = new DateTimeZone('Asia/Jakarta');
//         $datetime->setTimezone($timezone);
//         $formattedDate = $datetime->format('d F Y');

//         require_once 'vendor/autoload.php';
//         $mpdf = new \Mpdf\Mpdf();

//         $labId = $this->input->post('LabId');
//         $PeminjamanId = $this->input->post('PeminjamanId');

//         $queryPeminjamanAlat = "
//                                 select 
//                                 listAlat.Jumlah JumlahPeminjaman,
//                                 peminjaman.Tujuan, peminjaman.TujuanLainnya,
//                                 alat.Nama NamaAlat, alat.Merk,
//                                 lab.Nama NamaLab,
//                                 mhs.Name NamaMhs, mhs.Username NIM, mhs.AlamatMalang, mhs.Telpon, mhs.Email, mhs.RoleId,
//                                 kalab.Name NamaKalab, kalab.Username NIPKalab,
//                                 plp.Name NamaPLP, plp.Username NIPPLP
//                                 from PeminjamanListAlat listAlat
//                                 join PeminjamanAlat peminjaman on listAlat.PeminjamanId = peminjaman.Id
//                                 join Users mhs on peminjaman.MhsId = mhs.Id
//                                 join MasterAlat alat on listAlat.AlatId = alat.Id
//                                 join AkademikLaboratorium lab on listAlat.LabId = lab.Id
//                                 join Users kalab on lab.KalabId = kalab.Id
//                                 join Users plp on lab.PLPId = plp.Id
//                                 where listAlat.LabId = $labId AND listAlat.PeminjamanId = $PeminjamanId
//                                 ";
//         $PeminjamanAlat = $this->db->query($queryPeminjamanAlat)->result_array();

//         switch ($PeminjamanAlat[0]['RoleId']){
//             case 2:
//                 $Status = "Mahasiswa S1";
//                 break;
//             case 3:
//                 $Status = "Mahasiswa S2";
//                 break;
//             case 4:
//                 $Status = "Mahasiswa S3";
//                 break;
//         };


//         $html = '
//         <!DOCTYPE html>
//         <html lang="en">

//         <head>
//             <meta charset="UTF-8">
//             <meta name="viewport" content="width=device-width, initial-scale=1.0">
//             <title>Formulir Pemakaian Alat Laboratorium</title>
//             <style>
//             body {
//                 font-family: Arial, sans-serif;
//                 margin: 20px;
//                 font-size: 12px;
//             }

//             h1 {
//                 text-align: center;
//                 font-weight: bold;
//             }

//             main {
//                 margin: 0 auto;
//                 max-width: 600px;
//             }

//             label {
//                 display: inline-block;
//                 width: 200px;
//                 margin-bottom: 10px;
//                 font-size: 12px;
//             }

//             table {
//                 width: 100%;
//                 border-collapse: collapse;
//                 margin-top: 20px;
//             }

//             th, td {
//                 border: 1px solid #ddd;
//                 padding: 8px;
//                 text-align: left;
//             }

//             th {
//                 background-color: #f2f2f2;
//             }

//             .rangkasurat {
//                 width: 650px;
//                 margin: auto;
//                 background-color: #fff;
//                 /* height: 500px; */
//             }
            
//         </style>
        
//         </head>

//         <body>
//         <header>
//             <div class="rangkasurat">
//             <table width="100%">
//                 <tr>
//                         <td style="border: none;"><img src="/Users/rizqi/Documents/Sites/SikmaAdmin/assets/img/icons/ub.png" width="100px"> </td>
//                         <td style="border: none;">
//                         <h3>KEMENTERIAN PENDIDIKAN, KEBUDAYAAN, RISET, DAN TEKNOLOGi</h3>
//                         <h3>UNIVERSITAS BRAWIJAYA</h3>
//                         </td>
//                         <td width="27%"  style="border: none;">
//                         <h4>Fakultas Matematika Dan Ilmu Pengetahuan Alam</h4>
//                         <h5>Jl. Veteran, Malang 65145, Indonesia Telp-fax : +62341 554403, 551611</h5>
//                         </td>
//                     </tr>
//             </table>
//             </div>
//         </header>
//             <h1 style="padding-top: 30px;">Formulir Pemakaian Alat Laboratorium '. $PeminjamanAlat[0]['NamaLab'] .'</h1>
//             <main>
//             <div class="dataDiri" style="padding-left:50px; padding-top:30px;">
//                 <table>
//                 <tr>
//                     <td width="180px" style="border: none;">Nama</td>
//                     <td style="border: none;">: '. $PeminjamanAlat[0]['NamaMhs'] .'</td>
//                 </tr>
//                 <tr>
//                     <td width="180px" style="border: none;">NIM</td>
//                     <td style="border: none;">: '. $PeminjamanAlat[0]['NIM'] .'</td>
//                 </tr>
//                 <tr>
//                     <td width="180px" style="border: none;">Status</td>
//                     <td style="border: none;">: '. $Status .'</td>
//                 </tr>
//                 <tr>
//                     <td width="180px" style="border: none;">Departemen</td>
//                     <td style="border: none;">: Kimia Fakultas MIPA Universitas Brawijaya</td>
//                 </tr>
//                 <tr>
//                     <td width="180px" style="border: none;">Alamat di Malang</td>
//                     <td style="border: none;">: '. $PeminjamanAlat[0]['AlamatMalang'] .'</td>
//                 </tr>
//                 <tr>
//                     <td width="180px" style="border: none;">Telpon</td>
//                     <td style="border: none;">: '. $PeminjamanAlat[0]['Telpon'] .'</td>
//                 </tr>
//                 <tr>
//                     <td width="180px" style="border: none;">Email</td>
//                     <td style="border: none;">: '. $PeminjamanAlat[0]['Email'] .'</td>
//                 </tr>
//                 </table>
//             </div>

//                 <table>
//                     <thead>
//                         <tr>
//                             <th>No</th>
//                             <th>Nama Alat</th>
//                             <th>Merk</th>
//                             <th>Alat</th>
//                             <th>Keperluan</th>
//                             <th>Harga</th>
//                         </tr>
//                     </thead>
//                     <tbody>';

//                     $i = 1;
//                     foreach($PeminjamanAlat as $peminjaman){
//                         switch ($peminjaman['Tujuan']){
//                             case 1:
//                                 $tujuan = "Penelitian Dosen";
//                                 break;
//                             case 2:
//                                 $tujuan = "Proyek Kimia";
//                                 break;
//                             case 3:
//                                 $tujuan = "Program Kreatifitas Mahasiswa";
//                                 break;
//                             case 4:
//                                 $tujuan = "Penelitian Skripsi";
//                                 break;
//                             case 5:
//                                 $tujuan = "Penelitian Tesis";
//                                 break;
//                             case 6:
//                                 $tujuan = "Penelitian Disertasi";
//                                 break;
//                             case 7:
//                                 $tujuan = "Analisis Sampel";
//                                 break;
//                             case 8:
//                                 $tujuan = $peminjaman['TujuanLainnya'];
//                                 break;
//                         }

//                         $html .= '
//                             <tr>
//                                 <td>'.$i.'</td>
//                                 <td>'.$peminjaman['NamaAlat'].'</td>
//                                 <td>'.$peminjaman['Merk'].'</td>
//                                 <td>'.$peminjaman['JumlahPeminjaman'].'</td>
//                                 <td>'.$tujuan.'</td>
//                                 <td></td>
//                             </tr>
//                         ';
//                     $i++;
//                     };


// $html .= '
//                     </tbody>
//                 </table>

//                 <div class="rangkasurat" style="padding-left:50px; padding-top:30px;">
//                 <table width="100%">
//                     <tr>
//                     <td style="border: none;"></td>
//                     <td style="border: none;"></td>
//                     <td style="border: none;">Malang, '.$formattedDate.'</td>
//                     </tr>
//                     <tr>
//                     <td style="border: none;">Kalab. '. $PeminjamanAlat[0]['NamaLab'] .'</td>
//                     <td style="border: none;">PLP</td>
//                     <td style="border: none;">Yang Mengajukan</td>
//                     </tr>
//                     <tr><td style="border: none;"></td></tr>
//                     <tr><td style="border: none;"></td></tr>
//                     <tr><td style="border: none;"></td></tr>
//                     <tr><td style="border: none;"></td></tr>
//                     <tr><td style="border: none;"></td></tr>
//                     <tr>
//                     <td style="border: none;">'. $PeminjamanAlat[0]['NamaKalab'] .'</td>
//                     <td style="border: none;">'. $PeminjamanAlat[0]['NamaPLP'] .'</td>
//                     <td style="border: none;">'. $PeminjamanAlat[0]['NamaMhs'] .'</td>
//                     </tr>
//                     <tr>
//                     <td style="border: none;">'. $PeminjamanAlat[0]['NIPKalab'] .'</td>
//                     <td style="border: none;">'. $PeminjamanAlat[0]['NIPPLP'] .'</td>
//                     <td style="border: none;">'. $PeminjamanAlat[0]['NIM'] .'</td>
//                     </tr>
//                 </table>
//                 </div>
//             </main>
//         </body>

//         </html>
//         ';

//         $mpdf->WriteHTML($html);
//         $mpdf->Output('Bon Alat-'.$PeminjamanAlat[0]['NamaMhs'].'.pdf', \Mpdf\Output\Destination::DOWNLOAD);

//         die();
//     }

}