<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Surat extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->library('form_validation');
        if (!$this->session->userdata('Id')){
            redirect('auth');
        }
    }

    public function index(){
        $data['title'] = 'Permohonan Surat';
        $Id = $this->session->userdata['Id'];
        $RoleId = $this->session->userdata['RoleId'];
        $query = "select * from Users where Id = $Id";
        $User = $this->db->query($query)->row_array();
        $data['User'] = $User;
        
        /* Setting kembali */
        $data['url_kembali'] = get_referer_url();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('permohonanSurat/index', $data);
        $this->load->view('templates/footer');
    }

    /* Pengajuan Bebas Lab */
    public function SuratBebasLab($id = null){
        $data['title'] = 'Surat Bebas Lab';
        if ($id == null){
            $id = $this->session->userdata['Id'];
        }

        $roleId = $this->session->userdata['RoleId'];

        $kadep = $this->session->userdata['Kadep'];
        $sekdep = $this->session->userdata['Sekdep'];
        $kaprodiS1 = $this->session->userdata['KaprodiS1'];
        $kaprodiS2 = $this->session->userdata['KaprodiS2'];
        $kaprodiS3 = $this->session->userdata['KaprodiS3'];

        $mhsRole = [2,3,4];
        $dsnRole = [1];
        if (in_array($roleId, $mhsRole)){
            $query = "
                        select mhs.*, 
                        usr.Id, usr.Name, usr.Username, usr.Telpon, usr.Email, usr.RoleId, usr.CreatedAt, usr.Status, usr.ImageProfile, usr.AlamatMalang, usr.Alamat
                        from Users usr
                        join UserMahasiswa mhs on usr.Id = mhs.UserId
                        where usr.Id = '$id' 
                        ";

            /* Ambil Surat Bebas Lab */
            $queryBebasLab = "
                            select surat.*,
                            usr.Name NamaMahasiswa
                            from SuratBebasLab surat
                            join Users usr on usr.Id = surat.MhsId
                            where MhsId = $id
                            ";

            $SuratBebasLab = $this->db->query($queryBebasLab)->result_array();

            $data["SuratBebasLab"] = $SuratBebasLab;
        }else if(in_array($roleId, $dsnRole)  && ($kadep == false && $sekdep == false && $kaprodiS1 == false && $kaprodiS2 == false && $kaprodiS3 == false)){
            $query = "
                        select dsn.*, 
                        usr.Id, usr.Name, usr.Username, usr.Telpon, usr.Email, usr.RoleId, usr.CreatedAt, usr.Status, usr.ImageProfile, usr.AlamatMalang, usr.Alamat
                        from Users usr
                        join UserTenagaKependidikan dsn on usr.Id = dsn.UserId
                        where usr.Id = '$id'
                        ";

            /* Ambil Surat Bebas Lab */
            $queryBebasLab = "
                        select surat.*,
                        usr.Name NamaMahasiswa
                        from SuratBebasLab surat
                        join Users usr on usr.Id = surat.MhsId
                        ";

            $SuratBebasLab = $this->db->query($queryBebasLab)->result_array();

            $data["SuratBebasLab"] = $SuratBebasLab;
        }else{
            $query = "select * from Users where Id = '$id'";
            
            /* Ambil Surat Bebas Lab */
            $queryBebasLab = "
                            select surat.*,
                            usr.Name NamaMahasiswa
                            from SuratBebasLab surat
                            join Users usr on usr.Id = surat.MhsId
                            ";

            $SuratBebasLab = $this->db->query($queryBebasLab)->result_array();

            $data["SuratBebasLab"] = $SuratBebasLab;
        }

        $User = $this->db->query($query)->row_array();
        $data['User'] = $User;

        /* Ambil Data Lab */
        $queryLab ="
                select * from AkademikLaboratorium
                where Status = 1
        ";

        $Lab = $this->db->query($queryLab)->result_array();
        $data["Lab"] = $Lab;

        /* Ambil List Dosen */
        $queyrDosen = "select *
                        from Users
                        where RoleId in ('1')
                        ";

        $Dosen = $this->db->query($queyrDosen)->result_array();
        $data['Dosen'] = $Dosen;

        /* Setting kembali */
        $data['url_kembali'] = get_referer_url();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('permohonanSurat/BebasLab', $data);
        $this->load->view('templates/footer');
    }

    public function PengajuanSuratBebasLab(){
        $datetime = new DateTime();
        $timezone = new DateTimeZone('Asia/Jakarta'); // Waktu Indonesia Barat (WIB)
        $datetime->setTimezone($timezone);

        $MhsId = htmlspecialchars($this->input->post('mhsId'));

        $selectedLab = implode(',', $this->input->post('selectedLab[]'));

        $requestBebasLab = [
            'MhsId' => $MhsId,
            'LabId' => $selectedLab,
            'ProgramStudi' => htmlspecialchars($this->input->post('programStudi')),
            // 'DosenId' => htmlspecialchars($this->input->post('dosen')),
            // 'Tujuan' => htmlspecialchars($this->input->post('tujuan')),
            // 'TujuanLainnya' => "-",
            'valid' => 0,
            'CreatedAt' => $datetime->format("Y-m-d H:i:s")
        ];

        // if ($this->input->post('tujuan') == 8){
        //     $requestBebasLab['TujuanLainnya'] = htmlspecialchars($this->input->post('tujuanLainnya'));
        // }

        $this->db->insert('SuratBebasLab', $requestBebasLab);

        redirect('surat/SuratBebasLab');
    }

    public function DownloadSuratBebasLab($SuratId = null){
        require_once 'vendor/autoload.php';
        $phpWord = new \PhpOffice\PhpWord\PhpWord();

        /* Ambil Data Dari DB */
        /* Get Data Mahasiswa */
        $queryMahasiswa = "
                            select 
                            usr.Name NamaMahasiswa,
                            usr.Username, 
                            surat.ProgramStudi
                            FROM
                                SuratBebasLab surat
                            JOIN Users usr ON surat.MhsId = usr.Id
                            where surat.Id = $SuratId
                        ";
        // $queryMahasiswa = "
        //                     select 
        //                     usr.Name NamaMahasiswa,
        //                     usr.Username,
        //                     dsn.Name NamaDosen,
        //                     surat.Tujuan, surat.TujuanLainnya, surat.ProgramStudi
        //                     FROM
        //                         SuratBebasLab surat
        //                     JOIN Users usr ON surat.MhsId = usr.Id
        //                     JOIN Users dsn ON surat.DosenId = dsn.Id
        //                     where surat.Id = $SuratId
        //                 ";

        $DataMahasiswa = $this->db->query($queryMahasiswa)->row_array();

        // switch ($DataMahasiswa['Tujuan']){
        //     case 1:
        //         $Tujuan = "Penelitian Dosen";
        //         break;
        //     case 2:
        //         $Tujuan = "Proyek Kimia";
        //         break;
        //     case 3:
        //         $Tujuan = "Program Kreatifitas Mahasiswa";
        //         break;
        //     case 4:
        //         $Tujuan = "Penelitian Skripsi";
        //         break;
        //     case 5:
        //         $Tujuan = "Penelitian Tesis";
        //         break;
        //     case 6:
        //         $Tujuan = "Penelitian Disertasi";
        //         break;
        //     case 7:
        //         $Tujuan = "Analisis Sampel";
        //         break;
        //     case 8:
        //         $Tujuan = $DataMahasiswa['TujuanLainnya'];
        //         break;
        // }

        /* Get Data Surat */
        $query = "
                    SELECT
                        lab.Nama AS LabName,
                        kalab.Name AS KalabName,
                        Surat.CreatedAt AS tglPengajuan
                    FROM
                        SuratBebasLab Surat
                    JOIN AkademikLaboratorium lab ON FIND_IN_SET(lab.Id, Surat.LabId) > 0
                    JOIN Users kalab ON FIND_IN_SET(kalab.Id, lab.KalabId) > 0
                    where Surat.Id = $SuratId
                ";

        $DataSurat = $this->db->query($query)->result_array();

        /* Ambil Data Kadep */
        $queryKadep = "
                    select jajaran.KadepId, kadep.Name as Kadep, kadep.Username as NIP
                    from AkademikJajaran jajaran
                    join Users kadep on jajaran.KadepId = kadep.Id
                    ";

        $Kadep = $this->db->query($queryKadep)->row_array();
        
        /* Ambil Template */
        $rowCount = count($DataSurat);
        $section = $phpWord->addSection();
        switch ($rowCount){
            case 1:
                $document = new \PhpOffice\PhpWord\TemplateProcessor('assets/templates/word/suratBebasLab_1.docx');
                break;
            case 2:
                $document = new \PhpOffice\PhpWord\TemplateProcessor('assets/templates/word/suratBebasLab_2.docx');
                break;
            case 3:
                $document = new \PhpOffice\PhpWord\TemplateProcessor('assets/templates/word/suratBebasLab_3.docx');
                break;
            case 4:
                $document = new \PhpOffice\PhpWord\TemplateProcessor('assets/templates/word/suratBebasLab_4.docx');
                break;
            case 5:
                $document = new \PhpOffice\PhpWord\TemplateProcessor('assets/templates/word/suratBebasLab_5.docx');
                break;
            case 6:
                $document = new \PhpOffice\PhpWord\TemplateProcessor('assets/templates/word/suratBebasLab_6.docx');
                break;
            case 7:
                $document = new \PhpOffice\PhpWord\TemplateProcessor('assets/templates/word/suratBebasLab_7.docx');
                break;
        }
        
        /* Set Value */
        $document->setValue('namaMahasiswa', $DataMahasiswa['NamaMahasiswa']);
        $document->setValue('NIM', $DataMahasiswa['Username']);
        $document->setValue('programStudi', $DataMahasiswa['ProgramStudi']);

        // $document->setValue('namaDosen', $DataMahasiswa['NamaDosen']);
        // $document->setValue('tujuan', $Tujuan);

        // $document->setValue('tgl-bln-thn', date("d-m-Y", strtotime($DataSurat[0]['tglPengajuan'])));
        // $document->setValue('Kadep', $Kadep['Kadep']);
        // $document->setValue('NIP', $Kadep['NIP']);

         // Populate Table Data
        for ($i = 0; $i < $rowCount; $i++) {
            $document->setValue('LabName' . ($i + 1), $DataSurat[$i]['LabName']);
            $document->setValue('KalabName' . ($i + 1), $DataSurat[$i]['KalabName']);
        }

        if ($rowCount < 7){
            for ($i = $rowCount; $i < 7; $i++) {
                $document->setValue('LabName' . ($i + 1), "");
                $document->setValue('KalabName' . ($i + 1), "");
            }
        };

        /* Change Filename to userId-FormulirTA.pdf */
        $filename = $SuratId . '-SuratBebasLab.docx';
        $directory = './assets/templates/temp/';
        $fullPath = $directory . $filename;
        $document->saveAs($fullPath);

        // Offer the document as a download link
        $this->load->helper('download');
        force_download($fullPath, NULL);

        sleep(5);

        /* Remove the file */
        unlink($fullPath);
    }

    public function ValidasiSuratKeterangan(){
        $SuratId = $this->input->post('SuratId');
        $requestData = [
            'Valid' => $this->input->post('validasiSuratKeterangan'),
        ];

        $this->db->update('SuratBebasLab', $requestData, "Id = $SuratId");

        redirect('surat/SuratBebasLab');
    }

    public function DownloadSuratKeteranganBebasLab($SuratId = null){
        require_once 'vendor/autoload.php';
        $phpWord = new \PhpOffice\PhpWord\PhpWord();

        $datetime = new DateTime();
        $timezone = new DateTimeZone('Asia/Jakarta');
        $datetime->setTimezone($timezone);
        $formattedDate = $datetime->format('d F Y');

        /* Ambil Data Dari DB */
        /* Get Data Mahasiswa */
        $queryMahasiswa = "
                            select 
                            usr.Name NamaMahasiswa,
                            usr.Username,
                            surat.ProgramStudi
                            FROM
                                SuratBebasLab surat
                            JOIN Users usr ON surat.MhsId = usr.Id
                            where surat.Id = $SuratId
                        ";

        $DataMahasiswa = $this->db->query($queryMahasiswa)->row_array();

        /* Ambil Data Kadep */
        $queryKadep = "
                    select jajaran.KadepId, kadep.Name as Kadep, kadep.Username as NIP
                    from AkademikJajaran jajaran
                    join Users kadep on jajaran.KadepId = kadep.Id
                    ";

        $Kadep = $this->db->query($queryKadep)->row_array();
        
        /* Ambil Template */
        $section = $phpWord->addSection();
        $document = new \PhpOffice\PhpWord\TemplateProcessor('assets/templates/word/suratKeteranganBebasLab.docx');
        
        
        /* Set Value */
        $document->setValue('namaMahasiswa', $DataMahasiswa['NamaMahasiswa']);
        $document->setValue('NIM', $DataMahasiswa['Username']);
        $document->setValue('prodi', $DataMahasiswa['ProgramStudi']);

        $document->setValue('tgl-bln-thn', $formattedDate);
        $document->setValue('Kadep', $Kadep['Kadep']);
        $document->setValue('NIP', $Kadep['NIP']);

        /* Change Filename to userId-FormulirTA.pdf */
        $filename = $SuratId . '-SuratKeteranganBebasLab.docx';
        $directory = './assets/templates/temp/';
        $fullPath = $directory . $filename;
        $document->saveAs($fullPath);

        // Offer the document as a download link
        $this->load->helper('download');
        force_download($fullPath, NULL);

        sleep(5);

        /* Remove the file */
        unlink($fullPath);
    }

    /* Pengajuan Surat ijin Kerja */
    public function SuratIjinKerja($id = null){
        $data['title'] = 'Surat Ijin Kerja';
        if ($id == null){
            $id = $this->session->userdata['Id'];
        }

        $roleId = $this->session->userdata['RoleId'];

        $kadep = $this->session->userdata['Kadep'];
        $sekdep = $this->session->userdata['Sekdep'];
        $kaprodiS1 = $this->session->userdata['KaprodiS1'];
        $kaprodiS2 = $this->session->userdata['KaprodiS2'];
        $kaprodiS3 = $this->session->userdata['KaprodiS3'];

        $mhsRole = [2,3,4];
        $dsnRole = [1];
        if (in_array($roleId, $mhsRole)){
            $query = "
                        select mhs.*, 
                        usr.Id, usr.Name, usr.Username, usr.Telpon, usr.Email, usr.RoleId, usr.CreatedAt, usr.Status, usr.ImageProfile, usr.AlamatMalang, usr.Alamat
                        from Users usr
                        join UserMahasiswa mhs on usr.Id = mhs.UserId
                        where usr.Id = '$id' 
                        ";

            /* Ambil Surat Ijin Kerja Lab */
            $queryIjinKerjaLab = "
                            select surat.*,
                            usr.Name NamaMahasiswa,
                            lab.Nama NamaLab
                            from SuratIjinKerjaLab surat
                            join Users usr on usr.Id = surat.MhsId
                            join AkademikLaboratorium lab on surat.LabId = lab.Id
                            where MhsId = $id
                            ";

            $SuratIjinKerjaLab = $this->db->query($queryIjinKerjaLab)->result_array();
            
            foreach ($SuratIjinKerjaLab as $key => $surat){
                // Buat objek DateTime dari string tanggal
                $dateMulai = DateTime::createFromFormat('Y-m-d', $surat['TanggalMulai']);
                $dateSelesai = DateTime::createFromFormat('Y-m-d', $surat['TanggalSelesai']);

                // Ubah format tanggal
                $formattedDateMulai = $dateMulai->format('d-m-Y');
                $formattedDateSelesai = $dateSelesai->format('d-m-Y');

                // Simpan kembali dalam variabel $surat['TanggalMulai']
                $SuratIjinKerjaLab[$key]['TanggalMulai'] = $formattedDateMulai;
                $SuratIjinKerjaLab[$key]['TanggalSelesai'] = $formattedDateSelesai;
            }

            $data["SuratIjinKerjaLab"] = $SuratIjinKerjaLab;

        }else if(in_array($roleId, $dsnRole)  && ($kadep == false && $sekdep == false && $kaprodiS1 == false && $kaprodiS2 == false && $kaprodiS3 == false)){
            $query = "
                        select dsn.*, 
                        usr.Id, usr.Name, usr.Username, usr.Telpon, usr.Email, usr.RoleId, usr.CreatedAt, usr.Status, usr.ImageProfile, usr.AlamatMalang, usr.Alamat
                        from Users usr
                        join UserTenagaKependidikan dsn on usr.Id = dsn.UserId
                        where usr.Id = '$id'
                        ";

            /* Ambil Surat Ijin Kerja Lab */
            $queryIjinKerjaLab = "
                        select surat.*,
                        usr.Name NamaMahasiswa,
                        lab.Nama NamaLab
                        from SuratIjinKerjaLab surat
                        join Users usr on usr.Id = surat.MhsId
                        join AkademikLaboratorium lab on surat.LabId = lab.Id
                        ";

            $SuratIjinKerjaLab = $this->db->query($queryIjinKerjaLab)->result_array();

            $data["SuratIjinKerjaLab"] = $SuratIjinKerjaLab;
        }else{
            $query = "select * from Users where Id = '$id'";
            
            /* Ambil Surat Ijin Kerja Lab */
            $queryIjinKerjaLab = "
                            select surat.*,
                            usr.Name NamaMahasiswa,
                            lab.Nama NamaLab
                            from SuratIjinKerjaLab surat
                            join Users usr on usr.Id = surat.MhsId
                            join AkademikLaboratorium lab on surat.LabId = lab.Id
                            ";

            $SuratIjinKerjaLab = $this->db->query($queryIjinKerjaLab)->result_array();

            $data["SuratIjinKerjaLab"] = $SuratIjinKerjaLab;
        }

        $User = $this->db->query($query)->row_array();
        $data['User'] = $User;

        /* Ambil Data Lab */
        $queryLab = "
                    select * from AkademikLaboratorium
                    where Status = 1
                    ";

        $Lab = $this->db->query($queryLab)->result_array();
        $data["Lab"] = $Lab;

        /* Ambil List Dosen */
        $queyrDosen = "select *
                        from Users
                        where RoleId in ('1')
                        ";

        $Dosen = $this->db->query($queyrDosen)->result_array();
        $data['Dosen'] = $Dosen;

        /* Setting kembali */
        $data['url_kembali'] = get_referer_url();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('permohonanSurat/IjinKerjaLab', $data);
        $this->load->view('templates/footer');
    }

    public function PengajuanSuratIjinKerjaLab(){
        $datetime = new DateTime();
        $timezone = new DateTimeZone('Asia/Jakarta'); // Waktu Indonesia Barat (WIB)
        $datetime->setTimezone($timezone);

        $MhsId = htmlspecialchars($this->input->post('mhsId'));


        $requestIjinKerjaLab = [
            'MhsId' => $MhsId,
            'LabId' => htmlspecialchars($this->input->post('lab')),
            'DosenPembimbingId' => htmlspecialchars($this->input->post('dosen')),
            'Tujuan' => htmlspecialchars($this->input->post('tujuan')),
            'TujuanLainnya' => "-",
            'JudulPenelitian' => htmlspecialchars($this->input->post('judul')),
            'TanggalMulai' => htmlspecialchars($this->input->post('tanggalMulai')),
            'TanggalSelesai' => htmlspecialchars($this->input->post('tanggalSelesai')),
            'CreatedAt' => $datetime->format("Y-m-d H:i:s")
        ];

        if ($this->input->post('tujuan') == 8){
            $requestIjinKerjaLab['TujuanLainnya'] = htmlspecialchars($this->input->post('tujuanLainnya'));
        }

        $this->db->insert('SuratIjinKerjaLab', $requestIjinKerjaLab);

        redirect('surat/SuratIjinKerja');
    }

    public function DownloadSuratIjinKerjaLab($SuratId = null){
        require_once 'vendor/autoload.php';
        $phpWord = new \PhpOffice\PhpWord\PhpWord();

        $datetime = new DateTime();
        $timezone = new DateTimeZone('Asia/Jakarta');
        $datetime->setTimezone($timezone);
        $formattedDate = $datetime->format('d F Y');

        /* Ambil Data Dari DB */
        /* Get Data Surat */
        $querySurat = "
                            select 
                            usr.Name NamaMahasiswa, usr.AlamatMalang, usr.Telpon, usr.Email,
                            surat.Tujuan, surat.TujuanLainnya, surat.TanggalMulai, surat.TanggalSelesai, surat.JudulPenelitian,
                            lab.Nama NamaLab,
                            dsn.Name NamaDosenPembimbing,
                            kalab.Name NamaKalab
                            FROM
                                SuratIjinKerjaLab surat
                            JOIN Users usr ON surat.MhsId = usr.Id
                            JOIN Users dsn ON surat.DosenPembimbingId = dsn.Id
                            JOIN AkademikLaboratorium lab ON surat.LabId = lab.Id
                            JOIN Users kalab ON lab.KalabId = kalab.Id
                            where surat.Id = $SuratId
                        ";

        $DataSurat = $this->db->query($querySurat)->row_array();

        switch ($DataSurat['Tujuan']){
            case 1:
                $Tujuan = "Penelitian Dosen";
                break;
            case 2:
                $Tujuan = "Proyek Kimia";
                break;
            case 3:
                $Tujuan = "Program Kreatifitas Mahasiswa";
                break;
            case 4:
                $Tujuan = "Penelitian Skripsi";
                break;
            case 5:
                $Tujuan = "Penelitian Tesis";
                break;
            case 6:
                $Tujuan = "Penelitian Disertasi";
                break;
            case 7:
                $Tujuan = "Analisis Sampel";
                break;
            case 8:
                $Tujuan = $DataSurat['TujuanLainnya'];
                break;
        }

        // Buat objek DateTime dari string tanggal
        $dateMulai = DateTime::createFromFormat('Y-m-d', $DataSurat['TanggalMulai']);
        $dateSelesai = DateTime::createFromFormat('Y-m-d', $DataSurat['TanggalSelesai']);

        // Ubah format tanggal
        $formattedDateMulai = $dateMulai->format('d-m-Y');
        $formattedDateSelesai = $dateSelesai->format('d-m-Y');

        $DataSurat['TanggalMulai'] = $formattedDateMulai;
        $DataSurat['TanggalSelesai'] = $formattedDateSelesai;

        /* Ambil Data Kadep */
        $queryKadep = "
                    select jajaran.KadepId, kadep.Name as Kadep, kadep.Username as NIP
                    from AkademikJajaran jajaran
                    join Users kadep on jajaran.KadepId = kadep.Id
                    ";

        $Kadep = $this->db->query($queryKadep)->row_array();
        
        /* Ambil Template */
        $section = $phpWord->addSection();
        $document = new \PhpOffice\PhpWord\TemplateProcessor('assets/templates/word/suratIjinKerjaLab.docx');
        
        /* Set Value */
        $document->setValue('namaMahasiswa', $DataSurat['NamaMahasiswa']);
        $document->setValue('alamatMalang', $DataSurat['AlamatMalang']);
        $document->setValue('telpon', $DataSurat['Telpon']);
        $document->setValue('email', $DataSurat['Email']);
        $document->setValue('tujuan', $Tujuan);
        $document->setValue('namaLab', $DataSurat['NamaLab']);
        $document->setValue('tglMulai', $DataSurat['TanggalMulai']);
        $document->setValue('tglSelesai', $DataSurat['TanggalSelesai']);
        $document->setValue('judul', $DataSurat['JudulPenelitian']);
        $document->setValue('namaKalab', $DataSurat['NamaKalab']);
        $document->setValue('namaDosenPembimbing', $DataSurat['NamaDosenPembimbing']);

        $document->setValue('tgl-bln-thn', $formattedDate);
        $document->setValue('namaKadep', $Kadep['Kadep']);

        /* Change Filename to userId-FormulirTA.pdf */
        $filename = $SuratId . '-SuratIjinKerjaLab.docx';
        $directory = './assets/templates/temp/';
        $fullPath = $directory . $filename;
        $document->saveAs($fullPath);

        // Offer the document as a download link
        $this->load->helper('download');
        force_download($fullPath, NULL);

        sleep(5);

        /* Remove the file */
        unlink($fullPath);
    }

    public function UploadFileSuratKerjaLab(){
        $uploadPath = './assets/uploads/suratKerja_lab/';
        $config['upload_path'] = $uploadPath;
        $config['allowed_types'] = 'pdf|doc|docx';

        $SuratId = $this->input->post('suratId');

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('suratKerjaLab')) {
            $SuratIjinKerjaLab = $this->db->get_where('SuratIjinKerjaLab', ['Id' => $SuratId])->row_array();

            // File upload success
            $data = $this->upload->data();
            $uploaded_filename = $data['file_name'];
            $extention = pathinfo($uploaded_filename, PATHINFO_EXTENSION);
            $source_path = './assets/uploads/suratKerja_lab/' . $uploaded_filename;
            $new_filename = $SuratId.'-FileSuratKerjaLab.'.$extention; // Change this to your desired new name
            $new_path = './assets/uploads/suratKerja_lab/' . $new_filename;

            if (rename($source_path, $new_path)) {
                if ($SuratIjinKerjaLab['FileSuratKerjaLab'] != "" && $SuratIjinKerjaLab['FileSuratKerjaLab'] != $new_filename && $SuratIjinKerjaLab['FileSuratKerjaLab'] != null){
                    unlink('./assets/uploads/suratKerja_lab/'. $SuratIjinKerjaLab['FileSuratKerjaLab']);
                }

                $datetime = new DateTime;
                $timezone = new DateTimeZone('Asia/Jakarta'); 
        
                $datetime->setTimezone($timezone);

                $requestUploadFileSuratKerjaLab = [
                    'FileSuratKerjaLab' => $new_filename
                ];

                $this->db->update('SuratIjinKerjaLab', $requestUploadFileSuratKerjaLab, "Id = $SuratId");

                redirect('surat/SuratIjinKerja');
            }

            // $response = 'File uploaded successfully: ' . $data['file_name'];
        } else {
            // File upload failed
            $response = 'File upload failed: ' . $this->upload->display_errors();
        }
    }

    /* Pengajuan Surat Lembur */
    public function SuratIjinLembur($UserId = null){
        $data['title'] = 'Surat Ijin Lembur';
        if ($UserId == null){
            $UserId = $this->session->userdata['Id'];
        }

        $roleId = $this->session->userdata['RoleId'];

        $kadep = $this->session->userdata['Kadep'];
        $sekdep = $this->session->userdata['Sekdep'];
        $kaprodiS1 = $this->session->userdata['KaprodiS1'];
        $kaprodiS2 = $this->session->userdata['KaprodiS2'];
        $kaprodiS3 = $this->session->userdata['KaprodiS3'];

        $mhsRole = [2,3,4];
        $dsnRole = [1];
        if (in_array($roleId, $mhsRole)){
            $query = "
                        select mhs.*, 
                        usr.Id, usr.Name, usr.Username, usr.Telpon, usr.Email, usr.RoleId, usr.CreatedAt, usr.Status, usr.ImageProfile, usr.AlamatMalang, usr.Alamat
                        from Users usr
                        join UserMahasiswa mhs on usr.Id = mhs.UserId
                        where usr.Id = '$UserId' 
                        ";

            /* Ambil Surat Ijin Lembur */
            $queryIjinLembur = "
                        select surat.*,
                        usr.Name NamaMahasiswa,
                        lab.Nama NamaLab
                        from SuratIjinLembur surat
                        join Users usr on usr.Id = surat.MhsId
                        join AkademikLaboratorium lab on surat.LabId = lab.Id
                        where MhsId = $UserId
                        ";

            $SuratIjinLembur = $this->db->query($queryIjinLembur)->result_array();

            $data["SuratIjinLembur"] = $SuratIjinLembur;

        }else if(in_array($roleId, $dsnRole)  && ($kadep == false && $sekdep == false && $kaprodiS1 == false && $kaprodiS2 == false && $kaprodiS3 == false)){
            $query = "
                        select dsn.*, 
                        usr.Id, usr.Name, usr.Username, usr.Telpon, usr.Email, usr.RoleId, usr.CreatedAt, usr.Status, usr.ImageProfile, usr.AlamatMalang, usr.Alamat
                        from Users usr
                        join UserTenagaKependidikan dsn on usr.Id = dsn.UserId
                        where usr.Id = '$UserId'
                        ";

            /* Amnil Surat Lembur */
            $queryIjinLembur = "
                                select surat.*,
                                usr.Name NamaMahasiswa,
                                lab.Nama NamaLab
                                from SuratIjinLembur surat
                                join Users usr on usr.Id = surat.MhsId
                                join AkademikLaboratorium lab on surat.LabId = lab.Id
                                ";

            $SuratIjinLembur = $this->db->query($queryIjinLembur)->result_array();

            $data["SuratIjinLembur"] = $SuratIjinLembur;
        }else{
            $query = "select * from Users where Id = '$UserId'";
            
            /* Amnil Surat Lembur */
            $queryIjinLembur = "
                                select surat.*,
                                usr.Name NamaMahasiswa,
                                lab.Nama NamaLab
                                from SuratIjinLembur surat
                                join Users usr on usr.Id = surat.MhsId
                                join AkademikLaboratorium lab on surat.LabId = lab.Id
                                ";

            $SuratIjinLembur = $this->db->query($queryIjinLembur)->result_array();

            $data["SuratIjinLembur"] = $SuratIjinLembur;
        }

        $User = $this->db->query($query)->row_array();
        $data['User'] = $User;

        /* Ambil Data Lab */
        $queryLab = "
                    select * from AkademikLaboratorium
                    where Status = 1
                    ";

        $Lab = $this->db->query($queryLab)->result_array();
        $data["Lab"] = $Lab;

        /* Ambil List Dosen */
        $queyrDosen = "select *
                        from Users
                        where RoleId in ('1')
                        ";

        $Dosen = $this->db->query($queyrDosen)->result_array();
        $data['Dosen'] = $Dosen;

        /* Setting kembali */
        $data['url_kembali'] = get_referer_url();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('permohonanSurat/IjinLembur', $data);
        $this->load->view('templates/footer');
    }

    public function PengajuanSuratIjinLembur(){
        $datetime = new DateTime();
        $timezone = new DateTimeZone('Asia/Jakarta'); // Waktu Indonesia Barat (WIB)
        $datetime->setTimezone($timezone);

        $MhsId = htmlspecialchars($this->input->post('mhsId'));


        $requestIjinLembur = [
            'MhsId' => $MhsId,
            'LabId' => htmlspecialchars($this->input->post('lab')),
            'Tujuan' => htmlspecialchars($this->input->post('tujuan')),
            'TujuanLainnya' => "-",
            'DosenPembimbingId' => htmlspecialchars($this->input->post('dosen')),
            'JudulPenelitian' => htmlspecialchars($this->input->post('judul')),
            'Tanggal' => htmlspecialchars($this->input->post('tanggal')),
            'JamMulai' => htmlspecialchars($this->input->post('jamMulai')),
            'JamSelesai' => htmlspecialchars($this->input->post('jamSelesai')),
            'CreatedAt' => $datetime->format("Y-m-d H:i:s")
        ];

        if ($this->input->post('tujuan') == 8){
            $requestIjinKerjaLab['TujuanLainnya'] = htmlspecialchars($this->input->post('tujuanLainnya'));
        }

        $this->db->insert('SuratIjinLembur', $requestIjinLembur);

        redirect('surat/SuratIjinLembur');
    }

    public function DownloadSuratIjinLembur($SuratId = null){
        require_once 'vendor/autoload.php';
        $phpWord = new \PhpOffice\PhpWord\PhpWord();

        $datetime = new DateTime();
        $timezone = new DateTimeZone('Asia/Jakarta');
        $datetime->setTimezone($timezone);
        $formattedDate = $datetime->format('d F Y');

        /* Ambil Data Dari DB */
        /* Get Data Surat */
        $querySurat = "
                            select 
                            usr.Name NamaMahasiswa, usr.Username NIM, usr.AlamatMalang, usr.Telpon, 
                            surat.Tujuan, surat.TujuanLainnya, surat.JamMulai, surat.JamSelesai, surat.JudulPenelitian, surat.Tanggal,
                            lab.Nama NamaLab,
                            dsn.Name NamaDosenPembimbing,
                            kalab.Name NamaKalab,
                            plp.Name NamaPLP
                            FROM
                                SuratIjinLembur surat
                            JOIN Users usr ON surat.MhsId = usr.Id
                            JOIN Users dsn ON surat.DosenPembimbingId = dsn.Id
                            JOIN AkademikLaboratorium lab ON surat.LabId = lab.Id
                            JOIN Users kalab ON lab.KalabId = kalab.Id
                            JOIN Users plp on lab.PLPId = plp.Id
                            where surat.Id = $SuratId
                        ";

        $DataSurat = $this->db->query($querySurat)->row_array();

        switch ($DataSurat['Tujuan']){
            case 1:
                $Tujuan = "Penelitian Dosen";
                break;
            case 2:
                $Tujuan = "Proyek Kimia";
                break;
            case 3:
                $Tujuan = "Program Kreatifitas Mahasiswa";
                break;
            case 4:
                $Tujuan = "Penelitian Skripsi";
                break;
            case 5:
                $Tujuan = "Penelitian Tesis";
                break;
            case 6:
                $Tujuan = "Penelitian Disertasi";
                break;
            case 7:
                $Tujuan = "Analisis Sampel";
                break;
            case 8:
                $Tujuan = $DataSurat['TujuanLainnya'];
                break;
        }

        // Buat objek DateTime dari string tanggal
        $dateLembur = DateTime::createFromFormat('Y-m-d', $DataSurat['Tanggal']);

        // Ubah format tanggal
        $formattedDateLembur = $dateLembur->format('d-m-Y');

        $DataSurat['TanggalLembur'] = $formattedDateLembur;

        /* Ambil Data Kadep */
        $queryKadep = "
                    select jajaran.KadepId, kadep.Name as Kadep, kadep.Username as NIP
                    from AkademikJajaran jajaran
                    join Users kadep on jajaran.KadepId = kadep.Id
                    ";

        $Kadep = $this->db->query($queryKadep)->row_array();
        
        /* Ambil Template */
        $section = $phpWord->addSection();
        $document = new \PhpOffice\PhpWord\TemplateProcessor('assets/templates/word/suratIjinLembur.docx');
        
        /* Set Value */
        $document->setValue('namaMahasiswa', $DataSurat['NamaMahasiswa']);
        $document->setValue('NIM', $DataSurat['NIM']);
        $document->setValue('alamatMalang', $DataSurat['AlamatMalang']);
        $document->setValue('telpon', $DataSurat['Telpon']);
        $document->setValue('tujuan', $Tujuan);
        $document->setValue('lab', $DataSurat['NamaLab']);
        $document->setValue('tanggal', $DataSurat['TanggalLembur']);
        $document->setValue('jamMulai', $DataSurat['JamMulai']);
        $document->setValue('jamSelesai', $DataSurat['JamSelesai']);
        $document->setValue('judul', $DataSurat['JudulPenelitian']);
        $document->setValue('namaKalab', $DataSurat['NamaKalab']);
        $document->setValue('namaPLP', $DataSurat['NamaPLP']);
        $document->setValue('namaDosenPembimbing', $DataSurat['NamaDosenPembimbing']);

        $document->setValue('tgl-bln-thn', $formattedDate);
        $document->setValue('namaKadep', $Kadep['Kadep']);

        /* Change Filename to userId-FormulirTA.pdf */
        $filename = $SuratId . '-SuratIjinLembur.docx';
        $directory = './assets/templates/temp/';
        $fullPath = $directory . $filename;
        $document->saveAs($fullPath);

        // Offer the document as a download link
        $this->load->helper('download');
        force_download($fullPath, NULL);

        sleep(5);

        /* Remove the file */
        unlink($fullPath);
    }

    public function UploadFileSuratLemburLab(){
        $uploadPath = './assets/uploads/suratLembur_lab/';
        $config['upload_path'] = $uploadPath;
        $config['allowed_types'] = 'pdf|doc|docx';

        $SuratId = $this->input->post('suratId');

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('suratLemburLab')) {
            $SuratIjinLemburLab = $this->db->get_where('SuratIjinLembur', ['Id' => $SuratId])->row_array();

            // File upload success
            $data = $this->upload->data();
            $uploaded_filename = $data['file_name'];
            $extention = pathinfo($uploaded_filename, PATHINFO_EXTENSION);
            $source_path = './assets/uploads/suratLembur_lab/' . $uploaded_filename;
            $new_filename = $SuratId.'-FileSuratLemburLab.'.$extention; // Change this to your desired new name
            $new_path = './assets/uploads/suratLembur_lab/' . $new_filename;

            if (rename($source_path, $new_path)) {
                if ($SuratIjinLemburLab['FileSuratIjinLembur'] != "" && $SuratIjinLemburLab['FileSuratIjinLembur'] != $new_filename && $SuratIjinLemburLab['FileSuratIjinLembur'] != null){
                    unlink('./assets/uploads/suratLembur_lab/'. $SuratIjinLemburLab['FileSuratIjinLembur']);
                }

                $datetime = new DateTime;
                $timezone = new DateTimeZone('Asia/Jakarta'); 
        
                $datetime->setTimezone($timezone);

                $requestUploadFileSuratLemburLab = [
                    'FileSuratIjinLembur' => $new_filename
                ];

                $this->db->update('SuratIjinLembur', $requestUploadFileSuratLemburLab, "Id = $SuratId");

                redirect('surat/SuratIjinLembur');
            }

            // $response = 'File uploaded successfully: ' . $data['file_name'];
        } else {
            // File upload failed
            $response = 'File upload failed: ' . $this->upload->display_errors();
        }
    }
}