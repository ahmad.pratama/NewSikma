<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class Akademik extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->library('form_validation');
        if (!$this->session->userdata('Id')){
            redirect('auth');
        }
    }

    public function index(){
        $data['title'] = 'Data Akademik';

        $Id = $this->session->userdata['Id'];
        $query = "select * from Users where Id = $Id";
        $User = $this->db->query($query)->row_array();
        $data['User'] = $User;
        
        /* Setting kembali */
        $data['url_kembali'] = get_referer_url();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('akademik/index', $data);
        $this->load->view('templates/footer');
    }

    /* Ruangan */
    public function Ruangan(){
        $data['title'] = 'Data Ruangan';

        $Id = $this->session->userdata['Id'];
        $query = "select * from Users where Id = $Id";
        $User = $this->db->query($query)->row_array();
        $data['User'] = $User;

        /* Ambil Data Ruangan */
        $queryRuangan = "
                        select * 
                        from AkademikRuangan
                        where IsDeleted = 0
                        ";

        $Ruangan = $this->db->query($queryRuangan)->result_array();
        $data['Ruangan'] = $Ruangan;
        
        /* Setting kembali */
        $data['url_kembali'] = get_referer_url();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('akademik/ruangan', $data);
        $this->load->view('templates/footer');
    }

    public function DeleteRuangan($Id = null){
        $requestDelete = [
            "IsDeleted" => 1,
        ];
        $this->db->update('AkademikRuangan', $requestDelete, "Id = $Id");

        redirect('akademik/Ruangan');
    }
    
    public function TambahRuangan(){
        $requestRuangan = [
            "Nama" => htmlspecialchars($this->input->post("nama")),
            "Kapasitas" => htmlspecialchars($this->input->post("kapasitas")),
            "Keterangan" => htmlspecialchars($this->input->post("keterangan")),
            "Kategori" => htmlspecialchars($this->input->post("kategori")),
            "Status" => 1,
            "IsDeleted" => 0,
        ];

        $this->db->insert('AkademikRuangan', $requestRuangan);

        redirect('akademik/ruangan');
    }

    public function DetailRuangan(){
        $RuanganId = $this->input->post('RuanganId');

        $query = "
                select *
                from AkademikRuangan
                where Id = $RuanganId
                ";
        
        $DetailRuangan = $this->db->query($query)->row_array();

        echo json_encode($DetailRuangan);
        
    }

    public function EditRuangan($Id = null){

        $Id = $this->input->post('RuanganId');
        $requestRuangan = [
            "Nama" => htmlspecialchars($this->input->post("nama")),
            "Kapasitas" => htmlspecialchars($this->input->post("kapasitas")),
            "Keterangan" => htmlspecialchars($this->input->post("keterangan")),
            "Kategori" => htmlspecialchars($this->input->post("kategori")),
            "Status" => htmlspecialchars($this->input->post("status"))
        ];

        $this->db->update("AkademikRuangan", $requestRuangan, ["id"=> $Id]);

        redirect('akademik/ruangan');
    }

    public function ImportRuangan(){
        if($_FILES["importexcel"]["name"] != ''){
            $allowed_extension = array('xls', 'xlsx', '.csv');
            $file_array = explode(".", $_FILES['importexcel']['name']);
            $file_extension = end($file_array);

            if(in_array($file_extension, $allowed_extension)){
                $file_name = time() . '.' . $file_extension; // rename file
                move_uploaded_file($_FILES['importexcel']['tmp_name'], $file_name); // save file in directory
                $file_type = \PhpOffice\PhpSpreadsheet\IOFactory::identify($file_name); 
                $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($file_type);
                $spreadsheet = $reader->load($file_name); // get data file
                unlink($file_name); // hapus file
                $data = $spreadsheet->getActiveSheet()->toArray(); // cek sheet
                $sheetcount=count($data); // menjumlah data sheet

                /* proses pengambilan data */
                for($i=1; $i<$sheetcount; $i++){
                    $NamaRuangan = $data[$i][0];

                    /* Cek Duplicate Data */
                    $queryCekData = "select * from AkademikRuangan where Nama = '$NamaRuangan' AND IsDeleted = 0";
                    $duplicateData = $this->db->query($queryCekData)->row_array();

                    if ($duplicateData == null){
                        $requestData = [
                            'Nama' => $data[$i][0],
                            'Keterangan' => $data[$i][1],
                            'Kapasitas' => $data[$i][2],
                            'Kategori' => $data[$i][3],
                            'Status' => 1,
                            'IsDeleted' => 0
                        ];

                        $this->db->insert('AkademikRuangan', $requestData);
                        
                    }
                }
                $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
                    Data Ruangan Berhasil Ditambahkan
                </div>');

                unlink($file_name);

                redirect('Akademik/Ruangan');
            }else{
                $message = '<div class="alert alert-danger">Only .xls or .xlsx file allowed</div>';
            }
        }else{
            $message = '<div class="alert alert-danger">Please Select File</div>';
        }
    }

    /* Semester */
    public function Semester(){
        $data['title'] = 'Data Semester';

        $Id = $this->session->userdata['Id'];
        $query = "select * from Users where Id = $Id";
        $User = $this->db->query($query)->row_array();
        $data['User'] = $User;

        /* Ambil Data Semester */
        $querySemester = "
                        select * 
                        from AkademikSemester
                        where IsDeleted = 0
                        order by Id desc
                        ";

        $Semester = $this->db->query($querySemester)->result_array();
        $data['Semester'] = $Semester;
        
        if ($this->input->post('Json')){
            echo json_encode($Semester);
            die();
        }
        
        /* Setting kembali */
        $data['url_kembali'] = get_referer_url();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('akademik/semester', $data);
        $this->load->view('templates/footer');
    }

    public function DeleteSemester($Id = null){
        $requestDelete = [
            "IsDeleted" => 1,
        ];
        $this->db->update('AkademikSemester', $requestDelete, "Id = $Id");

        redirect('akademik/Semester');
    }
    
    public function TambahSemester(){
        $requestSemester = [
            "Semester" => htmlspecialchars($this->input->post("semester")),
            "TahunAkademik" => htmlspecialchars($this->input->post("tahunAkademik")),
            "Status" => 1,
            "IsDeleted" => 0,
        ];

        $this->db->insert('AkademikSemester', $requestSemester);

        redirect('akademik/semester');
    }

    public function DetailSemester(){
        $SemesterId = $this->input->post('SemesterId');

        $query = "
                select *
                from AkademikSemester
                where Id = $SemesterId
                ";
        
        $DetailSemester = $this->db->query($query)->row_array();

        echo json_encode($DetailSemester);
        
    }

    public function EditSemester($Id = null){

        $Id = htmlspecialchars($this->input->post('SemesterId'));
        $requestSemester = [
            "Semester" => htmlspecialchars($this->input->post("semester")),
            "TahunAkademik" => htmlspecialchars($this->input->post("tahunAkademik")),
            "Status" => htmlspecialchars($this->input->post("status"))
        ];

        $this->db->update("AkademikSemester", $requestSemester, ["id"=> $Id]);

        redirect('akademik/semester');
    }

    /* Skripsi */
    public function DateSkripsi(){
        $data['title'] = 'Data Skripsi';

        $Id = $this->session->userdata['Id'];
        $query = "select * from Users where Id = $Id";
        $User = $this->db->query($query)->row_array();
        $data['User'] = $User;

        /* Ambil Data Skripsi */
        $querySkripsi = "
                        select date.*,
                        semester.Semester, semester.TahunAkademik
                        from AkademikSkripsi date
                        join AkademikSemester semester on semester.Id = date.SemesterId
                        where date.IsDeleted = 0
                        order by date.Id desc
                        ";

        $Skripsi = $this->db->query($querySkripsi)->result_array();
        $data['Skripsi'] = $Skripsi;
        
        /* Setting kembali */
        $data['url_kembali'] = get_referer_url();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('akademik/skripsi', $data);
        $this->load->view('templates/footer');
    }

    public function DeleteDateSkripsi($Id = null){
        $requestDelete = [
            "IsDeleted" => 1,
        ];
        $this->db->update('AkademikSkripsi', $requestDelete, "Id = $Id");

        redirect('akademik/DateSkripsi');
    }
    
    public function TambahDateSkripsi(){
        $requestDateSkripsi = [
            "SemesterId" => htmlspecialchars($this->input->post("semester")),
            "MaxDateSempro" => htmlspecialchars($this->input->post("tanggalUjianSempro")),
            "MaxDateSemju" => htmlspecialchars($this->input->post("tanggalUjianSemju")),
            "MaxDateSkripsi" => htmlspecialchars($this->input->post("tanggalUjianSkripsi")),
            "Status" => 1,
            "IsDeleted" => 0,
        ];

        $this->db->insert('AkademikSkripsi', $requestDateSkripsi);

        redirect('akademik/DateSkripsi');
    }

    public function DetailDateSkripsi(){
        $DateSkripsiId = $this->input->post('DateSkripsiId');

        $query = "
                select *
                from AkademikSkripsi
                where Id = $DateSkripsiId
                ";
        
        $DetailDateSkripsi = $this->db->query($query)->row_array();

        $querySemester = "select * from AkademikSemester where Status = 1";
        $DataSemester = $this->db->query($querySemester)->result_array();

        $response = array(
            'DetailDateSkripsi' => $DetailDateSkripsi,
            'Semesters' => $DataSemester
        );

        echo json_encode($response);
        
    }

    public function EditDateSkripsi($Id = null){

        $Id = htmlspecialchars($this->input->post('dateSkripsiId'));
        $requestDateSkripsi = [
            "SemesterId" => htmlspecialchars($this->input->post("semesterId")),
            "MaxDateSempro" => htmlspecialchars($this->input->post("tanggalUjianSempro")),
            "MaxDateSemju" => htmlspecialchars($this->input->post("tanggalUjianSemju")),
            "MaxDateSkripsi" => htmlspecialchars($this->input->post("tanggalUjianSkripsi")),
            "Status" => htmlspecialchars($this->input->post("status"))
        ];

        $this->db->update("AkademikSkripsi", $requestDateSkripsi, ["id"=> $Id]);

        redirect('akademik/DateSkripsi');
    }

    /* Instansi */
    public function Instansi(){
        $data['title'] = 'Data Instansi';

        $Id = $this->session->userdata['Id'];
        $query = "select * from Users where Id = $Id";
        $User = $this->db->query($query)->row_array();
        $data['User'] = $User;

        /* Ambil Data Instansi */
        $queryInstansi = "
                        select * 
                        from AkademikInstansi
                        where IsDeleted = 0
                        ";

        $Instansi = $this->db->query($queryInstansi)->result_array();
        $data['Instansi'] = $Instansi;
        
        /* Setting kembali */
        $data['url_kembali'] = get_referer_url();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('akademik/Instansi', $data);
        $this->load->view('templates/footer');
    }

    public function DeleteInstansi($Id = null){
        $requestDelete = [
            "IsDeleted" => 1,
        ];
        $this->db->update('AkademikInstansi', $requestDelete, "Id = $Id");

        redirect('akademik/Instansi');
    }
    
    public function TambahInstansi(){
        $requestInstansi = [
            "Nama" => htmlspecialchars($this->input->post("nama")),
            "Alamat" => htmlspecialchars($this->input->post("alamat")),
            "Email" => htmlspecialchars($this->input->post("email")),
            "Telpon" => htmlspecialchars($this->input->post("telpon")),
            "Pimpinan" => htmlspecialchars($this->input->post("pimpinan")),
            "Status" => 1,
            "IsDeleted" => 0,
        ];

        $this->db->insert('AkademikInstansi', $requestInstansi);

        redirect('akademik/Instansi');
    }

    public function DetailInstansi(){
        $InstansiId = $this->input->post('InstansiId');

        $query = "
                select *
                from AkademikInstansi
                where Id = $InstansiId
                ";
        
        $DetailInstansi = $this->db->query($query)->row_array();

        echo json_encode($DetailInstansi);
        
    }

    public function EditInstansi($Id = null){

        $Id = htmlspecialchars($this->input->post('instansiId'));
        $requestInstansi = [
            "Nama" => htmlspecialchars($this->input->post("nama")),
            "Alamat" => htmlspecialchars($this->input->post("alamat")),
            "Email" => htmlspecialchars($this->input->post("email")),
            "Telpon" => htmlspecialchars($this->input->post("telpon")),
            "Pimpinan" => htmlspecialchars($this->input->post("pimpinan")),
            "Status" => htmlspecialchars($this->input->post("status"))
        ];

        $this->db->update("AkademikInstansi", $requestInstansi, ["id"=> $Id]);

        redirect('akademik/Instansi');
    }

    public function ImportInstansi(){
        if($_FILES["importexcel"]["name"] != ''){
            $allowed_extension = array('xls', 'xlsx', '.csv');
            $file_array = explode(".", $_FILES['importexcel']['name']);
            $file_extension = end($file_array);

            if(in_array($file_extension, $allowed_extension)){
                $file_name = time() . '.' . $file_extension; // rename file
                move_uploaded_file($_FILES['importexcel']['tmp_name'], $file_name); // save file in directory
                $file_type = \PhpOffice\PhpSpreadsheet\IOFactory::identify($file_name); 
                $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($file_type);
                $spreadsheet = $reader->load($file_name); // get data file
                unlink($file_name); // hapus file
                $data = $spreadsheet->getActiveSheet()->toArray(); // cek sheet
                $sheetcount=count($data); // menjumlah data sheet

                /* proses pengambilan data */
                for($i=1; $i<$sheetcount; $i++){
                    $NamaInstansi = $data[$i][0];

                    /* Cek Duplicate Data */
                    $queryCekData = "select * from AkademikInstansi where Nama = '$NamaInstansi' AND IsDeleted = 0";
                    $duplicateData = $this->db->query($queryCekData)->row_array();

                    if ($duplicateData == null){
                        $requestData = [
                            'Nama' => $data[$i][0],
                            'Alamat' => $data[$i][1],
                            'Email' => $data[$i][2],
                            'Telpon' => $data[$i][3],
                            'Pimpinan' => $data[$i][4],
                            'Status' => 1,
                            'IsDeleted' => 0
                        ];

                        $this->db->insert('AkademikInstansi', $requestData);
                        
                    }
                }
                $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
                    Data Instansi Berhasil Ditambahkan
                </div>');

                unlink($file_name);

                redirect('Akademik/Instansi');
            }else{
                $message = '<div class="alert alert-danger">Only .xls or .xlsx file allowed</div>';
            }
        }else{
            $message = '<div class="alert alert-danger">Please Select File</div>';
        }
    }

    public function DataInstansiByName($Nama = null){
        if ($Nama == null){
            $Nama = $this->input->get('Nama');
        }

        $query = "
                select * from AkademikInstansi
                where Nama like '%$Nama%' or Id = $Nama AND Status = 1
                ";

        $Instansi = $this->db->query($query)->result_array();

        return $Instansi;
    }

    /* Laboratorium */
    
    public function Laboratorium(){
        $data['title'] = 'Data Laboratorium';

        $Id = $this->session->userdata['Id'];
        $query = "select * from Users where Id = $Id";
        $User = $this->db->query($query)->row_array();
        $data['User'] = $User;

        /* Ambil Data Laboratorium */
        $queryLaboratorium = "
                        select lab.*,
                        (CASE WHEN lab.KalabId IS NOT NULL
                        THEN (SELECT Dosen.Name
                                FROM Users Dosen
                                WHERE Id = lab.KalabId)
                        ELSE NULL END) AS Kalab,
                        (CASE WHEN lab.PLPId IS NOT NULL
                        THEN (SELECT PLP.Name
                                FROM Users PLP
                                WHERE Id = lab.PLPId)
                        ELSE NULL END) AS PLP
                        from AkademikLaboratorium lab
                        where IsDeleted = 0
                        ";

        $Laboratorium = $this->db->query($queryLaboratorium)->result_array();
        $data['Laboratorium'] = $Laboratorium;

        /* Ambil Data Dosen */
        $queryDosen = "
                    select * from Users
                    where RoleId = 1
                    ";
        $Dosen = $this->db->query($queryDosen)->result_array();
        $data["Dosen"] = $Dosen;

        /* Ambil Data PLP */
        $queryPLP = "
                select * from Users
                where RoleId = 6
                ";
        $PLP = $this->db->query($queryPLP)->result_array();
        $data["PLP"] = $PLP;
        
        /* Setting kembali */
        $data['url_kembali'] = get_referer_url();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('akademik/Laboratorium', $data);
        $this->load->view('templates/footer');
    }

    public function DeleteLaboratorium($Id = null){
        $requestDelete = [
            "IsDeleted" => 1,
        ];
        $this->db->update('AkademikLaboratorium', $requestDelete, "Id = $Id");

        redirect('akademik/Laboratorium');
    }
    
    public function TambahLaboratorium(){
        $requestLaboratorium = [
            "Nama" => htmlspecialchars($this->input->post("laboratorium")),
            "KalabId" => htmlspecialchars($this->input->post("dosen")),
            "PLPId" => htmlspecialchars($this->input->post("plp")),
            "Status" => 1,
            "IsDeleted" => 0,
        ];

        $this->db->insert('AkademikLaboratorium', $requestLaboratorium);

        redirect('akademik/Laboratorium');
    }

    public function DetailLaboratorium(){
        $LaboratoriumId = $this->input->post('LaboratoriumId');

        $query = "
                select *
                from AkademikLaboratorium
                where Id = $LaboratoriumId
                ";
        
        $DetailLaboratorium = $this->db->query($query)->row_array();

        echo json_encode($DetailLaboratorium);
        
    }

    public function EditLaboratorium($Id = null){

        $Id = htmlspecialchars($this->input->post('laboratoriumId'));
        $requestLaboratorium = [
            "Nama" => htmlspecialchars($this->input->post("laboratorium")),
            "KalabId" => htmlspecialchars($this->input->post("dosen")),
            "PLPId" => htmlspecialchars($this->input->post("plp")),
            "Status" => htmlspecialchars($this->input->post("status"))
        ];

        $this->db->update("AkademikLaboratorium", $requestLaboratorium, ["id"=> $Id]);

        redirect('akademik/Laboratorium');
    }

    /* Jajaran */
    
    public function Jajaran(){
        $data['title'] = 'Data Jajaran';

        $Id = $this->session->userdata['Id'];
        $query = "select * from Users where Id = $Id";
        $User = $this->db->query($query)->row_array();
        $data['User'] = $User;

        /* Ambil Data Jajaran */
        $queryJajaran = "
                        select jajaran.*,
                        kadep.Name NamaKadep,
                        sekdep.Name NamaSekdep,
                        kaprodiS1.Name NamaKaprodiS1,
                        kaprodiS2.Name NamaKaprodiS2,
                        kaprodiS3.Name NamaKaprodiS3
                        from AkademikJajaran jajaran
                        join Users kadep on jajaran.KadepId = kadep.Id
                        join Users sekdep on jajaran.SekdepId = sekdep.Id
                        join Users kaprodiS1 on jajaran.KaprodiS1Id = kaprodiS1.Id
                        join Users kaprodiS2 on jajaran.KaprodiS2Id = kaprodiS2.Id
                        join Users kaprodiS3 on jajaran.KaprodiS3Id = kaprodiS3.Id
                        ";

        $Jajaran = $this->db->query($queryJajaran)->result_array();
        $data['Jajaran'] = $Jajaran;

        /* Ambil Data Dosen */
        $queryDosen = "
                    select * from Users
                    where RoleId = 1
                    ";
        $Dosen = $this->db->query($queryDosen)->result_array();
        $data["Dosen"] = $Dosen;
        
        /* Setting kembali */
        $data['url_kembali'] = get_referer_url();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('akademik/Jajaran', $data);
        $this->load->view('templates/footer');
    }
    
    public function TambahJajaran(){
        $requestJajaran = [
            "KadepId" => htmlspecialchars($this->input->post("kadep")),
            "SekdepId" => htmlspecialchars($this->input->post("sekdep")),
            "KaprodiS1Id" => htmlspecialchars($this->input->post("kaprodiS1")),
            "KaprodiS2Id" => htmlspecialchars($this->input->post("kaprodiS2")),
            "KaprodiS3Id" => htmlspecialchars($this->input->post("kaprodiS3")),
        ];

        $this->db->insert('AkademikJajaran', $requestJajaran);

        redirect('akademik/Jajaran');
    }

    public function DetailJajaran(){
        $JajaranId = $this->input->post('JajaranId');

        $query = "
                select *
                from AkademikJajaran
                where Id = $JajaranId
                ";
        
        $DetailJajaran = $this->db->query($query)->row_array();

        echo json_encode($DetailJajaran);
        
    }

    public function EditJajaran($Id = null){
        $Id = htmlspecialchars($this->input->post('jajaranId'));
        $requestJajaran = [
            "KadepId" => htmlspecialchars($this->input->post("kadep")),
            "SekdepId" => htmlspecialchars($this->input->post("sekdep")),
            "KaprodiS1Id" => htmlspecialchars($this->input->post("kaprodiS1")),
            "KaprodiS2Id" => htmlspecialchars($this->input->post("kaprodiS2")),
            "KaprodiS3Id" => htmlspecialchars($this->input->post("kaprodiS3")),
        ];

        $this->db->update("AkademikJajaran", $requestJajaran, ["id"=> $Id]);

        redirect('akademik/Jajaran');
    }

}