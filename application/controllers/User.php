<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
    
    public function __construct(){
        parent::__construct();
        $this->load->library('form_validation');
        if (!$this->session->userdata('Id')){
            redirect('auth');
        }
    }

    public function index($id = null){
        $data['title'] = 'Profil Saya';
        // $username = $this->session->userdata['Username'];
        // $email = $this->session->userdata['Email'];
        if ($id == null){
            $id = $this->session->userdata['Id'];
        }

        $roleId = $this->session->userdata['RoleId'];

        $mhsRole = [2,3,4];
        $dsnRole = [1,6];
        if (in_array($roleId, $mhsRole)){
            $query = "
                        select mhs.*, 
                        usr.Id, usr.Name, usr.Username, usr.Telpon, usr.Email, usr.RoleId, usr.CreatedAt, usr.Status, usr.ImageProfile, usr.AlamatMalang, usr.Alamat
                        from Users usr
                        join UserMahasiswa mhs on usr.Id = mhs.UserId
                        where usr.Id = '$id'
                        ";
        }else if(in_array($roleId, $dsnRole)){
            $query = "
                        select dsn.*, 
                        usr.Id, usr.Name, usr.Username, usr.Telpon, usr.Email, usr.RoleId, usr.CreatedAt, usr.Status, usr.ImageProfile, usr.AlamatMalang, usr.Alamat
                        from Users usr
                        join UserTenagaKependidikan dsn on usr.Id = dsn.UserId
                        where usr.Id = '$id'
                        ";
        }else if($roleId == 5){
            $query = "
                    select stf.Jabatan, stf.Bagian, stf.Golongan, stf.Pangkat, stf.PendidikanTerakhir, stf.Laboratorium,
                    usr.Id, usr.Name, usr.Username, usr.Telpon, usr.Email, usr.RoleId, usr.CreatedAt, usr.Status, usr.ImageProfile, usr.AlamatMalang, usr.Alamat
                    from Users usr
                    join UserTenagaKependidikan stf on usr.Id = stf.UserId
                    where usr.Id = '$id'
                    ";
        }else{
            $query = "select * from Users where Id = '$id'";
        }
        $User = $this->db->query($query)->row_array();
        $data['User'] = $User;

        /* Ambil Data Laboratorium */
        $queryLab = "
                    select * from AkademikLaboratorium
                    where Status = 1 AND IsDeleted = 0
                    ";
        $Lab = $this->db->query($queryLab)->result_array();

        $data['Lab'] = $Lab;

        /* Setting kembali */
        $data['url_kembali'] = get_referer_url();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('user/index', $data);
        $this->load->view('templates/footer');
    }

    public function UploadProfilePhoto(){
        $UserId = $this->input->post('userId');
        $upload_image = $_FILES['image']['name'];

        if($upload_image){
            $config['upload_path']          = './assets/img/profiles/';
            $config['allowed_types']        = 'jpg|png|jpeg';
            $config['max_size']             = 5000;

            $this->load->library('upload', $config);

            if($this->upload->do_upload('image')){
                $User = $this->db->get_where('Users', ['Id' => $UserId])->row_array();

                /* Upload New Image */
                $data = $this->upload->data();
                $uploaded_filename = $data['file_name'];
                $extention = pathinfo($uploaded_filename, PATHINFO_EXTENSION);
                $source_path = './assets/img/profiles/' . $uploaded_filename;
                $new_filename = $UserId.'-Profile.'.$extention; // Change this to your desired new name
                $new_path = './assets/img/profiles/' . $new_filename;

                if (rename($source_path, $new_path)) {
                    if ($User['ImageProfile'] != "default.jpg" && $User['ImageProfile'] != $new_filename){
                        unlink(FCPATH . 'assets/img/profiles/'. $User['ImageProfile']);
                    }

                    $this->db->set('ImageProfile', $new_filename)->where('Id', $UserId)->update('Users');

                    $this->index($UserId);
                }
            }else{
                echo $this->upload->display_errors();
            }
        }
    }

    public function EditUser(){
        $userId = htmlspecialchars($this->input->post('id'));
        $roleId = htmlspecialchars($this->input->post('roleId'));
        $requestUser = [
            'Name' => htmlspecialchars($this->input->post('name')),
            'Telpon' => htmlspecialchars($this->input->post('telpon')),
            'Email' => htmlspecialchars($this->input->post('email')),
            'AlamatMalang' => htmlspecialchars($this->input->post('alamatMalang')),
            'Alamat' => htmlspecialchars($this->input->post('alamat')),
        ];

        if($this->input->post('password')){
            $requestUser['Password'] = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
        }
        $this->db->where('Id', $userId)->update('Users', $requestUser);

        if($roleId != 0){
            $mhsRole = [2,3,4];
            if(in_array($roleId, $mhsRole)){
                /* Update Mahasiswa */
                $requestMahasiswa = [
                    'TempatLahir' => htmlspecialchars($this->input->post('tempatLahir')),
                    'tanggalLahir' => htmlspecialchars($this->input->post('tanggalLahir')),
                ];
    
                $this->db->where('UserId', $userId)->update('UserMahasiswa', $requestMahasiswa);    
            }else if($roleId == 1){
                $query = "
                            select dsn.*, 
                            usr.Id, usr.Name, usr.Username, usr.Telpon, usr.Email, usr.RoleId, usr.CreatedAt, usr.Status, usr.ImageProfile, usr.AlamatMalang, usr.Alamat
                            from Users usr
                            join UserTenagaKependidikan dsn on usr.Id = dsn.UserId
                            where usr.Id = '$userId'
                            ";
    
                $User = $this->db->query($query)->row_array();
    
                /* Update Dosen */
                $requestDosen = [
                    'NIDN' => htmlspecialchars($this->input->post('nidn')),
                    'Laboratorium' => htmlspecialchars($this->input->post('laboratorium')),
                    'Bidang' => htmlspecialchars($this->input->post('bidang')),
                    'Golongan' => htmlspecialchars($this->input->post('golongan')),
                    'Pangkat' => htmlspecialchars($this->input->post('pangkat')),
                    'Jabatan' => htmlspecialchars($this->input->post('jabatan')),
                    'TugasTambahan' => htmlspecialchars($this->input->post('tugasTambahan')),
                    'PendidikanS1' => htmlspecialchars($this->input->post('pendidikanS1')),
                    'LulusS1' => htmlspecialchars($this->input->post('lulusS1')),
                    'PendidikanS2' => htmlspecialchars($this->input->post('pendidikanS2')),
                    'LulusS2' => htmlspecialchars($this->input->post('lulusS2')),
                    'PendidikanS3' => htmlspecialchars($this->input->post('pendidikanS3')),
                    'LulusS3' => htmlspecialchars($this->input->post('lulusS3')),
                ];
    
                $IjazahS1 = $_FILES['ijazahS1']['name'];
                if($IjazahS1){
                    $config['upload_path']          = './assets/img/ijazah/';
                    $config['allowed_types']        = 'jpg|png';
                    $config['max_size']             = 5000;
        
                    $this->load->library('upload', $config);
        
                    if($this->upload->do_upload('ijazahS1')){
                        /* unlink old Image */
                        unlink('./assets/img/ijazah/'.$User['IjazahS1']);
    
                        /* Upload New Image */
                        $data = $this->upload->data();
                        $uploaded_filename = $data['file_name'];
                        $extention = pathinfo($uploaded_filename, PATHINFO_EXTENSION);
                        $source_path = './assets/img/ijazah/' . $uploaded_filename;
                        $new_filename = $userId.'-IjazahS1.'.$extention; // Change this to your desired new name
                        $new_path = './assets/img/ijazah/' . $new_filename;
    
                        if (rename($source_path, $new_path)) {
                            $requestDosen['IjazahS1'] = $new_filename;
                        }
                    }
                }
    
                $IjazahS2 = $_FILES['ijazahS2']['name'];
                if($IjazahS2){
                    $config['upload_path']          = './assets/img/ijazah/';
                    $config['allowed_types']        = 'jpg|png';
                    $config['max_size']             = 5000;
        
                    $this->load->library('upload', $config);
        
                    if($this->upload->do_upload('ijazahS2')){
                        /* unlink old Image */
                        unlink('./assets/img/ijazah/'.$User['IjazahS2']);
    
                        /* Upload New Image */
                        $data = $this->upload->data();
                        $uploaded_filename = $data['file_name'];
                        $extention = pathinfo($uploaded_filename, PATHINFO_EXTENSION);
                        $source_path = './assets/img/ijazah/' . $uploaded_filename;
                        $new_filename = $userId.'-IjazahS2.'.$extention; // Change this to your desired new name
                        $new_path = './assets/img/ijazah/' . $new_filename;
    
                        if (rename($source_path, $new_path)) {
                            $requestDosen['IjazahS2'] = $new_filename;
                        }
                    }
                }
    
                $IjazahS3 = $_FILES['ijazahS3']['name'];
                if($IjazahS3){
                    $config['upload_path']          = './assets/img/ijazah/';
                    $config['allowed_types']        = 'jpg|png';
                    $config['max_size']             = 5000;
        
                    $this->load->library('upload', $config);
        
                    if($this->upload->do_upload('ijazahS3')){
                        /* unlink old Image */
                        unlink('./assets/img/ijazah/'.$User['IjazahS3']);
    
                        /* Upload New Image */
                        $data = $this->upload->data();
                        $uploaded_filename = $data['file_name'];
                        $extention = pathinfo($uploaded_filename, PATHINFO_EXTENSION);
                        $source_path = './assets/img/ijazah/' . $uploaded_filename;
                        $new_filename = $userId.'-IjazahS3.'.$extention; // Change this to your desired new name
                        $new_path = './assets/img/ijazah/' . $new_filename;
    
                        if (rename($source_path, $new_path)) {
                            $requestDosen['IjazahS3'] = $new_filename;
                        }
                    }
                }
    
                $this->db->where('UserId', $userId)->update('UserTenagaKependidikan', $requestDosen);
            }else if($roleId == 5){
                /* Update Tendik */
                $requestTendik = [
                    'Jabatan' => htmlspecialchars($this->input->post('jabatan')),
                    'Bagian' => htmlspecialchars($this->input->post('bagian')),
                    'Golongan' => htmlspecialchars($this->input->post('golongan')),
                    'Pangkat' => htmlspecialchars($this->input->post('pangkat')),
                    'PendidikanTerakhir' => htmlspecialchars($this->input->post('pendidikanTerakhir')),
                ];
    
                $this->db->where('UserId', $userId)->update('UserTenagaKependidikan', $requestTendik);
            }
        }

        $this->index($userId);
    }
}

    
