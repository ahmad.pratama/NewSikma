<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class Agenda extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->library('form_validation');
        if (!$this->session->userdata('Id')){
            redirect('auth');
        }
    }

    public function Index(){
        $Id = $this->session->userdata['Id'];
        $query = "select * from Users where Id = $Id";
        $User = $this->db->query($query)->row_array();
        $data['User'] = $User;

        /* Setting kembali */
        $data['url_kembali'] = get_referer_url();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('agenda/kalender', $data);
        $this->load->view('templates/footer');
    }

    /* Agenda Ruangan */
    public function AgendaRuangan($date = null){
        $Id = $this->session->userdata['Id'];
        $query = "select * from Users where Id = $Id";
        $User = $this->db->query($query)->row_array();
        $data['User'] = $User;
        $data['Date'] = date("d F Y", strtotime($date));

        /* Ujian PKL */
        $queryAgendaPKL = "
                        select 
                        ujian.TanggalUjian TglPkl, ujian.JamMulai MulaiPkl, ujian.JamSelesai SelesaiPkl, ujian.RuangId RuangPKL,
                        mhs.Name NamaMhsUjianPkl
                        from UjianPKL ujian
                        join PraktekKerjaLapangan pkl on ujian.PklId = pkl.Id
                        join Users mhs on pkl.MhsId = mhs.Id
                        where ujian.TanggalUjian = '$date'
                        ";
        $AgendaPKL = $this->db->query($queryAgendaPKL)->result_array();

        $data['UjianPKL'] = $AgendaPKL;

        // var_dump($AgendaPKL);

        /* Seminar Proposal */
        $queryAgendaSempro = "
                            select 
                            sempro.TanggalUjian TglSempro, sempro.JamMulai MulaiSempro, sempro.JamSelesai SelesaiSempro, sempro.RuangId RuangSempro,
                            mhs.Name NamaMhsSempro
                            from SeminarProposalTA sempro
                            join TugasAkhirs ta on sempro.TaId = ta.Id
                            join Users mhs on ta.MhsId = mhs.Id
                            where sempro.TanggalUjian = '$date'
                            ";

        $AgendaSempro = $this->db->query($queryAgendaSempro)->result_array();
        
        $data['Sempro'] = $AgendaSempro;

        // var_dump($AgendaSempro);

        /* Seminar Kemajuan */
        $queryAgendaSemju = "
                            select
                            semju.TanggalUjian TglSemju, semju.JamMulai MulaiSemju, semju.JamSelesai SelesaiSemju, semju.RuangId RuangSemju,
                            mhs.Name NamaMhsSemju
                            from SeminarKemajuanTA semju
                            join TugasAkhirs ta on semju.TaId = ta.Id
                            join Users mhs on ta.MhsId = mhs.Id
                            where semju.TanggalUjian = '$date'
                            ";
        $AgendaSemju = $this->db->query($queryAgendaSemju)->result_array();

        $data['Semju'] = $AgendaSemju;

        // var_dump($AgendaSemju);

        /* Agenda Ujian Skripsi */
        $queryAgendaUjianSkripsi = "
                                    select
                                    skripsi.TanggalUjian TglSkripsi, skripsi.JamMulai MulaiSkripsi, skripsi.JamSelesai SelesaiSkripsi, skripsi.RuangId RuangSkripsi,
                                    mhs.Name NamaMhsSkripsi
                                    from SeminarSkripsiTA skripsi
                                    join TugasAkhirs ta on skripsi.TaId = ta.Id
                                    join Users mhs on ta.MhsId = mhs.Id
                                    where skripsi.TanggalUjian = '$date'
                                    ";

        $AgendaUjianSkripsi = $this->db->query($queryAgendaUjianSkripsi)->result_array();

        $data['Skripsi'] = $AgendaUjianSkripsi;

        /* Agenda Agenda Ruangan */
        $queryAgendaRuangan = "
                                    select
                                    kegiatan.NamaKegiatan, kegiatan.TanggalPelaksanaan TglKegiatan, kegiatan.JamMulai MulaiKegiatan, kegiatan.JamSelesai SelesaiKegiatan, kegiatan.RuangId RuangKegiatan,
                                    mhs.Name NamaMhsKegiatan
                                    from AgendaRuangan kegiatan
                                    join Users mhs on kegiatan.UserId = mhs.Id
                                    where kegiatan.TanggalPelaksanaan = '$date'
                                    ";

        $AgendaRuangan = $this->db->query($queryAgendaRuangan)->result_array();

        $data['AgendaRuangan'] = $AgendaRuangan;

        // var_dump($AgendaRuangan);

        $queryRuangan = "
                        select Id, Nama from AkademikRuangan where Status = 1 AND IsDeleted = 0
                        ";

        $Ruangan = $this->db->query($queryRuangan)->result_array();
        $data['Ruangan'] = $Ruangan;
        
        /* Setting kembali */
        $data['url_kembali'] = get_referer_url();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('agenda/kalenderRuangan', $data);
        $this->load->view('templates/footer');
    }

    public function FetchEvent($table, $formattedDate, $RuangId, $JamMulai, $JamSelesai) {
        if ($table == "AgendaRuangan"){
            $query = "
                        SELECT *
                        FROM $table
                        WHERE 
                        TanggalPelaksanaan = '$formattedDate' AND 
                        RuangId = '$RuangId' AND
                        JamMulai >= '$JamMulai' AND 
                        JamSelesai <= '$JamSelesai'
                    ";
        }else{
            $query = "
                        SELECT *
                        FROM $table
                        WHERE 
                        TanggalUjian = '$formattedDate' AND 
                        RuangId = '$RuangId' AND
                        JamMulai >= '$JamMulai' AND 
                        JamSelesai <= '$JamSelesai'
                    ";
        }

        return $this->db->query($query)->row_array();
    }

    public function TambahAgenda(){
        $date = new DateTime();
        $timezone = new DateTimeZone('Asia/Jakarta'); 

        $date->setTimezone($timezone);
        
        /* Ambil Request Data */
        $RuangId = $this->input->post('ruangId');
        $JamMulai = $this->input->post('jamMulai');
        $JamSelesai = $this->input->post('jamSelesai');
        
        $dateString = $this->input->post('date');
        $dateTime = DateTime::createFromFormat('d F Y', $dateString);
        $formattedDate = $dateTime->format('Y-m-d'); 

        $Userid = $this->session->userdata['Id'];

        /* Cek Data Ruangan */
        /* Ujian Skripsi */
        $Skripsi = $this->FetchEvent('SeminarSkripsiTA', $formattedDate, $RuangId, $JamMulai, $JamSelesai);

        /* Seminar Kemajuan */
        $Semju = $this->FetchEvent('SeminarKemajuanTA', $formattedDate, $RuangId, $JamMulai, $JamSelesai);

        /* Seminar Proposal */
        $Sempro = $this->FetchEvent('SeminarProposalTA', $formattedDate, $RuangId, $JamMulai, $JamSelesai);

        /* Ujian PKL */
        $UjianPKL = $this->FetchEvent('UjianPKL', $formattedDate, $RuangId, $JamMulai, $JamSelesai);

        /* Agenda Ruangan */
        $AgendaRuang = $this->FetchEvent('AgendaRuangan', $formattedDate, $RuangId, $JamMulai, $JamSelesai);

        if ($Skripsi == null && $Semju == null && $Sempro == null && $UjianPKL == null && $AgendaRuang == null){
            $request = [
                'UserId' => $Userid,
                'NamaKegiatan' => $this->input->post('namaKegiatan'),
                'TanggalPelaksanaan' => $formattedDate,
                'JamMulai' => $JamMulai,
                'JamSelesai' => $JamSelesai,
                'RuangId' => $RuangId,
                'CreatedAt' => $date->format("Y-m-d H:i:s")
            ];

            $this->db->insert('AgendaRuangan', $request);

            redirect('agenda/AgendaRuangan/'.$formattedDate);
        } else {
            echo '<script>alert("Ruangan pada waktu tersebut telah digunakan");</script>';

            echo '<script>window.location.href = "'.site_url('agenda/AgendaRuangan/'.$formattedDate).'";</script>';
        }

    }
}