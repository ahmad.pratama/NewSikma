<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->library('form_validation');
    }
    public function index(){
        $this->form_validation->set_rules('username', 'username', 'required|trim');
        $this->form_validation->set_rules('password', 'Password', 'required|trim');

        if ($this->form_validation->run() == false){
            $data['title'] = 'Login';
            $this->load->view('templates/auth_header', $data);
            $this->load->view('auth/login');
            $this->load->view('templates/auth_footer');
        }else{
            $this->_login();
        }
    }

    private function _login(){
        /* Get User Data */
        $username = $this->input->post('username');
        $password = $this->input->post('password');

        $query = "select * from Users where Username='$username' or Email='$username'";
        $UserData = $this->db->query($query)->row_array();

        $datetime = new DateTime;
        $timezone = new DateTimeZone('Asia/Jakarta'); 

        $datetime->setTimezone($timezone);

        /* Cek Data User By Username */
        if($UserData){
            /* Cek User Aktif */
            if($UserData['Status']==1){
                /* Cek Password sama */
                if(password_verify($password, $UserData['Password'])){
                    /* Generate Session data */
                    $SessionData = [
                        'Username' => $UserData['Username'],
                        'Email' => $UserData['Email'],
                        'RoleId' => $UserData['RoleId'],
                        'Id' => $UserData['Id'],
                        'Kadep' => false,
                        'Sekdep' => false,
                        'KaprodiS1' => false,
                        'KaprodiS2' => false,
                        'KaprodiS3' => false,
                        'Kalab' => false,
                    ];

                    $UserId = $UserData['Id'];

                    /* Cek Jabatan */
                    if ($UserData['RoleId'] == 1){
                        /* Ambil data Jajaran */
                        $queryJajaran = "
                                        select * 
                                        from AkademikJajaran 
                                        where KaprodiS1Id = $UserId OR KaprodiS2Id = $UserId OR KaprodiS3Id = $UserId OR KadepId = $UserId OR SekdepId = $UserId
                                        ";
                                    
                        $Jajaran = $this->db->query($queryJajaran)->row_array();

                        if ($Jajaran != null){
                            if($UserId == $Jajaran['KadepId']){
                                $SessionData['Kadep'] = true;
                            }elseif($UserId == $Jajaran['SekdepId']){
                                $SessionData['Sekdep'] = true;
                            }elseif($UserId == $Jajaran['KaprodiS1Id']){
                                $SessionData['KaprodiS1'] = true;
                            }elseif($UserId == $Jajaran['KaprodiS2Id']){
                                $SessionData['KaprodiS2'] = true;
                            }elseif($UserId == $Jajaran['KaprodiS3Id']){
                                $SessionData['KaprodiS3'] = true;
                            }
                        }

                        /* Ambil data Kalab */
                        $queryKalab = "
                                select * from AkademikLaboratorium 
                                where Status = 1 and IsDeleted = 0 and KalabId = $UserId
                                ";
                        $Kalab = $this->db->query($queryKalab)->row_array();
                        if($Kalab != null){
                            $SessionData['Kalab'] = true;
                        }

                        /* Cek Dosen Luar */
                        $queryDosenLuar = "
                                            select * from UserTenagaKependidikan
                                            where UserId=$UserId AND IsKimia = 0
                                            ";

                        $DosenLuar = $this->db->query($queryDosenLuar)->row_array();
                        
                        if ($DosenLuar != null && $DosenLuar['ActiveDate'] < $datetime->format("Y-m-d H:i:s")){
                            $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
                                Your Account is already expired!
                            </div>');

                            redirect('auth');
                        }
                    }

                    $this->session->set_userdata($SessionData);

                    /* Cek Mahasiswa Non Kimia */
                    if ($UserData['RoleId'] == 2 || $UserData['RoleId'] == 3 || $UserData['RoleId'] == 4){
                        $UserId = $UserData['Id'];

                        $query = "select * from UserMahasiswa where UserId='$UserId'";
                        $MahasiswaData = $this->db->query($query)->row_array();

                        if ($MahasiswaData['IsKimia'] == 0 && $MahasiswaData['ActiveDate'] < $datetime->format("Y-m-d H:i:s")){
                            $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
                                Your Account is already expired!
                            </div>');

                            redirect('auth');
                        }
                    }
                    if ($UserData['ChangedPassword'] == 0){
                        redirect('auth/ChangePassword');
                    }else{
                        redirect('user');
                    }
                }else{
                    $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
                        Wrong Password!
                    </div>');

                    redirect('auth');
                }
            }else{
                $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
                    User is not active!
                </div>');

                redirect('auth');
            }
        }else{
            $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
                Username is not registered!
            </div>');

            redirect('auth');
        }
    }

    public function logout() {
        /* Clear Session */
        $this->session->unset_userdata('Username');
        $this->session->unset_userdata('RoleId');
        $this->session->unset_userdata('Id');
        $this->session->unset_userdata('Email');

        $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
            You have been logout!
        </div>');

        redirect('auth');
    }

    public function blocked(){
        $this->load->view('auth/blocked');
    }

    public function ChangePassword(){
        $Id = $this->session->userdata['Id'];
        $query = "select * from Users where Id = $Id";
        $User = $this->db->query($query)->row_array();

        $this->form_validation->set_rules('password1', 'Password', 'required|min_length[6]|matches[password2]', [
            'matches' => 'password dont match!',
            'min_length' => 'password to short',
        ]);
        $this->form_validation->set_rules('password2', 'Password', 'required|matches[password1]');

        if ($this->form_validation->run() == false){
            $data['title'] = 'Change Password';
            $this->load->view('templates/auth_header', $data);
            $this->load->view('auth/password');
            $this->load->view('templates/auth_footer');
        }else{
            $UserId = $User['Id'];
            $RequestData = [
                'Password' => password_hash($this->input->post('password1'), PASSWORD_DEFAULT),
                'ChangedPassword' => 1,
            ];
            $this->db->update('Users', $RequestData, "id = $UserId");

            redirect('user');
        }
    }

    public function Registration(){
        $this->form_validation->set_rules('name', 'Name', 'required|trim');
        $this->form_validation->set_rules('username', 'Username', 'required|trim|is_unique[Users.Username]',[
            'is_unique' => 'This Username has already registered!'
        ]);
        // $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[Users.Email]', [
        //     'is_unique' => 'This email has already registered!'
        // ]);
        $this->form_validation->set_rules('role_id', 'RoleId', 'required|trim',[
            'required' => 'The Role field is required.'
        ]);
        $this->form_validation->set_rules('password1', 'Password', 'required|trim|min_length[6]|matches[password2]', [
            'matches' => 'password dont match!',
            'min_length' => 'password to short',
        ]);
        $this->form_validation->set_rules('password2', 'Password', 'required|trim|matches[password1]');
        $this->form_validation->set_rules('fakultas', 'Fakultas', 'required|trim');
        $this->form_validation->set_rules('programStudi', 'ProgramStudi', 'required|trim');
        $this->form_validation->set_rules('angkatan', 'Angkatan', 'required|trim|max_length[4]', [
            'max_length' => 'angkatan to long!',
        ]);

        $datetime = new DateTime();
        $timezone = new DateTimeZone('Asia/Jakarta'); 

        $datetime->setTimezone($timezone);

        if ($this->form_validation->run() == false){
            $data['title'] = 'Registration';
            $this->load->view('templates/auth_header', $data);
            $this->load->view('auth/regis');
            $this->load->view('templates/auth_footer');
        }else{
            $requestData = [
                'Name' => htmlspecialchars($this->input->post('name')),
                'Username' => htmlspecialchars($this->input->post('username')),
                'Email' => htmlspecialchars($this->input->post('email')),
                'Password' => password_hash($this->input->post('password1'), PASSWORD_DEFAULT),
                'RoleId' => htmlspecialchars($this->input->post('role_id')),
                'CreatedAt' => $datetime->format("Y-m-d H:i:s"),
                'ChangedPassword' => 0,
                'Status' => 1,
                'ImageProfile' => 'default.jpg'
            ];

            $this->db->insert('Users', $requestData);

            $User = $this->db->get_where('users', ['Username' => $this->input->post('username')])->row_array();

            $modifDate = new DateTime('now');
            $modifDate->modify('+6 month'); // or you can use '-90 day' for deduct
            $modifDate = $modifDate->format('Y-m-d h:i:s');
            
            $requestMahasiswaEksternal = [
                'UserId' => $User['Id'],
                'Angkatan' => $this->input->post('angkatan'),
                'Fakultas' => ucwords($this->input->post('fakultas')),
                'ProgramStudi' => ucwords($this->input->post('programStudi')),
                'IsKimia' => 0,
                'ActiveDate' => $modifDate,
            ];

            $this->db->insert('UserMahasiswa', $requestMahasiswaEksternal);

            $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
                Congratulation! your account has been created. Please Login
            </div>');

            redirect('auth');
        }
    }

    public function CreateAdmin(){
        $datetime = new DateTime();
        $timezone = new DateTimeZone('Asia/Jakarta'); 

        $datetime->setTimezone($timezone);
        
        $queryCekAdmin = "select * from Users where Username = '12345678'";
        $SuperAdmin = $this->db->query($queryCekAdmin)->row_array();

        if($SuperAdmin != NULL){
            $requestData = [
                'Password' => password_hash('Kimia_abc123', PASSWORD_DEFAULT),
            ];

            $this->db->update("Users", $requestData, "Username = '12345678'");
        }else{
            $requestData = [
                'Name' => 'Admin1',
                'Username' => 12345678,
                'Password' => password_hash('Kimia_abc123', PASSWORD_DEFAULT),
                'RoleId' => 0,
                'CreatedAt' => $datetime->format("Y-m-d H:i:s"),
                'ChangedPassword' => 0,
                'Status' => 1,
                'ImageProfile' => 'default.jpg'
            ];
    
            $this->db->insert('Users', $requestData);
        }
        redirect('auth');
    }
}
