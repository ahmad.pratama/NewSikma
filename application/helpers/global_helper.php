<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function CheckSession(){
    $lib = get_instance();
    if (!$lib->session->userdata('Username')){
        $lib->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
            You need login first!
        </div>');

        redirect('auth');
    }
}

if (!function_exists('get_referer_url')) {
    function get_referer_url() {
        return isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : site_url();
    }
}