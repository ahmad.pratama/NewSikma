

    <div class="container">

        <!-- Outer Row -->
        <div class="row justify-content-center">

            <div class="col-lg-7">

                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row">
                            <div class="col-lg">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-4">Login Page</h1>
                                    </div>

                                    <?= $this->session->flashdata('message'); ?>

                                    <form class="user" method="post" action="<?= base_url('auth'); ?>">
                                        <div class="form-group">
                                            <input type="text" class="form-control form-control-user"
                                                id="username" name="username"
                                                placeholder="Enter Username Address..." value="<?= set_value('username');?>">
                                                <?= form_error('username','<small class="text-danger pl-3">','</small>'); ?>
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control form-control-user"
                                                id="password" name="password" placeholder="Password">
                                                <?= form_error('password','<small class="text-danger pl-3">','</small>'); ?>
                                        </div>
                                        <button type="submit" class="btn btn-primary btn-user btn-block">
                                            Login
                                        </button>
                                    </form>
                                    <hr>
                                    <div class="text-center">
                                        <a class="small" data-toggle="modal" data-target="#contactAdmin">Contact Admin</a>
                                    </div>
                                    <div class="text-center">
                                        <a class="small" href="<?= base_url('auth/Registration');?>">Registration</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>


    <!-- Tambah Dosen Modal-->
    <div class="modal fade" id="contactAdmin"  role="dialog" aria-labelledby="exampleModalLabel"
        >
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Contact Admin</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="text-center">
                        <p>Harap Hubungi di Jam Kerja</p>
                        <p>Available at office Hours</p>
                    </div>
                    <div class="text-center">
                        <a type="button" class="btn btn-primary" href="https://wa.me/6285814786703">Admin 1</a>
                        <a type="button" class="btn btn-primary" href="https://wa.me/6285814786703">Admin 2</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
