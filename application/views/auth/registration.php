

    <div class="container">

        <div class="card o-hidden border-0 shadow-lg my-5 col-lg-7 mx-auto">
            <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                <div class="row">
                    <div class="col-lg">
                        <div class="p-5">
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4">Add New User</h1>
                            </div>
                            <form class="user" method="post" action="<?= base_url('user/CreateUser');?>">
                                <div class="form-group">
                                    <input type="text" class="form-control form-control-user" id="name" name="name"
                                        placeholder="Fullname" value="<?= set_value('name'); ?>">
                                    <?= form_error('name', '<small class="text-danger pl-3">','</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control form-control-user" id="username" name="username"
                                        placeholder="Username" value="<?= set_value('username'); ?>">
                                    <?= form_error('username', '<small class="text-danger pl-3">','</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <select name="role_id" id="role_id" class="form-control">
                                        <option value="">Select Role</option>
                                        <option value="1">Dosen</option>
                                        <option value="2">S1</option>
                                        <option value="3">S2</option>
                                        <option value="4">S3</option>
                                        <option value="5">Penanggung Jawab</option>
                                        <option value="6">Bagian Akademik</option>
                                        <option value="7">Admin</option>
                                        <option value="8">PLP</option>
                                        <option value="9">Bagian IT</option>
                                        <option value="10">Bagian Analisis</option>
                                    </select>
                                    <?= form_error('role_id', '<small class="text-danger pl-3">','</small>'); ?>
                                </div>
                                <button type="submit" class="btn btn-primary btn-user btn-block">
                                    Add User
                                </button>
                            </form>
                            <hr>
                            <div class="text-center">
                                <a class="small" href="forgot-password.html">Forgot Password?</a>
                            </div>
                            <div class="text-center">
                                <a class="small" href="<?= base_url('auth');?>">Already have an account? Login!</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

