

    <div class="container">

        <div class="card o-hidden border-0 shadow-lg my-5 col-lg-7 mx-auto">
            <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                <div class="row">
                    <div class="col-lg">
                        <div class="p-5">
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4">Registration</h1>
                            </div>
                            <div class="form-group text-center">
                                Registration hanya dilakukan untuk mahasiswa Non-Kimia Fakultas MIPA Universitas Brawijaya.
                                Bagi mahasiswa Kimia (S1, S2, S3) dapat melakukan login menggunakan NIM pada Username dan Password
                            </div>
                            <form class="user" method="post" action="<?= base_url('auth/Registration');?>">
                                <div class="form-group">
                                    <input type="text" class="form-control form-control-user" id="name" name="name"
                                        placeholder="Fullname" value="<?= set_value('name'); ?>">
                                    <?= form_error('name', '<small class="text-danger pl-3">','</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control form-control-user" id="username" name="username"
                                        placeholder="Username" value="<?= set_value('username'); ?>">
                                    <?= form_error('username', '<small class="text-danger pl-3">','</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control form-control-user" id="email" name="email"
                                        placeholder="Email" value="<?= set_value('email'); ?>">
                                    <?= form_error('email', '<small class="text-danger pl-3">','</small>'); ?>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <input type="password" class="form-control form-control-user"
                                            id="password1" name="password1" placeholder="Password">
                                            <?= form_error('password1', '<small class="text-danger pl-3">','</small>'); ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="password" class="form-control form-control-user"
                                            id="password2" name="password2" placeholder="Repeat Password">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <input type="text" class="form-control form-control-user" id="fakultas" name="fakultas"
                                            placeholder="Fakultas" value="<?= set_value('fakultas'); ?>">
                                        <?= form_error('fakultas', '<small class="text-danger pl-3">','</small>'); ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control form-control-user" id="programStudi" name="programStudi"
                                            placeholder="Program Studi" value="<?= set_value('programStudi'); ?>">
                                        <?= form_error('programStudi', '<small class="text-danger pl-3">','</small>'); ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <select name="role_id" id="role_id" class="form-control">
                                            <option value="2">S1</option>
                                            <option value="3">S2</option>
                                            <option value="4">S3</option>
                                        </select>
                                        <?= form_error('role_id', '<small class="text-danger pl-3">','</small>'); ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control form-control-user" id="angkatan" name="angkatan"
                                            placeholder="Angkatan" value="<?= set_value('angkatan'); ?>">
                                        <?= form_error('angkatan', '<small class="text-danger pl-3">','</small>'); ?>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary btn-user btn-block">
                                    Add User
                                </button>
                            </form>
                            <hr>
                            <div class="text-center">
                                <a class="small" href="<?= base_url('auth');?>">Already have an account? Login!</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

