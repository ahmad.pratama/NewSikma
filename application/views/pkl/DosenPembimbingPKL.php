<!-- Begin Page Content -->
<div class="container-fluid">

    <div class="card">
        <div class="row mt-3 ml-2 mr-2">
            <div class="col-4">
                <h4 class="text-white bg-dark">Nilai Dosen Pembimbing</h4>
            </div>
            <div class="col-5">
            </div>
            <div class="col-3">
            </div>
        </div>
        <div class="row g-0 pb-3 pl-2 pr-2">
        <div class="card-body">
            <div class="card">
                <div class="card-body">
                    <table class="table table-striped">
                    <thead class="thead-dark">
                        <tr>
                        <th scope="col" width="50px">No</th>
                        <th scope="col" width="500px">Lembar Penilaian</th>
                        <th scope="col" width="500px">Unduh</th>
                        <th scope="col" width="500px">Tampil</th>
                        <?php if($PKL['DosenId'] == $this->session->userdata['Id']): ?>
                        <th scope="col" width="500px">Input</th>
                        <?php endif; ?>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                        <th scope="row" >1</th>
                        <td class="PklId" hidden><?=$PKL['Id']?></td>
                        <td>
                            <p>
                                Laporan Akhir Program
                            </p>
                        </td>
                        <td>
                            <p>
                                <a href="https://kimia.ub.ac.id/wp-content/uploads/2023/10/LP_PKL_B_Pembimbing-Lapang.docx">Unduh disini</a>
                            </p>
                        </td>
                        <td>
                            <p>
                                <?php if($PKL['LP_PKL_A'] != null): ?>
                                    <button type="button" class="btn btn-warning viewNilaiDosenPembimbingLPA">Preview</button>
                                <?php endif; ?>
                            </p>
                        </td>
                        <?php 
                            $acc = [0,1,5];
                            $admin = [0,5];
                            if (in_array($User['RoleId'], $acc) && ($PKL['DosenId'] == $this->session->userdata['Id'] || in_array($User['RoleId'], $admin))):
                        ?>
                            <td>
                                <p>
                                    <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#inputNilaiDosenPembimbingLPA">Input Nilai</button>
                                </p>
                            </td>
                        <?php endif; ?>
                        </tr>
                        <?php 
                            $sks = [7,10,13,16];
                            if(in_array($PKL['JenisSks'], $sks)):
                        ?>
                        <tr>
                        <th scope="row" >2</th>
                        <td class="PklId" hidden><?=$PKL['Id']?></td>
                        <td>
                            <p>
                                Laporan di Tengah Program
                            </p>
                        </td>
                        <td>
                            <p>
                            <a href="https://kimia.ub.ac.id/wp-content/uploads/2023/10/LP_PKL_A_Pembimbing-Lapang.docx">Unduh disini</a>
                            </p>
                        </td>
                        <td>
                            <p>
                                <?php if($PKL['LP_PKL_B'] != null): ?>
                                    <button type="button" class="btn btn-warning viewNilaiDosenPembimbingLPB">Preview</button>
                                <?php endif; ?>
                            </p>
                        </td>
                        <?php 
                            $acc = [0,1,5];
                            $admin = [0,5];
                            if (in_array($User['RoleId'], $acc) && ($PKL['DosenId'] == $this->session->userdata['Id'] || in_array($User['RoleId'], $admin))):
                        ?>
                            <td>
                                <p>
                                    <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#inputNilaiDosenPembimbingLPB">Input Nilai</button>
                                </p>
                            </td>
                        <?php endif; ?>
                        </tr>
                        <tr>
                        <th scope="row" >3</th>
                        <td class="PklId" hidden><?=$PKL['Id']?></td>
                        <td>
                            <p>
                                <?= $PKL['MataKuliah1Text']?>
                            </p>
                        </td>
                        <td>
                            <p>
                                <?php if($PKL['MataKuliah1'] == 1): ?>
                                    <a href="https://kimia.ub.ac.id/wp-content/uploads/2023/10/LP_CPL_3.1_Pembimbing-Lapang.docx">Unduh disini</a>
                                <?php elseif($PKL['MataKuliah1'] == 2):?>
                                    <a href="https://kimia.ub.ac.id/wp-content/uploads/2023/10/LP_CPL_5.1_Pembimbing-Lapang.docx">Unduh disini</a>
                                <?php elseif($PKL['MataKuliah1'] == 3):?>
                                    <a href="https://kimia.ub.ac.id/wp-content/uploads/2023/10/LP_CPL_6.1_Pembimbing-Lapang.docx">Unduh disini</a>
                                <?php elseif($PKL['MataKuliah1'] == 4):?>
                                    <a href="https://kimia.ub.ac.id/wp-content/uploads/2023/10/LP_CPL_8.1_Pembimbing-Lapang.docx">Unduh disini</a>
                                <?php elseif($PKL['MataKuliah1'] == 5):?>
                                    <a href="https://kimia.ub.ac.id/wp-content/uploads/2023/10/LP_CPL_10.1_Pembimbing-Lapang.docx">Unduh disini</a>
                                <?php endif; ?>
                            </p>
                        </td>
                        <td>
                            <p>
                                <?php if($PKL['LP_MK1'] != null): ?>
                                    <button type="button" class="btn btn-warning viewNilaiDosenPembimbingMK1">Preview</button>
                                <?php endif; ?>
                            </p>
                        </td>
                        <?php 
                            $acc = [0,1,5];
                            $admin = [0,5];
                            if (in_array($User['RoleId'], $acc) && ($PKL['DosenId'] == $this->session->userdata['Id'] || in_array($User['RoleId'], $admin))):
                        ?>
                            <td>
                                <p>
                                    <?php if($PKL['MataKuliah1'] == 1): ?>
                                        <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#inputNilaiDosenPembimbingSumberDayaAlamMK1">Input Nilai</button>
                                    <?php elseif($PKL['MataKuliah1'] == 2):?>
                                        <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#inputNilaiDosenPembimbingKeselamatandanLingkunganMK1">Input Nilai</button>
                                    <?php elseif($PKL['MataKuliah1'] == 3):?>
                                        <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#inputNilaiDosenPembimbingMetodedanTerapanMK1">Input Nilai</button>
                                    <?php elseif($PKL['MataKuliah1'] == 4):?>
                                        <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#inputNilaiDosenPembimbingAnalisisPenilaiandanTindakanMK1">Input Nilai</button>
                                    <?php elseif($PKL['MataKuliah1'] == 5):?>
                                        <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#inputNilaiDosenPembimbingKemandirianPengayaanPengetahuanMK1">Input Nilai</button>
                                    <?php endif; ?>
                                </p>
                            </td>
                        <?php endif; ?>
                        </tr>

                        <?php if($PKL['JenisSks'] == "10" || $PKL['JenisSks'] == "13" || $PKL['JenisSks'] == "16"):?>
                        <tr>
                        <th scope="row" >4</th>
                        <td class="PklId" hidden><?=$PKL['Id']?></td>
                        <td>
                            <p>
                                <?= $PKL['MataKuliah2Text']?>
                            </p>
                        </td>
                        <td>
                            <p>
                                <?php if($PKL['MataKuliah2'] == 1): ?>
                                    <a href="https://kimia.ub.ac.id/wp-content/uploads/2023/10/LP_CPL_3.1_Pembimbing-Lapang.docx">Unduh disini</a>
                                <?php elseif($PKL['MataKuliah2'] == 2):?>
                                    <a href="https://kimia.ub.ac.id/wp-content/uploads/2023/10/LP_CPL_5.1_Pembimbing-Lapang.docx">Unduh disini</a>
                                <?php elseif($PKL['MataKuliah2'] == 3):?>
                                    <a href="https://kimia.ub.ac.id/wp-content/uploads/2023/10/LP_CPL_6.1_Pembimbing-Lapang.docx">Unduh disini</a>
                                <?php elseif($PKL['MataKuliah2'] == 4):?>
                                    <a href="https://kimia.ub.ac.id/wp-content/uploads/2023/10/LP_CPL_8.1_Pembimbing-Lapang.docx">Unduh disini</a>
                                <?php elseif($PKL['MataKuliah2'] == 5):?>
                                    <a href="https://kimia.ub.ac.id/wp-content/uploads/2023/10/LP_CPL_10.1_Pembimbing-Lapang.docx">Unduh disini</a>
                                <?php endif; ?>
                            </p>
                        </td>
                        <td>
                            <p>
                                <?php if($PKL['LP_MK2'] != null): ?>
                                    <button type="button" class="btn btn-warning viewNilaiDosenPembimbingMK2">Preview</button>
                                <?php endif; ?>
                            </p>
                        </td>
                        <?php 
                            $acc = [0,1,5];
                            $admin = [0,5];
                            if (in_array($User['RoleId'], $acc) && ($PKL['DosenId'] == $this->session->userdata['Id'] || in_array($User['RoleId'], $admin))):
                        ?>
                            <td>
                                <p>
                                    <?php if($PKL['MataKuliah2'] == 1): ?>
                                        <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#inputNilaiDosenPembimbingSumberDayaAlamMK2">Input Nilai</button>
                                    <?php elseif($PKL['MataKuliah2'] == 2):?>
                                        <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#inputNilaiDosenPembimbingKeselamatandanLingkunganMK2">Input Nilai</button>
                                    <?php elseif($PKL['MataKuliah2'] == 3):?>
                                        <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#inputNilaiDosenPembimbingMetodedanTerapanMK2">Input Nilai</button>
                                    <?php elseif($PKL['MataKuliah2'] == 4):?>
                                        <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#inputNilaiDosenPembimbingAnalisisPenilaiandanTindakanMK2">Input Nilai</button>
                                    <?php elseif($PKL['MataKuliah2'] == 5):?>
                                        <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#inputNilaiDosenPembimbingKemandirianPengayaanPengetahuanMK2">Input Nilai</button>
                                    <?php endif; ?>
                                </p>
                            </td>
                        <?php endif; ?>
                        </tr>
                        <?php endif; ?>

                        <?php if($PKL['JenisSks'] == "13" || $PKL['JenisSks'] == "16"):?>
                        <tr>
                        <th scope="row" >5</th>
                        <td class="PklId" hidden><?=$PKL['Id']?></td>
                        <td>
                            <p>
                                <?= $PKL['MataKuliah3Text']?>
                            </p>
                        </td>
                        <td>
                            <p>
                                <?php if($PKL['MataKuliah3'] == 1): ?>
                                    <a href="https://kimia.ub.ac.id/wp-content/uploads/2023/10/LP_CPL_3.1_Pembimbing-Lapang.docx">Unduh disini</a>
                                <?php elseif($PKL['MataKuliah3'] == 2):?>
                                    <a href="https://kimia.ub.ac.id/wp-content/uploads/2023/10/LP_CPL_5.1_Pembimbing-Lapang.docx">Unduh disini</a>
                                <?php elseif($PKL['MataKuliah3'] == 3):?>
                                    <a href="https://kimia.ub.ac.id/wp-content/uploads/2023/10/LP_CPL_6.1_Pembimbing-Lapang.docx">Unduh disini</a>
                                <?php elseif($PKL['MataKuliah3'] == 4):?>
                                    <a href="https://kimia.ub.ac.id/wp-content/uploads/2023/10/LP_CPL_8.1_Pembimbing-Lapang.docx">Unduh disini</a>
                                <?php elseif($PKL['MataKuliah3'] == 5):?>
                                    <a href="https://kimia.ub.ac.id/wp-content/uploads/2023/10/LP_CPL_10.1_Pembimbing-Lapang.docx">Unduh disini</a>
                                <?php endif; ?>
                            </p>
                        </td>
                        <td>
                            <p>
                                <?php if($PKL['LP_MK3'] != null): ?>
                                    <button type="button" class="btn btn-warning viewNilaiDosenPembimbingMK3">Preview</button>
                                <?php endif; ?>
                            </p>
                        </td>
                        <?php 
                            $acc = [0,1,5];
                            $admin = [0,5];
                            if (in_array($User['RoleId'], $acc) && ($PKL['DosenId'] == $this->session->userdata['Id'] || in_array($User['RoleId'], $admin))):
                        ?>
                            <td>
                                <p>
                                    <?php if($PKL['MataKuliah3'] == 1): ?>
                                        <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#inputNilaiDosenPembimbingSumberDayaAlamMK3">Input Nilai</button>
                                    <?php elseif($PKL['MataKuliah3'] == 2):?>
                                        <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#inputNilaiDosenPembimbingKeselamatandanLingkunganMK3">Input Nilai</button>
                                    <?php elseif($PKL['MataKuliah3'] == 3):?>
                                        <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#inputNilaiDosenPembimbingMetodedanTerapanMK3">Input Nilai</button>
                                    <?php elseif($PKL['MataKuliah3'] == 4):?>
                                        <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#inputNilaiDosenPembimbingAnalisisPenilaiandanTindakanMK3">Input Nilai</button>
                                    <?php elseif($PKL['MataKuliah3'] == 5):?>
                                        <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#inputNilaiDosenPembimbingKemandirianPengayaanPengetahuanMK3">Input Nilai</button>
                                    <?php endif; ?>
                                </p>
                            </td>
                            <?php endif; ?>
                        </tr>
                        <?php endif; ?>

                        <?php endif; ?>
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="inputNilaiDosenPembimbingLPA" role="dialog" aria-labelledby="exampleModalLabel" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Input Nilai Laporan Akhir Program</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="<?= base_url('pkl/NilaiDosenPembimbingLPA'); ?>" >
                <div class="form-group">
                    <h5>CPMK 12.1</h5>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">Memahami prinsip-prinsip etika profesional dalam bidang kimia</label>
                        <input type="text" class="form-control" id="inputName" name="PklId" value="<?=$PKL['Id']?>" hidden>
                        <div class="col-sm-4">
                            <select name="12.1_1">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">Menghormati dan mengakui kontribusi intelektual orang lain</label>
                        <div class="col-sm-4">
                            <select name="12.1_2" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">Pelaporan yang akurat</label>
                        <div class="col-sm-4">
                            <select name="12.1_3" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <hr>
                    <h5>CPMK 12.2</h5>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">Mengenal dan menerapkan standar-standar kimia</label>
                        <div class="col-sm-4">
                            <select name="12.2_1" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">penghormatan terhadap hak cipta, pelaporan data secara akurat, dan penghindaran plagiarisme</label>
                        <div class="col-sm-4">
                            <select name="12.2_2" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">mengetahui prinsip-prinsip pemantauan dampak lingkungan dari pekerjaan kimia serta cara mitigasi dampak negatif</label>
                        <div class="col-sm-4">
                            <select name="12.2_3" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">tahu bagaimana mengelola limbah kimia dengan benar, termasuk pemilahan, penyimpanan, dan pembuangan yang aman dan sesuai</label>
                        <div class="col-sm-4">
                            <select name="12.2_4" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <hr>
                    <h5>CPMK 12.3</h5>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">Bertindak secara etis dalam praktik kimia</label>
                        <div class="col-sm-4">
                            <select name="12.3_1" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">menerapkan prinsip-prinsip etika profesional dan standar-standar kimia dalam praktek</label>
                        <div class="col-sm-4">
                            <select name="12.3_2" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <hr>
                    <h5>CPMK 12.4</h5>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">Memiliki kesadaran terhadap tanggung jawab sosial dalam praktik kimia</label>
                        <div class="col-sm-4">
                            <select name="12.4_1" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">mempertimbangkan dampak lingkungan, kesehatan, dan keamanan dalam aktivitas kimia, serta memahami peran dan tanggung jawab mereka dalam mempromosikan keberlanjutan dan keadilan sosial</label>
                        <div class="col-sm-4">
                            <select name="12.4_2" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <h5>CPMK 12.5</h5>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">Berkomunikasi secara efektif tentang tanggung jawab sosial dan profesional</label>
                        <div class="col-sm-4">
                            <select name="12.5_1" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">dapat menyampaikan informasi tentang prinsip-prinsip etika dan standar-standar kimia kepada rekan kerja, masyarakat, dan pemangku kepentingan lainnya dengan bahasa yang mudah dipahami</label>
                        <div class="col-sm-4">
                            <select name="12.5_2" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <hr>
                    <h5>CPMK 15.1</h5>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">Memahami praktik dan kebutuhan industri/akademik terkait</label>
                        <div class="col-sm-4">
                            <select name="15.1_1" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">memiliki pemahaman yang kuat tentang tren, teknologi, peraturan, dan tantangan yang dihadapi dalam konteks profesional</label>
                        <div class="col-sm-4">
                            <select name="15.1_2" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <hr>
                    <h5>CPMK 15.2</h5>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">Mampu menerapkan pengetahuan dan keterampilan dalam situasi nyata</label>
                        <div class="col-sm-4">
                            <select name="15.2_1" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">mengidentifikasi masalah dan mengembangkan solusi yang sesuai dengan persyaratan industri atau akademik</label>
                        <div class="col-sm-4">
                            <select name="15.2_2" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <hr>
                    <h5>CPMK 15.3</h5>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">Memiliki keterampilan komunikasi yang efektif</label>
                        <div class="col-sm-4">
                            <select name="15.3_1" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">menyampaikan hasil penelitian atau konsep yang kompleks secara terstruktur dan mudah dipahami</label>
                        <div class="col-sm-4">
                            <select name="15.3_2" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <hr>
                    <h5>CPMK 15.4</h5>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">Memiliki kemampuan beradaptasi dan fleksibilitas</label>
                        <div class="col-sm-4">
                            <select name="15.4_1" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">berpikir kreatif, mengatasi hambatan, dan menyesuaikan diri dengan perubahan teknologi, kebijakan, atau tuntutan pekerjaan yang muncul</label>
                        <div class="col-sm-4">
                            <select name="15.4_2" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <hr>
                    <h5>CPMK 15.5</h5>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">Memiliki pemahaman tentang profesionalisme dan etika kerja</label>
                        <div class="col-sm-4">
                            <select name="15.5_1" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">menghormati hak dan kebutuhan orang lain, bersikap yang positif dan kolaboratif</label>
                        <div class="col-sm-4">
                            <select name="15.5_2" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="inputNilaiDosenPembimbingLPB" role="dialog" aria-labelledby="exampleModalLabel" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Input Nilai Laporan Tengah Program</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="<?= base_url('pkl/NilaiDosenPembimbingLPB'); ?>" >
                <div class="form-group">
                    <h5>CPMK 12.1</h5>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">Memahami prinsip-prinsip etika profesional dalam bidang kimia</label>
                        <input type="text" class="form-control" id="inputName" name="PklId" value="<?=$PKL['Id']?>" hidden>
                        <div class="col-sm-4">
                            <select name="12.1_1">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">Menghormati dan mengakui kontribusi intelektual orang lain</label>
                        <div class="col-sm-4">
                            <select name="12.1_2" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">Pelaporan yang akurat</label>
                        <div class="col-sm-4">
                            <select name="12.1_3" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <hr>
                    <h5>CPMK 12.2</h5>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">Mengenal dan menerapkan standar-standar kimia</label>
                        <div class="col-sm-4">
                            <select name="12.2_1" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">penghormatan terhadap hak cipta, pelaporan data secara akurat, dan penghindaran plagiarisme</label>
                        <div class="col-sm-4">
                            <select name="12.2_2" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">mengetahui prinsip-prinsip pemantauan dampak lingkungan dari pekerjaan kimia serta cara mitigasi dampak negatif</label>
                        <div class="col-sm-4">
                            <select name="12.2_3" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">tahu bagaimana mengelola limbah kimia dengan benar, termasuk pemilahan, penyimpanan, dan pembuangan yang aman dan sesuai</label>
                        <div class="col-sm-4">
                            <select name="12.2_3" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <hr>
                    <h5>CPMK 12.3</h5>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">Bertindak secara etis dalam praktik kimia</label>
                        <div class="col-sm-4">
                            <select name="12.3_1" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">menerapkan prinsip-prinsip etika profesional dan standar-standar kimia dalam praktek</label>
                        <div class="col-sm-4">
                            <select name="12.3_2" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <hr>
                    <h5>CPMK 12.4</h5>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">Memiliki kesadaran terhadap tanggung jawab sosial dalam praktik kimia</label>
                        <div class="col-sm-4">
                            <select name="12.4_1" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">mempertimbangkan dampak lingkungan, kesehatan, dan keamanan dalam aktivitas kimia, serta memahami peran dan tanggung jawab mereka dalam mempromosikan keberlanjutan dan keadilan sosial</label>
                        <div class="col-sm-4">
                            <select name="12.4_2" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <h5>CPMK 12.5</h5>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">Berkomunikasi secara efektif tentang tanggung jawab sosial dan profesional</label>
                        <div class="col-sm-4">
                            <select name="12.5_1" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">dapat menyampaikan informasi tentang prinsip-prinsip etika dan standar-standar kimia kepada rekan kerja, masyarakat, dan pemangku kepentingan lainnya dengan bahasa yang mudah dipahami</label>
                        <div class="col-sm-4">
                            <select name="12.5_2" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <hr>
                    <h5>CPMK 15.1</h5>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">Memahami praktik dan kebutuhan industri/akademik terkait</label>
                        <div class="col-sm-4">
                            <select name="15.1_1" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">memiliki pemahaman yang kuat tentang tren, teknologi, peraturan, dan tantangan yang dihadapi dalam konteks profesional</label>
                        <div class="col-sm-4">
                            <select name="15.1_2" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <hr>
                    <h5>CPMK 15.2</h5>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">Mampu menerapkan pengetahuan dan keterampilan dalam situasi nyata</label>
                        <div class="col-sm-4">
                            <select name="15.2_1" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">mengidentifikasi masalah dan mengembangkan solusi yang sesuai dengan persyaratan industri atau akademik</label>
                        <div class="col-sm-4">
                            <select name="15.2_2" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <hr>
                    <h5>CPMK 15.3</h5>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">Memiliki keterampilan komunikasi yang efektif</label>
                        <div class="col-sm-4">
                            <select name="15.3_1" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">menyampaikan hasil penelitian atau konsep yang kompleks secara terstruktur dan mudah dipahami</label>
                        <div class="col-sm-4">
                            <select name="15.3_2" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <hr>
                    <h5>CPMK 15.4</h5>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">Memiliki kemampuan beradaptasi dan fleksibilitas</label>
                        <div class="col-sm-4">
                            <select name="15.4_1" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">berpikir kreatif, mengatasi hambatan, dan menyesuaikan diri dengan perubahan teknologi, kebijakan, atau tuntutan pekerjaan yang muncul</label>
                        <div class="col-sm-4">
                            <select name="15.4_2" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <hr>
                    <h5>CPMK 15.5</h5>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">Memiliki pemahaman tentang profesionalisme dan etika kerja</label>
                        <div class="col-sm-4">
                            <select name="15.5_1" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">menghormati hak dan kebutuhan orang lain, bersikap yang positif dan kolaboratif</label>
                        <div class="col-sm-4">
                            <select name="15.5_2" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- Sumber Daya Alam -->
<div class="modal fade" id="inputNilaiDosenPembimbingSumberDayaAlamMK1" role="dialog" aria-labelledby="exampleModalLabel" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Input Nilai Mata Kuliah Sumber Daya ALam</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="sumberDayaAlamPembimbingLapang" method="post" action="<?= base_url('pkl/NilaiDosenPembimbingMk1');?>">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Memiliki pengetahuan tentang sumber daya alam Indonesia</label>
                            <input type="text" class="form-control" id="inputName" name="PklId" value="<?=$PKL['Id']?>" hidden>
                            <div class="col-sm-4">
                                <select name="1">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Mengenali proses ekstraksi dan pemurnian</label>
                            <div class="col-sm-4">
                                <select name="2" id="">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Mampu menerapkan konsep kimia dalam pemanfaatan sumber daya alam</label>
                            <div class="col-sm-4">
                                <select name="3" id="">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="inputNilaiDosenPembimbingSumberDayaAlamMK2" role="dialog" aria-labelledby="exampleModalLabel" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Input Nilai Mata Kuliah Sumber Daya ALam</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="sumberDayaAlamPembimbingLapang" method="post" action="<?= base_url('pkl/NilaiDosenPembimbingMk2');?>">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Memiliki pengetahuan tentang sumber daya alam Indonesia</label>
                            <input type="text" class="form-control" id="inputName" name="PklId" value="<?=$PKL['Id']?>" hidden>
                            <div class="col-sm-4">
                                <select name="1">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Mengenali proses ekstraksi dan pemurnian</label>
                            <div class="col-sm-4">
                                <select name="2" id="">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Mampu menerapkan konsep kimia dalam pemanfaatan sumber daya alam</label>
                            <div class="col-sm-4">
                                <select name="3" id="">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="inputNilaiDosenPembimbingSumberDayaAlamMK3" role="dialog" aria-labelledby="exampleModalLabel" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Input Nilai Mata Kuliah Sumber Daya ALam</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="sumberDayaAlamPembimbingLapang" method="post" action="<?= base_url('pkl/NilaiDosenPembimbingMk3');?>">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Memiliki pengetahuan tentang sumber daya alam Indonesia</label>
                            <input type="text" class="form-control" id="inputName" name="PklId" value="<?=$PKL['Id']?>" hidden>
                            <div class="col-sm-4">
                                <select name="1">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Mengenali proses ekstraksi dan pemurnian</label>
                            <div class="col-sm-4">
                                <select name="2" id="">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Mampu menerapkan konsep kimia dalam pemanfaatan sumber daya alam</label>
                            <div class="col-sm-4">
                                <select name="3" id="">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- Keselamatan dan Linkungan -->
<div class="modal fade" id="inputNilaiDosenPembimbingKeselamatandanLingkunganMK1" role="dialog" aria-labelledby="exampleModalLabel" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Input Nilai Keselamatan dan Lingkungan</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="<?= base_url('pkl/NilaiDosenPembimbingMK1');?>" id="keselamatandanLingkunganPembimbingLapang">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Memahami permasalahan keselamatan dan lingkungan yang relevan dengan bidang keahliannya</label>
                            <input type="text" class="form-control" id="inputName" name="PklId" value="<?=$PKL['Id']?>" hidden>
                            <div class="col-sm-4">
                                <select name="1">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Mengenali aspek hukum dan peraturan yang berlaku</label>
                            <div class="col-sm-4">
                                <select name="2" id="">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Mengetahui langkah-langkah mitigasi dan pengelolaan risiko</label>
                            <div class="col-sm-4">
                                <select name="3" id="">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="inputNilaiDosenPembimbingKeselamatandanLingkunganMK2" role="dialog" aria-labelledby="exampleModalLabel" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Input Nilai Keselamatan dan Lingkungan</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="<?= base_url('pkl/NilaiDosenPembimbingMK2');?>" id="keselamatandanLingkunganPembimbingLapang">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Memahami permasalahan keselamatan dan lingkungan yang relevan dengan bidang keahliannya</label>
                            <input type="text" class="form-control" id="inputName" name="PklId" value="<?=$PKL['Id']?>" hidden>
                            <div class="col-sm-4">
                                <select name="1">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Mengenali aspek hukum dan peraturan yang berlaku</label>
                            <div class="col-sm-4">
                                <select name="2" id="">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Mengetahui langkah-langkah mitigasi dan pengelolaan risiko</label>
                            <div class="col-sm-4">
                                <select name="3" id="">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="inputNilaiDosenPembimbingKeselamatandanLingkunganMK3" role="dialog" aria-labelledby="exampleModalLabel" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Input Nilai Keselamatan dan Lingkungan</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="<?= base_url('pkl/NilaiDosenPembimbingMK3');?>" id="keselamatandanLingkunganPembimbingLapang">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Memahami permasalahan keselamatan dan lingkungan yang relevan dengan bidang keahliannya</label>
                            <input type="text" class="form-control" id="inputName" name="PklId" value="<?=$PKL['Id']?>" hidden>
                            <div class="col-sm-4">
                                <select name="1">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Mengenali aspek hukum dan peraturan yang berlaku</label>
                            <div class="col-sm-4">
                                <select name="2" id="">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Mengetahui langkah-langkah mitigasi dan pengelolaan risiko</label>
                            <div class="col-sm-4">
                                <select name="3" id="">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- Metode dan Terapan -->
<div class="modal fade" id="inputNilaiDosenPembimbingMetodedanTerapanMK1" role="dialog" aria-labelledby="exampleModalLabel" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Input Nilai Metode dan Terapan</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="<?= base_url('pkl/NilaiDosenPembimbingMK1') ?>" id="metodedanTerapanPembimbingLapang">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Mahasiswa memahami metode penyelesaian masalah kimia</label>
                            <input type="text" class="form-control" id="inputName" name="PklId" value="<?=$PKL['Id']?>" hidden>
                            <div class="col-sm-4">
                                <select name="1">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Mampu mengidentifikasi permasalahan kimia.</label>
                            <div class="col-sm-4">
                                <select name="2" id="">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Menyusun rencana penyelesaian masalah.</label>
                            <div class="col-sm-4">
                                <select name="3" id="">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="inputNilaiDosenPembimbingMetodedanTerapanMK2" role="dialog" aria-labelledby="exampleModalLabel" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Input Nilai Metode dan Terapan</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="<?= base_url('pkl/NilaiDosenPembimbingMK2') ?>" id="metodedanTerapanPembimbingLapang">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Mahasiswa memahami metode penyelesaian masalah kimia</label>
                            <input type="text" class="form-control" id="inputName" name="PklId" value="<?=$PKL['Id']?>" hidden>
                            <div class="col-sm-4">
                                <select name="1">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Mampu mengidentifikasi permasalahan kimia.</label>
                            <div class="col-sm-4">
                                <select name="2" id="">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Menyusun rencana penyelesaian masalah.</label>
                            <div class="col-sm-4">
                                <select name="3" id="">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="inputNilaiDosenPembimbingMetodedanTerapanMK3" role="dialog" aria-labelledby="exampleModalLabel" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Input Nilai Metode dan Terapan</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="<?= base_url('pkl/NilaiDosenPembimbingMK3') ?>" id="metodedanTerapanPembimbingLapang">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Mahasiswa memahami metode penyelesaian masalah kimia</label>
                            <input type="text" class="form-control" id="inputName" name="PklId" value="<?=$PKL['Id']?>" hidden>
                            <div class="col-sm-4">
                                <select name="1">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Mampu mengidentifikasi permasalahan kimia.</label>
                            <div class="col-sm-4">
                                <select name="2" id="">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Menyusun rencana penyelesaian masalah.</label>
                            <div class="col-sm-4">
                                <select name="3" id="">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- Analisis Penilaian dan Tindakan -->
<div class="modal fade" id="inputNilaiDosenPembimbingAnalisisPenilaiandanTindakanMK1" role="dialog" aria-labelledby="exampleModalLabel" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Input Nilai Analisis, Penilaian, dan Tindakan</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="<?= base_url('pkl/NilaiDosenPembimbingMK1') ?>" id="analisisPenilaiandanTindakanPembimbingLapang">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Memahami prinsip-prinsip pengumpulan data yang valid</label>
                            <input type="text" class="form-control" id="inputName" name="PklId" value="<?=$PKL['Id']?>" hidden>
                            <div class="col-sm-4">
                                <select name="1">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Menganalisis dan menafsirkan data secara kritis</label>
                            <div class="col-sm-4">
                                <select name="2" id="">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Menggunakan kaidah ilmiah dalam penelitian</label>
                            <div class="col-sm-4">
                                <select name="3" id="">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="inputNilaiDosenPembimbingAnalisisPenilaiandanTindakanMK2" role="dialog" aria-labelledby="exampleModalLabel" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Input Nilai Analisis, Penilaian, dan Tindakan</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="<?= base_url('pkl/NilaiDosenPembimbingMK2') ?>" id="analisisPenilaiandanTindakanPembimbingLapang">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Memahami prinsip-prinsip pengumpulan data yang valid</label>
                            <input type="text" class="form-control" id="inputName" name="PklId" value="<?=$PKL['Id']?>" hidden>
                            <div class="col-sm-4">
                                <select name="1">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Menganalisis dan menafsirkan data secara kritis</label>
                            <div class="col-sm-4">
                                <select name="2" id="">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Menggunakan kaidah ilmiah dalam penelitian</label>
                            <div class="col-sm-4">
                                <select name="3" id="">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="inputNilaiDosenPembimbingAnalisisPenilaiandanTindakanMK3" role="dialog" aria-labelledby="exampleModalLabel" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Input Nilai Analisis, Penilaian, dan Tindakan</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="<?= base_url('pkl/NilaiDosenPembimbingMK3') ?>" id="analisisPenilaiandanTindakanPembimbingLapang">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Memahami prinsip-prinsip pengumpulan data yang valid</label>
                            <input type="text" class="form-control" id="inputName" name="PklId" value="<?=$PKL['Id']?>" hidden>
                            <div class="col-sm-4">
                                <select name="1">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Menganalisis dan menafsirkan data secara kritis</label>
                            <div class="col-sm-4">
                                <select name="2" id="">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Menggunakan kaidah ilmiah dalam penelitian</label>
                            <div class="col-sm-4">
                                <select name="3" id="">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- Kemandirian Pengayaan Pengetahuan -->
<div class="modal fade" id="inputNilaiDosenPembimbingKemandirianPengayaanPengetahuanMK1" role="dialog" aria-labelledby="exampleModalLabel" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Input Nilai Kemandirian Pengayaan Pengetahuan</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="<?= base_url('pkl/NilaiDosenPembimbingMK1') ?>" id="kemandirianPengayaanPengetahuanPembimbingLapang">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Mampu menyusun dan melaksanakan rencana penelitian atau proyek mandiri dengan menggunakan metode ilmiah yang tepat</label>
                            <input type="text" class="form-control" id="inputName" name="PklId" value="<?=$PKL['Id']?>" hidden>
                            <div class="col-sm-4">
                                <select name="1">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Mampu mengidentifikasi dan memanfaatkan berbagai sumber informasi untuk mendukung penelitian atau kajian ilmiah secara mandiri</label>
                            <div class="col-sm-4">
                                <select name="2" id="">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Mampu melakukan analisis kritis terhadap berbagai sumber literatur dan data yang relevan</label>
                            <div class="col-sm-4">
                                <select name="3" id="">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="inputNilaiDosenPembimbingKemandirianPengayaanPengetahuanMK2" role="dialog" aria-labelledby="exampleModalLabel" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Input Nilai Kemandirian Pengayaan Pengetahuan</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="<?= base_url('pkl/NilaiDosenPembimbingMK2') ?>" id="kemandirianPengayaanPengetahuanPembimbingLapang">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Mampu menyusun dan melaksanakan rencana penelitian atau proyek mandiri dengan menggunakan metode ilmiah yang tepat</label>
                            <input type="text" class="form-control" id="inputName" name="PklId" value="<?=$PKL['Id']?>" hidden>
                            <div class="col-sm-4">
                                <select name="1">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Mampu mengidentifikasi dan memanfaatkan berbagai sumber informasi untuk mendukung penelitian atau kajian ilmiah secara mandiri</label>
                            <div class="col-sm-4">
                                <select name="2" id="">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Mampu melakukan analisis kritis terhadap berbagai sumber literatur dan data yang relevan</label>
                            <div class="col-sm-4">
                                <select name="3" id="">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="inputNilaiDosenPembimbingKemandirianPengayaanPengetahuanMK3" role="dialog" aria-labelledby="exampleModalLabel" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Input Nilai Kemandirian Pengayaan Pengetahuan</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="<?= base_url('pkl/NilaiDosenPembimbingMK3') ?>" id="kemandirianPengayaanPengetahuanPembimbingLapang">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Mampu menyusun dan melaksanakan rencana penelitian atau proyek mandiri dengan menggunakan metode ilmiah yang tepat</label>
                            <input type="text" class="form-control" id="inputName" name="PklId" value="<?=$PKL['Id']?>" hidden>
                            <div class="col-sm-4">
                                <select name="1">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Mampu mengidentifikasi dan memanfaatkan berbagai sumber informasi untuk mendukung penelitian atau kajian ilmiah secara mandiri</label>
                            <div class="col-sm-4">
                                <select name="2" id="">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Mampu melakukan analisis kritis terhadap berbagai sumber literatur dan data yang relevan</label>
                            <div class="col-sm-4">
                                <select name="3" id="">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- View Nilai -->

<div class="modal fade" id="viewNilaiDosenPembimbingLPA" role="dialog" aria-labelledby="exampleModalLabel" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">View Nilai Laporan Akhir Program</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Judul</th>
                        <th scope="col">Nilai</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="table-active">
                        <th scope="row"></th>
                        <td>CPMK 12.1</td>
                        <td></td>
                        </tr>
                        <tr>
                        <th scope="row">1</th>
                        <td>Memahami prinsip-prinsip etika profesional dalam bidang kimia</td>
                        <td id="12_1_1"></td>
                        </tr>
                        <tr>
                        <th scope="row">2</th>
                        <td>Ketepatan dan kejujuran</td>
                        <td id="12_1_2"></td>
                        </tr>
                        <tr>
                        <th scope="row">3</th>
                        <td>Pelaporan yang akurat</td>
                        <td id="12_1_3"></td>
                        </tr>
                        <tr>
                        <th scope="row">4</th>
                        <td>Kerjasama dan kontribusi</td>
                        <td id="12_1_4"></td>
                        </tr>
                        <tr class="table-active">
                        <th scope="row"></th>
                        <td>CPMK 12.2</td>
                        <td></td>
                        </tr>
                        <tr>
                        <th scope="row">5</th>
                        <td>Mengenal dan menerapkan standar-standar kimia</td>
                        <td id="12_2_1"></td>
                        </tr>
                        <tr>
                        <th scope="row">6</th>
                        <td>memahami protokol keselamatan laboratorium yang melibatkan penggunaan alat pelindung diri (APD), penanganan bahan kimia berbahaya, serta tindakan darurat dalam kasus insiden.</td>
                        <td id="12_2_2"></td>
                        </tr>
                        <tr>
                        <th scope="row">7</th>
                        <td>memahami bagaimana mengelola, menyimpan, dan membuang bahan kimia berbahaya dengan aman dan sesuai dengan peraturan</td>
                        <td id="12_2_3"></td>
                        </tr>
                        <tr>
                        <th scope="row">8</th>
                        <td>mahir dalam penggunaan peralatan dan instrumen laboratorium yang umum digunakan dalam penelitian kimia</td>
                        <td id="12_2_4"></td>
                        </tr>
                        <tr class="table-active">
                        <th scope="row"></th>
                        <td>CPMK 12.3</td>
                        <td></td>
                        </tr>
                        <tr>
                        <th scope="row">9</th>
                        <td>Bertindak secara etis dalam praktik kimia</td>
                        <td id="12_3_1"></td>
                        </tr>
                        <tr>
                        <th scope="row">10</th>
                        <td>membuat keputusan berdasarkan pertimbangan etika, mematuhi peraturan dan regulasi yang berlaku, serta menjaga integritas dan kerahasiaan data</td>
                        <td id="12_3_2"></td>
                        </tr>
                        <tr class="table-active">
                        <th scope="row"></th>
                        <td>CPMK 12.5</td>
                        <td></td>
                        </tr>
                        <tr>
                        <th scope="row">11</th>
                        <td>Berkomunikasi secara efektif tentang tanggung jawab sosial dan profesional</td>
                        <td id="12_5_1"></td>
                        </tr>
                        <tr>
                        <th scope="row">12</th>
                        <td>dapat menyampaikan informasi tentang prinsip-prinsip etika dan standar-standar kimia kepada rekan kerja, masyarakat, dan pemangku kepentingan lainnya dengan bahasa yang mudah dipahami</td>
                        <td id="12_5_2"></td>
                        </tr>
                        <tr class="table-active">
                        <th scope="row"></th>
                        <td>CPMK 15.1</td>
                        <td></td>
                        </tr>
                        <tr>
                        <th scope="row">13</th>
                        <td>Memahami praktik dan kebutuhan industri/akademik terkait</td>
                        <td id="15_1_1"></td>
                        </tr>
                        <tr>
                        <th scope="row">14</th>
                        <td>Mematuhi dan melaksanakan peraturan dan tata tertib di lingkungan kerja industri atau akademik</td>
                        <td id="15_1_2"></td>
                        </tr>
                        </tr>
                        <tr class="table-active">
                        <th scope="row"></th>
                        <td>CPMK 15.2</td>
                        <td></td>
                        </tr>
                        <tr>
                        <th scope="row">15</th>
                        <td>Mampu menerapkan pengetahuan dan keterampilan dalam situasi nyata</td>
                        <td id="15_2_1"></td>
                        </tr>
                        <tr>
                        <th scope="row">16</th>
                        <td>menerapkan pengetahuan dan keterampilan yang mahasiswa peroleh dari perkuliahan dalam konteks kerja nyata</td>
                        <td id="15_2_2"></td>
                        </tr>
                        </tr>
                        <tr class="table-active">
                        <th scope="row"></th>
                        <td>CPMK 15.3</td>
                        <td></td>
                        </tr>
                        <tr>
                        <th scope="row">17</th>
                        <td>Memiliki keterampilan komunikasi yang efektif</td>
                        <td id="15_3_1"></td>
                        </tr>
                        <tr>
                        <th scope="row">18</th>
                        <td>berkomunikasi dengan rekan kerja, klien, atau sesama akademisi dengan jelas dan efektif</td>
                        <td id="15_3_2"></td>
                        </tr>
                        </tr>
                        <tr class="table-active">
                        <th scope="row"></th>
                        <td>CPMK 15.4</td>
                        <td></td>
                        </tr>
                        <tr>
                        <th scope="row">19</th>
                        <td>Memiliki kemampuan beradaptasi dan fleksibilitas</td>
                        <td id="15_4_1"></td>
                        </tr>
                        <tr>
                        <th scope="row">20</th>
                        <td>beradaptasi dengan perubahan dan tantangan dalam lingkungan kerja industri atau akademik</td>
                        <td id="15_4_2"></td>
                        </tr>
                        </tr>
                        <tr class="table-active">
                        <th scope="row"></th>
                        <td>CPMK 15.5</td>
                        <td></td>
                        </tr>
                        <tr>
                        <th scope="row">21</th>
                        <td>Memiliki pemahaman tentang profesionalisme dan etika kerja</td>
                        <td id="15_5_1"></td>
                        </tr>
                        <tr>
                        <th scope="row">22</th>
                        <td>bersikap profesional, memiliki integritas, menghormati etika kerja, dan bekerjasama dalam lingkungan kerja</td>
                        <td id="15_5_2"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="viewNilaiDosenPembimbingLPB" role="dialog" aria-labelledby="exampleModalLabel" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">View Nilai Laporan Tengah Program</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Judul</th>
                        <th scope="col">Nilai</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="table-active">
                        <th scope="row"></th>
                        <td>CPMK 12.1</td>
                        <td></td>
                        </tr>
                        <tr>
                        <th scope="row">1</th>
                        <td>Memahami prinsip-prinsip etika profesional dalam bidang kimia</td>
                        <td id="12_1_1"></td>
                        </tr>
                        <tr>
                        <th scope="row">2</th>
                        <td>Ketepatan dan kejujuran</td>
                        <td id="12_1_2"></td>
                        </tr>
                        <tr>
                        <th scope="row">3</th>
                        <td>Pelaporan yang akurat</td>
                        <td id="12_1_3"></td>
                        </tr>
                        <tr>
                        <th scope="row">4</th>
                        <td>Kerjasama dan kontribusi</td>
                        <td id="12_1_4"></td>
                        </tr>
                        <tr class="table-active">
                        <th scope="row"></th>
                        <td>CPMK 12.2</td>
                        <td></td>
                        </tr>
                        <tr>
                        <th scope="row">5</th>
                        <td>Mengenal dan menerapkan standar-standar kimia</td>
                        <td id="12_2_1"></td>
                        </tr>
                        <tr>
                        <th scope="row">6</th>
                        <td>memahami protokol keselamatan laboratorium yang melibatkan penggunaan alat pelindung diri (APD), penanganan bahan kimia berbahaya, serta tindakan darurat dalam kasus insiden.</td>
                        <td id="12_2_2"></td>
                        </tr>
                        <tr>
                        <th scope="row">7</th>
                        <td>memahami bagaimana mengelola, menyimpan, dan membuang bahan kimia berbahaya dengan aman dan sesuai dengan peraturan</td>
                        <td id="12_2_3"></td>
                        </tr>
                        <tr>
                        <th scope="row">8</th>
                        <td>mahir dalam penggunaan peralatan dan instrumen laboratorium yang umum digunakan dalam penelitian kimia</td>
                        <td id="12_2_4"></td>
                        </tr>
                        <tr class="table-active">
                        <th scope="row"></th>
                        <td>CPMK 12.3</td>
                        <td></td>
                        </tr>
                        <tr>
                        <th scope="row">9</th>
                        <td>Bertindak secara etis dalam praktik kimia</td>
                        <td id="12_3_1"></td>
                        </tr>
                        <tr>
                        <th scope="row">10</th>
                        <td>membuat keputusan berdasarkan pertimbangan etika, mematuhi peraturan dan regulasi yang berlaku, serta menjaga integritas dan kerahasiaan data</td>
                        <td id="12_3_2"></td>
                        </tr>
                        <tr class="table-active">
                        <th scope="row"></th>
                        <td>CPMK 12.5</td>
                        <td></td>
                        </tr>
                        <tr>
                        <th scope="row">11</th>
                        <td>Berkomunikasi secara efektif tentang tanggung jawab sosial dan profesional</td>
                        <td id="12_5_1"></td>
                        </tr>
                        <tr>
                        <th scope="row">12</th>
                        <td>dapat menyampaikan informasi tentang prinsip-prinsip etika dan standar-standar kimia kepada rekan kerja, masyarakat, dan pemangku kepentingan lainnya dengan bahasa yang mudah dipahami</td>
                        <td id="12_5_2"></td>
                        </tr>
                        <tr class="table-active">
                        <th scope="row"></th>
                        <td>CPMK 15.1</td>
                        <td></td>
                        </tr>
                        <tr>
                        <th scope="row">13</th>
                        <td>Memahami praktik dan kebutuhan industri/akademik terkait</td>
                        <td id="15_1_1"></td>
                        </tr>
                        <tr>
                        <th scope="row">14</th>
                        <td>Mematuhi dan melaksanakan peraturan dan tata tertib di lingkungan kerja industri atau akademik</td>
                        <td id="15_1_2"></td>
                        </tr>
                        </tr>
                        <tr class="table-active">
                        <th scope="row"></th>
                        <td>CPMK 15.2</td>
                        <td></td>
                        </tr>
                        <tr>
                        <th scope="row">15</th>
                        <td>Mampu menerapkan pengetahuan dan keterampilan dalam situasi nyata</td>
                        <td id="15_2_1"></td>
                        </tr>
                        <tr>
                        <th scope="row">16</th>
                        <td>menerapkan pengetahuan dan keterampilan yang mahasiswa peroleh dari perkuliahan dalam konteks kerja nyata</td>
                        <td id="15_2_2"></td>
                        </tr>
                        </tr>
                        <tr class="table-active">
                        <th scope="row"></th>
                        <td>CPMK 15.3</td>
                        <td></td>
                        </tr>
                        <tr>
                        <th scope="row">17</th>
                        <td>Memiliki keterampilan komunikasi yang efektif</td>
                        <td id="15_3_1"></td>
                        </tr>
                        <tr>
                        <th scope="row">18</th>
                        <td>berkomunikasi dengan rekan kerja, klien, atau sesama akademisi dengan jelas dan efektif</td>
                        <td id="15_3_2"></td>
                        </tr>
                        </tr>
                        <tr class="table-active">
                        <th scope="row"></th>
                        <td>CPMK 15.4</td>
                        <td></td>
                        </tr>
                        <tr>
                        <th scope="row">19</th>
                        <td>Memiliki kemampuan beradaptasi dan fleksibilitas</td>
                        <td id="15_4_1"></td>
                        </tr>
                        <tr>
                        <th scope="row">20</th>
                        <td>beradaptasi dengan perubahan dan tantangan dalam lingkungan kerja industri atau akademik</td>
                        <td id="15_4_2"></td>
                        </tr>
                        </tr>
                        <tr class="table-active">
                        <th scope="row"></th>
                        <td>CPMK 15.5</td>
                        <td></td>
                        </tr>
                        <tr>
                        <th scope="row">21</th>
                        <td>Memiliki pemahaman tentang profesionalisme dan etika kerja</td>
                        <td id="15_5_1"></td>
                        </tr>
                        <tr>
                        <th scope="row">22</th>
                        <td>bersikap profesional, memiliki integritas, menghormati etika kerja, dan bekerjasama dalam lingkungan kerja</td>
                        <td id="15_5_2"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="viewNilaiDosenPembimbingSumberDayaAlam" role="dialog" aria-labelledby="exampleModalLabel" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">View Nilai Sumber Daya Alam</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Judul</th>
                        <th scope="col">Nilai</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                        <th scope="row">1</th>
                        <td>Memiliki pengetahuan tentang sumber daya alam Indonesia</td>
                        <td id="1"></td>
                        </tr>
                        <tr>
                        <th scope="row">2</th>
                        <td>Mengenali proses ekstraksi dan pemurnian</td>
                        <td id="2"></td>
                        </tr>
                        <tr>
                        <th scope="row">3</th>
                        <td>Mampu menerapkan konsep kimia dalam pemanfaatan sumber daya alam</td>
                        <td id="3"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="viewNilaiDosenPembimbingKeselamatandanLingkungan" role="dialog" aria-labelledby="exampleModalLabel" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">View Nilai Keselamatan dan Lingkungan</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Judul</th>
                        <th scope="col">Nilai</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                        <th scope="row">1</th>
                        <td>Memahami permasalahan keselamatan dan lingkungan yang relevan dengan bidang keahliannya</td>
                        <td id="1"></td>
                        </tr>
                        <tr>
                        <th scope="row">2</th>
                        <td>Mengenali aspek hukum dan peraturan yang berlaku</td>
                        <td id="2"></td>
                        </tr>
                        <tr>
                        <th scope="row">3</th>
                        <td>Mengetahui langkah-langkah mitigasi dan pengelolaan risiko</td>
                        <td id="3"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="viewNilaiDosenPembimbingMetodedanTerapan" role="dialog" aria-labelledby="exampleModalLabel" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">View Nilai Metode dan Terapan</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Judul</th>
                        <th scope="col">Nilai</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                        <th scope="row">1</th>
                        <td>Memahami permasalahan keselamatan dan lingkungan yang relevan dengan bidang keahliannya</td>
                        <td id="1"></td>
                        </tr>
                        <tr>
                        <th scope="row">2</th>
                        <td>Mengenali aspek hukum dan peraturan yang berlaku</td>
                        <td id="2"></td>
                        </tr>
                        <tr>
                        <th scope="row">3</th>
                        <td>Mengetahui langkah-langkah mitigasi dan pengelolaan risiko</td>
                        <td id="3"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="viewNilaiDosenPembimbingAnalisisPenilaiandanTindakan" role="dialog" aria-labelledby="exampleModalLabel" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">View Nilai Analisis, Penilaian dan Tindakan</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Judul</th>
                        <th scope="col">Nilai</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                        <th scope="row">1</th>
                        <td>Memahami prinsip-prinsip pengumpulan data yang valid</td>
                        <td id="1"></td>
                        </tr>
                        <tr>
                        <th scope="row">2</th>
                        <td>Menganalisis dan menafsirkan data secara kritis</td>
                        <td id="2"></td>
                        </tr>
                        <tr>
                        <th scope="row">3</th>
                        <td>Menggunakan kaidah ilmiah dalam penelitian</td>
                        <td id="3"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="viewNilaiDosenPembimbingKemandirianPengayaanPengetahuan" role="dialog" aria-labelledby="exampleModalLabel" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">View Nilai Kemandirian Pengayaan Pengetahuan</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Judul</th>
                        <th scope="col">Nilai</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                        <th scope="row">1</th>
                        <td>Mampu menyusun dan melaksanakan rencana penelitian atau proyek mandiri dengan menggunakan metode ilmiah yang tepat</td>
                        <td id="1"></td>
                        </tr>
                        <tr>
                        <th scope="row">2</th>
                        <td>Mampu mengidentifikasi dan memanfaatkan berbagai sumber informasi untuk mendukung penelitian atau kajian ilmiah secara mandiri</td>
                        <td id="2"></td>
                        </tr>
                        <tr>
                        <th scope="row">3</th>
                        <td>Mampu melakukan analisis kritis terhadap berbagai sumber literatur dan data yang relevan</td>
                        <td id="3"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>

<!-- End of Main Content -->