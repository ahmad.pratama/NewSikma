<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Card Bootstrap -->
    <div class="card mb-3" style="max-width: 1500px;">
        <div class="col-md-6 mt-3 ml-2">
            <h3>Prakter Kerja Lapangan</h3>
        </div>
        <div class="row g-0 pt-4 pb-3 pl-2 pr-2">

        <!-- Pendaftaran PKL -->
        <div class="col-sm-3 pb-2">
            <a href="<?= base_url('pkl/PendaftaranPKL'); ?>">
                    <div class="card bg-gradient-light" style="height: 18rem;">
                        <div class="card-body mx-auto">
                            <i class="fa-solid fa-fw fa-file-circle-plus fa-8x"></i>
                            <p></p>
                            <h4 class="card-text text-gray-800">Pendaftaran PKL</h4>
                        </div> 
                        <div class="card-footer bg-gray-400">
                        <button class="btn btn-primary btn-user btn-block">
                            <b>
                                Detail
                                <i class="fa-regular fa-fw fa-circle-right fa-1x"></i>
                            </b>
                        </button>
                    </div>
                </div>
            </a>
        </div>

        <!-- Ujian PKL -->
        <?php 
        $mhs = [2,3,4];
        if (in_array($this->session->userdata('RoleId'), $mhs)):
            if($UjianPKL):
        ?>
            <div class="col-sm-3 pb-2">
                <a href="<?= base_url('pkl/PendaftaranUjianPKL'); ?>">
                    <div class="card bg-gradient-light" style="height: 18rem;">
                        <div class="card-body mx-auto">
                            <i class="fa-solid fa-fw fa-person-chalkboard fa-8x"></i>
                            <p></p>
                            
                            <h4 class="card-text text-gray-800">Pendaftaran Ujian PKL</h4>
                        </div> 
                        <div class="card-footer bg-gray-400">
                        <button class="btn btn-primary btn-user btn-block">
                            <b>
                                Detail
                                <i class="fa-regular fa-fw fa-circle-right fa-1x"></i>
                            </b>
                        </button>
                        </div>
                    </div>
                </a>
            </div>
        <?php endif; ?>
        <?php else : ?>
            <div class="col-sm-3 pb-2">
                <a href="<?= base_url('pkl/PendaftaranUjianPKL'); ?>">
                    <div class="card bg-gradient-light" style="height: 18rem;">
                        <div class="card-body mx-auto">
                            <i class="fa-solid fa-fw fa-person-chalkboard fa-8x"></i>
                            <p></p>
                            
                            <h4 class="card-text text-gray-800">Pendaftaran Ujian PKL</h4>
                        </div> 
                        <div class="card-footer bg-gray-400">
                        <button class="btn btn-primary btn-user btn-block">
                            <b>
                                Detail
                                <i class="fa-regular fa-fw fa-circle-right fa-1x"></i>
                            </b>
                        </button>
                        </div>
                    </div>
                </a>
            </div>
        <?php endif; ?>


        </div>
    </div>

    </div>
    <!-- /.container-fluid -->

</div>
<!-- End of Main Content -->