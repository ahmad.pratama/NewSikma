<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Filter -->
    <?php 
    $mhs = [2,3,4];
    if(!in_array($User['RoleId'], $mhs)):?>
        <div class="card">
            <div class="row mt-3 ml-2 mb-0">
                <div class="col">
                <form method="post" action="<?= base_url('#'); ?>" enctype="multipart/form-data">
                    <div class="form-row">
                        <div class="form-group col-md-2">
                            <div class="form-row">
                                <div class="form-group col">
                                <select id="jenjang" name="jenjang" class="form-control">
                                    <option value="" selected>Pilih Jenjang</option>
                                    <option value="2">S1</option>
                                    <option value="3">S2</option>
                                    <option value="4">S3</option>
                                </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-2">
                            <div class="form-row">
                                <div class="form-group col">
                                <select id="limit" name="limit" class="form-control">
                                    <option value="">Tampil Data</option>
                                    <option value="10">10</option>
                                    <option value="25">25</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-2">
                            <div class="form-row">
                                <div class="form-group col">
                                    <input type="text" class="form-control" id="search" name="search" placeholder="Nama / NIM">
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-2">
                            <button type="submit" class="btn btn-secondary">Search</button>
                        </div>
                    </div>
                </form>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <!-- Table PKL -->
    <div class="card">
        <div class="row mt-3 ml-2 mr-2">
            <div class="col-4">
                <h4 class="text-white bg-dark">Riwayat Ujian Praktek Kerja Lapangan</h4>
            </div>
            <div class="col-5">
            </div>
            <div class="col-3">
            </div>
        </div>
        <div class="row g-0 pb-3 pl-2 pr-2">
        <div class="card-body">
            <table class="table table-striped">
            <thead class="thead-dark">
                <tr>
                <th scope="col" width="50px">No</th>
                <th scope="col" width="500px">Mahasiswa</th>
                <th scope="col" width="500px">Dosen</th>
                <th scope="col" width="500px">Tanggal Ujian</th>
                <th scope="col" width="500px">Ruangan</th>
                <th scope="col" width="500px">Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $i = 1;
                    foreach($PendaftaranPKL as $pkl): 
                ?>
                <tr>
                <th scope="row" ><?=$i?></th>
                <td class="PklId" hidden><?=$pkl['Id']?></td>
                <td>
                    <p>
                        <?= $pkl['Name']?>
                    </p>
                </td>
                <td>
                    <p>
                        <?= $pkl['NamaDosen'] ?>
                    </p>
                </td>
                <td>
                    <p>
                        <?php echo (isset($pkl['TanggalUjian'])) ? $pkl['TanggalUjian']: '' ?>
                    </p>
                    <p>
                        <?php echo (isset($pkl['JamMulai'])) ? $pkl['JamMulai']: '' ?> - <?php echo (isset($pkl['JamSelesai'])) ? $pkl['JamSelesai']: '' ?>
                    </p>
                </td>
                <td>
                    <p>
                        <?php echo (isset($pkl['RuangNama'])) ? $pkl['RuangNama']: '' ?>
                    </p>
                </td>
                <td>
                    <div class="btn-group">
                        <button type="button" class="btn btn-warning btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Aksi
                            <i class="fa-solid fa-sliders"></i>
                        </button>
                        <div class="dropdown-menu">
                            <?php 
                            $acc = [0,2,3,4,5];
                            if(in_array($User['RoleId'], $acc)):
                                if(!$pkl["Uploaded"]) :
                            ?>
                                <div class="row mx-auto p-1">
                                    <button type="button" class="btn btn-success berkasUjianPKL">Berkas Ujian PKL</button>
                                </div>
                                <?php else :?>
                                    <?php if(!$pkl["Registered"]) :?>
                                    <div class="row mx-auto p-1">
                                        <button type="button" class="btn btn-success daftarUjianPKL">Daftar</button>
                                    </div>
                                    <?php else :?>
                                <div class="row mx-auto p-1">
                                    <button type="button" class="btn btn-warning editUjianPkl">Edit</button>
                                </div>
                                <div class="row mx-auto p-1">
                                    <button type="button" class="btn btn-success berkasUjianPKL">Berkas Ujian PKL</button>
                                </div>

                                <!-- Nilai Dosen Pembimbing -->
                                <div class="row mx-auto p-1">
                                    <a type="button" class="btn btn-warning" href="<?= base_url('pkl/NilaiDosenPembimbing/') . $pkl['Id']; ?>">Nilai Dosen Pembimbing</a>
                                </div>
                            <?php 
                                    endif; 
                                endif; 
                            endif; 
                            ?>
                            <?php if($User['RoleId'] == 1) :?>
                            <div class="row mx-auto p-1">
                                <button type="button" class="btn btn-success berkasUjianPKL">Berkas Ujian PKL</button>
                            </div>

                            <!-- Nilai Dosen Pembimbing -->
                            <div class="row mx-auto p-1">
                                <a type="button" class="btn btn-warning" href="<?= base_url('pkl/NilaiDosenPembimbing/') . $pkl['Id']; ?>">Nilai Dosen Pembimbing</a>
                            </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </td>
                </tr>
                <?php 
                    $i++;
                    endforeach; 
                ?>
            </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Daftar Ujian Pkl Modal-->
<div class="modal fade" id="daftarUjianPKL" role="dialog" aria-labelledby="exampleModalLabel"
    >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Daftar Ujian Praktek Kerja Lapangan</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <?php $minimalDate = date("Y-m-d"); ?>
                <form method="post" action="<?= base_url('pkl/DaftarUjianPKL'); ?>" >

                <div class="form-group row">
                    <label for="inputTanggalUjian" class="col-sm-4 col-form-label">Tanggal Ujian</label>
                    <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputName" name="PklId" hidden>
                    <input type="date" class="form-control" id="inputTanggalUjian" name="tanggalUjian" min="<?=  $minimalDate ?>" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputtimeSelect" class="col-sm-4 col-form-label">Jam Mulai</label>
                    <div class="col-sm-8">
                    <select id="JamMulai" name="jamMulai" style="width: 100%;" required></select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputtimeSelect" class="col-sm-4 col-form-label">Jam Selesai</label>
                    <div class="col-sm-8">
                    <select id="JamSelesai" name="jamSelesai" style="width: 100%;" required></select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputRuangan" class="col-sm-4 col-form-label">Ruangan</label>
                    <div class="col-sm-8">
                        <select id="RuanganDaftarUjianPKL" name="ruangId">
                            <option value="">Select an option</option>
                            <?php foreach ($Ruangan as $room): ?>
                                <option value="<?= $room['Id'] ?>"><?= $room['Nama'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputLinkZoom" class="col-sm-4 col-form-label">Link Zoom</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputLinkZoom" name="linkZoom">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Daftar</button>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- Detail Pkl Model -->
<div class="modal fade" id="detailUjianPkl" role="dialog" aria-labelledby="exampleModalLabel"
    >
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Detail Ujian Praktek Kerja Lapangan</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
            <div class="card">
                <div class="card-body">
                    <div class="form-group row">
                        <label for="inputMahasiswa" class="col-sm-4 col-form-label">Nama Mahasiswa</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputMahasiswa" name="TaId" hidden>
                        <input type="text" class="form-control" id="inputMahasiswa" name="namaMahasiswa" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputUsername" class="col-sm-4 col-form-label">Nomor Induk Mahasiswa</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputUsername" name="username" disabled>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group row">
                        <label for="inputNamaInstansi" class="col-sm-4 col-form-label">Nama Instansi</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputNamaInstansi" name="namaInstansi" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputAlamatInstansi" class="col-sm-4 col-form-label">Alamat Instansi</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputAlamatInstansi" name="alamatInstansi" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputEmailInstansi" class="col-sm-4 col-form-label">Email Instansi</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputEmailInstansi" name="emailInstansi" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputTelponInstansi" class="col-sm-4 col-form-label">Telpon Instansi</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputTelponInstansi" name="telponInstansi" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPembimbingLapang" class="col-sm-4 col-form-label">Pembimbing Lapang</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputPembimbingLapang" name="pembimbingLapang" disabled>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group row">
                        <label for="inputDosenPembimbing" class="col-sm-4 col-form-label">Dosen Pembimbing</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputDosenPembimbing" name="dosenPembimbing" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputTanggalPelaksanaan" class="col-sm-4 col-form-label">Tanggal Pelaksanaan</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputTanggalPelaksanaan" name="tanggalPelaksanaan" disabled>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>

<!-- Edit Ujian Pkl Model -->
<div class="modal fade" id="editUjianPkl" role="dialog" aria-labelledby="exampleModalLabel"
    >
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Ujian Praktek Kerja Lapangan</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
            <?php $minimalDate = date("Y-m-d"); ?>
            <form method="post" action="<?=base_url('pkl/editUjianPkl')?>" >
            <div class="card">
                <div class="card-body">
                    <div class="form-group row">
                        <label for="inputTanggalUjian" class="col-sm-4 col-form-label">Tanggal Ujian</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputName" name="PklId" hidden>
                        <input type="date" class="form-control" id="inputTanggalUjian" name="tanggalUjian" min="<?=  $minimalDate ?>" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputtimeSelect" class="col-sm-4 col-form-label">Jam Mulai</label>
                        <div class="col-sm-8">
                        <select id="JamMulai2" name="jamMulai" style="width: 100%;" required></select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputtimeSelect" class="col-sm-4 col-form-label">Jam Selesai</label>
                        <div class="col-sm-8">
                        <select id="JamSelesai2" name="jamSelesai" style="width: 100%;" required></select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputRuangan" class="col-sm-4 col-form-label">Ruangan</label>
                        <div class="col-sm-8">
                            <select id="RuanganEditUjianPKL" name="ruangId">
                                <option value="">Select an option</option>
                                <?php foreach ($Ruangan as $room): ?>
                                    <option value="<?= $room['Id'] ?>"><?= $room['Nama'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputLinkZoom" class="col-sm-4 col-form-label">Link Zoom</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputLinkZoom" name="linkZoom">
                        </div>
                    </div>
                </div>
            </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Edit</button>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- Berkas Pkl Model -->
<div class="modal fade" id="berkasUjianPKL" role="dialog" aria-labelledby="exampleModalLabel"
    >
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Berkas Pendaftaran Ta</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
            <table class="table table-striped">
                <thead class="thead-dark">
                    <tr>
                    <th scope="col">#</th>
                    <th scope="col">Berkas</th>
                    <th scope="col">Unggah</th>
                    <th scope="col">File</th>
                    <th scope="col">Tanggal</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                    <th scope="row">1</th>
                    <td>Daftar Hadir</td>
                    <form method="post" action="<?= base_url('pkl/UploadDaftarHadirUjianPKL'); ?>" enctype="multipart/form-data">
                        <td hidden><input type="text" class="form-control" id="PklId" name="PklId"></td>
                        <td>
                            <?php
                            $notacc = [1];
                            if (!in_array($User['RoleId'], $notacc)):?>
                            <label for="input-DaftarHadirUjianPKL">
                                <i class="fa-solid fa-fw fa-file-arrow-up fa-2x"></i> 
                            </label>
                            <input type="file" id="input-DaftarHadirUjianPKL" name="daftarHadirUjianPKL" style="display: none;" accept=".pdf">
                            <button class="btn btn-primary" id="submit-DaftarHadirUjianPKL" type="submit" style="display: none;" >Unggah</button>
                            <?php endif; ?>
                        </td>
                    </form>
                    <td>
                        <form action="">
                            <a id="linkDaftarHadir" href="#" target="_blank"><i id="linkIconDaftarHadir" class="fa-solid fa-fw fa-file-export fa-2x" style="display: none;"></i></a>
                        </form>
                    </td>
                    <td>
                        <label id="UploadDaftarHadir"></label>
                    </td>
                    </tr>
                    <tr>
                    <th scope="row">2</th>
                    <td>Kartu Bimbingan</td>
                    <form method="post" action="<?= base_url('pkl/UploadKartuBimbinganUjianPKL'); ?>" enctype="multipart/form-data">
                        <td hidden><input type="text" class="form-control" id="PklId" name="PklId"></td>
                        <td>
                            <?php
                            $notacc = [1];
                            if (!in_array($User['RoleId'], $notacc)):?>
                            <label for="input-KartuBimbinganUjianPKL">
                                <i class="fa-solid fa-fw fa-file-arrow-up fa-2x"></i> 
                            </label>
                            <input type="file" id="input-KartuBimbinganUjianPKL" name="kartuBimbinganUjianPKL" style="display: none;" accept=".pdf">
                            <button class="btn btn-primary" id="submit-KartuBimbinganUjianPKL" type="submit" style="display: none;" >Unggah</button>
                            <?php endif; ?>
                        </td>
                    </form>
                    <td>
                        <form action="">
                            <a id="linkKartuBimbingan" href="#" target="_blank"><i id="linkIconKartuBimbingan" class="fa-solid fa-fw fa-file-export fa-2x" style="display: none;"></i></a>
                        </form>
                    </td>
                    <td>
                        <label id="UploadKartuBimbingan"></label>
                    </td>
                    </tr>
                    <tr>
                    <th scope="row">3</th>
                    <td>Lembar Persetujuan</td>
                    <form method="post" action="<?= base_url('pkl/UploadLembarPersetujuanUjianPKL'); ?>" enctype="multipart/form-data">
                        <td hidden><input type="text" class="form-control" id="PklId" name="PklId"></td>
                        <td>
                            <?php
                            $notacc = [1];
                            if (!in_array($User['RoleId'], $notacc)):?>
                            <label for="input-LembarPersetujuanUjianPKL">
                                <i class="fa-solid fa-fw fa-file-arrow-up fa-2x"></i> 
                            </label>
                            <input type="file" id="input-LembarPersetujuanUjianPKL" name="lembarPersetujuanUjianPKL" style="display: none;" accept=".pdf">
                            <button class="btn btn-primary" id="submit-LembarPersetujuanUjianPKL" type="submit" style="display: none;" >Unggah</button>
                            <?php endif; ?>
                        </td>
                    </form>
                    <td>
                        <form action="">
                            <a id="linkLembarPersetujuan" href="#" target="_blank"><i id="linkIconLembarPersetujuan" class="fa-solid fa-fw fa-file-export fa-2x" style="display: none;"></i></a>
                        </form>
                    </td>
                    <td>
                        <label id="UploadLembarPersetujuan"></label>
                    </td>
                    </tr>
                    <tr>
                    <th scope="row">4</th>
                    <td>Penilaian Pembimbing</td>
                    <form method="post" action="<?= base_url('pkl/UploadPenilaianPembimbingUjianPKL'); ?>" enctype="multipart/form-data">
                        <td hidden><input type="text" class="form-control" id="PklId" name="PklId"></td>
                        <td>
                            <?php
                            $notacc = [1];
                            if (!in_array($User['RoleId'], $notacc)):?>
                            <label for="input-PenilaianPembimbingUjianPKL">
                                <i class="fa-solid fa-fw fa-file-arrow-up fa-2x"></i> 
                            </label>
                            <input type="file" id="input-PenilaianPembimbingUjianPKL" name="penilaianPembimbingUjianPKL" style="display: none;" accept=".pdf">
                            <button class="btn btn-primary" id="submit-PenilaianPembimbingUjianPKL" type="submit" style="display: none;" >Unggah</button>
                            <?php endif; ?>
                        </td>
                    </form>
                    <td>
                        <form action="">
                            <a id="linkPenilaianPembimbing" href="#" target="_blank"><i id="linkIconPenilaianPembimbing" class="fa-solid fa-fw fa-file-export fa-2x" style="display: none;"></i></a>
                        </form>
                    </td>
                    <td>
                        <label id="UploadPenilaianPembimbing"></label>
                    </td>
                    </tr>
                    <tr>
                    <th scope="row">5</th>
                    <td>File Presentasi</td>
                    <form method="post" action="<?= base_url('pkl/UploadPresentasiUjianPKL'); ?>" enctype="multipart/form-data">
                        <td hidden><input type="text" class="form-control" id="PklId" name="PklId"></td>
                        <td>
                            <?php
                            $notacc = [1];
                            if (!in_array($User['RoleId'], $notacc)):?>
                            <label for="input-PresentasiUjianPKL">
                                <i class="fa-solid fa-fw fa-file-arrow-up fa-2x"></i> 
                            </label>
                            <input type="file" id="input-PresentasiUjianPKL" name="presentasiUjianPKL" style="display: none;" accept=".pptx,.ppt">
                            <button class="btn btn-primary" id="submit-PresentasiUjianPKL" type="submit" style="display: none;" >Unggah</button>
                            <?php endif; ?>
                        </td>
                    </form>
                    <td>
                        <form action="">
                            <a id="linkPresentasi" href="#" target="_blank"><i id="linkIconPresentasi" class="fa-solid fa-fw fa-file-export fa-2x" style="display: none;"></i></a>
                        </form>
                    </td>
                    <td>
                        <label id="UploadPresentasi"></label>
                    </td>
                    </tr>
                    <tr>
                    <th scope="row">6</th>
                    <td>File Laporan Ujian PKL</td>
                    <form method="post" action="<?= base_url('pkl/UploadLaporanPKLUjianPKL'); ?>" enctype="multipart/form-data">
                        <td hidden><input type="text" class="form-control" id="PklId" name="PklId"></td>
                        <td>
                            <?php
                            $notacc = [1];
                            if (!in_array($User['RoleId'], $notacc)):?>
                            <label for="input-LaporanPKLUjianPKL">
                                <i class="fa-solid fa-fw fa-file-arrow-up fa-2x"></i> 
                            </label>
                            <input type="file" id="input-LaporanPKLUjianPKL" name="laporanPKLUjianPKL" style="display: none;" accept=".pdf">
                            <button class="btn btn-primary" id="submit-LaporanPKLUjianPKL" type="submit" style="display: none;" >Unggah</button>
                            <?php endif; ?>
                        </td>
                    </form>
                    <td>
                        <form action="">
                            <a id="linkLaporanPKL" href="#" target="_blank"><i id="linkIconLaporanPKL" class="fa-solid fa-fw fa-file-export fa-2x" style="display: none;"></i></a>
                        </form>
                    </td>
                    <td>
                        <label id="UploadLaporanPKL"></label>
                    </td>
                    </tr>
                </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>

<!-- End of Main Content -->