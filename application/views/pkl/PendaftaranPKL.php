<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Filter -->
    <?php 
    $mhs = [2,3,4];
    if(!in_array($User['RoleId'], $mhs)):?>
        <div class="card">
            <div class="row mt-3 ml-2 mb-0">
                <div class="col">
                <form method="post" action="<?= base_url('pkl/PendaftaranPKL'); ?>" enctype="multipart/form-data">
                    <div class="form-row">
                        <div class="form-group col-md-2">
                            <div class="form-row">
                                <div class="form-group col">
                                <select id="jenjang" name="jenjang" class="form-control">
                                    <option value="" selected>Pilih Jenjang</option>
                                    <option value="2">S1</option>
                                    <option value="3">S2</option>
                                    <option value="4">S3</option>
                                </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-2">
                            <div class="form-row">
                                <div class="form-group col">
                                <select id="limit" name="limit" class="form-control">
                                    <option value="">Tampil Data</option>
                                    <option value="10">10</option>
                                    <option value="25">25</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-2">
                            <div class="form-row">
                                <div class="form-group col">
                                    <input type="text" class="form-control" id="search" name="search" placeholder="Nama / NIM">
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-2">
                            <button type="submit" class="btn btn-secondary">Search</button>
                        </div>
                    </div>
                </form>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <!-- Table PKL -->
    <div class="card">
        <div class="row mt-3 ml-2 mr-2">
            <div class="col-4">
                <h4 class="text-white bg-dark">Riwayat Praktek Kerja Lapangan</h4>
            </div>
            <div class="col-5">
            </div>
            <div class="col-3">
                <?php 
                $mhs = [2,3,4];
                if(in_array($User['RoleId'], $mhs)):?>
                <div class="btn-group">
                    <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#DaftarTA">
                        <i class="fa-solid fa-fw fa-user-plus"></i>
                        Daftar Praktek Kerja Lapangan
                    </button>
                </div>
                <?php endif; ?>
            </div>
        </div>
        <div class="row g-0 pb-3 pl-2 pr-2">
        <div class="card-body">
            <table class="table table-striped">
            <thead class="thead-dark">
                <tr>
                <th scope="col" width="50px">No</th>
                <th scope="col" width="500px">Mahasiswa</th>
                <th scope="col" width="500px">Instansi</th>
                <th scope="col" width="500px">Tanggal Pelaksanaan</th>
                <th scope="col" width="500px">Status</th>
                <th scope="col" width="500px">Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $i = 1;
                    foreach($PendaftaranPKL as $pkl): 
                ?>
                <tr>
                <th scope="row" ><?=$i?></th>
                <td class="PklId" hidden><?=$pkl['Id']?></td>
                <td>
                    <p>
                        <?= $pkl['Name']?>
                    </p>
                </td>
                <td>
                    <p>
                        <?= $pkl['NamaInstansi']?>
                    </p>
                    <p>
                        <?= $pkl['AlamatInstansi']?>
                    </p>
                </td>
                <td>
                    <p>
                        <?= $pkl['TanggalMulai']?> s/d <?= $pkl['TanggalSelesai']?>
                    </p>
                </td>
                <td>
                    <p>
                        <?= $pkl['Status'] ?>
                    </p>
                </td>
                <td>
                    <div class="btn-group">
                        <button type="button" class="btn btn-warning btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Aksi
                            <i class="fa-solid fa-sliders"></i>
                        </button>
                        <div class="dropdown-menu">
                            <?php 
                            $acc = [0,2,3,4,5];
                            if(in_array($User['RoleId'], $acc)):?>
                                <div class="row mx-auto p-1">
                                    <button type="button" class="btn btn-warning editPkl">Edit</button>
                                </div>
                            <?php endif; ?>
                            <div class="row mx-auto p-1">
                                <button type="button" class="btn btn-info detailPkl">Detil</button>
                            </div>
                            <?php 
                            $acc = [0,2,3,4,5];
                            if(in_array($User['RoleId'], $acc)):?>
                                <div class="row mx-auto p-1">
                                    <button type="button" class="btn btn-warning berkasPkl">Berkas Pkl</button>
                                </div>
                                
                                <?php 
                                    $admin = [0,5];
                                    if ($pkl['FileSuratLamaran'] != null && $pkl['FileProposal'] != null && $pkl['FileCVMahasiswa'] != null  && $pkl['FileSuratPengantar'] != null && $pkl['FileTranskrip'] != null || in_array($User['RoleId'], $admin)):
                                    ?>
                                    <hr>
                                    <div class="row mx-auto p-1">
                                        <button type="button" class="btn btn-warning uploadSuratInstansi">Surat Instansi</button>
                                    </div>
                                    <?php if($pkl['FileSuratInstansi'] != null) :?>
                                    <div class="row mx-auto p-1">
                                        <a href="<?=base_url('/assets/uploads/suratInstansi_pkl/') . $pkl['FileSuratInstansi'];?>" target="_blank">
                                            <button type="button" class="btn btn-info">
                                                Surat Instansi <i id="linkIconSuratLamaranPkl" class="fa-solid fa-fw fa-file-export"></i>
                                            </button>
                                        </a>
                                    </div>
                                    <?php endif; ?>
                                    
                                    <?php
                                        if($pkl['Status'] == 'Diterima'): ?>
                                    <hr>
                                        <div class="row mx-auto p-1">
                                            <a type="button" class="btn btn-warning" href="<?= base_url('pkl/NilaiPembimbingLapang/') . $pkl['Id']; ?>">Nilai Pembimbing Lapang</a>
                                        </div>
                                    <?php endif; ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </td>
                </tr>
                <?php 
                    $i++;
                    endforeach; 
                ?>
            </tbody>
            </table>
        </div>
    </div>
</div>


<!-- Tambah Pkl Modal-->
<div class="modal fade" id="DaftarTA" role="dialog" aria-labelledby="exampleModalLabel"
    >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Daftar Praktek Kerja Lapangan</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <?php $minimalDate = date("Y-m-d"); ?>
                <form method="post" action="<?= base_url('pkl/DaftarPKL'); ?>" >

                <div class="form-group row">
                    <label for="inputName" class="col-sm-4 col-form-label">Nama</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputName" name="mhsId" value="<?=$User['Id']?>" hidden>
                        <input type="text" class="form-control" id="inputName" placeholder="Name" name="username" value="<?=$User['Name']?>" disabled>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputUsername" class="col-sm-4 col-form-label">NIM</label>
                    <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputUsername" placeholder="NIM" name="username" value="<?=$User['Username']?>" disabled>
                    </div>
                </div>

                <hr>
                <div class="form-group row">
                    <label for="inputDosenPembimbing1" class="col-sm-4 col-form-label">Instansi</label>
                    <div class="col-sm-8">
                    <select id="InstansiDaftarPKL" name="instansi" required>
                        <option value="">Select an option</option>
                        <option value="lainnya"> --- Lainnya --- </option>
                        <?php foreach ($Instansi as $instansi): ?>
                            <option value="<?= $instansi['Id'] ?>"><?= $instansi['Nama'] ?></option>
                        <?php endforeach; ?>
                    </select>
                    </div>
                </div>

                <div id="CardInstansiDaftarPKL" style="display: none;">
                    <div class="card mb-2">
                        <div class="form-group row mt-2 ml-2 mr-2">
                            <label for="inputPimpinan" class="col-sm-4 col-form-label">Pimpinan Instansi</label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputPimpinan" placeholder="Pimpinan Instansi" name="pimpinan">
                            </div>
                        </div>
                        <div class="form-group row mt-2 ml-2 mr-2">
                            <label for="inputNama" class="col-sm-4 col-form-label">Nama Instansi</label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputNama" placeholder="Nama Instansi" name="nama" >
                            </div>
                        </div>
                        <div class="form-group row mt-2 ml-2 mr-2">
                            <label for="inputAlamat" class="col-sm-4 col-form-label">Alamat Instansi</label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputAlamat" placeholder="Alamat Instansi" name="alamat" >
                            </div>
                        </div>
                        <div class="form-group row mt-2 ml-2 mr-2">
                            <label for="inputEmail" class="col-sm-4 col-form-label">Email Instansi</label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputEmail" placeholder="Email Instansi" name="email" >
                            </div>
                        </div>
                        <div class="form-group row mt-2 ml-2 mr-2">
                            <label for="inputTelpon" class="col-sm-4 col-form-label">Telpon Instansi</label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputTelpon" placeholder="Telpon Instansi" name="telpon" >
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="inputPembimbingLapang" class="col-sm-4 col-form-label">Pembimbing Lapang</label>
                    <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputPembimbingLapang" placeholder="Pembimbing Lapang" name="pembimbingLapang" required>
                    </div>
                </div>

                <hr>
                <div class="form-group row">
                    <label for="inputSemesterTahunAkademik" class="col-sm-4 col-form-label">Semester dan Tahun Akademik</label>
                    <div class="col-sm-8">
                    <select id="inputSemesterTahunAkademik" name="semesterId" class="form-control" required>
                        <?php foreach($Semester as $semester) :?>
                            <option value="<?= $semester['Id'] ?>"><?= $semester['Semester'] ?> - <?= $semester['TahunAkademik'] ?></option>
                        <?php endforeach; ?>
                    </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="inputTanggalMulai" class="col-sm-4 col-form-label">Tanggal Mulai</label>
                    <div class="col-sm-8">
                    <input type="date" class="form-control" id="inputTanggalMulai" name="tanggalMulai" min="<?=  $minimalDate ?>" required>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="inputTanggalSelesai" class="col-sm-4 col-form-label">Tanggal Selesai</label>
                    <div class="col-sm-8">
                    <input type="date" class="form-control" id="inputTanggalSelesai" name="tanggalSelesai" min="<?=  $minimalDate ?>" required>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="inputDosenPembimbing" class="col-sm-4 col-form-label">Dosen Pembimbing</label>
                    <div class="col-sm-8">
                    <select id="Dosen1" name="dosen1" required>
                        <option value="">Select an option</option>
                        <?php foreach ($Dosen as $dsn): ?>
                            <option value="<?= $dsn['Id'] ?>"><?= $dsn['Name'] ?></option>
                        <?php endforeach; ?>
                    </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit"
                <?php foreach($User as $key => $value):?>
                    <?php if($value==null || $value==""):?>
                        <?php if($key != 'TanggalLahir' && $key !=  'ActiveDate' && $key != 'Email'):?>
                            disabled
                        <?php endif; ?>
                    <?php endif; ?>
                <?php endforeach; ?>
                >Daftar</button>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- Detail Pkl Model -->
<div class="modal fade" id="detailPkl" role="dialog" aria-labelledby="exampleModalLabel"
    >
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Detail Praktek Kerja Lapangan</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
            <div class="card">
                <div class="card-body">
                    <div class="form-group row" style="">
                        <label for="inputUsername" class="col-sm-4 col-form-label">Nomor Induk Mahasiswa</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputUsername" name="username" disabled>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group row">
                        <label for="inputNamaInstansi" class="col-sm-4 col-form-label">Nama Instansi</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputNamaInstansi" name="namaInstansi" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputAlamatInstansi" class="col-sm-4 col-form-label">Alamat Instansi</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputAlamatInstansi" name="alamatInstansi" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputEmailInstansi" class="col-sm-4 col-form-label">Email Instansi</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputEmailInstansi" name="emailInstansi" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputTelponInstansi" class="col-sm-4 col-form-label">Telpon Instansi</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputTelponInstansi" name="telponInstansi" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPembimbingLapang" class="col-sm-4 col-form-label">Pembimbing Lapang</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputPembimbingLapang" name="pembimbingLapang" disabled>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group row">
                        <label for="inputDosenPembimbing" class="col-sm-4 col-form-label">Dosen Pembimbing</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputDosenPembimbing" name="dosenPembimbing" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputTanggalPelaksanaan" class="col-sm-4 col-form-label">Tanggal Pelaksanaan</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputTanggalPelaksanaan" name="tanggalPelaksanaan" disabled>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>

<!-- Edit Pkl Model -->
<div class="modal fade" id="editPkl" role="dialog" aria-labelledby="exampleModalLabel"
    >
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit PKL</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
            <form method="post" action="<?=base_url('pkl/editPkl')?>"  enctype="multipart/form-data">
            <div class="card">
                <div class="card-body">
                    <?php $minimalDate = date("Y-m-d"); ?>
                    <div class="form-group row">
                        <label for="inputDosenPembimbing" class="col-sm-4 col-form-label">Dosen Pembimbing</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputPembimbingLapang" name="PklId" hidden>
                        <select id="DosenPkl" name="dosenPkl" required>
                            <option value="">Select an option</option>
                            <?php foreach ($Dosen as $dsn): ?>
                                <option value="<?= $dsn['Id'] ?>"><?= $dsn['Name'] ?></option>
                            <?php endforeach; ?>
                        </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputTanggalMulai" class="col-sm-4 col-form-label">Tanggal Mulai</label>
                        <div class="col-sm-8">
                        <input type="date" class="form-control" id="inputTanggalMulai" name="tanggalMulai" min="<?=  $minimalDate ?>" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputTanggalSelesai" class="col-sm-4 col-form-label">Tanggal Selesai</label>
                        <div class="col-sm-8">
                        <input type="date" class="form-control" id="inputTanggalSelesai" name="tanggalSelesai" min="<?=  $minimalDate ?>" required>
                        </div>
                    </div>

                    <?php
                        $admin = [0,5];
                        if (in_array($User['RoleId'], $admin)) :
                    ?>
                    <hr>
                    <div class="form-group row">
                        <label for="inputStatus" class="col-sm-4 col-form-label">Status</label>
                        <div class="col-sm-8">
                        <select id="Status" name="status" class="form-control" required>
                            <option value="1">Diterima</option>
                            <option value="2">Diproses</option>
                            <option value="0">Ditolak</option>
                        </select>
                        </div>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Edit</button>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- Upload Surat Instansi -->
<div class="modal fade" id="uploadSuratInstansi"  role="dialog" aria-labelledby="exampleModalLabel"
    >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Unggah Surat Instansi</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
            <form method="post" action="<?= base_url('pkl/UploadSuratInstansi'); ?>" enctype="multipart/form-data">
            <div class="form-group">
                <div class="form-group row">
                    <div class="col-sm-3">Surat Instansi</div>
                    <div class="col-sm-9">
                        <div class="row">
                            <input type="text" class="form-control" id="inputUserId" name="PklId" hidden>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="customFile" name="suratInstansi" accept=".pdf">
                                <label class="custom-file-label" for="customFile">Choose file</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Unggah</button>
            </div>
            </form>
            </div>
        </div>
    </div>
</div>

<!-- Upload Surat Keluar -->
<div class="modal fade" id="uploadSuratKeluar"  role="dialog" aria-labelledby="exampleModalLabel"
    >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Unggah Surat Pengantar</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
            <form method="post" action="<?= base_url('pkl/uploadSuratKeluar'); ?>" enctype="multipart/form-data">
            <div class="form-group">
                <div class="form-group row">
                    <div class="col-sm-3">Surat Pengantar</div>
                    <div class="col-sm-9">
                        <div class="row">
                            <input type="text" class="form-control" id="inputUserId" name="PklId" hidden>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="customFile" name="suratKeluar" accept=".pdf">
                                <label class="custom-file-label" for="customFile">Choose file</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Unggah</button>
            </div>
            </form>
            </div>
        </div>
    </div>
</div>

<!-- Upload Surat Keluar -->
<div class="modal fade" id="uploadCVMahasiswa"  role="dialog" aria-labelledby="exampleModalLabel"
    >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Unggah CV Mahasiswa</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
            <form method="post" action="<?= base_url('pkl/uploadCVMahasiswa'); ?>" enctype="multipart/form-data">
            <div class="form-group">
                <div class="form-group row">
                    <div class="col-sm-3">CV Mahasiswa</div>
                    <div class="col-sm-9">
                        <div class="row">
                            <input type="text" class="form-control" id="inputUserId" name="PklId" hidden>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="customFile" name="CVMahasiswa" accept=".pdf">
                                <label class="custom-file-label" for="customFile">Choose file</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Unggah</button>
            </div>
            </form>
            </div>
        </div>
    </div>
</div>

<!-- Berkas Praktek Kerja Lapang Model -->
<div class="modal fade" id="berkasPkl" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Berkas Pendaftaran Ta</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
            <table class="table table-striped">
                <thead class="thead-dark">
                    <tr>
                    <th scope="col">#</th>
                    <th scope="col">Berkas</th>
                    <th scope="col">Unggah</th>
                    <th scope="col">File</th>
                    <th scope="col">Tanggal</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope="row">1</th>
                        <td hidden><input id="inputId" type="hidden" class="form-control" name="PklId"></td>
                        <td>Surat lamaran
                        </td>
                        <form method="post" action="<?= base_url('pkl/UploadSuratLamaran'); ?>" enctype="multipart/form-data">
                            <td hidden><input type="text" class="form-control" id="PklId" name="PklId"></td>
                            <td>
                                <label for="input-lamaranPkl">
                                    <i class="fa-solid fa-fw fa-file-arrow-up fa-2x"></i> 
                                </label>
                                <input type="file" id="input-lamaranPkl" name="lamaranPkl" style="display: none;" accept=".pdf">
                                <button class="btn btn-primary" id="submit-lamaranPkl" type="submit" style="display: none;" >Unggah</button>
                            </td>
                        </form>
                        <td>
                            <form action="">
                                <a id="linkSuratLamaranPkl" href="#" target="_blank">
                                    <i id="linkIconSuratLamaranPkl" class="fa-solid fa-fw fa-file-export fa-2x"></i>
                                </a>
                            </form>
                        </td>
                        <td>
                            <label id="UploadSuratLamaranPkl"></label>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">2</th>
                        <td hidden><input id="inputId" type="hidden" class="form-control" name="PklId"></td>
                        <td>Curriculum Vitae
                        </td>
                        <form method="post" action="<?= base_url('pkl/UploadCV'); ?>" enctype="multipart/form-data">
                            <td hidden><input type="text" class="form-control" id="PklId" name="PklId"></td>
                            <td>
                                <label for="input-CVPkl">
                                    <i class="fa-solid fa-fw fa-file-arrow-up fa-2x"></i> 
                                </label>
                                <input type="file" id="input-CVPkl" name="cvPkl" style="display: none;" accept=".pdf">
                                <button class="btn btn-primary" id="submit-CVPkl" type="submit" style="display: none;" >Unggah</button>
                            </td>
                        </form>
                        <td>
                            <form action="">
                                <a id="linkCVPkl" href="#" target="_blank">
                                    <i id="linkIconCVPkl" class="fa-solid fa-fw fa-file-export fa-2x"></i>
                                </a>
                            </form>
                        </td>
                        <td>
                            <label id="UploadCVPkl"></label>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">3</th>
                        <td hidden><input id="inputId" type="hidden" class="form-control" name="PklId"></td>
                        <td>Proposal PKL
                        </td>
                        <form method="post" action="<?= base_url('pkl/UploadProposal'); ?>" enctype="multipart/form-data">
                            <td hidden><input type="text" class="form-control" id="PklId" name="PklId"></td>
                            <td>
                                <label for="input-ProposalPkl">
                                    <i class="fa-solid fa-fw fa-file-arrow-up fa-2x"></i> 
                                </label>
                                <input type="file" id="input-ProposalPkl" name="proposalPkl" style="display: none;" accept=".pdf">
                                <button class="btn btn-primary" id="submit-ProposalPkl" type="submit" style="display: none;" >Unggah</button>
                            </td>
                        </form>
                        <td>
                            <form action="">
                                <a id="linkProposalPkl" href="#" target="_blank">
                                    <i id="linkIconProposalPkl" class="fa-solid fa-fw fa-file-export fa-2x"></i>
                                </a>
                            </form>
                        </td>
                        <td>
                            <label id="UploadProposalPkl"></label>
                        </td>
                    </tr>

                    <tr>
                        <th scope="row">4</th>
                        <td hidden><input id="inputId" type="hidden" class="form-control" name="PklId"></td>
                        <td>Surat Pengantar
                        </td>
                        <?php 
                        $admin = [0,5];
                        if(in_array($this->session->userdata('RoleId'), $admin)):
                        ?>
                        <form method="post" action="<?= base_url('pkl/UploadSuratPengantar'); ?>" enctype="multipart/form-data">
                            <td hidden><input type="text" class="form-control" id="PklId" name="PklId"></td>
                            <td>
                                <label for="input-PengantarPkl">
                                    <i class="fa-solid fa-fw fa-file-arrow-up fa-2x"></i> 
                                </label>
                                <input type="file" id="input-PengantarPkl" name="pengantarPkl" style="display: none;" accept=".pdf">
                                <button class="btn btn-primary" id="submit-PengantarPkl" type="submit" style="display: none;" >Unggah</button>
                            </td>
                        </form>
                        <?php else: ?>
                        <td></td>
                        <?php endif; ?>
                        <td>
                            <form action="">
                                <a id="linkPengantarPkl" href="#" target="_blank">
                                    <i id="linkIconPengantarPkl" class="fa-solid fa-fw fa-file-export fa-2x"></i>
                                </a>
                            </form>
                        </td>
                        <td>
                            <label id="UploadPengantarPkl"></label>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">5</th>
                        <td hidden><input id="inputId" type="hidden" class="form-control" name="PklId"></td>
                        <td>Transkrip
                        </td>
                        <?php 
                        $admin = [0,5];
                        if(in_array($this->session->userdata('RoleId'), $admin)):
                        ?>
                        <form method="post" action="<?= base_url('pkl/UploadTranskrip'); ?>" enctype="multipart/form-data">
                            <td hidden><input type="text" class="form-control" id="PklId" name="PklId"></td>
                            <td>
                                <label for="input-TranskripPkl">
                                    <i class="fa-solid fa-fw fa-file-arrow-up fa-2x"></i> 
                                </label>
                                <input type="file" id="input-TranskripPkl" name="TranskripPkl" style="display: none;" accept=".pdf">
                                <button class="btn btn-primary" id="submit-TranskripPkl" type="submit" style="display: none;" >Unggah</button>
                            </td>
                        </form>
                        <?php else: ?>
                        <td></td>
                        <?php endif; ?>
                        <td>
                            <form action="">
                                <a id="linkTranskripPkl" href="#" target="_blank">
                                    <i id="linkIconTranskripPkl" class="fa-solid fa-fw fa-file-export fa-2x"></i>
                                </a>
                            </form>
                        </td>
                        <td>
                            <label id="UploadTranskripPkl"></label>
                        </td>
                    </tr>
                </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>

<script>
    document.addEventListener('DOMContentLoaded', function () {
        var selectInstansiDaftarPKL = document.getElementById("InstansiDaftarPKL");
        var CardInstansiDaftarPKL = document.getElementById("CardInstansiDaftarPKL");

        if (selectInstansiDaftarPKL) {
            selectInstansiDaftarPKL.addEventListener("change", function () {
                if (selectInstansiDaftarPKL.value === "lainnya") {
                    CardInstansiDaftarPKL.style.display = "block";
                } else {
                    CardInstansiDaftarPKL.style.display = "none";
                }
            });
        } else {
            console.error('Element with ID "InstansiDaftarPKL" not found!');
        }
    });
</script>

<!-- End of Main Content -->