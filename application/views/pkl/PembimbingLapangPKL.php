<!-- Begin Page Content -->
<div class="container-fluid">

    <div class="card">
        <div class="row mt-3 ml-2 mr-2">
            <div class="col-4">
                <h4 class="text-white bg-dark">Nilai Pembimbing Lapang</h4>
            </div>
            <div class="col-5">
            </div>
            <div class="col-3">
            </div>
        </div>
        <div class="row g-0 pb-3 pl-2 pr-2">
        <div class="card-body">
            <div class="card">
                <form method="post" action="<?= base_url('pkl/UpdateJenisSks')?>">
                <div class="card-body">
                    <div class="form-group row">
                        <label for="inputJenisSks" class="col-sm-4 col-form-label">Jenis SKS</label>
                        <div class="col-sm-8">
                        <input type="text" id="inputPklId" class="form-control" name="PklId" value="<?= $PKL['Id']?>" hidden>
                        <select id="inputJenisSks" name="jenisSks" required onchange="showMataKuliahFields()">
                            <option value="">Select an option</option>
                            <option value="4" <?= ($PKL['JenisSks'] == 4) ? 'selected' : '' ?>>4</option>
                            <option value="7" <?= ($PKL['JenisSks'] == 7) ? 'selected' : '' ?>>7</option>
                            <option value="10" <?= ($PKL['JenisSks'] == 10) ? 'selected' : '' ?>>10</option>
                            <option value="13" <?= ($PKL['JenisSks'] == 13) ? 'selected' : '' ?>>13</option>
                            <option value="16" <?= ($PKL['JenisSks'] == 16) ? 'selected' : '' ?>>16</option>
                        </select>
                        </div>
                    </div>
                    <div class="form-group row" id="rowMK1">
                        <div class="row">
                            <label for="inputMK1" class="col-sm-4 col-form-label">Mata Kuliah Tambahan 1</label>
                            <div class="col-sm-8">
                            <select id="inputMK1" name="mataKuliah1">
                                <option value="">Select an option</option>
                                <option value="1" <?= ($PKL['MataKuliah1'] == 1) ? 'selected' : '' ?>>Sumber Daya Alam</option>
                                <option value="2" <?= ($PKL['MataKuliah1'] == 2) ? 'selected' : '' ?>>Keselamatan dan Lingkungan</option>
                                <option value="3" <?= ($PKL['MataKuliah1'] == 3) ? 'selected' : '' ?>>Metode dan Terapan</option>
                                <option value="4" <?= ($PKL['MataKuliah1'] == 4) ? 'selected' : '' ?>>Analisis, Penilaian, dan Tindakan</option>
                                <option value="5" <?= ($PKL['MataKuliah1'] == 5) ? 'selected' : '' ?>>Kemandirian Pengayaan Pengetahuan</option>
                            </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row" id="rowMK2">
                        <div class="row">
                            <label for="inputMK2" class="col-sm-4 col-form-label">Mata Kuliah Tambahan 2</label>
                            <div class="col-sm-8">
                            <select id="inputMK2" name="mataKuliah2">
                                <option value="">Select an option</option>
                                <option value="1" <?= ($PKL['MataKuliah2'] == 1) ? 'selected' : '' ?>>Sumber Daya Alam</option>
                                <option value="2" <?= ($PKL['MataKuliah2'] == 2) ? 'selected' : '' ?>>Keselamatan dan Lingkungan</option>
                                <option value="3" <?= ($PKL['MataKuliah2'] == 3) ? 'selected' : '' ?>>Metode dan Terapan</option>
                                <option value="4" <?= ($PKL['MataKuliah2'] == 4) ? 'selected' : '' ?>>Analisis, Penilaian, dan Tindakan</option>
                                <option value="5" <?= ($PKL['MataKuliah2'] == 5) ? 'selected' : '' ?>>Kemandirian Pengayaan Pengetahuan</option>
                            </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row" id="rowMK3">
                        <div class="row">
                            <label for="inputMK3" class="col-sm-4 col-form-label">Mata Kuliah Tambahan 3</label>
                            <div class="col-sm-8">
                            <select id="inputMK3" name="mataKuliah3">
                                <option value="">Select an option</option>
                                <option value="1" <?= ($PKL['MataKuliah3'] == 1) ? 'selected' : '' ?>>Sumber Daya Alam</option>
                                <option value="2" <?= ($PKL['MataKuliah3'] == 2) ? 'selected' : '' ?>>Keselamatan dan Lingkungan</option>
                                <option value="3" <?= ($PKL['MataKuliah3'] == 3) ? 'selected' : '' ?>>Metode dan Terapan</option>
                                <option value="4" <?= ($PKL['MataKuliah3'] == 4) ? 'selected' : '' ?>>Analisis, Penilaian, dan Tindakan</option>
                                <option value="5" <?= ($PKL['MataKuliah3'] == 5) ? 'selected' : '' ?>>Kemandirian Pengayaan Pengetahuan</option>
                            </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer d-flex justify-content-end">
                    <button type="submit" class="btn btn-success">Submit</button>
                </div>
                </form>
            </div>

            <div class="card">
                <div class="card-body">
                    <table class="table table-striped">
                    <thead class="thead-dark">
                        <tr>
                        <th scope="col" width="50px">No</th>
                        <th scope="col" width="500px">Lembar Penilaian</th>
                        <th scope="col" width="500px">Unduh</th>
                        <th scope="col" width="500px">Tampil</th>
                        <th scope="col" width="500px">Input</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                        <th scope="row" >1</th>
                        <td class="PklId" hidden><?=$PKL['Id']?></td>
                        <td>
                            <p>
                                Laporan Akhir Program
                            </p>
                        </td>
                        <td>
                            <p>
                                <a href="https://kimia.ub.ac.id/wp-content/uploads/2023/10/LP_PKL_B_Pembimbing-Lapang.docx">Unduh disini</a>
                            </p>
                        </td>
                        <td>
                            <p>
                                <?php if($PKL['LP_PKL_A'] != null): ?>
                                    <button type="button" class="btn btn-warning viewNilaiPembimbingLapangLPA">Preview</button>
                                <?php endif; ?>
                            </p>
                        </td>
                        <td>
                            <p>
                                <?php 
                                    $admin = [0,5];
                                    if(in_array($User['RoleId'], $admin)):
                                ?>
                                <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#inputNilaiPembimbingLapangLPA">Input Nilai</button>
                                <?php endif; ?>
                            </p>
                        </td>
                        </tr>
                        <?php 
                            $sks = [7,10,13,16];
                            if(in_array($PKL['JenisSks'], $sks)):
                        ?>
                        <tr>
                        <th scope="row" >2</th>
                        <td class="PklId" hidden><?=$PKL['Id']?></td>
                        <td>
                            <p>
                                Laporan di Tengah Program
                            </p>
                        </td>
                        <td>
                            <p>
                            <a href="https://kimia.ub.ac.id/wp-content/uploads/2023/10/LP_PKL_A_Pembimbing-Lapang.docx">Unduh disini</a>
                            </p>
                        </td>
                        <td>
                            <p>
                                <?php if($PKL['LP_PKL_B'] != null): ?>
                                    <button type="button" class="btn btn-warning viewNilaiPembimbingLapangLPB">Preview</button>
                                <?php endif; ?>
                            </p>
                        </td>
                        <td>
                            <p>
                                <?php 
                                    $admin = [0,5];
                                    if(in_array($User['RoleId'], $admin)):
                                ?>
                                <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#inputNilaiPembimbingLapangLPB">Input Nilai</button>
                                <?php endif; ?>
                            </p>
                        </td>
                        </tr>
                        <tr>
                        <th scope="row" >3</th>
                        <td class="PklId" hidden><?=$PKL['Id']?></td>
                        <td>
                            <p>
                                <?= $PKL['MataKuliah1Text']?>
                            </p>
                        </td>
                        <td>
                            <p>
                                <?php if($PKL['MataKuliah1'] == 1): ?>
                                    <a href="https://kimia.ub.ac.id/wp-content/uploads/2023/10/LP_CPL_3.1_Pembimbing-Lapang.docx">Unduh disini</a>
                                <?php elseif($PKL['MataKuliah1'] == 2):?>
                                    <a href="https://kimia.ub.ac.id/wp-content/uploads/2023/10/LP_CPL_5.1_Pembimbing-Lapang.docx">Unduh disini</a>
                                <?php elseif($PKL['MataKuliah1'] == 3):?>
                                    <a href="https://kimia.ub.ac.id/wp-content/uploads/2023/10/LP_CPL_6.1_Pembimbing-Lapang.docx">Unduh disini</a>
                                <?php elseif($PKL['MataKuliah1'] == 4):?>
                                    <a href="https://kimia.ub.ac.id/wp-content/uploads/2023/10/LP_CPL_8.1_Pembimbing-Lapang.docx">Unduh disini</a>
                                <?php elseif($PKL['MataKuliah1'] == 5):?>
                                    <a href="https://kimia.ub.ac.id/wp-content/uploads/2023/10/LP_CPL_10.1_Pembimbing-Lapang.docx">Unduh disini</a>
                                <?php endif; ?>
                            </p>
                        </td>
                        <td>
                            <p>
                                <?php if($PKL['LP_MK1'] != null): ?>
                                    <button type="button" class="btn btn-warning viewNilaiPembimbingLapangMK1">Preview</button>
                                <?php endif; ?>
                            </p>
                        </td>
                        <td>
                            <p>
                                <?php 
                                    $admin = [0,5];
                                    if(in_array($User['RoleId'], $admin)):
                                ?>
                                    <?php if($PKL['MataKuliah1'] == 1): ?>
                                        <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#inputNilaiPembimbingLapangSumberDayaAlamMK1">Input Nilai</button>
                                    <?php elseif($PKL['MataKuliah1'] == 2):?>
                                        <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#inputNilaiPembimbingLapangKeselamatandanLingkunganMK1">Input Nilai</button>
                                    <?php elseif($PKL['MataKuliah1'] == 3):?>
                                        <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#inputNilaiPembimbingLapangMetodedanTerapanMK1">Input Nilai</button>
                                    <?php elseif($PKL['MataKuliah1'] == 4):?>
                                        <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#inputNilaiPembimbingLapangAnalisisPenilaiandanTindakanMK1">Input Nilai</button>
                                    <?php elseif($PKL['MataKuliah1'] == 5):?>
                                        <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#inputNilaiPembimbingLapangKemandirianPengayaanPengetahuanMK1">Input Nilai</button>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </p>
                        </td>
                        </tr>

                        <?php if($PKL['JenisSks'] == "10" || $PKL['JenisSks'] == "13" || $PKL['JenisSks'] == "16"):?>
                        <tr>
                        <th scope="row" >4</th>
                        <td class="PklId" hidden><?=$PKL['Id']?></td>
                        <td>
                            <p>
                                <?= $PKL['MataKuliah2Text']?>
                            </p>
                        </td>
                        <td>
                            <p>
                                <?php if($PKL['MataKuliah2'] == 1): ?>
                                    <a href="https://kimia.ub.ac.id/wp-content/uploads/2023/10/LP_CPL_3.1_Pembimbing-Lapang.docx">Unduh disini</a>
                                <?php elseif($PKL['MataKuliah2'] == 2):?>
                                    <a href="https://kimia.ub.ac.id/wp-content/uploads/2023/10/LP_CPL_5.1_Pembimbing-Lapang.docx">Unduh disini</a>
                                <?php elseif($PKL['MataKuliah2'] == 3):?>
                                    <a href="https://kimia.ub.ac.id/wp-content/uploads/2023/10/LP_CPL_6.1_Pembimbing-Lapang.docx">Unduh disini</a>
                                <?php elseif($PKL['MataKuliah2'] == 4):?>
                                    <a href="https://kimia.ub.ac.id/wp-content/uploads/2023/10/LP_CPL_8.1_Pembimbing-Lapang.docx">Unduh disini</a>
                                <?php elseif($PKL['MataKuliah2'] == 5):?>
                                    <a href="https://kimia.ub.ac.id/wp-content/uploads/2023/10/LP_CPL_10.1_Pembimbing-Lapang.docx">Unduh disini</a>
                                <?php endif; ?>
                            </p>
                        </td>
                        <td>
                            <p>
                                <?php if($PKL['LP_MK2'] != null): ?>
                                    <button type="button" class="btn btn-warning viewNilaiPembimbingLapangMK2">Preview</button>
                                <?php endif; ?>
                            </p>
                        </td>
                        <td>
                            <p>
                                <?php 
                                    $admin = [0,5];
                                    if(in_array($User['RoleId'], $admin)):
                                ?>
                                    <?php if($PKL['MataKuliah2'] == 1): ?>
                                        <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#inputNilaiPembimbingLapangSumberDayaAlamMK2">Input Nilai</button>
                                    <?php elseif($PKL['MataKuliah2'] == 2):?>
                                        <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#inputNilaiPembimbingLapangKeselamatandanLingkunganMK2">Input Nilai</button>
                                    <?php elseif($PKL['MataKuliah2'] == 3):?>
                                        <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#inputNilaiPembimbingLapangMetodedanTerapanMK2">Input Nilai</button>
                                    <?php elseif($PKL['MataKuliah2'] == 4):?>
                                        <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#inputNilaiPembimbingLapangAnalisisPenilaiandanTindakanMK2">Input Nilai</button>
                                    <?php elseif($PKL['MataKuliah2'] == 5):?>
                                        <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#inputNilaiPembimbingLapangKemandirianPengayaanPengetahuanMK2">Input Nilai</button>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </p>
                        </td>
                        </tr>
                        <?php endif; ?>

                        <?php if($PKL['JenisSks'] == "13" || $PKL['JenisSks'] == "16"):?>
                        <tr>
                        <th scope="row" >5</th>
                        <td class="PklId" hidden><?=$PKL['Id']?></td>
                        <td>
                            <p>
                                <?= $PKL['MataKuliah3Text']?>
                            </p>
                        </td>
                        <td>
                            <p>
                                <?php if($PKL['MataKuliah3'] == 1): ?>
                                    <a href="https://kimia.ub.ac.id/wp-content/uploads/2023/10/LP_CPL_3.1_Pembimbing-Lapang.docx">Unduh disini</a>
                                <?php elseif($PKL['MataKuliah3'] == 2):?>
                                    <a href="https://kimia.ub.ac.id/wp-content/uploads/2023/10/LP_CPL_5.1_Pembimbing-Lapang.docx">Unduh disini</a>
                                <?php elseif($PKL['MataKuliah3'] == 3):?>
                                    <a href="https://kimia.ub.ac.id/wp-content/uploads/2023/10/LP_CPL_6.1_Pembimbing-Lapang.docx">Unduh disini</a>
                                <?php elseif($PKL['MataKuliah3'] == 4):?>
                                    <a href="https://kimia.ub.ac.id/wp-content/uploads/2023/10/LP_CPL_8.1_Pembimbing-Lapang.docx">Unduh disini</a>
                                <?php elseif($PKL['MataKuliah3'] == 5):?>
                                    <a href="https://kimia.ub.ac.id/wp-content/uploads/2023/10/LP_CPL_10.1_Pembimbing-Lapang.docx">Unduh disini</a>
                                <?php endif; ?>
                            </p>
                        </td>
                        <td>
                            <p>
                                <?php if($PKL['LP_MK3'] != null): ?>
                                    <button type="button" class="btn btn-warning viewNilaiPembimbingLapangMK2">Preview</button>
                                <?php endif; ?>
                            </p>
                        </td>
                        <td>
                            <p>
                                <?php 
                                    $admin = [0,5];
                                    if(in_array($User['RoleId'], $admin)):
                                ?>
                                    <?php if($PKL['MataKuliah3'] == 1): ?>
                                        <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#inputNilaiPembimbingLapangSumberDayaAlamMK3">Input Nilai</button>
                                    <?php elseif($PKL['MataKuliah3'] == 2):?>
                                        <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#inputNilaiPembimbingLapangKeselamatandanLingkunganMK3">Input Nilai</button>
                                    <?php elseif($PKL['MataKuliah3'] == 3):?>
                                        <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#inputNilaiPembimbingLapangMetodedanTerapanMK3">Input Nilai</button>
                                    <?php elseif($PKL['MataKuliah3'] == 4):?>
                                        <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#inputNilaiPembimbingLapangAnalisisPenilaiandanTindakanMK3">Input Nilai</button>
                                    <?php elseif($PKL['MataKuliah3'] == 5):?>
                                        <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#inputNilaiPembimbingLapangKemandirianPengayaanPengetahuanMK3">Input Nilai</button>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </p>
                        </td>
                        </tr>
                        <?php endif; ?>

                        <?php endif; ?>
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="inputNilaiPembimbingLapangLPA" role="dialog" aria-labelledby="exampleModalLabel" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Input Nilai Laporan Akhir Program</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="<?= base_url('pkl/NilaiPembimbingLapangLPA'); ?>" >
                <div class="form-group">
                    <h5>CPMK 12.1</h5>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">Memahami prinsip-prinsip etika profesional dalam bidang kimia</label>
                        <input type="text" class="form-control" id="inputName" name="PklId" value="<?=$PKL['Id']?>" hidden>
                        <div class="col-sm-4">
                            <select name="12.1_1">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">Ketepatan dan kejujuran</label>
                        <div class="col-sm-4">
                            <select name="12.1_2" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">Pelaporan yang akurat</label>
                        <div class="col-sm-4">
                            <select name="12.1_3" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">Kerjasama dan kontribusi</label>
                        <div class="col-sm-4">
                            <select name="12.1_4" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <hr>
                    <h5>CPMK 12.2</h5>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">Mengenal dan menerapkan standar-standar kimia</label>
                        <div class="col-sm-4">
                            <select name="12.2_1" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">memahami protokol keselamatan laboratorium yang melibatkan penggunaan alat pelindung diri (APD), penanganan bahan kimia berbahaya, serta tindakan darurat dalam kasus insiden.</label>
                        <div class="col-sm-4">
                            <select name="12.2_2" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">memahami bagaimana mengelola, menyimpan, dan membuang bahan kimia berbahaya dengan aman dan sesuai dengan peraturan</label>
                        <div class="col-sm-4">
                            <select name="12.2_3" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">mahir dalam penggunaan peralatan dan instrumen laboratorium yang umum digunakan dalam penelitian kimia</label>
                        <div class="col-sm-4">
                            <select name="12.2_4" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <hr>
                    <h5>CPMK 12.3</h5>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">Bertindak secara etis dalam praktik kimia</label>
                        <div class="col-sm-4">
                            <select name="12.3_1" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">membuat keputusan berdasarkan pertimbangan etika, mematuhi peraturan dan regulasi yang berlaku, serta menjaga integritas dan kerahasiaan data</label>
                        <div class="col-sm-4">
                            <select name="12.3_2" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <hr>
                    <h5>CPMK 12.5</h5>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">Berkomunikasi secara efektif tentang tanggung jawab sosial dan profesional</label>
                        <div class="col-sm-4">
                            <select name="12.5_1" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">dapat menyampaikan informasi tentang prinsip-prinsip etika dan standar-standar kimia kepada rekan kerja, masyarakat, dan pemangku kepentingan lainnya dengan bahasa yang mudah dipahami</label>
                        <div class="col-sm-4">
                            <select name="12.5_2" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <hr>
                    <h5>CPMK 15.1</h5>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">Memahami praktik dan kebutuhan industri/akademik terkait</label>
                        <div class="col-sm-4">
                            <select name="15.1_1" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">Mematuhi dan melaksanakan peraturan dan tata tertib di lingkungan kerja industri atau akademik</label>
                        <div class="col-sm-4">
                            <select name="15.1_2" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <hr>
                    <h5>CPMK 15.2</h5>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">Mampu menerapkan pengetahuan dan keterampilan dalam situasi nyata</label>
                        <div class="col-sm-4">
                            <select name="15.2_1" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">menerapkan pengetahuan dan keterampilan yang mahasiswa peroleh dari perkuliahan dalam konteks kerja nyata</label>
                        <div class="col-sm-4">
                            <select name="15.2_2" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <hr>
                    <h5>CPMK 15.3</h5>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">Memiliki keterampilan komunikasi yang efektif</label>
                        <div class="col-sm-4">
                            <select name="15.3_1" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">berkomunikasi dengan rekan kerja, klien, atau sesama akademisi dengan jelas dan efektif</label>
                        <div class="col-sm-4">
                            <select name="15.3_2" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <hr>
                    <h5>CPMK 15.4</h5>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">Memiliki kemampuan beradaptasi dan fleksibilitas</label>
                        <div class="col-sm-4">
                            <select name="15.4_1" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">beradaptasi dengan perubahan dan tantangan dalam lingkungan kerja industri atau akademik</label>
                        <div class="col-sm-4">
                            <select name="15.4_2" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <hr>
                    <h5>CPMK 15.5</h5>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">Memiliki pemahaman tentang profesionalisme dan etika kerja</label>
                        <div class="col-sm-4">
                            <select name="15.5_1" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">bersikap profesional, memiliki integritas, menghormati etika kerja, dan bekerjasama dalam lingkungan kerja</label>
                        <div class="col-sm-4">
                            <select name="15.5_2" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="inputNilaiPembimbingLapangLPB" role="dialog" aria-labelledby="exampleModalLabel" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Input Nilai Laporan Tengah Program</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="<?= base_url('pkl/NilaiPembimbingLapangLPB'); ?>" >
                <div class="form-group">
                    <h5>CPMK 12.1</h5>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">Memahami prinsip-prinsip etika profesional dalam bidang kimia</label>
                        <input type="text" class="form-control" id="inputName" name="PklId" value="<?=$PKL['Id']?>" hidden>
                        <div class="col-sm-4">
                            <select name="12.1_1">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">Ketepatan dan kejujuran</label>
                        <div class="col-sm-4">
                            <select name="12.1_2" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">Pelaporan yang akurat</label>
                        <div class="col-sm-4">
                            <select name="12.1_3" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">Kerjasama dan kontribusi</label>
                        <div class="col-sm-4">
                            <select name="12.1_4" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <hr>
                    <h5>CPMK 12.2</h5>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">Mengenal dan menerapkan standar-standar kimia</label>
                        <div class="col-sm-4">
                            <select name="12.2_1" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">memahami protokol keselamatan laboratorium yang melibatkan penggunaan alat pelindung diri (APD), penanganan bahan kimia berbahaya, serta tindakan darurat dalam kasus insiden.</label>
                        <div class="col-sm-4">
                            <select name="12.2_2" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">memahami bagaimana mengelola, menyimpan, dan membuang bahan kimia berbahaya dengan aman dan sesuai dengan peraturan</label>
                        <div class="col-sm-4">
                            <select name="12.2_3" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">mahir dalam penggunaan peralatan dan instrumen laboratorium yang umum digunakan dalam penelitian kimia</label>
                        <div class="col-sm-4">
                            <select name="12.2_4" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <hr>
                    <h5>CPMK 12.3</h5>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">Bertindak secara etis dalam praktik kimia</label>
                        <div class="col-sm-4">
                            <select name="12.3_1" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">membuat keputusan berdasarkan pertimbangan etika, mematuhi peraturan dan regulasi yang berlaku, serta menjaga integritas dan kerahasiaan data</label>
                        <div class="col-sm-4">
                            <select name="12.3_2" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <hr>
                    <h5>CPMK 12.5</h5>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">Berkomunikasi secara efektif tentang tanggung jawab sosial dan profesional</label>
                        <div class="col-sm-4">
                            <select name="12.5_1" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">dapat menyampaikan informasi tentang prinsip-prinsip etika dan standar-standar kimia kepada rekan kerja, masyarakat, dan pemangku kepentingan lainnya dengan bahasa yang mudah dipahami</label>
                        <div class="col-sm-4">
                            <select name="12.5_2" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <hr>
                    <h5>CPMK 15.1</h5>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">Memahami praktik dan kebutuhan industri/akademik terkait</label>
                        <div class="col-sm-4">
                            <select name="15.1_1" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">Mematuhi dan melaksanakan peraturan dan tata tertib di lingkungan kerja industri atau akademik</label>
                        <div class="col-sm-4">
                            <select name="15.1_2" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <hr>
                    <h5>CPMK 15.2</h5>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">Mampu menerapkan pengetahuan dan keterampilan dalam situasi nyata</label>
                        <div class="col-sm-4">
                            <select name="15.2_1" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">menerapkan pengetahuan dan keterampilan yang mahasiswa peroleh dari perkuliahan dalam konteks kerja nyata</label>
                        <div class="col-sm-4">
                            <select name="15.2_2" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <hr>
                    <h5>CPMK 15.3</h5>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">Memiliki keterampilan komunikasi yang efektif</label>
                        <div class="col-sm-4">
                            <select name="15.3_1" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">berkomunikasi dengan rekan kerja, klien, atau sesama akademisi dengan jelas dan efektif</label>
                        <div class="col-sm-4">
                            <select name="15.3_2" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <hr>
                    <h5>CPMK 15.4</h5>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">Memiliki kemampuan beradaptasi dan fleksibilitas</label>
                        <div class="col-sm-4">
                            <select name="15.4_1" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">beradaptasi dengan perubahan dan tantangan dalam lingkungan kerja industri atau akademik</label>
                        <div class="col-sm-4">
                            <select name="15.4_2" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <hr>
                    <h5>CPMK 15.5</h5>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">Memiliki pemahaman tentang profesionalisme dan etika kerja</label>
                        <div class="col-sm-4">
                            <select name="15.5_1" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-8 col-form-label">bersikap profesional, memiliki integritas, menghormati etika kerja, dan bekerjasama dalam lingkungan kerja</label>
                        <div class="col-sm-4">
                            <select name="15.5_2" id="">
                                <option value="0">Tidak tampak</option>
                                <option value="1">Kurang</option>
                                <option value="2">Cukup</option>
                                <option value="3">Baik</option>
                                <option value="4">Sangat baik</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- Sumber Daya Alam -->
<div class="modal fade" id="inputNilaiPembimbingLapangSumberDayaAlamMK1" role="dialog" aria-labelledby="exampleModalLabel" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Input Nilai Mata Kuliah Sumber Daya ALam</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="sumberDayaAlamPembimbingLapang" method="post" action="<?= base_url('pkl/NilaiPembimbingLapangMk1');?>">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Memiliki pengetahuan tentang sumber daya alam Indonesia</label>
                            <input type="text" class="form-control" id="inputName" name="PklId" value="<?=$PKL['Id']?>" hidden>
                            <div class="col-sm-4">
                                <select name="1">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Mengenali proses ekstraksi dan pemurnian</label>
                            <div class="col-sm-4">
                                <select name="2" id="">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Mampu menerapkan konsep kimia dalam pemanfaatan sumber daya alam</label>
                            <div class="col-sm-4">
                                <select name="3" id="">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="inputNilaiPembimbingLapangSumberDayaAlamMK2" role="dialog" aria-labelledby="exampleModalLabel" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Input Nilai Mata Kuliah Sumber Daya ALam</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="sumberDayaAlamPembimbingLapang" method="post" action="<?= base_url('pkl/NilaiPembimbingLapangMk2');?>">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Memiliki pengetahuan tentang sumber daya alam Indonesia</label>
                            <input type="text" class="form-control" id="inputName" name="PklId" value="<?=$PKL['Id']?>" hidden>
                            <div class="col-sm-4">
                                <select name="1">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Mengenali proses ekstraksi dan pemurnian</label>
                            <div class="col-sm-4">
                                <select name="2" id="">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Mampu menerapkan konsep kimia dalam pemanfaatan sumber daya alam</label>
                            <div class="col-sm-4">
                                <select name="3" id="">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="inputNilaiPembimbingLapangSumberDayaAlamMK3" role="dialog" aria-labelledby="exampleModalLabel" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Input Nilai Mata Kuliah Sumber Daya ALam</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="sumberDayaAlamPembimbingLapang" method="post" action="<?= base_url('pkl/NilaiPembimbingLapangMk3');?>">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Memiliki pengetahuan tentang sumber daya alam Indonesia</label>
                            <input type="text" class="form-control" id="inputName" name="PklId" value="<?=$PKL['Id']?>" hidden>
                            <div class="col-sm-4">
                                <select name="1">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Mengenali proses ekstraksi dan pemurnian</label>
                            <div class="col-sm-4">
                                <select name="2" id="">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Mampu menerapkan konsep kimia dalam pemanfaatan sumber daya alam</label>
                            <div class="col-sm-4">
                                <select name="3" id="">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- Keselamatan dan Linkungan -->
<div class="modal fade" id="inputNilaiPembimbingLapangKeselamatandanLingkunganMK1" role="dialog" aria-labelledby="exampleModalLabel" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Input Nilai Keselamatan dan Lingkungan</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="<?= base_url('pkl/NilaiPembimbingLapangMK1');?>" id="keselamatandanLingkunganPembimbingLapang">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Memahami permasalahan keselamatan dan lingkungan yang relevan dengan bidang keahliannya</label>
                            <input type="text" class="form-control" id="inputName" name="PklId" value="<?=$PKL['Id']?>" hidden>
                            <div class="col-sm-4">
                                <select name="1">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Mengenali aspek hukum dan peraturan yang berlaku</label>
                            <div class="col-sm-4">
                                <select name="2" id="">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Mengetahui langkah-langkah mitigasi dan pengelolaan risiko</label>
                            <div class="col-sm-4">
                                <select name="3" id="">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="inputNilaiPembimbingLapangKeselamatandanLingkunganMK2" role="dialog" aria-labelledby="exampleModalLabel" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Input Nilai Keselamatan dan Lingkungan</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="<?= base_url('pkl/NilaiPembimbingLapangMK2');?>" id="keselamatandanLingkunganPembimbingLapang">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Memahami permasalahan keselamatan dan lingkungan yang relevan dengan bidang keahliannya</label>
                            <input type="text" class="form-control" id="inputName" name="PklId" value="<?=$PKL['Id']?>" hidden>
                            <div class="col-sm-4">
                                <select name="1">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Mengenali aspek hukum dan peraturan yang berlaku</label>
                            <div class="col-sm-4">
                                <select name="2" id="">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Mengetahui langkah-langkah mitigasi dan pengelolaan risiko</label>
                            <div class="col-sm-4">
                                <select name="3" id="">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="inputNilaiPembimbingLapangKeselamatandanLingkunganMK3" role="dialog" aria-labelledby="exampleModalLabel" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Input Nilai Keselamatan dan Lingkungan</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="<?= base_url('pkl/NilaiPembimbingLapangMK3');?>" id="keselamatandanLingkunganPembimbingLapang">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Memahami permasalahan keselamatan dan lingkungan yang relevan dengan bidang keahliannya</label>
                            <input type="text" class="form-control" id="inputName" name="PklId" value="<?=$PKL['Id']?>" hidden>
                            <div class="col-sm-4">
                                <select name="1">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Mengenali aspek hukum dan peraturan yang berlaku</label>
                            <div class="col-sm-4">
                                <select name="2" id="">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Mengetahui langkah-langkah mitigasi dan pengelolaan risiko</label>
                            <div class="col-sm-4">
                                <select name="3" id="">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- Metode dan Terapan -->
<div class="modal fade" id="inputNilaiPembimbingLapangMetodedanTerapanMK1" role="dialog" aria-labelledby="exampleModalLabel" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Input Nilai Metode dan Terapan</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="<?= base_url('pkl/NilaiPembimbingLapangMK1') ?>" id="metodedanTerapanPembimbingLapang">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Mahasiswa memahami metode penyelesaian masalah kimia</label>
                            <input type="text" class="form-control" id="inputName" name="PklId" value="<?=$PKL['Id']?>" hidden>
                            <div class="col-sm-4">
                                <select name="1">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Mampu mengidentifikasi permasalahan kimia.</label>
                            <div class="col-sm-4">
                                <select name="2" id="">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Menyusun rencana penyelesaian masalah.</label>
                            <div class="col-sm-4">
                                <select name="3" id="">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="inputNilaiPembimbingLapangMetodedanTerapanMK2" role="dialog" aria-labelledby="exampleModalLabel" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Input Nilai Metode dan Terapan</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="<?= base_url('pkl/NilaiPembimbingLapangMK2') ?>" id="metodedanTerapanPembimbingLapang">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Mahasiswa memahami metode penyelesaian masalah kimia</label>
                            <input type="text" class="form-control" id="inputName" name="PklId" value="<?=$PKL['Id']?>" hidden>
                            <div class="col-sm-4">
                                <select name="1">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Mampu mengidentifikasi permasalahan kimia.</label>
                            <div class="col-sm-4">
                                <select name="2" id="">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Menyusun rencana penyelesaian masalah.</label>
                            <div class="col-sm-4">
                                <select name="3" id="">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="inputNilaiPembimbingLapangMetodedanTerapanMK3" role="dialog" aria-labelledby="exampleModalLabel" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Input Nilai Metode dan Terapan</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="<?= base_url('pkl/NilaiPembimbingLapangMK3') ?>" id="metodedanTerapanPembimbingLapang">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Mahasiswa memahami metode penyelesaian masalah kimia</label>
                            <input type="text" class="form-control" id="inputName" name="PklId" value="<?=$PKL['Id']?>" hidden>
                            <div class="col-sm-4">
                                <select name="1">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Mampu mengidentifikasi permasalahan kimia.</label>
                            <div class="col-sm-4">
                                <select name="2" id="">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Menyusun rencana penyelesaian masalah.</label>
                            <div class="col-sm-4">
                                <select name="3" id="">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- Analisis Penilaian dan Tindakan -->
<div class="modal fade" id="inputNilaiPembimbingLapangAnalisisPenilaiandanTindakanMK1" role="dialog" aria-labelledby="exampleModalLabel" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Input Nilai Analisis, Penilaian, dan Tindakan</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="<?= base_url('pkl/NilaiPembimbingLapangMK1') ?>" id="analisisPenilaiandanTindakanPembimbingLapang">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Memahami prinsip-prinsip pengumpulan data yang valid</label>
                            <input type="text" class="form-control" id="inputName" name="PklId" value="<?=$PKL['Id']?>" hidden>
                            <div class="col-sm-4">
                                <select name="1">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Menganalisis dan menafsirkan data secara kritis</label>
                            <div class="col-sm-4">
                                <select name="2" id="">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Menggunakan kaidah ilmiah dalam penelitian</label>
                            <div class="col-sm-4">
                                <select name="3" id="">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="inputNilaiPembimbingLapangAnalisisPenilaiandanTindakanMK2" role="dialog" aria-labelledby="exampleModalLabel" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Input Nilai Analisis, Penilaian, dan Tindakan</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="<?= base_url('pkl/NilaiPembimbingLapangMK2') ?>" id="analisisPenilaiandanTindakanPembimbingLapang">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Memahami prinsip-prinsip pengumpulan data yang valid</label>
                            <input type="text" class="form-control" id="inputName" name="PklId" value="<?=$PKL['Id']?>" hidden>
                            <div class="col-sm-4">
                                <select name="1">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Menganalisis dan menafsirkan data secara kritis</label>
                            <div class="col-sm-4">
                                <select name="2" id="">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Menggunakan kaidah ilmiah dalam penelitian</label>
                            <div class="col-sm-4">
                                <select name="3" id="">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="inputNilaiPembimbingLapangAnalisisPenilaiandanTindakanMK3" role="dialog" aria-labelledby="exampleModalLabel" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Input Nilai Analisis, Penilaian, dan Tindakan</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="<?= base_url('pkl/NilaiPembimbingLapangMK3') ?>" id="analisisPenilaiandanTindakanPembimbingLapang">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Memahami prinsip-prinsip pengumpulan data yang valid</label>
                            <input type="text" class="form-control" id="inputName" name="PklId" value="<?=$PKL['Id']?>" hidden>
                            <div class="col-sm-4">
                                <select name="1">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Menganalisis dan menafsirkan data secara kritis</label>
                            <div class="col-sm-4">
                                <select name="2" id="">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Menggunakan kaidah ilmiah dalam penelitian</label>
                            <div class="col-sm-4">
                                <select name="3" id="">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- Kemandirian Pengayaan Pengetahuan -->
<div class="modal fade" id="inputNilaiPembimbingLapangKemandirianPengayaanPengetahuanMK1" role="dialog" aria-labelledby="exampleModalLabel" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Input Nilai Kemandirian Pengayaan Pengetahuan</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="<?= base_url('pkl/NilaiPembimbingLapangMK1') ?>" id="kemandirianPengayaanPengetahuanPembimbingLapang">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Mampu menyusun dan melaksanakan rencana penelitian atau proyek mandiri dengan menggunakan metode ilmiah yang tepat</label>
                            <input type="text" class="form-control" id="inputName" name="PklId" value="<?=$PKL['Id']?>" hidden>
                            <div class="col-sm-4">
                                <select name="1">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Mampu mengidentifikasi dan memanfaatkan berbagai sumber informasi untuk mendukung penelitian atau kajian ilmiah secara mandiri</label>
                            <div class="col-sm-4">
                                <select name="2" id="">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Mampu melakukan analisis kritis terhadap berbagai sumber literatur dan data yang relevan</label>
                            <div class="col-sm-4">
                                <select name="3" id="">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="inputNilaiPembimbingLapangKemandirianPengayaanPengetahuanMK2" role="dialog" aria-labelledby="exampleModalLabel" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Input Nilai Kemandirian Pengayaan Pengetahuan</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="<?= base_url('pkl/NilaiPembimbingLapangMK2') ?>" id="kemandirianPengayaanPengetahuanPembimbingLapang">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Mampu menyusun dan melaksanakan rencana penelitian atau proyek mandiri dengan menggunakan metode ilmiah yang tepat</label>
                            <input type="text" class="form-control" id="inputName" name="PklId" value="<?=$PKL['Id']?>" hidden>
                            <div class="col-sm-4">
                                <select name="1">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Mampu mengidentifikasi dan memanfaatkan berbagai sumber informasi untuk mendukung penelitian atau kajian ilmiah secara mandiri</label>
                            <div class="col-sm-4">
                                <select name="2" id="">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Mampu melakukan analisis kritis terhadap berbagai sumber literatur dan data yang relevan</label>
                            <div class="col-sm-4">
                                <select name="3" id="">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="inputNilaiPembimbingLapangKemandirianKemandirianPengayaanPengetahuanMK3" role="dialog" aria-labelledby="exampleModalLabel" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Input Nilai Kemandirian Pengayaan Pengetahuan</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="<?= base_url('pkl/NilaiPembimbingLapangMK3') ?>" id="kemandirianPengayaanPengetahuanPembimbingLapang">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Mampu menyusun dan melaksanakan rencana penelitian atau proyek mandiri dengan menggunakan metode ilmiah yang tepat</label>
                            <input type="text" class="form-control" id="inputName" name="PklId" value="<?=$PKL['Id']?>" hidden>
                            <div class="col-sm-4">
                                <select name="1">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Mampu mengidentifikasi dan memanfaatkan berbagai sumber informasi untuk mendukung penelitian atau kajian ilmiah secara mandiri</label>
                            <div class="col-sm-4">
                                <select name="2" id="">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-8 col-form-label">Mampu melakukan analisis kritis terhadap berbagai sumber literatur dan data yang relevan</label>
                            <div class="col-sm-4">
                                <select name="3" id="">
                                    <option value="0">Tidak tampak</option>
                                    <option value="1">Kurang</option>
                                    <option value="2">Cukup</option>
                                    <option value="3">Baik</option>
                                    <option value="4">Sangat baik</option>
                                </select>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- View Nilai -->
<div class="modal fade" id="viewNilaiPembimbingLapangLPA" role="dialog" aria-labelledby="exampleModalLabel" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">View Nilai Laporan Akhir Program</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Judul</th>
                        <th scope="col">Nilai</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="table-active">
                        <th scope="row"></th>
                        <td>CPMK 12.1</td>
                        <td></td>
                        </tr>
                        <tr>
                        <th scope="row">1</th>
                        <td>Memahami prinsip-prinsip etika profesional dalam bidang kimia</td>
                        <td id="12_1_1"></td>
                        </tr>
                        <tr>
                        <th scope="row">2</th>
                        <td>Ketepatan dan kejujuran</td>
                        <td id="12_1_2"></td>
                        </tr>
                        <tr>
                        <th scope="row">3</th>
                        <td>Pelaporan yang akurat</td>
                        <td id="12_1_3"></td>
                        </tr>
                        <tr>
                        <th scope="row">4</th>
                        <td>Kerjasama dan kontribusi</td>
                        <td id="12_1_4"></td>
                        </tr>
                        <tr class="table-active">
                        <th scope="row"></th>
                        <td>CPMK 12.2</td>
                        <td></td>
                        </tr>
                        <tr>
                        <th scope="row">5</th>
                        <td>Mengenal dan menerapkan standar-standar kimia</td>
                        <td id="12_2_1"></td>
                        </tr>
                        <tr>
                        <th scope="row">6</th>
                        <td>memahami protokol keselamatan laboratorium yang melibatkan penggunaan alat pelindung diri (APD), penanganan bahan kimia berbahaya, serta tindakan darurat dalam kasus insiden.</td>
                        <td id="12_2_2"></td>
                        </tr>
                        <tr>
                        <th scope="row">7</th>
                        <td>memahami bagaimana mengelola, menyimpan, dan membuang bahan kimia berbahaya dengan aman dan sesuai dengan peraturan</td>
                        <td id="12_2_3"></td>
                        </tr>
                        <tr>
                        <th scope="row">8</th>
                        <td>mahir dalam penggunaan peralatan dan instrumen laboratorium yang umum digunakan dalam penelitian kimia</td>
                        <td id="12_2_4"></td>
                        </tr>
                        <tr class="table-active">
                        <th scope="row"></th>
                        <td>CPMK 12.3</td>
                        <td></td>
                        </tr>
                        <tr>
                        <th scope="row">9</th>
                        <td>Bertindak secara etis dalam praktik kimia</td>
                        <td id="12_3_1"></td>
                        </tr>
                        <tr>
                        <th scope="row">10</th>
                        <td>membuat keputusan berdasarkan pertimbangan etika, mematuhi peraturan dan regulasi yang berlaku, serta menjaga integritas dan kerahasiaan data</td>
                        <td id="12_3_2"></td>
                        </tr>
                        <tr class="table-active">
                        <th scope="row"></th>
                        <td>CPMK 12.5</td>
                        <td></td>
                        </tr>
                        <tr>
                        <th scope="row">11</th>
                        <td>Berkomunikasi secara efektif tentang tanggung jawab sosial dan profesional</td>
                        <td id="12_5_1"></td>
                        </tr>
                        <tr>
                        <th scope="row">12</th>
                        <td>dapat menyampaikan informasi tentang prinsip-prinsip etika dan standar-standar kimia kepada rekan kerja, masyarakat, dan pemangku kepentingan lainnya dengan bahasa yang mudah dipahami</td>
                        <td id="12_5_2"></td>
                        </tr>
                        <tr class="table-active">
                        <th scope="row"></th>
                        <td>CPMK 15.1</td>
                        <td></td>
                        </tr>
                        <tr>
                        <th scope="row">13</th>
                        <td>Memahami praktik dan kebutuhan industri/akademik terkait</td>
                        <td id="15_1_1"></td>
                        </tr>
                        <tr>
                        <th scope="row">14</th>
                        <td>Mematuhi dan melaksanakan peraturan dan tata tertib di lingkungan kerja industri atau akademik</td>
                        <td id="15_1_2"></td>
                        </tr>
                        </tr>
                        <tr class="table-active">
                        <th scope="row"></th>
                        <td>CPMK 15.2</td>
                        <td></td>
                        </tr>
                        <tr>
                        <th scope="row">15</th>
                        <td>Mampu menerapkan pengetahuan dan keterampilan dalam situasi nyata</td>
                        <td id="15_2_1"></td>
                        </tr>
                        <tr>
                        <th scope="row">16</th>
                        <td>menerapkan pengetahuan dan keterampilan yang mahasiswa peroleh dari perkuliahan dalam konteks kerja nyata</td>
                        <td id="15_2_2"></td>
                        </tr>
                        </tr>
                        <tr class="table-active">
                        <th scope="row"></th>
                        <td>CPMK 15.3</td>
                        <td></td>
                        </tr>
                        <tr>
                        <th scope="row">17</th>
                        <td>Memiliki keterampilan komunikasi yang efektif</td>
                        <td id="15_3_1"></td>
                        </tr>
                        <tr>
                        <th scope="row">18</th>
                        <td>berkomunikasi dengan rekan kerja, klien, atau sesama akademisi dengan jelas dan efektif</td>
                        <td id="15_3_2"></td>
                        </tr>
                        </tr>
                        <tr class="table-active">
                        <th scope="row"></th>
                        <td>CPMK 15.4</td>
                        <td></td>
                        </tr>
                        <tr>
                        <th scope="row">19</th>
                        <td>Memiliki kemampuan beradaptasi dan fleksibilitas</td>
                        <td id="15_4_1"></td>
                        </tr>
                        <tr>
                        <th scope="row">20</th>
                        <td>beradaptasi dengan perubahan dan tantangan dalam lingkungan kerja industri atau akademik</td>
                        <td id="15_4_2"></td>
                        </tr>
                        </tr>
                        <tr class="table-active">
                        <th scope="row"></th>
                        <td>CPMK 15.5</td>
                        <td></td>
                        </tr>
                        <tr>
                        <th scope="row">21</th>
                        <td>Memiliki pemahaman tentang profesionalisme dan etika kerja</td>
                        <td id="15_5_1"></td>
                        </tr>
                        <tr>
                        <th scope="row">22</th>
                        <td>bersikap profesional, memiliki integritas, menghormati etika kerja, dan bekerjasama dalam lingkungan kerja</td>
                        <td id="15_5_2"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="viewNilaiPembimbingLapangLPB" role="dialog" aria-labelledby="exampleModalLabel" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">View Nilai Laporan Tengah Program</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Judul</th>
                        <th scope="col">Nilai</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="table-active">
                        <th scope="row"></th>
                        <td>CPMK 12.1</td>
                        <td></td>
                        </tr>
                        <tr>
                        <th scope="row">1</th>
                        <td>Memahami prinsip-prinsip etika profesional dalam bidang kimia</td>
                        <td id="12_1_1"></td>
                        </tr>
                        <tr>
                        <th scope="row">2</th>
                        <td>Ketepatan dan kejujuran</td>
                        <td id="12_1_2"></td>
                        </tr>
                        <tr>
                        <th scope="row">3</th>
                        <td>Pelaporan yang akurat</td>
                        <td id="12_1_3"></td>
                        </tr>
                        <tr>
                        <th scope="row">4</th>
                        <td>Kerjasama dan kontribusi</td>
                        <td id="12_1_4"></td>
                        </tr>
                        <tr class="table-active">
                        <th scope="row"></th>
                        <td>CPMK 12.2</td>
                        <td></td>
                        </tr>
                        <tr>
                        <th scope="row">5</th>
                        <td>Mengenal dan menerapkan standar-standar kimia</td>
                        <td id="12_2_1"></td>
                        </tr>
                        <tr>
                        <th scope="row">6</th>
                        <td>memahami protokol keselamatan laboratorium yang melibatkan penggunaan alat pelindung diri (APD), penanganan bahan kimia berbahaya, serta tindakan darurat dalam kasus insiden.</td>
                        <td id="12_2_2"></td>
                        </tr>
                        <tr>
                        <th scope="row">7</th>
                        <td>memahami bagaimana mengelola, menyimpan, dan membuang bahan kimia berbahaya dengan aman dan sesuai dengan peraturan</td>
                        <td id="12_2_3"></td>
                        </tr>
                        <tr>
                        <th scope="row">8</th>
                        <td>mahir dalam penggunaan peralatan dan instrumen laboratorium yang umum digunakan dalam penelitian kimia</td>
                        <td id="12_2_4"></td>
                        </tr>
                        <tr class="table-active">
                        <th scope="row"></th>
                        <td>CPMK 12.3</td>
                        <td></td>
                        </tr>
                        <tr>
                        <th scope="row">9</th>
                        <td>Bertindak secara etis dalam praktik kimia</td>
                        <td id="12_3_1"></td>
                        </tr>
                        <tr>
                        <th scope="row">10</th>
                        <td>membuat keputusan berdasarkan pertimbangan etika, mematuhi peraturan dan regulasi yang berlaku, serta menjaga integritas dan kerahasiaan data</td>
                        <td id="12_3_2"></td>
                        </tr>
                        <tr class="table-active">
                        <th scope="row"></th>
                        <td>CPMK 12.5</td>
                        <td></td>
                        </tr>
                        <tr>
                        <th scope="row">11</th>
                        <td>Berkomunikasi secara efektif tentang tanggung jawab sosial dan profesional</td>
                        <td id="12_5_1"></td>
                        </tr>
                        <tr>
                        <th scope="row">12</th>
                        <td>dapat menyampaikan informasi tentang prinsip-prinsip etika dan standar-standar kimia kepada rekan kerja, masyarakat, dan pemangku kepentingan lainnya dengan bahasa yang mudah dipahami</td>
                        <td id="12_5_2"></td>
                        </tr>
                        <tr class="table-active">
                        <th scope="row"></th>
                        <td>CPMK 15.1</td>
                        <td></td>
                        </tr>
                        <tr>
                        <th scope="row">13</th>
                        <td>Memahami praktik dan kebutuhan industri/akademik terkait</td>
                        <td id="15_1_1"></td>
                        </tr>
                        <tr>
                        <th scope="row">14</th>
                        <td>Mematuhi dan melaksanakan peraturan dan tata tertib di lingkungan kerja industri atau akademik</td>
                        <td id="15_1_2"></td>
                        </tr>
                        </tr>
                        <tr class="table-active">
                        <th scope="row"></th>
                        <td>CPMK 15.2</td>
                        <td></td>
                        </tr>
                        <tr>
                        <th scope="row">15</th>
                        <td>Mampu menerapkan pengetahuan dan keterampilan dalam situasi nyata</td>
                        <td id="15_2_1"></td>
                        </tr>
                        <tr>
                        <th scope="row">16</th>
                        <td>menerapkan pengetahuan dan keterampilan yang mahasiswa peroleh dari perkuliahan dalam konteks kerja nyata</td>
                        <td id="15_2_2"></td>
                        </tr>
                        </tr>
                        <tr class="table-active">
                        <th scope="row"></th>
                        <td>CPMK 15.3</td>
                        <td></td>
                        </tr>
                        <tr>
                        <th scope="row">17</th>
                        <td>Memiliki keterampilan komunikasi yang efektif</td>
                        <td id="15_3_1"></td>
                        </tr>
                        <tr>
                        <th scope="row">18</th>
                        <td>berkomunikasi dengan rekan kerja, klien, atau sesama akademisi dengan jelas dan efektif</td>
                        <td id="15_3_2"></td>
                        </tr>
                        </tr>
                        <tr class="table-active">
                        <th scope="row"></th>
                        <td>CPMK 15.4</td>
                        <td></td>
                        </tr>
                        <tr>
                        <th scope="row">19</th>
                        <td>Memiliki kemampuan beradaptasi dan fleksibilitas</td>
                        <td id="15_4_1"></td>
                        </tr>
                        <tr>
                        <th scope="row">20</th>
                        <td>beradaptasi dengan perubahan dan tantangan dalam lingkungan kerja industri atau akademik</td>
                        <td id="15_4_2"></td>
                        </tr>
                        </tr>
                        <tr class="table-active">
                        <th scope="row"></th>
                        <td>CPMK 15.5</td>
                        <td></td>
                        </tr>
                        <tr>
                        <th scope="row">21</th>
                        <td>Memiliki pemahaman tentang profesionalisme dan etika kerja</td>
                        <td id="15_5_1"></td>
                        </tr>
                        <tr>
                        <th scope="row">22</th>
                        <td>bersikap profesional, memiliki integritas, menghormati etika kerja, dan bekerjasama dalam lingkungan kerja</td>
                        <td id="15_5_2"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="viewNilaiPembimbingLapangSumberDayaAlam" role="dialog" aria-labelledby="exampleModalLabel" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">View Nilai Sumber Daya Alam</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Judul</th>
                        <th scope="col">Nilai</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                        <th scope="row">1</th>
                        <td>Memiliki pengetahuan tentang sumber daya alam Indonesia</td>
                        <td id="1"></td>
                        </tr>
                        <tr>
                        <th scope="row">2</th>
                        <td>Mengenali proses ekstraksi dan pemurnian</td>
                        <td id="2"></td>
                        </tr>
                        <tr>
                        <th scope="row">3</th>
                        <td>Mampu menerapkan konsep kimia dalam pemanfaatan sumber daya alam</td>
                        <td id="3"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="viewNilaiPembimbingLapangKeselamatandanLingkungan" role="dialog" aria-labelledby="exampleModalLabel" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">View Nilai Keselamatan dan Lingkungan</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Judul</th>
                        <th scope="col">Nilai</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                        <th scope="row">1</th>
                        <td>Memahami permasalahan keselamatan dan lingkungan yang relevan dengan bidang keahliannya</td>
                        <td id="1"></td>
                        </tr>
                        <tr>
                        <th scope="row">2</th>
                        <td>Mengenali aspek hukum dan peraturan yang berlaku</td>
                        <td id="2"></td>
                        </tr>
                        <tr>
                        <th scope="row">3</th>
                        <td>Mengetahui langkah-langkah mitigasi dan pengelolaan risiko</td>
                        <td id="3"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="viewNilaiPembimbingLapangMetodedanTerapan" role="dialog" aria-labelledby="exampleModalLabel" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">View Nilai Metode dan Terapan</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Judul</th>
                        <th scope="col">Nilai</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                        <th scope="row">1</th>
                        <td>Memahami permasalahan keselamatan dan lingkungan yang relevan dengan bidang keahliannya</td>
                        <td id="1"></td>
                        </tr>
                        <tr>
                        <th scope="row">2</th>
                        <td>Mengenali aspek hukum dan peraturan yang berlaku</td>
                        <td id="2"></td>
                        </tr>
                        <tr>
                        <th scope="row">3</th>
                        <td>Mengetahui langkah-langkah mitigasi dan pengelolaan risiko</td>
                        <td id="3"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="viewNilaiPembimbingLapangAnalisisPenilaiandanTindakan" role="dialog" aria-labelledby="exampleModalLabel" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">View Nilai Analisis, Penilaian dan Tindakan</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Judul</th>
                        <th scope="col">Nilai</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                        <th scope="row">1</th>
                        <td>Memahami prinsip-prinsip pengumpulan data yang valid</td>
                        <td id="1"></td>
                        </tr>
                        <tr>
                        <th scope="row">2</th>
                        <td>Menganalisis dan menafsirkan data secara kritis</td>
                        <td id="2"></td>
                        </tr>
                        <tr>
                        <th scope="row">3</th>
                        <td>Menggunakan kaidah ilmiah dalam penelitian</td>
                        <td id="3"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="viewNilaiPembimbingLapangKemandirianPengayaanPengetahuan" role="dialog" aria-labelledby="exampleModalLabel" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">View Nilai Kemandirian Pengayaan Pengetahuan</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Judul</th>
                        <th scope="col">Nilai</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                        <th scope="row">1</th>
                        <td>Mampu menyusun dan melaksanakan rencana penelitian atau proyek mandiri dengan menggunakan metode ilmiah yang tepat</td>
                        <td id="1"></td>
                        </tr>
                        <tr>
                        <th scope="row">2</th>
                        <td>Mampu mengidentifikasi dan memanfaatkan berbagai sumber informasi untuk mendukung penelitian atau kajian ilmiah secara mandiri</td>
                        <td id="2"></td>
                        </tr>
                        <tr>
                        <th scope="row">3</th>
                        <td>Mampu melakukan analisis kritis terhadap berbagai sumber literatur dan data yang relevan</td>
                        <td id="3"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>

<!-- End of Main Content -->