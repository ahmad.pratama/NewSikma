<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Card Ruangan Kalender -->
    <div class="card">
        <div class="row mt-3 ml-2 mr-2">
            <div class="col-4">
                <h4 class="text-white bg-dark">Kalender Ruangan</h4>
            </div>
            <div class="col-6">
            </div>
            <div class="col-2">
                <button class="btn btn-warning" data-toggle="modal" data-target="#tambahAgenda">Tambah Agenda Ruangan</button>
            </div>
        </div>
        <div class="card-body text-center">
            <div class="row">
                <div class="d-flex justify-content-between mb-3 mx-auto">
                    <h3 class="text-center mb-3"><?= $Date; ?></h3>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="table-container" style="overflow-x: auto;">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <th>Waktu</th>
                                <?php foreach ($Ruangan as $ruang): ?>
                                <th id=<?= $ruang['Id'] ?>><?= $ruang['Nama'] ?></th>
                                <?php endforeach; ?>
                            </thead>
                            <tbody>
                                <?php
                                $start_time = strtotime('06:00');
                                $end_time = strtotime('21:00');

                                for ($current_time = $start_time; $current_time <= $end_time; $current_time += 3600) {
                                    $formatted_time = date('H:i', $current_time);
                                    ?>
                                    <tr>
                                        <td><?= $formatted_time ?></td>
                                        <?php foreach ($Ruangan as $ruang): ?>
                                                <?php
                                                // Check if there is an event at the current time and room
                                                $event_data = null;

                                                // Check UjianPKL
                                                if (!$event_data) {
                                                    foreach ($UjianPKL as $event) {
                                                        if (
                                                            $event['RuangPKL'] == $ruang['Id'] &&
                                                            strtotime($event['MulaiPkl']) <= $current_time &&
                                                            strtotime($event['SelesaiPkl']) >= $current_time
                                                        ) {
                                                            $event_data = $event;
                                                            $event_data['type'] = 1;
                                                            break;
                                                        }
                                                    }
                                                }

                                                // Check Sempro
                                                if (!$event_data) {
                                                    foreach ($Sempro as $event) {
                                                        if (
                                                            $event['RuangSempro'] == $ruang['Id'] &&
                                                            strtotime($event['MulaiSempro']) <= $current_time &&
                                                            strtotime($event['SelesaiSempro']) >= $current_time
                                                        ) {
                                                            $event_data = $event;
                                                            $event_data['type'] = 2;
                                                            break;
                                                        }
                                                    }
                                                }

                                                // Check Semju
                                                if (!$event_data) {
                                                    foreach ($Semju as $event) {
                                                        if (
                                                            $event['RuangSemju'] == $ruang['Id'] &&
                                                            strtotime($event['MulaiSemju']) <= $current_time &&
                                                            strtotime($event['SelesaiSemju']) >= $current_time
                                                        ) {
                                                            $event_data = $event;
                                                            $event_data['type'] = 3;
                                                            break;
                                                        }
                                                    }
                                                }

                                                // Check Skripsi
                                                if (!$event_data) {
                                                    foreach ($Skripsi as $event) {
                                                        if (
                                                            $event['RuangSkripsi'] == $ruang['Id'] &&
                                                            strtotime($event['MulaiSkripsi']) <= $current_time &&
                                                            strtotime($event['SelesaiSkripsi']) >= $current_time
                                                        ) {
                                                            $event_data = $event;
                                                            $event_data['type'] = 4;
                                                            break;
                                                        }
                                                    }
                                                }

                                                // Check Kegiataa
                                                if (!$event_data) {
                                                    foreach ($AgendaRuangan as $event) {
                                                        if (
                                                            $event['RuangKegiatan'] == $ruang['Id'] &&
                                                            strtotime($event['MulaiKegiatan']) <= $current_time &&
                                                            strtotime($event['SelesaiKegiatan']) >= $current_time
                                                        ) {
                                                            $event_data = $event;
                                                            $event_data['type'] = 5;
                                                            break;
                                                        }
                                                    }
                                                }

                                                // Display event data if available
                                                if ($event_data) {
                                                    echo '<td class="table-primary">';
                                                    switch ($event_data['type']) {
                                                        case 1:
                                                            echo "Ujian PKL ";
                                                            echo $event_data['NamaMhsUjianPkl'];
                                                            break;
                                                        case 2:
                                                            echo "Seminar Proposal ";
                                                            echo $event_data['NamaMhsSempro'];
                                                            break;
                                                        case 3:
                                                            echo "Seminar Kemajuan ";
                                                            echo $event_data['NamaMhsSemju'];
                                                            break;
                                                        case 4:
                                                            echo "Ujian Tugas Akhir ";
                                                            echo $event_data['NamaMhsSkripsi'];
                                                            break;
                                                        case 5:
                                                            echo "Kegiatan ";
                                                            echo $event_data['NamaKegiatan'];
                                                            break;
                                                    }
                                                    echo '</td>';
                                                } else {
                                                    echo '<td></td>';
                                                }
                                            ?>
                                        <?php endforeach; ?>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

    <!-- Tambah Agenda Modal-->
    <div class="modal fade" id="tambahAgenda"  role="dialog" aria-labelledby="exampleModalLabel"
        >
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Staff</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" action="<?= base_url('agenda/TambahAgenda'); ?>" >
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 col-form-label">Nama Kegiatan</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputName" name="date" value="<?=$Date?>" hidden>
                        <input type="text" class="form-control" id="inputName" name="namaKegiatan" placeholder="Nama Kegiatan" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputtimeSelect" class="col-sm-4 col-form-label">Jam Mulai</label>
                        <div class="col-sm-8">
                        <select id="JamMulai" name="jamMulai" style="width: 100%;" required></select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputtimeSelect" class="col-sm-4 col-form-label">Jam Selesai</label>
                        <div class="col-sm-8">
                        <select id="JamSelesai" name="jamSelesai" style="width: 100%;" required></select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputRuangan" class="col-sm-4 col-form-label">Ruangan</label>
                        <div class="col-sm-8">
                            <select id="Ruangan" name="ruangId" required>
                                <option value="">Select an option</option>
                                <?php foreach ($Ruangan as $room): ?>
                                    <option value="<?= $room['Id'] ?>"><?= $room['Nama'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                    <button class="btn btn-primary" type="submit">Tambah</button>
                </div>
                </form>
            </div>
        </div>
    </div>
<!-- End of Main Content -->