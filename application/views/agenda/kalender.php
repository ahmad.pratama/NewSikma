<!-- Begin Page Content -->
<div class="container-fluid">
        
    <!-- card Kalender -->
    <div class="card">
        <div class="row mt-3 ml-2 mr-2">
            <div class="col-2">
                <h4 class="text-white bg-dark">Kalender</h4>
            </div>
            <div class="col-8">
            </div>
            <div class="col-2">
            </div>
        </div>
        <div class="card-body text-center">
            <div class="row">
                <div class="d-flex justify-content-between mb-3 mx-auto">
                    <button class="btn btn-light" onclick="previousMonth()"><i class="fa-solid fa-angles-left"></i></button>
                    <h3 class="text-center mb-3" id="currentMonthYear"></h3>
                    <button class="btn btn-light" onclick="nextMonth()"><i class="fa-solid fa-angles-right"></i></button>
                </div>
            </div>
            <div class="row">
                <div class="col-8 mx-auto">
                    <div id="calendar"></div>
                </div>
            </div>
        </div>
    </div>

</div>

<!-- End of Main Content -->