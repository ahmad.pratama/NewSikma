<!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?=base_url('user');?>">
        <div class="sidebar-brand-icon">
            <i class="fa-solid fa-flask-vial"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Sikma Admin</div>
    </a>

    <!-- General Menu -->
    <!-- <?php if($title == $sm['Title']): ?>
        <li class="nav-item active">
    <?php else: ?>
        <li class="nav-item">
    <?php endif; ?> -->

    <?php 
        $admin = [0,5]; 
        $mhs = [2,3,4];
        $RoleId = $this->session->userdata('RoleId');
    ?>
    
    <li class="nav-item">
        <a class="nav-link pb-0" href="<?=base_url('user');?>">
        <i class="fa-solid fa-fw fa-house"></i>
        <span>Dashboard</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link pb-0" href="<?=base_url('sdm');?>">
        <i class="fa-solid fa-fw fa-people-group"></i>
        <span>Data SDM</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link pb-0" href="#">
        <i class="fa-solid fa-fw fa-user-plus"></i>
        <span>Pendaftaran Fast Track</span></a>
    </li>
    <?php 
        if (in_array($RoleId, $admin)):
    ?>
    <li class="nav-item">
        <a class="nav-link pb-0" href="<?=base_url('akademik')?>">
        <i class="fa-solid fa-fw fa-laptop"></i>
        <span>Akademik</span></a>
    </li>
    <?php endif; ?>
    
    <?php 
    if((in_array($RoleId, [0,6])) || (in_array(true, [
        $this->session->userdata('Kadep'),
        $this->session->userdata('Sekdep'),
        $this->session->userdata('Kalab')]) || (in_array($RoleId, $mhs))
        )):
    ?>
    <li class="nav-item">
        <a class="nav-link collapsed pb-0" href="#" data-toggle="collapse" data-target="#collapseTwo"
            aria-expanded="true" aria-controls="collapseTwo">
            <i class="fa-solid fa-fw fa-microscope"></i>
            <span>Data Laboratorium</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <?php if(!in_array($RoleId, $mhs)) :?>
                <a class="collapse-item" href="<?= base_url('laboratorium/DataAlat');?>">
                    <i class="fa-solid fa-fw fa-screwdriver-wrench"></i>
                    <span>Data Alat</span>
                </a>
                <a class="collapse-item" href="<?= base_url('laboratorium/DataBahan');?>">
                    <i class="fa-solid fa-fw fa-fire"></i>
                    <span>Data Bahan</span>
                </a>
                <?php endif ?>
                <a class="collapse-item" href="<?= base_url('laboratorium/PeminjamanAlat') ?>">
                    <i class="fa-regular fa-fw fa-clipboard"></i>
                    <span>Bon Alat</span>
                </a>
                <a class="collapse-item" href="<?= base_url('laboratorium/PeminjamanBahan') ?>">
                    <i class="fa-solid fa-fw fa-clipboard"></i>
                    <span>Bon Bahan</span>
                </a>
                <a class="collapse-item" href="">
                    <i class="fa-solid fa-fw fa-vial"></i>
                    <span>Praktikum</span>
                </a>
            </div>
        </div>
    </li>
    <?php endif; ?>

    <li class="nav-item">
        <a class="nav-link pb-0" href="#">
        <i class="fa-solid fa-fw fa-magnifying-glass-chart"></i>
        <span>Analisa</span></a>
    </li>

    <li class="nav-item">
        <a class="nav-link pb-0" href="<?= base_url('agenda');?>">
        <i class="fa-solid fa-fw fa-calendar-days"></i>
        <span>Agenda</span></a>
    </li>

    <li class="nav-item">
        <a class="nav-link pb-0" href="#">
        <i class="fa-solid fa-fw fa-circle-question"></i>
        <span>Kuisioner</span></a>
    </li>

    <li class="nav-item">
        <a class="nav-link pb-0" href="<?= base_url('surat'); ?>">
        <i class="fa-solid fa-fw fa-envelope-open-text"></i>
        <span>Permohonan Surat</span></a>
    </li>

    <?php 
    $s2s3 = [3,4];
    if(!in_array($RoleId,$s2s3)):
    ?>
    <li class="nav-item">
        <a class="nav-link pb-0" href="<?= base_url('pkl'); ?>">
        <i class="fa-solid fa-fw fa-briefcase"></i>
        <span>KKN / PKL</span></a>
    </li>

    <li class="nav-item">
        <a class="nav-link pb-0" href="<?= base_url('tugasakhir'); ?>">
        <i class="fa-solid fa-fw fa-graduation-cap"></i>
        <span>Tugas Akhir</span></a>
    </li>
    <?php endif; ?>

    <li class="nav-item">
        <a class="nav-link pb-0" href="#">
        <i class="fa-solid fa-fw fa-school"></i>
        <span>Tracer Study</span></a>
    </li>

    <li class="nav-item">
        <a class="nav-link pb-0" href="#">
        <i class="fa-solid fa-fw fa-book"></i>
        <span>Dokumen</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Nav Item - Logout -->
    <li class="nav-item">
        <a class="nav-link" data-toggle="modal" data-target="#logoutModal">
            <i class="fa-solid fa-right-from-bracket"></i>
            <span>Log Out</span></a>
    </li>

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

    </ul>
<!-- End of Sidebar -->
