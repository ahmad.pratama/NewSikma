
            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; Sikma Kimia <?= date('Y') ?></span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal"  role="dialog" aria-labelledby="exampleModalLabel"
        >
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                    <a class="btn btn-primary" href="<?= base_url('auth/logout')?>">Logout</a>
                </div>
            </div>
        </div>
    </div>
    

    <!-- Bootstrap core JavaScript-->
    <script src="<?= base_url('assets/'); ?>vendor/jquery/jquery.min.js"></script>
    <script src="<?= base_url('assets/'); ?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="<?= base_url('assets/'); ?>vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="<?= base_url('assets/'); ?>js/sb-admin-2.min.js"></script>
    
    <!-- Charts -->
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    
    <!-- Select2 -->
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

    <!-- Edit Peminjaman List Bahan -->
    <script>
        $(document).ready(function (){
            $('.editPeminjamanListBahan').click(function (e) {
                e.preventDefault();

                var ListBahanId = $(this).closest('tr').find('.ListBahanId').text();
                
                console.log(ListBahanId);
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('laboratorium/DetailPeminjamanListBahan'); ?>",
                    data: {
                        'ListBahanId': ListBahanId,
                    },
                    success: function (response) {
                        response = JSON.parse(response);
                        console.log(response);

                        if (response != null){

                            $("#editPeminjamanListBahan [name=\"listBahanId\"]").val(response.Id);
                            $("#editPeminjamanListBahan [name=\"peminjamanId\"]").val(response.PeminjamanId);
                            $("#editPeminjamanListBahan [name=\"bahanLabId\"]").val(response.BahanLabId);
                            $("#editPeminjamanListBahan [name=\"namaBahan\"]").val(response.NamaBahan);
                            $("#editPeminjamanListBahan [name=\"namaLab\"]").val(response.NamaLab);
                            $("#editPeminjamanListBahan [name=\"jumlah\"]").val(response.Jumlah);
                            $("#editPeminjamanListBahan [name=\"status\"]").val(response.Status);
                        }

                        $('#editPeminjamanListBahan').modal('show');
                    }
                })
            })
        });
    </script>

    <!-- Select 2 List Bahan Ketika Pengajuan Awal -->
    <script>
        $(document).ready(function () {
            // Initialize Select2 on both select elements
            $('.select2').select2();

            // Add change event listener for the 'alat' select
            $('#dynamicTable').on('change', '.bahan-dropdown', function () {
                var selectedBahanId = $(this).val();
                var bahanLabId = $(this).find('option:selected').data('bahan-lab-id');
                var labDropdown = $(this).closest('tr').find('.lab-dropdown');
                var stokBahanInput = $(this).closest('tr').find('input[name="stokBahan[]"]');
                labDropdown.html('');

                <?php foreach ($ListBahanLab as $item): ?>
                    if ('<?php echo $item['BahanId']; ?>' === selectedBahanId) {
                        var option = new Option('<?php echo $item['NamaLab']; ?>', '<?php echo $item['LabId']; ?>', true, true);
                        labDropdown.append(option).trigger('change');

                        // Set the value of the stokBahan input
                        stokBahanInput.val('<?php echo $item['Stok']; ?>');
                    }
                <?php endforeach; ?>

                // // Set the value of the hidden input with BahanLabId
                // $(this).closest('tr').find('input[name="BahanLabId[]"]').val(BahanLabId);
            });

            // Add change event listener for the 'lab' select
            $('#dynamicTable').on('change', '.lab-dropdown', function () {
                var selectedLabId = $(this).val();
                var selectedBahanId = $(this).closest('tr').find('.bahan-dropdown').val();
                var stokBahanInput = $(this).closest('tr').find('input[name="stokBahan[]"]');
                var bahanLabIdInput = $(this).closest('tr').find('input[name="bahanLabId[]"]');

                // Update the 'Stok' value based on the selected Lab
                <?php foreach ($ListBahanLab as $item): ?>
                    if ('<?php echo $item['BahanId']; ?>' === selectedBahanId && '<?php echo $item['LabId']; ?>' === selectedLabId) {
                        stokBahanInput.val('<?php echo $item['Stok']; ?>');
                        bahanLabIdInput.val('<?php echo $item['BahanLabId']; ?>'); // Set the correct BahanLabId
                    }
                <?php endforeach; ?>
            });

            // Add row on button click
            $('#dynamicTable').on('click', '.add-row', function () {
                var newRow = $('<tr>');
                var nomor = $('#dynamicTable tbody tr').length + 1;

                newRow.append('<th scope="row" class="nomor">' + nomor + '</th>');
                newRow.append('<td class="select-bahan"><select name="bahan[]" class="form-control select2 bahan-dropdown" required>' + $('.bahan-dropdown').html() + '</select></td>');
                newRow.append('<td class="select-lab"><select name="lab[]" class="form-control select2 lab-dropdown" required></select></td>');
                newRow.append('<td class="stokBahan"><input type="text" name="stokBahan[]" class="form-control" disabled></td>');
                newRow.append('<td class="stok"><input type="text" name="stok[]" class="form-control" required></td>');
                // newRow.append('<td><button type="button" class="btn btn-success add-row"><i class="fas fa-plus"></i></button></td>');
                newRow.append('<td><button type="button" class="btn btn-danger remove-row"><i class="fas fa-trash"></i></button></td>');
                // Hidden input for BahanLabId
                newRow.append('<input type="hidden" name="bahanLabId[]">');

                // Enable the button for the first row
                if (nomor === 1) {
                    newRow.find('.add-row').attr('disabled', false);
                }

                $('#dynamicTable tbody').append(newRow);

                // Initialize Select2 for the newly added row
                newRow.find('.select2').select2();
            });

            $('#dynamicTable').on('click', '.remove-row', function () {
                var currentRow = $(this).closest('tr');
                currentRow.remove();

                // Update the row numbers
                $('#dynamicTable tbody tr').each(function (index) {
                    $(this).find('.nomor').text(index + 1);
                });
            });
        });
    </script>

    <!-- Tujuan Peminjaman Bahan -->
    <script>
        $(document).ready(function () {
            // Function to toggle the display of CardTujuanPeminjamanBahanLainnya based on TujuanPeminjamanBahan value
            function toggleCardTujuanPeminjamanBahanLainnya() {
                var selectedValue = $("#TujuanPeminjamanBahan").val();
                if (selectedValue === "8") {
                    $("#CardTujuanPeminjamanBahanLainnya").show();
                } else {
                    $("#CardTujuanPeminjamanBahanLainnya").hide();
                }
            }

            // Initial check on page load
            toggleCardTujuanPeminjamanBahanLainnya();

            // Event listener for change in TujuanPeminjamanBahan dropdown
            $("#TujuanPeminjamanBahan").change(function () {
                toggleCardTujuanPeminjamanBahanLainnya();
            });
        });
    </script>
    
    <!-- Edit Stok Bahan -->
    <script>
        $(document).ready(function (){
            $('.editStokBahan').click(function (e) {
                e.preventDefault();

                var StokId = $(this).closest('tr').find('.StokId').text();
                
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('laboratorium/DetailStokBahanLab'); ?>",
                    data: {
                        'StokId': StokId,
                    },
                    success: function (response) {
                        response = JSON.parse(response);
                        console.log(response);

                        if (response != null){

                            $("#editStokBahan [name=\"pengadaanId\"]").val(response.PengadaanId);
                            $("#editStokBahan [name=\"stokId\"]").val(response.StokId);
                            $("#editStokBahan [name=\"namaBahan\"]").val(response.NamaBahan);
                            $("#editStokBahan [name=\"keterangan\"]").val(response.Keterangan);
                            $("#editStokBahan [name=\"jumlah\"]").val(response.JumlahPerubah);
                        }

                        $('#editStokBahan').modal('show');
                    }
                })
            })
        });
    </script>

    <!-- Detail Stok Bahan -->
    <script>
        $(document).ready(function (){
            $('.detailStokBahan').click(function (e) {
                e.preventDefault();

                var StokId = $(this).closest('tr').find('.StokId').text();
                
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('laboratorium/DetailStokBahanLab'); ?>",
                    data: {
                        'StokId': StokId,
                    },
                    success: function (response) {
                        response = JSON.parse(response);
                        console.log(response);

                        if (response != null){
                            $("#detailStokBahan [name=\"namaBahan\"]").val(response.NamaBahan);
                            $("#detailStokBahan [name=\"keterangan\"]").val(response.Keterangan);
                            $("#detailStokBahan [name=\"jumlahAwal\"]").val(response.JumlahAwal);
                            $("#detailStokBahan [name=\"jumlahPerubah\"]").val(response.JumlahPerubah);
                            // $("#detailStokBahan [name=\"totalStok\"]").val(response.TotalStok);
                            $("#detailStokBahan [name=\"tanggal\"]").val(response.Tanggal);
                        }

                        $('#detailStokBahan').modal('show');
                    }
                })
            })
        });
    </script>

    <!-- Edit Bahan Lab -->
    <script>
        $(document).ready(function (){
            $('.editBahanLab').click(function (e) {
                e.preventDefault();

                var BahanLabId = $(this).closest('tr').find('.BahanLabId').text();
                
                console.log(BahanLabId);
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('laboratorium/DetailBahanLab'); ?>",
                    data: {
                        'BahanLabId': BahanLabId,
                    },
                    success: function (response) {
                        response = JSON.parse(response);
                        console.log(response);

                        if (response != null){

                            $("#editBahanLab [name=\"bahanLabId\"]").val(response.Id);
                            $("#editBahanLab [name=\"labId\"]").val(response.LabId);
                            $("#editBahanLab [name=\"nama\"]").val(response.NamaBahan);
                            $("#editBahanLab [name=\"status\"]").val(response.Status);
                        }

                        $('#editBahanLab').modal('show');
                    }
                })
            })
        });
    </script>

    <!-- Edit Master Bahan -->
    <script>
        $(document).ready(function (){
            $('.editMasterBahan').click(function (e) {
                e.preventDefault();

                var BahanId = $(this).closest('tr').find('.BahanId').text();
                
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('laboratorium/DetailMasterBahan'); ?>",
                    data: {
                        'BahanId': BahanId,
                    },
                    success: function (response) {
                        response = JSON.parse(response);
                        console.log(response);

                        if (response != null){

                            $("#editMasterBahan [name=\"bahanId\"]").val(response.Id);
                            $("#editMasterBahan [name=\"nama\"]").val(response.Nama);
                            $("#editMasterBahan [name=\"merk\"]").val(response.Merk);
                            $("#editMasterBahan [name=\"status\"]").val(response.Status);
                        }

                        $('#editMasterBahan').modal('show');
                    }
                })
            })
        });
    </script>

    <!-- Edit Peminjaman List Alat -->
    <script>
        $(document).ready(function (){
            $('.editPeminjamanListAlat').click(function (e) {
                e.preventDefault();

                var ListAlatId = $(this).closest('tr').find('.ListAlatId').text();
                
                console.log(ListAlatId);
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('laboratorium/DetailPeminjamanListAlat'); ?>",
                    data: {
                        'ListAlatId': ListAlatId,
                    },
                    success: function (response) {
                        response = JSON.parse(response);
                        console.log(response);

                        if (response != null){

                            $("#editPeminjamanListAlat [name=\"listAlatId\"]").val(response.Id);
                            $("#editPeminjamanListAlat [name=\"peminjamanId\"]").val(response.PeminjamanId);
                            $("#editPeminjamanListAlat [name=\"alatLabId\"]").val(response.AlatLabId);
                            $("#editPeminjamanListAlat [name=\"namaAlat\"]").val(response.NamaAlat);
                            $("#editPeminjamanListAlat [name=\"namaLab\"]").val(response.NamaLab);
                            $("#editPeminjamanListAlat [name=\"jumlah\"]").val(response.Jumlah);
                            $("#editPeminjamanListAlat [name=\"status\"]").val(response.Status);
                        }

                        $('#editPeminjamanListAlat').modal('show');
                    }
                })
            })
        });
    </script>

    <!-- Select 2 List Alat Ketika Pengajuan Awal -->
    <!-- <script>
        $(document).ready(function () {
            // Initialize Select2 on both select elements
            $('.select2').select2();

            // Add change event listener for the 'alat' select
            $('#dynamicTable').on('change', '.alat-dropdown', function () {
                var selectedAlatId = $(this).val();
                var alatLabId = $(this).find('option:selected').data('alat-lab-id');
                var labDropdown = $(this).closest('tr').find('.lab-dropdown');
                var stokAlatInput = $(this).closest('tr').find('input[name="stokAlat[]"]');
                labDropdown.html('');

                <?php foreach ($ListAlatLab as $item): ?>
                    if ('<?php echo $item['AlatId']; ?>' === selectedAlatId) {
                        var option = new Option('<?php echo $item['NamaLab']; ?>', '<?php echo $item['LabId']; ?>', true, true);
                        labDropdown.append(option).trigger('change');

                        // Set the value of the stokAlat input
                        stokAlatInput.val('<?php echo $item['Stok']; ?>');
                    }
                <?php endforeach; ?>

                // Set the value of the hidden input with alatLabId
                $(this).closest('tr').find('input[name="alatLabId[]"]').val(alatLabId);
            });

            // Add change event listener for the 'lab' select
            $('#dynamicTable').on('change', '.lab-dropdown', function () {
                var selectedLabId = $(this).val();
                var stokAlatInput = $(this).closest('tr').find('input[name="stokAlat[]"]');

                // Update the 'Stok' value based on the selected Lab
                <?php foreach ($ListAlatLab as $item): ?>
                    if ('<?php echo $item['LabId']; ?>' === selectedLabId) {
                        stokAlatInput.val('<?php echo $item['Stok']; ?>');
                    }
                <?php endforeach; ?>
            });

            // Add row on button click
            $('#dynamicTable').on('click', '.add-row', function () {
                var newRow = $('<tr>');
                var nomor = $('#dynamicTable tbody tr').length + 1;

                newRow.append('<th scope="row" class="nomor">' + nomor + '</th>');
                newRow.append('<td class="select-alat"><select name="alat[]" class="form-control select2 alat-dropdown">' + $('.alat-dropdown').html() + '</select></td>');
                newRow.append('<td class="select-lab"><select name="lab[]" class="form-control select2 lab-dropdown"></select></td>');
                newRow.append('<td class="stokAlat"><input type="text" name="stokAlat[]" class="form-control" disabled></td>');
                newRow.append('<td class="stok"><input type="text" name="stok[]" class="form-control"></td>');
                // newRow.append('<td><button type="button" class="btn btn-success add-row"><i class="fas fa-plus"></i></button></td>');
                newRow.append('<td><button type="button" class="btn btn-danger remove-row"><i class="fas fa-trash"></i></button></td>');
                // Hidden input for alatLabId
                newRow.append('<input type="hidden" name="alatLabId[]">');

                // Enable the button for the first row
                if (nomor === 1) {
                    newRow.find('.add-row').attr('disabled', false);
                }

                $('#dynamicTable tbody').append(newRow);

                // Initialize Select2 for the newly added row
                newRow.find('.select2').select2();
            });

            $('#dynamicTable').on('click', '.remove-row', function () {
                var currentRow = $(this).closest('tr');
                currentRow.remove();

                // Update the row numbers
                $('#dynamicTable tbody tr').each(function (index) {
                    $(this).find('.nomor').text(index + 1);
                });
            });
        });
    </script> -->
    <script>
        $(document).ready(function () {
            // Initialize Select2 on both select elements
            $('.select2').select2();

            // Add change event listener for the 'alat' select
            $('#dynamicTable').on('change', '.alat-dropdown', function () {
                var selectedAlatId = $(this).val();
                var alatLabId = $(this).find('option:selected').data('alat-lab-id');
                var labDropdown = $(this).closest('tr').find('.lab-dropdown');
                var stokAlatInput = $(this).closest('tr').find('input[name="stokAlat[]"]');
                labDropdown.html('');

                <?php foreach ($ListAlatLab as $item): ?>
                    if ('<?php echo $item['AlatId']; ?>' === selectedAlatId) {
                        var option = new Option('<?php echo $item['NamaLab']; ?>', '<?php echo $item['LabId']; ?>', true, true);
                        labDropdown.append(option).trigger('change');

                        // Set the value of the stokAlat input
                        stokAlatInput.val('<?php echo $item['Stok']; ?>');
                    }
                <?php endforeach; ?>

                // // Set the value of the hidden input with alatLabId
                // $(this).closest('tr').find('input[name="alatLabId[]"]').val(alatLabId);
            });

            // Add change event listener for the 'lab' select
            $('#dynamicTable').on('change', '.lab-dropdown', function () {
                var selectedLabId = $(this).val();
                var selectedAlatId = $(this).closest('tr').find('.alat-dropdown').val();
                var stokAlatInput = $(this).closest('tr').find('input[name="stokAlat[]"]');
                var alatLabIdInput = $(this).closest('tr').find('input[name="alatLabId[]"]');

                // Update the 'Stok' value based on the selected Lab
                <?php foreach ($ListAlatLab as $item): ?>
                    if ('<?php echo $item['AlatId']; ?>' === selectedAlatId && '<?php echo $item['LabId']; ?>' === selectedLabId) {
                        stokAlatInput.val('<?php echo $item['Stok']; ?>');
                        alatLabIdInput.val('<?php echo $item['AlatLabId']; ?>'); // Set the correct AlatLabId
                    }
                <?php endforeach; ?>
            });

            // Add row on button click
            $('#dynamicTable').on('click', '.add-row', function () {
                var newRow = $('<tr>');
                var nomor = $('#dynamicTable tbody tr').length + 1;

                newRow.append('<th scope="row" class="nomor">' + nomor + '</th>');
                newRow.append('<td class="select-alat"><select name="alat[]" class="form-control select2 alat-dropdown" required>' + $('.alat-dropdown').html() + '</select></td>');
                newRow.append('<td class="select-lab"><select name="lab[]" class="form-control select2 lab-dropdown" required></select></td>');
                newRow.append('<td class="stokAlat"><input type="text" name="stokAlat[]" class="form-control" disabled></td>');
                newRow.append('<td class="stok"><input type="text" name="stok[]" class="form-control" required></td>');
                // newRow.append('<td><button type="button" class="btn btn-success add-row"><i class="fas fa-plus"></i></button></td>');
                newRow.append('<td><button type="button" class="btn btn-danger remove-row"><i class="fas fa-trash"></i></button></td>');
                // Hidden input for alatLabId
                newRow.append('<input type="hidden" name="alatLabId[]">');

                // Enable the button for the first row
                if (nomor === 1) {
                    newRow.find('.add-row').attr('disabled', false);
                }

                $('#dynamicTable tbody').append(newRow);

                // Initialize Select2 for the newly added row
                newRow.find('.select2').select2();
            });

            $('#dynamicTable').on('click', '.remove-row', function () {
                var currentRow = $(this).closest('tr');
                currentRow.remove();

                // Update the row numbers
                $('#dynamicTable tbody tr').each(function (index) {
                    $(this).find('.nomor').text(index + 1);
                });
            });
        });
    </script>

    <!-- Tujuan Peminjaman Alat -->
    <script>
        $(document).ready(function () {
            // Function to toggle the display of CardTujuanPeminjamanAlatLainnya based on TujuanPeminjamanAlat value
            function toggleCardTujuanPeminjamanAlatLainnya() {
                var selectedValue = $("#TujuanPeminjamanAlat").val();
                if (selectedValue === "8") {
                    $("#CardTujuanPeminjamanAlatLainnya").show();
                } else {
                    $("#CardTujuanPeminjamanAlatLainnya").hide();
                }
            }

            // Initial check on page load
            toggleCardTujuanPeminjamanAlatLainnya();

            // Event listener for change in TujuanPeminjamanAlat dropdown
            $("#TujuanPeminjamanAlat").change(function () {
                toggleCardTujuanPeminjamanAlatLainnya();
            });
        });
    </script>

    <!-- Edit Stok Alat -->
    <script>
        $(document).ready(function (){
            $('.editStokAlat').click(function (e) {
                e.preventDefault();

                var StokId = $(this).closest('tr').find('.StokId').text();
                
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('laboratorium/DetailStokAlatLab'); ?>",
                    data: {
                        'StokId': StokId,
                    },
                    success: function (response) {
                        response = JSON.parse(response);
                        console.log(response);

                        if (response != null){

                            $("#editStokAlat [name=\"pengadaanId\"]").val(response.PengadaanId);
                            $("#editStokAlat [name=\"stokId\"]").val(response.StokId);
                            $("#editStokAlat [name=\"namaAlat\"]").val(response.NamaAlat);
                            $("#editStokAlat [name=\"keterangan\"]").val(response.Keterangan);
                            $("#editStokAlat [name=\"jumlah\"]").val(response.JumlahPerubah);
                        }

                        $('#editStokAlat').modal('show');
                    }
                })
            })
        });
    </script>

    <!-- Detail Stok Alat -->
    <script>
        $(document).ready(function (){
            $('.detailStokAlat').click(function (e) {
                e.preventDefault();

                var StokId = $(this).closest('tr').find('.StokId').text();
                
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('laboratorium/DetailStokAlatLab'); ?>",
                    data: {
                        'StokId': StokId,
                    },
                    success: function (response) {
                        response = JSON.parse(response);
                        console.log(response);

                        if (response != null){
                            $("#detailStokAlat [name=\"namaAlat\"]").val(response.NamaAlat);
                            $("#detailStokAlat [name=\"keterangan\"]").val(response.Keterangan);
                            $("#detailStokAlat [name=\"jumlahAwal\"]").val(response.JumlahAwal);
                            $("#detailStokAlat [name=\"jumlahPerubah\"]").val(response.JumlahPerubah);
                            // $("#detailStokAlat [name=\"totalStok\"]").val(response.TotalStok);
                            $("#detailStokAlat [name=\"tanggal\"]").val(response.Tanggal);
                        }

                        $('#detailStokAlat').modal('show');
                    }
                })
            })
        });
    </script>

    <!-- Edit Alat Lab -->
    <script>
        $(document).ready(function (){
            $('.editAlatLab').click(function (e) {
                e.preventDefault();

                var AlatLabId = $(this).closest('tr').find('.AlatLabId').text();
                
                console.log(AlatLabId);
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('laboratorium/DetailAlatLab'); ?>",
                    data: {
                        'AlatLabId': AlatLabId,
                    },
                    success: function (response) {
                        response = JSON.parse(response);
                        console.log(response);

                        if (response != null){

                            $("#editAlatLab [name=\"alatLabId\"]").val(response.Id);
                            $("#editAlatLab [name=\"labId\"]").val(response.LabId);
                            $("#editAlatLab [name=\"nama\"]").val(response.NamaAlat);
                            $("#editAlatLab [name=\"status\"]").val(response.Status);
                        }

                        $('#editAlatLab').modal('show');
                    }
                })
            })
        });
    </script>

    <!-- Edit Master Alat -->
    <script>
        $(document).ready(function (){
            $('.editMasterAlat').click(function (e) {
                e.preventDefault();

                var AlatId = $(this).closest('tr').find('.AlatId').text();
                
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('laboratorium/DetailMasterAlat'); ?>",
                    data: {
                        'AlatId': AlatId,
                    },
                    success: function (response) {
                        response = JSON.parse(response);
                        console.log(response);

                        if (response != null){

                            $("#editMasterAlat [name=\"alatId\"]").val(response.Id);
                            $("#editMasterAlat [name=\"nama\"]").val(response.Nama);
                            $("#editMasterAlat [name=\"merk\"]").val(response.Merk);
                            $("#editMasterAlat [name=\"status\"]").val(response.Status);
                        }

                        $('#editMasterAlat').modal('show');
                    }
                })
            })
        });
    </script>

    <!-- Surat IjinKerja Lab Tujuan Lainnya -->
    <script>
        $(document).ready(function () {
            // Function to toggle the display of CardTujuanSuratIjinKerjaLabLainnya based on TujuanSuratIjinKerjaLab value
            function toggleCardTujuanSuratIjinKerjaLabLainnya() {
                var selectedValue = $("#TujuanSuratIjinKerjaLab").val();
                if (selectedValue === "8") {
                    $("#CardTujuanSuratIjinKerjaLabLainnya").show();
                } else {
                    $("#CardTujuanSuratIjinKerjaLabLainnya").hide();
                }
            }

            // Initial check on page load
            toggleCardTujuanSuratIjinKerjaLabLainnya();

            // Event listener for change in TujuanSuratIjinKerjaLab dropdown
            $("#TujuanSuratIjinKerjaLab").change(function () {
                toggleCardTujuanSuratIjinKerjaLabLainnya();
            });
        });
    </script>

    <!-- Set Rentang Waktu Surat Ijin Lab -->
    <script>
        function updateMaxDate() {
            var tujuanSelect = document.getElementById("TujuanSuratIjinKerjaLab");
            var tanggalSelesaiInput = document.getElementById("inputTanggalSelesai");
            
            if (tujuanSelect.value == "5" || tujuanSelect.value == "6") {
                // Set max date to 6 months from today
                var maxDate = new Date();
                maxDate.setMonth(maxDate.getMonth() + 6);
                var formattedMaxDate = maxDate.toISOString().split('T')[0];
                tanggalSelesaiInput.setAttribute("max", formattedMaxDate);
            } else {
                // Set max date to 3 months from today
                var maxDate = new Date();
                maxDate.setMonth(maxDate.getMonth() + 3);
                var formattedMaxDate = maxDate.toISOString().split('T')[0];
                tanggalSelesaiInput.setAttribute("max", formattedMaxDate);
            }
        }
    </script>

    <!-- Set laboratorium PLP -->
    <script>
        $(document).ready(function(){
            // Initial check on page load
            toggleInputLab();

            // Handle change event of inputIsPLP
            $('#inputIsPLP').change(function(){
                toggleInputLab();
            });

            function toggleInputLab() {
                var isPLPValue = $('#inputIsPLP').val();
                if (isPLPValue === '1') {
                    $('#inputLab').show();
                } else {
                    $('#inputLab').hide();
                }
            }
        });
    </script>

    <!-- Tambah Date Skripsi -->
    <script>
        $(document).ready(function (){
            $('.tambahDateSkripsi').click(function (e) {
                e.preventDefault();

                $.ajax({
                    type: "POST",
                    url: "<?= base_url('akademik/Semester'); ?>",
                    data: {
                        'Json': true,
                    },
                    success: function (response) {
                        response = JSON.parse(response);
                        console.log(response);

                        $('#SemesterTambahDateSkripsi').select2();
                        $.each(response, function (index, item) {
                            $('#SemesterTambahDateSkripsi').append('<option value="' + item.Id + '">' + item.Semester + ' - ' + item.TahunAkademik + '</option>');
                        });
                        $('#SemesterTambahDateSkripsi').trigger('change');

                        $('#tambahDateSkripsi').modal('show');
                    }
                })
            })

            $('#tambahDateSkripsi').on('hidden.bs.modal', function () {
                $('#SemesterTambahDateSkripsi').empty();
            });
        });
    </script>

    <!-- Edit Jajaran -->
    <script>
        $(document).ready(function (){
            $('.editJajaran').click(function (e) {
                e.preventDefault();

                var JajaranId = $(this).closest('tr').find('.JajaranId').text();
                
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('Akademik/DetailJajaran'); ?>",
                    data: {
                        'JajaranId': JajaranId,
                    },
                    success: function (response) {
                        response = JSON.parse(response);
                        console.log(response);

                        var kadepId = response.KadepId; 
                        $("#editJajaran [name=\"kadep\"]").val(kadepId).trigger('change');
                        var sekdepId = response.SekdepId; 
                        $("#editJajaran [name=\"sekdep\"]").val(sekdepId).trigger('change'); 
                        var kaprodiS1Id = response.KaprodiS1Id; 
                        $("#editJajaran [name=\"kaprodiS1\"]").val(kaprodiS1Id).trigger('change'); 
                        var kaprodiS2Id = response.KaprodiS2Id; 
                        $("#editJajaran [name=\"kaprodiS2\"]").val(kaprodiS2Id).trigger('change'); 
                        var kaprodiS3Id = response.KaprodiS3Id; 
                        $("#editJajaran [name=\"kaprodiS3\"]").val(kaprodiS3Id).trigger('change'); 

                        $("#editJajaran [name=\"kadep\"]").select2();
                        $("#editJajaran [name=\"sekdep\"]").select2();
                        $("#editJajaran [name=\"kaprodiS1\"]").select2();
                        $("#editJajaran [name=\"kaprodiS2\"]").select2();
                        $("#editJajaran [name=\"kaprodiS3\"]").select2();
                        $("#editJajaran [name=\"jajaranId\"]").val(response.Id);
                        $('#editJajaran').modal('show');
                    }
                })
            })
        });
    </script>

    <!-- Edit Laboratorium -->
    <script>
        $(document).ready(function (){
            $('.editLaboratorium').click(function (e) {
                e.preventDefault();

                var LaboratoriumId = $(this).closest('tr').find('.LaboratoriumId').text();
                
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('Akademik/DetailLaboratorium'); ?>",
                    data: {
                        'LaboratoriumId': LaboratoriumId,
                    },
                    success: function (response) {
                        response = JSON.parse(response);
                        console.log(response);

                        var kalabId = response.KalabId; 
                        $("#editLaboratorium [name=\"dosen\"]").val(kalabId).trigger('change'); 

                        $("#editLaboratorium [name=\"dosen\"]").select2();
                        $("#editLaboratorium [name=\"laboratoriumId\"]").val(response.Id);
                        $("#editLaboratorium [name=\"laboratorium\"]").val(response.Nama);
                        $("#editLaboratorium [name=\"status\"]").val(response.Status);
                        $('#editLaboratorium').modal('show');
                    }
                })
            })
        });
    </script>

    <!-- Edit Instansi -->
    <script>
        $(document).ready(function (){
            $('.editInstansi').click(function (e) {
                e.preventDefault();

                var InstansiId = $(this).closest('tr').find('.InstansiId').text();
                
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('Akademik/DetailInstansi'); ?>",
                    data: {
                        'InstansiId': InstansiId,
                    },
                    success: function (response) {
                        response = JSON.parse(response);
                        console.log(response);
                        $("#editInstansi [name=\"instansiId\"]").val(response.Id);
                        $("#editInstansi [name=\"nama\"]").val(response.Nama);
                        $("#editInstansi [name=\"alamat\"]").val(response.Alamat);
                        $("#editInstansi [name=\"email\"]").val(response.Email);
                        $("#editInstansi [name=\"telpon\"]").val(response.Telpon);
                        $("#editInstansi [name=\"pimpinan\"]").val(response.Pimpinan);
                        $("#editInstansi [name=\"status\"]").val(response.Status);
                        $('#editInstansi').modal('show');
                    }
                })
            })
        });
    </script>

    <!-- Edit Date Skripsi -->
    <script>
        $(document).ready(function (){
            $('.editDateSkripsi').click(function (e) {
                e.preventDefault();

                var DateSkripsiId = $(this).closest('tr').find('.DateSkripsiId').text();
                
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('Akademik/DetailDateSkripsi'); ?>",
                    data: {
                        'DateSkripsiId': DateSkripsiId,
                    },
                    success: function (response) {
                        response = JSON.parse(response);
                        console.log(response);

                        var SemesterId = response.DetailDateSkripsi.SemesterId;
                        $("#editDateSkripsi [name=\"semesterId\"]").empty(); // Clear existing options

                        $("#editDateSkripsi [name=\"semesterId\"]").append('<option value="">Select an option</option>');

                        $.each(response.Semesters, function (index, semester) {
                            var selected = semester.Id == SemesterId ? 'selected' : '';
                            $("#editDateSkripsi [name=\"semesterId\"]").append('<option value="' + semester.Id + '" ' + selected + '>' + semester.Semester + '-' + semester.TahunAkademik + '</option>');
                        });

                        $("#editDateSkripsi [name=\"semesterId\"]").select2();

                        $("#editDateSkripsi [name=\"dateSkripsiId\"]").val(response.DetailDateSkripsi.Id);
                        $("#editDateSkripsi [name=\"tanggalUjianSempro\"]").val(response.DetailDateSkripsi.MaxDateSempro);
                        $("#editDateSkripsi [name=\"tanggalUjianSemju\"]").val(response.DetailDateSkripsi.MaxDateSemju);
                        $("#editDateSkripsi [name=\"tanggalUjianSkripsi\"]").val(response.DetailDateSkripsi.MaxDateSkripsi);
                        $("#editDateSkripsi [name=\"status\"]").val(response.DetailDateSkripsi.Status);

                        $('#editDateSkripsi').modal('show');
                    }
                })
            })
        });
    </script>

    <!-- Edit Ruangan -->
    <script>
        $(document).ready(function (){
            $('.editRuangan').click(function (e) {
                e.preventDefault();

                var RuanganId = $(this).closest('tr').find('.RuanganId').text();
                
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('Akademik/DetailRuangan'); ?>",
                    data: {
                        'RuanganId': RuanganId,
                    },
                    success: function (response) {
                        response = JSON.parse(response);
                        console.log(response);
                        $("#editRuangan [name=\"RuanganId\"]").val(response.Id);
                        $("#editRuangan [name=\"nama\"]").val(response.Nama);
                        $("#editRuangan [name=\"kapasitas\"]").val(response.Kapasitas);
                        $("#editRuangan [name=\"keterangan\"]").val(response.Keterangan);
                        $("#editRuangan [name=\"kategori\"]").val(response.Kategori);
                        $("#editRuangan [name=\"status\"]").val(response.Status);
                        $('#editRuangan').modal('show');
                    }
                })
            })
        });
    </script>

    <!-- Edit Semester -->
    <script>
        $(document).ready(function (){
            $('.editSemester').click(function (e) {
                e.preventDefault();

                var SemesterId = $(this).closest('tr').find('.SemesterId').text();
                
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('Akademik/DetailSemester'); ?>",
                    data: {
                        'SemesterId': SemesterId,
                    },
                    success: function (response) {
                        response = JSON.parse(response);
                        console.log(response);
                        $("#editSemester [name=\"SemesterId\"]").val(response.Id);
                        $("#editSemester [name=\"semester\"]").val(response.Semester);
                        $("#editSemester [name=\"tahunAkademik\"]").val(response.TahunAkademik);
                        $("#editSemester [name=\"status\"]").val(response.Status);
                        $('#editSemester').modal('show');
                    }
                })
            })
        });
    </script>

    <!-- Berkas Ujian PKL -->
    <script>
    $(document).ready(function (){
        $('.berkasUjianPKL').click(function (e) {
            e.preventDefault();

            var PklId = $(this).closest('tr').find('.PklId').text();
            console.log(PklId);

            $.ajax({
                type: "POST",
                url: "<?= base_url('pkl/DetailUjianPKL'); ?>",
                data: {
                    'PklId': PklId,
                },
                success: function (response) {
                    response = JSON.parse(response);
                    console.log(response);

                    if (response != null){
                        /* Upload Daftar Hadir PKL */
                        var responseFileDaftarHadir = "<?=base_url('/assets/uploads/berkasUjian_pkl/');?>" + response.FileDaftarHadir;
                        var linkDaftarHadir = document.getElementById("linkDaftarHadir");
                        linkDaftarHadir.href = responseFileDaftarHadir;
                        var linkIconDaftarHadir = document.getElementById("linkIconDaftarHadir");
                        var responseFileDaftarHadir = response.FileDaftarHadir;
                        if (responseFileDaftarHadir != null && responseFileDaftarHadir !== ""){
                            linkIconDaftarHadir.style.display ="block";
                            var responseUploadDaftarHadir = response.UploadDaftarHadir;
                            document.getElementById("UploadDaftarHadir").textContent = responseUploadDaftarHadir;
                        }else{
                            linkIconDaftarHadir.style.display ="none";
                        }

                        /* Upload Kartu Bimbingan PKL */
                        var responseFileKartuBimbingan = "<?=base_url('/assets/uploads/berkasUjian_pkl/');?>" + response.FileKartuBimbingan;
                        var linkKartuBimbingan = document.getElementById("linkKartuBimbingan");
                        linkKartuBimbingan.href = responseFileKartuBimbingan;
                        var linkIconKartuBimbingan = document.getElementById("linkIconKartuBimbingan");
                        var responseFileKartuBimbingan = response.FileKartuBimbingan;
                        if (responseFileKartuBimbingan != null && responseFileKartuBimbingan !== ""){
                            linkIconKartuBimbingan.style.display ="block";
                            var responseUploadKartuBimbingan = response.UploadKartuBimbingan;
                            document.getElementById("UploadKartuBimbingan").textContent = responseUploadKartuBimbingan;
                        }else{
                            linkIconKartuBimbingan.style.display ="none";
                        }

                        /* Upload Lembar Persetujuan PKL */
                        var responseFileLembarPersetujuan = "<?=base_url('/assets/uploads/berkasUjian_pkl/');?>" + response.FileLembarPersetujuan;
                        var linkLembarPersetujuan = document.getElementById("linkLembarPersetujuan");
                        linkLembarPersetujuan.href = responseFileLembarPersetujuan;
                        var linkIconLembarPersetujuan = document.getElementById("linkIconLembarPersetujuan");
                        var responseFileLembarPersetujuan = response.FileLembarPersetujuan;
                        if (responseFileLembarPersetujuan != null && responseFileLembarPersetujuan !== ""){
                            linkIconLembarPersetujuan.style.display ="block";
                            var responseUploadLembarPersetujuan = response.UploadLembarPersetujuan;
                            document.getElementById("UploadLembarPersetujuan").textContent = responseUploadLembarPersetujuan;
                        }else{
                            linkIconLembarPersetujuan.style.display ="none";
                        }

                        /* Upload Penilaian Pembimbing PKL */
                        var responseFilePenilaianPembimbing = "<?=base_url('/assets/uploads/berkasUjian_pkl/');?>" + response.FilePenilaianPembimbing;
                        var linkPenilaianPembimbing = document.getElementById("linkPenilaianPembimbing");
                        linkPenilaianPembimbing.href = responseFilePenilaianPembimbing;
                        var linkIconPenilaianPembimbing = document.getElementById("linkIconPenilaianPembimbing");
                        var responseFilePenilaianPembimbing = response.FilePenilaianPembimbing;
                        if (responseFilePenilaianPembimbing != null && responseFilePenilaianPembimbing !== ""){
                            linkIconPenilaianPembimbing.style.display ="block";
                            var responseUploadPenilaianPembimbing = response.UploadPenilaianPembimbing;
                            document.getElementById("UploadPenilaianPembimbing").textContent = responseUploadPenilaianPembimbing;
                        }else{
                            linkIconPenilaianPembimbing.style.display ="none";
                        }

                        /* Upload File Presentasi PKL */
                        var responseFilePresentasi = "<?=base_url('/assets/uploads/berkasUjian_pkl/');?>" + response.FilePresentasi;
                        var linkPresentasi = document.getElementById("linkPresentasi");
                        linkPresentasi.href = responseFilePresentasi;
                        var linkIconPresentasi = document.getElementById("linkIconPresentasi");
                        var responseFilePresentasi = response.FilePresentasi;
                        if (responseFilePresentasi != null && responseFilePresentasi !== ""){
                            linkIconPresentasi.style.display ="block";
                            var responseUploadPresentasi = response.UploadPresentasi;
                            document.getElementById("UploadPresentasi").textContent = responseUploadPresentasi;
                        }else{
                            linkIconPresentasi.style.display ="none";
                        }

                        /* Upload Laporan PKL */
                        var responseFileLaporanPKL = "<?=base_url('/assets/uploads/berkasUjian_pkl/');?>" + response.FileLaporanPKL;
                        var linkLaporanPKL = document.getElementById("linkLaporanPKL");
                        linkLaporanPKL.href = responseFileLaporanPKL;
                        var linkIconLaporanPKL = document.getElementById("linkIconLaporanPKL");
                        var responseFileLaporanPKL = response.FileLaporanPKL;
                        if (responseFileLaporanPKL != null && responseFileLaporanPKL !== ""){
                            linkIconLaporanPKL.style.display ="block";
                            var responseUploadLaporanPKL = response.UploadLaporanPKL;
                            document.getElementById("UploadLaporanPKL").textContent = responseUploadLaporanPKL;
                        }else{
                            linkIconLaporanPKL.style.display ="none";
                        }
                    }

                    $("#berkasUjianPKL [name=\"PklId\"]").val(PklId);
                    $('#berkasUjianPKL').modal('show');
                }
            })
        })
    });
    </script>

    <!-- Upload Daftar Hadir Ujian PKL -->
    <script>
        const daftarHadirUjianPKL = document.getElementById('input-DaftarHadirUjianPKL');
        if (daftarHadirUjianPKL) {
            daftarHadirUjianPKL.addEventListener('change', function () {
                document.getElementById('submit-DaftarHadirUjianPKL').style.display = 'block';
            });
        }
    </script>

    <!-- Upload Kartu Bimbingan Ujian PKL -->
    <script>
        const kartuBimbinganUjianPKL = document.getElementById('input-KartuBimbinganUjianPKL');
        if (kartuBimbinganUjianPKL) {
            kartuBimbinganUjianPKL.addEventListener('change', function () {
                document.getElementById('submit-KartuBimbinganUjianPKL').style.display = 'block';
            });
        }
    </script>

    <!-- Upload Lembar Persetujuan Ujian PKL -->
    <script>
        const LembarPersetujuanUjianPKL = document.getElementById('input-LembarPersetujuanUjianPKL');
        if (LembarPersetujuanUjianPKL) {
            LembarPersetujuanUjianPKL.addEventListener('change', function () {
                document.getElementById('submit-LembarPersetujuanUjianPKL').style.display = 'block';
            });
        }
    </script>

    <!-- Upload Penilaian Pembimbing Ujian PKL -->
    <script>
        const PenilaianPembimbingUjianPKL = document.getElementById('input-PenilaianPembimbingUjianPKL');
        if (PenilaianPembimbingUjianPKL) {
            PenilaianPembimbingUjianPKL.addEventListener('change', function () {
                document.getElementById('submit-PenilaianPembimbingUjianPKL').style.display = 'block';
            });
        }
    </script>

    <!-- Upload Presentasi Ujian PKL -->
    <script>
        const PresentasiUjianPKL = document.getElementById('input-PresentasiUjianPKL');
        if (PresentasiUjianPKL) {
            PresentasiUjianPKL.addEventListener('change', function () {
                document.getElementById('submit-PresentasiUjianPKL').style.display = 'block';
            });
        }
    </script>

    <!-- Upload Laporan Ujian PKL -->
    <script>
        const LaporanPKLUjianPKL = document.getElementById('input-LaporanPKLUjianPKL');
        if (LaporanPKLUjianPKL) {
            LaporanPKLUjianPKL.addEventListener('change', function () {
                document.getElementById('submit-LaporanPKLUjianPKL').style.display = 'block';
            });
        }
    </script>

    <!-- Edit Ujian PKL -->
    <script>
        $(document).ready(function (){
            $('.editUjianPkl').click(function (e) {
                e.preventDefault();

                var PklId = $(this).closest('tr').find('.PklId').text();
                
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('pkl/DetailUjianPkl'); ?>",
                    data: {
                        'PklId': PklId,
                    },
                    success: function (response) {
                        response = JSON.parse(response);
                        console.log(response);

                        $("#editUjianPkl [name=\"PklId\"]").val(response.Id);
 
                        var RuangId = response.RuangId; 
                        $("#editUjianPkl [name=\"ruangId\"]").val(RuangId).trigger('change'); 
                        var JamMulai = response.JamMulai; 
                        $("#editUjianPkl [name=\"jamMulai2\"]").select2().val(JamMulai).trigger('change'); 
                        var JamSelesai = response.JamSelesai; 
                        $("#editUjianPkl [name=\"jamSelesai2\"]").val(JamSelesai).trigger('change'); 

                        $("#editUjianPkl [name=\"ruangId\"]").select2();
                        $("#editUjianPkl [name=\"jamMulai2\"]").select2();
                        $("#editUjianPkl [name=\"jamSelesai2\"]").select2();

                        $("#editUjianPkl [name=\"tanggalUjian\"]").val(response.TanggalUjian);
                        $("#editUjianPkl [name=\"linkZoom\"]").val(response.LinkZoom);

                        $('#editUjianPkl').modal('show');
                    }
                })
            })
        });
    </script>

    <!-- Nilai LPA Dosen Pembimbing PKL -->
    <script>
        $(document).ready(function (){
            $('.viewNilaiDosenPembimbingLPA').click(function (e) {
                e.preventDefault();

                var PklId = $(this).closest('tr').find('.PklId').text();
                console.log(PklId);
                
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('pkl/ViewNilaiDosenPembimbingLPA'); ?>",
                    data: {
                        'PklId': PklId,
                    },
                    success: function (response) {
                        response = JSON.parse(response);
                        var LPA = JSON.parse(response.LP_PKL_A);
                        console.log(LPA);

                        $("#viewNilaiDosenPembimbingLPA [id=\"12_1_1\"]").text(LPA["12_1_1"]);
                        $("#viewNilaiDosenPembimbingLPA [id=\"12_1_2\"]").text(LPA["12_1_2"]);
                        $("#viewNilaiDosenPembimbingLPA [id=\"12_1_3\"]").text(LPA["12_1_3"]);
                        $("#viewNilaiDosenPembimbingLPA [id=\"12_1_4\"]").text(LPA["12_1_4"]);
                        $("#viewNilaiDosenPembimbingLPA [id=\"12_2_1\"]").text(LPA["12_2_1"]);
                        $("#viewNilaiDosenPembimbingLPA [id=\"12_2_2\"]").text(LPA["12_2_2"]);
                        $("#viewNilaiDosenPembimbingLPA [id=\"12_2_3\"]").text(LPA["12_2_3"]);
                        $("#viewNilaiDosenPembimbingLPA [id=\"12_2_4\"]").text(LPA["12_2_4"]);
                        $("#viewNilaiDosenPembimbingLPA [id=\"12_3_1\"]").text(LPA["12_3_1"]);
                        $("#viewNilaiDosenPembimbingLPA [id=\"12_3_2\"]").text(LPA["12_3_2"]);
                        $("#viewNilaiDosenPembimbingLPA [id=\"12_5_1\"]").text(LPA["12_5_1"]);
                        $("#viewNilaiDosenPembimbingLPA [id=\"12_5_2\"]").text(LPA["12_5_2"]);
                        $("#viewNilaiDosenPembimbingLPA [id=\"15_1_1\"]").text(LPA["15_1_1"]);
                        $("#viewNilaiDosenPembimbingLPA [id=\"15_1_2\"]").text(LPA["15_1_2"]);
                        $("#viewNilaiDosenPembimbingLPA [id=\"15_2_1\"]").text(LPA["15_2_1"]);
                        $("#viewNilaiDosenPembimbingLPA [id=\"15_2_2\"]").text(LPA["15_2_2"]);
                        $("#viewNilaiDosenPembimbingLPA [id=\"15_3_1\"]").text(LPA["15_3_1"]);
                        $("#viewNilaiDosenPembimbingLPA [id=\"15_3_2\"]").text(LPA["15_3_2"]);
                        $("#viewNilaiDosenPembimbingLPA [id=\"15_4_1\"]").text(LPA["15_4_1"]);
                        $("#viewNilaiDosenPembimbingLPA [id=\"15_4_2\"]").text(LPA["15_4_2"]);
                        $("#viewNilaiDosenPembimbingLPA [id=\"15_5_1\"]").text(LPA["15_5_1"]);
                        $("#viewNilaiDosenPembimbingLPA [id=\"15_5_2\"]").text(LPA["15_5_2"]);
                        $('#viewNilaiDosenPembimbingLPA').modal('show');
                    }
                })
            })
        });
    </script>

    <!-- Nilai LPB Dosen Pembimbing PKL -->
    <script>
        $(document).ready(function (){
            $('.viewNilaiDosenPembimbingLPB').click(function (e) {
                e.preventDefault();

                var PklId = $(this).closest('tr').find('.PklId').text();
                console.log(PklId);
                
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('pkl/ViewNilaiDosenPembimbingLPB'); ?>",
                    data: {
                        'PklId': PklId,
                    },
                    success: function (response) {
                        response = JSON.parse(response);
                        var LPB = JSON.parse(response.LP_PKL_B);
                        console.log(LPB);

                        $("#viewNilaiDosenPembimbingLPB [id=\"12_1_1\"]").text(LPB["12_1_1"]);
                        $("#viewNilaiDosenPembimbingLPB [id=\"12_1_2\"]").text(LPB["12_1_2"]);
                        $("#viewNilaiDosenPembimbingLPB [id=\"12_1_3\"]").text(LPB["12_1_3"]);
                        $("#viewNilaiDosenPembimbingLPB [id=\"12_1_4\"]").text(LPB["12_1_4"]);
                        $("#viewNilaiDosenPembimbingLPB [id=\"12_2_1\"]").text(LPB["12_2_1"]);
                        $("#viewNilaiDosenPembimbingLPB [id=\"12_2_2\"]").text(LPB["12_2_2"]);
                        $("#viewNilaiDosenPembimbingLPB [id=\"12_2_3\"]").text(LPB["12_2_3"]);
                        $("#viewNilaiDosenPembimbingLPB [id=\"12_2_4\"]").text(LPB["12_2_4"]);
                        $("#viewNilaiDosenPembimbingLPB [id=\"12_3_1\"]").text(LPB["12_3_1"]);
                        $("#viewNilaiDosenPembimbingLPB [id=\"12_3_2\"]").text(LPB["12_3_2"]);
                        $("#viewNilaiDosenPembimbingLPB [id=\"12_5_1\"]").text(LPB["12_5_1"]);
                        $("#viewNilaiDosenPembimbingLPB [id=\"12_5_2\"]").text(LPB["12_5_2"]);
                        $("#viewNilaiDosenPembimbingLPB [id=\"15_1_1\"]").text(LPB["15_1_1"]);
                        $("#viewNilaiDosenPembimbingLPB [id=\"15_1_2\"]").text(LPB["15_1_2"]);
                        $("#viewNilaiDosenPembimbingLPB [id=\"15_2_1\"]").text(LPB["15_2_1"]);
                        $("#viewNilaiDosenPembimbingLPB [id=\"15_2_2\"]").text(LPB["15_2_2"]);
                        $("#viewNilaiDosenPembimbingLPB [id=\"15_3_1\"]").text(LPB["15_3_1"]);
                        $("#viewNilaiDosenPembimbingLPB [id=\"15_3_2\"]").text(LPB["15_3_2"]);
                        $("#viewNilaiDosenPembimbingLPB [id=\"15_4_1\"]").text(LPB["15_4_1"]);
                        $("#viewNilaiDosenPembimbingLPB [id=\"15_4_2\"]").text(LPB["15_4_2"]);
                        $("#viewNilaiDosenPembimbingLPB [id=\"15_5_1\"]").text(LPB["15_5_1"]);
                        $("#viewNilaiDosenPembimbingLPB [id=\"15_5_2\"]").text(LPB["15_5_2"]);
                        $('#viewNilaiDosenPembimbingLPB').modal('show');
                    }
                })
            })
        });
    </script>

    <!-- Nilai MK1 Dosen Pembimbing PKL -->
    <script>
        $(document).ready(function (){
            $('.viewNilaiDosenPembimbingMK1').click(function (e) {
                e.preventDefault();

                var PklId = $(this).closest('tr').find('.PklId').text();
                console.log(PklId);
                
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('pkl/ViewNilaiDosenPembimbingMK1'); ?>",
                    data: {
                        'PklId': PklId,
                    },
                    success: function (response) {
                        response = JSON.parse(response);
                        var MK1 = JSON.parse(response.LP_MK1);
                        console.log(response);

                        switch (response.MataKuliah1){
                            case '1':
                                $("#viewNilaiDosenPembimbingSumberDayaAlam [id=\"1\"]").text(MK1["1"]);
                                $("#viewNilaiDosenPembimbingSumberDayaAlam [id=\"2\"]").text(MK1["2"]);
                                $("#viewNilaiDosenPembimbingSumberDayaAlam [id=\"3\"]").text(MK1["3"]);

                                $('#viewNilaiDosenPembimbingSumberDayaAlam').modal('show');
                                break;
                            case '2':
                                $("#viewNilaiDosenPembimbingKeselamatandanLingkungan [id=\"1\"]").text(MK1["1"]);
                                $("#viewNilaiDosenPembimbingKeselamatandanLingkungan [id=\"2\"]").text(MK1["2"]);
                                $("#viewNilaiDosenPembimbingKeselamatandanLingkungan [id=\"3\"]").text(MK1["3"]);

                                $('#viewNilaiDosenPembimbingKeselamatandanLingkungan').modal('show');
                                break;
                            case '3':
                                $("#viewNilaiDosenPembimbingMetodedanTerapan [id=\"1\"]").text(MK1["1"]);
                                $("#viewNilaiDosenPembimbingMetodedanTerapan [id=\"2\"]").text(MK1["2"]);
                                $("#viewNilaiDosenPembimbingMetodedanTerapan [id=\"3\"]").text(MK1["3"]);

                                $('#viewNilaiDosenPembimbingMetodedanTerapan').modal('show');
                                break;
                            case '4':
                                $("#viewNilaiDosenPembimbingAnalisisPenilaiandanTindakan [id=\"1\"]").text(MK1["1"]);
                                $("#viewNilaiDosenPembimbingAnalisisPenilaiandanTindakan [id=\"2\"]").text(MK1["2"]);
                                $("#viewNilaiDosenPembimbingAnalisisPenilaiandanTindakan [id=\"3\"]").text(MK1["3"]);

                                $('#viewNilaiDosenPembimbingAnalisisPenilaiandanTindakan').modal('show');
                                break;
                            case '5':
                                $("#viewNilaiDosenPembimbingKemandirianPengayaanPengetahuan [id=\"1\"]").text(MK1["1"]);
                                $("#viewNilaiDosenPembimbingKemandirianPengayaanPengetahuan [id=\"2\"]").text(MK1["2"]);
                                $("#viewNilaiDosenPembimbingKemandirianPengayaanPengetahuan [id=\"3\"]").text(MK1["3"]);

                                $('#viewNilaiDosenPembimbingKemandirianPengayaanPengetahuan').modal('show');
                                break;
                        }
                    }
                })
            })
        });
    </script>

    <!-- Nilai MK2 Dosen Pembimbing PKL -->
    <script>
        $(document).ready(function (){
            $('.viewNilaiDosenPembimbingMK2').click(function (e) {
                e.preventDefault();

                var PklId = $(this).closest('tr').find('.PklId').text();
                console.log(PklId);
                
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('pkl/ViewNilaiDosenPembimbingMK2'); ?>",
                    data: {
                        'PklId': PklId,
                    },
                    success: function (response) {
                        response = JSON.parse(response);
                        var MK2 = JSON.parse(response.LP_MK2);
                        console.log(response);

                        switch (response.MataKuliah2){
                            case '1':
                                $("#viewNilaiDosenPembimbingSumberDayaAlam [id=\"1\"]").text(MK2["1"]);
                                $("#viewNilaiDosenPembimbingSumberDayaAlam [id=\"2\"]").text(MK2["2"]);
                                $("#viewNilaiDosenPembimbingSumberDayaAlam [id=\"3\"]").text(MK2["3"]);

                                $('#viewNilaiDosenPembimbingSumberDayaAlam').modal('show');
                                break;
                            case '2':
                                $("#viewNilaiDosenPembimbingKeselamatandanLingkungan [id=\"1\"]").text(MK2["1"]);
                                $("#viewNilaiDosenPembimbingKeselamatandanLingkungan [id=\"2\"]").text(MK2["2"]);
                                $("#viewNilaiDosenPembimbingKeselamatandanLingkungan [id=\"3\"]").text(MK2["3"]);

                                $('#viewNilaiDosenPembimbingKeselamatandanLingkungan').modal('show');
                                break;
                            case '3':
                                $("#viewNilaiDosenPembimbingMetodedanTerapan [id=\"1\"]").text(MK2["1"]);
                                $("#viewNilaiDosenPembimbingMetodedanTerapan [id=\"2\"]").text(MK2["2"]);
                                $("#viewNilaiDosenPembimbingMetodedanTerapan [id=\"3\"]").text(MK2["3"]);

                                $('#viewNilaiDosenPembimbingMetodedanTerapan').modal('show');
                                break;
                            case '4':
                                $("#viewNilaiDosenPembimbingAnalisisPenilaiandanTindakan [id=\"1\"]").text(MK2["1"]);
                                $("#viewNilaiDosenPembimbingAnalisisPenilaiandanTindakan [id=\"2\"]").text(MK2["2"]);
                                $("#viewNilaiDosenPembimbingAnalisisPenilaiandanTindakan [id=\"3\"]").text(MK2["3"]);

                                $('#viewNilaiDosenPembimbingAnalisisPenilaiandanTindakan').modal('show');
                                break;
                            case '5':
                                $("#viewNilaiDosenPembimbingKemandirianPengayaanPengetahuan [id=\"1\"]").text(MK2["1"]);
                                $("#viewNilaiDosenPembimbingKemandirianPengayaanPengetahuan [id=\"2\"]").text(MK2["2"]);
                                $("#viewNilaiDosenPembimbingKemandirianPengayaanPengetahuan [id=\"3\"]").text(MK2["3"]);

                                $('#viewNilaiDosenPembimbingKemandirianPengayaanPengetahuan').modal('show');
                                break;
                        }
                    }
                })
            })
        });
    </script>

    <!-- Nilai MK3 Dosen Pembimbing PKL -->
    <script>
        $(document).ready(function (){
            $('.viewNilaiDosenPembimbingMK3').click(function (e) {
                e.preventDefault();

                var PklId = $(this).closest('tr').find('.PklId').text();
                console.log(PklId);
                
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('pkl/ViewNilaiDosenPembimbingMK3'); ?>",
                    data: {
                        'PklId': PklId,
                    },
                    success: function (response) {
                        response = JSON.parse(response);
                        var MK3 = JSON.parse(response.LP_MK3);
                        console.log(response);

                        switch (response.MataKuliah2){
                            case '1':
                                $("#viewNilaiDosenPembimbingSumberDayaAlam [id=\"1\"]").text(MK3["1"]);
                                $("#viewNilaiDosenPembimbingSumberDayaAlam [id=\"2\"]").text(MK3["2"]);
                                $("#viewNilaiDosenPembimbingSumberDayaAlam [id=\"3\"]").text(MK3["3"]);

                                $('#viewNilaiDosenPembimbingSumberDayaAlam').modal('show');
                                break;
                            case '2':
                                $("#viewNilaiDosenPembimbingKeselamatandanLingkungan [id=\"1\"]").text(MK3["1"]);
                                $("#viewNilaiDosenPembimbingKeselamatandanLingkungan [id=\"2\"]").text(MK3["2"]);
                                $("#viewNilaiDosenPembimbingKeselamatandanLingkungan [id=\"3\"]").text(MK3["3"]);

                                $('#viewNilaiDosenPembimbingKeselamatandanLingkungan').modal('show');
                                break;
                            case '3':
                                $("#viewNilaiDosenPembimbingMetodedanTerapan [id=\"1\"]").text(MK3["1"]);
                                $("#viewNilaiDosenPembimbingMetodedanTerapan [id=\"2\"]").text(MK3["2"]);
                                $("#viewNilaiDosenPembimbingMetodedanTerapan [id=\"3\"]").text(MK3["3"]);

                                $('#viewNilaiDosenPembimbingMetodedanTerapan').modal('show');
                                break;
                            case '4':
                                $("#viewNilaiDosenPembimbingAnalisisPenilaiandanTindakan [id=\"1\"]").text(MK3["1"]);
                                $("#viewNilaiDosenPembimbingAnalisisPenilaiandanTindakan [id=\"2\"]").text(MK3["2"]);
                                $("#viewNilaiDosenPembimbingAnalisisPenilaiandanTindakan [id=\"3\"]").text(MK3["3"]);

                                $('#viewNilaiDosenPembimbingAnalisisPenilaiandanTindakan').modal('show');
                                break;
                            case '5':
                                $("#viewNilaiDosenPembimbingKemandirianPengayaanPengetahuan [id=\"1\"]").text(MK3["1"]);
                                $("#viewNilaiDosenPembimbingKemandirianPengayaanPengetahuan [id=\"2\"]").text(MK3["2"]);
                                $("#viewNilaiDosenPembimbingKemandirianPengayaanPengetahuan [id=\"3\"]").text(MK3["3"]);

                                $('#viewNilaiDosenPembimbingKemandirianPengayaanPengetahuan').modal('show');
                                break;
                        }
                    }
                })
            })
        });
    </script>

    <!-- Daftar Ujian PKL -->
    <script>
        $(document).ready(function (){
            $('.daftarUjianPKL').click(function (e) {
                e.preventDefault();
                var PklId = $(this).closest('tr').find('.PklId').text();
                
                $("#daftarUjianPKL [name=\"PklId\"]").val(PklId);
                $('#daftarUjianPKL').modal('show');
            })
        });
    </script>

    <!-- Show Instansi Lainnya -->
    <script>
        $(document).ready(function () {
            // Function to toggle the display of CardInstansiDaftarPKL based on InstansiDaftarPKL value
            function toggleCardInstansiDaftarPKL() {
                var selectedValue = $("#InstansiDaftarPKL").val();
                if (selectedValue === "lainnya") {
                    $("#CardInstansiDaftarPKL").show();
                } else {
                    $("#CardInstansiDaftarPKL").hide();
                }
            }

            // Initial check on page load
            toggleCardInstansiDaftarPKL();

            // Event listener for change in InstansiDaftarPKL dropdown
            $("#InstansiDaftarPKL").change(function () {
                toggleCardInstansiDaftarPKL();
            });
        });
    </script>

    <!-- Detail (PKL) -->
    <script>
        $(document).ready(function (){
            $('.detailPkl').click(function (e) {
                e.preventDefault();

                var PklId = $(this).closest('tr').find('.PklId').text();
                
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('pkl/DetailPkl'); ?>",
                    data: {
                        'PklId': PklId,
                    },
                    success: function (response) {
                        response = JSON.parse(response);
                        console.log(response);

                        var TanggalPelaksanaan = response.TanggalMulaiFormat + " s/d " + response.TanggalSelesaiFormat;

                        $("#detailPkl [name=\"PklId\"]").val(response.Id);
                        $("#detailPkl [name=\"namaMahasiswa\"]").val(response.NamaMahasiswa);
                        $("#detailPkl [name=\"username\"]").val(response.Username);
                        $("#detailPkl [name=\"namaInstansi\"]").val(response.NamaInstansi);
                        $("#detailPkl [name=\"alamatInstansi\"]").val(response.AlamatInstansi);
                        $("#detailPkl [name=\"emailInstansi\"]").val(response.EmailInstansi);
                        $("#detailPkl [name=\"telponInstansi\"]").val(response.TelponInstansi);
                        $("#detailPkl [name=\"pembimbingLapang\"]").val(response.PembimbingLapangan);
                        $("#detailPkl [name=\"dosenPembimbing\"]").val(response.DosenNama);
                        $("#detailPkl [name=\"tanggalPelaksanaan\"]").val(TanggalPelaksanaan);

                        $('#detailPkl').modal('show');
                    }
                })
            })
        });
    </script>

    <!-- Edit (PKL) -->
    <script>
        $(document).ready(function (){
            $('.editPkl').click(function (e) {
                e.preventDefault();

                var PklId = $(this).closest('tr').find('.PklId').text();
                
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('pkl/DetailPkl'); ?>",
                    data: {
                        'PklId': PklId,
                    },
                    success: function (response) {
                        response = JSON.parse(response);
                        console.log(response);

                        $("#editPkl [name=\"PklId\"]").val(response.Id);
                        $("#editPkl [name=\"pembimbingLapang\"]").val(response.PembimbingLapangan);
                        $("#editPkl [name=\"tanggalMulai\"]").val(response.TanggalMulai);
                        $("#editPkl [name=\"tanggalSelesai\"]").val(response.TanggalSelesai);
                        $("#editPkl [name=\"status\"]").val(response.Status);

                        var InstansiId = response.InstansiId; 
                        $("#editPkl [name=\"instansi\"]").select2().val(InstansiId).trigger('change');
                        var dosenId = response.DosenId; 
                        $("#editPkl [name=\"dosenPkl\"]").val(dosenId).trigger('change'); 

                        $("#editPkl [name=\"instansi\"]").select2();
                        $("#editPkl [name=\"dosenPkl\"]").select2();

                        $('#editPkl').modal('show');
                    }
                })
            })
        });
    </script>

    <!-- Nilai LPA Pembimbing Lapang PKL -->
    <script>
        $(document).ready(function (){
            $('.viewNilaiPembimbingLapangLPA').click(function (e) {
                e.preventDefault();

                var PklId = $(this).closest('tr').find('.PklId').text();
                console.log(PklId);
                
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('pkl/ViewNilaiPembimbingLapangLPA'); ?>",
                    data: {
                        'PklId': PklId,
                    },
                    success: function (response) {
                        response = JSON.parse(response);
                        var LPA = JSON.parse(response.LP_PKL_A);
                        console.log(LPA);

                        $("#viewNilaiPembimbingLapangLPA [id=\"12_1_1\"]").text(LPA["12_1_1"]);
                        $("#viewNilaiPembimbingLapangLPA [id=\"12_1_2\"]").text(LPA["12_1_2"]);
                        $("#viewNilaiPembimbingLapangLPA [id=\"12_1_3\"]").text(LPA["12_1_3"]);
                        $("#viewNilaiPembimbingLapangLPA [id=\"12_1_4\"]").text(LPA["12_1_4"]);
                        $("#viewNilaiPembimbingLapangLPA [id=\"12_2_1\"]").text(LPA["12_2_1"]);
                        $("#viewNilaiPembimbingLapangLPA [id=\"12_2_2\"]").text(LPA["12_2_2"]);
                        $("#viewNilaiPembimbingLapangLPA [id=\"12_2_3\"]").text(LPA["12_2_3"]);
                        $("#viewNilaiPembimbingLapangLPA [id=\"12_2_4\"]").text(LPA["12_2_4"]);
                        $("#viewNilaiPembimbingLapangLPA [id=\"12_3_1\"]").text(LPA["12_3_1"]);
                        $("#viewNilaiPembimbingLapangLPA [id=\"12_3_2\"]").text(LPA["12_3_2"]);
                        $("#viewNilaiPembimbingLapangLPA [id=\"12_5_1\"]").text(LPA["12_5_1"]);
                        $("#viewNilaiPembimbingLapangLPA [id=\"12_5_2\"]").text(LPA["12_5_2"]);
                        $("#viewNilaiPembimbingLapangLPA [id=\"15_1_1\"]").text(LPA["15_1_1"]);
                        $("#viewNilaiPembimbingLapangLPA [id=\"15_1_2\"]").text(LPA["15_1_2"]);
                        $("#viewNilaiPembimbingLapangLPA [id=\"15_2_1\"]").text(LPA["15_2_1"]);
                        $("#viewNilaiPembimbingLapangLPA [id=\"15_2_2\"]").text(LPA["15_2_2"]);
                        $("#viewNilaiPembimbingLapangLPA [id=\"15_3_1\"]").text(LPA["15_3_1"]);
                        $("#viewNilaiPembimbingLapangLPA [id=\"15_3_2\"]").text(LPA["15_3_2"]);
                        $("#viewNilaiPembimbingLapangLPA [id=\"15_4_1\"]").text(LPA["15_4_1"]);
                        $("#viewNilaiPembimbingLapangLPA [id=\"15_4_2\"]").text(LPA["15_4_2"]);
                        $("#viewNilaiPembimbingLapangLPA [id=\"15_5_1\"]").text(LPA["15_5_1"]);
                        $("#viewNilaiPembimbingLapangLPA [id=\"15_5_2\"]").text(LPA["15_5_2"]);
                        $('#viewNilaiPembimbingLapangLPA').modal('show');
                    }
                })
            })
        });
    </script>

    <!-- Nilai LPB Pembimbing Lapang PKL -->
    <script>
        $(document).ready(function (){
            $('.viewNilaiPembimbingLapangLPB').click(function (e) {
                e.preventDefault();

                var PklId = $(this).closest('tr').find('.PklId').text();
                console.log(PklId);
                
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('pkl/ViewNilaiPembimbingLapangLPB'); ?>",
                    data: {
                        'PklId': PklId,
                    },
                    success: function (response) {
                        response = JSON.parse(response);
                        var LPB = JSON.parse(response.LP_PKL_B);
                        console.log(LPB);

                        $("#viewNilaiPembimbingLapangLPB [id=\"12_1_1\"]").text(LPB["12_1_1"]);
                        $("#viewNilaiPembimbingLapangLPB [id=\"12_1_2\"]").text(LPB["12_1_2"]);
                        $("#viewNilaiPembimbingLapangLPB [id=\"12_1_3\"]").text(LPB["12_1_3"]);
                        $("#viewNilaiPembimbingLapangLPB [id=\"12_1_4\"]").text(LPB["12_1_4"]);
                        $("#viewNilaiPembimbingLapangLPB [id=\"12_2_1\"]").text(LPB["12_2_1"]);
                        $("#viewNilaiPembimbingLapangLPB [id=\"12_2_2\"]").text(LPB["12_2_2"]);
                        $("#viewNilaiPembimbingLapangLPB [id=\"12_2_3\"]").text(LPB["12_2_3"]);
                        $("#viewNilaiPembimbingLapangLPB [id=\"12_2_4\"]").text(LPB["12_2_4"]);
                        $("#viewNilaiPembimbingLapangLPB [id=\"12_3_1\"]").text(LPB["12_3_1"]);
                        $("#viewNilaiPembimbingLapangLPB [id=\"12_3_2\"]").text(LPB["12_3_2"]);
                        $("#viewNilaiPembimbingLapangLPB [id=\"12_5_1\"]").text(LPB["12_5_1"]);
                        $("#viewNilaiPembimbingLapangLPB [id=\"12_5_2\"]").text(LPB["12_5_2"]);
                        $("#viewNilaiPembimbingLapangLPB [id=\"15_1_1\"]").text(LPB["15_1_1"]);
                        $("#viewNilaiPembimbingLapangLPB [id=\"15_1_2\"]").text(LPB["15_1_2"]);
                        $("#viewNilaiPembimbingLapangLPB [id=\"15_2_1\"]").text(LPB["15_2_1"]);
                        $("#viewNilaiPembimbingLapangLPB [id=\"15_2_2\"]").text(LPB["15_2_2"]);
                        $("#viewNilaiPembimbingLapangLPB [id=\"15_3_1\"]").text(LPB["15_3_1"]);
                        $("#viewNilaiPembimbingLapangLPB [id=\"15_3_2\"]").text(LPB["15_3_2"]);
                        $("#viewNilaiPembimbingLapangLPB [id=\"15_4_1\"]").text(LPB["15_4_1"]);
                        $("#viewNilaiPembimbingLapangLPB [id=\"15_4_2\"]").text(LPB["15_4_2"]);
                        $("#viewNilaiPembimbingLapangLPB [id=\"15_5_1\"]").text(LPB["15_5_1"]);
                        $("#viewNilaiPembimbingLapangLPB [id=\"15_5_2\"]").text(LPB["15_5_2"]);
                        $('#viewNilaiPembimbingLapangLPB').modal('show');
                    }
                })
            })
        });
    </script>

    <!-- Nilai MK1 Pembimbing Lapang PKL -->
    <script>
        $(document).ready(function (){
            $('.viewNilaiPembimbingLapangMK1').click(function (e) {
                e.preventDefault();

                var PklId = $(this).closest('tr').find('.PklId').text();
                console.log(PklId);
                
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('pkl/ViewNilaiPembimbingLapangMK1'); ?>",
                    data: {
                        'PklId': PklId,
                    },
                    success: function (response) {
                        response = JSON.parse(response);
                        var MK1 = JSON.parse(response.LP_MK1);
                        console.log(response);

                        switch (response.MataKuliah1){
                            case '1':
                                $("#viewNilaiPembimbingLapangSumberDayaAlam [id=\"1\"]").text(MK1["1"]);
                                $("#viewNilaiPembimbingLapangSumberDayaAlam [id=\"2\"]").text(MK1["2"]);
                                $("#viewNilaiPembimbingLapangSumberDayaAlam [id=\"3\"]").text(MK1["3"]);

                                $('#viewNilaiPembimbingLapangSumberDayaAlam').modal('show');
                                break;
                            case '2':
                                $("#viewNilaiPembimbingLapangKeselamatandanLingkungan [id=\"1\"]").text(MK1["1"]);
                                $("#viewNilaiPembimbingLapangKeselamatandanLingkungan [id=\"2\"]").text(MK1["2"]);
                                $("#viewNilaiPembimbingLapangKeselamatandanLingkungan [id=\"3\"]").text(MK1["3"]);

                                $('#viewNilaiPembimbingLapangKeselamatandanLingkungan').modal('show');
                                break;
                            case '3':
                                $("#viewNilaiPembimbingLapangMetodedanTerapan [id=\"1\"]").text(MK1["1"]);
                                $("#viewNilaiPembimbingLapangMetodedanTerapan [id=\"2\"]").text(MK1["2"]);
                                $("#viewNilaiPembimbingLapangMetodedanTerapan [id=\"3\"]").text(MK1["3"]);

                                $('#viewNilaiPembimbingLapangMetodedanTerapan').modal('show');
                                break;
                            case '4':
                                $("#viewNilaiPembimbingLapangAnalisisPenilaiandanTindakan [id=\"1\"]").text(MK1["1"]);
                                $("#viewNilaiPembimbingLapangAnalisisPenilaiandanTindakan [id=\"2\"]").text(MK1["2"]);
                                $("#viewNilaiPembimbingLapangAnalisisPenilaiandanTindakan [id=\"3\"]").text(MK1["3"]);

                                $('#viewNilaiPembimbingLapangAnalisisPenilaiandanTindakan').modal('show');
                                break;
                            case '5':
                                $("#viewNilaiPembimbingLapangKemandirianPengayaanPengetahuan [id=\"1\"]").text(MK1["1"]);
                                $("#viewNilaiPembimbingLapangKemandirianPengayaanPengetahuan [id=\"2\"]").text(MK1["2"]);
                                $("#viewNilaiPembimbingLapangKemandirianPengayaanPengetahuan [id=\"3\"]").text(MK1["3"]);

                                $('#viewNilaiPembimbingLapangKemandirianPengayaanPengetahuan').modal('show');
                                break;
                        }
                    }
                })
            })
        });
    </script>

    <!-- Nilai MK2 Pembimbing Lapang PKL -->
    <script>
        $(document).ready(function (){
            $('.viewNilaiPembimbingLapangMK2').click(function (e) {
                e.preventDefault();

                var PklId = $(this).closest('tr').find('.PklId').text();
                console.log(PklId);
                
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('pkl/ViewNilaiPembimbingLapangMK2'); ?>",
                    data: {
                        'PklId': PklId,
                    },
                    success: function (response) {
                        response = JSON.parse(response);
                        var MK2 = JSON.parse(response.LP_MK2);
                        console.log(response);

                        switch (response.MataKuliah2){
                            case '1':
                                $("#viewNilaiPembimbingLapangSumberDayaAlam [id=\"1\"]").text(MK2["1"]);
                                $("#viewNilaiPembimbingLapangSumberDayaAlam [id=\"2\"]").text(MK2["2"]);
                                $("#viewNilaiPembimbingLapangSumberDayaAlam [id=\"3\"]").text(MK2["3"]);

                                $('#viewNilaiPembimbingLapangSumberDayaAlam').modal('show');
                                break;
                            case '2':
                                $("#viewNilaiPembimbingLapangKeselamatandanLingkungan [id=\"1\"]").text(MK2["1"]);
                                $("#viewNilaiPembimbingLapangKeselamatandanLingkungan [id=\"2\"]").text(MK2["2"]);
                                $("#viewNilaiPembimbingLapangKeselamatandanLingkungan [id=\"3\"]").text(MK2["3"]);

                                $('#viewNilaiPembimbingLapangKeselamatandanLingkungan').modal('show');
                                break;
                            case '3':
                                $("#viewNilaiPembimbingLapangMetodedanTerapan [id=\"1\"]").text(MK2["1"]);
                                $("#viewNilaiPembimbingLapangMetodedanTerapan [id=\"2\"]").text(MK2["2"]);
                                $("#viewNilaiPembimbingLapangMetodedanTerapan [id=\"3\"]").text(MK2["3"]);

                                $('#viewNilaiPembimbingLapangMetodedanTerapan').modal('show');
                                break;
                            case '4':
                                $("#viewNilaiPembimbingLapangAnalisisPenilaiandanTindakan [id=\"1\"]").text(MK2["1"]);
                                $("#viewNilaiPembimbingLapangAnalisisPenilaiandanTindakan [id=\"2\"]").text(MK2["2"]);
                                $("#viewNilaiPembimbingLapangAnalisisPenilaiandanTindakan [id=\"3\"]").text(MK2["3"]);

                                $('#viewNilaiPembimbingLapangAnalisisPenilaiandanTindakan').modal('show');
                                break;
                            case '5':
                                $("#viewNilaiPembimbingLapangKemandirianPengayaanPengetahuan [id=\"1\"]").text(MK2["1"]);
                                $("#viewNilaiPembimbingLapangKemandirianPengayaanPengetahuan [id=\"2\"]").text(MK2["2"]);
                                $("#viewNilaiPembimbingLapangKemandirianPengayaanPengetahuan [id=\"3\"]").text(MK2["3"]);

                                $('#viewNilaiPembimbingLapangKemandirianPengayaanPengetahuan').modal('show');
                                break;
                        }
                    }
                })
            })
        });
    </script>

    <!-- Nilai MK3 Pembimbing Lapang PKL -->
    <script>
        $(document).ready(function (){
            $('.viewNilaiPembimbingLapangMK3').click(function (e) {
                e.preventDefault();

                var PklId = $(this).closest('tr').find('.PklId').text();
                console.log(PklId);
                
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('pkl/ViewNilaiPembimbingLapangMK3'); ?>",
                    data: {
                        'PklId': PklId,
                    },
                    success: function (response) {
                        response = JSON.parse(response);
                        var MK3 = JSON.parse(response.LP_MK3);
                        console.log(response);

                        switch (response.MataKuliah2){
                            case '1':
                                $("#viewNilaiPembimbingLapangSumberDayaAlam [id=\"1\"]").text(MK3["1"]);
                                $("#viewNilaiPembimbingLapangSumberDayaAlam [id=\"2\"]").text(MK3["2"]);
                                $("#viewNilaiPembimbingLapangSumberDayaAlam [id=\"3\"]").text(MK3["3"]);

                                $('#viewNilaiPembimbingLapangSumberDayaAlam').modal('show');
                                break;
                            case '2':
                                $("#viewNilaiPembimbingLapangKeselamatandanLingkungan [id=\"1\"]").text(MK3["1"]);
                                $("#viewNilaiPembimbingLapangKeselamatandanLingkungan [id=\"2\"]").text(MK3["2"]);
                                $("#viewNilaiPembimbingLapangKeselamatandanLingkungan [id=\"3\"]").text(MK3["3"]);

                                $('#viewNilaiPembimbingLapangKeselamatandanLingkungan').modal('show');
                                break;
                            case '3':
                                $("#viewNilaiPembimbingLapangMetodedanTerapan [id=\"1\"]").text(MK3["1"]);
                                $("#viewNilaiPembimbingLapangMetodedanTerapan [id=\"2\"]").text(MK3["2"]);
                                $("#viewNilaiPembimbingLapangMetodedanTerapan [id=\"3\"]").text(MK3["3"]);

                                $('#viewNilaiPembimbingLapangMetodedanTerapan').modal('show');
                                break;
                            case '4':
                                $("#viewNilaiPembimbingLapangAnalisisPenilaiandanTindakan [id=\"1\"]").text(MK3["1"]);
                                $("#viewNilaiPembimbingLapangAnalisisPenilaiandanTindakan [id=\"2\"]").text(MK3["2"]);
                                $("#viewNilaiPembimbingLapangAnalisisPenilaiandanTindakan [id=\"3\"]").text(MK3["3"]);

                                $('#viewNilaiPembimbingLapangAnalisisPenilaiandanTindakan').modal('show');
                                break;
                            case '5':
                                $("#viewNilaiPembimbingLapangKemandirianPengayaanPengetahuan [id=\"1\"]").text(MK3["1"]);
                                $("#viewNilaiPembimbingLapangKemandirianPengayaanPengetahuan [id=\"2\"]").text(MK3["2"]);
                                $("#viewNilaiPembimbingLapangKemandirianPengayaanPengetahuan [id=\"3\"]").text(MK3["3"]);

                                $('#viewNilaiPembimbingLapangKemandirianPengayaanPengetahuan').modal('show');
                                break;
                        }
                    }
                })
            })
        });
    </script>

    <!-- Hide Dropdown Mata Kuliah -->
    <script>
        function showMataKuliahFields() {
            var jenisSks = document.getElementById("inputJenisSks").value;
            var rowMK1 = document.getElementById("rowMK1");
            var rowMK2 = document.getElementById("rowMK2");
            var rowMK3 = document.getElementById("rowMK3");

            // Hide all rows initially
            rowMK1.style.display = "none";
            rowMK2.style.display = "none";
            rowMK3.style.display = "none";

            // Show rows based on jenisSks value
            if (jenisSks == 7) {
                rowMK1.style.display = "block";
            } else if (jenisSks == 10) {
                rowMK1.style.display = "block";
                rowMK2.style.display = "block";
            } else if (jenisSks == 13 || jenisSks == 16) {
                rowMK1.style.display = "block";
                rowMK2.style.display = "block";
                rowMK3.style.display = "block";
            }
        }
        
        // Call the function initially to set the initial state
        showMataKuliahFields();
    </script>

    <!-- Berkas Pkl -->
    <script>
    $(document).ready(function (){
        $('.berkasPkl').click(function (e) {
            e.preventDefault();
            var PklId = $(this).closest('tr').find('.PklId').text();
            $.ajax({
                type: "POST",
                url: "<?= base_url('Pkl/DetailPkl'); ?>",
                data: {
                    'PklId': PklId,
                },
                success: function (response) {
                    response = JSON.parse(response);
                    console.log(response);

                    /* Upload Surat Lamaran */
                    var responseUploadSuratLamaran = response.UploadSuratLamaran;
                    var linkIconSuratLamaran = document.getElementById("linkIconSuratLamaranPkl");
                    if (responseUploadSuratLamaran !== null){
                        document.getElementById("UploadSuratLamaranPkl").textContent = responseUploadSuratLamaran;
                        var responseFileSuratLamaran = "<?=base_url('/assets/uploads/berkasPendaftaran_pkl/');?>" + response.FileSuratLamaran;
                        var linkSuratLamaranPkl = document.getElementById("linkSuratLamaranPkl");
                        linkSuratLamaranPkl.href = responseFileSuratLamaran;
                        linkIconSuratLamaran.style.display ="block";
                    }else{
                        linkIconSuratLamaran.style.display ="none";
                    }

                    /* Upload Curriculum Vitae */
                    var responseUploadCVPkl = response.UploadCVMahasiswa;
                    console.log(responseUploadCVPkl);
                    var linkIconCVPkl = document.getElementById("linkIconCVPkl");
                    if (responseUploadCVPkl !== null){
                        document.getElementById("UploadCVPkl").textContent = responseUploadCVPkl;
                        var responseFileCVPkl = "<?=base_url('/assets/uploads/berkasPendaftaran_pkl/');?>" + response.FileCVMahasiswa;
                        var linkCVPkl = document.getElementById("linkCVPkl");
                        linkCVPkl.href = responseFileCVPkl;
                        linkIconCVPkl.style.display ="block";
                    }else{
                        linkIconCVPkl.style.display ="none";
                    }

                    /* Upload Proposal PKL */
                    var responseUploadProposalPkl = response.UploadProposal;
                    var linkIconProposalPkl = document.getElementById("linkIconProposalPkl");
                    if (responseUploadProposalPkl !== null){
                        document.getElementById("UploadProposalPkl").textContent = responseUploadProposalPkl;
                        var responseFileProposalPkl = "<?=base_url('/assets/uploads/berkasPendaftaran_pkl/');?>" + response.FileProposal;
                        var linkProposalPkl = document.getElementById("linkProposalPkl");
                        linkProposalPkl.href = responseFileProposalPkl;
                        linkIconProposalPkl.style.display ="block";
                    }else{
                        linkIconProposalPkl.style.display ="none";
                    }

                    /* Upload Surat Pengantar */
                    var responseUploadPengantarPkl = response.UploadSuratPengantar;
                    var linkIconPengantarPkl = document.getElementById("linkIconPengantarPkl");
                    if (responseUploadPengantarPkl !== null){
                        document.getElementById("UploadPengantarPkl").textContent = responseUploadPengantarPkl;
                        var responseFilePengantarPkl = "<?=base_url('/assets/uploads/berkasPendaftaran_pkl/');?>" + response.FileSuratPengantar;
                        var linkPengantarPkl = document.getElementById("linkPengantarPkl");
                        linkPengantarPkl.href = responseFilePengantarPkl;
                        linkIconPengantarPkl.style.display ="block";
                    }else{
                        linkIconPengantarPkl.style.display ="none";
                    }

                    /* Upload Transkrip */
                    var responseUploadTranskrip = response.UploadTranskrip;
                    var linkIconTranskrip = document.getElementById("linkIconTranskripPkl");
                    if (responseUploadTranskrip !== null){
                        document.getElementById("UploadTranskripPkl").textContent = responseUploadTranskrip;
                        var responseFileTranskrip = "<?=base_url('/assets/uploads/berkasPendaftaran_pkl/');?>" + response.FileTranskrip;
                        var linkTranskrip = document.getElementById("linkTranskripPkl");
                        linkTranskrip.href = responseFileTranskrip;
                        linkIconTranskrip.style.display ="block";
                    }else{
                        linkIconTranskrip.style.display ="none";
                    }

                    $("#berkasPkl [name=\"PklId\"]").val(response.Id);
                    $('#berkasPkl').modal('show');
                }
            })
        })
    });
    </script>

    <!-- Upload Surat Lamaran -->
    <script>
        const inputLamaran = document.getElementById('input-lamaranPkl');
        if (inputLamaran) {
            inputLamaran.addEventListener('change', function () {
                document.getElementById('submit-lamaranPkl').style.display = 'block';
            });
        }
    </script>

    <!-- Upload CV Mahasiswa -->
    <script>
        const inputCV = document.getElementById('input-CVPkl');
        if (inputCV) {
            inputCV.addEventListener('change', function () {
                document.getElementById('submit-CVPkl').style.display = 'block';
            });
        }
    </script>

    <!-- Upload Proposal -->
    <script>
        const inputProposal = document.getElementById('input-ProposalPkl');
        if (inputProposal) {
            inputProposal.addEventListener('change', function () {
                document.getElementById('submit-ProposalPkl').style.display = 'block';
            });
        }
    </script>

    <!-- Upload Surat Pengantar -->
    <script>
        const inputSuratPengantar = document.getElementById('input-PengantarPkl');
        if (inputSuratPengantar) {
            inputSuratPengantar.addEventListener('change', function () {
                document.getElementById('submit-PengantarPkl').style.display = 'block';
            });
        }
    </script>

    <!-- Upload Transkrip -->
    <script>
        const inputTranskripPKL = document.getElementById('input-TranskripPkl');
        if (inputTranskripPKL) {
            inputTranskripPKL.addEventListener('change', function () {
                document.getElementById('submit-TranskripPkl').style.display = 'block';
            });
        }
    </script>

    <!-- Upload Surat Instansi (PKL) -->
    <script>
        $(document).ready(function() {
            $(".uploadSuratInstansi").click(function() {
            // Ambil ID baris dari atribut data-rowid
            var PklId = $(this).closest('tr').find('.PklId').text();
            
            // Tampilkan modal
            $("#uploadSuratInstansi [name=\"PklId\"]").val(PklId);
            $('#uploadSuratInstansi').modal('show');
            });
        });
    </script>

    <!-- Daftar Skripsi -->
    <script>
        $(document).ready(function (){
            $('.daftarSkripsi').click(function (e) {
                e.preventDefault();

                var TaId = $(this).closest('tr').find('.TaId').text();
                
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('TugasAkhir/DetailTA'); ?>",
                    data: {
                        'TaId': TaId,
                    },
                    success: function (response) {
                        response = JSON.parse(response);
                        console.log(response);
                        var dosen1Id = response.Dosen1Id; 
                        $("#daftarSkripsi [name=\"dosen1\"]").val(dosen1Id).trigger('change'); 
                        var dosen2Id = response.Dosen2Id; 
                        $("#daftarSkripsi [name=\"dosen2\"]").val(dosen2Id).trigger('change'); 
                        var dosen3Id = response.Dosen3Id; 
                        $("#daftarSkripsi [name=\"dosen3\"]").val(dosen3Id).trigger('change'); 

                        $("#daftarSkripsi [name=\"dosen1\"]").select2();
                        $("#daftarSkripsi [name=\"dosen2\"]").select2();
                        $("#daftarSkripsi [name=\"dosen3\"]").select2();
                        $("#daftarSkripsi [name=\"TaId\"]").val(response.Id);
                        
                        var maxDate = response.MaxDateSkripsi;
                        var inputTanggalUjian = document.getElementById("inputTanggalUjianSkripsiDaftar");
                        inputTanggalUjian.setAttribute("max", maxDate);

                        var radioButtons = document.getElementsByName("jenisPenilaian");

                        for (var i = 0; i < radioButtons.length; i++) {
                            if (radioButtons[i].value === response.JenisPenilaian) {
                                radioButtons[i].checked = true;
                                break; 
                            }
                        }

                        $('#daftarSkripsi').modal('show');
                    }
                })
            })
        });
    </script>

    <!-- Upload Makalah Skripsi -->
    <script>
        $(document).ready(function() {
            $(".uploadSkripsi").click(function() {
            // Ambil ID baris dari atribut data-rowid
            var TaId = $(this).closest('tr').find('.TaId').text();
            
            // Tampilkan modal
            $("#uploadMakalahSkripsi [name=\"TaId\"]").val(TaId);
            $('#uploadMakalahSkripsi').modal('show');
            });
        });
    </script>

    <!-- Detail Skripsi -->
    <script>
        $(document).ready(function (){
            $('.detailSkripsi').click(function (e) {
                e.preventDefault();

                var TaId = $(this).closest('tr').find('.TaId').text();
                
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('TugasAkhir/DetailSkripsi'); ?>",
                    data: {
                        'TaId': TaId,
                    },
                    success: function (response) {
                        response = JSON.parse(response);
                        console.log(response);

                        $("#detailSkripsi [name=\"taId\"]").val(response.TaId);
                        $("#detailSkripsi [name=\"nama\"]").val(response.Name);
                        $("#detailSkripsi [name=\"username\"]").val(response.Username);
                        $("#detailSkripsi [name=\"bidangMinat\"]").val(response.Bidang);

                        $("#detailSkripsi [name=\"dosen1\"]").val(response.Dosen1Nama);
                        $("#detailSkripsi [name=\"dosen2\"]").val(response.Dosen2Nama);
                        $("#detailSkripsi [name=\"dosen3\"]").val(response.Dosen3Nama);
                        
                        $("#detailSkripsi [name=\"tanggalUjian\"]").val(response.TanggalUjian);
                        $("#detailSkripsi [name=\"waktu\"]").val(response.Waktu);
                        $("#detailSkripsi [name=\"ruangan\"]").val(response.RuangNama);
                        $("#detailSkripsi [name=\"linkZoom\"]").val(response.LinkZoom);
                        $("#detailSkripsi [name=\"judulIdn\"]").val(response.JudulIdn);
                        $("#detailSkripsi [name=\"judulEng\"]").val(response.JudulEng);

                        $('#detailSkripsi').modal('show');
                    }
                })
            })
        });
    </script>

    <!-- Nilai Presentasi Skripsi -->
    <script>
        $(document).ready(function (){
            $('.PresentasiSkripsi').click(function (e) {
                e.preventDefault();
                
                var presentasiSkripsiElements = document.querySelectorAll('.PresentasiSkripsi');
                var TaId = document.getElementById("TaId");
                
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('TugasAkhir/DetailNilaiSkripsi'); ?>",
                    data: {
                        'TaId': TaId.value,
                    },
                    success: function (response) {
                        response = JSON.parse(response);
                        
                        presentasiSkripsiElements.forEach(function(element) {
                            element.addEventListener('click', function() {
                                if (element.classList.contains('dosen1')) {
                                    $("#PresentasiSkripsi [name=\"Present1\"]").val(response.Dsn1Pres1);
                                    $("#PresentasiSkripsi [name=\"Present2\"]").val(response.Dsn1Pres2);
                                    $("#PresentasiSkripsi [name=\"Present3\"]").val(response.Dsn1Pres3);
                                    $("#PresentasiSkripsi [name=\"Present4\"]").val(response.Dsn1Pres4);
                                } else if (element.classList.contains('dosen2')) {
                                    $("#PresentasiSkripsi [name=\"Present1\"]").val(response.Dsn2Pres1);
                                    $("#PresentasiSkripsi [name=\"Present2\"]").val(response.Dsn2Pres2);
                                    $("#PresentasiSkripsi [name=\"Present3\"]").val(response.Dsn2Pres3);
                                    $("#PresentasiSkripsi [name=\"Present4\"]").val(response.Dsn2Pres4);
                                } else if (element.classList.contains('dosen3')) {
                                    $("#PresentasiSkripsi [name=\"Present1\"]").val(response.Dsn3Pres1);
                                    $("#PresentasiSkripsi [name=\"Present2\"]").val(response.Dsn3Pres2);
                                    $("#PresentasiSkripsi [name=\"Present3\"]").val(response.Dsn3Pres3);
                                    $("#PresentasiSkripsi [name=\"Present4\"]").val(response.Dsn3Pres4);
                                }

                                $('#PresentasiSkripsi').modal('show');
                            });
                        });
                    }
                })
            })
        });
    </script>

    <!-- Nilai Berkas Skripsi -->
    <script>
        $(document).ready(function (){
            $('.BerkasSkripsi').click(function (e) {
                e.preventDefault();
                
                var berkasSkripsiElements = document.querySelectorAll('.BerkasSkripsi');
                var TaId = document.getElementById("TaId");
                
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('TugasAkhir/DetailNilaiSkripsi'); ?>",
                    data: {
                        'TaId': TaId.value,
                    },
                    success: function (response) {
                        response = JSON.parse(response);
                        
                        console.log(response);
                        berkasSkripsiElements.forEach(function(element) {
                            element.addEventListener('click', function() {
                                if (element.classList.contains('dosen1')) {
                                    $("#BerkasSkripsi [name=\"Berkas1\"]").val(response.Dsn1Ber1);
                                    $("#BerkasSkripsi [name=\"Berkas2\"]").val(response.Dsn1Ber2);
                                    // $("#BerkasSkripsi [name=\"Berkas3\"]").val(response.Dsn1Ber3);
                                    $("#BerkasSkripsi [name=\"Berkas4\"]").val(response.Dsn1Ber4);
                                } else if (element.classList.contains('dosen2')) {
                                    $("#BerkasSkripsi [name=\"Berkas1\"]").val(response.Dsn2Ber1);
                                    $("#BerkasSkripsi [name=\"Berkas2\"]").val(response.Dsn2Ber2);
                                    // $("#BerkasSkripsi [name=\"Berkas3\"]").val(response.Dsn2Ber3);
                                    $("#BerkasSkripsi [name=\"Berkas4\"]").val(response.Dsn2Ber4);
                                } else if (element.classList.contains('dosen3')) {
                                    $("#BerkasSkripsi [name=\"Berkas1\"]").val(response.Dsn3Ber1);
                                    $("#BerkasSkripsi [name=\"Berkas2\"]").val(response.Dsn3Ber2);
                                    // $("#BerkasSkripsi [name=\"Berkas3\"]").val(response.Dsn3Ber3);
                                    $("#BerkasSkripsi [name=\"Berkas4\"]").val(response.Dsn3Ber4);
                                }

                                $('#BerkasSkripsi').modal('show');
                            });
                        });
                    }
                })
            })
        });
    </script>

    <!-- Nilai Naskah Skripsi -->
    <script>
        $(document).ready(function (){
            $('.NaskahSkripsi').click(function (e) {
                e.preventDefault();
                
                var naskahSkripsiElements = document.querySelectorAll('.NaskahSkripsi');
                var TaId = document.getElementById("TaId");
                
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('TugasAkhir/DetailNilaiSkripsi'); ?>",
                    data: {
                        'TaId': TaId.value,
                    },
                    success: function (response) {
                        response = JSON.parse(response);
                        
                        console.log(response);
                        naskahSkripsiElements.forEach(function(element) {
                            element.addEventListener('click', function() {
                                if (element.classList.contains('dosen1')) {
                                    $("#NaskahSkripsi [name=\"Naskah1\"]").val(response.Dsn1Skripsi1);
                                    $("#NaskahSkripsi [name=\"Naskah2\"]").val(response.Dsn1Skripsi2);
                                    $("#NaskahSkripsi [name=\"Naskah3\"]").val(response.Dsn1Skripsi3);
                                    $("#NaskahSkripsi [name=\"Naskah4\"]").val(response.Dsn1Skripsi4);
                                    $("#NaskahSkripsi [name=\"Naskah5\"]").val(response.Dsn1Skripsi5);
                                } else if (element.classList.contains('dosen2')) {
                                    $("#NaskahSkripsi [name=\"Naskah1\"]").val(response.Dsn2Skripsi1);
                                    $("#NaskahSkripsi [name=\"Naskah2\"]").val(response.Dsn2Skripsi2);
                                    $("#NaskahSkripsi [name=\"Naskah3\"]").val(response.Dsn2Skripsi3);
                                    $("#NaskahSkripsi [name=\"Naskah4\"]").val(response.Dsn2Skripsi4);
                                    $("#NaskahSkripsi [name=\"Naskah5\"]").val(response.Dsn2Skripsi5);
                                }

                                $('#NaskahSkripsi').modal('show');
                            });
                        });
                    }
                })
            })
        });
    </script>

    <!-- Edit Skripsi -->
    <script>
        $(document).ready(function (){
            $('.editSkripsi').click(function (e) {
                e.preventDefault();

                var TaId = $(this).closest('tr').find('.TaId').text();
                
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('TugasAkhir/DetailSkripsi'); ?>",
                    data: {
                        'TaId': TaId,
                    },
                    success: function (response) {
                        response = JSON.parse(response);
                        console.log(response);

                        var dosen1Id = response.Dosen1Id; 
                        $("#editSkripsi [name=\"dosen1\"]").val(dosen1Id).trigger('change'); 
                        var dosen2Id = response.Dosen2Id; 
                        $("#editSkripsi [name=\"dosen2\"]").val(dosen2Id).trigger('change'); 
                        var dosen3Id = response.Dosen3Id; 
                        $("#editSkripsi [name=\"dosen3\"]").val(dosen3Id).trigger('change'); 
                        var RuangId = response.RuangId; 
                        $("#editSkripsi [name=\"ruangId\"]").val(RuangId).trigger('change'); 
                        var JamMulai = response.JamMulai; 
                        var arrayJamMulai = JamMulai.split(':');
                        var jamMulai = arrayJamMulai[0];
                        var menitMulai = arrayJamMulai[1];
                        var JamMenitMulai = jamMulai + ":" + menitMulai;
                        $("#editSkripsi [name=\"jamMulai3\"]").val(JamMulai).trigger('change'); 
                        var JamSelesai = response.JamSelesai; 
                        var arrayJamSelesai = JamSelesai.split(':');
                        var jamSelesai = arrayJamSelesai[0];
                        var menitSelesai = arrayJamSelesai[1];
                        var JamMenitSelesai = jamSelesai + ":" + menitSelesai;
                        $("#editSkripsi [name=\"jamSelesai3\"]").val(JamSelesai).trigger('change'); 

                        $("#editSkripsi [name=\"dosen1\"]").select2();
                        $("#editSkripsi [name=\"dosen2\"]").select2();
                        $("#editSkripsi [name=\"dosen3\"]").select2();
                        $("#editSkripsi [name=\"ruangId\"]").select2();
                        $("#editSkripsi [name=\"jamMulai3\"]").select2();
                        $("#editSkripsi [name=\"jamSelesai3\"]").select2();

                        $("#editSkripsi [name=\"tanggalUjian\"]").val(response.TanggalUjian);
                        $("#editSkripsi [name=\"TaId\"]").val(response.TaId);
                        $("#editSkripsi [name=\"linkZoom\"]").val(response.LinkZoom);

                        var maxDate = response.MaxDateSkripsi;
                        var inputTanggalUjian = document.getElementById("inputTanggalUjianSkripsiEdit");
                        inputTanggalUjian.setAttribute("max", maxDate);

                        var radioButtons = document.getElementsByName("jenisPenilaianSkripsi");

                        for (var i = 0; i < radioButtons.length; i++) {
                            if (radioButtons[i].value === response.JenisPenilaian) {
                                radioButtons[i].checked = true;
                                break; 
                            }
                        }

                        $('#editSkripsi').modal('show');
                    }
                })
            })
        });
    </script>

    <!-- Download Berita Acara -->
    <script>
        $(document).ready(function (){
            $('.downloadBASkripsi').click(function (e) {
                e.preventDefault();

                var TaId = $(this).closest('tr').find('.TaId').text();
                var url = '<?= base_url('TugasAkhir/DownloadBASkripsi/') ?>' + TaId;
                
                console.log(TaId);
                fetch(url)
                .then(response => response.blob())
                .then(blob => {
                // Create a temporary link and trigger a download
                var a = document.createElement('a');
                a.href = window.URL.createObjectURL(blob);
                a.download = TaId + '-BeritaAcara.docx';
                document.body.appendChild(a);
                a.style.display = 'none';
                a.click();
                document.body.removeChild(a);
                })
                .catch(error => console.error('Error downloading file:', error));
            })
        });
        
    </script>

    <!-- Daftar Semju -->
    <script>
        $(document).ready(function (){
            $('.daftarSemju').click(function (e) {
                e.preventDefault();

                var TaId = $(this).closest('tr').find('.TaId').text();
                
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('TugasAkhir/DetailTA'); ?>",
                    data: {
                        'TaId': TaId,
                    },
                    success: function (response) {
                        response = JSON.parse(response);
                        console.log(response);
                        var dosen1Id = response.Dosen1Id; 
                        $("#daftarSemju [name=\"dosen1\"]").val(dosen1Id).trigger('change'); 
                        var dosen2Id = response.Dosen2Id; 
                        $("#daftarSemju [name=\"dosen2\"]").val(dosen2Id).trigger('change'); 
                        var dosen3Id = response.Dosen3Id; 
                        $("#daftarSemju [name=\"dosen3\"]").val(dosen3Id).trigger('change'); 

                        $("#daftarSemju [name=\"dosen1\"]").select2();
                        $("#daftarSemju [name=\"dosen2\"]").select2();
                        $("#daftarSemju [name=\"dosen3\"]").select2();
                        $("#daftarSemju [name=\"TaId\"]").val(response.Id);
                        
                        var maxDate = response.MaxDateSemju;
                        var inputTanggalUjian = document.getElementById("inputTanggalUjianSemjuDaftar");
                        inputTanggalUjian.setAttribute("max", maxDate);

                        var radioButtons = document.getElementsByName("jenisPenilaian");

                        for (var i = 0; i < radioButtons.length; i++) {
                            if (radioButtons[i].value === response.JenisPenilaian) {
                                radioButtons[i].checked = true;
                                break; 
                            }
                        }

                        $('#daftarSemju').modal('show');
                    }
                })
            })
        });
    </script>

    <!-- Upload Makalah Semju -->
    <script>
        $(document).ready(function() {
            $(".uploadSemju").click(function() {
            // Ambil ID baris dari atribut data-rowid
            var TaId = $(this).closest('tr').find('.TaId').text();
            
            // Tampilkan modal
            $("#uploadMakalahSemju [name=\"TaId\"]").val(TaId);
            $('#uploadMakalahSemju').modal('show');
            });
        });
    </script>

    <!-- Detail Semju -->
    <script>
        $(document).ready(function (){
            $('.detailSemju').click(function (e) {
                e.preventDefault();

                var TaId = $(this).closest('tr').find('.TaId').text();
                
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('TugasAkhir/DetailSemju'); ?>",
                    data: {
                        'TaId': TaId,
                    },
                    success: function (response) {
                        response = JSON.parse(response);
                        console.log(response);

                        $("#detailSemju [name=\"taId\"]").val(response.TaId);
                        $("#detailSemju [name=\"nama\"]").val(response.Name);
                        $("#detailSemju [name=\"username\"]").val(response.Username);
                        $("#detailSemju [name=\"bidangMinat\"]").val(response.Bidang);

                        $("#detailSemju [name=\"dosen1\"]").val(response.Dosen1Nama);
                        $("#detailSemju [name=\"dosen2\"]").val(response.Dosen2Nama);
                        $("#detailSemju [name=\"dosen3\"]").val(response.Dosen3Nama);
                        
                        $("#detailSemju [name=\"tanggalUjian\"]").val(response.TanggalUjian);
                        $("#detailSemju [name=\"waktu\"]").val(response.Waktu);
                        $("#detailSemju [name=\"ruangan\"]").val(response.RuangNama);
                        $("#detailSemju [name=\"linkZoom\"]").val(response.LinkZoom);
                        $("#detailSemju [name=\"judulIdn\"]").val(response.JudulIdn);
                        $("#detailSemju [name=\"judulEng\"]").val(response.JudulEng);

                        $('#detailSemju').modal('show');
                    }
                })
            })
        });
    </script>

    <!-- Nilai Presentasi Semju -->
    <script>
        $(document).ready(function (){
            $('.PresentasiSemju').click(function (e) {
                e.preventDefault();
                
                var presentasiSemjuElements = document.querySelectorAll('.PresentasiSemju');
                var TaId = document.getElementById("TaId");
                
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('TugasAkhir/DetailNilaiSemju'); ?>",
                    data: {
                        'TaId': TaId.value,
                    },
                    success: function (response) {
                        response = JSON.parse(response);
                        
                        presentasiSemjuElements.forEach(function(element) {
                            element.addEventListener('click', function() {
                                if (element.classList.contains('dosen1')) {
                                    $("#PresentasiSemju [name=\"Present1\"]").val(response.Dsn1Pres1);
                                    $("#PresentasiSemju [name=\"Present2\"]").val(response.Dsn1Pres2);
                                    $("#PresentasiSemju [name=\"Present3\"]").val(response.Dsn1Pres3);
                                    $("#PresentasiSemju [name=\"Present4\"]").val(response.Dsn1Pres4);
                                } else if (element.classList.contains('dosen2')) {
                                    $("#PresentasiSemju [name=\"Present1\"]").val(response.Dsn2Pres1);
                                    $("#PresentasiSemju [name=\"Present2\"]").val(response.Dsn2Pres2);
                                    $("#PresentasiSemju [name=\"Present3\"]").val(response.Dsn2Pres3);
                                    $("#PresentasiSemju [name=\"Present4\"]").val(response.Dsn2Pres4);
                                } else if (element.classList.contains('dosen3')) {
                                    $("#PresentasiSemju [name=\"Present1\"]").val(response.Dsn3Pres1);
                                    $("#PresentasiSemju [name=\"Present2\"]").val(response.Dsn3Pres2);
                                    $("#PresentasiSemju [name=\"Present3\"]").val(response.Dsn3Pres3);
                                    $("#PresentasiSemju [name=\"Present4\"]").val(response.Dsn3Pres4);
                                }

                                $('#PresentasiSemju').modal('show');
                            });
                        });
                    }
                })
            })
        });
    </script>

    <!-- Nilai Naskah Semju -->
    <script>
        $(document).ready(function (){
            $('.NaskahSemju').click(function (e) {
                e.preventDefault();
                
                var naskahSemjuElements = document.querySelectorAll('.NaskahSemju');
                var TaId = document.getElementById("TaId");
                
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('TugasAkhir/DetailNilaiSemju'); ?>",
                    data: {
                        'TaId': TaId.value,
                    },
                    success: function (response) {
                        response = JSON.parse(response);
                        
                        console.log(response);
                        naskahSemjuElements.forEach(function(element) {
                            element.addEventListener('click', function() {
                                if (element.classList.contains('dosen1')) {
                                    $("#NaskahSemju [name=\"Naskah1\"]").val(response.Dsn1Ber1);
                                    $("#NaskahSemju [name=\"Naskah2\"]").val(response.Dsn1Ber2);
                                    $("#NaskahSemju [name=\"Naskah3\"]").val(response.Dsn1Ber3);
                                    $("#NaskahSemju [name=\"Naskah4\"]").val(response.Dsn1Ber4);
                                } else if (element.classList.contains('dosen2')) {
                                    $("#NaskahSemju [name=\"Naskah1\"]").val(response.Dsn2Ber1);
                                    $("#NaskahSemju [name=\"Naskah2\"]").val(response.Dsn2Ber2);
                                    $("#NaskahSemju [name=\"Naskah3\"]").val(response.Dsn2Ber3);
                                    $("#NaskahSemju [name=\"Naskah4\"]").val(response.Dsn2Ber4);
                                } else if (element.classList.contains('dosen3')) {
                                    $("#NaskahSemju [name=\"Naskah1\"]").val(response.Dsn3Ber1);
                                    $("#NaskahSemju [name=\"Naskah2\"]").val(response.Dsn3Ber2);
                                    $("#NaskahSemju [name=\"Naskah3\"]").val(response.Dsn3Ber3);
                                    $("#NaskahSemju [name=\"Naskah4\"]").val(response.Dsn3Ber4);
                                }

                                $('#NaskahSemju').modal('show');
                            });
                        });
                    }
                })
            })
        });
    </script>

    <!-- Edit Semju -->
    <script>
        $(document).ready(function (){
            $('.editSemju').click(function (e) {
                e.preventDefault();

                var TaId = $(this).closest('tr').find('.TaId').text();
                
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('TugasAkhir/DetailSemju'); ?>",
                    data: {
                        'TaId': TaId,
                    },
                    success: function (response) {
                        response = JSON.parse(response);
                        console.log(response);

                        var dosen1Id = response.Dosen1Id; 
                        $("#editSemju [name=\"dosen1\"]").val(dosen1Id).trigger('change'); 
                        var dosen2Id = response.Dosen2Id; 
                        $("#editSemju [name=\"dosen2\"]").val(dosen2Id).trigger('change'); 
                        var dosen3Id = response.Dosen3Id; 
                        $("#editSemju [name=\"dosen3\"]").val(dosen3Id).trigger('change'); 
                        var RuangId = response.RuangId; 
                        $("#editSemju [name=\"ruangId\"]").val(RuangId).trigger('change'); 
                        var JamMulai = response.JamMulai; 
                        var arrayJamMulai = JamMulai.split(':');
                        var jamMulai = arrayJamMulai[0];
                        var menitMulai = arrayJamMulai[1];
                        var JamMenitMulai = jamMulai + ":" + menitMulai;
                        $("#editSemju [name=\"jamMulai\"]").val(JamMenitMulai).trigger('change'); 
                        var JamSelesai = response.JamSelesai; 
                        var arrayJamSelesai = JamSelesai.split(':');
                        var jamSelesai = arrayJamSelesai[0];
                        var menitSelesai = arrayJamSelesai[1];
                        var JamMenitSelesai = jamSelesai + ":" + menitSelesai;
                        $("#editSemju [name=\"jamSelesai3\"]").val(JamSelesai).trigger('change'); 

                        $("#editSemju [name=\"dosen1\"]").select2();
                        $("#editSemju [name=\"dosen2\"]").select2();
                        $("#editSemju [name=\"dosen3\"]").select2();
                        $("#editSemju [name=\"ruangId\"]").select2();
                        $("#editSemju [name=\"jamMulai3\"]").select2();
                        $("#editSemju [name=\"jamSelesai3\"]").select2();

                        $("#editSemju [name=\"tanggalUjian\"]").val(response.TanggalUjian);
                        $("#editSemju [name=\"TaId\"]").val(response.TaId);
                        $("#editSemju [name=\"linkZoom\"]").val(response.LinkZoom);

                        var maxDate = response.MaxDateSemju;
                        var inputTanggalUjian = document.getElementById("inputTanggalUjianSemjuEdit");
                        inputTanggalUjian.setAttribute("max", maxDate);

                        var radioButtons = document.getElementsByName("jenisPenilaianSemju");

                        for (var i = 0; i < radioButtons.length; i++) {
                            if (radioButtons[i].value === response.JenisPenilaian) {
                                radioButtons[i].checked = true;
                                break; 
                            }
                        }

                        $('#editSemju').modal('show');
                    }
                })
            })
        });
    </script>

    <!-- Daftar Sempro -->
    <script>
        $(document).ready(function (){
            $('.daftarSempro').click(function (e) {
                e.preventDefault();

                var TaId = $(this).closest('tr').find('.TaId').text();
                
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('TugasAkhir/DetailTA'); ?>",
                    data: {
                        'TaId': TaId,
                    },
                    success: function (response) {
                        response = JSON.parse(response);
                        console.log(response);
                        var dosen1Id = response.Dosen1Id; 
                        $("#daftarSempro [name=\"dosen1\"]").val(dosen1Id).trigger('change'); 
                        var dosen2Id = response.Dosen2Id; 
                        $("#daftarSempro [name=\"dosen2\"]").val(dosen2Id).trigger('change'); 
                        var dosen3Id = response.Dosen3Id; 
                        $("#daftarSempro [name=\"dosen3\"]").val(dosen3Id).trigger('change'); 

                        $("#daftarSempro [name=\"dosen1\"]").select2();
                        $("#daftarSempro [name=\"dosen2\"]").select2();
                        $("#daftarSempro [name=\"dosen3\"]").select2();
                        $("#daftarSempro [name=\"TaId\"]").val(response.Id);

                        var maxDate = response.MaxDateSempro;
                        var inputTanggalUjian = document.getElementById("inputTanggalUjianSemproDaftar");
                        inputTanggalUjian.setAttribute("max", maxDate);

                        var radioButtons = document.getElementsByName("jenisPenilaian");

                        for (var i = 0; i < radioButtons.length; i++) {
                            if (radioButtons[i].value === response.JenisPenilaian) {
                                radioButtons[i].checked = true;
                                break; 
                            }
                        }

                        $('#daftarSempro').modal('show');
                    }
                })
            })
        });
    </script>

    <!-- Upload Makalah Sempro -->
    <script>
        $(document).ready(function() {
            $(".uploadSempro").click(function() {
            // Ambil ID baris dari atribut data-rowid
            var TaId = $(this).closest('tr').find('.TaId').text();
            
            // Tampilkan modal
            $("#uploadMakalahSempro [name=\"TaId\"]").val(TaId);
            $('#uploadMakalahSempro').modal('show');
            });
        });
    </script>

    <!-- Detail Sempro -->
    <script>
        $(document).ready(function (){
            $('.detailSempro').click(function (e) {
                e.preventDefault();

                var TaId = $(this).closest('tr').find('.TaId').text();
                
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('TugasAkhir/DetailSempro'); ?>",
                    data: {
                        'TaId': TaId,
                    },
                    success: function (response) {
                        response = JSON.parse(response);
                        console.log(response);

                        $("#detailSempro [name=\"taId\"]").val(response.TaId);
                        $("#detailSempro [name=\"nama\"]").val(response.Name);
                        $("#detailSempro [name=\"username\"]").val(response.Username);
                        $("#detailSempro [name=\"bidangMinat\"]").val(response.Bidang);

                        $("#detailSempro [name=\"dosen1\"]").val(response.Dosen1Nama);
                        $("#detailSempro [name=\"dosen2\"]").val(response.Dosen2Nama);
                        $("#detailSempro [name=\"dosen3\"]").val(response.Dosen3Nama);
                        
                        $("#detailSempro [name=\"tanggalUjian\"]").val(response.TanggalUjian);
                        $("#detailSempro [name=\"waktu\"]").val(response.Waktu);
                        $("#detailSempro [name=\"ruangan\"]").val(response.RuangNama);
                        $("#detailSempro [name=\"linkZoom\"]").val(response.LinkZoom);
                        $("#detailSempro [name=\"judulIdn\"]").val(response.JudulIdn);
                        $("#detailSempro [name=\"judulEng\"]").val(response.JudulEng);

                        $('#detailSempro').modal('show');
                    }
                })
            })
        });
    </script>

    <!-- Nilai Presentasi Sempro -->
    <script>
        $(document).ready(function (){
            $('.PresentasiSempro').click(function (e) {
                e.preventDefault();
                
                var presentasiSemproElements = document.querySelectorAll('.PresentasiSempro');
                var TaId = document.getElementById("TaId");
                
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('TugasAkhir/DetailNilaiSempro'); ?>",
                    data: {
                        'TaId': TaId.value,
                    },
                    success: function (response) {
                        response = JSON.parse(response);
                        
                        presentasiSemproElements.forEach(function(element) {
                            element.addEventListener('click', function() {
                                if (element.classList.contains('dosen1')) {
                                    $("#PresentasiSempro [name=\"Present1\"]").val(response.Dsn1Pres1);
                                    $("#PresentasiSempro [name=\"Present2\"]").val(response.Dsn1Pres2);
                                    $("#PresentasiSempro [name=\"Present3\"]").val(response.Dsn1Pres3);
                                    $("#PresentasiSempro [name=\"Present4\"]").val(response.Dsn1Pres4);
                                } else if (element.classList.contains('dosen2')) {
                                    $("#PresentasiSempro [name=\"Present1\"]").val(response.Dsn2Pres1);
                                    $("#PresentasiSempro [name=\"Present2\"]").val(response.Dsn2Pres2);
                                    $("#PresentasiSempro [name=\"Present3\"]").val(response.Dsn2Pres3);
                                    $("#PresentasiSempro [name=\"Present4\"]").val(response.Dsn2Pres4);
                                } else if (element.classList.contains('dosen3')) {
                                    $("#PresentasiSempro [name=\"Present1\"]").val(response.Dsn3Pres1);
                                    $("#PresentasiSempro [name=\"Present2\"]").val(response.Dsn3Pres2);
                                    $("#PresentasiSempro [name=\"Present3\"]").val(response.Dsn3Pres3);
                                    $("#PresentasiSempro [name=\"Present4\"]").val(response.Dsn3Pres4);
                                }

                                $('#PresentasiSempro').modal('show');
                            });
                        });
                    }
                })
            })
        });
    </script>

    <!-- Nilai Naskah Sempro -->
    <script>
        $(document).ready(function (){
            $('.NaskahSempro').click(function (e) {
                e.preventDefault();
                
                var naskahSemproElements = document.querySelectorAll('.NaskahSempro');
                var TaId = document.getElementById("TaId");
                
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('TugasAkhir/DetailNilaiSempro'); ?>",
                    data: {
                        'TaId': TaId.value,
                    },
                    success: function (response) {
                        response = JSON.parse(response);
                        
                        naskahSemproElements.forEach(function(element) {
                            element.addEventListener('click', function() {
                                if (element.classList.contains('dosen1')) {
                                    $("#NaskahSempro [name=\"Naskah1\"]").val(response.Dsn1Nas1);
                                    $("#NaskahSempro [name=\"Naskah2\"]").val(response.Dsn1Nas2);
                                    $("#NaskahSempro [name=\"Naskah3\"]").val(response.Dsn1Nas3);
                                    $("#NaskahSempro [name=\"Naskah4\"]").val(response.Dsn1Nas4);
                                } else if (element.classList.contains('dosen2')) {
                                    $("#NaskahSempro [name=\"Naskah1\"]").val(response.Dsn2Nas1);
                                    $("#NaskahSempro [name=\"Naskah2\"]").val(response.Dsn2Nas2);
                                    $("#NaskahSempro [name=\"Naskah3\"]").val(response.Dsn2Nas3);
                                    $("#NaskahSempro [name=\"Naskah4\"]").val(response.Dsn2Nas4);
                                } else if (element.classList.contains('dosen3')) {
                                    $("#NaskahSempro [name=\"Naskah1\"]").val(response.Dsn3Nas1);
                                    $("#NaskahSempro [name=\"Naskah2\"]").val(response.Dsn3Nas2);
                                    $("#NaskahSempro [name=\"Naskah3\"]").val(response.Dsn3Nas3);
                                    $("#NaskahSempro [name=\"Naskah4\"]").val(response.Dsn3Nas4);
                                }

                                $('#NaskahSempro').modal('show');
                            });
                        });
                    }
                })
            })
        });
    </script>

    <!-- Edit Sempro -->
    <script>
        $(document).ready(function (){
            $('.editSempro').click(function (e) {
                e.preventDefault();

                var TaId = $(this).closest('tr').find('.TaId').text();
                
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('TugasAkhir/DetailSempro'); ?>",
                    data: {
                        'TaId': TaId,
                    },
                    success: function (response) {
                        response = JSON.parse(response);
                        console.log(response);

                        var dosen1Id = response.Dosen1Id; 
                        $("#editSempro [name=\"dosen1\"]").val(dosen1Id).trigger('change'); 
                        var dosen2Id = response.Dosen2Id; 
                        $("#editSempro [name=\"dosen2\"]").val(dosen2Id).trigger('change'); 
                        var dosen3Id = response.Dosen3Id; 
                        $("#editSempro [name=\"dosen3\"]").val(dosen3Id).trigger('change'); 
                        var RuangId = response.RuangId; 
                        $("#editSempro [name=\"ruangId\"]").val(RuangId).trigger('change'); 
                        var JamMulai = response.JamMulai;
                        var arrayJamMulai = JamMulai.split(':');
                        var jamMulai = arrayJamMulai[0];
                        var menitMulai = arrayJamMulai[1];
                        var JamMenitMulai = jamMulai + ":" + menitMulai;
                        $("#editSempro [name=\"jamMulai2\"]").val(JamMulai).trigger('change'); 
                        var JamSelesai = response.JamSelesai; 
                        var arrayJamSelesai = JamSelesai.split(':');
                        var jamSelesai = arrayJamSelesai[0];
                        var menitSelesai = arrayJamSelesai[1];
                        var JamMenitSelesai = jamSelesai + ":" + menitSelesai;
                        $("#editSempro [name=\"jamSelesai2\"]").val(JamSelesai).trigger('change'); 

                        $("#editSempro [name=\"dosen1\"]").select2();
                        $("#editSempro [name=\"dosen2\"]").select2();
                        $("#editSempro [name=\"dosen3\"]").select2();
                        $("#editSempro [name=\"ruangId\"]").select2();
                        $("#editSempro [name=\"jamMulai2\"]").select2();
                        $("#editSempro [name=\"jamSelesai2\"]").select2();

                        $("#editSempro [name=\"tanggalUjian\"]").val(response.TanggalUjian);
                        $("#editSempro [name=\"TaId\"]").val(response.TaId);
                        $("#editSempro [name=\"linkZoom\"]").val(response.LinkZoom);

                        var maxDate = response.MaxDateSempro;
                        var inputTanggalUjian = document.getElementById("inputTanggalUjianSemproEdit");
                        inputTanggalUjian.setAttribute("max", maxDate);

                        var radioButtons = document.getElementsByName("jenisPenilaianSempro");

                        for (var i = 0; i < radioButtons.length; i++) {
                            if (radioButtons[i].value === response.JenisPenilaian) {
                                radioButtons[i].checked = true;
                                break; 
                            }
                        }

                        $('#editSempro').modal('show');
                    }
                })
            })
        });
    </script>
    
    <!-- Edit TA -->
    <script>
        $(document).ready(function (){
            $('.editTa').click(function (e) {
                e.preventDefault();

                var TaId = $(this).closest('tr').find('.TaId').text();
                var Dosen1Id = $(this).closest('tr').find('.Dosen1Id').text();
                var Dosen2Id = $(this).closest('tr').find('.Dosen2Id').text();
                console.log(Dosen2Id);
                
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('tugasAkhir/DetailTa'); ?>",
                    data: {
                        'TaId': TaId,
                        'Dosen1Id' : Dosen1Id,
                        'Dosen2Id' : Dosen2Id,
                    },
                    success: function (response) {
                        response = JSON.parse(response);
                        console.log(response);

                        var dosen1Id = response.Dosen1Id; 
                        $("#editTa [name=\"Dosen1\"]").val(dosen1Id).trigger('change'); 
                        var dosen2Id = response.Dosen2Id; 
                        $("#editTa [name=\"Dosen2\"]").val(dosen2Id).trigger('change'); 

                        $("#editTa [name=\"TaId\"]").val(response.Id);
                        $("#editTa [name=\"semester\"]").val(response.Semester);
                        $("#editTa [name=\"tahunAkademik\"]").val(response.TahunAkademik);
                        $("#editTa [name=\"jumlahSKS\"]").val(response.JumlahSks);
                        $("#editTa [name=\"IPK\"]").val(response.IPK);
                        $("#editTa [name=\"bidangMinat\"]").val(response.BidangMinat);
                        $("#editTa [name=\"Dosen1\"]").val(response.Dosen1Id);
                        $("#editTa [name=\"Dosen1\"]").select2().select2();
                        $("#editTa [name=\"Dosen2\"]").select2().select2();
                        $("#editTa [name=\"judulIdn\"]").val(response.JudulIdn);
                        $("#editTa [name=\"judulEng\"]").val(response.JudulEng);
                        $('#editTa').modal('show');
                    }
                })
            })
        });
    </script>

    <!-- Detail TA -->
    <script>
        $(document).ready(function (){
            $('.viewTa').click(function (e) {
                e.preventDefault();

                var TaId = $(this).closest('tr').find('.TaId').text();
                var Dosen1Id = $(this).closest('tr').find('.Dosen1Id').text();
                var Dosen2Id = $(this).closest('tr').find('.Dosen2Id').text();

                $.ajax({
                    type: "POST",
                    url: "<?= base_url('tugasAkhir/DetailTA'); ?>",
                    data: {
                        'TaId': TaId,
                        'Dosen1Id' : Dosen1Id,
                        'Dosen2Id' : Dosen2Id,
                    },
                    success: function (response) {
                        response = JSON.parse(response);
                        console.log(response);

                        $("#viewTa [name=\"TaId\"]").val(response.Id);
                        $("#viewTa [name=\"semester\"]").val(response.Semester);
                        $("#viewTa [name=\"tahunAkademik\"]").val(response.TahunAkademik);
                        $("#viewTa [name=\"jumlahSKS\"]").val(response.JumlahSks);
                        $("#viewTa [name=\"IPK\"]").val(response.IPK);
                        $("#viewTa [name=\"bidangMinat\"]").val(response.Bidang);
                        $("#viewTa [name=\"Dosen1\"]").val(response.Dosen1Id);
                        $("#viewTa [name=\"Dosen1\"]").val(response.Dosen1Name);
                        $("#viewTa [name=\"Dosen2\"]").val(response.Dosen2Name);
                        $("#viewTa [name=\"judulIdn\"]").val(response.JudulIdn);
                        $("#viewTa [name=\"judulEng\"]").val(response.JudulEng);
                        $('#viewTa').modal('show');
                    }
                })
            })
        });
    </script>

    <!-- Download Formulir TA -->
    <script>
        function downloadFormulirTA() {
        var taId = document.getElementById("inputId").value;
        var url = '<?= base_url('TugasAkhir/DownloadTemplatePendaftaranTA/') ?>' + taId;
        
        fetch(url)
            .then(response => response.blob())
            .then(blob => {
            // Create a temporary link and trigger a download
            var a = document.createElement('a');
            a.href = window.URL.createObjectURL(blob);
            a.download = taId + '-FormulirTA.docx';
            document.body.appendChild(a);
            a.style.display = 'none';
            a.click();
            document.body.removeChild(a);
            })
            .catch(error => console.error('Error downloading file:', error));
        }
    </script>

    <!-- Validasi nilai Value SKS -->
    <script>
        function validateForm() {
            // Mendapatkan nilai dari input Jumlah SKS
            var inputJumlahSKS = document.getElementById("inputJumlahSKS").value;

            // Konversi nilai menjadi angka (integer)
            var jumlahSKSValue = parseInt(inputJumlahSKS);

            // Melakukan pemeriksaan
            if (isNaN(jumlahSKSValue) || jumlahSKSValue < 126) {
                alert("Jumlah SKS harus minimal 126");
                return false; // Menghentikan pengiriman formulir jika validasi gagal
            }

            // Lanjutkan dengan mengirim formulir jika valid
            return true;
        }
    </script>

     <!-- Berkas TA -->
     <script>
        $(document).ready(function (){
            $('.berkasTa').click(function (e) {
                e.preventDefault();

                var TaId = $(this).closest('tr').find('.TaId').text();

                $.ajax({
                    type: "POST",
                    url: "<?= base_url('tugasAkhir/DataBerkas'); ?>",
                    data: {
                        'TaId': TaId,
                    },
                    success: function (response) {
                        response = JSON.parse(response);
                        console.log(response);

                        /* Upload Formulir TA */
                        var responseUploadFormulir = response.UploadFormulir;
                        document.getElementById("UploadFormulir").textContent = responseUploadFormulir;
                        var responseFileFormulir = "<?=base_url('/assets/uploads/pendaftaran_ta/');?>" + response.FileFormulir;
                        var linkFormulirTA = document.getElementById("linkFormulirTA");
                        linkFormulirTA.href = responseFileFormulir;
                        var linkIconFormulir = document.getElementById("linkIconFormulirTA");
                        if (responseUploadFormulir !== null){
                            linkIconFormulir.style.display ="block";
                        }else{
                            linkIconFormulir.style.display ="none";
                        }

                        /* Upload KHS Reguler*/
                        var responseUploadKHSReguler = response.UploadKHSReguler;
                        document.getElementById("UploadKHSReguler").textContent = responseUploadKHSReguler;
                        var responseFileKHSReguler = "<?=base_url('/assets/uploads/pendaftaran_ta/');?>" + response.FileKHSReguler;
                        var linkKHSReguler = document.getElementById("linkKHSReguler");
                        linkKHSReguler.href = responseFileKHSReguler;
                        var linkIconKHSReguler = document.getElementById("linkIconKHSReguler");
                        if (responseUploadKHSReguler !== null){
                            linkIconKHSReguler.style.display ="block";
                        }else{
                            linkIconKHSReguler.style.display ="none";
                        }

                        /* Upload KHS Pendek */
                        var responseUploadKHSPendek = response.UploadKHSPendek;
                        document.getElementById("UploadKHSPendek").textContent = responseUploadKHSPendek;
                        var responseFileKHSPendek = "<?=base_url('/assets/uploads/pendaftaran_ta/');?>" + response.FileKHSPendek;
                        var linkKHSPendek = document.getElementById("linkKHSPendek");
                        linkKHSPendek.href = responseFileKHSPendek;
                        var linkIconKHSPendek = document.getElementById("linkIconKHSPendek");
                        if (responseUploadKHSPendek !== null){
                            linkIconKHSPendek.style.display ="block";
                        }else{
                            linkIconKHSPendek.style.display ="none";
                        }

                        /* Upload KRS */
                        var responseUploadKRS = response.UploadKRS;
                        document.getElementById("UploadKRS").textContent = responseUploadKRS;
                        var responseFileKRS = "<?=base_url('/assets/uploads/pendaftaran_ta/');?>" + response.FileKRS;
                        var linkKRS = document.getElementById("linkKRS");
                        linkKRS.href = responseFileKRS;
                        var linkIconKRS = document.getElementById("linkIconKRS");
                        if (responseUploadKRS !== null){
                            linkIconKRS.style.display ="block";
                        }else{
                            linkIconKRS.style.display ="none";
                        }

                        /* Upload Foto Formal */
                        var responseUploadFoto = response.UploadFoto;
                        document.getElementById("UploadFoto").textContent = responseUploadFoto;
                        var responseFileFoto = "<?=base_url('/assets/uploads/pendaftaran_ta/');?>" + response.FileFoto;
                        var linkFoto = document.getElementById("linkFoto");
                        linkFoto.href = responseFileFoto;
                        var linkIconFoto = document.getElementById("linkIconFoto");
                        if (responseUploadFoto !== null){
                            linkIconFoto.style.display ="block";
                        }else{
                            linkIconFoto.style.display ="none";
                        }

                        /* Upload Kartu Seminar */
                        var responseUploadKartuSeminar = response.UploadKartuSeminar;
                        document.getElementById("UploadKartuSeminar").textContent = responseUploadKartuSeminar;
                        var responseFileKartuSeminar = "<?=base_url('/assets/uploads/pendaftaran_ta/');?>" + response.FileKartuSeminar;
                        var linkKartuSeminar = document.getElementById("linkKartuSeminar");
                        linkKartuSeminar.href = responseFileKartuSeminar;
                        var linkIconKartuSeminar = document.getElementById("linkIconKartuSeminar");
                        if (responseUploadKartuSeminar !== null){
                            linkIconKartuSeminar.style.display ="block";
                        }else{
                            linkIconKartuSeminar.style.display ="none";
                        }

                        /* Upload FIDK */
                        var responseUploadFIDK = response.UploadFIDK;
                        document.getElementById("UploadFIDK").textContent = responseUploadFIDK;
                        var responseFileFIDK = "<?=base_url('/assets/uploads/pendaftaran_ta/');?>" + response.FileFIDK;
                        var linkFIDK = document.getElementById("linkFIDK");
                        linkFIDK.href = responseFileFIDK;
                        var linkIconFIDK = document.getElementById("linkIconFIDK");
                        if (responseUploadFIDK !== null){
                            linkIconFIDK.style.display ="block";
                        }else{
                            linkIconFIDK.style.display ="none";
                        }

                        /* Upload Transkrip */
                        var responseUploadTranskrip = response.UploadTranskrip;
                        document.getElementById("UploadTranskrip").textContent = responseUploadTranskrip;
                        var responseFileTranskrip = "<?=base_url('/assets/uploads/pendaftaran_ta/');?>" + response.FileTranskrip;
                        var linkTranskrip = document.getElementById("linkTranskrip");
                        linkTranskrip.href = responseFileTranskrip;
                        var linkIconTranskrip = document.getElementById("linkIconTranskrip");
                        if (responseUploadTranskrip !== null){
                            linkIconTranskrip.style.display ="block";
                        }else{
                            linkIconTranskrip.style.display ="none";
                        }
                        
                        $("#berkasTa [name=\"TaId\"]").val(response.TaId);
                        $("#berkasTa [name=\"validasiFormulir\"]").val(response.ValidFormulir);
                        $("#berkasTa [name=\"validasiKHSReguler\"]").val(response.ValidKHSReguler);
                        $("#berkasTa [name=\"validasiKHSPendek\"]").val(response.ValidKHSPendek);
                        $("#berkasTa [name=\"validasiKRS\"]").val(response.ValidKRS);
                        $("#berkasTa [name=\"validasiFoto\"]").val(response.ValidFoto);
                        $("#berkasTa [name=\"validasiKartuSeminar\"]").val(response.ValidKartuSeminar);
                        $("#berkasTa [name=\"validasiFIDK\"]").val(response.ValidFIDK);
                        $("#berkasTa [name=\"validasiTranskrip\"]").val(response.ValidTranskrip);
                        $('#berkasTa').modal('show');
                    }
                })
            })
        });
    </script>

    <!-- Upload Formulir TA -->
    <script>
        const inputFormulir = document.getElementById('input-formulirTA');
        if (inputFormulir) {
            inputFormulir.addEventListener('change', function () {
                document.getElementById('submit-formulirTA').style.display = 'block';
            });
        }
    </script>

    <!-- Upload KHS Reguler -->
    <script>
        const inputKHSReguler = document.getElementById('input-KHSReguler');
        
        if (inputKHSReguler){
            inputKHSReguler.addEventListener('change', function () {
                document.getElementById('submit-KHSReguler').style.display = 'block';
            });
        }
    </script>

    <!-- Upload KHS Pendek -->
    <script>
        const inputKHSPendek = document.getElementById('input-KHSPendek');
        
        if (inputKHSPendek){
            inputKHSPendek.addEventListener('change', function () {
                document.getElementById('submit-KHSPendek').style.display = 'block';
            });
        }
    </script>

    <!-- Upload KRS -->
    <script>
        const inputKRS = document.getElementById('input-KRS');
        
        if(inputKRS){
            inputKRS.addEventListener('change', function () {
                document.getElementById('submit-KRS').style.display = 'block';
            });
        }
    </script>

    <!-- Upload Foto -->
    <script>
        const inputFoto = document.getElementById('input-Foto');
        
        if (inputFoto){
            inputFoto.addEventListener('change', function () {
                document.getElementById('submit-Foto').style.display = 'block';
            });
        }
    </script>

    <!-- Upload Kartu Seminar -->
    <script>
        const inputKartuSeminar = document.getElementById('input-KartuSeminar');
        
        if (inputKartuSeminar){
            inputKartuSeminar.addEventListener('change', function () {
                document.getElementById('submit-KartuSeminar').style.display = 'block';
            });
        }
    </script>

    <!-- Upload FIDK -->
    <script>
        const inputFIDK = document.getElementById('input-FIDK');
        
        if (inputFIDK){
            inputFIDK.addEventListener('change', function () {
                document.getElementById('submit-FIDK').style.display = 'block';
            });
        }
    </script>

    <!-- Upload Transkrip -->
    <script>
        const inputTranskrip = document.getElementById('input-Transkrip');
        
        if (inputTranskrip){
            inputTranskrip.addEventListener('change', function () {
                document.getElementById('submit-Transkrip').style.display = 'block';
            });
        }
    </script>

    <!-- Update Valid Formulir -->
    <script>
        $(document).ready(function() {
            // Listen for the 'change' event on the select element
            $('#validFormulir').change(function() {
                var ValidFormulir = $(this).val();
                var TaId = document.getElementById('TaId').value;

                console.log(ValidFormulir);
                console.log(TaId);
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('TugasAkhir/ValidFormulir'); ?>",
                    data: {
                        'TaId': TaId,
                        'ValidFormulir': ValidFormulir 
                    },
                    success: function (response) {}
                });
            });
        });
    </script>

    <!-- Update Valid KHS Reguler -->
    <script>
        $(document).ready(function() {
            $('#validKHSReguler').change(function() {
                var ValidKHSReguler = $(this).val();
                var TaId = document.getElementById('TaId').value;

                $.ajax({
                    type: 'POST',
                    url: '<?= base_url('TugasAkhir/ValidKHSReguler') ?>',
                    data: { 
                        'TaId': TaId,
                        'ValidKHSReguler': ValidKHSReguler 
                    },
                    dataType: 'json',
                    success: function(response) {
                    }
                });
            });
        });
    </script>

    <!-- Update Valid KHS Pendek -->
    <script>
        $(document).ready(function() {
            $('#validKHSPendek').change(function() {
                var ValidKHSPendek = $(this).val();
                var TaId = document.getElementById('TaId').value;

                $.ajax({
                    type: 'POST',
                    url: '<?= base_url('TugasAkhir/ValidKHSPendek') ?>',
                    data: { 
                        'TaId': TaId,
                        'ValidKHSPendek': ValidKHSPendek 
                    },
                    dataType: 'json',
                    success: function(response) {
                    }
                });
            });
        });
    </script>

    <!-- Update Valid KRS -->
    <script>
        $(document).ready(function() {
            $('#validKRS').change(function() {
                var ValidKRS = $(this).val();
                var TaId = document.getElementById('TaId').value;

                $.ajax({
                    type: 'POST',
                    url: '<?= base_url('TugasAkhir/ValidKRS') ?>',
                    data: { 
                        'TaId': TaId,
                        'ValidKRS': ValidKRS 
                    },
                    dataType: 'json',
                    success: function(response) {
                    }
                });
            });
        });
    </script>

    <!-- Update Valid Foto -->
    <script>
        $(document).ready(function() {
            $('#validFoto').change(function() {
                var ValidFoto = $(this).val();
                var TaId = document.getElementById('TaId').value;

                $.ajax({
                    type: 'POST',
                    url: '<?= base_url('TugasAkhir/ValidFoto') ?>',
                    data: { 
                        'TaId': TaId,
                        'ValidFoto': ValidFoto 
                    },
                    dataType: 'json',
                    success: function(response) {
                    }
                });
            });
        });
    </script>

    <!-- Update Valid Kartu Seminar -->
    <script>
        $(document).ready(function() {
            $('#validKartuSeminar').change(function() {
                var ValidKartuSeminar = $(this).val();
                var TaId = document.getElementById('TaId').value;

                $.ajax({
                    type: 'POST',
                    url: '<?= base_url('TugasAkhir/ValidKartuSeminar') ?>',
                    data: { 
                        'TaId': TaId,
                        'ValidKartuSeminar': ValidKartuSeminar 
                    },
                    dataType: 'json',
                    success: function(response) {
                    }
                });
            });
        });
    </script>

    <!-- Update Valid FIDK -->
    <script>
        $(document).ready(function() {
            $('#validFIDK').change(function() {
                var ValidFIDK = $(this).val();
                var TaId = document.getElementById('TaId').value;

                $.ajax({
                    type: 'POST',
                    url: '<?= base_url('TugasAkhir/ValidFIDK') ?>',
                    data: { 
                        'TaId': TaId,
                        'ValidFIDK': ValidFIDK 
                    },
                    dataType: 'json',
                    success: function(response) {
                    }
                });
            });
        });
    </script>

    <!-- Update Valid Transkrip -->
    <script>
        $(document).ready(function() {
            $('#validTranskrip').change(function() {
                var ValidTranskrip = $(this).val();
                var TaId = document.getElementById('TaId').value;

                $.ajax({
                    type: 'POST',
                    url: '<?= base_url('TugasAkhir/ValidTranskrip') ?>',
                    data: { 
                        'TaId': TaId,
                        'ValidTranskrip': ValidTranskrip 
                    },
                    dataType: 'json',
                    success: function(response) {
                    }
                });
            });
        });
    </script>

    <!-- Detail Staff Tendik -->
    <script>
        $(document).ready(function (){
            $('.viewStaff').click(function (e) {
                e.preventDefault();

                var UserId = $(this).closest('tr').find('.UserId').text();

                $.ajax({
                    type: "POST",
                    url: "<?= base_url('sdm/DetailStaff'); ?>",
                    data: {
                        'UserId': UserId,
                    },
                    success: function (response) {
                        response = JSON.parse(response);
                        console.log(response);

                        $("#viewStaff [name=\"UserId\"]").val(response.Id);
                        $("#viewStaff [name=\"name\"]").val(response.Name);
                        $("#viewStaff [name=\"email\"]").val(response.Email);
                        $("#viewStaff [name=\"golongan\"]").val(response.Golongan);
                        $("#viewStaff [name=\"pangkat\"]").val(response.Pangkat);
                        $("#viewStaff [name=\"jabatan\"]").val(response.Jabatan);
                        $("#viewStaff [name=\"bagian\"]").val(response.Bagian);
                        $("#viewStaff [name=\"pendidikan\"]").val(response.PendidikanTerakhir);
                        $("#viewStaff [name=\"departemen\"]").val(response.Departemen);
                        $("#viewStaff [name=\"laboratorium\"]").val(response.NamaLab);

                        $('#viewStaff').modal('show');
                    }
                })
            })
        });
    </script>

    <!-- Edit Staff Tendik -->
    <script>
        $(document).ready(function (){
            $('.editStaff').click(function (e) {
                e.preventDefault();

                var UserId = $(this).closest('tr').find('.UserId').text();

                $.ajax({
                    type: "POST",
                    url: "<?= base_url('sdm/DetailStaff'); ?>",
                    data: {
                        'UserId': UserId,
                    },
                    success: function (response) {
                        response = JSON.parse(response);
                        console.log(response);

                        $("#editStaff [name=\"UserId\"]").val(response.Id);
                        $("#editStaff [name=\"name\"]").val(response.Name);
                        $("#editStaff [name=\"email\"]").val(response.Email);
                        $("#editStaff [name=\"golongan\"]").val(response.Golongan);
                        $("#editStaff [name=\"pangkat\"]").val(response.Pangkat);
                        $("#editStaff [name=\"jabatan\"]").val(response.Jabatan);
                        $("#editStaff [name=\"bagian\"]").val(response.Bagian);
                        $("#editStaff [name=\"pendidikan\"]").val(response.PendidikanTerakhir);
                        $("#editStaff [name=\"departemen\"]").val(response.Departemen);
                        $("#editStaff [name=\"laboratorium\"]").val(response.Laboratorium);

                        $('#editStaff').modal('show');
                    }
                })
            })
        });
    </script>

    <!-- Edit Mahasiswa -->
    <script>
        $(document).ready(function (){
            $('.editMahasiswa').click(function (e) {
                e.preventDefault();

                var UserId = $(this).closest('tr').find('.UserId').text();
                
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('sdm/DetailMahasiswa'); ?>",
                    data: {
                        'UserId': UserId,
                        'view': false,
                    },
                    success: function (response) {
                        $('.edit_mahasiswa').html(response);
                        $('#editMahasiswa').modal('show');
                    }
                })
            })
        });
    </script>

    <!-- Detail Mahasiswa -->
    <script>
        $(document).ready(function (){
            $('.viewMahasiswa').click(function (e) {
                e.preventDefault();

                var UserId = $(this).closest('tr').find('.UserId').text();

                $.ajax({
                    type: "POST",
                    url: "<?= base_url('sdm/DetailMahasiswa'); ?>",
                    data: {
                        'UserId': UserId,
                        'view': true,
                    },
                    success: function (response) {
                        $('.view_mahasiswa').html(response);
                        $('#viewMahasiswa').modal('show');
                    }
                })
            })
        });
    </script>

    <!-- Upload Profile Photo Dosen -->
    <script>
        $('.custom-file-input').on('change', function(){
            let fileName = $(this).val().split('\\').pop();
            $(this).next('.custom-file-label').addClass("selected").html(fileName);
        })
    </script>

    <!-- Dropdown Search -->
    <script>
    $(document).ready(function() {
        $("#Dosen1").select2(); 

        $("#Dosen1").on("change", function() {
            var selectedValue = $(this).val();

            $.ajax({
                url: "<?= base_url('sdm/DataDosenByName') ?>", 
                method: "GET",
                data: { Name: selectedValue },
                success: function(response) {
                },
                error: function(xhr, status, error) {
                    console.error(error);
                }
            });
        });
    });
    </script>

    <script>
    $(document).ready(function() {
        $("#Dosen1Daftar").select2(); 

        $("#Dosen1Daftar").on("change", function() {
            var selectedValue = $(this).val();

            $.ajax({
                url: "<?= base_url('sdm/DataDosenByName') ?>", 
                method: "GET",
                data: { Name: selectedValue },
                success: function(response) {
                },
                error: function(xhr, status, error) {
                    console.error(error);
                }
            });
        });
    });
    </script>

    <script>
    $(document).ready(function() {
        $("#Dosen2").select2(); 

        $("#Dosen2").on("change", function() {
            var selectedValue = $(this).val();

            console.log(selectedValue);
            if (selectedValue !== null && selectedValue !== "") {
                $.ajax({
                    url: "<?= base_url('sdm/DataDosenByName') ?>", 
                    method: "GET",
                    data: { Name: selectedValue },
                    success: function(response) {
                    },
                    error: function(xhr, status, error) {
                        console.error(error);
                    }
                });
            }

        });
    });
    </script>

    <script>
    $(document).ready(function() {
        $("#Dosen2Daftar").select2(); 

        $("#Dosen2Daftar").on("change", function() {
            var selectedValue = $(this).val();

            $.ajax({
                url: "<?= base_url('sdm/DataDosenByName') ?>", 
                method: "GET",
                data: { Name: selectedValue },
                success: function(response) {
                },
                error: function(xhr, status, error) {
                    console.error(error);
                }
            });
        });
    });
    </script>

    <script>
    $(document).ready(function() {
        $("#DosenPkl").select2(); 

        $("#DosenPkl").on("change", function() {
            var selectedValue = $(this).val();

            $.ajax({
                url: "<?= base_url('sdm/DataDosenByName') ?>", 
                method: "GET",
                data: { Name: selectedValue },
                success: function(response) {
                },
                error: function(xhr, status, error) {
                    console.error(error);
                }
            });
        });
    });
    </script>

    <script>
    $(document).ready(function() {
        $("#DosenPembimbing").select2(); 

        $("#DosenPembimbing").on("change", function() {
            var selectedValue = $(this).val();

            $.ajax({
                url: "<?= base_url('sdm/DataDosenByName') ?>", 
                method: "GET",
                data: { Name: selectedValue },
                success: function(response) {
                },
                error: function(xhr, status, error) {
                    console.error(error);
                }
            });
        });
    });
    </script>

    <!-- Dropdown Time -->
    <script>
    $(document).ready(function() {
        var select = $('#JamMulai');
        for (var hour = 6; hour < 21; hour++) {
            for (var minute = 0; minute < 60; minute += 05) {
                var time = (hour < 10 ? '0' + hour : hour) + ':' + (minute === 0 ? '00' : minute);
                select.append('<option value="' + time + '">' + time + '</option>');
            }
        }

        select.select2();
    });

    $(document).ready(function() {
        var select = $('#JamMulai2');
        for (var hour = 6; hour < 21; hour++) {
            for (var minute = 0; minute < 60; minute += 05) {
                var time = (hour < 10 ? '0' + hour : hour) + ':' + (minute === 0 ? '00' : minute);
                select.append('<option value="' + time + '">' + time + '</option>');
            }
        }

        select.select2();
    });

    $(document).ready(function() {
        var select = $('#JamMulai3');
        for (var hour = 6; hour < 21; hour++) {
            for (var minute = 0; minute < 60; minute += 05) {
                var time = (hour < 10 ? '0' + hour : hour) + ':' + (minute === 0 ? '00' : minute);
                select.append('<option value="' + time + '">' + time + '</option>');
            }
        }

        select.select2();
    });

    $(document).ready(function() {
        var select = $('#JamMulaiLembur');
        for (var hour = 6; hour < 21; hour++) {
            for (var minute = 0; minute < 60; minute += 05) {
                var time = (hour < 10 ? '0' + hour : hour) + ':' + (minute === 0 ? '00' : minute);
                select.append('<option value="' + time + '">' + time + '</option>');
            }
        }

        select.select2();
    });
    </script>

    <script>
    $(document).ready(function() {
        var select = $('#JamSelesai');
        for (var hour = 7; hour < 22; hour++) {
            for (var minute = 0; minute < 60; minute += 05) {
                var time = (hour < 10 ? '0' + hour : hour) + ':' + (minute === 0 ? '00' : minute);
                select.append('<option value="' + time + '">' + time + '</option>');
            }
        }

        select.select2();
    });

    $(document).ready(function() {
        var select = $('#JamSelesai2');
        for (var hour = 7; hour < 22; hour++) {
            for (var minute = 0; minute < 60; minute += 05) {
                var time = (hour < 10 ? '0' + hour : hour) + ':' + (minute === 0 ? '00' : minute);
                select.append('<option value="' + time + '">' + time + '</option>');
            }
        }

        select.select2();
    });

    $(document).ready(function() {
        var select = $('#JamSelesai3');
        for (var hour = 7; hour < 22; hour++) {
            for (var minute = 0; minute < 60; minute += 05) {
                var time = (hour < 10 ? '0' + hour : hour) + ':' + (minute === 0 ? '00' : minute);
                select.append('<option value="' + time + '">' + time + '</option>');
            }
        }

        select.select2();
    });

    $(document).ready(function() {
        var select = $('#JamSelesaiLembur');
        for (var hour = 7; hour < 21; hour++) {
            for (var minute = 0; minute < 60; minute += 05) {
                var time = (hour < 10 ? '0' + hour : hour) + ':' + (minute === 0 ? '00' : minute);
                select.append('<option value="' + time + '">' + time + '</option>');
            }
        }

        select.select2();
    });
    </script>

    <!-- Dropdown Ruangan -->
    <script>
    $(document).ready(function() {
        $("#Ruangan").select2(); 

        $("#Ruangan").on("change", function() {
            var selectedValue = $(this).val();

            $.ajax({
                url: "<?= base_url('akademik/Ruangan') ?>", 
                method: "GET",
                data: { Name: selectedValue },
                success: function(response) {
                },
                error: function(xhr, status, error) {
                    console.error(error);
                }
            });
        });
    });
    </script>

    <script>
    $(document).ready(function() {
        $("#RuanganDaftarUjianPKL").select2(); 

        $("#RuanganDaftarUjianPKL").on("change", function() {
            var selectedValue = $(this).val();

            $.ajax({
                url: "<?= base_url('akademik/Ruangan') ?>", 
                method: "GET",
                data: { Name: selectedValue },
                success: function(response) {
                },
                error: function(xhr, status, error) {
                    console.error(error);
                }
            });
        });
    });

    $(document).ready(function() {
        $("#RuanganEditUjianPKL").select2(); 

        $("#RuanganEditUjianPKL").on("change", function() {
            var selectedValue = $(this).val();

            $.ajax({
                url: "<?= base_url('akademik/Ruangan') ?>", 
                method: "GET",
                data: { Name: selectedValue },
                success: function(response) {
                },
                error: function(xhr, status, error) {
                    console.error(error);
                }
            });
        });
    });
    </script>

    <!-- Dropdown Instansi -->
    <script>
        $(document).ready(function() {
            $('#InstansiDaftarPKL').select2();
        });
    </script>

    <!-- Dropdown Lab -->
    <script>
        $(document).ready(function() {
            $('#Lab').select2();
        });
    </script>

    <!-- Dropdown MasterAlat -->
    <script>
        $(document).ready(function() {
            $('#MasterAlat').select2();
        });
    </script>

    <!-- Dropdown MasterBahan -->
    <script>
        $(document).ready(function() {
            $('#MasterBahan').select2();
        });
    </script>

    <!-- Dropdown PLP -->
    <script>
        $(document).ready(function() {
            $('#PLP').select2();
        });
    </script>

    <!-- Dropdown Dosen -->
    <script>
        $(document).ready(function() {
            $('#Kadep').select2();
        });

        $(document).ready(function() {
            $('#Sekdep').select2();
        });

        $(document).ready(function() {
            $('#KaprodiS1').select2();
        });

        $(document).ready(function() {
            $('#KaprodiS2').select2();
        });

        $(document).ready(function() {
            $('#KaprodiS3').select2();
        });

        $(document).ready(function() {
            $('#KadepEdit').select2();
        });

        $(document).ready(function() {
            $('#SekdepEdit').select2();
        });

        $(document).ready(function() {
            $('#KaprodiS1Edit').select2();
        });

        $(document).ready(function() {
            $('#KaprodiS2Edit').select2();
        });

        $(document).ready(function() {
            $('#KaprodiS3Edit').select2();
        });
    </script>

    <!-- Dropdown keterangan -->
    <script>
    $(document).ready(function() {
        $("#tataBahasa_9C").change(function() {
        var tataBahasa_9COption = $("#tataBahasa_9C option:selected");
        var info = tataBahasa_9COption.data("info");

        if (info) {
            $("#tataBahasa_9CInfo").text(info);
            $("#tataBahasa_9CInfo").show();
        } else {
            $("#tataBahasa_9CInfo").hide();
        }
        });
    });

    $(document).ready(function() {
        $("#kosaKata_9C").change(function() {
        var kosaKata_9COption = $("#kosaKata_9C option:selected");
        var info = kosaKata_9COption.data("info");

        if (info) {
            $("#kosaKata_9CInfo").text(info);
            $("#kosaKata_9CInfo").show();
        } else {
            $("#kosaKata_9CInfo").hide();
        }
        });
    });

    $(document).ready(function() {
        $("#pengucapan_9C").change(function() {
        var pengucapan_9COption = $("#pengucapan_9C option:selected");
        var info = pengucapan_9COption.data("info");

        if (info) {
            $("#pengucapan_9CInfo").text(info);
            $("#pengucapan_9CInfo").show();
        } else {
            $("#pengucapan_9CInfo").hide();
        }
        });
    });

    $(document).ready(function() {
        $("#penyampaian_6").change(function() {
        var penyampaian_6Option = $("#penyampaian_6 option:selected");
        var info = penyampaian_6Option.data("info");

        if (info) {
            $("#penyampaian_6Info").text(info);
            $("#penyampaian_6Info").show();
        } else {
            $("#penyampaian_6Info").hide();
        }
        });
    });

    $(document).ready(function() {
        $("#ideHasilPekerjaan_9C").change(function() {
        var ideHasilPekerjaan_9COption = $("#ideHasilPekerjaan_9C option:selected");
        var info = ideHasilPekerjaan_9COption.data("info");

        if (info) {
            $("#ideHasilPekerjaan_9CInfo").text(info);
            $("#ideHasilPekerjaan_9CInfo").show();
        } else {
            $("#ideHasilPekerjaan_9CInfo").hide();
        }
        });
    });

    $(document).ready(function() {
        $("#masalahTujuan_9C").change(function() {
        var masalahTujuan_9COption = $("#masalahTujuan_9C option:selected");
        var info = masalahTujuan_9COption.data("info");

        if (info) {
            $("#masalahTujuan_9CInfo").text(info);
            $("#masalahTujuan_9CInfo").show();
        } else {
            $("#masalahTujuan_9CInfo").hide();
        }
        });
    });

    $(document).ready(function() {
        $("#langkahLangkah_6").change(function() {
        var langkahLangkah_6Option = $("#langkahLangkah_6 option:selected");
        var info = langkahLangkah_6Option.data("info");

        if (info) {
            $("#langkahLangkah_6Info").text(info);
            $("#langkahLangkah_6Info").show();
        } else {
            $("#langkahLangkah_6Info").hide();
        }
        });
    });

    $(document).ready(function() {
        $("#cakupan_6").change(function() {
        var cakupan_6Option = $("#cakupan_6 option:selected");
        var info = cakupan_6Option.data("info");

        if (info) {
            $("#cakupan_6Info").text(info);
            $("#cakupan_6Info").show();
        } else {
            $("#cakupan_6Info").hide();
        }
        });
    });

    $(document).ready(function() {
        $("#penyampaian_9B").change(function() {
        var penyampaian_9BOption = $("#penyampaian_9B option:selected");
        var info = penyampaian_9BOption.data("info");

        if (info) {
            $("#penyampaian_9BInfo").text(info);
            $("#penyampaian_9BInfo").show();
        } else {
            $("#penyampaian_9BInfo").hide();
        }
        });
    });

    $(document).ready(function() {
        $("#langkahLangkah_8A").change(function() {
        var langkahLangkah_8AOption = $("#langkahLangkah_8A option:selected");
        var info = langkahLangkah_8AOption.data("info");

        if (info) {
            $("#langkahLangkah_8AInfo").text(info);
            $("#langkahLangkah_8AInfo").show();
        } else {
            $("#langkahLangkah_8AInfo").hide();
        }
        });
    });

    $(document).ready(function() {
        $("#data_8A").change(function() {
        var data_8AOption = $("#data_8A option:selected");
        var info = data_8AOption.data("info");

        if (info) {
            $("#data_8AInfo").text(info);
            $("#data_8AInfo").show();
        } else {
            $("#data_8AInfo").hide();
        }
        });
    });

    $(document).ready(function() {
        $("#mengolahData_8B").change(function() {
        var mengolahData_8BOption = $("#mengolahData_8B option:selected");
        var info = mengolahData_8BOption.data("info");

        if (info) {
            $("#mengolahData_8BInfo").text(info);
            $("#mengolahData_8BInfo").show();
        } else {
            $("#mengolahData_8BInfo").hide();
        }
        });
    });

    $(document).ready(function() {
        $("#kesimpulan_8C").change(function() {
        var kesimpulan_8COption = $("#kesimpulan_8C option:selected");
        var info = kesimpulan_8COption.data("info");

        if (info) {
            $("#kesimpulan_8CInfo").text(info);
            $("#kesimpulan_8CInfo").show();
        } else {
            $("#kesimpulan_8CInfo").hide();
        }
        });
    });

    $(document).ready(function() {
        $("#kesimpulanSkripsi_8C").change(function() {
        var kesimpulanSkripsi_8COption = $("#kesimpulanSkripsi_8C option:selected");
        var info = kesimpulanSkripsi_8COption.data("info");

        if (info) {
            $("#kesimpulanSkripsi_8CInfo").text(info);
            $("#kesimpulanSkripsi_8CInfo").show();
        } else {
            $("#kesimpulanSkripsi_8CInfo").hide();
        }
        });
    });
    
    $(document).ready(function() {
        $("#pengukuran_9C").change(function() {
        var pengukuran_9COption = $("#pengukuran_9C option:selected");
        var info = pengukuran_9COption.data("info");

        if (info) {
            $("#pengukuran_9CInfo").text(info);
            $("#pengukuran_9CInfo").show();
        } else {
            $("#pengukuran_9CInfo").hide();
        }
        });
    });
    
    $(document).ready(function() {
        $("#pengukuran_8B").change(function() {
        var pengukuran_8BOption = $("#pengukuran_8B option:selected");
        var info = pengukuran_8BOption.data("info");

        if (info) {
            $("#pengukuran_8BInfo").text(info);
            $("#pengukuran_8BInfo").show();
        } else {
            $("#pengukuran_8BInfo").hide();
        }
        });
    });

    $(document).ready(function() {
        $("#aturanPenulisan_9A").change(function() {
        var aturanPenulisan_9AOption = $("#aturanPenulisan_9A option:selected");
        var info = aturanPenulisan_9AOption.data("info");

        if (info) {
            $("#aturanPenulisan_9AInfo").text(info);
            $("#aturanPenulisan_9AInfo").show();
        } else {
            $("#aturanPenulisan_9AInfo").hide();
        }
        });
    });

    $(document).ready(function() {
        $("#tampilan_9A").change(function() {
        var tampilan_9AOption = $("#tampilan_9A option:selected");
        var info = tampilan_9AOption.data("info");

        if (info) {
            $("#tampilan_9AInfo").text(info);
            $("#tampilan_9AInfo").show();
        } else {
            $("#tampilan_9AInfo").hide();
        }
        });
    });

    $(document).ready(function() {
        $("#tampilan_9B").change(function() {
        var tampilan_9BOption = $("#tampilan_9B option:selected");
        var info = tampilan_9BOption.data("info");

        if (info) {
            $("#tampilan_9BInfo").text(info);
            $("#tampilan_9BInfo").show();
        } else {
            $("#tampilan_9BInfo").hide();
        }
        });
    });

    $(document).ready(function() {
        $("#organisasi_9B").change(function() {
        var organisasi_9BOption = $("#organisasi_9B option:selected");
        var info = organisasi_9BOption.data("info");

        if (info) {
            $("#organisasi_9BInfo").text(info);
            $("#organisasi_9BInfo").show();
        } else {
            $("#organisasi_9BInfo").hide();
        }
        });
    });

    $(document).ready(function() {
        $("#mengolahData_8B").change(function() {
        var mengolahData_8BOption = $("#mengolahData_8B option:selected");
        var info = mengolahData_8BOption.data("info");

        if (info) {
            $("#mengolahData_8BInfo").text(info);
            $("#mengolahData_8BInfo").show();
        } else {
            $("#mengolahData_8BInfo").hide();
        }
        });
    });

    $(document).ready(function() {
        $("#tataLetak_9A").change(function() {
        var tataLetak_9AOption = $("#tataLetak_9A option:selected");
        var info = tataLetak_9AOption.data("info");

        if (info) {
            $("#tataLetak_9AInfo").text(info);
            $("#tataLetak_9AInfo").show();
        } else {
            $("#tataLetak_9AInfo").hide();
        }
        });
    });

    $(document).ready(function() {
        $("#konsep_9A").change(function() {
        var konsep_9AOption = $("#konsep_9A option:selected");
        var info = konsep_9AOption.data("info");

        if (info) {
            $("#konsep_9AInfo").text(info);
            $("#konsep_9AInfo").show();
        } else {
            $("#konsep_9AInfo").hide();
        }
        });
    });

    $(document).ready(function() {
        $("#pembahasan_9B").change(function() {
        var pembahasan_9BOption = $("#pembahasan_9B option:selected");
        var info = pembahasan_9BOption.data("info");

        if (info) {
            $("#pembahasan_9BInfo").text(info);
            $("#pembahasan_9BInfo").show();
        } else {
            $("#pembahasan_9BInfo").hide();
        }
        });
    });
    </script>

    <!-- QuilJs -->
    <script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>
    <script>
        var quill = new Quill('#quillEditor', {
            theme: 'snow',
            modules: {
                toolbar: [
                    [{ header: [1, 2, 3, 4, 5, 6, false] }],
                    [{ font: [] }],
                    ["bold", "italic", "underline"],
                    ["link", "blockquote", "code-block"],
                    [{ list: "ordered" }, { list: "bullet" }],
                    [{ script: "sub" }, { script: "super" }],
                    [{ color: [] }, { background: [] }],
                ]
            },
        });
        quill.on('text-change', function(delta, oldDelta, source) {
            document.querySelector("input[name='saran']").value = quill.root.innerHTML;
        });
    </script>

    <!-- Kalender -->
    <script>
       let currentYear, currentMonth;

        document.addEventListener('DOMContentLoaded', function () {
            const today = new Date();
            currentYear = today.getFullYear();
            currentMonth = today.getMonth() + 1;

            updateMonthYear();
            generateCalendar(currentYear, currentMonth);
        });

        function generateCalendar(year, month) {
            const firstDayOfMonth = new Date(year, month - 1, 1);
            const lastDayOfMonth = new Date(year, month, 0);

            const daysInMonth = lastDayOfMonth.getDate();

            const calendarContainer = document.getElementById('calendar');
            const currentMonthYearElement = document.getElementById('currentMonthYear');

            let calendarHTML = `<table class="table table-bordered text-center">
                                    <thead>
                                        <tr>
                                            <th>Minggu</th>
                                            <th>Senin</th>
                                            <th>Selasa</th>
                                            <th>Rabu</th>
                                            <th>Kamis</th>
                                            <th>Jum'at</th>
                                            <th>Sabtu</th>
                                        </tr>
                                    </thead>
                                    <tbody>`;

            let dayCount = 1;

            for (let i = 0; i < 6; i++) {
                calendarHTML += '<tr>';

                for (let j = 0; j < 7; j++) {
                    if (i === 0 && j < firstDayOfMonth.getDay()) {
                        calendarHTML += '<td></td>';
                    } else if (dayCount > daysInMonth) {
                        break;
                    } else {
                        const fullDate = `${year}-${month.toString().padStart(2, '0')}-${dayCount.toString().padStart(2, '0')}`;
                        calendarHTML += `<td><a href="#" onclick="selectDate('${fullDate}')">${dayCount}</a></td>`;
                        dayCount++;
                    }
                }

                calendarHTML += '</tr>';
            }

            calendarHTML += `</tbody>
                            </table>`;

            calendarContainer.innerHTML = calendarHTML;
            updateMonthYear();
        }

        function selectDate(fullDate) {
            // alert(`You selected ${fullDate}. Add your navigation logic here.`);
            // Add your logic to navigate to a new page or perform an action with the selected date.
            window.location.href = `<?= base_url('agenda/AgendaRuangan/') ?>${fullDate}`;
        }

        function previousMonth() {
            currentMonth--;
            if (currentMonth < 1) {
                currentMonth = 12;
                currentYear--;
            }
            generateCalendar(currentYear, currentMonth);
        }

        function nextMonth() {
            currentMonth++;
            if (currentMonth > 12) {
                currentMonth = 1;
                currentYear++;
            }
            generateCalendar(currentYear, currentMonth);
        }

        function updateMonthYear() {
            const monthNames = [
                'January', 'February', 'March', 'April', 'May', 'June',
                'July', 'August', 'September', 'October', 'November', 'December'
            ];
            document.getElementById('currentMonthYear').textContent = `${monthNames[currentMonth - 1]} ${currentYear}`;
        }
    </script>

    <!-- Donut Chart -->
    <script>
        $(document).ready(function () {
            var donutChart;  // Declare a variable to hold the Chart instance

            $('.chartAlatLab').click(function (e) {
                e.preventDefault();

                var AlatLabId = $(this).closest('tr').find('.AlatLabId').text();

                console.log(AlatLabId);
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('laboratorium/DetailAlatLab'); ?>",
                    data: {
                        'AlatLabId': AlatLabId,
                    },
                    success: function (response) {
                        response = JSON.parse(response);
                        console.log(response);

                        if (response != null) {
                            // Destroy existing Chart if it exists
                            if (donutChart) {
                                donutChart.destroy();
                            }

                            // Create a new donut chart
                            createDonutChart(response);
                        }

                        $('#chartAlatLab').modal('show');
                    }
                });
            });

            function createDonutChart(response) {
                var ctx = document.getElementById('donutChart').getContext('2d');
                donutChart = new Chart(ctx, {
                    type: 'doughnut',
                    data: {
                        labels: ['Jumlah Baik', 'Jumlah Pinjam', 'Jumlah Rusak'],
                        datasets: [{
                            data: [response.JumlahBaik, response.JumlahPinjam, response.JumlahRusak],
                            backgroundColor: ['#80bfff', '#ffe680', '#ff9999'],
                        }],
                    },
                    options: {
                        title: {
                            display: true,
                            text: 'Alat Lab Status',
                        },
                    },
                });
            }
        });
    </script>


</body>

</html>