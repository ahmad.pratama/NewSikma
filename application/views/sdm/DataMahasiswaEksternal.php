<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Filter -->
    <div class="card">
        <div class="row mt-3 ml-2 mb-0">
            <div class="col">
            <form method="post" action="<?= base_url('sdm/DataMahasiswaEksternal'); ?>" enctype="multipart/form-data">
                <div class="form-row">
                    <div class="form-group col-md-2">
                        <div class="form-row">
                            <div class="form-group col">
                            <select id="limit" name="limit" class="form-control">
                                <option value="">Tampil Data</option>
                                <option value="10">10</option>
                                <option value="25">25</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-2">
                        <div class="form-row">
                            <div class="form-group col">
                                <input type="text" class="form-control" id="search" name="search" placeholder="Nama / NIM">
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-2">
                        <button type="submit" class="btn btn-secondary">Cari</button>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>

    <!-- Table Mahasiswa -->
    <div class="card">
        <div class="row mt-3 ml-2 mr-2">
            <div class="col-4">
                <h4 class="text-white bg-dark">Data Mahasiswa Eksternal</h4>
            </div>
            <div class="col-6">
            </div>
            <div class="col-2">
                <div class="btn-group">
                    <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa-solid fa-user-plus"></i>
                        Tambah
                    </button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" data-toggle="modal" data-target="#tambahMahasiswaEksternal">Tambah Mahasiswa</a>
                    </div>
            </div>
            </div>
        </div>
        <div class="row g-0 pb-3 pl-2 pr-2">
        <div class="card-body">
            <table class="table table-striped">
            <thead class="thead-dark">
                <tr>
                <th scope="col" width="50px">No</th>
                <th scope="col" width="500px">Nama</th>
                <th scope="col" width="500px">Instansi</th>
                <th scope="col" width="500px">Status</th>
                <th scope="col" width="500px">Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $i = 1;
                    foreach($Mahasiswa as $mhs): 
                ?>
                <tr>
                <th scope="row" ><?=$i?></th>
                <td class="UserId" hidden><?=$mhs['Id']?></td>
                <td>
                    <div class="row">
                        <p><?=$mhs['Name']?></p>
                    </div>
                    <div class="row">
                        <p><?=$mhs['Username']?></p>
                    </div>
                </td>
                <td style="color:blue;">
                    <div class="row">
                        <p><?=$mhs['Fakultas']?></p>
                    </div>
                    <div class="row">
                        <p><?=$mhs['ProgramStudi']?></p>
                    </div>
                </td>
                <td style="color:green;">
                <b>
                    <?php
                        if ($mhs['RoleId'] == 2){
                            echo 'S1';
                        }else if($mhs['RoleId'] == 3){
                            echo 'S2';
                        }else if($mhs['RoleId'] == 4){
                            echo 'S3';
                        }
                    ?>
                    - 
                    <?=$mhs['Status']?>
                </b>
                </td>
                <td>
                    <button type="button" class="btn btn-warning editMahasiswaEksternal">Edit</button>
                    <button type="button" class="btn btn-info viewMahasiswaEksernal">Detil</button>
                    <a href="<?= base_url("sdm/DeleteMahasiswaEkstrnal/".$mhs['Id']);?>" type="button" class="btn btn-danger">Hapus</a>
                </td>
                </tr>
                <?php 
                    $i++;
                    endforeach; 
                ?>
            </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Tambah Mahasiswa Modal-->
<div class="modal fade" id="tambahMahasiswaEksternal"  role="dialog" aria-labelledby="exampleModalLabel"
    >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Mahasiswa Eksternal</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="<?= base_url('sdm/TambahMahasiswaEksternal'); ?>" >
                <div class="form-group row">
                    <label for="inputName" class="col-sm-4 col-form-label">Nama Mahasiswa</label>
                    <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputName" name="name" placeholder="Name" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputUsername" class="col-sm-4 col-form-label">NIM</label>
                    <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputUsername" name="username" placeholder="NIM" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputAngkatan" class="col-sm-4 col-form-label">Angkatan</label>
                    <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputAngkatan" name="angkatan" placeholder="Angkatan" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputJenjang" class="col-sm-4 col-form-label">Jenjang</label>
                    <div class="col-sm-8">
                    <select id="inputJenjang" name="role_id" class="form-control" required>
                        <option value="" selected>Pilih Jenjang</option>
                        <option value="2">S1</option>
                        <option value="3">S2</option>
                        <option value="4">S3</option>
                    </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="inputFakultas" class="col-sm-4 col-form-label">Fakultas</label>
                    <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputFakultas" name="fakultas" placeholder="Fakultas" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputProgramStudi" class="col-sm-4 col-form-label">Program Studi</label>
                    <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputProgramStudi" name="programStudi" placeholder="Program Studi" required>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Tambah</button>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- Import Mahasiswa Modal-->
<div class="modal fade" id="importMahasiswa"  role="dialog" aria-labelledby="exampleModalLabel"
    >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Import Mahasiswa</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
            <form method="post" action="<?= base_url('sdm/importMahasiswa'); ?>" enctype="multipart/form-data">
            <div class="form-group">

                <div class="form-group row">
                    <label for="role_id" class="col-sm-4 col-form-label">Status</label>
                    <div class="col-sm-8">
                    <select id="role_id" name="role_id" class="form-control">
                        <option value="" selected>Pilih Jenjang</option>
                        <option value="2">S1</option>
                        <option value="3">S2</option>
                        <option value="4">S3</option>
                    </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="uploadexcel" class="col-sm-4 col-form-label">File Excel</label>
                    <div class="col-sm-6">
                        <input type="file" class="form-control-file" id="uploadexcel" name="importexcel" accept=".xlsx,.xls">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Tambah</button>
            </div>
            </form>
            </div>
        </div>
    </div>
</div>


<!-- View Mahasiswa Model -->
<div class="modal fade" id="viewMahasiswa"  role="dialog" aria-labelledby="exampleModalLabel"
    >
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="view_mahasiswa">
            </div>
        </div>
    </div>
</div>

<!-- Edit Mahasiswa Model -->
<div class="modal fade" id="editMahasiswa"  role="dialog" aria-labelledby="exampleModalLabel"
    >
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Mahasiswa</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
            <form method="post" action="<?=base_url('sdm/editMahasiswa')?>" >
            <div class="edit_mahasiswa">
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Edit</button>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- End of Main Content -->