<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Card Bootstrap -->
    <div class="card mb-3" style="max-width: 1500px;">
        <div class="col-md-6 mt-3 ml-2">
            <h3>Data SDM</h3>
        </div>
        <div class="row g-0 pt-4 pb-3 pl-2 pr-2">

        <!-- Pendek -->
        <!-- Data Dosen -->
        <div class="col-sm-3 pb-2">
            <a href="<?=base_url('sdm/DataDosen')?>">
                <div class="card bg-gradient-light" style="height: 18rem;">
                    <div class="card-body mx-auto">
                        <i class="fa-solid fa-fw fa-user-tie fa-8x"></i>
                        <p></p>
                        <h4 class="card-text text-gray-800" style="text-align:center;">Data Dosen</h4>
                    </div> 
                    <div class="card-footer bg-gray-400">
                    <button class="btn btn-primary btn-user btn-block">
                        <b>
                            Detail
                            <i class="fa-regular fa-fw fa-circle-right fa-1x"></i>
                        </b>
                    </button>
                    </div>
                </div>
            </a>
        </div>

        <!-- Staff Pendidikan -->
        <div class="col-sm-3 pb-2">
            <a href="<?=base_url('sdm/DataStaff')?>">
                <div class="card bg-gradient-light" style="height: 18rem;">
                    <div class="card-body mx-auto">
                        <i class="fa-solid fa-fw fa-user-tie fa-8x"></i>
                        <p></p>
                        <h4 class="card-text text-gray-800" style="text-align:center;">Staff Kependidikan</h4>
                    </div> 
                    <div class="card-footer bg-gray-400">
                    <button class="btn btn-primary btn-user btn-block">
                        <b>
                            Detail
                            <i class="fa-regular fa-fw fa-circle-right fa-1x"></i>
                        </b>
                    </button>
                    </div>
                </div>
            </a>
        </div>

        <!-- Data Mahasiswa -->
        <?php 
        
            $kadep = $this->session->userdata['Kadep'];
            $sekdep = $this->session->userdata['Sekdep'];
            $kaprodiS1 = $this->session->userdata['KaprodiS1'];
            $kaprodiS2 = $this->session->userdata['KaprodiS2'];
            $kaprodiS3 = $this->session->userdata['KaprodiS3'];
            $admin = [0,5];

            if((in_array($this->session->userdata['RoleId'], $admin)) || ($kadep == true || $sekdep == true || $kaprodiS1 == true || $kaprodiS2 == true || $kaprodiS3 == true)):
        ?>
        <div class="col-sm-3 pb-2">
            <a href="<?=base_url('sdm/DataMahasiswa')?>">
                <div class="card bg-gradient-light" style="height: 18rem;">
                    <div class="card-body mx-auto">
                        <div class="row">
                            <i class="fa-solid fa-fw fa-user-graduate fa-8x mx-auto"></i>
                        </div>
                        <div class="row mt-3">
                            <h4 class="card-text text-gray-800" style="text-align:center;">Data Mahasiswa</h4>
                        </div>
                    </div> 
                    <div class="card-footer bg-gray-400">
                    <button class="btn btn-primary btn-user btn-block">
                        <b>
                            Detail
                            <i class="fa-regular fa-fw fa-circle-right fa-1x"></i>
                        </b>
                    </button>
                    </div>
                </div>
            </a>
        </div>

        <!-- Data Mahasiswa Non Kimia -->
        <div class="col-sm-3 pb-2">
            <a href="<?=base_url('sdm/DataMahasiswaEksternal')?>">
                <div class="card bg-gradient-light" style="height: 18rem;">
                    <div class="card-body">
                        <div class="row">
                            <i class="fa-solid fa-fw fa-user-graduate fa-8x mx-auto"></i>
                        </div>
                        <div class="row mt-3">
                            <h4 class="card-text text-gray-800" style="text-align:center;">Data Mahasiswa Non Kimia</h4>
                        </div>
                    </div> 
                    <div class="card-footer bg-gray-400">
                        <button class="btn btn-primary btn-user btn-block">
                            <b>
                                Detail
                                <i class="fa-regular fa-fw fa-circle-right fa-1x"></i>
                            </b>
                        </button>
                    </div>
                </div>
            </a>
        </div>
        <?php endif; ?>
        
        </div>
    </div>

    </div>
    <!-- /.container-fluid -->

</div>
<!-- End of Main Content -->