<!-- Begin Page Content -->
    <div class="container-fluid">
        <!-- Filter -->
        <div class="card">
            <div class="row mt-3 ml-2 mb-0">
                <div class="col">
                <form method="post" action="<?= base_url('sdm/DataDosen'); ?>" enctype="multipart/form-data">
                    <div class="form-row">
                            <div class="form-group col-md-2">
                            <select id="ProgramStudi" name="ProgramStudi" class="form-control" data-live-search="true" data-width="100%">
                                <option value="">Pilih Program Studi</option>
                                <option value="S1 Kimia">S1 Kimia</option>
                                <option value="S1 Biologi">S1 Biologi</option>
                                <option value="S1 Fisika">S1 Fisika</option>
                                <option value="S1 Matematika">S1 Matematika</option>
                                <option value="S1 Statistika">S1 Statistika</option>
                                <option value="S1 Ilmu Aktuaria">S1 Ilmu Aktuaria</option>
                                <option value="S1 Instrumentasi">S1 Instrumentasi</option>
                                <option value="S1 Teknik Geofisika">S1 Teknik Geofisika</option>
                                <!--  -->
                                <option value="S2 Kimia">S2 Kimia</option>
                                <option value="S2 Biologi">S2 Biologi</option>
                                <option value="S2 Fisika">S2 Fisika</option>
                                <option value="S2 Matematika">S2 Matematika</option>
                                <option value="S2 Statistika">S2 Statistika</option>
                                <!--  -->
                                <option value="S3 Kimia">S3 Kimia</option>
                                <option value="S3 Biologi">S3 Biologi</option>
                                <option value="S3 Fisika">S3 Fisika</option>
                                <option value="S3 Matematika">S3 Matematika</option>
                            </select>
                        </div>
                        <div class="form-group col-md-2">
                            <select id="laboratorium" name="laboratorium" class="form-control">
                                <option value="" selected>Pilih Laboratorium</option>
                                    <?php foreach ($Lab as $lab): ?>
                                        <option value="<?= $lab['Id'] ?>" ><?= $lab['Nama']?></option>
                                    <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group col-md-2">
                            <div class="form-row">
                                <div class="form-group col">
                                <select id="limit" name="limit" class="form-control">
                                    <option value="">Tampil Data</option>
                                    <option value="10">10</option>
                                    <option value="25">25</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-2">
                            <input type="text" class="form-control" id="search" name="search" placeholder="Nama Dosen">
                        </div>
                        <div class="form-group col-md-2">
                            <button type="submit" class="btn btn-secondary">Cari</button>
                        </div>
                    </div>
                </form>
                </div>
            </div>
        </div>
        
        <!-- card Dosen -->
        <div class="card">
            <div class="row mt-3 ml-2 mr-2">
                <div class="col-2">
                    <h4 class="text-white bg-dark">Data Dosen</h4>
                </div>
                <div class="col-8">
                </div>
                <div class="col-2">
                    <?php if($this->session->userdata['RoleId'] == 0 || $this->session->userdata['RoleId'] == 5):?>
                    <div class="btn-group">
                        <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa-solid fa-user-plus"></i>
                            Tambah
                        </button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" data-toggle="modal" data-target="#tambahDosen">Tambah Dosen</a>
                            <a class="dropdown-item" data-toggle="modal" data-target="#tambahDosenLuar">Tambah Dosen Luar</a>
                            <a class="dropdown-item" data-toggle="modal" data-target="#importDosen">Import Dosen</a>
                        </div>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                <?php foreach ($Dosen as $dsn): ?>
                <div class="col-lg-3 d-flex align-items-stretch">
                    <div class="card bg-gradient-light">
                        <div class="card-body">
                            <img src="<?= base_url('assets/img/profiles/') . $dsn['ImageProfile'] ?>" class="img-fluid rounded-start">
                            <p></p>
                            <p class="card-text text-gray-800">Nama : <?= $dsn['Name'] ?></p>
                            <p class="card-text text-gray-800">Laboratorium : <?= $dsn['Lab'] ?></p>
                        </div> 
                        <div class="card-footer bg-gray-400">
                        <a href="<?= base_url('sdm/DetailDosen/'.$dsn['Id']); ?>" class="btn btn-primary btn-user btn-block">
                            <b>
                                Detail
                                <i class="fa-regular fa-fw fa-circle-right fa-1x"></i>
                            </b>
                        </a>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
        </div>
    </div>

    <!-- Tambah Dosen Modal-->
    <div class="modal fade" id="tambahDosen"  role="dialog" aria-labelledby="exampleModalLabel"
        >
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Dosen</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" action="<?= base_url('sdm/TambahDosen'); ?>" >
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 col-form-label">Nama Dosen</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputName" name="name" placeholder="Name" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputUsername" class="col-sm-4 col-form-label">NIP</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputUsername" name="username" placeholder="NIP" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputEmail" class="col-sm-4 col-form-label">Email</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputEmail" name="email" placeholder="Email" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputLaboratorium" class="col-sm-4 col-form-label">Laboratorium</label>
                        <div class="col-sm-8">
                        <select id="inputLaboratorium" name="laboratorium" class="form-control" required>
                            <option value="" selected>Pilih Lab</option>
                            <?php foreach ($Lab as $lab): ?>
                                <option value="<?= $lab['Id'] ?>"><?= $lab['Nama']?></option>
                            <?php endforeach; ?>
                        </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputBidang" class="col-sm-4 col-form-label">Bidang Keilmuan</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputBidang" name="bidang" placeholder="Bidang" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputProgramStudi" class="col-sm-4 col-form-label">Program Studi</label>
                        <div class="col-sm-8">
                        <select id="inputProgramStudi" name="programstudi" class="form-control" required>
                            <option value="" selected>Pilih Program Studi</option>
                            <option value="Kimia">Kimia</option>
                        </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                    <button class="btn btn-primary" type="submit">Tambah</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Tambah Dosen Luar Modal-->
    <div class="modal fade" id="tambahDosenLuar"  role="dialog" aria-labelledby="exampleModalLabel"
        >
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Dosen Luar</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" action="<?= base_url('sdm/TambahDosenLuar'); ?>" >
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 col-form-label">Nama</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputName" name="name" placeholder="Name" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputEmail" class="col-sm-4 col-form-label">Email</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputEmail" name="email" placeholder="Email" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputInstitusi" class="col-sm-4 col-form-label">Institusi</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputInstitusi" name="institusi" placeholder="Institusi" required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                    <button class="btn btn-primary" type="submit">Tambah</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Import Dosen Modal-->
    <div class="modal fade" id="importDosen"  role="dialog" aria-labelledby="exampleModalLabel"
        >
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Import Dosen</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                <form method="post" action="<?= base_url('sdm/importDosen'); ?>" enctype="multipart/form-data">
                <div class="form-group">
                    <div class="form-group row">
                        <label for="uploadexcel" class="col-sm-4 col-form-label">File Excel</label>
                        <div class="col-sm-6">
                            <input type="file" class="form-control-file" id="uploadexcel" name="importexcel" accept=".xlsx,.xls">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="<?=base_url('sdm/TemplateExcelTendik');?>">Unduh Template</a>
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                    <button class="btn btn-primary" type="submit">Tambah</button>
                </div>
                </form>
                </div>
            </div>
        </div>
    </div>

<!-- End of Main Content -->