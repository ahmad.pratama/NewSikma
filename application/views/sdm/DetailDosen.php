<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Card Bootstrap -->
    <div class="card mb-3">

        <?php if($this->session->userdata['RoleId'] == 0 || $this->session->userdata['RoleId'] == 5):?>
        <div class="card-header py-4 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary"></h6>
            <div class="dropdown no-arrow">
                <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa-solid fa-fw fa-sliders fa-1x"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in"
                    aria-labelledby="dropdownMenuLink">
                    <div class="dropdown-header">Setting</div>
                    <a class="dropdown-item" data-toggle="modal" data-target="#editDosen">Edit</a>
                    <a class="dropdown-item" href="<?= base_url('sdm/DeleteDosen/'.$Dosen['Id']);?>">Non-Aktifkan Dosen</a>
                    <a class="dropdown-item" href="<?= base_url('sdm/ResetPasswordDosen/'.$Dosen['Id']);?>">Reset Password</a>
                    <a class="dropdown-item" data-toggle="modal" data-target="#uploadTTDDosen">Unggah TTD</a>
                    <a class="dropdown-item" data-toggle="modal" data-target="#uploadProfileDosen">Unggah Photo Profile</a>
                </div>
            </div>
        </div>
        <?php endif; ?>
        <div class="card-body">
        <div class="row g-0">
            <div class="col-md-4">
                <img src="<?= base_url('assets/img/profiles/') . $Dosen['ImageProfile'] ?>" class="img-fluid rounded-start">
            </div>
            <div class="col-md-0">
                <div class="card-body">
                    <h4 class="card-title">Nama Lengkap : <?= $Dosen['Name'] ?></h4> 
                    <h5 class="card-title">Laboratorium : <?= $Dosen['Lab'] ?></h5>
                    <h5 class="card-title">NIDN / NIP : <?= $Dosen['NIDN'] ?> / <?= $Dosen['Username'] ?></h5>
                    <h5 class="card-title">Email : <?= $Dosen['Email'] ?></h5>
                </div>
            </div>
        </div>
        <div class="row g-0">
            <!-- Pendidikan -->
            <div class="col-md-4 p-4">
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Pendidikan</h6>
                    </div>
                    <div class="card-body">
                        <h4 class="font-weight-bold">Sarjana</h4>
                        <p><?=$Dosen['PendidikanS1']?> - <?=$Dosen['LulusS1']?></p>
                        <hr>
                        <h4 class="font-weight-bold">Magister</h4>
                        <p><?=$Dosen['PendidikanS2']?> - <?=$Dosen['LulusS2']?></p>
                        <hr>
                        <h4 class="font-weight-bold">Spesialis</h4>
                        <p><?=$Dosen['PendidikanS3']?> - <?=$Dosen['LulusS3']?></p>
                        <hr>
                    </div>
                </div>
            </div>
            <!-- Notes -->
            <div class="col-md-4 p-4">
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Notes</h6>
                    </div>
                    <div class="card-body">
                    <table class="table">
                        <thead class="thead-dark">
                            <tr>
                            <th scope="col">#</th>
                            <th scope="col" style="text-align: center">Note</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                            <th scope="row">1</th>
                            <td>Melakukan Pengujian Pukul 10.00</td>
                            </tr>
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
            <div class="col-md-4 p-4">
                <!-- Project Card Example -->
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Riset</h6>
                    </div>
                    <div class="card-body">
                    <table class="table">
                        <thead class="thead-dark">
                            <tr>
                            <th scope="col">#</th>
                            <th scope="col" style="text-align: center">Riset</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                            <th scope="row">1</th>
                            <td>Biokimia</td>
                            </tr>
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
    <!-- /.container-fluid -->

</div>


<!-- Upload Profile Dosen Modal-->
<div class="modal fade" id="uploadProfileDosen"  role="dialog" aria-labelledby="exampleModalLabel"
    >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Unggah Profile Dosen</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
            <form method="post" action="<?= base_url('sdm/UploadProfilePhotoDosen'); ?>" enctype="multipart/form-data">
            <div class="form-group">
                <div class="form-group row">
                    <div class="col-sm-2">Picture</div>
                    <div class="col-sm-10">
                        <div class="row">
                            <div class="col-sm-3">
                                <img src="<?= base_url('assets/img/profiles/') . $Dosen['ImageProfile'] ?>" class="img-thumbnail">
                            </div>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="inputUserId" name="userId" value="<?=$Dosen['Id']?>" hidden>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="customFile" name="image" accept=".jpg,.png,.jpeg">
                                    <label class="custom-file-label" for="customFile">Choose file</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Unggah</button>
            </div>
            </form>
            </div>
        </div>
    </div>
</div>

<!-- Upload TTD Dosen Modal-->
<div class="modal fade" id="uploadTTDDosen"  role="dialog" aria-labelledby="exampleModalLabel"
    >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Unggah TTD Dosen</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
            <form method="post" action="<?= base_url('sdm/UploadTTDDosen'); ?>" enctype="multipart/form-data">
            <div class="form-group">
                <div class="form-group row">
                    <div class="col-sm-4">Tanda Tangan</div>
                    <div class="col-sm-8">
                        <div class="row">
                            <div class="col-sm">
                                <input type="text" class="form-control" id="inputUserId" name="userId" value="<?=$Dosen['Id']?>" hidden>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="customFile" name="image" accept=".png">
                                    <label class="custom-file-label" for="customFile">Choose file</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Unggah</button>
            </div>
            </form>
            </div>
        </div>
    </div>
</div>


<!-- Edit User Modal-->
<div class="modal fade" id="editDosen"  role="dialog" aria-labelledby="exampleModalLabel"
    >
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Data Dosen</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="<?= base_url('sdm/EditDosen'); ?>" enctype="multipart/form-data">
                <div class="card">
                    <div class="card-body">
                        <div class="form-group row">
                            <label for="inputName" class="col-sm-4 col-form-label">Nama Lengkap Dosen</label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputName" name="id" value="<?= $Dosen['Id'] ?>" hidden>
                            <input type="text" class="form-control" id="inputName" name="name" placeholder="Name" value="<?= $Dosen['Name']?>" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputPassword" class="col-sm-4 col-form-label">Password</label>
                            <div class="col-sm-8">
                            <input type="password" class="form-control" id="inputPassword" name="password" placeholder="New Password">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputTelpon" class="col-sm-4 col-form-label">Telpon</label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputTelpon" name="telpon" placeholder="Telpon" value="<?= $Dosen['Telpon']?>" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail" class="col-sm-4 col-form-label">Email</label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputEmail" name="email" placeholder="Email" value="<?= $Dosen['Email']?>" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputAlamatMalang" class="col-sm-4 col-form-label">Alamat Di Malang</label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputAlamatMalang" name="alamatMalang" placeholder="Alamat Di Malang" value="<?= $Dosen['AlamatMalang']?>" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputAlamat" class="col-sm-4 col-form-label">Alamat</label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputAlamat" name="alamat" placeholder="Alamat" value="<?= $Dosen['Alamat']?>" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <div class="form-group row">
                            <label for="inputNidn" class="col-sm-4 col-form-label">NIDN</label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputNidn" name="nidn" placeholder="NIDN" value="<?= $Dosen['NIDN']?>" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputLab" class="col-sm-4 col-form-label">Laboratorium</label>
                            <div class="col-sm-8">
                            <select name="laboratorium" id="inputLab" class="form-control" required>
                                <?php foreach ($Lab as $lab): ?>
                                    <option value="<?= $lab['Id'] ?>" <?= ($lab['Id'] == $Dosen['Laboratorium']) ? 'selected' : '' ?>><?= $lab['Nama']?></option>
                                <?php endforeach; ?>
                            </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputBidang" class="col-sm-4 col-form-label">Bidang Keilmuan</label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputBidang" name="bidang" placeholder="Bidang Keilmuan" value="<?= $Dosen['Bidang']?>" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputGolongan" class="col-sm-4 col-form-label">Golongan</label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputGolongan" name="golongan" placeholder="Golongan" value="<?= $Dosen['Golongan']?>" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputPangkat" class="col-sm-4 col-form-label">Pangkat</label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputPangkat" name="pangkat" placeholder="Pangkat" value="<?= $Dosen['Pangkat']?>" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputJabatan" class="col-sm-4 col-form-label">Jabatan</label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputJabatan" name="jabatan" placeholder="Jabatan" value="<?= $Dosen['Jabatan']?>"required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputTugasTambahan" class="col-sm-4 col-form-label">Tugas Tambahan</label>
                            <div class="col-sm-8">
                            <textarea class="form-control" rows="2" id="inputTugasTambahan" name="tugasTambahan" required><?=$Dosen['TugasTambahan']?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <div class="form-group row">
                            <label for="inputPendidikanS1" class="col-sm-4 col-form-label">Pendidikan S1</label>
                            <div class="col-sm-4">
                            <input type="text" class="form-control" id="inputPendidikanS1" name="pendidikanS1" placeholder="Pendidikan S1" value="<?= $Dosen['PendidikanS1']?>"required>
                            </div>
                            <div class="col-sm-3">
                            <input type="text" class="form-control" id="inputPendidikanS1" name="lulusS1" placeholder="Tahun" value="<?= $Dosen['LulusS1']?>"required>
                            </div>
                            <div class="col-sm-1 p-0">
                                <label for="file-input">
                                    <i class="fa-solid fa-fw fa-file-arrow-up fa-2x"></i> 
                                </label>
                                <input type="file" id="file-input" name="ijazahS1" style="display: none;" accept=".png,.jpg">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputPendidikanS2" class="col-sm-4 col-form-label">Pendidikan S2</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="inputPendidikanS2" name="pendidikanS2" placeholder="Pendidikan S2" value="<?= $Dosen['PendidikanS2']?>" required>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" id="inputPendidikanS2" name="lulusS2" placeholder="Tahun" value="<?= $Dosen['LulusS2']?>" required>
                            </div>
                            <div class="col-sm-1 p-0">
                                <label for="file-input">
                                    <i class="fa-solid fa-fw fa-file-arrow-up fa-2x"></i> 
                                </label>
                                <input type="file" id="file-input" name="ijazahS2" style="display: none;" accept=".png,.jpg">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputPendidikanS3" class="col-sm-4 col-form-label">Pendidikan S3</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="inputPendidikanS3" name="pendidikanS3" placeholder="Pendidikan S3" value="<?= $Dosen['PendidikanS3']?>" required>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" id="inputPendidikanS3" name="lulusS3" placeholder="Tahun" value="<?= $Dosen['LulusS3']?>" required>
                            </div>
                            <div class="col-sm-1 p-0">
                                <label for="file-input">
                                    <i class="fa-solid fa-fw fa-file-arrow-up fa-2x"></i> 
                                </label>
                                <input type="file" id="file-input" name="ijazahS3" style="display: none;" accept=".png,.jpg">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-warning" type="submit">Simpan</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- End of Main Content -->