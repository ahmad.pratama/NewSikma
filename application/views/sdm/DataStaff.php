<!-- Begin Page Content -->
    <div class="container-fluid">
        <!-- Filter -->
        <div class="card">
            <div class="row mt-3 ml-2 mb-0">
                <div class="col">
                <form method="post" action="<?= base_url('sdm/DataStaff'); ?>" enctype="multipart/form-data">
                    <div class="form-row">
                        <div class="form-group col-md-2">
                            <div class="form-row">
                                <div class="form-group col">
                                <select id="limit" name="limit" class="form-control">
                                    <option value="">Tampil Data</option>
                                    <option value="10">10</option>
                                    <option value="25">25</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-2">
                            <input type="text" class="form-control" id="search" name="search" placeholder="Nama Staff">
                        </div>
                        <div class="form-group col-md-2">
                            <button type="submit" class="btn btn-secondary">Cari</button>
                        </div>
                    </div>
                </form>
                </div>
            </div>
        </div>
        
        <!-- Table Mahasiswa -->
        <div class="card">
            <div class="row mt-3 ml-2 mr-2">
                <div class="col-5">
                    <h4 class="text-white bg-dark">Data Staff Tenaga Kependidikan</h4>
                </div>
                <div class="col-5">
                </div>
                <div class="col-2">
                    <?php if($this->session->userdata['RoleId'] == 0):?>
                    <div class="btn-group">
                        <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa-solid fa-user-plus"></i>
                            Tambah
                        </button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" data-toggle="modal" data-target="#tambahStaff">Tambah Staff</a>
                            <!-- <a class="dropdown-item" data-toggle="modal" data-target="#importStaff">Import Staff</a> -->
                        </div>
                    </div>
                    <?php endif;?>
                </div>
            </div>
            <div class="row g-0 pb-3 pl-2 pr-2">
            <div class="card-body">
                <table class="table table-striped">
                <thead class="thead-dark">
                    <tr>
                    <th scope="col" width="50px">No</th>
                    <th scope="col" width="500px">Nama</th>
                    <th scope="col" width="500px">Jabatan Fungsional</th>
                    <th scope="col" width="500px">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $i = 1;
                        foreach ($Staff as $staff) :
                    ?>
                    <tr>
                    <th scope="row" ><?= $i ?></th>
                    <td class="UserId" hidden><?= $staff['Id'] ?></td>
                    <td>
                        <div class="row">
                            <p><?=$staff['Name']?></p>
                        </div>
                    </td>
                    <td>
                        <div class="row">
                            <p>
                               <?= $staff['Jabatan'] ?> - <?=$staff['Bagian']?>
                            </p>
                        </div>
                    </td>
                    <td>
                        <div class="btn-group">
                            <button type="button" class="btn btn-warning btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <!-- <i class="fa-solid fa-user-plus"></i> -->
                                Aksi
                                <i class="fa-solid fa-sliders"></i>
                            </button>
                            <div class="dropdown-menu">
                                <?php 
                                $admin = [0,5]; 
                                if(in_array($this->session->userdata['RoleId'], $admin)) :
                                ?>
                                <div class="row mx-auto p-1">
                                    <button type="button" class="btn btn-warning editStaff">Edit</button>
                                </div>
                                <div class="row mx-auto p-1">
                                    <a href="<?= base_url("sdm/DeleteStaff/".$staff['Id']);?>" type="button" class="btn btn-danger">Hapus</a>
                                </div>
                                <div class="row mx-auto p-1">
                                    <a href="<?= base_url("sdm/ResetPasswordStaff/".$staff['Id']); ?>" class="btn btn-warning" type="button" data-dismiss="modal">Reset Password</a>
                                </div>
                                <?php endif; ?>
                                <div class="row mx-auto p-1">
                                    <button type="button" class="btn btn-info viewStaff">Detil</button>
                                </div>
                            </div>
                        </div>
                    </td>
                    </tr>
                    <?php 
                        endforeach;
                        $i++;
                    ?>
                </tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- Tambah Staff Modal-->
    <div class="modal fade" id="tambahStaff"  role="dialog" aria-labelledby="exampleModalLabel"
        >
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Staff</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" action="<?= base_url('sdm/TambahStaff'); ?>" >
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 col-form-label">Nama Staff</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputName" name="name" placeholder="Name">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputNIP" class="col-sm-4 col-form-label">NIP</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputNIP" name="NIP" placeholder="NIP">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputEmail" class="col-sm-4 col-form-label">Email</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputEmail" name="email" placeholder="Email">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputJabatan" class="col-sm-4 col-form-label">Jabatan Fungsional</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputJabatan" name="jabatan" placeholder="Jabatan Fungsional">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputBagian" class="col-sm-4 col-form-label">Bagian</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputBagian" name="bagian" placeholder="Bagian">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputIsPLP" class="col-sm-4 col-form-label">PLP</label>
                        <div class="col-sm-8">
                        <select id="inputIsPLP" name="isPLP" class="form-control" required>
                            <option value="1" selected>Ya</option>
                            <option value="0" selected>Tidak</option>
                        </select>
                        </div>
                    </div>
                    <div class="form-group row" id="inputLab">
                        <label for="inputLaboratorium" class="col-sm-4 col-form-label">Laboratorium</label>
                        <div class="col-sm-8">
                        <select id="inputLaboratorium" name="laboratorium" class="form-control">
                            <option value="" selected>Pilih Lab</option>
                            <?php foreach ($Lab as $lab): ?>
                                <option value="<?= $lab['Id'] ?>"><?= $lab['Nama']?></option>
                            <?php endforeach; ?>
                        </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                    <button class="btn btn-primary" type="submit">Tambah</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Detail Staff Model -->
    <div class="modal fade" id="viewStaff" role="dialog" aria-labelledby="exampleModalLabel"
        >
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Detail Staff Tenaga Kependidikan</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                <div class="card">
                    <div class="card-body">
                        <div class="form-group row">
                            <label for="inputName" class="col-sm-4 col-form-label">Nama</label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputName" name="name" disabled>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail" class="col-sm-4 col-form-label">Email</label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputEmail" name="email" disabled>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputGolongan" class="col-sm-4 col-form-label">Golongan</label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputGolongan" name="golongan" disabled>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputPangkat" class="col-sm-4 col-form-label">Pangkat</label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputPangkat" name="pangkat" disabled>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputJabatan" class="col-sm-4 col-form-label">Jabatan</label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputJabatan" name="jabatan" disabled>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputbagian" class="col-sm-4 col-form-label">Bagian</label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputbagian" name="bagian" disabled>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputPendidikan" class="col-sm-4 col-form-label">Pendidikan Terakhir</label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputPendidikan" name="pendidikan" disabled>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputDepartemen" class="col-sm-4 col-form-label">Departemen</label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputDepartemen" name="departemen" disabled>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputLaboratorium" class="col-sm-4 col-form-label">Laboratorium</label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputLaboratorium" name="laboratorium" disabled>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Edit Staff Model -->
    <div class="modal fade" id="editStaff" role="dialog" aria-labelledby="exampleModalLabel"
        >
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Staff Tenaga Kependidikan</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
            <form method="post" action="<?=base_url('sdm/editStaff')?>"  enctype="multipart/form-data">
                <div class="card">
                    <div class="card-body">
                        <div class="form-group row">
                            <label for="inputName" class="col-sm-4 col-form-label">Nama</label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputName" name="UserId" hidden>
                            <input type="text" class="form-control" id="inputName" name="name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail" class="col-sm-4 col-form-label">Email</label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputEmail" name="email">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputGolongan" class="col-sm-4 col-form-label">Golongan</label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputGolongan" name="golongan">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputPangkat" class="col-sm-4 col-form-label">Pangkat</label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputPangkat" name="pangkat">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputJabatan" class="col-sm-4 col-form-label">Jabatan</label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputJabatan" name="jabatan">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputbagian" class="col-sm-4 col-form-label">Bagian</label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputbagian" name="bagian">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputPendidikan" class="col-sm-4 col-form-label">Pendidikan Terakhir</label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputPendidikan" name="pendidikan">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputDepartemen" class="col-sm-4 col-form-label">Departemen</label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputDepartemen" name="departemen">
                            </div>
                        </div>
                        <div class="form-group row" id="inputLab">
                            <label for="inputLaboratorium" class="col-sm-4 col-form-label">Laboratorium</label>
                            <div class="col-sm-8">
                            <select id="inputLaboratorium" name="laboratorium" class="form-control">
                                <option value="" selected>Pilih Lab</option>
                                <?php foreach ($Lab as $lab): ?>
                                    <option value="<?= $lab['Id'] ?>"><?= $lab['Nama']?></option>
                                <?php endforeach; ?>
                            </select>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Tutup</button>
                    <button class="btn btn-primary" type="submit">Edit</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Import Staff Modal-->
    <div class="modal fade" id="importStaff"  role="dialog" aria-labelledby="exampleModalLabel"
        >
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Import Staff</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                <form method="post" action="<?= base_url('sdm/importStaff'); ?>" enctype="multipart/form-data">
                <div class="form-group">
                    <div class="form-group row">
                        <label for="uploadexcel" class="col-sm-4 col-form-label">File Excel</label>
                        <div class="col-sm-6">
                            <input type="file" class="form-control-file" id="uploadexcel" name="importexcel" accept=".xlsx,.xls">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="<?=base_url('sdm/TemplateExcelTendik');?>">Unduh Template</a>
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                    <button class="btn btn-primary" type="submit">Tambah</button>
                </div>
                </form>
                </div>
            </div>
        </div>
    </div>
<!-- End of Main Content -->