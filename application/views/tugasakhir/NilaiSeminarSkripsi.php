<!-- Begin Page Content -->
<div class="container-fluid">
    <?php 
        $mhs = [2,3,4];
        if (!in_array($this->session->userdata('RoleId'), $mhs)):
    ?>
    <div class="card">
        <div class="row mt-3 ml-2 mr-2">
            <div class="col-4">
                <h4 class="text-white bg-dark">Penilaian Seminar Ujian Akhir (Skripsi)</h4>
            </div>
            <div class="col-5">
                <input id="TaId" type="text" value="<?= $TaId ?>" hidden>
            </div>
            <div class="col-3">
            </div>
        </div>
        <form method="post" action="<?= base_url('TugasAkhir/SubmitNilaiSkripsi/'. $TaId); ?>" >
            <?php 
                if($TipeDosen == 0 && in_array($this->session->userdata('RoleId'), [0,5])):
            ?>
                <div class="row mt-3 ml-4 mr-4">
                    <div class="col-2">
                        <label for="tipeDosen">Silahkan Pilih Dosen</label>
                        <select name="tipeDosen" id="tipeDosen" class="form-control">
                            <option value="1">Dosen 1</option>
                            <option value="2">Dosen 2</option>
                            <option value="3">Dosen 3</option>
                        </select>
                    </div>
                    <div class="col-2">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item active" aria-current="page"><?= $TA['NamaMahasiswa'] ?></li>
                            </ol>
                        </nav>
                    </div>
                    <div class="col-8">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                <th scope="col">Dosen 1</th>
                                <th scope="col">Dosen 2</th>
                                <th scope="col">Dosen 3</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                <td><?= $TA['Dosen1Name'] ?></td>
                                <td><?= $TA['Dosen2Name'] ?></td>
                                <td><?= $TA['Dosen3Name'] ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            <?php endif; ?>
            <div class="row mt-3 ml-2 mr-2">
                <div class="col-6">
                    <h4 class="text-white bg-dark">Presentasi</h4>
                    <table class="table">
                        <thead class="thead-light">
                            <tr>
                            <th scope="col">CPMK</th>
                            <th scope="col">Kategori</th>
                            <th scope="col">Nilai</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                            <td>8A</td>
                            <td>Langkah-langkah pengerjaan berdasarkan teori dan pengalaman</td>
                            <td class="col-6">
                                <select id="langkahLangkah_8A" class="form-control" name="langkahLangkah_8A">
                                    <option value="" data-info="">Pilih opsi</option>
                                    <option value="1" <?php 
                                        if ($NilaiUjianTA != null){
                                            switch ($this->session->userdata('Id')){
                                                case $NilaiUjianTA['Dosen1Id']:
                                                    if ($NilaiUjianTA['Dsn1Pres1'] == 1){
                                                        echo "selected";
                                                    }else{
                                                        echo "";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen2Id']:
                                                    if ($NilaiUjianTA['Dsn2Pres1'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen3Id']:
                                                    if ($NilaiUjianTA['Dsn3Pres1'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Tidak ada hubungan antara langkah pengerjaan dan data yang diperoleh">1</option>
                                    <option value="2" <?php 
                                        if ($NilaiUjianTA != null){
                                            switch ($this->session->userdata('Id')){
                                                case $NilaiUjianTA['Dosen1Id']:
                                                    if ($NilaiUjianTA['Dsn1Pres1'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen2Id']:
                                                    if ($NilaiUjianTA['Dsn2Pres1'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen3Id']:
                                                    if ($NilaiUjianTA['Dsn3Pres1'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Langkah-langkah pengerjaan hingga data yang diperoleh sangat kurang dijelaskan.">2</option>
                                    <option value="3" <?php 
                                        if ($NilaiUjianTA != null){
                                            switch ($this->session->userdata('Id')){
                                                case $NilaiUjianTA['Dosen1Id']:
                                                    if ($NilaiUjianTA['Dsn1Pres1'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen2Id']:
                                                    if ($NilaiUjianTA['Dsn2Pres1'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen3Id']:
                                                    if ($NilaiUjianTA['Dsn3Pres1'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                                default:
                                                    echo "";
                                                    break;
                                            }
                                        }
                                    ?> data-info="Langkah-langkah pengerjaan hingga mendapatkan data disampaikan dan langkah-langkah pengerjaan dibandingkan dengan teori atau hasil lain yang serupa.">3</option>
                                    <option value="4" <?php 
                                        if ($NilaiUjianTA != null){
                                            switch ($this->session->userdata('Id')){
                                                case $NilaiUjianTA['Dosen1Id']:
                                                    if ($NilaiUjianTA['Dsn1Pres1'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen2Id']:
                                                    if ($NilaiUjianTA['Dsn2Pres1'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen3Id']:
                                                    if ($NilaiUjianTA['Dsn3Pres1'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Langkah-langkah pengerjaan hingga mendapatkan data disampaikan dengan sangat baik dan langkah-langkah pengerjaan dibandingkan dengan teori atau hasil lain yang serupa.">4</option>
                                </select>
                                <div id="langkahLangkah_8AInfo" class="alert alert-info" style="display: none;"></div>
                            </td>
                            </tr>
                            <tr>
                            <td>8B</td>
                            <td>Membaca luaran hasil pengukuran</td>
                            <td class="col-6">
                                <select id="pengukuran_8B" class="form-control" name="pengukuran_8B">
                                    <option value="" data-info="">Pilih opsi</option>
                                    <option value="1" <?php
                                        if ($NilaiUjianTA != null){ 
                                            switch ($this->session->userdata('Id')){
                                                case $NilaiUjianTA['Dosen1Id']:
                                                    if ($NilaiUjianTA['Dsn1Pres2'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen2Id']:
                                                    if ($NilaiUjianTA['Dsn2Pres2'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen3Id']:
                                                    if ($NilaiUjianTA['Dsn3Pres2'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Penyaji hanya menyampaikan data atau informasi yang meragukan/ salah.">1</option>
                                    <option value="2" <?php 
                                        if ($NilaiUjianTA != null){
                                            switch ($this->session->userdata('Id')){
                                                case $NilaiUjianTA['Dosen1Id']:
                                                    if ($NilaiUjianTA['Dsn1Pres2'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen2Id']:
                                                    if ($NilaiUjianTA['Dsn2Pres2'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen3Id']:
                                                    if ($NilaiUjianTA['Dsn3Pres2'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Penyaji kurang dapat menjelaskan asal informasi penelitian.">2</option>
                                    <option value="3" <?php 
                                        if ($NilaiUjianTA != null){
                                            switch ($this->session->userdata('Id')){
                                                case $NilaiUjianTA['Dosen1Id']:
                                                    if ($NilaiUjianTA['Dsn1Pres2'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen2Id']:
                                                    if ($NilaiUjianTA['Dsn2Pres2'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen3Id']:
                                                    if ($NilaiUjianTA['Dsn3Pres2'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                                default:
                                                    echo "";
                                                    break;
                                            }
                                        }
                                    ?> data-info="Penyaji menjelaskan bagaimana data yang diperoleh diubah menjadi informasi penelitian.">3</option>
                                    <option value="4" <?php 
                                        if ($NilaiUjianTA != null){
                                            switch ($this->session->userdata('Id')){
                                                case $NilaiUjianTA['Dosen1Id']:
                                                    if ($NilaiUjianTA['Dsn1Pres2'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen2Id']:
                                                    if ($NilaiUjianTA['Dsn2Pres2'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen3Id']:
                                                    if ($NilaiUjianTA['Dsn3Pres2'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Penyaji menjelaskan dengan baik sekali bagaimana data yang diperoleh diubah menjadi informasi penelitian.">4</option>
                                </select>
                                <div id="pengukuran_8BInfo" class="alert alert-info" style="display: none;"></div>
                            </td>
                            </tr>
                            <tr>
                            <td>9B</td>
                            <td>Membahas hasil pengukuran</td>
                            <td class="col-6">
                                <select id="pengukuran_9C" class="form-control" name="pengukuran_9C">
                                    <option value="" data-info="">Pilih opsi</option>
                                    <option value="1" <?php 
                                        if ($NilaiUjianTA != null){
                                            switch ($this->session->userdata('Id')){
                                                case $NilaiUjianTA['Dosen1Id']:
                                                    if ($NilaiUjianTA['Dsn1Pres3'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen2Id']:
                                                    if ($NilaiUjianTA['Dsn2Pres3'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen3Id']:
                                                    if ($NilaiUjianTA['Dsn3Pres3'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Tidak ada nilai 1">1</option>
                                    <option value="2" <?php 
                                        if ($NilaiUjianTA != null){
                                            switch ($this->session->userdata('Id')){
                                                case $NilaiUjianTA['Dosen1Id']:
                                                    if ($NilaiUjianTA['Dsn1Pres3'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen2Id']:
                                                    if ($NilaiUjianTA['Dsn2Pres3'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen3Id']:
                                                    if ($NilaiUjianTA['Dsn3Pres3'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Hasil-hasil pengukuran dibandingkan dengan hasil lain yang tidak setara atau tidak dibandingkan.">2</option>
                                    <option value="3" <?php 
                                        if ($NilaiUjianTA != null){
                                            switch ($this->session->userdata('Id')){
                                                case $NilaiUjianTA['Dosen1Id']:
                                                    if ($NilaiUjianTA['Dsn1Pres3'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen2Id']:
                                                    if ($NilaiUjianTA['Dsn2Pres3'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen3Id']:
                                                    if ($NilaiUjianTA['Dsn3Pres3'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Hasil-hasil pengukuran dibandingkan dengan hasil-hasil yang ada dalam kepustakaan.">3</option>
                                    <option value="4" <?php 
                                        if ($NilaiUjianTA != null){
                                            switch ($this->session->userdata('Id')){
                                                case $NilaiUjianTA['Dosen1Id']:
                                                    if ($NilaiUjianTA['Dsn1Pres3'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen2Id']:
                                                    if ($NilaiUjianTA['Dsn2Pres3'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen3Id']:
                                                    if ($NilaiUjianTA['Dsn3Pres3'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Hasil-hasil pengukuran secara sangat baik dibandingkan dengan hasil-hasil yang ada dalam kepustakaan.">4</option>
                                </select>
                                <div id="pengukuran_9CInfo" class="alert alert-info" style="display: none;"></div>
                            </td>
                            </tr>
                            <tr>
                            <td>8C</td>
                            <td>Pengambilan kesimpulan</td>
                            <td class="col-6">
                                <select id="kesimpulan_8C" class="form-control" name="kesimpulan_8C">
                                    <option value="" data-info="">Pilih opsi</option>
                                    <option value="1" <?php 
                                        if ($NilaiUjianTA != null){
                                            switch ($this->session->userdata('Id')){
                                                case $NilaiUjianTA['Dosen1Id']:
                                                    if ($NilaiUjianTA['Dsn1Pres4'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen2Id']:
                                                    if ($NilaiUjianTA['Dsn2Pres4'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen3Id']:
                                                    if ($NilaiUjianTA['Dsn3Pres4'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Tidak ada nilai 1">1</option>
                                    <option value="2" <?php 
                                        if ($NilaiUjianTA != null){
                                            switch ($this->session->userdata('Id')){
                                                case $NilaiUjianTA['Dosen1Id']:
                                                    if ($NilaiUjianTA['Dsn1Pres4'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen2Id']:
                                                    if ($NilaiUjianTA['Dsn2Pres4'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen3Id']:
                                                    if ($NilaiUjianTA['Dsn3Pres4'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Penyaji langsung menyimpulkan setiap data percobaan.">2</option>
                                    <option value="3" <?php 
                                        if ($NilaiUjianTA != null){
                                            switch ($this->session->userdata('Id')){
                                                case $NilaiUjianTA['Dosen1Id']:
                                                    if ($NilaiUjianTA['Dsn1Pres4'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen2Id']:
                                                    if ($NilaiUjianTA['Dsn2Pres4'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen3Id']:
                                                    if ($NilaiUjianTA['Dsn3Pres4'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Penyaji menyampaikan evaluasi terhadap informasi dan menarik kesimpulan.">3</option>
                                    <option value="4" <?php 
                                        if ($NilaiUjianTA != null){
                                            switch ($this->session->userdata('Id')){
                                                case $NilaiUjianTA['Dosen1Id']:
                                                    if ($NilaiUjianTA['Dsn1Pres4'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen2Id']:
                                                    if ($NilaiUjianTA['Dsn2Pres4'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen3Id']:
                                                    if ($NilaiUjianTA['Dsn3Pres4'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Penyaji menyampaikan analisis dan evaluasi sangat baik terhadap informasi dan menarik kesimpulan yang tepat.">4</option>
                                </select>
                                <div id="kesimpulan_8CInfo" class="alert alert-info" style="display: none;"></div>
                            </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-6">
                    <h4 class="text-white bg-dark">Berkas Presentasi</h4>
                    <table class="table">
                        <thead class="thead-light">
                            <tr>
                            <th scope="col">CPMK</th>
                            <th scope="col">Kategori</th>
                            <th scope="col">Nilai</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                            <td>9A</td>
                            <td>Aturan penulisan ilmiah</td>
                            <td class="col-6">
                                <select id="aturanPenulisan_9A" class="form-control" name="aturanPenulisan_9A">
                                    <option value="" data-info="">Pilih opsi</option>
                                    <option value="1" <?php 
                                        if ($NilaiUjianTA != null){
                                            switch ($this->session->userdata('Id')){
                                                case $NilaiUjianTA['Dosen1Id']:
                                                    if ($NilaiUjianTA['Dsn1Ber1'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen2Id']:
                                                    if ($NilaiUjianTA['Dsn2Ber1'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen3Id']:
                                                    if ($NilaiUjianTA['Dsn3Ber1'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Tidak menyebutkan sumber pustaka">1</option>
                                    <option value="2" <?php 
                                        if ($NilaiUjianTA != null){
                                            switch ($this->session->userdata('Id')){
                                                case $NilaiUjianTA['Dosen1Id']:
                                                    if ($NilaiUjianTA['Dsn1Ber1'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen2Id']:
                                                    if ($NilaiUjianTA['Dsn2Ber1'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen3Id']:
                                                    if ($NilaiUjianTA['Dsn3Ber1'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Hanya sedikit menybutkan sumber informasi berupa tulisan dan gambar dari hasil pekerjaan orang lain yang dikutip.">2</option>
                                    <option value="3" <?php 
                                        if ($NilaiUjianTA != null){
                                            switch ($this->session->userdata('Id')){
                                                case $NilaiUjianTA['Dosen1Id']:
                                                    if ($NilaiUjianTA['Dsn1Ber1'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen2Id']:
                                                    if ($NilaiUjianTA['Dsn2Ber1'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen3Id']:
                                                    if ($NilaiUjianTA['Dsn3Ber1'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Setiap informasi berupa tulisan dan gambar dari hasil pekerjaan orang lain yang dikutip ke dalam slide disebutkan sumbernya.">3</option>
                                    <option value="4" <?php 
                                        if ($NilaiUjianTA != null){
                                            switch ($this->session->userdata('Id')){
                                                case $NilaiUjianTA['Dosen1Id']:
                                                    if ($NilaiUjianTA['Dsn1Ber1'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen2Id']:
                                                    if ($NilaiUjianTA['Dsn2Ber1'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen3Id']:
                                                    if ($NilaiUjianTA['Dsn3Ber1'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Setiap informasi berupa tulisan dan gambar dari hasil pekerjaan orang lain yang dikutip ke dalam slide disebutkan sumbernya secara tepat.">4</option>
                                </select>
                                <div id="aturanPenulisan_9AInfo" class="alert alert-info" style="display: none;"></div>
                            </td>
                            </tr>
                            <tr>
                            <td>9A, 9B</td>
                            <td>Tampilan data percobaan</td>
                            <td class="col-6">
                                <select id="tampilan_9A" class="form-control" name="tampilan_9A">
                                    <option value="" data-info="">Pilih opsi</option>
                                    <option value="1" <?php 
                                        if ($NilaiUjianTA != null){
                                            switch ($this->session->userdata('Id')){
                                                case $NilaiUjianTA['Dosen1Id']:
                                                    if ($NilaiUjianTA['Dsn1Ber2'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen2Id']:
                                                    if ($NilaiUjianTA['Dsn2Ber2'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen3Id']:
                                                    if ($NilaiUjianTA['Dsn3Ber2'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Tidak ada nilai 1">1</option>
                                    <option value="2" <?php 
                                        if ($NilaiUjianTA != null){
                                            switch ($this->session->userdata('Id')){
                                                case $NilaiUjianTA['Dosen1Id']:
                                                    if ($NilaiUjianTA['Dsn1Ber2'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen2Id']:
                                                    if ($NilaiUjianTA['Dsn2Ber2'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen3Id']:
                                                    if ($NilaiUjianTA['Dsn3Ber2'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Data percobaan ditampilkan tanpa dilengkapi informasi apapun.">2</option>
                                    <option value="3" <?php 
                                        if ($NilaiUjianTA != null){
                                            switch ($this->session->userdata('Id')){
                                                case $NilaiUjianTA['Dosen1Id']:
                                                    if ($NilaiUjianTA['Dsn1Ber2'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen2Id']:
                                                    if ($NilaiUjianTA['Dsn2Ber2'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen3Id']:
                                                    if ($NilaiUjianTA['Dsn3Ber2'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Data percobaan ditampilkan dan dilengkapi informasi atau keterangan lain yang menguatkan data.">3</option>
                                    <option value="4" <?php 
                                        if ($NilaiUjianTA != null){
                                            switch ($this->session->userdata('Id')){
                                                case $NilaiUjianTA['Dosen1Id']:
                                                    if ($NilaiUjianTA['Dsn1Ber2'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen2Id']:
                                                    if ($NilaiUjianTA['Dsn2Ber2'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen3Id']:
                                                    if ($NilaiUjianTA['Dsn3Ber2'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Data percobaan ditampilkan secara sangat baik dan dilengkapi informasi instrumen dan alat yang digunakan atau keterangan lain yang menguatkan data.">4</option>
                                </select>
                                <div id="tampilan_9AInfo" class="alert alert-info" style="display: none;"></div>
                            </td>
                            </tr>
                            <!-- <tr>
                            <td>9B</td>
                            <td>Tampilan data percobaan</td>
                            <td class="col-6">
                                <select id="tampilan_9B" class="form-control" name="tampilan_9B">
                                    <option value="" data-info="">Pilih opsi</option>
                                    <option value="1" data-info="Tidak ada nilai 1">1</option>
                                    <option value="2" data-info="Data percobaan ditampilkan tanpa dilengkapi informasi apapun.">2</option>
                                    <option value="3" data-info="Data percobaan ditampilkan dan dilengkapi informasi atau keterangan lain yang menguatkan data.">3</option>
                                    <option value="4" data-info="Data percobaan ditampilkan secara sangat baik dan dilengkapi informasi instrumen dan alat yang digunakan atau keterangan lain yang menguatkan data.">4</option>
                                </select>
                                <div id="tampilan_9BInfo" class="alert alert-info" style="display: none;"></div>
                            </td>
                            </tr> -->
                            <tr>
                            <td>9B</td>
                            <td>Organisasi presentasi</td>
                            <td class="col-6">
                                <select id="organisasi_9B" class="form-control" name="organisasi_9B">
                                    <option value="" data-info="">Pilih opsi</option>
                                    <option value="1" <?php 
                                        if ($NilaiUjianTA != null){
                                            switch ($this->session->userdata('Id')){
                                                case $NilaiUjianTA['Dosen1Id']:
                                                    if ($NilaiUjianTA['Dsn1Ber4'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen2Id']:
                                                    if ($NilaiUjianTA['Dsn2Ber4'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen3Id']:
                                                    if ($NilaiUjianTA['Dsn3Ber4'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Hasil pengukuran tidak terkait satu sama lain.">1</option>
                                    <option value="2" <?php 
                                        if ($NilaiUjianTA != null){
                                            switch ($this->session->userdata('Id')){
                                                case $NilaiUjianTA['Dosen1Id']:
                                                    if ($NilaiUjianTA['Dsn1Ber4'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen2Id']:
                                                    if ($NilaiUjianTA['Dsn2Ber4'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen3Id']:
                                                    if ($NilaiUjianTA['Dsn3Ber4'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Tampilan presentasi sangat kurang menunjukkan keterkaitan hasil-hasil pengukuran.">2</option>
                                    <option value="3" <?php 
                                        if ($NilaiUjianTA != null){
                                            switch ($this->session->userdata('Id')){
                                                case $NilaiUjianTA['Dosen1Id']:
                                                    if ($NilaiUjianTA['Dsn1Ber4'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen2Id']:
                                                    if ($NilaiUjianTA['Dsn2Ber4'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen3Id']:
                                                    if ($NilaiUjianTA['Dsn3Ber4'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Tampilan presentasi menunjukkan keterkaitan hasil-hasil pengukuran.">3</option>
                                    <option value="4" <?php 
                                        if ($NilaiUjianTA != null){
                                            switch ($this->session->userdata('Id')){
                                                case $NilaiUjianTA['Dosen1Id']:
                                                    if ($NilaiUjianTA['Dsn1Ber4'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen2Id']:
                                                    if ($NilaiUjianTA['Dsn2Ber4'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen3Id']:
                                                    if ($NilaiUjianTA['Dsn3Ber4'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Tampilan presentasi memudahkan hadirin melihat keterkaitan hasil-hasil pengukuran yang saling menguatkan.">4</option>
                                </select>
                                <div id="organisasi_9BInfo" class="alert alert-info" style="display: none;"></div>
                            </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <?php if($TA['JenisPenilaian'] == 1 && $TA['Dosen2Id'] == $this->session->userdata['Id']) :?>
                <div class="row mt-3 ml-2 mr-2">
                    <div class="col-3"></div>
                    <div class="col-6">
                        <h4 class="text-white bg-dark">Naskah Skripsi</h4>
                        <table class="table">
                            <thead class="thead-light">
                                <tr>
                                <th scope="col">CPMK</th>
                                <th scope="col">Kategori</th>
                                <th scope="col">Nilai</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                <td>8B</td>
                                <td>Mengolah data</td>
                                <td class="col-6">
                                    <select id="mengolahData_8B" class="form-control" name="mengolahData_8B">
                                        <option value="" data-info="">Pilih opsi</option>
                                        <option value="1" <?php 
                                        if ($NilaiUjianTA != null){
                                            switch ($this->session->userdata('Id')){
                                                case $NilaiUjianTA['Dosen1Id']:
                                                    if ($NilaiUjianTA['Dsn1Skripsi1'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen2Id']:
                                                    if ($NilaiUjianTA['Dsn2Skripsi1'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen3Id']:
                                                    if ($NilaiUjianTA['Dsn3Skripsi1'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                        ?> data-info="Tidak tepat dalam menampilkan data yang sudah diperoleh">1</option>
                                        <option value="2" <?php 
                                        if ($NilaiUjianTA != null){
                                            switch ($this->session->userdata('Id')){
                                                case $NilaiUjianTA['Dosen1Id']:
                                                    if ($NilaiUjianTA['Dsn1Skripsi1'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen2Id']:
                                                    if ($NilaiUjianTA['Dsn2Skripsi1'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen3Id']:
                                                    if ($NilaiUjianTA['Dsn3Skripsi1'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                        ?> data-info="Menampilkan data dalam tabel, tidak memperhatikan kecenderungan data.">2</option>
                                        <option value="3" <?php 
                                        if ($NilaiUjianTA != null){
                                            switch ($this->session->userdata('Id')){
                                                case $NilaiUjianTA['Dosen1Id']:
                                                    if ($NilaiUjianTA['Dsn1Skripsi1'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen2Id']:
                                                    if ($NilaiUjianTA['Dsn2Skripsi1'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen3Id']:
                                                    if ($NilaiUjianTA['Dsn3Skripsi1'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                        ?> data-info="Menampilkan data dalam grafik, mengelompokkan atau memisahkan data, dan menunjukkan kecenderungan.">3</option>
                                        <option value="4" <?php 
                                        if ($NilaiUjianTA != null){
                                            switch ($this->session->userdata('Id')){
                                                case $NilaiUjianTA['Dosen1Id']:
                                                    if ($NilaiUjianTA['Dsn1Skripsi1'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen2Id']:
                                                    if ($NilaiUjianTA['Dsn2Skripsi1'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen3Id']:
                                                    if ($NilaiUjianTA['Dsn3Skripsi1'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                        ?> data-info="Menampilkan data dalam grafik yang tepat, mengubah data menjadi informasi yang berguna bagi pencapaian tujuan.">4</option>
                                    </select>
                                    <div id="mengolahData_8BInfo" class="alert alert-info" style="display: none;"></div>
                                </td>
                                </tr>
                                <tr>
                                <td>8C</td>
                                <td>Membuat kesimpulan</td>
                                <td class="col-6">
                                    <select id="kesimpulanSkripsi_8C" class="form-control" name="kesimpulanSkripsi_8C">
                                        <option value="" data-info="">Pilih opsi</option>
                                        <option value="1" <?php 
                                        if ($NilaiUjianTA != null){
                                            switch ($this->session->userdata('Id')){
                                                case $NilaiUjianTA['Dosen1Id']:
                                                    if ($NilaiUjianTA['Dsn1Skripsi2'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen2Id']:
                                                    if ($NilaiUjianTA['Dsn2Skripsi2'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen3Id']:
                                                    if ($NilaiUjianTA['Dsn3Skripsi2'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                        ?> data-info="Tidak ada nilai 1">1</option>
                                        <option value="2" <?php 
                                        if ($NilaiUjianTA != null){
                                            switch ($this->session->userdata('Id')){
                                                case $NilaiUjianTA['Dosen1Id']:
                                                    if ($NilaiUjianTA['Dsn1Skripsi2'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen2Id']:
                                                    if ($NilaiUjianTA['Dsn2Skripsi2'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen3Id']:
                                                    if ($NilaiUjianTA['Dsn3Skripsi2'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                        ?> data-info="Salah menyimpulkan informasi yang terkandung dari data yang sudah diperoleh.">2</option>
                                        <option value="3" <?php 
                                        if ($NilaiUjianTA != null){
                                            switch ($this->session->userdata('Id')){
                                                case $NilaiUjianTA['Dosen1Id']:
                                                    if ($NilaiUjianTA['Dsn1Skripsi2'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen2Id']:
                                                    if ($NilaiUjianTA['Dsn2Skripsi2'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen3Id']:
                                                    if ($NilaiUjianTA['Dsn3Skripsi2'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                        ?> data-info="Membuat kesimpulan atau menulis kembali informasi yang diperoleh dari data dikumpulkan.">3</option>
                                        <option value="4" <?php 
                                        if ($NilaiUjianTA != null){
                                            switch ($this->session->userdata('Id')){
                                                case $NilaiUjianTA['Dosen1Id']:
                                                    if ($NilaiUjianTA['Dsn1Skripsi2'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen2Id']:
                                                    if ($NilaiUjianTA['Dsn2Skripsi2'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen3Id']:
                                                    if ($NilaiUjianTA['Dsn3Skripsi2'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                        ?> data-info="Membuat kesimpulan yang tepat dari informasi yang sudah diperoleh.">4</option>
                                    </select>
                                    <div id="kesimpulanSkripsi_8CInfo" class="alert alert-info" style="display: none;"></div>
                                </td>
                                </tr>
                                <tr>
                                <td>9A</td>
                                <td>Tataletak dan kalimat</td>
                                <td class="col-6">
                                    <select id="tataLetak_9A" class="form-control" name="tataLetak_9A">
                                        <option value="" data-info="">Pilih opsi</option>
                                        <option value="1" <?php 
                                        if ($NilaiUjianTA != null){
                                            switch ($this->session->userdata('Id')){
                                                case $NilaiUjianTA['Dosen1Id']:
                                                    if ($NilaiUjianTA['Dsn1Skripsi3'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen2Id']:
                                                    if ($NilaiUjianTA['Dsn2Skripsi3'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen3Id']:
                                                    if ($NilaiUjianTA['Dsn3Skripsi3'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                                default:
                                                    echo "";
                                                    break;
                                            }
                                        }
                                        ?> data-info="Tidak mengikuti aturan penulisan dan banyak kalimat tidak sempurna atau hampir separuh sumber pustaka yang dikutip tidak dituliskan.">1</option>
                                        <option value="2" <?php 
                                        if ($NilaiUjianTA != null){
                                            switch ($this->session->userdata('Id')){
                                                case $NilaiUjianTA['Dosen1Id']:
                                                    if ($NilaiUjianTA['Dsn1Skripsi3'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen2Id']:
                                                    if ($NilaiUjianTA['Dsn2Skripsi3'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen3Id']:
                                                    if ($NilaiUjianTA['Dsn3Skripsi3'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                        ?> data-info="Mudah ditemukan penyimpangan dari aturan penulisan naskah atau banyak kalimat tidak sempurna.">2</option>
                                        <option value="3" <?php 
                                        if ($NilaiUjianTA != null){
                                            switch ($this->session->userdata('Id')){
                                                case $NilaiUjianTA['Dosen1Id']:
                                                    if ($NilaiUjianTA['Dsn1Skripsi3'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen2Id']:
                                                    if ($NilaiUjianTA['Dsn2Skripsi3'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen3Id']:
                                                    if ($NilaiUjianTA['Dsn3Skripsi3'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                        ?> data-info="Naskah skripsi mematuhi aturan penulisan. Beberapa kalimat dalam naskah kurang sesuai tata bahasa.">3</option>
                                        <option value="4" <?php 
                                        if ($NilaiUjianTA != null){
                                            switch ($this->session->userdata('Id')){
                                                case $NilaiUjianTA['Dosen1Id']:
                                                    if ($NilaiUjianTA['Dsn1Skripsi3'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen2Id']:
                                                    if ($NilaiUjianTA['Dsn2Skripsi3'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen3Id']:
                                                    if ($NilaiUjianTA['Dsn3Skripsi3'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                        ?> data-info="Naskah skripsi mematuhi aturan penulisan dengan baik. Isi naskah ditulis dengan kalimat yang baik.">4</option>
                                    </select>
                                    <div id="tataLetak_9AInfo" class="alert alert-info" style="display: none;"></div>
                                </td>
                                </tr>
                                <tr>
                                <td>9A</td>
                                <td>Konsep penting dalam penelitian</td>
                                <td class="col-6">
                                    <select id="konsep_9A" class="form-control" name="konsep_9A">
                                        <option value="" data-info="">Pilih opsi</option>
                                        <option value="1" <?php 
                                        if ($NilaiUjianTA != null){
                                            switch ($this->session->userdata('Id')){
                                                case $NilaiUjianTA['Dosen1Id']:
                                                    if ($NilaiUjianTA['Dsn1Skripsi4'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen2Id']:
                                                    if ($NilaiUjianTA['Dsn2Skripsi4'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen3Id']:
                                                    if ($NilaiUjianTA['Dsn3Skripsi4'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                        ?> data-info="Ditemukan kesalahan konsep kimia yang sangat mengganggu.">1</option>
                                        <option value="2" <?php 
                                        if ($NilaiUjianTA != null){
                                            switch ($this->session->userdata('Id')){
                                                case $NilaiUjianTA['Dosen1Id']:
                                                    if ($NilaiUjianTA['Dsn1Skripsi4'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen2Id']:
                                                    if ($NilaiUjianTA['Dsn2Skripsi4'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen3Id']:
                                                    if ($NilaiUjianTA['Dsn3Skripsi4'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                        ?> data-info="Naskah mengandung kesalahan konsep kimia.">2</option>
                                        <option value="3" <?php 
                                        if ($NilaiUjianTA != null){
                                            switch ($this->session->userdata('Id')){
                                                case $NilaiUjianTA['Dosen1Id']:
                                                    if ($NilaiUjianTA['Dsn1Skripsi4'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen2Id']:
                                                    if ($NilaiUjianTA['Dsn2Skripsi4'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen3Id']:
                                                    if ($NilaiUjianTA['Dsn3Skripsi4'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                        ?> data-info="Naskah tidak mengandung kesalahan konsep kimia yang umum.">3</option>
                                        <option value="4" <?php 
                                        if ($NilaiUjianTA != null){
                                            switch ($this->session->userdata('Id')){
                                                case $NilaiUjianTA['Dosen1Id']:
                                                    if ($NilaiUjianTA['Dsn1Skripsi4'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen2Id']:
                                                    if ($NilaiUjianTA['Dsn2Skripsi4'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen3Id']:
                                                    if ($NilaiUjianTA['Dsn3Skripsi4'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                        ?> data-info="Naskah sama sekali tidak mengandung kesalahan konsep kimia yang umum maupun yang khusus.">4</option>
                                    </select>
                                    <div id="konsep_9AInfo" class="alert alert-info" style="display: none;"></div>
                                </td>
                                </tr>
                                <tr>
                                <td>9B</td>
                                <td>Pembahasan</td>
                                <td class="col-6">
                                    <select id="pembahasan_9B" class="form-control" name="pembahasan_9B">
                                        <option value="" data-info="">Pilih opsi</option>
                                        <option value="1" <?php 
                                        if ($NilaiUjianTA != null){
                                            switch ($this->session->userdata('Id')){
                                                case $NilaiUjianTA['Dosen1Id']:
                                                    if ($NilaiUjianTA['Dsn1Skripsi5'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen2Id']:
                                                    if ($NilaiUjianTA['Dsn2Skripsi5'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen3Id']:
                                                    if ($NilaiUjianTA['Dsn3Skripsi5'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                        ?> data-info="Pembahasan hasil pengukuran sangat kurang dan sebagian luaran hasil pengukuran tidak dibahas">1</option>
                                        <option value="2" <?php 
                                        if ($NilaiUjianTA != null){
                                            switch ($this->session->userdata('Id')){
                                                case $NilaiUjianTA['Dosen1Id']:
                                                    if ($NilaiUjianTA['Dsn1Skripsi5'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen2Id']:
                                                    if ($NilaiUjianTA['Dsn2Skripsi5'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen3Id']:
                                                    if ($NilaiUjianTA['Dsn3Skripsi5'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                        ?> data-info="Luaran hasil pengukuran diuraikan tanpa dasar yang kuat, atau sangat kurang menggali luaran hasil pengukuran">2</option>
                                        <option value="3" <?php 
                                        if ($NilaiUjianTA != null){
                                            switch ($this->session->userdata('Id')){
                                                case $NilaiUjianTA['Dosen1Id']:
                                                    if ($NilaiUjianTA['Dsn1Skripsi5'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen2Id']:
                                                    if ($NilaiUjianTA['Dsn2Skripsi5'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen3Id']:
                                                    if ($NilaiUjianTA['Dsn3Skripsi5'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                        ?> data-info="Luaran hasil pengukuran diuraikan sesuai cakupannya">3</option>
                                        <option value="4" <?php 
                                        if ($NilaiUjianTA != null){
                                            switch ($this->session->userdata('Id')){
                                                case $NilaiUjianTA['Dosen1Id']:
                                                    if ($NilaiUjianTA['Dsn1Skripsi5'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen2Id']:
                                                    if ($NilaiUjianTA['Dsn2Skripsi5'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $NilaiUjianTA['Dosen3Id']:
                                                    if ($NilaiUjianTA['Dsn3Skripsi5'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                        ?> data-info="Semua luaran hasil pengukuran diuraikan secara luas sesuai cakuppannya">4</option>
                                    </select>
                                    <div id="pembahasan_9BInfo" class="alert alert-info" style="display: none;"></div>
                                </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-3"></div>
                </div>
            <?php else : ?>
                <?php 
                    $admin = [0,5];
                    if ($TA['Dosen1Id'] == $this->session->userdata['Id'] || in_array($this->session->userdata['RoleId'], $admin)) :
                ?>
                    <div class="row mt-3 ml-2 mr-2">
                        <div class="col-3"></div>
                        <div class="col-6">
                            <h4 class="text-white bg-dark">Naskah Skripsi</h4>
                            <table class="table">
                                <thead class="thead-light">
                                    <tr>
                                    <th scope="col">CPMK</th>
                                    <th scope="col">Kategori</th>
                                    <th scope="col">Nilai</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                    <td>8B</td>
                                    <td>Mengolah data</td>
                                    <td class="col-6">
                                        <select id="mengolahData_8B" class="form-control" name="mengolahData_8B">
                                            <option value="" data-info="">Pilih opsi</option>
                                            <option value="1" <?php 
                                            if ($NilaiUjianTA != null){
                                                switch ($this->session->userdata('Id')){
                                                    case $NilaiUjianTA['Dosen1Id']:
                                                        if ($NilaiUjianTA['Dsn1Skripsi1'] == 1){
                                                            echo "selected";
                                                        }
                                                        break;
                                                    case $NilaiUjianTA['Dosen2Id']:
                                                        if ($NilaiUjianTA['Dsn2Skripsi1'] == 1){
                                                            echo "selected";
                                                        }
                                                        break;
                                                    case $NilaiUjianTA['Dosen3Id']:
                                                        if ($NilaiUjianTA['Dsn3Skripsi1'] == 1){
                                                            echo "selected";
                                                        }
                                                        break;
                                                }
                                            }
                                            ?> data-info="Tidak tepat dalam menampilkan data yang sudah diperoleh">1</option>
                                            <option value="2" <?php 
                                            if ($NilaiUjianTA != null){
                                                switch ($this->session->userdata('Id')){
                                                    case $NilaiUjianTA['Dosen1Id']:
                                                        if ($NilaiUjianTA['Dsn1Skripsi1'] == 2){
                                                            echo "selected";
                                                        }
                                                        break;
                                                    case $NilaiUjianTA['Dosen2Id']:
                                                        if ($NilaiUjianTA['Dsn2Skripsi1'] == 2){
                                                            echo "selected";
                                                        }
                                                        break;
                                                    case $NilaiUjianTA['Dosen3Id']:
                                                        if ($NilaiUjianTA['Dsn3Skripsi1'] == 2){
                                                            echo "selected";
                                                        }
                                                        break;
                                                }
                                            }
                                            ?> data-info="Menampilkan data dalam tabel, tidak memperhatikan kecenderungan data.">2</option>
                                            <option value="3" <?php 
                                            if ($NilaiUjianTA != null){
                                                switch ($this->session->userdata('Id')){
                                                    case $NilaiUjianTA['Dosen1Id']:
                                                        if ($NilaiUjianTA['Dsn1Skripsi1'] == 3){
                                                            echo "selected";
                                                        }
                                                        break;
                                                    case $NilaiUjianTA['Dosen2Id']:
                                                        if ($NilaiUjianTA['Dsn2Skripsi1'] == 3){
                                                            echo "selected";
                                                        }
                                                        break;
                                                    case $NilaiUjianTA['Dosen3Id']:
                                                        if ($NilaiUjianTA['Dsn3Skripsi1'] == 3){
                                                            echo "selected";
                                                        }
                                                        break;
                                                }
                                            }
                                            ?> data-info="Menampilkan data dalam grafik, mengelompokkan atau memisahkan data, dan menunjukkan kecenderungan.">3</option>
                                            <option value="4" <?php 
                                            if ($NilaiUjianTA != null){
                                                switch ($this->session->userdata('Id')){
                                                    case $NilaiUjianTA['Dosen1Id']:
                                                        if ($NilaiUjianTA['Dsn1Skripsi1'] == 4){
                                                            echo "selected";
                                                        }
                                                        break;
                                                    case $NilaiUjianTA['Dosen2Id']:
                                                        if ($NilaiUjianTA['Dsn2Skripsi1'] == 4){
                                                            echo "selected";
                                                        }
                                                        break;
                                                    case $NilaiUjianTA['Dosen3Id']:
                                                        if ($NilaiUjianTA['Dsn3Skripsi1'] == 4){
                                                            echo "selected";
                                                        }
                                                        break;
                                                }
                                            }
                                            ?> data-info="Menampilkan data dalam grafik yang tepat, mengubah data menjadi informasi yang berguna bagi pencapaian tujuan.">4</option>
                                        </select>
                                        <div id="mengolahData_8BInfo" class="alert alert-info" style="display: none;"></div>
                                    </td>
                                    </tr>
                                    <tr>
                                    <td>8C</td>
                                    <td>Membuat kesimpulan</td>
                                    <td class="col-6">
                                        <select id="kesimpulanSkripsi_8C" class="form-control" name="kesimpulanSkripsi_8C">
                                            <option value="" data-info="">Pilih opsi</option>
                                            <option value="1" <?php 
                                            if ($NilaiUjianTA != null){
                                                switch ($this->session->userdata('Id')){
                                                    case $NilaiUjianTA['Dosen1Id']:
                                                        if ($NilaiUjianTA['Dsn1Skripsi2'] == 1){
                                                            echo "selected";
                                                        }
                                                        break;
                                                    case $NilaiUjianTA['Dosen2Id']:
                                                        if ($NilaiUjianTA['Dsn2Skripsi2'] == 1){
                                                            echo "selected";
                                                        }
                                                        break;
                                                    case $NilaiUjianTA['Dosen3Id']:
                                                        if ($NilaiUjianTA['Dsn3Skripsi2'] == 1){
                                                            echo "selected";
                                                        }
                                                        break;
                                                }
                                            }
                                            ?> data-info="Tidak ada nilai 1">1</option>
                                            <option value="2" <?php 
                                            if ($NilaiUjianTA != null){
                                                switch ($this->session->userdata('Id')){
                                                    case $NilaiUjianTA['Dosen1Id']:
                                                        if ($NilaiUjianTA['Dsn1Skripsi2'] == 2){
                                                            echo "selected";
                                                        }
                                                        break;
                                                    case $NilaiUjianTA['Dosen2Id']:
                                                        if ($NilaiUjianTA['Dsn2Skripsi2'] == 2){
                                                            echo "selected";
                                                        }
                                                        break;
                                                    case $NilaiUjianTA['Dosen3Id']:
                                                        if ($NilaiUjianTA['Dsn3Skripsi2'] == 2){
                                                            echo "selected";
                                                        }
                                                        break;
                                                }
                                            }
                                            ?> data-info="Salah menyimpulkan informasi yang terkandung dari data yang sudah diperoleh.">2</option>
                                            <option value="3" <?php 
                                            if ($NilaiUjianTA != null){
                                                switch ($this->session->userdata('Id')){
                                                    case $NilaiUjianTA['Dosen1Id']:
                                                        if ($NilaiUjianTA['Dsn1Skripsi2'] == 3){
                                                            echo "selected";
                                                        }
                                                        break;
                                                    case $NilaiUjianTA['Dosen2Id']:
                                                        if ($NilaiUjianTA['Dsn2Skripsi2'] == 3){
                                                            echo "selected";
                                                        }
                                                        break;
                                                    case $NilaiUjianTA['Dosen3Id']:
                                                        if ($NilaiUjianTA['Dsn3Skripsi2'] == 3){
                                                            echo "selected";
                                                        }
                                                        break;
                                                }
                                            }
                                            ?> data-info="Membuat kesimpulan atau menulis kembali informasi yang diperoleh dari data dikumpulkan.">3</option>
                                            <option value="4" <?php 
                                            if ($NilaiUjianTA != null){
                                                switch ($this->session->userdata('Id')){
                                                    case $NilaiUjianTA['Dosen1Id']:
                                                        if ($NilaiUjianTA['Dsn1Skripsi2'] == 4){
                                                            echo "selected";
                                                        }
                                                        break;
                                                    case $NilaiUjianTA['Dosen2Id']:
                                                        if ($NilaiUjianTA['Dsn2Skripsi2'] == 4){
                                                            echo "selected";
                                                        }
                                                        break;
                                                    case $NilaiUjianTA['Dosen3Id']:
                                                        if ($NilaiUjianTA['Dsn3Skripsi2'] == 4){
                                                            echo "selected";
                                                        }
                                                        break;
                                                }
                                            }
                                            ?> data-info="Membuat kesimpulan yang tepat dari informasi yang sudah diperoleh.">4</option>
                                        </select>
                                        <div id="kesimpulanSkripsi_8CInfo" class="alert alert-info" style="display: none;"></div>
                                    </td>
                                    </tr>
                                    <tr>
                                    <td>9A</td>
                                    <td>Tataletak dan kalimat</td>
                                    <td class="col-6">
                                        <select id="tataLetak_9A" class="form-control" name="tataLetak_9A">
                                            <option value="" data-info="">Pilih opsi</option>
                                            <option value="1" <?php 
                                            if ($NilaiUjianTA != null){
                                                switch ($this->session->userdata('Id')){
                                                    case $NilaiUjianTA['Dosen1Id']:
                                                        if ($NilaiUjianTA['Dsn1Skripsi3'] == 1){
                                                            echo "selected";
                                                        }
                                                        break;
                                                    case $NilaiUjianTA['Dosen2Id']:
                                                        if ($NilaiUjianTA['Dsn2Skripsi3'] == 1){
                                                            echo "selected";
                                                        }
                                                        break;
                                                    case $NilaiUjianTA['Dosen3Id']:
                                                        if ($NilaiUjianTA['Dsn3Skripsi3'] == 1){
                                                            echo "selected";
                                                        }
                                                        break;
                                                }
                                            }
                                            ?> data-info="Tidak mengikuti aturan penulisan dan banyak kalimat tidak sempurna atau hampir separuh sumber pustaka yang dikutip tidak dituliskan.">1</option>
                                            <option value="2" <?php 
                                            if ($NilaiUjianTA != null){
                                                switch ($this->session->userdata('Id')){
                                                    case $NilaiUjianTA['Dosen1Id']:
                                                        if ($NilaiUjianTA['Dsn1Skripsi3'] == 2){
                                                            echo "selected";
                                                        }
                                                        break;
                                                    case $NilaiUjianTA['Dosen2Id']:
                                                        if ($NilaiUjianTA['Dsn2Skripsi3'] == 2){
                                                            echo "selected";
                                                        }
                                                        break;
                                                    case $NilaiUjianTA['Dosen3Id']:
                                                        if ($NilaiUjianTA['Dsn3Skripsi3'] == 2){
                                                            echo "selected";
                                                        }
                                                        break;
                                                }
                                            }
                                            ?> data-info="Mudah ditemukan penyimpangan dari aturan penulisan naskah atau banyak kalimat tidak sempurna.">2</option>
                                            <option value="3" <?php 
                                            if ($NilaiUjianTA != null){
                                                switch ($this->session->userdata('Id')){
                                                    case $NilaiUjianTA['Dosen1Id']:
                                                        if ($NilaiUjianTA['Dsn1Skripsi3'] == 3){
                                                            echo "selected";
                                                        }
                                                        break;
                                                    case $NilaiUjianTA['Dosen2Id']:
                                                        if ($NilaiUjianTA['Dsn2Skripsi3'] == 3){
                                                            echo "selected";
                                                        }
                                                        break;
                                                    case $NilaiUjianTA['Dosen3Id']:
                                                        if ($NilaiUjianTA['Dsn3Skripsi3'] == 3){
                                                            echo "selected";
                                                        }
                                                        break;
                                                }
                                            }
                                            ?> data-info="Naskah skripsi mematuhi aturan penulisan. Beberapa kalimat dalam naskah kurang sesuai tata bahasa.">3</option>
                                            <option value="4" <?php 
                                            if ($NilaiUjianTA != null){
                                                switch ($this->session->userdata('Id')){
                                                    case $NilaiUjianTA['Dosen1Id']:
                                                        if ($NilaiUjianTA['Dsn1Skripsi3'] == 4){
                                                            echo "selected";
                                                        }
                                                        break;
                                                    case $NilaiUjianTA['Dosen2Id']:
                                                        if ($NilaiUjianTA['Dsn2Skripsi3'] == 4){
                                                            echo "selected";
                                                        }
                                                        break;
                                                    case $NilaiUjianTA['Dosen3Id']:
                                                        if ($NilaiUjianTA['Dsn3Skripsi3'] == 4){
                                                            echo "selected";
                                                        }
                                                        break;
                                                }
                                            }
                                            ?> data-info="Naskah skripsi mematuhi aturan penulisan dengan baik. Isi naskah ditulis dengan kalimat yang baik.">4</option>
                                        </select>
                                        <div id="tataLetak_9AInfo" class="alert alert-info" style="display: none;"></div>
                                    </td>
                                    </tr>
                                    <tr>
                                    <td>9A</td>
                                    <td>Konsep penting dalam penelitian</td>
                                    <td class="col-6">
                                        <select id="konsep_9A" class="form-control" name="konsep_9A">
                                            <option value="" data-info="">Pilih opsi</option>
                                            <option value="1" <?php 
                                            if ($NilaiUjianTA != null){
                                                switch ($this->session->userdata('Id')){
                                                    case $NilaiUjianTA['Dosen1Id']:
                                                        if ($NilaiUjianTA['Dsn1Skripsi4'] == 1){
                                                            echo "selected";
                                                        }
                                                        break;
                                                    case $NilaiUjianTA['Dosen2Id']:
                                                        if ($NilaiUjianTA['Dsn2Skripsi4'] == 1){
                                                            echo "selected";
                                                        }
                                                        break;
                                                    case $NilaiUjianTA['Dosen3Id']:
                                                        if ($NilaiUjianTA['Dsn3Skripsi4'] == 1){
                                                            echo "selected";
                                                        }
                                                        break;
                                                }
                                            }
                                            ?> data-info="Ditemukan kesalahan konsep kimia yang sangat mengganggu.">1</option>
                                            <option value="2" <?php 
                                            if ($NilaiUjianTA != null){
                                                switch ($this->session->userdata('Id')){
                                                    case $NilaiUjianTA['Dosen1Id']:
                                                        if ($NilaiUjianTA['Dsn1Skripsi4'] == 2){
                                                            echo "selected";
                                                        }
                                                        break;
                                                    case $NilaiUjianTA['Dosen2Id']:
                                                        if ($NilaiUjianTA['Dsn2Skripsi4'] == 2){
                                                            echo "selected";
                                                        }
                                                        break;
                                                    case $NilaiUjianTA['Dosen3Id']:
                                                        if ($NilaiUjianTA['Dsn3Skripsi4'] == 2){
                                                            echo "selected";
                                                        }
                                                        break;
                                                }
                                            }
                                            ?> data-info="Naskah mengandung kesalahan konsep kimia.">2</option>
                                            <option value="3" <?php 
                                            if ($NilaiUjianTA != null){
                                                switch ($this->session->userdata('Id')){
                                                    case $NilaiUjianTA['Dosen1Id']:
                                                        if ($NilaiUjianTA['Dsn1Skripsi4'] == 3){
                                                            echo "selected";
                                                        }
                                                        break;
                                                    case $NilaiUjianTA['Dosen2Id']:
                                                        if ($NilaiUjianTA['Dsn2Skripsi4'] == 3){
                                                            echo "selected";
                                                        }
                                                        break;
                                                    case $NilaiUjianTA['Dosen3Id']:
                                                        if ($NilaiUjianTA['Dsn3Skripsi4'] == 3){
                                                            echo "selected";
                                                        }
                                                        break;
                                                }
                                            }
                                            ?> data-info="Naskah tidak mengandung kesalahan konsep kimia yang umum.">3</option>
                                            <option value="4" <?php 
                                            if ($NilaiUjianTA != null){
                                                switch ($this->session->userdata('Id')){
                                                    case $NilaiUjianTA['Dosen1Id']:
                                                        if ($NilaiUjianTA['Dsn1Skripsi4'] == 4){
                                                            echo "selected";
                                                        }
                                                        break;
                                                    case $NilaiUjianTA['Dosen2Id']:
                                                        if ($NilaiUjianTA['Dsn2Skripsi4'] == 4){
                                                            echo "selected";
                                                        }
                                                        break;
                                                    case $NilaiUjianTA['Dosen3Id']:
                                                        if ($NilaiUjianTA['Dsn3Skripsi4'] == 4){
                                                            echo "selected";
                                                        }
                                                        break;
                                                }
                                            }
                                            ?> data-info="Naskah sama sekali tidak mengandung kesalahan konsep kimia yang umum maupun yang khusus.">4</option>
                                        </select>
                                        <div id="konsep_9AInfo" class="alert alert-info" style="display: none;"></div>
                                    </td>
                                    </tr>
                                    <tr>
                                    <td>9B</td>
                                    <td>Pembahasan</td>
                                    <td class="col-6">
                                        <select id="pembahasan_9B" class="form-control" name="pembahasan_9B">
                                            <option value="" data-info="">Pilih opsi</option>
                                            <option value="1" <?php 
                                            if ($NilaiUjianTA != null){
                                                switch ($this->session->userdata('Id')){
                                                    case $NilaiUjianTA['Dosen1Id']:
                                                        if ($NilaiUjianTA['Dsn1Skripsi5'] == 1){
                                                            echo "selected";
                                                        }
                                                        break;
                                                    case $NilaiUjianTA['Dosen2Id']:
                                                        if ($NilaiUjianTA['Dsn2Skripsi5'] == 1){
                                                            echo "selected";
                                                        }
                                                        break;
                                                    case $NilaiUjianTA['Dosen3Id']:
                                                        if ($NilaiUjianTA['Dsn3Skripsi5'] == 1){
                                                            echo "selected";
                                                        }
                                                        break;
                                                }
                                            }
                                            ?> data-info="-">1</option>
                                            <option value="2" <?php 
                                            if ($NilaiUjianTA != null){
                                                switch ($this->session->userdata('Id')){
                                                    case $NilaiUjianTA['Dosen1Id']:
                                                        if ($NilaiUjianTA['Dsn1Skripsi5'] == 2){
                                                            echo "selected";
                                                        }
                                                        break;
                                                    case $NilaiUjianTA['Dosen2Id']:
                                                        if ($NilaiUjianTA['Dsn2Skripsi5'] == 2){
                                                            echo "selected";
                                                        }
                                                        break;
                                                    case $NilaiUjianTA['Dosen3Id']:
                                                        if ($NilaiUjianTA['Dsn3Skripsi5'] == 2){
                                                            echo "selected";
                                                        }
                                                        break;
                                                }
                                            }
                                            ?> data-info="-">2</option>
                                            <option value="3" <?php 
                                            if ($NilaiUjianTA != null){
                                                switch ($this->session->userdata('Id')){
                                                    case $NilaiUjianTA['Dosen1Id']:
                                                        if ($NilaiUjianTA['Dsn1Skripsi5'] == 3){
                                                            echo "selected";
                                                        }
                                                        break;
                                                    case $NilaiUjianTA['Dosen2Id']:
                                                        if ($NilaiUjianTA['Dsn2Skripsi5'] == 3){
                                                            echo "selected";
                                                        }
                                                        break;
                                                    case $NilaiUjianTA['Dosen3Id']:
                                                        if ($NilaiUjianTA['Dsn3Skripsi5'] == 3){
                                                            echo "selected";
                                                        }
                                                        break;
                                                }
                                            }
                                            ?> data-info="-">3</option>
                                            <option value="4" <?php 
                                            if ($NilaiUjianTA != null){
                                                switch ($this->session->userdata('Id')){
                                                    case $NilaiUjianTA['Dosen1Id']:
                                                        if ($NilaiUjianTA['Dsn1Skripsi5'] == 4){
                                                            echo "selected";
                                                        }
                                                        break;
                                                    case $NilaiUjianTA['Dosen2Id']:
                                                        if ($NilaiUjianTA['Dsn2Skripsi5'] == 4){
                                                            echo "selected";
                                                        }
                                                        break;
                                                    case $NilaiUjianTA['Dosen3Id']:
                                                        if ($NilaiUjianTA['Dsn3Skripsi5'] == 4){
                                                            echo "selected";
                                                        }
                                                        break;
                                                }
                                            }
                                            ?> data-info="-">4</option>
                                        </select>
                                        <div id="pembahasan_9BInfo" class="alert alert-info" style="display: none;"></div>
                                    </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-3"></div>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
            <?php 
                if (
                    !in_array(true, [
                        $this->session->userdata('Kadep'),
                        $this->session->userdata('Sekdep'),
                        $this->session->userdata('KaprodiS1'),
                        $this->session->userdata('KaprodiS2'),
                        $this->session->userdata('KaprodiS3'),
                    ]) || 
                    in_array($this->session->userdata('Id'), [
                        $DosenId['Dosen1Id'],
                        $DosenId['Dosen2Id'],
                        $DosenId['Dosen3Id'],
                    ])
                ) :
            ?>
            <div class="text-right mr-5 mb-3">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Submit</button>
            </div>
            <?php endif; ?>
        </form> 
    </div>
    <?php endif; ?>

    <!-- Card Kritik & Saran -->
    <?php if($TipeDosen != 0):?>
    <div class="card">
        <form method="post" action="<?= base_url('tugasAkhir/SubmitSaranSkripsi/'. $TaId); ?>" >
            <div class="card-content mr-2 ml-2 mb-2">
                <h4 class="text-white bg-dark">Pertanyaan & Saran</h4>
                <input type="hidden" name="saran" value="<?= set_value('saran') ?>">
                <?php if (isset($NilaiUjianTA) && $this->session->userdata['Id'] == $NilaiUjianTA['Dosen1Id']) : ?>
                    <div id="quillEditor" style="min-height: 100px;"><?php
                        if ($NilaiUjianTA['Dsn1Saran'] != null){
                            echo strip_tags($NilaiUjianTA['Dsn1Saran']);
                        }else{
                            echo "";
                        }
                         ?></div>
                <?php elseif (isset($NilaiUjianTA) && $this->session->userdata['Id'] == $NilaiUjianTA['Dosen2Id']) : ?>
                    <div id="quillEditor" style="min-height: 100px;"><?php
                        if ($NilaiUjianTA['Dsn2Saran'] != null){
                            echo strip_tags($NilaiUjianTA['Dsn2Saran']);
                        }else{
                            echo "";
                        }
                        ?></div>
                <?php elseif (isset($NilaiUjianTA) && $this->session->userdata['Id'] == $NilaiUjianTA['Dosen3Id']) : ?>
                    <div id="quillEditor" style="min-height: 100px;"><?php
                        if ($NilaiUjianTA['Dsn3Saran'] != null){
                            echo strip_tags($NilaiUjianTA['Dsn3Saran']);
                        }else{
                            echo "";
                        }
                        ?></div>
                <?php else : ?>
                    <div id="quillEditor" style="min-height: 100px;"><?= set_value('saran') ?></div>
                <?php endif; ?>
            </div>
            <div class="text-right mr-5 mb-3">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Submit</button>
            </div>
        </form> 
    </div>
    <?php endif; ?>

    <!-- Card Tampil Saran -->
    <?php 
        $mhs = [0,2,3,4];
        if (in_array($this->session->userdata('RoleId'), $mhs)):
    ?>
    <div class="card">
        <div class="row mt-3 ml-2 mr-2">
            <div class="col-xl-4 col-md-6 mb-4">
                <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                    Saran dan Kritik Dosen 1 :</div>
                                <?= $Dsn1Saran; ?>
                            </div>
                            <div class="col-auto">
                                <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6 mb-4">
                <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                    Saran dan Kritik Dosen 2 :</div>
                                <?= $Dsn2Saran; ?>
                            </div>
                            <div class="col-auto">
                                <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6 mb-4">
                <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                    Saran dan Kritik Dosen 3 :</div>
                                <?= $Dsn3Saran; ?>
                            </div>
                            <div class="col-auto">
                                <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php endif; ?>

    <!-- Card Nilai -->
    <div class="card">
        <?php if ($TipeDosen == 1) :?>
        <div class="row mt-3 ml-2 mr-2">
            <div class="col-12">
                <div class="row ml-2">
                    <h4 class="text-white bg-dark">Dosen 1</h4>
                </div>
                <div class="row">
                    <div class="col-xl-4 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Nilai Presentasi</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $totalNilaiPresentasiDsn1 ?></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Nilai Naskah</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $totalNilaiBerkasDsn1 ?></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Nilai Dosen</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $totalNilaiUjianDsn1TA ?></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php elseif ($TipeDosen == 2) :?>
        <div class="row mt-3 ml-2 mr-2">
            <div class="col-12">
                <div class="row ml-2">
                    <h4 class="text-white bg-dark">Dosen 2</h4>
                </div>
                <div class="row">
                    <div class="col-xl-4 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Nilai Presentasi</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $totalNilaiPresentasiDsn2 ?></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Nilai Naskah</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $totalNilaiBerkasDsn2 ?></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Nilai Dosen</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $totalNilaiUjianDsn2TA ?></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php elseif ($TipeDosen == 3) :?>
        <div class="row mt-3 ml-2 mr-2">
            <div class="col-12">
                <div class="row ml-2">
                    <h4 class="text-white bg-dark">Dosen 3</h4>
                </div>
                <div class="row">
                    <div class="col-xl-4 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Nilai Presentasi</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $totalNilaiPresentasiDsn3 ?></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Nilai Naskah</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $totalNilaiBerkasDsn3 ?></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Nilai Dosen</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $totalNilaiUjianDsn3TA ?></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
            elseif ($TipeDosen == 0 & in_array($this->session->userdata('RoleId'), [0,5])):
        ?>
        <div class="row mt-3 ml-2 mr-2">
            <h4 class="text-white bg-dark">Nilai Ujian Tugas Akhir</h4>
        </div>

        <div class="row mt-3 ml-2 mr-2">
            <div class="col-12">
                <div class="row ml-2">
                    <h4 class="text-white bg-dark">Dosen 1</h4>
                </div>
                <div class="row">
                    <div class="col-xl-4 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Nilai Presentasi</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $totalNilaiPresentasiDsn1 ?></div>
                                    </div>
                                    <div class="col-auto PresentasiSkripsi dosen1">
                                        <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Nilai Naskah</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $totalNilaiBerkasDsn1 ?></div>
                                    </div>
                                    <div class="col-auto BerkasSkripsi dosen1">
                                        <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Nilai Dosen 1</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $totalNilaiUjianDsn1TA ?></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mt-3 ml-2 mr-2">
            <div class="col-12">
                <div class="row ml-2">
                    <h4 class="text-white bg-dark">Dosen 2</h4>
                </div>
                <div class="row">
                    <div class="col-xl-4 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Nilai Presentasi</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $totalNilaiPresentasiDsn2 ?></div>
                                    </div>
                                    <div class="col-auto PresentasiSkripsi dosen2">
                                        <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Nilai Naskah</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $totalNilaiBerkasDsn2 ?></div>
                                    </div>
                                    <div class="col-auto BerkasSkripsi dosen2">
                                        <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Nilai Dosen 2</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $totalNilaiUjianDsn2TA ?></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mt-3 ml-2 mr-2">
            <div class="col-12">
                <div class="row ml-2">
                    <h4 class="text-white bg-dark">Dosen 3</h4>
                </div>
                <div class="row">
                    <div class="col-xl-4 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Nilai Presentasi</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $totalNilaiPresentasiDsn3 ?></div>
                                    </div>
                                    <div class="col-auto PresentasiSkripsi dosen3">
                                        <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Nilai Naskah</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $totalNilaiBerkasDsn3 ?></div>
                                    </div>
                                    <div class="col-auto BerkasSkripsi dosen3">
                                        <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Nilai Dosen 3</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $totalNilaiUjianDsn3TA ?></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php endif;?>

        <?php 
            $mhs = [2,3,4];
            if(in_array($this->session->userdata('RoleId'),$mhs)):
        ?>
        <hr>
        <div class="row mt-3 ml-2 mr-2">
            <div class="col-xl-2 col-md-6 mb-4">
            </div>
            <div class="col-xl-4 col-md-6 mb-4">
                <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                    Rata-rata Nilai Presentasi Seminar Skripsi</div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $averagePresentasi ?></div>
                            </div>
                            <div class="col-auto">
                                <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6 mb-4">
                <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                    Rata-Rata Nilai Berkas Presentasi Seminar Skripsi</div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $averageBerkas ?></div>
                            </div>
                            <div class="col-auto">
                                <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-2 col-md-6 mb-4">
            </div>
        </div>

        <?php endif; ?>

        <div class="row mt-3 ml-2 mr-2">
            <div class="col-xl-4 col-md-6 mb-4">
            </div>
            <div class="col-xl-4 col-md-6 mb-4">
                <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                    Nilai Akhir Ujian Skripsi</div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $NilaiAkhirUjianTA ?></div>
                            </div>
                            <div class="col-auto">
                                <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6 mb-4">
            </div>
        </div>

        <hr>
        <div class="row mt-3 ml-2 mr-2">
            <h4 class="text-white bg-dark">Nilai Naskah Skripsi</h4>
        </div>
        <div class="row mt-3 ml-2 mr-2">
            <div class="col-xl-4 col-md-6 mb-4">
                <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                    Nilai Naskah Dosen 1</div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $NilaiSkripsiDosen1 ?></div>
                            </div>
                            <div class="col-auto NaskahSkripsi dosen1">
                                <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6 mb-4">
                <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                    Nilai Naskah Dosen 2</div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $NilaiSkripsiDosen2 ?></div>
                            </div>
                            <div class="col-auto NaskahSkripsi dosen2">
                                <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6 mb-4">
                <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                    Total Nilai Naskah Skripsi</div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $TotalNilaiSkripsi ?></div>
                            </div>
                            <div class="col-auto">
                                <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <hr>
        <div class="row mt-3 ml-2 mr-2">
            <h4 class="text-white bg-dark">Nilai Total Tugas Akhir</h4>
        </div>
        <div class="row mt-3 ml-2 mr-2">
            <div class="col-xl-3 col-md-6 mb-4">
                <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                    Nilai Sempro</div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $Nilai["NilaiAkhirSempro"] ?></div>
                            </div>
                            <div class="col-auto">
                                <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-md-6 mb-4">
                <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                    Nilai Semju</div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $Nilai["NilaiAkhirSemju"] ?></div>
                            </div>
                            <div class="col-auto">
                                <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-md-6 mb-4">
                <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                    Nilai Ujian</div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $Nilai["NilaiAkhirSkripsi"] ?></div>
                            </div>
                            <div class="col-auto">
                                <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-md-6 mb-4">
                <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                    Nilai Naskah</div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $Nilai["NilaiNaskah"] ?></div>
                            </div>
                            <div class="col-auto">
                                <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-3 ml-2 mr-2">
            <div class="col-xl-1 col-md-6 mb-4">
                
            </div>
            <div class="col-xl-4 col-md-6 mb-4">
                <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                    Nilai Akhir Skripsi</div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $Nilai["TotalNilaiAkhir"] ?></div>
                            </div>
                            <div class="col-auto">
                                <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-2 col-md-6 mb-4">
                
            </div>
            <div class="col-xl-4 col-md-6 mb-4">
                <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                    Konversi Nilai</div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $Nilai["HurufMutu"] ?></div>
                            </div>
                            <div class="col-auto">
                                <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-1 col-md-6 mb-4">
                
            </div>
        </div>
    </div>

    
    <!-- Nilai Presentasi -->
    <div class="modal fade" id="PresentasiSkripsi" role="dialog" aria-labelledby="exampleModalLabel"
        >
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Detail Nilai Presentasi Skripsi</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                <div class="card">
                    <div class="card-body">
                    <div class="form-group row">
                        <label for="inputPresent1" class="col-sm-4 col-form-label">Langkah-langkah pengerjaan berdasarkan teori dan pengalaman</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputPresent1" name="Present1" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPresent2" class="col-sm-4 col-form-label">Membaca luaran hasil pengukuran</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputPresent2" name="Present2" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPresent3" class="col-sm-4 col-form-label">Membahas hasil pengukuran</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputPresent3" name="Present3" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPresent4" class="col-sm-4 col-form-label">Pengambilan kesimpulan</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputPresent4" name="Present4" disabled>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Tutup</button>
                </div>
            </div>
            </div>
        </div>
    </div>
    </div>

     <!-- Nilai Berkas -->
     <div class="modal fade" id="BerkasSkripsi" role="dialog" aria-labelledby="exampleModalLabel"
        >
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Detail Nilai Naskah Skripsi</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                <div class="card">
                    <div class="card-body">
                    <div class="form-group row">
                        <label for="inputBerkas1" class="col-sm-4 col-form-label">Aturan penulisan ilmiah</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputBerkas1" name="Berkas1" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputBerkas2" class="col-sm-4 col-form-label">Tampilan data percobaan</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputBerkas2" name="Berkas2" disabled>
                        </div>
                    </div>
                    <!-- <div class="form-group row">
                        <label for="inputBerkas3" class="col-sm-4 col-form-label">Menyusun langkah-langkah pengerjaan</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputBerkas3" name="Berkas3" disabled>
                        </div>
                    </div> -->
                    <div class="form-group row">
                        <label for="inputBerkas4" class="col-sm-4 col-form-label">Organisasi presentasi</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputBerkas4" name="Berkas4" disabled>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Tutup</button>
                </div>
            </div>
            </div>
        </div>
    </div>
    </div>

     <!-- Nilai Naskah -->
     <div class="modal fade" id="NaskahSkripsi" role="dialog" aria-labelledby="exampleModalLabel"
        >
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Detail Nilai Naskah Skripsi</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                <div class="card">
                    <div class="card-body">
                    <div class="form-group row">
                        <label for="inputNaskah1" class="col-sm-4 col-form-label">Mengolah data</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputNaskah1" name="Naskah1" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputNaskah2" class="col-sm-4 col-form-label">Membuat kesimpulan</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputNaskah2" name="Naskah2" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputNaskah3" class="col-sm-4 col-form-label">Tataletak dan kalimat</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputNaskah3" name="Naskah3" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputNaskah4" class="col-sm-4 col-form-label">Konsep penting dalam penelitian</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputNaskah4" name="Naskah4" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputNaskah5" class="col-sm-4 col-form-label">Pembahasan</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputNaskah5" name="Naskah5" disabled>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Tutup</button>
                </div>
            </div>
            </div>
        </div>
    </div>
    </div>

</div>