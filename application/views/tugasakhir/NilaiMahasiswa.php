<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Filter -->
    <div class="card">
        <div class="row mt-3 ml-2 mb-0">
            <div class="col">
            <form method="post" action="<?= base_url('TugasAkhir/NilaiTugasAkhirMahasiswa'); ?>" enctype="multipart/form-data">
                <div class="form-row">
                    <div class="form-group col-md-2">
                        <div class="form-row">
                            <div class="form-group col">
                            <select id="semester" name="semester" class="form-control">
                                <option value="">Pilih Semester</option>
                                <?php foreach ($Semester as $semester) : ?>
                                    <option value="<?= $semester['Id'] ?>"><?= $semester['Semester']?> - <?= $semester['TahunAkademik']?></option>
                                <?php endforeach; ?>
                            </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-2">
                        <div class="form-row">
                            <div class="form-group col">
                            <select id="limit" name="limit" class="form-control">
                                <option value="">Tampil Data</option>
                                <option value="10">10</option>
                                <option value="25">25</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-2">
                        <div class="form-row">
                            <div class="form-group col">
                                <input type="text" class="form-control" id="search" name="search" placeholder="Nama / NIM">
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-2">
                        <button type="submit" class="btn btn-secondary">Search</button>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>

    <!-- <?php 
    $mhs = [2,3,4];
    if(!in_array($User['RoleId'], $mhs)):?>
        <div class="card">
            <div class="row mt-3 ml-2 mb-0">
                <div class="col">
                <form method="post" action="<?= base_url('TugasAkhir/SeminarProposal'); ?>" enctype="multipart/form-data">
                    <div class="form-row">
                        <div class="form-group col-md-2">
                            <div class="form-row">
                                <div class="form-group col">
                                <select id="jenjang" name="jenjang" class="form-control">
                                    <option value="" selected>Pilih Jenjang</option>
                                    <option value="2">S1</option>
                                    <option value="3">S2</option>
                                    <option value="4">S3</option>
                                </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-2">
                            <div class="form-row">
                                <div class="form-group col">
                                <select id="limit" name="limit" class="form-control">
                                    <option value="">Tampil Data</option>
                                    <option value="10">10</option>
                                    <option value="25">25</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-2">
                            <div class="form-row">
                                <div class="form-group col">
                                    <input type="text" class="form-control" id="search" name="search" placeholder="Nama / NIM">
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-2">
                            <button type="submit" class="btn btn-secondary">Search</button>
                        </div>
                    </div>
                </form>
                </div>
            </div>
        </div>
    <?php endif; ?> -->

    <!-- Table TA -->
    <div class="card">
        <div class="row mt-3 ml-2 mr-2">
            <div class="col-4">
                <h4 class="text-white bg-dark">Seminar Proposal Tugas Akhir</h4>
            </div>
            <div class="col-5">
            </div>
            <div class="col-3">
            </div>
        </div>
        <div class="row g-0 pb-3 pl-2 pr-2">
        <div class="card-body">
            <table class="table table-striped">
            <thead class="thead-dark">
                <tr>
                <th scope="col" width="50px">No</th>
                <th scope="col" width="500px">Nama Mahasiswa</th>
                <th scope="col" width="500px">NIM</th>
                <th scope="col" width="500px">Nilai Seminar Proposal</th>
                <th scope="col" width="500px">Nilai Seminar Kemajuan</th>
                <th scope="col" width="500px">Nilai Skripsi</th>
                <th scope="col" width="500px">Nilai Naskah</th>
                <th scope="col" width="500px">Nilai Total</th>
                <th scope="col" width="500px">Nilai Konversi</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $i = 1;
                    foreach($Nilai as $Nilai):
                ?>
                <tr>
                <th scope="row" ><?=$i?></th>
                <td class="TaId" hidden><?=$Nilai['TaId']?></td>
                <td>
                    <p><?= $Nilai['Name'] ?></p>
                </td>
                <td>
                    <p><?= $Nilai['Username'] ?></p>
                </td>
                <td>
                    <p><?= $Nilai['NilaiAkhirSempro'] ?></p>
                </td>
                <td>
                    <p><?= $Nilai['NilaiAkhirSemju'] ?></p>
                </td>
                <td>
                    <p><?= $Nilai['NilaiAkhirSkripsi'] ?></p>
                </td>
                <td>
                    <p><?= $Nilai['NilaiNaskah'] ?></p>
                </td>
                <td>
                    <p><?= $Nilai['TotalNilaiAkhir'] ?></p>
                </td>
                <td>
                    <p><?= $Nilai['HurufMutu'] ?></p>
                </td>
                </tr>
                <?php 
                    $i++;
                    endforeach; 
                ?>
            </tbody>
            </table>
        </div>
    </div>
</div>