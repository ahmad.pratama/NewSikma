<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Filter -->

    <?php 
    $mhs = [2,3,4];
    if(!in_array($User['RoleId'], $mhs)):?>
        <div class="card">
            <div class="row mt-3 ml-2 mb-0">
                <div class="col">
                <form method="post" action="<?= base_url('TugasAkhir/SeminarSkripsi'); ?>" enctype="multipart/form-data">
                    <div class="form-row">
                        <div class="form-group col-md-2">
                            <div class="form-row">
                                <div class="form-group col">
                                <select id="jenjang" name="jenjang" class="form-control">
                                    <option value="" selected>Pilih Jenjang</option>
                                    <option value="2">S1</option>
                                    <option value="3">S2</option>
                                    <option value="4">S3</option>
                                </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-2">
                            <div class="form-row">
                                <div class="form-group col">
                                <select id="semester" name="semester" class="form-control">
                                    <option value="">Pilih Semester</option>
                                    <?php foreach ($Semester as $semester) : ?>
                                        <option value="<?= $semester['Id'] ?>"><?= $semester['Semester']?> - <?= $semester['TahunAkademik']?></option>
                                    <?php endforeach; ?>
                                </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-2">
                            <div class="form-row">
                                <div class="form-group col">
                                <select id="limit" name="limit" class="form-control">
                                    <option value="">Tampil Data</option>
                                    <option value="10">10</option>
                                    <option value="25">25</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-2">
                            <div class="form-row">
                                <div class="form-group col">
                                    <input type="text" class="form-control" id="search" name="search" placeholder="Nama / NIM">
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-2">
                            <button type="submit" class="btn btn-secondary">Search</button>
                        </div>
                    </div>
                </form>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <!-- Table TA -->
    <div class="card">
        <div class="row mt-3 ml-2 mr-2">
            <div class="col-4">
                <h4 class="text-white bg-dark">Seminar Skripsi Tugas Akhir</h4>
            </div>
            <div class="col-5">
            </div>
            <div class="col-3">
            </div>
        </div>
        <div class="row g-0 pb-3 pl-2 pr-2">
        <div class="card-body">
            <table class="table table-striped">
            <thead class="thead-dark">
                <tr>
                <th scope="col" width="50px">No</th>
                <th scope="col" width="500px">Mahasiswa</th>
                <th scope="col" width="500px">Judul</th>
                <th scope="col" width="500px">Dosen</th>
                <th scope="col" width="500px">Ruangan</th>
                <th scope="col" width="500px">Tanggal Ujian</th>
                <th scope="col" width="500px">Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $i = 1;
                    foreach($SeminarSkripsi as $Ta):
                ?>
                <tr>
                <th scope="row" ><?=$i?></th>
                <td class="TaId" hidden><?=$Ta['Id']?></td>
                <td>
                    <div class="row">
                        <p><?= $Ta['Name']?></p>
                    </div>
                    <div class="row">
                        <p><?= $Ta['Username']?></p>
                    </div>
                    <div class="row">
                        <p>Bidang Minat : <?= $Ta['Bidang'] ?></p>
                    </div>
                </td>
                <td>
                    <div class="row">
                        <p><?= $Ta['JudulIdn']?></p>
                    </div>
                </td>
                <td>
                    <div class="row">
                        <p><?= (isset($Ta['Dosen1Name']) && $Ta['Dosen1Name'] != null) ? $Ta['Dosen1Name'] : "-"; ?></p>
                    </div>
                    <div class="row">
                        <p><?= (isset($Ta['Dosen2Name']) && $Ta['Dosen2Name'] != null) ? $Ta['Dosen2Name'] : "-"; ?></p>
                    </div>
                    <div class="row">
                        <p><?= (isset($Ta['Dosen3Name']) && $Ta['Dosen3Name'] != null) ? $Ta['Dosen3Name'] : "-"; ?></p>
                    </div>
                </td>
                <td>
                    <div class="row">
                        <p><?php echo (isset($Ta['RuangNama'])) ? $Ta['RuangNama']: "" ?></p>
                    </div>
                    <div class="row">
                        <p><?php echo (isset($Ta['LinkZoom'])) ? $Ta['LinkZoom']: "" ?></p>
                    </div>
                </td>
                <td>
                    <div class="row">
                        <p>Tanggal : <?php echo (isset($Ta['TanggalUjian'])) ? $Ta['TanggalUjian']: '' ?></p>
                    </div>
                    <div class="row">
                        <p>Pukul : <?php echo (isset($Ta['JamMulai'])) ? $Ta['JamMulai']: '' ?> - <?php echo (isset($Ta['JamSelesai'])) ? $Ta['JamSelesai']: '' ?></p>
                    </div>
                </td>

                <td>
                    <div class="btn-group">
                        <button type="button" class="btn btn-warning btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Aksi
                            <i class="fa-solid fa-sliders"></i>
                        </button>
                        <div class="dropdown-menu">
                            <!-- Cek Sudah Daftar Belum -->
                            <?php if (!isset($Ta['TanggalUjian'])): ?>
                                <!-- Cek Role Id & tampilkan Daftar-->
                                <?php 
                                $mhsRole = [0,2,3,4,5];
                                if(in_array($User['RoleId'], $mhsRole)):?>
                                    <div class="row mx-auto p-1">
                                        <button type="button" class="btn btn-success daftarSkripsi">Daftar</button>
                                    </div>
                                <?php endif; ?>
                            <?php else: ?>
                                <?php 
                                $mhsRole = [0,2,3,4,5];
                                if(in_array($User['RoleId'], $mhsRole)):?>
                                <!-- Tampilkan Upload Makalah & Edit Skripsi-->
                                    <div class="row mx-auto p-1">
                                        <button type="button" class="btn btn-success uploadSkripsi">Unggah</button>
                                    </div>
                                    <div class="row mx-auto p-1">
                                        <button type="button" class="btn btn-warning editSkripsi">Edit</button>
                                    </div>
                                <?php endif; ?>

                                <?php 
                                    $dsnRole = [0,1,5,6];
                                    if(in_array($User['RoleId'], $dsnRole)) :
                                ?>
                                    <div class="row mx-auto p-1">
                                        <a type="button" class="btn btn-warning" href="<?= base_url('TugasAkhir/NilaiSkripsiTa/'). $Ta['Id'] ?>">Nilai</a>
                                    </div>
                                <?php 
                                    elseif (strtotime($Ta['TanggalUjian']) < time()) :
                                ?>
                                    <div class="row mx-auto p-1">
                                        <a type="button" class="btn btn-warning" href="<?= base_url('TugasAkhir/NilaiSkripsiTa/'). $Ta['Id'] ?>">Nilai</a>
                                    </div>
                                <?php
                                    endif; 
                                ?>
                                <div class="row mx-auto p-1">
                                    <button type="button" class="btn btn-info detailSkripsi">Detil</button>
                                </div>

                                <?php if ($Ta['MakalahSkripsi'] != null && $Ta['MakalahSkripsi'] != ""):?>
                                    <div class="row mx-auto p-1">
                                        <a href="<?= base_url('/assets/uploads/skripsi_ta/') . $Ta['MakalahSkripsi']; ?>" target="_blank">
                                            <button type="button" class="btn btn-success">
                                                Makalah<i class="fa-solid fa-fw fa-file-export"></i>
                                            </button>
                                        </a>
                                    </div>
                                <?php endif; ?>

                                <?php 
                                $admin = [0,5];
                                if (in_array($User['RoleId'], $admin)):
                                ?>
                                    <div class="row mx-auto p-1">
                                        <a href="#" class="btn btn-success downloadBASkripsi" type="button">Unduh</a>
                                    </div>
                                <?php endif; ?>
                            <?php endif; ?>
                                
                        </div>
                    </div>
                </td>
                </tr>
                <?php 
                    $i++;
                    endforeach; 
                ?>
            </tbody>
            </table>
        </div>
    </div>
</div>


<!-- Daftar Skripsi Modal-->
<div class="modal fade" id="daftarSkripsi"  role="dialog" aria-labelledby="exampleModalLabel"
    >
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Daftar Seminar Ujian Akhir (Skripsi)</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <?php $minimalDate = date("Y-m-d"); ?>
                <form method="post" action="<?= base_url('tugasAkhir/DaftarSkripsi'); ?>" >
                <div class="form-group row">
                    <input type="text" class="form-control" id="inputSemester" name="TaId" hidden>
                    <label for="inputPenilaian" class="col-sm-4 col-form-label">Jenis Penilaian</label>
                    <div class="col-sm-8">
                        <p>
                            <input type="radio" name="jenisPenilaian" id="inputPenilaian" value="1"> (2 Pembimbing, 1 Penguji)
                        </p>
                        <p>
                            <input type="radio" name="jenisPenilaian" id="inputPenilaian" value="2"> (1 Pembimbing, 2 Penguji)
                        </p>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputDosenPembimbing1" class="col-sm-4 col-form-label" disabled>Dosen 1</label>
                    <div class="col-sm-8">
                    <select id="Dosen1" name="dosen1">
                        <option value="">Select an option</option>
                        <?php foreach ($Dosen as $dsn): ?>
                            <option value="<?= $dsn['Id'] ?>"><?= $dsn['Name'] ?></option>
                        <?php endforeach; ?>
                    </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputDosenPembimbing2" class="col-sm-4 col-form-label">Dosen 2</label>
                    <div class="col-sm-8">
                    <select id="Dosen2" name="dosen2" required>
                        <option value="">Select an option</option>
                        <?php foreach ($Dosen as $dsn): ?>
                            <option value="<?= $dsn['Id'] ?>"><?= $dsn['Name'] ?></option>
                        <?php endforeach; ?>
                    </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputDosenPembimbing3" class="col-sm-4 col-form-label">Dosen 3</label>
                    <div class="col-sm-8">
                    <select id="Dosen3" name="dosen3" required>
                        <option value="">Select an option</option>
                        <?php foreach ($Dosen as $dsn): ?>
                            <option value="<?= $dsn['Id'] ?>"><?= $dsn['Name'] ?></option>
                        <?php endforeach; ?>
                    </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputTanggalUjianSkripsiDaftar" class="col-sm-4 col-form-label">Tanggal Ujian</label>
                    <div class="col-sm-8">
                    <input type="date" class="form-control" id="inputTanggalUjianSkripsiDaftar" name="tanggalUjian" min="<?=  $minimalDate ?>" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputtimeSelect" class="col-sm-4 col-form-label">Jam Mulai</label>
                    <div class="col-sm-8">
                    <select id="JamMulai" name="jamMulai" style="width: 100%;" required></select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputtimeSelect" class="col-sm-4 col-form-label">Jam Selesai</label>
                    <div class="col-sm-8">
                    <select id="JamSelesai" name="jamSelesai" style="width: 100%;" required></select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputRuangan" class="col-sm-4 col-form-label">Ruangan</label>
                    <div class="col-sm-8">
                        <select id="Ruangan" name="ruangId">
                            <option value="">Select an option</option>
                            <?php foreach ($Ruangan as $room): ?>
                                <option value="<?= $room['Id'] ?>"><?= $room['Nama'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputLinkZoom" class="col-sm-4 col-form-label">Link Zoom</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputLinkZoom" name="linkZoom">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Tambah</button>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- Detail Skripsi Modal-->
<div class="modal fade" id="detailSkripsi"  role="dialog" aria-labelledby="exampleModalLabel"
    >
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Detail Seminar Ujian Akhir (Skripsi)</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="card-body">
                        <div class="form-group row">
                            <label for="inputNama" class="col-sm-4 col-form-label">Nama Mahasiswa</label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputNama" name="taId" hidden>
                            <input type="text" class="form-control" id="inputNama" name="nama" disabled>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputUsername" class="col-sm-4 col-form-label">NIM</label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputUsername" name="username" disabled>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputBidang" class="col-sm-4 col-form-label">Bidang Minat</label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputBidang" name="bidangMinat" disabled>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="card">
                    <div class="card-body">
                        <div class="form-group row">
                            <label for="inputDosen1" class="col-sm-4 col-form-label">Dosen 1</label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputDosen1" name="dosen1" disabled>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputDosen2" class="col-sm-4 col-form-label">Dosen 2</label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputDosen2" name="dosen2" disabled>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputDosen3" class="col-sm-4 col-form-label">Dosen 3</label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputDosen3" name="dosen3" disabled>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="card">
                    <div class="card-body">
                        <div class="form-group row">
                            <label for="inputTanggal" class="col-sm-4 col-form-label">Hari, Tanggal Ujian</label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputTanggal" name="tanggalUjian" disabled>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputWaktu" class="col-sm-4 col-form-label">Waktu</label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputWaktu" name="waktu" disabled>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputRuangan" class="col-sm-4 col-form-label">Ruangan</label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputRuangan" name="ruangan" disabled>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputLinkZoom" class="col-sm-4 col-form-label">Link Zoom</label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputLinkZoom" name="linkZoom" disabled>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputJudulIdn" class="col-sm-4 col-form-label">Judul ID</label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputJudulIdn" name="judulIdn" disabled>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputJudulEng" class="col-sm-4 col-form-label">Judul Eng</label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputJudulEng" name="judulEng" disabled>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="card">
                    <div class="card-body">
                        <div class="row justify-content-center">
                                <button type="button" class="btn btn-primary btn-sm mx-1">Undangan</button>
                                <button type="button" class="btn btn-primary btn-sm mx-1">Berita Acara</button>
                                <button type="button" class="btn btn-primary btn-sm mx-1">Nilai Presentasi</button>
                                <button type="button" class="btn btn-primary btn-sm mx-1">Nilai Naskah</button>
                                <button type="button" class="btn btn-primary btn-sm mx-1">Pertanyaan & Saran</button>
                                <button type="button" class="btn btn-primary btn-sm mx-1">Presensi</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>

<!-- Upload Malakah Skripsi Modal-->
<div class="modal fade" id="uploadMakalahSkripsi"  role="dialog" aria-labelledby="exampleModalLabel"
    >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Unggah Berkas Seminar Ujian Akhir (Skripsi)</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
            <form method="post" action="<?= base_url('tugasAkhir/uploadMakalahSkripsi'); ?>" enctype="multipart/form-data">
            <div class="form-group">
                <div class="form-group row">
                    <div class="col-sm-3">Berkas Skripsi</div>
                    <div class="col-sm-9">
                        <div class="row">
                            <input type="text" class="form-control" id="inputUserId" name="TaId" hidden>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="customFile" name="makalahSkripsi" accept=".pdf">
                                <label class="custom-file-label" for="customFile">Choose file</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Unggah</button>
            </div>
            </form>
            </div>
        </div>
    </div>
</div>

<!-- Edit Skripsi Modal -->
<div class="modal fade" id="editSkripsi"  role="dialog" aria-labelledby="exampleModalLabel"
    >
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Seminar Skripsi</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <?php $minimalDate = date("Y-m-d"); ?>
                <form method="post" action="<?= base_url('tugasAkhir/EditSkripsi'); ?>" >
                <div class="form-group row">
                    <input type="text" class="form-control" id="inputSemester" name="TaId" hidden>
                    <label for="inputPenilaian" class="col-sm-4 col-form-label">Jenis Penilaian</label>
                    <div class="col-sm-8">
                        <p>
                            <input type="radio" name="jenisPenilaianSkripsi" id="inputPenilaian" value="1"> (2 Pembimbing, 1 Penguji)
                        </p>
                        <p>
                            <input type="radio" name="jenisPenilaianSkripsi" id="inputPenilaian" value="2"> (1 Pembimbing, 2 Penguji)
                        </p>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputDosenPembimbing1" class="col-sm-4 col-form-label" disabled>Dosen 1</label>
                    <div class="col-sm-8">
                    <select id="Dosen1" name="dosen1" required>
                        <option value="">Select an option</option>
                        <?php foreach ($Dosen as $dsn): ?>
                            <option value="<?= $dsn['Id'] ?>"><?= $dsn['Name'] ?></option>
                        <?php endforeach; ?>
                    </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputDosenPembimbing2" class="col-sm-4 col-form-label">Dosen 2</label>
                    <div class="col-sm-8">
                    <select id="Dosen2" name="dosen2" required>
                        <option value="">Select an option</option>
                        <?php foreach ($Dosen as $dsn): ?>
                            <option value="<?= $dsn['Id'] ?>"><?= $dsn['Name'] ?></option>
                        <?php endforeach; ?>
                    </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputDosenPembimbing3" class="col-sm-4 col-form-label">Dosen 3</label>
                    <div class="col-sm-8">
                    <select id="Dosen3" name="dosen3" required>
                        <option value="">Select an option</option>
                        <?php foreach ($Dosen as $dsn): ?>
                            <option value="<?= $dsn['Id'] ?>"><?= $dsn['Name'] ?></option>
                        <?php endforeach; ?>
                    </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputTanggalUjianSkripsiEdit" class="col-sm-4 col-form-label">Tanggal Ujian</label>
                    <div class="col-sm-8">
                    <input type="date" class="form-control" id="inputTanggalUjianSkripsiEdit" name="tanggalUjian" min="<?=  $minimalDate ?>" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputtimeSelect" class="col-sm-4 col-form-label">Jam Mulai</label>
                    <div class="col-sm-8">
                    <select id="JamMulai2" name="jamMulai" style="width: 100%;" required></select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputtimeSelect" class="col-sm-4 col-form-label">Jam Selesai</label>
                    <div class="col-sm-8">
                    <select id="JamSelesai2" name="jamSelesai" style="width: 100%;" required></select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputRuangan" class="col-sm-4 col-form-label">Ruangan</label>
                    <div class="col-sm-8">
                        <select id="Ruangan" name="ruangId">
                            <option value="">Select an option</option>
                            <?php foreach ($Ruangan as $room): ?>
                                <option value="<?= $room['Id'] ?>"><?= $room['Nama'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputLinkZoom" class="col-sm-4 col-form-label">Link Zoom</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputLinkZoom" name="linkZoom">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Edit</button>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- End of Main Content -->