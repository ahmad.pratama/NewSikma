<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Card Bootstrap -->
    <div class="card mb-3" style="max-width: 1500px;">
        <div class="col-md-6 mt-3 ml-2">
            <h3>Tugas Akhir</h3>
        </div>
        <div class="row g-0 pt-4 pb-3 pl-2 pr-2">

        <!-- Pendaftaran TA -->
        <div class="col-sm-3 pb-2">
            <a href="<?= base_url('TugasAkhir/PendaftaranTA'); ?>">
                <div class="card bg-gradient-light" style="height: 18rem;">
                    <div class="card-body mx-auto">
                        <i class="fa-solid fa-fw fa-file-circle-plus fa-8x"></i>
                        <p></p>
                        <h4 class="card-text text-gray-800">Pendaftaran TA</h4>
                    </div> 
                    <div class="card-footer bg-gray-400">
                    <button class="btn btn-primary btn-user btn-block">
                        <b>
                            Detail
                            <i class="fa-regular fa-fw fa-circle-right fa-1x"></i>
                        </b>
                    </button>
                    </div>
                </div>
            </a>
        </div>

        <!-- Seminar Proposal -->
        <?php 
        $mhs = [2,3,4];
        if (in_array($this->session->userdata('RoleId'), $mhs)):
            if($Sempro):
        ?>
            <div class="col-sm-3 pb-2">
                <a href="<?= base_url('TugasAkhir/SeminarProposal'); ?>">
                    <div class="card bg-gradient-light" style="height: 18rem;">
                        <div class="card-body mx-auto">
                            <i class="fa-solid fa-fw fa-person-chalkboard fa-8x"></i>
                            <p></p>
                            <h4 class="card-text text-gray-800">Seminar Proposal</h4>
                        </div> 
                        <div class="card-footer bg-gray-400">
                        <button class="btn btn-primary btn-user btn-block">
                            <b>
                                Detail
                                <i class="fa-regular fa-fw fa-circle-right fa-1x"></i>
                            </b>
                        </button>
                        </div>
                    </div>
                </a>
            </div>
        <?php endif; ?>
        <?php else : ?>
            <div class="col-sm-3 pb-2">
                <a href="<?= base_url('TugasAkhir/SeminarProposal'); ?>">
                    <div class="card bg-gradient-light" style="height: 18rem;">
                        <div class="card-body mx-auto">
                            <i class="fa-solid fa-fw fa-person-chalkboard fa-8x"></i>
                            <p></p>
                            <h4 class="card-text text-gray-800">Seminar Proposal</h4>
                        </div> 
                        <div class="card-footer bg-gray-400">
                        <button class="btn btn-primary btn-user btn-block">
                            <b>
                                Detail
                                <i class="fa-regular fa-fw fa-circle-right fa-1x"></i>
                            </b>
                        </button>
                        </div>
                    </div>
                </a>
            </div>
        <?php endif; ?>

        <!-- Seminar Kemajuan -->
        <?php 
        $mhs = [2,3,4];
        if (in_array($this->session->userdata('RoleId'), $mhs)):
            if($Semju):
        ?>
        <div class="col-sm-3 pb-2">
            <a href="<?= base_url('TugasAkhir/SeminarKemajuan'); ?>">
                <div class="card bg-gradient-light" style="height: 18rem;">
                    <div class="card-body mx-auto">
                        <i class="fa-solid fa-fw fa-person-chalkboard fa-8x"></i>
                        <p></p>
                        <h4 class="card-text text-gray-800">Seminar Kemajuan</h4>
                    </div> 
                    <div class="card-footer bg-gray-400">
                    <button class="btn btn-primary btn-user btn-block">
                        <b>
                            Detail
                            <i class="fa-regular fa-fw fa-circle-right fa-1x"></i>
                        </b>
                    </button>
                    </div>
                </div>
            </a>
        </div>
        <?php endif; ?>
        <?php else : ?>
        <div class="col-sm-3 pb-2">
            <a href="<?= base_url('TugasAkhir/SeminarKemajuan'); ?>">
                <div class="card bg-gradient-light" style="height: 18rem;">
                    <div class="card-body mx-auto">
                        <i class="fa-solid fa-fw fa-person-chalkboard fa-8x"></i>
                        <p></p>
                        <h4 class="card-text text-gray-800">Seminar Kemajuan</h4>
                    </div> 
                    <div class="card-footer bg-gray-400">
                    <button class="btn btn-primary btn-user btn-block">
                        <b>
                            Detail
                            <i class="fa-regular fa-fw fa-circle-right fa-1x"></i>
                        </b>
                    </button>
                    </div>
                </div>
            </a>
        </div>
        <?php endif; ?>
        
        <!-- Tugas Akhir (Skripsi) -->
        <?php 
        $mhs = [2,3,4];
        if (in_array($this->session->userdata('RoleId'), $mhs)):
            if($Skripsi):
        ?>
        <div class="col-sm-3 pb-2">
            <a href="<?= base_url('TugasAkhir/SeminarSkripsi') ?>">
                <div class="card bg-gradient-light" style="height: 18rem;">
                    <div class="card-body mx-auto">
                        <i class="fa-solid fa-fw fa-file-signature fa-8x"></i>
                        <p></p>
                        <h4 class="card-text text-gray-800">Ujian Tugas Akhir (Skripsi)</h4>
                    </div> 
                    <div class="card-footer bg-gray-400">
                    <button class="btn btn-primary btn-user btn-block">
                        <b>
                            Detail
                            <i class="fa-regular fa-fw fa-circle-right fa-1x"></i>
                        </b>
                    </button>
                    </div>
                </div>
            </a>
        </div>
        <?php endif; ?>
        <?php else : ?>
        <div class="col-sm-3 pb-2">
            <a href="<?= base_url('TugasAkhir/SeminarSkripsi') ?>">
                <div class="card bg-gradient-light" style="height: 18rem;">
                    <div class="card-body mx-auto">
                        <i class="fa-solid fa-fw fa-file-signature fa-8x"></i>
                        <p></p>
                        <h4 class="card-text text-gray-800">Ujian Tugas Akhir (Skripsi)</h4>
                    </div> 
                    <div class="card-footer bg-gray-400">
                    <button class="btn btn-primary btn-user btn-block">
                        <b>
                            Detail
                            <i class="fa-regular fa-fw fa-circle-right fa-1x"></i>
                        </b>
                    </button>
                    </div>
                </div>
            </a>
        </div>
        <?php endif; ?>

        <!-- Nilai Tugas Akhir Mahasiswa -->
        <?php 
        $admin = [0,5];
        if (in_array($this->session->userdata('RoleId'), $admin)):
        ?>
        <div class="col-sm-3 pb-2">
            <a href="<?= base_url('TugasAkhir/NilaiTugasAkhirMahasiswa') ?>">
                <div class="card bg-gradient-light" style="height: 18rem;">
                    <div class="card-body mx-auto">
                        <i class="fa-solid fa-fw fa-table-list fa-8x"></i>
                        </i>
                        <p></p>
                        <h4 class="card-text text-gray-800">List Nilai Mahasiswa</h4>
                    </div> 
                    <div class="card-footer bg-gray-400">
                    <button class="btn btn-primary btn-user btn-block">
                        <b>
                            Detail
                            <i class="fa-regular fa-fw fa-circle-right fa-1x"></i>
                        </b>
                    </button>
                    </div>
                </div>
            </a>
        </div>
        <?php endif; ?>

        </div>
    </div>

    </div>
    <!-- /.container-fluid -->

</div>
<!-- End of Main Content -->