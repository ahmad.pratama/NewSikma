<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Filter -->
    <?php 
    $mhs = [2,3,4];
    if(!in_array($User['RoleId'], $mhs)):?>
        <div class="card">
            <div class="row mt-3 ml-2 mb-0">
                <div class="col">
                <form method="post" action="<?= base_url('TugasAkhir/PendaftaranTA'); ?>" enctype="multipart/form-data">
                    <div class="form-row">
                        <div class="form-group col-md-2">
                            <div class="form-row">
                                <div class="form-group col">
                                <select id="jenjang" name="jenjang" class="form-control">
                                    <option value="" selected>Pilih Jenjang</option>
                                    <option value="2">S1</option>
                                    <option value="3">S2</option>
                                    <option value="4">S3</option>
                                </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-2">
                            <div class="form-row">
                                <div class="form-group col">
                                <select id="semester" name="semester" class="form-control">
                                    <option value="">Pilih Semester</option>
                                    <?php foreach ($Semester as $semester) : ?>
                                        <option value="<?= $semester['Id'] ?>"><?= $semester['Semester']?> - <?= $semester['TahunAkademik']?></option>
                                    <?php endforeach; ?>
                                </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-2">
                            <div class="form-row">
                                <div class="form-group col">
                                <select id="limit" name="limit" class="form-control">
                                    <option value="">Tampil Data</option>
                                    <option value="10">10</option>
                                    <option value="25">25</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-2">
                            <div class="form-row">
                                <div class="form-group col">
                                    <input type="text" class="form-control" id="search" name="search" placeholder="Nama / NIM">
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-2">
                            <button type="submit" class="btn btn-secondary">Search</button>
                        </div>
                    </div>
                </form>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <!-- Table TA -->
    <div class="card">
        <div class="row mt-3 ml-2 mr-2">
            <div class="col-4">
                <h4 class="text-white bg-dark">Riwayat Tugas Akhir</h4>
            </div>
            <div class="col-3">
            </div>
            <div class="col-2">
                <?php 
                $admin = [0,5];
                if(in_array($User['RoleId'], $admin)):
                ?>
                    <a class="btn btn-success btn-sm" href="<?=base_url('TugasAkhir/DownloadExcelMahasiswaTA')?>">
                        <i class="fa-solid fa-fw fa-cloud-arrow-down"></i>
                        Cetak
                    </a>
                <?php endif;?>
            </div>
            <div class="col-3">
                <?php 
                $mhs = [2,3,4];
                if(in_array($User['RoleId'], $mhs)):?>
                <div class="btn-group">
                    <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#DaftarTA">
                        <i class="fa-solid fa-fw fa-user-plus"></i>
                        Daftar Tugas Akhir
                    </button>
                </div>
                <?php endif; ?>
            </div>
        </div>
        <div class="row g-0 pb-3 pl-2 pr-2">
        <div class="card-body">
            <table class="table table-striped">
            <thead class="thead-dark">
                <tr>
                <th scope="col" width="50px">No</th>
                <th scope="col" width="500px">Mahasiswa</th>
                <th scope="col" width="500px">Judul</th>
                <th scope="col" width="500px">Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $i = 1;
                    foreach($PendaftaranTA as $Ta): 
                ?>
                <tr>
                <th scope="row" ><?=$i?></th>
                <td class="TaId" hidden><?=$Ta['Id']?></td>
                <td class="Dosen1Id" hidden><?=$Ta['Dosen1Id']?></td>
                <td class="Dosen2Id" hidden><?=$Ta['Dosen2Id']?></td>
                <td>
                    <div class="row">
                        <p><?= $Ta['Name']?></p>
                    </div>
                    <div class="row">
                        <p><?= $Ta['Username']?></p>
                    </div>
                </td>
                <td style="color:green;">
                    <b><?= $Ta['JudulIdn']?></b>
                </td>
                <td>
                    <div class="btn-group">
                        <button type="button" class="btn btn-warning btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Aksi
                            <i class="fa-solid fa-sliders"></i>
                        </button>
                        <div class="dropdown-menu">
                            <?php 
                            $acc = [0,2,3,4,5];
                            if(in_array($User['RoleId'], $acc)):?>
                                <div class="row mx-auto p-1">
                                    <button type="button" class="btn btn-warning editTa">Edit</button>
                                </div>
                            <?php endif; ?>
                            <div class="row mx-auto p-1">
                                <button type="button" class="btn btn-info viewTa">Detil</button>
                            </div>
                            <?php 
                            $acc = [0,2,3,4,5];
                            if(in_array($User['RoleId'], $acc)):?>
                                <div class="row mx-auto p-1">
                                    <button type="button" class="btn btn-secondary berkasTa">Berkas</button>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </td>
                </tr>
                <?php 
                    $i++;
                    endforeach; 
                ?>
            </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Tambah Ta Modal-->
<div class="modal fade" id="DaftarTA" role="dialog" aria-labelledby="exampleModalLabel"
    >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Daftar Tugas Akhir</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="<?= base_url('TugasAkhir/DaftarTA'); ?>" onsubmit="return validateForm()">
                <div class="form-group row">
                    <label for="inputName" class="col-sm-4 col-form-label">Nama</label>
                    <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputName" name="mhsId" value="<?=$User['Id']?>" hidden>
                    <!-- <input type="hidden" name="name" value="<?=$User['Name']?>"> -->
                    <input type="text" class="form-control" id="inputName" placeholder="Name" name="username" value="<?=$User['Name']?>" disabled>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputUsername" class="col-sm-4 col-form-label">NIM</label>
                    <div class="col-sm-8">
                    <!-- <input type="hidden" name="username" value="<?=$User['Username']?>"> -->
                    <input type="text" class="form-control" id="inputUsername" placeholder="NIM" name="username" value="<?=$User['Username']?>" disabled>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputAngkatan" class="col-sm-4 col-form-label">Tahun Masuk</label>
                    <div class="col-sm-8">
                    <!-- <input type="hidden" name="angkatan" value="<?=$User['Angkatan']?>"> -->
                    <input type="text" class="form-control" id="inputAngkatan" placeholder="Angkatan" name="angkatan" value="<?=$User['Angkatan']?>" disabled>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputProgramStudi" class="col-sm-4 col-form-label">Program Studi</label>
                    <div class="col-sm-8">
                    <!-- <input type="hidden" name="programStudi" value="Jurusan Kimia Fakultas MIPA Universitas Brawijaya"> -->
                    <textarea class="form-control" id="inputProgramStudi" name="programStudi" rows="2" name="programStudi" disabled>Jurusan Kimia Fakultas MIPA Universitas Brawijaya</textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputAlamatMalang" class="col-sm-4 col-form-label">Alamat Malang</label>
                    <div class="col-sm-8">
                    <!-- <input type="hidden" name="alamatMalang" value="<?= $User['AlamatMalang']?>"> -->
                    <textarea class="form-control" id="inputAlamatMalang" name="alamatMalang" rows="2" disabled><?= $User['AlamatMalang']?></textarea>
                    <?php if($User['AlamatMalang'] == null || $User['AlamatMalang'] == ""):?>
                        <span class="text-danger">*Silahkan Lengkapi Data Profil Anda</span>
                    <?php endif; ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputTelpon" class="col-sm-4 col-form-label">Telpon</label>
                    <div class="col-sm-8">
                    <!-- <input type="hidden" name="telpon" value="<?= $User['Telpon']?>"> -->
                    <input type="text" class="form-control" id="inputTelpon" name="telpon" value="<?=$User['Telpon']?>" disabled>
                    <?php if($User['Telpon'] == null || $User['Telpon'] == ""):?>
                        <span class="text-danger">*Silahkan Lengkapi Data Profil Anda</span>
                    <?php endif; ?>    
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputSemester" class="col-sm-4 col-form-label">Semester</label>
                    <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputSemester" name="semester" placeholder="Semester" value="<?=$Akademik['Semester']?>" disabled>
                    <input type="text" class="form-control" id="inputSemester" name="semester" placeholder="Semester" value="<?=$Akademik['Semester']?>" hidden>
                    <input type="hidden" class="form-control" id="inputSemester" name="semesterId" value="<?=$Akademik['Id']?>">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputTahunAkademik" class="col-sm-4 col-form-label">Tahun Akademik</label>
                    <div class="col-sm-8">
                    <input type="hidden" name="tahunAkademik" value="<?= $Akademik['TahunAkademik']?>">
                    <input type="text" class="form-control" id="inputTahunAkademik" name="tahunAkademik" placeholder="Tahun Akademik" value="<?=$Akademik['TahunAkademik']?>" disabled>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputJumlahSKS" class="col-sm-4 col-form-label">Jumlah SKS</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputJumlahSKS" name="jumlahSKS" placeholder="Jumlah SKS" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputIPK" class="col-sm-4 col-form-label">IPK</label>
                    <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputIPK" name="IPK" placeholder="IPK" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputBidang" class="col-sm-4 col-form-label">Bidang Minat</label>
                    <div class="col-sm-8">
                    <select id="inputBidang" name="bidangMinat" class="form-control" required>
                        <option value="0">Belum Ada</option>
                        <?php foreach ($Lab as $lab): ?>
                            <option value="<?= $lab['Id'] ?>"><?= $lab['Nama']?></option>
                        <?php endforeach; ?>
                    </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputDosenPembimbing1" class="col-sm-4 col-form-label">Dosen Pembimbing 1</label>
                    <div class="col-sm-8">
                    <select id="Dosen1Daftar" name="Dosen1" required>
                        <option value="">Select an option</option>
                        <?php foreach ($Dosen as $dsn): ?>
                            <option value="<?= $dsn['Id'] ?>"><?= $dsn['Name'] ?></option>
                        <?php endforeach; ?>
                    </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputDosenPembimbing2" class="col-sm-4 col-form-label">Dosen Pembimbing 2</label>
                    <div class="col-sm-8">
                    <select id="Dosen2Daftar" name="Dosen2">
                        <option value="">Select an option</option>
                        <?php foreach ($Dosen as $dsn): ?>
                            <option value="<?= $dsn['Id'] ?>"><?= $dsn['Name'] ?></option>
                        <?php endforeach; ?>
                    </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputJudul" class="col-sm-4 col-form-label">Judul Tugas Akhir (Indonesia)</label>
                    <div class="col-sm-8">
                    <textarea class="form-control" id="inputJudul" name="judulIdn" placeholder="Judul Tugas Akhir" rows="2" required></textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputJudul" class="col-sm-4 col-form-label">Judul Tugas Akhir (Inggris)</label>
                    <div class="col-sm-8">
                    <textarea class="form-control" id="inputJudul" name="judulEng" placeholder="Judul Tugas Akhir" rows="2" required></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit"
                    <?php foreach($User as $key => $value):?>
                        <?php if($value==null || $value==""):?>
                            <?php if($key != 'TanggalLahir' && $key !=  'ActiveDate' && $key != 'Email' && $key != 'JenisKelamin' && $key != 'Seleksi' && $key != 'Tertanggal' && $key != 'BidikMisi'):?>
                                disabled
                            <?php endif; ?>
                        <?php endif; ?>
                    <?php endforeach; ?>
                >Daftar</button>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- View Ta Model -->
<div class="modal fade" id="viewTa" role="dialog" aria-labelledby="exampleModalLabel"
    >
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Detail Tugas Akhir</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
            <div class="card">
                <div class="card-body">
                <div class="form-group row">
                    <label for="inputSemester" class="col-sm-4 col-form-label">Semester</label>
                    <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputSemester" name="TaId" hidden>
                    <input type="text" class="form-control" id="inputSemester" name="semester" disabled>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputTahunAkademik" class="col-sm-4 col-form-label">Jumlah SKS</label>
                    <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputTahunAkademik" name="tahunAkademik" disabled>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputJumlahSks" class="col-sm-4 col-form-label">Jumlah SKS</label>
                    <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputJumlahSks" name="jumlahSKS" disabled>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputIPK" class="col-sm-4 col-form-label">IPK</label>
                    <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputIPK" name="IPK" disabled>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputBidang" class="col-sm-4 col-form-label">Bidang Minat</label>
                    <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputBidang" name="bidangMinat" disabled>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputDosen1" class="col-sm-4 col-form-label">Dosen Pembimbing 1</label>
                    <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputDosen1" name="Dosen1" disabled>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputDosen2" class="col-sm-4 col-form-label">Dosen Pembimbing 2</label>
                    <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputDosen2" name="Dosen2" disabled>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputJudulIdn" class="col-sm-4 col-form-label">Judul Tugas Akhir</label>
                    <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputJudulIdn" name="judulIdn" disabled>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputJudulEng" class="col-sm-4 col-form-label">Judul Tugas Akhir</label>
                    <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputJudulEng" name="judulEng" disabled>
                    </div>
                </div>
                </div>
            </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Tutup</button>
                <a href="#" class="btn btn-success" type="button" onclick="downloadFormulirTA()">Unduh</a>
                <input type="text" class="form-control" id="inputId" name="TaId" hidden>
            </div>
        </div>
    </div>
</div>

<!-- Edit Ta Model -->
<div class="modal fade" id="editTa" role="dialog" aria-labelledby="exampleModalLabel"
    >
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Ta</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
            <form method="post" action="<?=base_url('TugasAkhir/editTA')?>" >
            <div class="card">
                <div class="card-body">
                    <div class="form-group row">
                        <label for="inputSemester" class="col-sm-4 col-form-label">Semester</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputSemester" name="TaId" hidden>
                        <input type="text" class="form-control" id="inputSemester" name="semester" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputTahunAkademik" class="col-sm-4 col-form-label">Tahun Akademik</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputTahunAkademik" name="tahunAkademik" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputJumlahSks" class="col-sm-4 col-form-label">Jumlah SKS</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputJumlahSks" name="jumlahSKS" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputIPK" class="col-sm-4 col-form-label">IPK</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputIPK" name="IPK" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputBidang" class="col-sm-4 col-form-label">Bidang Minat</label>
                        <div class="col-sm-8">
                        <select id="inputBidang" name="bidangMinat" class="form-control" required>
                            <option value="1">Biokimia</option>
                            <option value="2">Kimia Analitik</option>
                            <option value="3">Kimia Anorganik</option>
                            <option value="4">Kimia Fisik</option>
                            <option value="5">Kimia Organik</option>
                        </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputDosenPembimbing1" class="col-sm-4 col-form-label">Dosen Pembimbing 1</label>
                        <div class="col-sm-8">
                        <select id="Dosen1" name="Dosen1" required>
                            <option value="">Select an option</option>
                            <?php foreach ($Dosen as $dsn): ?>
                                <option value="<?= $dsn['Id'] ?>"><?= $dsn['Name'] ?></option>
                            <?php endforeach; ?>
                        </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputDosenPembimbing2" class="col-sm-4 col-form-label">Dosen Pembimbing 2</label>
                        <div class="col-sm-8">
                        <select id="Dosen2" name="Dosen2">
                            <option value="">Select an option</option>
                            <?php foreach ($Dosen as $dsn): ?>
                                <option value="<?= $dsn['Id'] ?>"><?= $dsn['Name'] ?></option>
                            <?php endforeach; ?>
                        </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputJudulIdn" class="col-sm-4 col-form-label">Judul Tugas Akhir</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputJudulIdn" name="judulIdn" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputJudulEng" class="col-sm-4 col-form-label">Judul Tugas Akhir</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputJudulEng" name="judulEng" required>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Edit</button>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- Berkas Ta Model -->
<div class="modal fade" id="berkasTa" role="dialog" aria-labelledby="exampleModalLabel"
    >
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Berkas Pendaftaran Ta</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
            <table class="table table-striped">
                <thead class="thead-dark">
                    <tr>
                    <th scope="col">#</th>
                    <th scope="col">Berkas</th>
                    <th scope="col">Unggah</th>
                    <th scope="col">File</th>
                    <th scope="col">Tanggal</th>
                    <?php 
                    $admin = [0,5];
                    if(in_array($this->session->userdata('RoleId'), $admin)):
                    ?>
                    <th scope="col">Validasi</th>
                    <?php endif; ?>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                    <th scope="row">1</th>
                    <td hidden><input id="inputId" type="hidden" class="form-control" name="TaId"></td>
                    <td>Formulir pendaftaran tugas akhir yang sudah disetujui/tanda tangan kepala lab.
(ttd elektronik ; foto ditempel ) *** 
                    </td>
                    <form method="post" action="<?= base_url('TugasAkhir/UploadFormulirTA'); ?>" enctype="multipart/form-data">
                        <td hidden><input type="text" class="form-control" id="TaId" name="TaId"></td>
                        <td>
                            <label for="input-formulirTA">
                                <i class="fa-solid fa-fw fa-file-arrow-up fa-2x"></i> 
                            </label>
                            <input type="file" id="input-formulirTA" name="formulirTA" style="display: none;" accept=".pdf">
                            <button class="btn btn-primary" id="submit-formulirTA" type="submit" style="display: none;" >Unggah</button>
                        </td>
                    </form>
                    <td>
                        <form action="">
                            <a id="linkFormulirTA" href="#" target="_blank">
                                <i id="linkIconFormulirTA" class="fa-solid fa-fw fa-file-export fa-2x"></i>
                            </a>
                        </form>
                    </td>
                    <td>
                        <label id="UploadFormulir"></label>
                    </td>
                    <?php 
                    $admin = [0,5];
                    if(in_array($this->session->userdata('RoleId'), $admin)):
                    ?>
                    <form>
                        <td>
                            <select id="validFormulir" name="validasiFormulir">
                                <option value="1">Iya</option>
                                <option value="0">Tidak</option>
                            </select>
                        </td>
                    </form>
                    <?php endif; ?>
                    </tr>
                    <tr>
                    <th scope="row">2</th>
                    <td>KHS (Kartu Hasil Studi) Semester 1 s/d Akhir (tanpa ttd PA) ***</td>
                    <form method="post" action="<?= base_url('TugasAkhir/UploadKHSReguler'); ?>" enctype="multipart/form-data">
                        <td hidden><input type="text" class="form-control" id="TaId" name="TaId"></td>
                        <td>
                            <label for="input-KHSReguler">
                                <i class="fa-solid fa-fw fa-file-arrow-up fa-2x"></i> 
                            </label>
                            <input type="file" id="input-KHSReguler" name="KHSReguler" style="display: none;" accept=".pdf">
                            <button class="btn btn-primary" id="submit-KHSReguler" type="submit" style="display: none;" >Unggah</button>
                        </td>
                    </form>
                    <td>
                        <form action="">
                            <a id="linkKHSReguler" href="#" target="_blank"><i id="linkIconKHSReguler" class="fa-solid fa-fw fa-file-export fa-2x" style="display: none;"></i></a>
                        </form>
                    </td>
                    <td>
                        <label id="UploadKHSReguler"></label>
                    </td>
                    <?php 
                    $admin = [0,5];
                    if(in_array($this->session->userdata('RoleId'), $admin)):
                    ?>
                    <form>
                        <td>
                            <select id="validKHSReguler" name="validasiKHSReguler">
                                <option value="1">Iya</option>
                                <option value="0">Tidak</option>
                            </select>
                        </td>
                    </form>
                    <?php endif; ?>
                    </tr>
                    <tr>
                    <th scope="row">3</th>
                    <td>KHS semester Pendek (apabila memprogram) tanpa ttd PA ***</td>
                    <form method="post" action="<?= base_url('TugasAkhir/UploadKHSPendek'); ?>" enctype="multipart/form-data">
                        <td hidden><input type="text" class="form-control" id="TaId" name="TaId"></td>
                        <td>
                            <label for="input-KHSPendek">
                                <i class="fa-solid fa-fw fa-file-arrow-up fa-2x"></i> 
                            </label>
                            <input type="file" id="input-KHSPendek" name="KHSPendek" style="display: none;" accept=".pdf">
                            <button class="btn btn-primary" id="submit-KHSPendek" type="submit" style="display: none;" >Unggah</button>
                        </td>
                    </form>
                    <td>
                        <form action="">
                            <a id="linkKHSPendek" href="#" target="_blank"><i id="linkIconKHSPendek" class="fa-solid fa-fw fa-file-export fa-2x" style="display: none;"></i></a>
                        </form>
                    </td>
                    <td>
                        <label id="UploadKHSPendek"></label>
                    </td>
                    <?php 
                    $admin = [0,5];
                    if(in_array($this->session->userdata('RoleId'), $admin)):
                    ?>
                    <form>
                        <td>
                            <select id="validKHSPendek" name="validasiKHSPendek">
                                <option value="1">Iya</option>
                                <option value="0">Tidak</option>
                            </select>
                        </td>
                    </form>
                    <?php endif; ?>
                    </tr>
                    <tr>
                    <th scope="row">4</th>
                    <td>KRS (kartu Rencana Studi) yang telah ditandatangani Penasehat Akademik ***</td>
                    <form method="post" action="<?= base_url('TugasAkhir/UploadKRS'); ?>" enctype="multipart/form-data">
                        <td hidden><input type="text" class="form-control" id="TaId" name="TaId"></td>
                        <td>
                            <label for="input-KRS">
                                <i class="fa-solid fa-fw fa-file-arrow-up fa-2x"></i> 
                            </label>
                            <input type="file" id="input-KRS" name="KRS" style="display: none;" accept=".pdf">
                            <button class="btn btn-primary" id="submit-KRS" type="submit" style="display: none;" >Unggah</button>
                        </td>
                    </form>
                    <td>
                        <form action="">
                            <a id="linkKRS" href="#" target="_blank"><i id="linkIconKRS" class="fa-solid fa-fw fa-file-export fa-2x" style="display: none;"></i></a>
                        </form>
                    </td>
                    <td>
                        <label id="UploadKRS"></label>
                    </td>
                    <?php 
                    $admin = [0,5];
                    if(in_array($this->session->userdata('RoleId'), $admin)):
                    ?>
                    <form>
                        <td>
                            <select id="validKRS" name="validasiKRS">
                                <option value="1">Iya</option>
                                <option value="0">Tidak</option>
                            </select>
                        </td>
                    </form>
                    <?php endif; ?>
                    </tr>
                    <tr>
                    <th scope="row">5</th>
                    <td>Foto Berwarna Ukuran (3 x 4) Jenis file .jpg ***</td>
                    <form method="post" action="<?= base_url('TugasAkhir/UploadFoto'); ?>" enctype="multipart/form-data">
                        <td hidden><input type="text" class="form-control" id="TaId" name="TaId"></td>
                        <td>
                            <label for="input-Foto">
                                <i class="fa-solid fa-fw fa-file-arrow-up fa-2x"></i> 
                            </label>
                            <input type="file" id="input-Foto" name="Foto" style="display: none;" accept=".jpg">
                            <button class="btn btn-primary" id="submit-Foto" type="submit" style="display: none;" >Unggah</button>
                        </td>
                    </form>
                    <td>
                        <form action="">
                            <a id="linkFoto" href="#" target="_blank"><i id="linkIconFoto" class="fa-solid fa-fw fa-file-export fa-2x" style="display: none;"></i></a>
                        </form>
                    </td>
                    <td>
                        <label id="UploadFoto"></label>
                    </td>
                    <?php 
                    $admin = [0,5];
                    if(in_array($this->session->userdata('RoleId'), $admin)):
                    ?>
                    <form>
                        <td>
                            <select id="validFoto" name="validasiFoto">
                                <option value="1">Iya</option>
                                <option value="0">Tidak</option>
                            </select>
                        </td>
                    </form>
                    <?php endif; ?>
                    </tr>
                    <tr>
                    <th scope="row">6</th>
                    <td>Kartu Peserta Seminar minimal 2x mengikuti</td>
                    <form method="post" action="<?= base_url('TugasAkhir/UploadKartuSeminar'); ?>" enctype="multipart/form-data">
                        <td hidden><input type="text" class="form-control" id="TaId" name="TaId"></td>
                        <td>
                            <label for="input-KartuSeminar">
                                <i class="fa-solid fa-fw fa-file-arrow-up fa-2x"></i> 
                            </label>
                            <input type="file" id="input-KartuSeminar" name="KartuSeminar" style="display: none;" accept=".pdf">
                            <button class="btn btn-primary" id="submit-KartuSeminar" type="submit" style="display: none;" >Unggah</button>
                        </td>
                    </form>
                    <td>
                        <form action="">
                            <a id="linkKartuSeminar" href="#" target="_blank"><i id="linkIconKartuSeminar" class="fa-solid fa-fw fa-file-export fa-2x" style="display: none;"></i></a>
                        </form>
                    </td>
                    <td>
                        <label id="UploadKartuSeminar"></label>
                    </td>
                    <?php 
                    $admin = [0,5];
                    if(in_array($this->session->userdata('RoleId'), $admin)):
                    ?>
                    <form>
                        <td>
                            <select id="validKartuSeminar" name="validasiKartuSeminar">
                                <option value="1">Iya</option>
                                <option value="0">Tidak</option>
                            </select>
                        </td>
                    </form>
                    <?php endif; ?>
                    </tr>
                    <tr>
                    <th scope="row">7</th>
                    <td>Bukti mengikuti FIDK (Forum Ilmiah Departemen Kimia)</td>
                    <form method="post" action="<?= base_url('TugasAkhir/UploadFIDK'); ?>" enctype="multipart/form-data">
                        <td hidden><input type="text" class="form-control" id="TaId" name="TaId"></td>
                        <td>
                            <label for="input-FIDK">
                                <i class="fa-solid fa-fw fa-file-arrow-up fa-2x"></i> 
                            </label>
                            <input type="file" id="input-FIDK" name="FIDK" style="display: none;" accept=".pdf">
                            <button class="btn btn-primary" id="submit-FIDK" type="submit" style="display: none;" >Unggah</button>
                        </td>
                    </form>
                    <td>
                        <form action="">
                            <a id="linkFIDK" href="#" target="_blank"><i id="linkIconFIDK" class="fa-solid fa-fw fa-file-export fa-2x" style="display: none;"></i></a>
                        </form>
                    </td>
                    <td>
                        <label id="UploadFIDK"></label>
                    </td>
                    <?php 
                    $admin = [0,5];
                    if(in_array($this->session->userdata('RoleId'), $admin)):
                    ?>
                    <form>
                        <td>
                            <select id="validFIDK" name="validasiFIDK">
                                <option value="1">Iya</option>
                                <option value="0">Tidak</option>
                            </select>
                        </td>
                    </form>
                    <?php endif; ?>
                    </tr>
                    <tr>
                    <th scope="row">8</th>
                    <td>Transkrip Akademik ***</td>
                    <form method="post" action="<?= base_url('TugasAkhir/UploadTranskrip'); ?>" enctype="multipart/form-data">
                        <td hidden><input type="text" class="form-control" id="TaId" name="TaId"></td>
                        <td>
                            <label for="input-Transkrip">
                                <i class="fa-solid fa-fw fa-file-arrow-up fa-2x"></i> 
                            </label>
                            <input type="file" id="input-Transkrip" name="Transkrip" style="display: none;" accept=".pdf">
                            <button class="btn btn-primary" id="submit-Transkrip" type="submit" style="display: none;" >Unggah</button>
                        </td>
                    </form>
                    <td>
                        <form action="">
                            <a id="linkTranskrip" href="#" target="_blank"><i id="linkIconTranskrip" class="fa-solid fa-fw fa-file-export fa-2x" style="display: none;"></i></a>
                        </form>
                    </td>
                    <td>
                        <label id="UploadTranskrip"></label>
                    </td>
                    <?php 
                    $admin = [0,5];
                    if(in_array($this->session->userdata('RoleId'), $admin)):
                    ?>
                    <form>
                        <td>
                            <select id="validTranskrip" name="validasiTranskrip">
                                <option value="1">Iya</option>
                                <option value="0">Tidak</option>
                            </select>
                        </td>
                    </form>
                    <?php endif; ?>
                    </tr>
                </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>

<!-- End of Main Content -->