<!-- Begin Page Content -->
<div class="container-fluid">
    <?php 
        $mhs = [2,3,4];
        if (!in_array($this->session->userdata('RoleId'), $mhs)):
    ?>
    <div class="card">
        <div class="row mt-3 ml-2 mr-2">
            <div class="col-4">
                <input id="TaId" type="text" value="<?= $TaId ?>" hidden>
                <h4 class="text-white bg-dark">Penilaian Seminar Kemajuan Tugas Akhir</h4>
            </div>
            <div class="col-8">
            </div>
        </div>
        <form method="post" action="<?= base_url('TugasAkhir/SubmitNilaiSemju/'. $TaId); ?>" >
            <?php 
                if($TipeDosen == 0 && in_array($this->session->userdata('RoleId'), [0,5])):
            ?>
                <div class="row mt-3 ml-4 mr-4">
                    <div class="col-2">
                        <label for="tipeDosen">Silahkan Pilih Dosen</label>
                        <select name="tipeDosen" id="tipeDosen" class="form-control">
                            <option value="1">Dosen 1</option>
                            <option value="2">Dosen 2</option>
                            <option value="3">Dosen 3</option>
                        </select>
                    </div>
                    <div class="col-2">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item active" aria-current="page"><?= $DosenId['NamaMahasiswa'] ?></li>
                            </ol>
                        </nav>
                    </div>
                    <div class="col-8">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                <th scope="col">Dosen 1</th>
                                <th scope="col">Dosen 2</th>
                                <th scope="col">Dosen 3</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                <td><?= $DosenId['Dosen1Name'] ?></td>
                                <td><?= $DosenId['Dosen2Name'] ?></td>
                                <td><?= $DosenId['Dosen3Name'] ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            <?php endif; ?>
            <div class="row mt-3 ml-2 mr-2">
                <div class="col-6">
                    <h4 class="text-white bg-dark">Presentasi</h4>
                    <table class="table">
                        <thead class="thead-light">
                            <tr>
                            <th scope="col">CPMK</th>
                            <th scope="col">Kategori</th>
                            <th scope="col">Nilai</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                            <td>9C</td>
                            <td>Tata bahasa dan susunan kalimat.</td>
                            <td class="col-6">
                                <select id="tataBahasa_9C" class="form-control" name="tataBahasa_9C">
                                    <option value="" data-info="">Pilih opsi</option>
                                    <option value="1" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Pres1'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Pres1'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Pres1'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Penyaji menggunakan kalimat dengan tata bahasa yang salah atau tidak lengkap dan tidak mendukung topik yang dibawakan. Sering menggunakan kalimat bahasa pergaulan (lebih dari 6 kali).">1</option>
                                    <option value="2" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Pres1'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Pres1'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Pres1'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Penyaji menggunakan kalimat yang baik dan cukup mendukung topik yang dibawakan. Sesekali menggunakan kalimat bahasa pergaulan.">2</option>
                                    <option value="3" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Pres1'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Pres1'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Pres1'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Penyaji menggunakan kalimat dengan tata bahasa yang baik dan cukup mendukung topik yang dibawakan. Tidak pernah menggunakan kalimat bahasa pergaulan.">3</option>
                                    <option value="4" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Pres1'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Pres1'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Pres1'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Penyaji menggunakan kalimat dengan tata bahasa yang sangat baik dan mendukung topik yang dibawakan. Tidak pernah menggunakan kalimat bahasa pergaulan.">4</option>
                                </select>
                                <div id="tataBahasa_9CInfo" class="alert alert-info" style="display: none;"></div>
                            </td>
                            </tr>
                            <tr>
                            <td>9C</td>
                            <td>Kosa kata</td>
                            <td class="col-6">
                                <select id="kosaKata_9C" class="form-control" name="kosaKata_9C">
                                    <option value="" data-info="">Pilih opsi</option>
                                    <option value="1" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Pres2'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Pres2'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Pres2'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Penyaji menggunakan kata-kata atau frasa yang tidak cocok dan tidak dipahami penonton.">1</option>
                                    <option value="2" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Pres2'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Pres2'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Pres2'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Penyaji menggunakan kata-kata yang cukup baik. Tidak menggunakan kata-kata yang mungkin baru baik penonton.">2</option>
                                    <option value="3" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Pres2'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Pres2'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Pres2'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Penyaji menggunakan kata-kata yang cukup baik dan menggunakan beberapa istilah baru bagi penonton tapi tidak menjelaskannya.">3</option>
                                    <option value="4" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Pres2'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Pres2'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Pres2'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Penyaji menggunakan kata-kata yang sangat tepat dan menjelaskan semua istilah bila dianggap baru bagi penonton.">4</option>
                                </select>
                                <div id="kosaKata_9CInfo" class="alert alert-info" style="display: none;"></div>
                            </td>
                            </tr>
                            <tr>
                            <td>9C</td>
                            <td>pengucapan_9C</td>
                            <td class="col-6">
                                <select id="pengucapan_9C" class="form-control" name="pengucapan_9C">
                                    <option value="" data-info="">Pilih opsi</option>
                                    <option value="1" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Pres3'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Pres3'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Pres3'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Sering bergumam atau salah pengucapan_9C kata enam kali atau lebih.">1</option>
                                    <option value="2" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Pres3'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Pres3'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Pres3'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Hampir selalu berbicara secara jelas (94 -- 85%) dan ada dua sampai lima salah pengucapan_9C kata.">2</option>
                                    <option value="3" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Pres3'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Pres3'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Pres3'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Berbicara secara jelas (100 -- 95%) dan ada satu salah pengucapan_9C kata.">3</option>
                                    <option value="4" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Pres3'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Pres3'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Pres3'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Berbicara secara jelas (100 -- 95%) dan tidak ada salah pengucapan_9C kata.">4</option>
                                </select>
                                <div id="pengucapan_9CInfo" class="alert alert-info" style="display: none;"></div>
                            </td>
                            </tr>
                            <tr>
                            <td>9B</td>
                            <td>penyampaian_9B</td>
                            <td class="col-6">
                                <select id="penyampaian_9B" class="form-control" name="penyampaian_9B">
                                    <option value="" data-info="">Pilih opsi</option>
                                    <option value="1" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Pres4'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Pres4'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Pres4'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Penyaji tampak sekali tidak mempersiapkan diri sebelumnya. Gerak badan tidak layak bagi orang yang sedang presentasi.">1</option>
                                    <option value="2" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Pres4'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Pres4'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Pres4'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Penyaji tampak telah mempersiapkan diri seadanya dan tidak ingat data dan percobaan yang dilakukan sehingga perlu waktu untuk membaca catatan.">2</option>
                                    <option value="3" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Pres4'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Pres4'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Pres4'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Penyaji tampak telah mempersiapkan diri dengan cukup baik dan cukup menguasai data dan percobaan yang sudah dilakukan">3</option>
                                    <option value="4" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Pres4'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Pres4'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Pres4'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Penyaji tampak telah mempersiapkan diri dengan sangat baik dan menguasai data dan percobaan yang sudah diperoleh.">4</option>
                                </select>
                                <div id="penyampaian_9BInfo" class="alert alert-info" style="display: none;"></div>
                            </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-6">
                    <h4 class="text-white bg-dark">Berkas Presentasi</h4>
                    <table class="table">
                        <thead class="thead-light">
                            <tr>
                            <th scope="col">CPMK</th>
                            <th scope="col">Kategori</th>
                            <th scope="col">Nilai</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                            <td>8A</td>
                            <td>Mendapatkan data berdasarkan langkah-langkah yang telah dibuat</td>
                            <td class="col-6">
                                <select id="langkahLangkah_8A" class="form-control" name="langkahLangkah_8A">
                                    <option value="" data-info="">Pilih opsi</option>
                                    <option value="1" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Ber1'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Ber1'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Ber1'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Melakukan percobaan pengambilan data dengan metoda yang salah">1</option>
                                    <option value="2" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Ber1'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Ber1'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Ber1'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Langkah-langkah yang dilaksanakan telah menghasilkan data yang tidak dapat dipercaya.">2</option>
                                    <option value="3" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Ber1'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Ber1'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Ber1'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Pengambilan data dilaksanakan sesuai rencana, ada hasil dan dapat menunjukkan rencana selanjutnya.">3</option>
                                    <option value="4" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Ber1'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Ber1'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Ber1'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Pengambilan data dilaksanakan dengan benar dengan hasil dapat dijelaskan dengan baik, dan dapat menunjukkan apa yang akan dikerjakan berikutnya dengan tepat.">4</option>
                                </select>
                                <div id="langkahLangkah_8AInfo" class="alert alert-info" style="display: none;"></div>
                            </td>
                            </tr>
                            <tr>
                            <td>8A</td>
                            <td>Mendapatkan data sesuai teori dan pengalaman saat kuliah dan praktikum</td>
                            <td class="col-6">
                                <select id="data_8A" class="form-control" name="data_8A">
                                    <option value="" data-info="">Pilih opsi</option>
                                    <option value="1" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Ber2'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Ber2'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Ber2'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Melaksanakan percobaan tanpa dapat menyebutkan dasarnya.">1</option>
                                    <option value="2" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Ber2'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Ber2'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Ber2'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Melaksanaan percobaan dengan sedikit dukungan teori atau pengalaman terhadap metode yang dipilih.">2</option>
                                    <option value="3" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Ber2'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Ber2'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Ber2'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Melaksanaan percobaan sesuai dukungan teori atau pengalaman terhadap metode yang dipilih.">3</option>
                                    <option value="4" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Ber2'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Ber2'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Ber2'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Melaksanaan percobaan dengan tepat dan menunjukkan dukungan teori atau pengalaman kuat terhadap metode yang dipilih.">4</option>
                                </select>
                                <div id="data_8AInfo" class="alert alert-info" style="display: none;"></div>
                            </td>
                            </tr>
                            <tr>
                            <td>8B</td>
                            <td>Mengolah data</td>
                            <td class="col-6">
                                <select id="mengolahData_8B" class="form-control" name="mengolahData_8B">
                                    <option value="" data-info="">Pilih opsi</option>
                                    <option value="1" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Ber3'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Ber3'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Ber3'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Tidak tepat dalam menampilkan data yang sudah diperoleh">1</option>
                                    <option value="2" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Ber3'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Ber3'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Ber3'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Menampilkan data dalam tabel, tidak memperhatikan kecenderungan data.">2</option>
                                    <option value="3" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Ber3'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Ber3'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Ber3'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Menampilkan data dalam grafik, mengelompokkan atau memisahkan data, dan menunjukkan kecenderungan.">3</option>
                                    <option value="4" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Ber3'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Ber3'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Ber3'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Menampilkan data dalam grafik yang tepat, mengubah data menjadi informasi yang berguna bagi pencapaian tujuan.">4</option>
                                </select>
                                <div id="mengolahData_8BInfo" class="alert alert-info" style="display: none;"></div>
                            </td>
                            </tr>
                            <tr>
                            <td>8C</td>
                            <td>Membuat kesimpulan</td>
                            <td class="col-6">
                                <select id="kesimpulan_8C" class="form-control" name="kesimpulan_8C">
                                    <option value="" data-info="">Pilih opsi</option>
                                    <option value="1" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Ber4'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Ber4'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Ber4'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Data masih tercecer belum berhubungan satu sama lain.">1</option>
                                    <option value="2" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Ber4'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Ber4'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Ber4'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Tidak dapat menyebutkan informasi yang terkandung dari data yang sudah diperoleh.">2</option>
                                    <option value="3" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Ber4'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Ber4'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Ber4'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Menyebutkan semua informasi yang diperoleh dari data yang sudah dapat dikumpulkan.">3</option>
                                    <option value="4" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Ber4'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Ber4'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Ber4'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Menyampaikan kesimpulan sementara dari informasi yang sudah diperoleh.">4</option>
                                </select>
                                <div id="kesimpulan_8CInfo" class="alert alert-info" style="display: none;"></div>
                            </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <?php 
                if (
                    !in_array(true, [
                        $this->session->userdata('Kadep'),
                        $this->session->userdata('Sekdep'),
                        $this->session->userdata('KaprodiS1'),
                        $this->session->userdata('KaprodiS2'),
                        $this->session->userdata('KaprodiS3'),
                    ]) || 
                    in_array($this->session->userdata('Id'), [
                        $DosenId['Dosen1Id'],
                        $DosenId['Dosen2Id'],
                        $DosenId['Dosen3Id'],
                    ])
                ) :
            ?>
            <div class="text-right mr-5 mb-3">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Submit</button>
            </div>
            <?php endif; ?>
        </form> 
    </div>
    <?php endif; ?>

    <!-- Card Kritik & Saran -->
    <?php if($TipeDosen != 0):?>
    <div class="card">
        <form method="post" action="<?= base_url('tugasAkhir/SubmitSaranSemju/'. $TaId); ?>" >
            <div class="card-content mr-2 ml-2 mb-2">
                <h4 class="text-white bg-dark">Pertanyaan & Saran</h4>
                <input type="hidden" name="saran" value="<?= set_value('saran') ?>">
                <?php if (isset($Nilai) && $this->session->userdata['Id'] == $Nilai['Dosen1Id']) : ?>
                    <div id="quillEditor" style="min-height: 100px;"><?php
                        if ($Nilai['Dsn1Saran'] != null){
                            echo strip_tags($Nilai['Dsn1Saran']);
                        }else{
                            echo "";
                        }
                         ?></div>
                <?php elseif (isset($Nilai) && $this->session->userdata['Id'] == $Nilai['Dosen2Id']) : ?>
                    <div id="quillEditor" style="min-height: 100px;"><?php
                        if ($Nilai['Dsn2Saran'] != null){
                            echo strip_tags($Nilai['Dsn2Saran']);
                        }else{
                            echo "";
                        }
                        ?></div>
                <?php elseif (isset($Nilai) && $this->session->userdata['Id'] == $Nilai['Dosen3Id']) : ?>
                    <div id="quillEditor" style="min-height: 100px;"><?php
                        if ($Nilai['Dsn3Saran'] != null){
                            echo strip_tags($Nilai['Dsn3Saran']);
                        }else{
                            echo "";
                        }
                        ?></div>
                <?php else : ?>
                    <div id="quillEditor" style="min-height: 100px;"><?= set_value('saran') ?></div>
                <?php endif; ?>
            </div>
            <div class="text-right mr-5 mb-3">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Submit</button>
            </div>
        </form> 
    </div>
    <?php endif; ?>

    <!-- Card Tampil Saran -->
    <?php 
        $mhs = [0,2,3,4];
        if (in_array($this->session->userdata('RoleId'), $mhs)):
    ?>
    <div class="card">
        <div class="row mt-3 ml-2 mr-2">
            <div class="col-xl-4 col-md-6 mb-4">
                <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                    Saran dan Kritik Dosen 1 :</div>
                                <?= $Dsn1Saran; ?>
                            </div>
                            <div class="col-auto">
                                <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6 mb-4">
                <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                    Saran dan Kritik Dosen 2 :</div>
                                <?= $Dsn2Saran; ?>
                            </div>
                            <div class="col-auto">
                                <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6 mb-4">
                <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                    Saran dan Kritik Dosen 3 :</div>
                                <?= $Dsn3Saran; ?>
                            </div>
                            <div class="col-auto">
                                <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php endif; ?>

    <!-- Card Nilai -->
    <div class="card">
        <?php if ($TipeDosen == 1) :?>
        <div class="row mt-3 ml-2 mr-2">
            <div class="col-12">
                <div class="row ml-2">
                    <h4 class="text-white bg-dark">Dosen 1</h4>
                </div>
                <div class="row">
                    <div class="col-xl-4 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Nilai Presentasi</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $totalNilaiPresentasiDsn1 ?></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Nilai Berkas</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $totalNilaiBerkasDsn1 ?></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Nilai Dosen</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $totalNilaiSemjuDsn1TA ?></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php elseif ($TipeDosen == 2) :?>
        <div class="row mt-3 ml-2 mr-2">
            <div class="col-12">
                <div class="row ml-2">
                    <h4 class="text-white bg-dark">Dosen 2</h4>
                </div>
                <div class="row">
                    <div class="col-xl-4 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Nilai Presentasi</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $totalNilaiPresentasiDsn2 ?></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Nilai Berkas</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $totalNilaiBerkasDsn2 ?></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Nilai Dosen</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $totalNilaiSemjuDsn2TA ?></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php elseif ($TipeDosen == 3) :?>
        <div class="row mt-3 ml-2 mr-2">
            <div class="col-12">
                <div class="row ml-2">
                    <h4 class="text-white bg-dark">Dosen 3</h4>
                </div>
                <div class="row">
                    <div class="col-xl-4 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Nilai Presentasi</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $totalNilaiPresentasiDsn3 ?></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Nilai Berkas</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $totalNilaiBerkasDsn3 ?></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Nilai Dosen</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $totalNilaiSemjuDsn3TA ?></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
            elseif ($TipeDosen == 0 & in_array($this->session->userdata('RoleId'), [0,5])):
        ?>

        <div class="row mt-3 ml-2 mr-2">
            <div class="col-12">
                <div class="row ml-2">
                    <h4 class="text-white bg-dark">Dosen 1</h4>
                </div>
                <div class="row">
                    <div class="col-xl-4 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Nilai Presentasi</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $totalNilaiPresentasiDsn1 ?></div>
                                    </div>
                                    <div class="col-auto PresentasiSemju dosen1">
                                        <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Nilai Berkas</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $totalNilaiBerkasDsn1 ?></div>
                                    </div>
                                    <div class="col-auto NaskahSemju dosen1">
                                        <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Nilai Dosen 1</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $totalNilaiSemjuDsn1TA ?></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mt-3 ml-2 mr-2">
            <div class="col-12">
                <div class="row ml-2">
                    <h4 class="text-white bg-dark">Dosen 2</h4>
                </div>
                <div class="row">
                    <div class="col-xl-4 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Nilai Presentasi</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $totalNilaiPresentasiDsn2 ?></div>
                                    </div>
                                    <div class="col-auto PresentasiSemju dosen2">
                                        <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Nilai Berkas</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $totalNilaiBerkasDsn2 ?></div>
                                    </div>
                                    <div class="col-auto NaskahSemju dosen2">
                                        <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Nilai Dosen 2</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $totalNilaiSemjuDsn2TA ?></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mt-3 ml-2 mr-2">
            <div class="col-12">
                <div class="row ml-2">
                    <h4 class="text-white bg-dark">Dosen 3</h4>
                </div>
                <div class="row">
                    <div class="col-xl-4 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Nilai Presentasi</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $totalNilaiPresentasiDsn3 ?></div>
                                    </div>
                                    <div class="col-auto PresentasiSemju dosen3">
                                        <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Nilai Berkas</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $totalNilaiBerkasDsn3 ?></div>
                                    </div>
                                    <div class="col-auto NaskahSemju dosen3">
                                        <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Nilai Dosen 3</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $totalNilaiSemjuDsn3TA ?></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php endif ;?>
        
        <?php 
            $mhs = [2,3,4];
            if(in_array($this->session->userdata('RoleId'),$mhs)):
        ?>
        <hr>
        <div class="row mt-3 ml-2 mr-2">
            <div class="col-xl-2 col-md-6 mb-4">
            </div>
            <div class="col-xl-4 col-md-6 mb-4">
                <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                    Rata-rata Nilai Presentasi Seminar Kemajuan</div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $averagePresentasi ?></div>
                            </div>
                            <div class="col-auto">
                                <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6 mb-4">
                <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                    Rata-Rata Nilai Berkah Seminar Kemajuan</div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $averageBerkas ?></div>
                            </div>
                            <div class="col-auto">
                                <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-2 col-md-6 mb-4">
            </div>
        </div>

        <?php endif; ?>

        <hr>
        <div class="row">
            <div class="col-xl-4 col-md-6 mb-4">
            </div>
            <div class="col-xl-4 col-md-6 mb-4">
                <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                    Nilai Akhir Seminar Kemajuan</div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $NilaiAkhirSemjuTA ?></div>
                            </div>
                            <div class="col-auto">
                                <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6 mb-4">
            </div>
        </div>
    </div>
    
    <!-- Nilai Presentasi -->
    <div class="modal fade" id="PresentasiSemju" role="dialog" aria-labelledby="exampleModalLabel"
        >
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Detail Nilai Presentasi Seminar Kemajuan</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                <div class="card">
                    <div class="card-body">
                    <div class="form-group row">
                        <label for="inputPresent1" class="col-sm-4 col-form-label">Tata bahasa dan susunan kalimat.</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputPresent1" name="Present1" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPresent2" class="col-sm-4 col-form-label">Kosa kata</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputPresent2" name="Present2" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPresent3" class="col-sm-4 col-form-label">Pengucapan</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputPresent3" name="Present3" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPresent4" class="col-sm-4 col-form-label">Penyampaian</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputPresent4" name="Present4" disabled>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Tutup</button>
                </div>
            </div>
            </div>
        </div>
    </div>
    </div>

     <!-- Nilai Naskah -->
     <div class="modal fade" id="NaskahSemju" role="dialog" aria-labelledby="exampleModalLabel"
        >
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Detail Nilai Naskah Seminar Kemajuan</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                <div class="card">
                    <div class="card-body">
                    <div class="form-group row">
                        <label for="inputNaskah1" class="col-sm-4 col-form-label">Mendapatkan data berdasarkan langkah-langkah yang telah dibuat</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputNaskah1" name="Naskah1" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputNaskah2" class="col-sm-4 col-form-label">Mendapatkan data sesuai teori dan pengalaman saat kuliah dan praktikum</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputNaskah2" name="Naskah2" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputNaskah3" class="col-sm-4 col-form-label">Mengolah data</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputNaskah3" name="Naskah3" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputNaskah4" class="col-sm-4 col-form-label">Membuat kesimpulan</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputNaskah4" name="Naskah4" disabled>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Tutup</button>
                </div>
            </div>
            </div>
        </div>
    </div>
    </div>

</div>