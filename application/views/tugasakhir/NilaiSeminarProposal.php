<!-- Begin Page Content -->
<div class="container-fluid">
    <?php 
        $UserId = $this->session->userdata('Id');
        $mhs = [2,3,4];
        if (!in_array($this->session->userdata('RoleId'), $mhs)):
    ?>
    <div class="card">
        <div class="row mt-3 ml-2 mr-2">
            <div class="col-4">
                <h4 class="text-white bg-dark">Penilaian Seminar Proposal Tugas Akhir</h4>
            </div>
            <div class="col-5">
                <input id="TaId" type="text" value="<?= $TaId ?>" hidden>
            </div>
            <div class="col-3">
            </div>
        </div>
        <form method="post" action="<?= base_url('TugasAkhir/SubmitNilaiSempro/'. $TaId); ?>" >
            <?php 
            if($TipeDosen == 0 && in_array($this->session->userdata('RoleId'), [0,5])):
            ?>
                <div class="row mt-3 ml-4 mr-4">
                    <div class="col-2">
                        <label for="tipeDosen">Silahkan Pilih Dosen</label>
                        <select name="tipeDosen" id="tipeDosen" class="form-control">
                            <option value="1">Dosen 1</option>
                            <option value="2">Dosen 2</option>
                            <option value="3">Dosen 3</option>
                        </select>
                    </div>
                    <div class="col-2">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item active" aria-current="page"><?= $DosenId['NamaMahasiswa'] ?></li>
                            </ol>
                        </nav>
                    </div>
                    <div class="col-8">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                <th scope="col">Dosen 1</th>
                                <th scope="col">Dosen 2</th>
                                <th scope="col">Dosen 3</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                <td><?= $DosenId['Dosen1Name'] ?></td>
                                <td><?= $DosenId['Dosen2Name'] ?></td>
                                <td><?= $DosenId['Dosen3Name'] ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            <?php endif; ?>
            <div class="row mt-3 ml-2 mr-2">
                <div class="col-6">
                    <h4 class="text-white bg-dark">Presentasi</h4>
                    <table class="table">
                        <thead class="thead-light">
                            <tr>
                            <th scope="col">CPMK</th>
                            <th scope="col">Kategori</th>
                            <th scope="col">Nilai</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                            <td>9C</td>
                            <td>Tata bahasa dan susunan kalimat.</td>
                            <td class="col-6">
                                <select id="tataBahasa_9C" class="form-control" name="tataBahasa_9C">
                                    <option value="" data-info="">Pilih opsi</option>
                                    <option value="1" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Pres1'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Pres1'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Pres1'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Penyaji menggunakan kalimat dengan tata bahasa yang salah atau tidak lengkap dan tidak mendukung topik yang dibawakan. Sering menggunakan kalimat bahasa pergaulan (lebih dari 6 kali).">1</option>
                                    <option value="2" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Pres1'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Pres1'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Pres1'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Penyaji menggunakan kalimat yang baik dan cukup mendukung topik yang dibawakan. Sesekali menggunakan kalimat bahasa pergaulan.">2</option>
                                    <option value="3" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Pres1'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Pres1'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Pres1'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Penyaji menggunakan kalimat dengan tata bahasa yang baik dan cukup mendukung topik yang dibawakan. Tidak pernah menggunakan kalimat bahasa pergaulan.">3</option>
                                    <option value="4" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Pres1'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Pres1'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Pres1'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Penyaji menggunakan kalimat dengan tata bahasa yang sangat baik dan mendukung topik yang dibawakan. Tidak pernah menggunakan kalimat bahasa pergaulan.">4</option>
                                </select>
                                <div id="tataBahasa_9CInfo" class="alert alert-info" style="display: none;"></div>
                            </td>
                            </tr>
                            <tr>
                            <td>9C</td>
                            <td>Kosa kata</td>
                            <td class="col-6">
                                <select id="kosaKata_9C" class="form-control" name="kosaKata_9C">
                                    <option value="" data-info="">Pilih opsi</option>
                                    <option value="1" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Pres2'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Pres2'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Pres2'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Penyaji menggunakan kata-kata atau frasa yang tidak cocok dan tidak dipahami penonton.">1</option>
                                    <option value="2" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Pres2'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Pres2'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Pres2'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Penyaji menggunakan kata-kata yang cukup baik. Tidak menggunakan kata-kata yang mungkin baru baik penonton.">2</option>
                                    <option value="3" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Pres2'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Pres2'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Pres2'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Penyaji menggunakan kata-kata yang cukup baik dan menggunakan beberapa istilah baru bagi penonton tapi tidak menjelaskannya.">3</option>
                                    <option value="4" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Pres2'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Pres2'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Pres2'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Penyaji menggunakan kata-kata yang sangat tepat dan menjelaskan semua istilah bila dianggap baru bagi penonton.">4</option>
                                </select>
                                <div id="kosaKata_9CInfo" class="alert alert-info" style="display: none;"></div>
                            </td>
                            </tr>
                            <tr>
                            <td>9C</td>
                            <td>Pengucapan</td>
                            <td class="col-6">
                                <select id="pengucapan_9C" class="form-control" name="pengucapan_9C">
                                    <option value="" data-info="">Pilih opsi</option>
                                    <option value="1" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Pres3'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Pres3'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Pres3'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Sering bergumam atau salah pengucapan_9C kata enam kali atau lebih.">1</option>
                                    <option value="2" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Pres3'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Pres3'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Pres3'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Hampir selalu berbicara secara jelas (94 -- 85%) dan ada dua sampai lima salah pengucapan_9C kata.">2</option>
                                    <option value="3" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Pres3'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Pres3'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Pres3'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Berbicara secara jelas (100 -- 95%) dan ada satu salah pengucapan_9C kata.">3</option>
                                    <option value="4" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Pres3'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Pres3'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Pres3'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Berbicara secara jelas (100 -- 95%) dan tidak ada salah pengucapan_9C kata.">4</option>
                                </select>
                                <div id="pengucapan_9CInfo" class="alert alert-info" style="display: none;"></div>
                            </td>
                            </tr>
                            <tr>
                            <td>6</td>
                            <td>Penyampaian</td>
                            <td class="col-6">
                                <select id="penyampaian_6" class="form-control" name="penyampaian_6">
                                    <option value="" data-info="">Pilih opsi</option>
                                    <option value="1" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Pres4'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Pres4'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Pres4'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Penyaji tampak sekali tidak mempersiapkan diri sebelumnya. Tidak dapat menyebut jelas satu pun literatur dalam presentasinya.">1</option>
                                    <option value="2" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Pres4'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Pres4'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Pres4'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Penyaji tampak telah mempersiapkan diri seadanya dan sedikit tahu literatur yang mendukung presentasinya.">2</option>
                                    <option value="3" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Pres4'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Pres4'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Pres4'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Penyaji tampak telah mempersiapkan diri dengan cukup baik dan menyebutkan sebagian literatur yang dibutuhkan dalam presentasinya.">3</option>
                                    <option value="4" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Pres4'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Pres4'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Pres4'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Penyaji tampak telah mempersiapkan diri dengan sangat baik yaitu mengetahui semua literatur yang mendukung presentasinya.">4</option>
                                </select>
                                <div id="penyampaian_6Info" class="alert alert-info" style="display: none;"></div>
                            </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-6">
                    <h4 class="text-white bg-dark">Naskah</h4>
                    <table class="table">
                        <thead class="thead-light">
                            <tr>
                            <th scope="col">CPMK</th>
                            <th scope="col">Kategori</th>
                            <th scope="col">Nilai</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                            <td>9C</td>
                            <td>Menguraikan ide atau hasil pekerjaannya</td>
                            <td class="col-6">
                                <select id="ideHasilPekerjaan_9C" class="form-control" name="ideHasilPekerjaan_9C">
                                    <option value="" data-info="">Pilih opsi</option>
                                    <option value="1" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Nas1'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Nas1'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Nas1'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Pembaca tidak dapat memahami proposal karena tidak ada urutan informasi.">1</option>
                                    <option value="2" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Nas1'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Nas1'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Nas1'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Pembaca mengalami kesulitan mengikuti isi proposal karena penyampaian_6 melompat-lompat dan hanya menunjukkan organisasi minimal. Pustaka tertulis tidak konsisten dan terbatas.">2</option>
                                    <option value="3" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Nas1'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Nas1'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Nas1'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Informasi disampaikan dalam urutan logis yang dapat diikuti pembaca. Pustaka tertulis dengan baik dan cukup lengkap.">3</option>
                                    <option value="4" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Nas1'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Nas1'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Nas1'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Proposal berisi informasi dalam urutan logis dan menarik yang dapat diikuti oleh pembaca. Pustaka tertulis dengan sangat baik dan sangat lengkap.">4</option>
                                </select>
                                <div id="ideHasilPekerjaan_9CInfo" class="alert alert-info" style="display: none;"></div>
                            </td>
                            </tr>
                            <tr>
                            <td>9C</td>
                            <td>Merumuskan masalah dan tujuan penelitian</td>
                            <td class="col-6">
                                <select id="masalahTujuan_9C" class="form-control" name="masalahTujuan_9C">
                                    <option value="" data-info="">Pilih opsi</option>
                                    <option value="1" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Nas2'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Nas2'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Nas2'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Masalah dan tujuan penelitian tidak jelas.">1</option>
                                    <option value="2" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Nas2'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Nas2'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Nas2'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Masalah dan tujuan penelitian dinyatakan, tetapi keterkaitan keduanya kurang didukung isi proposal.">2</option>
                                    <option value="3" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Nas2'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Nas2'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Nas2'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Masalah dan tujuan penelitian dinyatakan dan didukung isi proposal secara keseluruhan.">3</option>
                                    <option value="4" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Nas2'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Nas2'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Nas2'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Masalah dan tujuan penelitian dinyatakan dengan jelas, dirumuskan dengan tepat dan sangat didukung isi proposal secara keseluruhan.">4</option>
                                </select>
                                <div id="masalahTujuan_9CInfo" class="alert alert-info" style="display: none;"></div>
                            </td>
                            </tr>
                            <tr>
                            <td>6</td>
                            <td>Menyusun langkah-langkah pengerjaan</td>
                            <td class="col-6">
                                <select id="langkahLangkah_6" class="form-control" name="langkahLangkah_6">
                                    <option value="" data-info="">Pilih opsi</option>
                                    <option value="1" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Nas3'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Nas3'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Nas3'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Langkah-langkah penelitian tidak jelas.">1</option>
                                    <option value="2" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Nas3'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Nas3'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Nas3'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Langkah-langkah penelitian dinyatakan, tetapi hanya meliputi sebagian penelitian.">2</option>
                                    <option value="3" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Nas3'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Nas3'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Nas3'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Langkah-langkah dituliskan dan meliputi seluruh rangkaian penelitian.">3</option>
                                    <option value="4" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Nas3'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Nas3'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Nas3'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Langkah-langkah penelitian diuraikan dengan jelas, dirumuskan dengan tepat dan lengkap.">4</option>
                                </select>
                                <div id="langkahLangkah_6Info" class="alert alert-info" style="display: none;"></div>
                            </td>
                            </tr>
                            <tr>
                            <td>6</td>
                            <td>Cakupan penelitian</td>
                            <td class="col-6">
                                <select id="cakupan_6" class="form-control" name="cakupan_6">
                                    <option value="" data-info="">Pilih opsi</option>
                                    <option value="1" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Nas4'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Nas4'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Nas4'] == 1){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Tidak ada nilai 1">1</option>
                                    <option value="2" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Nas4'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Nas4'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Nas4'] == 2){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Rancangan percobaan hanya melihat pengaruh satu variabel terhadap hasil atau karakterisasi suatu produk percobaan pada satu parameter yang sama.">2</option>
                                    <option value="3" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Nas4'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Nas4'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Nas4'] == 3){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Rancangan percobaan cukup lengkap untuk mengamati pengaruh dua variabel terhadap hasil atau karakterisasi suatu produk percobaan pada dua parameter dengan dua instrumen berbeda.">3</option>
                                    <option value="4" <?php 
                                        if (isset($Nilai)){
                                            switch ($this->session->userdata('Id')){
                                                case $Nilai['Dosen1Id']:
                                                    if ($Nilai['Dsn1Nas4'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen2Id']:
                                                    if ($Nilai['Dsn2Nas4'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                                case $Nilai['Dosen3Id']:
                                                    if ($Nilai['Dsn3Nas4'] == 4){
                                                        echo "selected";
                                                    }
                                                    break;
                                            }
                                        }
                                    ?> data-info="Rancangan percobaan lengkap untuk mengamati pengaruh dua variabel terhadap hasil atau karakterisasi suatu produk percobaan pada dua parameter dengan dua instrumen berbeda.">4</option>
                                </select>
                                <div id="cakupan_6Info" class="alert alert-info" style="display: none;"></div>
                            </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <?php 
            $acc = [0,1,5];
                if (
                    (!in_array(true, [
                        $this->session->userdata('Kadep'),
                        $this->session->userdata('Sekdep'),
                        $this->session->userdata('KaprodiS1'),
                        $this->session->userdata('KaprodiS2'),
                        $this->session->userdata('KaprodiS3'),
                    ]) || 
                    in_array($this->session->userdata('Id'), [
                        $DosenId['Dosen1Id'],
                        $DosenId['Dosen2Id'],
                        $DosenId['Dosen3Id'],
                    ])) && (in_array($this->session->userdata('Id'), $acc)) 
                ) :
            ?>
            <div class="text-right mr-5 mb-3">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Submit</button>
            </div>
            <?php endif ?>
        </form> 
    </div>
    <?php endif; ?>

    <!-- Card Kritik & Saran -->
    <?php if($TipeDosen != 0):?>
    <div class="card">
        <div class="row mt-3 ml-2 mr-2">
            <div class="col-12">
                <form method="post" action="<?= base_url('tugasAkhir/SubmitSaranSempro/'. $TaId); ?>" >
                    <div class="card-content mr-2 ml-2 mb-2">
                        <h4 class="text-white bg-dark">Pertanyaan & Saran</h4>
                        <input type="hidden" name="saran" value="<?= set_value('saran') ?>">
                        <?php if (isset($Nilai) && $this->session->userdata['Id'] == $Nilai['Dosen1Id']) : ?>
                            <div id="quillEditor" style="min-height: 100px;"><?php
                                if ($Nilai['Dsn1Saran'] != null){
                                    echo strip_tags($Nilai['Dsn1Saran']);
                                }else{
                                    echo "";
                                }
                                ?></div>
                        <?php elseif (isset($Nilai) && $this->session->userdata['Id'] == $Nilai['Dosen2Id']) : ?>
                            <div id="quillEditor" style="min-height: 100px;"><?php
                                if ($Nilai['Dsn2Saran'] != null){
                                    echo strip_tags($Nilai['Dsn2Saran']);
                                }else{
                                    echo "";
                                }
                                ?></div>
                        <?php elseif (isset($Nilai) && $this->session->userdata['Id'] == $Nilai['Dosen3Id']) : ?>
                            <div id="quillEditor" style="min-height: 100px;"><?php
                                if ($Nilai['Dsn3Saran'] != null){
                                    echo strip_tags($Nilai['Dsn3Saran']);
                                }else{
                                    echo "";
                                }
                                ?></div>
                        <?php else : ?>
                            <div id="quillEditor" style="min-height: 100px;"><?= set_value('saran') ?></div>
                        <?php endif; ?>
                    </div>
                    <div class="text-right mr-5 mb-3">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                        <button class="btn btn-primary" type="submit">Submit</button>
                    </div>
                </form> 
            </div>
        </div>
    </div>
    <?php endif; ?>

    <!-- Card Tampil Saran -->
    <?php 
        $mhs = [0,2,3,4];
        if (in_array($this->session->userdata('RoleId'), $mhs)):
    ?>
    <div class="card">
        <div class="row mt-3 ml-2 mr-2">
            <div class="col-xl-4 col-md-6 mb-4">
                <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                    Saran dan Kritik Dosen 1 :</div>
                                <?= $Dsn1Saran; ?>
                            </div>
                            <div class="col-auto">
                                <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6 mb-4">
                <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                    Saran dan Kritik Dosen 2 :</div>
                                <?= $Dsn2Saran; ?>
                            </div>
                            <div class="col-auto">
                                <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6 mb-4">
                <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                    Saran dan Kritik Dosen 3 :</div>
                                <?= $Dsn3Saran; ?>
                            </div>
                            <div class="col-auto">
                                <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php endif; ?>

    <!-- Card Nilai -->
    <div class="card">
        <?php if ($TipeDosen == 1) :?>
        <div class="row mt-3 ml-2 mr-2">
            <div class="col-12">
                <div class="row ml-2">
                    <h4 class="text-white bg-dark">Dosen 1</h4>
                </div>
                <div class="row">
                    <div class="col-xl-4 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Nilai Presentasi</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $totalNilaiPresentasiDsn1 ?></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Nilai Naskah</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $totalNilaiNaskahDsn1 ?></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Nilai Dosen</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $totalNilaiSemproDsn1TA ?></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php elseif ($TipeDosen == 2) :?>
        <div class="row mt-3 ml-2 mr-2">
            <div class="col-12">
                <div class="row ml-2">
                    <h4 class="text-white bg-dark">Dosen 2</h4>
                </div>
                <div class="row">
                    <div class="col-xl-4 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Nilai Presentasi</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $totalNilaiPresentasiDsn2 ?></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Nilai Naskah</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $totalNilaiNaskahDsn2 ?></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Nilai Dosen</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $totalNilaiSemproDsn2TA ?></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php elseif ($TipeDosen == 3) :?>
        <div class="row mt-3 ml-2 mr-2">
            <div class="col-12">
                <div class="row ml-2">
                    <h4 class="text-white bg-dark">Dosen 3</h4>
                </div>
                <div class="row">
                    <div class="col-xl-4 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Nilai Presentasi</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $totalNilaiPresentasiDsn3 ?></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Nilai Naskah</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $totalNilaiNaskahDsn3 ?></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Nilai Dosen</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $totalNilaiSemproDsn3TA ?></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
            elseif ($TipeDosen == 0 & in_array($this->session->userdata('RoleId'), [0,5])):
        ?>

        <div class="row mt-3 ml-2 mr-2">
            <div class="col-12">
                <div class="row ml-2">
                    <h4 class="text-white bg-dark">Dosen 1</h4>
                </div>
                <div class="row">
                    <div class="col-xl-4 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Nilai Presentasi</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $totalNilaiPresentasiDsn1 ?></div>
                                    </div>
                                    <div class="col-auto PresentasiSempro dosen1">
                                        <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Nilai Naskah</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $totalNilaiNaskahDsn1 ?></div>
                                    </div>
                                    <div class="col-auto NaskahSempro dosen1">
                                        <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Nilai Dosen 1</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $totalNilaiSemproDsn1TA ?></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mt-3 ml-2 mr-2">
            <div class="col-12">
                <div class="row ml-2">
                    <h4 class="text-white bg-dark">Dosen 2</h4>
                </div>
                <div class="row">
                    <div class="col-xl-4 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Nilai Presentasi</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $totalNilaiPresentasiDsn2 ?></div>
                                    </div>
                                    <div class="col-auto PresentasiSempro dosen2">
                                        <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Nilai Naskah</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $totalNilaiNaskahDsn2 ?></div>
                                    </div>
                                    <div class="col-auto NaskahSempro dosen2">
                                        <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Nilai Dosen 2</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $totalNilaiSemproDsn2TA ?></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mt-3 ml-2 mr-2">
            <div class="col-12">
                <div class="row ml-2">
                    <h4 class="text-white bg-dark">Dosen 3</h4>
                </div>
                <div class="row">
                    <div class="col-xl-4 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Nilai Presentasi</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $totalNilaiPresentasiDsn3 ?></div>
                                    </div>
                                    <div class="col-auto PresentasiSempro dosen3">
                                        <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Nilai Naskah</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $totalNilaiNaskahDsn3 ?></div>
                                    </div>
                                    <div class="col-auto NaskahSempro dosen3">
                                        <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Nilai Dosen 3</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $totalNilaiSemproDsn3TA ?></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php endif ;?>
        <?php 
            $mhs = [2,3,4];
            if(in_array($this->session->userdata('RoleId'),$mhs)):
        ?>
        <hr>
        <div class="row mt-3 ml-2 mr-2">
            <div class="col-xl-2 col-md-6 mb-4">
            </div>
            <div class="col-xl-4 col-md-6 mb-4">
                <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                    Rata-rata Nilai Presentasi Seminar Proposal</div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $averagePresentasi ?></div>
                            </div>
                            <div class="col-auto">
                                <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6 mb-4">
                <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                    Rata-Rata Nilai Naskah Seminar Proposal</div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $averageNaskah ?></div>
                            </div>
                            <div class="col-auto">
                                <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-2 col-md-6 mb-4">
            </div>
        </div>

        <?php endif; ?>
        <hr>
        <div class="row">
            <div class="col-xl-4 col-md-6 mb-4">
            </div>
            <div class="col-xl-4 col-md-6 mb-4">
                <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                    Nilai Akhir Seminar Proposal</div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $NilaiAkhirSemproTA ?></div>
                            </div>
                            <div class="col-auto">
                                <i class="fa-solid fa-square-poll-horizontal fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6 mb-4">
            </div>
        </div>

    </div>

    <!-- Nilai Presentasi -->
    <div class="modal fade" id="PresentasiSempro" role="dialog" aria-labelledby="exampleModalLabel"
        >
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Detail Nilai Presentasi Seminar Proposal</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                <div class="card">
                    <div class="card-body">
                    <div class="form-group row">
                        <label for="inputPresent1" class="col-sm-4 col-form-label">Tata bahasa dan susunan kalimat.</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputPresent1" name="Present1" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPresent2" class="col-sm-4 col-form-label">Kosa kata</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputPresent2" name="Present2" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPresent3" class="col-sm-4 col-form-label">Pengucapan</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputPresent3" name="Present3" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPresent4" class="col-sm-4 col-form-label">Penyampaian</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputPresent4" name="Present4" disabled>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Tutup</button>
                </div>
            </div>
            </div>
        </div>
    </div>
    </div>

     <!-- Nilai Naskah -->
     <div class="modal fade" id="NaskahSempro" role="dialog" aria-labelledby="exampleModalLabel"
        >
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Detail Nilai Naskah Seminar Proposal</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                <div class="card">
                    <div class="card-body">
                    <div class="form-group row">
                        <label for="inputNaskah1" class="col-sm-4 col-form-label">Menguraikan ide atau hasil pekerjaannya.</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputNaskah1" name="Naskah1" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputNaskah2" class="col-sm-4 col-form-label">Merumuskan masalah dan tujuan penelitian</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputNaskah2" name="Naskah2" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputNaskah3" class="col-sm-4 col-form-label">Menyusun langkah-langkah pengerjaan</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputNaskah3" name="Naskah3" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputNaskah4" class="col-sm-4 col-form-label">Cakupan penelitian</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputNaskah4" name="Naskah4" disabled>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Tutup</button>
                </div>
            </div>
            </div>
        </div>
    </div>
    </div>

</div>