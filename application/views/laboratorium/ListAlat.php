<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Filter -->
    <div class="card">
        <div class="row mt-3 ml-2 mb-0">
            <div class="col">
            <form method="post" action="<?= base_url('laboratorium/StokAlatLab'); ?>" enctype="multipart/form-data">
                <div class="form-row">
                    <div class="form-group col-md-2">
                        <div class="form-row">
                            <div class="form-group col">
                            <select id="limit" name="limit" class="form-control">
                                <option value="">Tampil Data</option>
                                <option value="10">10</option>
                                <option value="25">25</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-2">
                        <div class="form-row">
                            <div class="form-group col">
                                <input type="text" class="form-control" id="search" name="search" placeholder="Nama Alat">
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-2">
                        <button type="submit" class="btn btn-secondary">Search</button>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>

    <!-- Table Stok Alat -->
    <div class="card">
        <div class="row mt-3 ml-2 mr-2">
            <div class="col-4">
                <h4 class="text-white bg-dark">List Alat Kimia</h4>
            </div>
            <div class="col-6">
            </div>
            <div class="col-2">
                <div class="btn-group">
                    <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa-solid fa-school-flag"></i>
                        Tambah
                    </button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" data-toggle="modal" data-target="#tambahAlat">Tambah Alat</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row g-0 pb-3 pl-2 pr-2">
        <div class="card-body">
            <table class="table table-striped">
            <thead class="thead-dark">
                <tr>
                <th scope="col" width="50px">No</th>
                <th scope="col" width="500px">Nama</th>
                <th scope="col" width="500px">Merk</th>
                <th scope="col" width="500px">Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $i = 1;
                    foreach($ListAlat as $alat): 
                ?>
                <tr <?php echo ($alat['Status'] == 0) ? 'style="background-color:#FF7E62"':'style="background-color:#FFFFFF"' ?>>
                <th scope="row" ><?=$i?></th>
                <td class="AlatId" hidden><?=$alat['Id']?></td>
                <td>
                    <p><?= $alat['Nama'] ?></p>
                </td>
                <td>
                    <p><?= $alat['Merk'] ?></p>
                </td>
                <td>
                    <div class="btn-group">
                        <button type="button" class="btn btn-warning btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Aksi
                            <i class="fa-solid fa-sliders"></i>
                        </button>
                        <div class="dropdown-menu">
                            <div class="row mx-auto p-1">
                                <button type="button" class="btn btn-info editMasterAlat">Edit</button>
                            </div>
                        </div>
                    </div>
                </td>
                </tr>
                <?php 
                    $i++;
                    endforeach; 
                ?>
            </tbody>
            </table>
        </div>
    </div>

    <!-- Tambah Alat Modal-->
    <div class="modal fade" id="tambahAlat"  role="dialog" aria-labelledby="exampleModalLabel"
        >
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Alat</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" action="<?= base_url('laboratorium/TambahMasterAlat'); ?>" >
                    <div class="form-group row">
                        <label for="inputNama" class="col-sm-4 col-form-label">Nama Alat</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputNama" name="nama" placeholder="Nama" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputMerk" class="col-sm-4 col-form-label">Merk Alat</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputMerk" name="merk" placeholder="Merk" required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                    <button class="btn btn-primary" type="submit">Tambah</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Edit Master Alat Modal-->
    <div class="modal fade" id="editMasterAlat"  role="dialog" aria-labelledby="exampleModalLabel"
        >
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Alat</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form method="post" action="<?=base_url('laboratorium/EditMasterAlat')?>" >
                    <div class="modal-body">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group row">
                                    <label for="inputNamaAlat" class="col-sm-4 col-form-label">Nama Alat</label>
                                    <div class="col-sm-8">
                                    <input type="text" class="form-control" id="inputNamaAlat" name="alatId" hidden>
                                    <input type="text" class="form-control" id="inputNamaAlat" name="nama">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputMerk" class="col-sm-4 col-form-label">Merk</label>
                                    <div class="col-sm-8">
                                    <input type="text" class="form-control" id="inputMerk" name="merk">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputStatus" class="col-sm-4 col-form-label">Status</label>
                                    <div class="col-sm-8">
                                    <select id="inputStatus" name="status" class="form-control">
                                        <option value="1">Aktif</option>
                                        <option value="0">Tidak Aktif</option>
                                    </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                        <button class="btn btn-primary" type="submit">Edit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
