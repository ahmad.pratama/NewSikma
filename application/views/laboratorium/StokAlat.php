<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Filter -->
    <div class="card">
        <div class="row mt-3 ml-2 mb-0">
            <div class="col">
            <form method="post" action="<?= base_url('laboratorium/StokAlatLab'); ?>" enctype="multipart/form-data">
                <div class="form-row">
                    <div class="form-group col-md-2">
                        <div class="form-row">
                            <div class="form-group col">
                            <select id="limit" name="limit" class="form-control">
                                <option value="">Tampil Data</option>
                                <option value="10">10</option>
                                <option value="25">25</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-2">
                        <div class="form-row">
                            <div class="form-group col">
                                <input type="text" class="form-control" id="search" name="search" placeholder="Nama Alat">
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-2">
                        <button type="submit" class="btn btn-secondary">Search</button>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>

    <!-- Table Stok Alat -->
    <div class="card">
        <div class="row mt-3 ml-2 mr-2">
            <div class="col-4">
                <h4 class="text-white bg-dark">Riwayat Stok <?= $Alat['NamaAlat'] ?></h4>
            </div>
            <div class="col-6">
            </div>
            <div class="col-2">
                <div class="btn-group">
                    <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa-solid fa-school-flag"></i>
                        Tambah
                    </button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" data-toggle="modal" data-target="#tambahStokAlat">Tambah Stok Alat</a>
                        <a class="dropdown-item" data-toggle="modal" data-target="#pelaporanAlatRusak">Pelaporan Alat Rusak</a>
                        <!-- <a class="dropdown-item" data-toggle="modal" data-target="#importAlat">Import Alat</a> -->
                    </div>
            </div>
            </div>
        </div>
        <div class="row g-0 pb-3 pl-2 pr-2">
        <div class="card-body">
            <table class="table table-striped">
            <thead class="thead-dark">
                <tr>
                <th scope="col" width="50px">No</th>
                <th scope="col" width="500px">Tanggal</th>
                <th scope="col" width="500px">Keterangan</th>
                <th scope="col" width="500px">Jenis</th>
                <th scope="col" width="500px">Jumlah</th>
                <th scope="col" width="500px">Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $i = 1;
                    foreach($StokAlat as $stok): 
                ?>
                <tr>
                <?php if($stok['Id'] != null):?>
                    <th scope="row" ><?=$i?></th>
                    <td class="StokId" hidden><?=$stok['Id']?></td>
                    <td>
                        <p><?= $stok['Tanggal'] ?></p>
                    </td>
                    <td>
                        <p><?= $stok['Keterangan'] ?></p>
                    </td>
                    <td>
                        <p><?= $stok['JenisText'] ?></p>
                    </td>
                    <td>
                        <p><?= $stok['JumlahPerubah'] ?></p>
                    </td>
                    <td>
                        <div class="btn-group">
                            <button type="button" class="btn btn-warning btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Aksi
                                <i class="fa-solid fa-sliders"></i>
                            </button>
                            <div class="dropdown-menu">
                                <div class="row mx-auto p-1">
                                    <button type="button" class="btn btn-info detailStokAlat">Detil</button>
                                </div>
                                <?php if($stok['Id'] == $LastIdStok['Id'] && $stok['Jenis'] != 2): ?>
                                <div class="row mx-auto p-1">
                                    <button type="button" class="btn btn-info editStokAlat">Edit</button>
                                </div>
                                <?php endif; ?>
                                <?php if($stok['Jenis'] == 1 && $stok['Bukti'] != null && $stok['Bukti'] != "") :?>
                                    <div class="row mx-auto p-1">
                                        <a class="btn btn-success" href="<?= base_url('/assets/uploads/buktiPengadaanAlat/'. $stok['Bukti'])?>" target="_blank">Bukti <i class="fa-solid fa-fw fa-file-export"></i></a>
                                    </div>
                                <?php endif;?>
                            </div>
                        </div>
                    </td>
                <?php endif; ?>
                </tr>
                <?php 
                    $i++;
                    endforeach; 
                ?>
            </tbody>
            </table>
        </div>
    </div>

    <!-- Tambah Stok Alat Modal-->
    <div class="modal fade" id="tambahStokAlat"  role="dialog" aria-labelledby="exampleModalLabel"
        >
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Riwayat Stok Alat</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" action="<?= base_url('laboratorium/TambahStokAlatLab'); ?>" >
                    <div class="form-group row">
                        <label for="inputTanggalPengadaan" class="col-sm-4 col-form-label">Tanggal</label>
                        <div class="col-sm-8">
                        <input type="date" class="form-control" id="inputTanggalPengadaan" name="tanggal" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputKeterangan" class="col-sm-4 col-form-label">Keterangan</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputKeterangan" name="alatLabId" value="<?= $Alat['Id'] ?>" hidden>
                        <textarea name="keterangan" id="inputKeterangan" cols="33.5" rows="3" required></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputJumlah" class="col-sm-4 col-form-label">Jumlah</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputJumlah" name="jumlah" placeholder="Jumlah">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputBuktiPengadaanAlat" class="col-sm-4 col-form-label">Bukti</label>
                        <div class="col-sm-8">
                            <button class="btn btn-warning" type="button">
                                <label for="file-input">
                                    Upload
                                    <i class="fa-solid fa-fw fa-file-arrow-up"></i> 
                                </label>
                                <input type="file" id="file-input" name="buktiPengadaanAlat" style="display: none;" accept=".png,.jpg,.pdf">
                            </button>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                    <button class="btn btn-primary" type="submit">Tambah</button>
                </div>
                </form>
            </div>
        </div>
    </div>


    <!-- Pelaporan Alat Rusak Modal-->
    <div class="modal fade" id="pelaporanAlatRusak"  role="dialog" aria-labelledby="exampleModalLabel"
        >
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Pelaporan Alat Rusak</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" action="<?= base_url('laboratorium/TambahAlatLabRusak'); ?>" >
                    <div class="form-group row">
                        <label for="inputTanggalPengadaan" class="col-sm-4 col-form-label">Tanggal</label>
                        <div class="col-sm-8">
                        <input type="date" class="form-control" id="inputTanggalPengadaan" name="tanggal" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputJumlah" class="col-sm-4 col-form-label">Jumlah</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputKeterangan" name="alatLabId" value="<?= $Alat['Id'] ?>" hidden>
                        <input type="text" class="form-control" id="inputJumlah" name="jumlah" placeholder="Jumlah">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputBuktiPelaporanAlatRusak" class="col-sm-4 col-form-label">Bukti</label>
                        <div class="col-sm-8">
                            <button class="btn btn-warning" type="button">
                                <label for="file-input">
                                    Upload
                                    <i class="fa-solid fa-fw fa-file-arrow-up"></i> 
                                </label>
                                <input type="file" id="file-input" name="buktiPelaporanAlatRusak" style="display: none;" accept=".png,.jpg,.pdf">
                            </button>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                    <button class="btn btn-primary" type="submit">Tambah</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Detail Stok Alat Modal-->
    <div class="modal fade" id="detailStokAlat"  role="dialog" aria-labelledby="exampleModalLabel"
        >
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Detail Stok Alat</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group row">
                                <label for="inputNamaAlat" class="col-sm-4 col-form-label">Nama Alat</label>
                                <div class="col-sm-8">
                                <input type="text" class="form-control" id="inputNamaAlat" name="namaAlat" disabled>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputJenis" class="col-sm-4 col-form-label">Jenis</label>
                                <div class="col-sm-8">
                                <input type="text" class="form-control" id="inputJenis" name="jenis" value="Pengadaan" disabled>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputKeterangan" class="col-sm-4 col-form-label">Keterangan</label>
                                <div class="col-sm-8">
                                <input type="text" class="form-control" id="inputKeterangan" name="keterangan" disabled>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputJumlah" class="col-sm-4 col-form-label">Jumlah Awal</label>
                                <div class="col-sm-8">
                                <input type="text" class="form-control" id="inputJumlah" name="jumlahAwal" disabled>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputJumlah" class="col-sm-4 col-form-label">Jumlah Perubah</label>
                                <div class="col-sm-8">
                                <input type="text" class="form-control" id="inputJumlah" name="jumlahPerubah" disabled>
                                </div>
                            </div>
                            <!-- <div class="form-group row">
                                <label for="inputJumlah" class="col-sm-4 col-form-label">Total Stok</label>
                                <div class="col-sm-8">
                                <input type="text" class="form-control" id="inputJumlah" name="totalStok" disabled>
                                </div>
                            </div> -->
                            <div class="form-group row">
                                <label for="inputTanggal" class="col-sm-4 col-form-label">Tanggal</label>
                                <div class="col-sm-8">
                                <input type="text" class="form-control" id="inputTanggal" name="tanggal" disabled>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Edit Stok Alat Modal-->
    <div class="modal fade" id="editStokAlat"  role="dialog" aria-labelledby="exampleModalLabel"
        >
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Detail Stok Alat</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form method="post" action="<?=base_url('laboratorium/EditStokAlatLab')?>" >
                    <div class="modal-body">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group row">
                                    <label for="inputNamaAlat" class="col-sm-4 col-form-label">Nama Alat</label>
                                    <div class="col-sm-8">
                                    <input type="text" class="form-control" id="inputNamaAlat" name="pengadaanId" hidden>
                                    <input type="text" class="form-control" id="inputNamaAlat" name="stokId" hidden>
                                    <input type="text" class="form-control" id="inputNamaAlat" name="namaAlat" disabled>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputJenis" class="col-sm-4 col-form-label">Jenis</label>
                                    <div class="col-sm-8">
                                    <input type="text" class="form-control" id="inputJenis" name="jenis" value="Pengadaan" disabled>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputKeterangan" class="col-sm-4 col-form-label">Keterangan</label>
                                    <div class="col-sm-8">
                                    <input type="text" class="form-control" id="inputKeterangan" name="keterangan">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputJumlah" class="col-sm-4 col-form-label">Jumlah</label>
                                    <div class="col-sm-8">
                                    <input type="text" class="form-control" id="inputJumlah" name="jumlah">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputBuktiPengadaanAlat" class="col-sm-4 col-form-label">Bukti</label>
                                    <div class="col-sm-8">
                                        <button class="btn btn-warning" type="button">
                                            <label for="file-input">
                                                Upload
                                                <i class="fa-solid fa-fw fa-file-arrow-up"></i> 
                                            </label>
                                            <input type="file" id="file-input" name="buktiPengadaanAlat" style="display: none;" accept=".png,.jpg,.pdf">
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                        <button class="btn btn-primary" type="submit">Edit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
