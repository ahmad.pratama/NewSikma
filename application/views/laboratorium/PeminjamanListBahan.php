<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Table Peminjaman Bahan -->
    <div class="card">
        <div class="row mt-3 ml-2 mr-2">
            <div class="col-4">
                <h4 class="text-white bg-dark">Detail Peminjaman Bahan</h4>
            </div>
            <div class="col-6">
            </div>
            <div class="col-2">
                <?php 
                $mhs = [2,3,4];
                if(in_array($User['RoleId'], $mhs)) :
                ?>
                <div class="btn-group">
                    <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa-solid fa-school-flag"></i>
                        Tambah
                    </button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" data-toggle="modal" data-target="#tambahPeminjamanBahan">Tambah Bahan</a>
                    </div>
                </div>
                <?php endif; ?>
            </div>
        </div>
        <div class="row g-0 pb-3 pl-2 pr-2">
        <div class="card-body">
            <table class="table table-striped">
            <thead class="thead-dark">
                <tr>
                <th scope="col" width="50px">No</th>
                <th scope="col" width="500px">Nama Bahan</th>
                <th scope="col" width="500px">Nama Lab</th>
                <th scope="col" width="500px">Jumlah</th>
                <th scope="col" width="500px">Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $i = 1;
                    $currentLabId = null;
                    foreach($PeminjamanListBahan as $listBahan): 
                        if ($currentLabId !== $listBahan['LabId']) {
                            echo '<tr class="table-secondary">';
                            echo '<td colspan="4"><strong>Lab ' . $listBahan['NamaLab'] . '</strong></td>';
                            echo '<td>';
                            // echo '<form action="'. base_url("laboratorium/DownloadFormulirBonBahan") .'" method="post">';
                            // echo '<input type="hidden" name="ListBahanId" value="'.$listBahan['ListBahanId'].'">';
                            // echo '<input type="hidden" name="LabId" value="'.$listBahan['LabId'].'">';
                            // echo '<input type="hidden" name="PeminjamanId" value="'.$listBahan['PeminjamanId'].'">';
                            // echo '<button type="submit" class="btn btn-link">Unduh Bon Bahan ' . $listBahan['NamaLab'] . '</button>';
                            echo '</form>';
                            echo '</td>';
                            echo '</tr>';
                            $currentLabId = $listBahan['LabId'];
                        }
                ?>
                <tr>
                <th scope="row" ><?=$i?></th>
                <td class="ListBahanId" hidden><?=$listBahan['ListBahanId']?></td>
                <td class="LabId" hidden><?=$listBahan['LabId']?></td>
                <td class="PeminjamanId" hidden><?=$listBahan['PeminjamanId']?></td>
                <td>
                    <p><?= $listBahan['NamaBahan'] ?></p>
                </td>
                <td>
                    <p><?= $listBahan['NamaLab'] ?></p>
                </td>
                <td>
                    <p><?= $listBahan['Jumlah'] ?></p>
                </td>
                <td>
                    <?php 
                    $acc = [0,2,3,4,6];
                    if(in_array($User['RoleId'], $acc)):
                    ?>
                    <div class="btn-group">
                        <button type="button" class="btn btn-warning btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Aksi
                            <i class="fa-solid fa-sliders"></i>
                        </button>
                        <div class="dropdown-menu">
                            <div class="row mx-auto p-1">
                                <button type="button" class="btn btn-info editPeminjamanListBahan" <?= ($listBahan['Status'] == 1) ? "disabled" : "" ; ?>>Edit</button>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                </td>
                </tr>
                <?php 
                    $i++;
                    endforeach; 
                ?>
            </tbody>
            </table>
        </div>
    </div>

    <!-- Tambah Peminjaman Bahan Modal-->
    <div class="modal fade" id="tambahPeminjamanBahan"  role="dialog" aria-labelledby="exampleModalLabel"
        >
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Riwayat Peminjaman Bahan</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form method="post" action="<?= base_url('laboratorium/TambahPeminjamanListBahan'); ?>" >
                <div class="modal-body">
                    <div class="from-group row">
                        <input type="text" name="peminjamanId" value="<?= $PeminjamanId ?>" hidden>
                        <table class="table table-striped" id="dynamicTable">
                            <colgroup>
                                <col style="width: 8%;">
                                <col style="width: 24%;">
                                <col style="width: 24%;">
                                <col style="width: 16%;">
                                <col style="width: 16%;">
                                <col style="width: 8%;">
                            </colgroup>
                            <thead>
                                <tr>
                                    <th scope="col">No</th>
                                    <th scope="col">Nama Bahan</th>
                                    <th scope="col">Nama Lab</th>
                                    <th scope="col">Stok</th>
                                    <th scope="col">Kebutuhan</th>
                                    <th scope="col">Tambah</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row" class="nomor">1</th>
                                    <td class="select-bahan">
                                        <select name="bahan[]" class="form-control select2 bahan-dropdown" disabled>
                                            <option value="" selected>Select an option</option>
                                            <?php
                                            $previousBahanId = '';
                                            foreach ($ListBahanLab as $item) {
                                                if ($previousBahanId != $item['BahanId']) {
                                                    echo "<option value='{$item['BahanId']}' data-bahan-lab-id='{$item['BahanLabId']}'>{$item['NamaBahan']}</option>";
                                                    $previousBahanId = $item['BahanId'];
                                                }
                                            }
                                            ?>
                                        </select>
                                    </td>
                                    <td class="select-lab">
                                        <select name="lab[]" class="form-control select2 lab-dropdown" disabled></select>
                                    </td>
                                    <td class="stokBahan">
                                        <input type="text" name="stokBahan" class="form-control" disabled>
                                    </td>
                                    <td class="stok">
                                        <input type="text" name="stok[]" class="form-control" disabled>
                                    </td>
                                    <td><button type="button" class="btn btn-success add-row"><i class="fas fa-plus"></i></button></td>
                                    <!-- Hidden input to store BahanLabId -->
                                    <input type="hidden" name="bahanLabId[]">
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                    <button class="btn btn-primary" type="submit">Tambah</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Edit List Bahan Modal-->
    <div class="modal fade" id="editPeminjamanListBahan"  role="dialog" aria-labelledby="exampleModalLabel"
        >
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Bahan</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form method="post" action="<?=base_url('laboratorium/EditPeminjamanListBahan')?>" >
                    <div class="modal-body">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group row">
                                    <label for="inputNamaBahan" class="col-sm-4 col-form-label">Nama Bahan</label>
                                    <div class="col-sm-8">
                                    <input type="text" class="form-control" id="inputNamaBahan" name="listBahanId" hidden>
                                    <input type="text" class="form-control" id="inputNamaBahan" name="peminjamanId" hidden>
                                    <input type="text" class="form-control" id="inputNamaBahan" name="bahanLabId" hidden>
                                    <input type="text" class="form-control" id="inputNamaBahan" name="namaBahan" disabled>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputNamaLab" class="col-sm-4 col-form-label">Nama Lab</label>
                                    <div class="col-sm-8">
                                    <input type="text" class="form-control" id="inputNamaLab" name="namaLab" disabled>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputJumlah" class="col-sm-4 col-form-label">Jumlah</label>
                                    <div class="col-sm-8">
                                    <input type="text" class="form-control" id="inputJumlah" name="jumlah">
                                    </div>
                                </div>
                                <?php 
                                $tendik = [0,6];
                                if(in_array($User['RoleId'], $tendik)):
                                ?>
                                <div class="form-group row">
                                    <label for="inputStatus" class="col-sm-4 col-form-label">Status</label>
                                    <div class="col-sm-8">
                                    <input type="text" class="form-control" id="inputStatus" name="adminId" value="<?= $User['Id'] ?>" hidden>
                                    <select id="inputStatus" name="status" class="form-control">
                                        <option value="1">Selesai</option>
                                        <option value="0">Masih digunakan</option>
                                    </select>
                                    </div>
                                </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                        <button class="btn btn-primary" type="submit">Edit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
