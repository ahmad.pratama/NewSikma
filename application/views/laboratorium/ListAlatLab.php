<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Filter -->
    <div class="card">
        <div class="row mt-3 ml-2 mb-0">
            <div class="col">
            <form method="post" action="<?= base_url('laboratorium/ListAlatLab'); ?>" enctype="multipart/form-data">
                <div class="form-row">
                    <div class="form-group col-md-2">
                        <div class="form-row">
                            <div class="form-group col">
                            <select id="limit" name="limit" class="form-control">
                                <option value="">Tampil Data</option>
                                <option value="10">10</option>
                                <option value="25">25</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-2">
                        <div class="form-row">
                            <div class="form-group col">
                                <input type="text" class="form-control" id="search" name="search" placeholder="Nama Alat">
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-2">
                        <button type="submit" class="btn btn-secondary">Search</button>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>

    <!-- Table Alat -->
    <div class="card">
        <div class="row mt-3 ml-2 mr-2">
            <div class="col-4">
                <h4 class="text-white bg-dark">Data Alat Laboratorium</h4>
            </div>
            <div class="col-6">
            </div>
            <div class="col-2">
                <?php 
                $acc = [0,6];
                foreach($Kalab as $kalab):
                    if ($User['Id'] == $kalab['KalabId'] || in_array($User['RoleId'], $acc)):
                ?>
                <div class="btn-group">
                    <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa-solid fa-school-flag"></i>
                        Tambah
                    </button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" data-toggle="modal" data-target="#tambahAlat">Tambah Alat</a>
                    </div>
                </div>
                <?php 
                    break;
                    endif;
                endforeach; 
                ?>
            </div>
        </div>
        <div class="row g-0 pb-3 pl-2 pr-2">
        <div class="card-body">
            <table class="table table-striped">
            <thead class="thead-dark">
                <tr>
                <th scope="col" width="50px">No</th>
                <th scope="col" width="500px">Nama</th>
                <th scope="col" width="500px">Stok Permanen</th>
                <th scope="col" width="500px">Stok</th>
                <th scope="col" width="500px">Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $i = 1;
                    foreach($AlatLab as $alat): 
                ?>
                <tr <?php echo ($alat['Status'] == 0) ? 'style="background-color:#FF7E62"':'style="background-color:#FFFFFF"' ?>>
                <th scope="row" ><?=$i?></th>
                <td class="AlatLabId" hidden><?=$alat['Id']?></td>
                <td>
                    <p><?= $alat['NamaAlat'] ?></p>
                </td>
                <td>
                    <p><?= $alat['StokPermanen'] ?></p>
                </td>
                <td>
                    <p><?= $alat['Stok'] ?></p>
                </td>
                <td>
                    <div class="btn-group">
                        <?php 
                        $acc = [0,6];
                        foreach($Kalab as $kalab):
                            if ($User['Id'] == $kalab['KalabId']  || in_array($User['RoleId'], $acc)):
                        ?>
                        <button type="button" class="btn btn-warning btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Aksi
                            <i class="fa-solid fa-sliders"></i>
                        </button>
                        <div class="dropdown-menu">
                            <!-- <div class="row mx-auto p-1">
                                <button type="button" class="btn btn-info detailSemju">Detil</button>
                            </div> -->
                            <div class="row mx-auto p-1">
                                <button type="button" class="btn btn-info editAlatLab">Edit</button>
                            </div>
                            <div class="row mx-auto p-1">
                                <button type="button" class="btn btn-info chartAlatLab">Chart</button>
                            </div>
                            <div class="row mx-auto p-1">
                                    <a type="button" class="btn btn-warning" href="<?= base_url('Laboratorium/StokAlatLab/'). $alat['Id'] ?>">Stok</a>
                            </div>
                        </div>
                        <?php 
                            break;
                            endif;
                        endforeach; 
                        ?>
                    </div>
                </td>
                </tr>
                <?php 
                    $i++;
                    endforeach; 
                ?>
            </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Tambah Alat Modal-->
<div class="modal fade" id="tambahAlat"  role="dialog" aria-labelledby="exampleModalLabel"
    >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Alat</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="<?= base_url('laboratorium/TambahAlatLab'); ?>" >
                <div class="form-group row">
                    <label for="inputNama" class="col-sm-4 col-form-label">Nama Alat</label>
                    <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputNama" name="labId" value="<?= $LabId ?>" hidden>
                    <select name="alat" id="MasterAlat">
                        <?php foreach ($ListAlat as $alat) :?>
                            <option value="<?= $alat['Id']?>"><?= $alat['Nama']?></option>
                        <?php endforeach; ?>
                    </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputStok" class="col-sm-4 col-form-label">Stok Awal</label>
                    <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputStok" name="stok" placeholder="Stok">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Tambah</button>
            </div>
            </form>
        </div>
    </div>
</div>


<!-- Edit Alat Lab Model -->
<div class="modal fade" id="editAlatLab"  role="dialog" aria-labelledby="exampleModalLabel"
    >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Alat Laboratorium</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
            <form method="post" action="<?=base_url('Laboratorium/EditAlatLab')?>" >
            <div class="card">
                <div class="card-body">
                    <div class="form-group row">
                        <label for="inputNama" class="col-sm-4 col-form-label">Nama Alat</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputSemester" name="alatLabId" hidden>
                        <input type="text" class="form-control" id="inputSemester" name="labId" hidden>
                        <input type="text" class="form-control" id="inputNama" name="nama" placeholder="Nama" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputStatus" class="col-sm-4 col-form-label">Status</label>
                        <div class="col-sm-8">
                        <select id="inputStatus" name="status" class="form-control">
                            <option value="1">Aktif</option>
                            <option value="0">Tidak Aktif</option>
                        </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Edit</button>
            </div>
            </form>
        </div>
    </div>
    </div>
</div>

<!-- Char Modal -->
<div class="modal fade" id="chartAlatLab"  role="dialog" aria-labelledby="exampleModalLabel"
    >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Chart Alat Laboratorium</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
            <div class="card">
                <div class="card-body">
                    <canvas id="donutChart" width="400" height="400"></canvas>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
    </div>
</div>

<!-- End of Main Content -->