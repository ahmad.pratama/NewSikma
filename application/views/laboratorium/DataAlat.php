<div class="container-fluid">
    <!-- Filter -->
    <div class="card">
        <div class="row mt-3 ml-2 mb-0">
            <div class="col">
            <form method="post" action="<?= base_url('laboratorium/DataAlat'); ?>" enctype="multipart/form-data">
                <div class="form-row">
                    <div class="form-group col-md-2">
                        <input type="text" class="form-control" id="search" name="search" placeholder="Nama Laboratorium">
                    </div>
                    <div class="form-group col-md-2">
                        <button type="submit" class="btn btn-secondary">Cari</button>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
    
    <!-- card Lab -->
    <div class="card">
        <div class="row mt-3 ml-2 mr-2">
            <div class="col-3">
                <h4 class="text-white bg-dark">Data Laboratorium</h4>
            </div>
            <div class="col-7">
            </div>
            <div class="col-2">
                <?php 
                    if(($User['RoleId'] == 0) || (in_array(true, [
                        $this->session->userdata('Kadep'),
                        $this->session->userdata('Sekdep'),
                        $this->session->userdata('KaprodiS1'),
                        $this->session->userdata('KaprodiS2'),
                        $this->session->userdata('KaprodiS3'),
                    ]))):
                ?>
                <a class="btn btn-success" href="<?=base_url('laboratorium/ListMasterAlat');?>">Data Alat</a>
                <?php endif; ?>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
            <?php foreach ($Laboratorium as $lab): ?>
                <?php 
                    if(($User['RoleId'] == 0) || (in_array(true, [
                        $this->session->userdata('Kadep'),
                        $this->session->userdata('Sekdep'),
                        $this->session->userdata('KaprodiS1'),
                        $this->session->userdata('KaprodiS2'),
                        $this->session->userdata('KaprodiS3'),
                    ])) || ($User['Laboratorium'] == $lab['Id'])):
                ?>
                <?php if ($lab['Id'] != 7) :?>
                <div class="card text-center" style="width: 20rem;">
                    <a href="<?= base_url('laboratorium/ListAlatLab/'.$lab['Id']); ?>">
                        <div class="card-body">
                            <h5 class="card-title"><?= $lab['Nama'] ?></h5>
                            <button class="btn btn-primary">
                                Detail
                            </button>
                        </div>
                    </a>
                </div>
                <?php endif; ?>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>
    </div>
    </div>
</div>
