<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Filter -->
    <div class="card">
        <div class="row mt-3 ml-2 mb-0">
            <div class="col">
            <form method="post" action="<?= base_url('laboratorium/PeminjamanAlat'); ?>" enctype="multipart/form-data">
                <div class="form-row">
                    <div class="form-group col-md-2">
                        <div class="form-row">
                            <div class="form-group col">
                            <select id="limit" name="limit" class="form-control">
                                <option value="">Tampil Data</option>
                                <option value="10">10</option>
                                <option value="25">25</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-2">
                        <div class="form-row">
                            <div class="form-group col">
                                <input type="text" class="form-control" id="search" name="search" placeholder="Nama Alat">
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-2">
                        <button type="submit" class="btn btn-secondary">Search</button>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>

    <!-- Table Peminjaman Alat -->
    <div class="card">
        <div class="row mt-3 ml-2 mr-2">
            <div class="col-4">
                <h4 class="text-white bg-dark">Peminjaman Alat</h4>
            </div>
            <div class="col-6">
            </div>
            <div class="col-2">
                <?php 
                $mhs = [2,3,4];
                if(in_array($User['RoleId'], $mhs)) :
                ?>
                <div class="btn-group">
                    <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa-solid fa-school-flag"></i>
                        Tambah
                    </button>
                    <div class="dropdown-menu">
                        <!-- <a class="dropdown-item" data-toggle="modal" data-target="#tambahPeminjamanAlat">Buat Peminjaman</a> -->
                        <a class="dropdown-item" data-toggle="modal" data-target="#tambahPeminjamanAlatManual">Pengajuan Manual</a>
                    </div>
                </div>
                <?php endif; ?>
            </div>
        </div>
        <div class="row g-0 pb-3 pl-2 pr-2">
        <div class="card-body">
            <table class="table table-striped">
            <thead class="thead-dark">
                <tr>
                <th scope="col" width="50px">No</th>
                <th scope="col" width="500px">Nama</th>
                <th scope="col" width="500px">Tujuan</th>
                <th scope="col" width="500px">Keterangan</th>
                <th scope="col" width="500px">Tanggal Peminjaman</th>
                <th scope="col" width="500px">Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $i = 1;
                    foreach($PeminjamAlat as $peminjaman): 
                ?>
                <tr>
                <?php if($peminjaman['Id'] != null):?>
                    <th scope="row" ><?=$i?></th>
                    <td class="PeminjamanId" hidden><?=$peminjaman['Id']?></td>
                    <td>
                        <p><?= $peminjaman['NamaMahasiswa'] ?></p>
                    </td>
                    <td>
                        <p><?= $peminjaman['Tujuan'] ?></p>
                    </td>
                    <td>
                        <p><?= $peminjaman['Keterangan'] ?></p>
                    </td>
                    <td>
                        <p><?= $peminjaman['Tanggal'] ?></p>
                    </td>
                    <td>
                        <div class="btn-group">
                            <button type="button" class="btn btn-warning btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Aksi
                                <i class="fa-solid fa-sliders"></i>
                            </button>
                            <div class="dropdown-menu">
                                <div class="row mx-auto p-1">
                                    <a type="button" class="btn btn-warning" href="<?= base_url('Laboratorium/PeminjamanListAlat/'). $peminjaman['Id'] ?>">Detil</a>
                                </div>
                            </div>
                        </div>
                    </td>
                <?php endif; ?>
                </tr>
                <?php 
                    $i++;
                    endforeach; 
                ?>
            </tbody>
            </table>
        </div>
    </div>

    <!-- Tambah Peminjaman Alat Modal-->
    <div class="modal fade" id="tambahPeminjamanAlat"  role="dialog" aria-labelledby="exampleModalLabel"
        >
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Riwayat Peminjaman Alat</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" action="<?= base_url('laboratorium/TambahPeminjamanAlat'); ?>" >
                    <div class="form-group row">
                        <label for="inputTanggalPengadaan" class="col-sm-4 col-form-label">Tanggal</label>
                        <div class="col-sm-8">
                        <input type="date" class="form-control" id="inputTanggalPengadaan" name="tanggal" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputKeterangan" class="col-sm-4 col-form-label">Keterangan</label>
                        <div class="col-sm-8">
                        <textarea name="keterangan" id="inputKeterangan" cols="50" rows="3" required></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="TujuanPeminjamanAlat" class="col-sm-4 col-form-label">Tujuan</label>
                        <div class="col-sm-8">
                            <select id="TujuanPeminjamanAlat" name="tujuan" class="form-control" required>
                                <option value="">Select an option</option>
                                <?php 
                                    $mhs = [2,3,4];
                                    if(in_array($User['RoleId'], $mhs)):
                                ?>
                                <option value="1">Penelitian Dosen</option>
                                <?php endif; ?>
                                <option value="2">Proyek Kimia</option>
                                <option value="3">Program Kreatifitas Mahasiswa</option>
                                <option value="4">Penelitian Skripsi</option>
                                <option value="5">Penelitian Tesis</option>
                                <option value="6">Penelitian Disertasi</option>
                                <option value="7">Analisis Sampel</option>
                                <option value="8">Lainnya</option>
                            </select>
                        </div>
                    </div>
                    <div id="CardTujuanPeminjamanAlatLainnya" style="display: none;">
                        <div class="form-group row">
                            <label for="inputTujuanLainnya" class="col-sm-4 col-form-label">Tujuan Lainnya</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="inputTujuanLainnya" name="tujuanLainnya">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                    <table class="table table-striped" id="dynamicTable">
                        <colgroup>
                            <col style="width: 8%;">
                            <col style="width: 24%;">
                            <col style="width: 24%;">
                            <col style="width: 16%;">
                            <col style="width: 16%;">
                            <col style="width: 8%;">
                        </colgroup>
                        <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Nama Alat</th>
                                <th scope="col">Nama Lab</th>
                                <th scope="col">Stok</th>
                                <th scope="col">Kebutuhan</th>
                                <th scope="col">Tambah</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="row" class="nomor">1</th>
                                <td class="select-alat">
                                    <select name="alat[]" class="form-control select2 alat-dropdown" disabled>
                                        <option value="" selected>Select an option</option>
                                        <?php
                                        $previousAlatId = '';
                                        foreach ($ListAlatLab as $item) {
                                            if ($previousAlatId != $item['AlatId']) {
                                                echo "<option value='{$item['AlatId']}' data-alat-lab-id='{$item['AlatLabId']}'>{$item['NamaAlat']}</option>";
                                                $previousAlatId = $item['AlatId'];
                                            }
                                        }
                                        ?>
                                    </select>
                                </td>
                                <td class="select-lab">
                                    <select name="lab[]" class="form-control select2 lab-dropdown" disabled></select>
                                </td>
                                <td class="stokAlat">
                                    <input type="text" name="stokAlat" class="form-control" disabled>
                                </td>
                                <td class="stok">
                                    <input type="text" name="stok[]" class="form-control" disabled>
                                </td>
                                <td><button type="button" class="btn btn-success add-row"><i class="fas fa-plus"></i></button></td>
                                <!-- Hidden input to store alatLabId -->
                                <input type="hidden" name="alatLabId[]">
                            </tr>
                        </tbody>
                    </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                    <button class="btn btn-primary" type="submit">Tambah</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Peminjaman Manual -->
    <div class="modal fade" id="tambahPeminjamanAlatManual"  role="dialog" aria-labelledby="exampleModalLabel"
        >
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Peminjaman Alat Manual</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" action="<?= base_url('laboratorium/PengajuanAlatManual'); ?>" >
                    <div class="form-group row">
                        <label for="inputStatus" class="col-sm-4 col-form-label">Status</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputName" name="mhsId" value="<?=$User['Id']?>" hidden>
                            <input type="text" class="form-control" id="inputStatus" name="status" placeholder="Mahasiswa S1">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputProgramStudi" class="col-sm-4 col-form-label">Program Studi</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputProgramStudi" name="programStudi" placeholder="Kimia Fakultas MIPA Universitas Brawijaya">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputLab" class="col-sm-4 col-form-label">Laboratorium</label>
                        <div class="col-sm-8">
                        <select id="Lab" name="lab" required>
                            <option value="">Select an option</option>
                            <?php foreach ($Laboratorium as $lab): ?>
                                <option value="<?= $lab['Id'] ?>"><?= $lab['Nama'] ?></option>
                            <?php endforeach; ?>
                        </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                    <button class="btn btn-primary" type="submit">Tambah</button>
                </div>
                </form>
            </div>
        </div>
    </div>

</div>
