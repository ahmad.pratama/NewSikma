<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Table Peminjaman Alat -->
    <div class="card">
        <div class="row mt-3 ml-2 mr-2">
            <div class="col-4">
                <h4 class="text-white bg-dark">Detail Peminjaman Alat</h4>
            </div>
            <div class="col-6">
            </div>
            <div class="col-2">
                <?php 
                $mhs = [2,3,4];
                if(in_array($User['RoleId'], $mhs)) :
                ?>
                <div class="btn-group">
                    <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa-solid fa-school-flag"></i>
                        Tambah
                    </button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" data-toggle="modal" data-target="#tambahPeminjamanAlat">Tambah Alat</a>
                    </div>
                </div>
                <?php endif; ?>
            </div>
        </div>
        <div class="row g-0 pb-3 pl-2 pr-2">
        <div class="card-body">
            <table class="table table-striped">
            <thead class="thead-dark">
                <tr>
                <th scope="col" width="50px">No</th>
                <th scope="col" width="500px">Nama Alat</th>
                <th scope="col" width="500px">Nama Lab</th>
                <th scope="col" width="500px">Jumlah</th>
                <th scope="col" width="500px">Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $i = 1;
                    $currentLabId = null;
                    foreach($PeminjamanListAlat as $listAlat): 
                        if ($currentLabId !== $listAlat['LabId']) {
                            echo '<tr class="table-secondary">';
                            echo '<td colspan="4"><strong>Lab ' . $listAlat['NamaLab'] . '</strong></td>';
                            echo '<td>';
                            echo '<form action="'. base_url("laboratorium/DownloadFormulirBonAlat") .'" method="post">';
                            echo '<input type="hidden" name="ListAlatId" value="'.$listAlat['ListAlatId'].'">';
                            echo '<input type="hidden" name="LabId" value="'.$listAlat['LabId'].'">';
                            echo '<input type="hidden" name="PeminjamanId" value="'.$listAlat['PeminjamanId'].'">';
                            echo '<button type="submit" class="btn btn-link">Unduh Bon Alat ' . $listAlat['NamaLab'] . '</button>';
                            echo '</form>';
                            echo '</td>';
                            echo '</tr>';
                            $currentLabId = $listAlat['LabId'];
                        }
                ?>
                <tr>
                <th scope="row" ><?=$i?></th>
                <td class="ListAlatId" hidden><?=$listAlat['ListAlatId']?></td>
                <td class="LabId" hidden><?=$listAlat['LabId']?></td>
                <td class="PeminjamanId" hidden><?=$listAlat['PeminjamanId']?></td>
                <td>
                    <p><?= $listAlat['NamaAlat'] ?></p>
                </td>
                <td>
                    <p><?= $listAlat['NamaLab'] ?></p>
                </td>
                <td>
                    <p><?= $listAlat['Jumlah'] ?></p>
                </td>
                <td>
                    <?php 
                    $acc = [0,2,3,4,6];
                    if(in_array($User['RoleId'], $acc)):
                    ?>
                    <div class="btn-group">
                        <button type="button" class="btn btn-warning btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Aksi
                            <i class="fa-solid fa-sliders"></i>
                        </button>
                        <div class="dropdown-menu">
                            <div class="row mx-auto p-1">
                                <button type="button" class="btn btn-info editPeminjamanListAlat" <?= ($listAlat['Status'] == 1) ? "disabled" : "" ; ?>>Edit</button>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                </td>
                </tr>
                <?php 
                    $i++;
                    endforeach; 
                ?>
            </tbody>
            </table>
        </div>
    </div>

    <!-- Tambah Peminjaman Alat Modal-->
    <div class="modal fade" id="tambahPeminjamanAlat"  role="dialog" aria-labelledby="exampleModalLabel"
        >
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Riwayat Peminjaman Alat</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form method="post" action="<?= base_url('laboratorium/TambahPeminjamanListAlat'); ?>" >
                <div class="modal-body">
                    <div class="from-group row">
                        <input type="text" name="peminjamanId" value="<?= $PeminjamanId ?>" hidden>
                        <table class="table table-striped" id="dynamicTable">
                            <colgroup>
                                <col style="width: 8%;">
                                <col style="width: 24%;">
                                <col style="width: 24%;">
                                <col style="width: 16%;">
                                <col style="width: 16%;">
                                <col style="width: 8%;">
                            </colgroup>
                            <thead>
                                <tr>
                                    <th scope="col">No</th>
                                    <th scope="col">Nama Alat</th>
                                    <th scope="col">Nama Lab</th>
                                    <th scope="col">Stok</th>
                                    <th scope="col">Kebutuhan</th>
                                    <th scope="col">Tambah</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row" class="nomor">1</th>
                                    <td class="select-alat">
                                        <select name="alat[]" class="form-control select2 alat-dropdown" disabled>
                                            <option value="" selected>Select an option</option>
                                            <?php
                                            $previousAlatId = '';
                                            foreach ($ListAlatLab as $item) {
                                                if ($previousAlatId != $item['AlatId']) {
                                                    echo "<option value='{$item['AlatId']}' data-alat-lab-id='{$item['AlatLabId']}'>{$item['NamaAlat']}</option>";
                                                    $previousAlatId = $item['AlatId'];
                                                }
                                            }
                                            ?>
                                        </select>
                                    </td>
                                    <td class="select-lab">
                                        <select name="lab[]" class="form-control select2 lab-dropdown" disabled></select>
                                    </td>
                                    <td class="stokAlat">
                                        <input type="text" name="stokAlat" class="form-control" disabled>
                                    </td>
                                    <td class="stok">
                                        <input type="text" name="stok[]" class="form-control" disabled>
                                    </td>
                                    <td><button type="button" class="btn btn-success add-row"><i class="fas fa-plus"></i></button></td>
                                    <!-- Hidden input to store alatLabId -->
                                    <input type="hidden" name="alatLabId[]">
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                    <button class="btn btn-primary" type="submit">Tambah</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Edit List Alat Modal-->
    <div class="modal fade" id="editPeminjamanListAlat"  role="dialog" aria-labelledby="exampleModalLabel"
        >
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Alat</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form method="post" action="<?=base_url('laboratorium/EditPeminjamanListAlat')?>" >
                    <div class="modal-body">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group row">
                                    <label for="inputNamaAlat" class="col-sm-4 col-form-label">Nama Alat</label>
                                    <div class="col-sm-8">
                                    <input type="text" class="form-control" id="inputNamaAlat" name="listAlatId" hidden>
                                    <input type="text" class="form-control" id="inputNamaAlat" name="peminjamanId" hidden>
                                    <input type="text" class="form-control" id="inputNamaAlat" name="alatLabId" hidden>
                                    <input type="text" class="form-control" id="inputNamaAlat" name="namaAlat" disabled>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputNamaLab" class="col-sm-4 col-form-label">Nama Lab</label>
                                    <div class="col-sm-8">
                                    <input type="text" class="form-control" id="inputNamaLab" name="namaLab" disabled>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputJumlah" class="col-sm-4 col-form-label">Jumlah</label>
                                    <div class="col-sm-8">
                                    <input type="text" class="form-control" id="inputJumlah" name="jumlah">
                                    </div>
                                </div>
                                <?php 
                                $tendik = [0,6];
                                if(in_array($User['RoleId'], $tendik)):
                                ?>
                                <div class="form-group row">
                                    <label for="inputStatus" class="col-sm-4 col-form-label">Status</label>
                                    <div class="col-sm-8">
                                    <input type="text" class="form-control" id="inputStatus" name="adminId" value="<?= $User['Id'] ?>" hidden>
                                    <select id="inputStatus" name="status" class="form-control">
                                        <option value="1">Selesai</option>
                                        <option value="0">Masih digunakan</option>
                                    </select>
                                    </div>
                                </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                        <button class="btn btn-primary" type="submit">Edit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
