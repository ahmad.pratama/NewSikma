<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Filter -->
    <?php 
    $mhs = [2,3,4];
    if(!in_array($User['RoleId'], $mhs)):?>
        <div class="card">
            <div class="row mt-3 ml-2 mb-0">
                <div class="col">
                <form method="post" action="<?= base_url('surat/SuratIjinLembur'); ?>" enctype="multipart/form-data">
                    <div class="form-row">
                        <div class="form-group col-md-2">
                            <div class="form-row">
                                <div class="form-group col">
                                <select id="jenjang" name="jenjang" class="form-control">
                                    <option value="" selected>Pilih Jenjang</option>
                                    <option value="2">S1</option>
                                    <option value="3">S2</option>
                                    <option value="4">S3</option>
                                </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-2">
                            <div class="form-row">
                                <div class="form-group col">
                                <select id="limit" name="limit" class="form-control">
                                    <option value="">Tampil Data</option>
                                    <option value="10">10</option>
                                    <option value="25">25</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-2">
                            <div class="form-row">
                                <div class="form-group col">
                                    <input type="text" class="form-control" id="search" name="search" placeholder="Nama / NIM">
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-2">
                            <button type="submit" class="btn btn-secondary">Search</button>
                        </div>
                    </div>
                </form>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <!-- Table Surat -->
    <div class="card">
        <div class="row mt-3 ml-2 mr-2">
            <div class="col-4">
                <h4 class="text-white bg-dark">Riwayat Permohonan Surat Ijin Lembur</h4>
            </div>
            <div class="col-5">
            </div>
            <div class="col-3">
                <?php 
                $mhs = [2,3,4];
                if(in_array($User['RoleId'], $mhs)):?>
                <div class="btn-group">
                    <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#PengajuanSuratIjinLembur">
                        <i class="fa-solid fa-fw fa-user-plus"></i>
                        Pengajuan Surat Ijin Lembur
                    </button>
                </div>
                <?php endif; ?>
            </div>
        </div>
        <div class="row g-0 pb-3 pl-2 pr-2">
        <div class="card-body">
            <table class="table table-striped">
            <thead class="thead-dark">
                <tr>
                <th scope="col" width="50px">No</th>
                <th scope="col" width="500px">Mahasiswa</th>
                <th scope="col" width="500px">Laboratorium</th>
                <th scope="col" width="500px">Rentang Waktu</th>
                <th scope="col" width="500px">Tanggal Pengajuan</th>
                <th scope="col" width="500px">Unduh</th>
                <th scope="col" width="500px">Unggah</th>
                <th scope="col" width="500px">Preview</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $i = 1;
                    foreach($SuratIjinLembur as $surat): 
                ?>
                <tr>
                <th scope="row"><?=$i?></th>
                <td class="SuratId" hidden><?=$surat['Id']?></td>
                <td>
                    <p>
                        <?= $surat["NamaMahasiswa"]?>
                    </p>
                </td>
                <td>
                    <p>
                        <?= $surat['NamaLab'] ?>
                    </p>
                </td>
                <td>
                    <p>
                        <?= $surat['JamMulai'] ?> s/d <?= $surat['JamSelesai'] ?>
                    </p>
                </td>
                <td>
                    <p>
                        <?= $surat["CreatedAt"]; ?>
                    </p>
                </td>
                <td>
                    <div class="row mx-auto p-1">
                        <a href="<?= base_url("surat/DownloadSuratIjinLembur/". $surat['Id']) ?>" target="_blank">
                            <button type="button" class="btn btn-success">
                                Download Surat Ijin Lembur
                                <i class="fa-solid fa-fw fa-file-export"></i>
                            </button>
                        </a>
                    </div>
                </td>
                <td>
                    <div class="row mx-auto p-1">
                        <div class="text-center">
                            <button class="btn btn-warning" data-toggle="modal" data-target="#uploadFileSuratLembur">Unggah File Surat Lembur</button>
                        </div>
                    </div>
                </td>
                <td>
                    <?php if($surat['FileSuratIjinLembur'] != null):?>
                        <div class="row mx-auto p-1">
                            <a class="btn" href="<?= base_url('/assets/uploads/suratLembur_lab/'. $surat['FileSuratIjinLembur']) ?>" target="_blank"><i class="fa-solid fa-fw fa-file-export fa-2x"></i></a>
                        </div>
                    <?php endif ?>
                </td>
                </tr>
                <?php 
                    $i++;
                    endforeach; 
                ?>
            </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Pengajuan Surat Ijin Lembur Modal-->
<div class="modal fade" id="PengajuanSuratIjinLembur" role="dialog" aria-labelledby="exampleModalLabel"
    >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Pengajuan Surat Ijin Lembur</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="<?= base_url('surat/PengajuanSuratIjinLembur'); ?>" >

                <div class="form-group row">
                    <label for="inputName" class="col-sm-4 col-form-label">Nama</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputName" name="mhsId" value="<?=$User['Id']?>" hidden>
                        <input type="text" class="form-control" id="inputName" placeholder="Name" name="name" value="<?=$User['Name']?>" disabled>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputUsername" class="col-sm-4 col-form-label">NIM</label>
                    <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputUsername" placeholder="NIM" name="username" value="<?=$User['Username']?>" disabled>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="TujuanSuratIjinKerjaLab" class="col-sm-4 col-form-label">Tujuan</label>
                    <div class="col-sm-8">
                        <select id="TujuanSuratIjinKerjaLab" name="tujuan" class="form-control" required onchange="updateMaxDate()">
                            <option value="">Select an option</option>
                            <?php 
                                $mhs = [2,3,4];
                                if(in_array($User['RoleId'], $mhs)):
                            ?>
                            <option value="1">Penelitian Dosen</option>
                            <?php endif; ?>
                            <option value="2">Proyek Kimia</option>
                            <option value="3">Program Kreatifitas Mahasiswa</option>
                            <option value="4">Penelitian Skripsi</option>
                            <option value="5">Penelitian Tesis</option>
                            <option value="6">Penelitian Disertasi</option>
                            <option value="7">Analisis Sampel</option>
                            <option value="8">Lainnya</option>
                        </select>
                    </div>
                </div>
                <div id="CardTujuanSuratIjinKerjaLabLainnya" style="display: none;">
                    <div class="form-group row">
                        <label for="inputTujuanLainnya" class="col-sm-4 col-form-label">Tujuan Lainnya</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputTujuanLainnya" name="tujuanLainnya">
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputLab" class="col-sm-4 col-form-label">Laboratorium</label>
                    <div class="col-sm-8">
                    <select id="Lab" name="lab" required>
                        <option value="">Select an option</option>
                        <?php foreach ($Lab as $lab): ?>
                            <option value="<?= $lab['Id'] ?>"><?= $lab['Nama'] ?></option>
                        <?php endforeach; ?>
                    </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputDosenPembimbing" class="col-sm-4 col-form-label">Dosen Pembimbing</label>
                    <div class="col-sm-8">
                    <select id="DosenPembimbing" name="dosen" required>
                        <option value="">Select an option</option>
                        <?php foreach ($Dosen as $dsn): ?>
                            <option value="<?= $dsn['Id'] ?>"><?= $dsn['Name'] ?></option>
                        <?php endforeach; ?>
                    </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputJudul" class="col-sm-4 col-form-label">Judul Penelitian</label>
                    <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputJudul" placeholder="Judul Penelitian" name="judul">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputTanggal" class="col-sm-4 col-form-label">Tanggal</label>
                    <div class="col-sm-8">
                        <input type="date" class="form-control" id="inputTanggal" name="tanggal" min="<?= date('Y-m-d', strtotime('+3 days')); ?>">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputtimeSelect" class="col-sm-4 col-form-label">Jam Mulai</label>
                    <div class="col-sm-8">
                    <select id="JamMulaiLembur" name="jamMulai" style="width: 100%;" required></select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputtimeSelect" class="col-sm-4 col-form-label">Jam Selesai</label>
                    <div class="col-sm-8">
                    <select id="JamSelesaiLembur" name="jamSelesai" style="width: 100%;" required></select>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Simpan</button>
            </div>
            </form>
        </div>
    </div>
</div>

            <!-- Upload Surat Lembur Modal-->
            <div class="modal fade" id="uploadFileSuratLembur"  role="dialog" aria-labelledby="exampleModalLabel"
                >
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Unggah File Surat Lembur</h5>
                            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                        <form method="post" action="<?= base_url('surat/UploadFileSuratLemburLab'); ?>" enctype="multipart/form-data">
                        <div class="form-group">
                            <div class="form-group row">
                                <div class="col-sm-2">File Surat</div>
                                <div class="col-sm-10">
                                    <div class="row">
                                        <input type="text" class="form-control" id="inputSuratLemburLab" name="suratId" value="<?=$surat['Id']?>" hidden>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="customFile" name="suratLemburLab" accept=".pdf">
                                            <label class="custom-file-label" for="customFile">Pilih file</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                            <button class="btn btn-primary" type="submit">Unggah</button>
                        </div>
                        </form>
                        </div>
                    </div>
                </div>
            </div>

<!-- End of Main Content -->