<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Filter -->
    <?php 
    $mhs = [2,3,4];
    if(!in_array($User['RoleId'], $mhs)):?>
        <div class="card">
            <div class="row mt-3 ml-2 mb-0">
                <div class="col">
                <form method="post" action="<?= base_url('surat/SuratBebasLab'); ?>" enctype="multipart/form-data">
                    <div class="form-row">
                        <div class="form-group col-md-2">
                            <div class="form-row">
                                <div class="form-group col">
                                <select id="jenjang" name="jenjang" class="form-control">
                                    <option value="" selected>Pilih Jenjang</option>
                                    <option value="2">S1</option>
                                    <option value="3">S2</option>
                                    <option value="4">S3</option>
                                </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-2">
                            <div class="form-row">
                                <div class="form-group col">
                                <select id="limit" name="limit" class="form-control">
                                    <option value="">Tampil Data</option>
                                    <option value="10">10</option>
                                    <option value="25">25</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-2">
                            <div class="form-row">
                                <div class="form-group col">
                                    <input type="text" class="form-control" id="search" name="search" placeholder="Nama / NIM">
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-2">
                            <button type="submit" class="btn btn-secondary">Search</button>
                        </div>
                    </div>
                </form>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <!-- Table Surat -->
    <div class="card">
        <div class="row mt-3 ml-2 mr-2">
            <div class="col-4">
                <h4 class="text-white bg-dark">Riwayat Permohonan Surat Bebas Lab</h4>
            </div>
            <div class="col-5">
            </div>
            <div class="col-3">
                <?php 
                $mhs = [2,3,4];
                if(in_array($User['RoleId'], $mhs)):?>
                <div class="btn-group">
                    <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#PengajuanSuratBebasLab">
                        <i class="fa-solid fa-fw fa-user-plus"></i>
                        Pengajuan Surat Bebas Lab
                    </button>
                </div>
                <?php endif; ?>
            </div>
        </div>
        <div class="row g-0 pb-3 pl-2 pr-2">
        <div class="card-body">
            <table class="table table-striped">
            <thead class="thead-dark">
                <tr>
                <th scope="col" width="50px">No</th>
                <th scope="col" width="500px">Mahasiswa</th>
                <th scope="col" width="500px">Tanggal Pengajuan</th>
                <th scope="col" width="500px">Aksi</th>
                <?php 
                $admin = [0,5];
                if(in_array($User['RoleId'], $admin)) :
                ?>
                <th scope="col" width="500px">validasi</th>
                <?php endif; ?>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $i = 1;
                    foreach($SuratBebasLab as $surat): 
                ?>
                <tr>
                <th scope="row"><?=$i?></th>
                <td class="SuratId" hidden><?=$surat['Id']?></td>
                <td>
                    <p>
                        <?= $surat["NamaMahasiswa"]?>
                    </p>
                </td>
                <td>
                    <p>
                        <?= $surat["CreatedAt"]; ?>
                    </p>
                </td>
                <td>
                    <div class="btn-group">
                        <button type="button" class="btn btn-warning btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Aksi
                            <i class="fa-solid fa-sliders"></i>
                        </button>
                        <div class="dropdown-menu">
                            <div class="row mx-auto p-1">
                                <a href="<?= base_url("surat/DownloadSuratBebasLab/". $surat['Id']) ?>" target="_blank">
                                    <button type="button" class="btn btn-success">
                                        Download Surat Pengajuan
                                        <i class="fa-solid fa-fw fa-file-export"></i>
                                    </button>
                                </a>
                            </div>
                            <?php if($surat['Valid'] == 1):?>
                            <div class="row mx-auto p-1">
                                <a href="<?= base_url("surat/DownloadSuratKeteranganBebasLab/". $surat['Id']) ?>" target="_blank">
                                    <button type="button" class="btn btn-success">
                                        Download Surat Keterangan
                                        <i class="fa-solid fa-fw fa-file-export"></i>
                                    </button>
                                </a>
                            </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </td>
                <?php 
                $admin = [0,5];
                if(in_array($User['RoleId'], $admin)) :
                ?>
                <form method="post" action="<?= base_url('surat/ValidasiSuratKeterangan'); ?>">
                    <td>
                        <div class="card">
                            <div class="row mx-auto">
                                <div class="col-12">
                                    <input type="text" name="SuratId" value="<?= $surat['Id'] ?>" hidden>
                                    <select name="validasiSuratKeterangan">
                                        <option value="1" <?= ($surat['Valid'] == 1) ? "selected" : ""; ?>>Iya</option>
                                        <option value="0" <?= ($surat['Valid'] == 0) ? "selected" : ""; ?>>Tidak</option>
                                    </select>
                                    <button class="btn btn-success" type="submit">Simpan</button>
                                </div>
                            </div>
                        </div>
                    </td>
                </form>
                <?php endif; ?>
                </tr>
                <?php 
                    $i++;
                    endforeach; 
                ?>
            </tbody>
            </table>
        </div>
    </div>
</div>

<!-- PengajuanSuratBebasLab Modal-->
<div class="modal fade" id="PengajuanSuratBebasLab" role="dialog" aria-labelledby="exampleModalLabel"
    >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Pengajuan Surat Bebas Lab</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="<?= base_url('surat/PengajuanSuratBebasLab'); ?>" >

                <div class="form-group row">
                    <label for="inputName" class="col-sm-4 col-form-label">Nama</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputName" name="mhsId" value="<?=$User['Id']?>" hidden>
                        <input type="text" class="form-control" id="inputName" placeholder="Name" name="username" value="<?=$User['Name']?>" disabled>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputUsername" class="col-sm-4 col-form-label">NIM</label>
                    <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputUsername" placeholder="NIM" name="username" value="<?=$User['Username']?>" disabled>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputProdi" class="col-sm-4 col-form-label">Program Studi</label>
                    <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputProdi" placeholder="Contoh : Kimia" name="programStudi" required>
                    </div>
                </div>
                <!-- <div class="form-group row">
                    <label for="inputDosenPembimbing" class="col-sm-4 col-form-label">Dosen Pembimbing</label>
                    <div class="col-sm-8">
                    <select id="DosenPembimbing" name="dosen" required>
                        <option value="">Select an option</option>
                        <?php foreach ($Dosen as $dsn): ?>
                            <option value="<?= $dsn['Id'] ?>"><?= $dsn['Name'] ?></option>
                        <?php endforeach; ?>
                    </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="TujuanSuratBebasLab" class="col-sm-4 col-form-label">Tujuan</label>
                    <div class="col-sm-8">
                    <select id="TujuanSuratBebasLab" name="tujuan" class="form-control" required>
                        <option value="">Select an option</option>
                        <?php 
                            $mhs = [2,3,4];
                            if(in_array($User['RoleId'], $mhs)):
                        ?>
                        <option value="1">Penelitian Dosen</option>
                        <?php endif; ?>
                        <option value="2">Proyek Kimia</option>
                        <option value="3">Program Kreatifitas Mahasiswa</option>
                        <option value="4">Penelitian Skripsi</option>
                        <option value="5">Penelitian Tesis</option>
                        <option value="6">Penelitian Disertasi</option>
                        <option value="7">Analisis Sampel</option>
                        <option value="8">Lainnya</option>
                    </select>
                    </div>
                </div>
                <div id="CardTujuanSuratBebasLabLainnya" style="display: none;">
                    <div class="form-group row">
                        <label for="inputTujuanLainnya" class="col-sm-4 col-form-label">Tujuan Lainnya</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputTujuanLainnya" name="tujuanLainnya">
                        </div>
                    </div>
                </div> -->

                <hr>
                <div class="form-group row">
                <label for="inputLaboratorium" class="col-sm-4 col-form-label">Laboratorium</label>
                <div class="col-sm-8">
                    <?php foreach ($Lab as $lab): ?>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="selectedLab[]" value="<?= $lab['Id'] ?>">
                            <label class="form-check-label"><?= $lab['Nama'] ?></label>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Simpan</button>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- End of Main Content -->