<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Card Bootstrap -->
    <div class="card mb-3" style="max-width: 1500px;">
        <div class="col-md-6 mt-3 ml-2">
            <h3>Permohonan Surat</h3>
        </div>
        <div class="row g-0 pt-4 pb-3 pl-2 pr-2">

        <!-- Surat Bebas lab -->
        <div class="col-sm-3 pb-2">
            <a href="<?= base_url('surat/SuratBebasLab'); ?>">
                <div class="card bg-gradient-light" style="height: 18rem;">
                    <div class="card-body mx-auto">
                        <i class="fa-solid fa-fw fa-envelope-open-text fa-8x"></i>
                        <p></p>
                        <h4 class="card-text text-gray-800">Surat Bebas Lab</h4>
                    </div> 
                    <div class="card-footer bg-gray-400">
                        <button class="btn btn-primary btn-user btn-block">
                            <b>
                                Detail
                                <i class="fa-regular fa-fw fa-circle-right fa-1x"></i>
                            </b>
                        </button>
                    </div>
                </div>
            </a>
        </div>

        <!-- Surat Izin Kerja lab -->
        <div class="col-sm-3 pb-2">
            <a href="<?= base_url('surat/SuratIjinKerja'); ?>">
                <div class="card bg-gradient-light" style="height: 18rem;">
                    <div class="card-body mx-auto">
                        <i class="fa-solid fa-fw fa-envelope-open-text fa-8x"></i>
                        <p></p>
                        <h4 class="card-text text-gray-800">Surat Ijin Kerja Lab</h4>
                    </div> 
                    <div class="card-footer bg-gray-400">
                        <button class="btn btn-primary btn-user btn-block">
                            <b>
                                Detail
                                <i class="fa-regular fa-fw fa-circle-right fa-1x"></i>
                            </b>
                        </button>
                    </div>
                </div>
            </a>
        </div>

        <!-- Surat Izin Lembur lab -->
        <div class="col-sm-3 pb-2">
            <a href="<?= base_url('surat/SuratIjinLembur'); ?>">
                <div class="card bg-gradient-light" style="height: 18rem;">
                    <div class="card-body mx-auto">
                        <i class="fa-solid fa-fw fa-envelope-open-text fa-8x"></i>
                        <p></p>
                        <h4 class="card-text text-gray-800">Surat Ijin Lembur</h4>
                    </div> 
                    <div class="card-footer bg-gray-400">
                            <button class="btn btn-primary btn-user btn-block">
                                <b>
                                    Detail
                                    <i class="fa-regular fa-fw fa-circle-right fa-1x"></i>
                                </b>
                            </button>
                    </div>
                </div>
            </a>
        </div>

        </div>
    </div>

    </div>
    <!-- /.container-fluid -->

</div>
<!-- End of Main Content -->