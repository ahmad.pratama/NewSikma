            <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

                    <!-- Card Bootstrap -->
                    <div class="card mb-3" style="max-width: 1000px;">
                    <div class="row g-0">
                        <div class="col-md-4">
                            <img src="<?= base_url('assets/img/profiles/') . $User['ImageProfile'] ?>" class="img-fluid rounded-start">
                            <div class="text-center">
                                <button class="btn btn-warning" data-toggle="modal" data-target="#uploadProfilePhoto">Unggah Profil</button>
                            </div>
                        </div>
                        <div class="col-md-6">
                        <div class="card-body">
                            <h4 class="card-title">Nama Lengkap : <?= $User['Name'] ?></h4>
                            <?php if($User['RoleId'] == 1 || $User['RoleId'] == 6) : ?>
                                <h6 class="card-title">NIP : <?= $User['Username'] ?></h6>
                            <?php elseif($User['RoleId'] == 2 || $User['RoleId'] == 3 ||$User['RoleId'] == 4) : ?>
                                <h6 class="card-title">NIM : <?= $User['Username'] ?></h6>
                                <!-- <h6 class="card-text">Alamat Asal : <?= $User['AlamatAsal'] ?></h6> -->
                            <?php endif; ?>
                            <h6 class="card-text">Telpon : <?= $User['Telpon'] ?></h6>
                            <h6 class="card-text">Email : <?= $User['Email'] ?></h6>
                            <h6 class="card-text">Alamat Malang : <?= $User['AlamatMalang'] ?></h6>

                            <p class="card-text"><small class="text-body-secondary">Terdaftar Sejak 
                                <?php 
                                    $date = strtotime($User['CreatedAt']); 
                                    echo date ( 'd F Y' , $date );  
                                ?></small></p>
                        </div>
                        </div>
                        <div class="col-md-2 float-right pt-2">
                            <button class="btn btn-warning" data-toggle="modal" data-target="#editUser">Edit User</button>
                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>


            <!-- Edit User Modal-->
            <div class="modal fade" id="editUser"  role="dialog" aria-labelledby="exampleModalLabel"
                >
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Edit Data User</h5>
                            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form method="post" action="<?= base_url('user/EditUser'); ?>" enctype="multipart/form-data">
                            <div class="card">
                                <div class="card-body">
                                    <div class="form-group row">
                                        <label for="inputName" class="col-sm-4 col-form-label">Nama Lengkap</label>
                                        <div class="col-sm-8">
                                        <input type="text" class="form-control" id="inputName" name="id" value="<?= $User['Id'] ?>" hidden>
                                        <input type="text" class="form-control" id="inputName" name="roleId" value="<?= $User['RoleId'] ?>" hidden>
                                        <input type="text" class="form-control" id="inputName" name="name" placeholder="Name" value="<?= $User['Name']?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputPassword" class="col-sm-4 col-form-label">Password</label>
                                        <div class="col-sm-8">
                                        <input type="password" class="form-control" id="inputPassword" name="password" placeholder="New Password">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputTelpon" class="col-sm-4 col-form-label">Telpon</label>
                                        <div class="col-sm-8">
                                        <input type="text" class="form-control" id="inputTelpon" name="telpon" placeholder="Telpon" value="<?= $User['Telpon']?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputEmail" class="col-sm-4 col-form-label">Email</label>
                                        <div class="col-sm-8">
                                        <input type="text" class="form-control" id="inputEmail" name="email" placeholder="Email" value="<?= $User['Email']?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputAlamatMalang" class="col-sm-4 col-form-label">Alamat Di Malang</label>
                                        <div class="col-sm-8">
                                        <input type="text" class="form-control" id="inputAlamatMalang" name="alamatMalang" placeholder="Alamat Di Malang" value="<?= $User['AlamatMalang']?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputAlamat" class="col-sm-4 col-form-label">Alamat</label>
                                        <div class="col-sm-8">
                                        <input type="text" class="form-control" id="inputAlamat" name="alamat" placeholder="Alamat" value="<?= $User['Alamat']?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php 
                            $mhsRole = [2,3,4];
                            if(in_array($User['RoleId'], $mhsRole)):?>
                            <!-- Card Mahasiswa -->
                            <div class="card">
                                <div class="card-body">
                                    <?php $minimalDate = date("Y-m-d"); ?>
                                    <div class="form-group row">
                                        <label for="inputTempatLahir" class="col-sm-4 col-form-label">Tempat Lahir</label>
                                        <div class="col-sm-8">
                                        <input type="text" class="form-control" id="inputTempatLahir" name="tempatLahir" placeholder="Tempat Lahir" value="<?= $User['TempatLahir']?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputTanggalLahir" class="col-sm-4 col-form-label">Tanggal Lahir</label>
                                        <div class="col-sm-8">
                                        <input type="date" class="form-control" id="inputTanggalLahir" name="tanggalLahir" value="<?php if(($User['TanggalLahir'] != NULL) ? date('Y-m-d',strtotime($User['TanggalLahir'])) : ""); ?>"  max="<?=  $minimalDate ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php elseif($User['RoleId'] == 1): ?>
                            <!-- Card Dosen -->
                            <div class="card">
                                <div class="card-body">
                                    <div class="form-group row">
                                        <label for="inputNidn" class="col-sm-4 col-form-label">NIDN</label>
                                        <div class="col-sm-8">
                                        <input type="text" class="form-control" id="inputNidn" name="nidn" placeholder="NIDN" value="<?= $User['NIDN']?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputLab" class="col-sm-4 col-form-label">Laboratorium</label>
                                        <div class="col-sm-8">
                                        <select name="laboratorium" id="inputLab" class="form-control">
                                            <?php foreach($Lab as $lab) :?>
                                                <option value="<?=$lab['Id']?>" <?= ($User['Laboratorium'] == 1) ? "selected" : ""; ?>><?=$lab['Nama']?></option>
                                            <?php endforeach;?>
                                        </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputBidang" class="col-sm-4 col-form-label">Bidang Keilmuan</label>
                                        <div class="col-sm-8">
                                        <input type="text" class="form-control" id="inputBidang" name="bidang" placeholder="Bidang Keilmuan" value="<?= $User['Bidang']?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputGolongan" class="col-sm-4 col-form-label">Golongan</label>
                                        <div class="col-sm-8">
                                        <input type="text" class="form-control" id="inputGolongan" name="golongan" placeholder="Golongan" value="<?= $User['Golongan']?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputPangkat" class="col-sm-4 col-form-label">Pangkat</label>
                                        <div class="col-sm-8">
                                        <input type="text" class="form-control" id="inputPangkat" name="pangkat" placeholder="Pangkat" value="<?= $User['Pangkat']?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputJabatan" class="col-sm-4 col-form-label">Jabatan</label>
                                        <div class="col-sm-8">
                                        <input type="text" class="form-control" id="inputJabatan" name="jabatan" placeholder="Jabatan" value="<?= $User['Jabatan']?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputTugasTambahan" class="col-sm-4 col-form-label">Tugas Tambahan</label>
                                        <div class="col-sm-8">
                                        <textarea class="form-control" rows="2" id="inputTugasTambahan" name="tugasTambahan"><?=$User['TugasTambahan']?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-body">
                                    <div class="form-group row">
                                        <label for="inputPendidikanS1" class="col-sm-4 col-form-label">Pendidikan S1</label>
                                        <div class="col-sm-4">
                                        <input type="text" class="form-control" id="inputPendidikanS1" name="pendidikanS1" placeholder="Pendidikan S1" value="<?= $User['PendidikanS1']?>">
                                        </div>
                                        <div class="col-sm-3">
                                        <input type="text" class="form-control" id="inputPendidikanS1" name="lulusS1" placeholder="Tahun" value="<?= $User['LulusS1']?>">
                                        </div>
                                        <div class="col-sm-1 p-0">
                                            <label for="file-input">
                                                <i class="fa-solid fa-fw fa-file-arrow-up fa-2x"></i> 
                                            </label>
                                            <input type="file" id="file-input" name="ijazahS1" style="display: none;" accept=".png,.jpg">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputPendidikanS2" class="col-sm-4 col-form-label">Pendidikan S2</label>
                                        <div class="col-sm-4">
                                            <input type="text" class="form-control" id="inputPendidikanS2" name="pendidikanS2" placeholder="Pendidikan S2" value="<?= $User['PendidikanS2']?>">
                                        </div>
                                        <div class="col-sm-3">
                                            <input type="text" class="form-control" id="inputPendidikanS2" name="lulusS2" placeholder="Tahun" value="<?= $User['LulusS2']?>">
                                        </div>
                                        <div class="col-sm-1 p-0">
                                            <label for="file-input">
                                                <i class="fa-solid fa-fw fa-file-arrow-up fa-2x"></i> 
                                            </label>
                                            <input type="file" id="file-input" name="ijazahS2" style="display: none;" accept=".png,.jpg">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputPendidikanS3" class="col-sm-4 col-form-label">Pendidikan S3</label>
                                        <div class="col-sm-4">
                                            <input type="text" class="form-control" id="inputPendidikanS3" name="pendidikanS3" placeholder="Pendidikan S3" value="<?= $User['PendidikanS3']?>">
                                        </div>
                                        <div class="col-sm-3">
                                            <input type="text" class="form-control" id="inputPendidikanS3" name="lulusS3" placeholder="Tahun" value="<?= $User['LulusS3']?>">
                                        </div>
                                        <div class="col-sm-1 p-0">
                                            <label for="file-input">
                                                <i class="fa-solid fa-fw fa-file-arrow-up fa-2x"></i> 
                                            </label>
                                            <input type="file" id="file-input" name="ijazahS3" style="display: none;" accept=".png,.jpg">
                                        </div>
                                    </div>
                                </div>
                            </div>
                                <?php if($User['TTDImage'] == null):?>
                                <div class="card">
                                    <div class="card-body">
                                        <div class="form-group row">
                                            <label for="uploadTTD" class="col-sm-4 col-form-label">Unggah TTD</label>
                                            <div class="col-sm-6">
                                                <input type="file" class="form-control-file" id="uploadTTD" name="TTDImage" accept=".png">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php endif; ?>
                            <?php elseif($User['RoleId'] == 5) :?>
                                <div class="card">
                                    <div class="card-body">
                                        <div class="form-group row">
                                            <label for="inputJabatan" class="col-sm-4 col-form-label">Jabatan</label>
                                            <div class="col-sm-8">
                                            <input type="text" class="form-control" id="inputJabatan" name="jabatan" placeholder="Jabatan" value="<?= $User['Jabatan']?>">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputBagian" class="col-sm-4 col-form-label">Bagian</label>
                                            <div class="col-sm-8">
                                            <input type="text" class="form-control" id="inputBagian" name="bagian" placeholder="Bagian" value="<?= $User['Bagian']?>">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputGolongan" class="col-sm-4 col-form-label">Golongan</label>
                                            <div class="col-sm-8">
                                            <input type="text" class="form-control" id="inputGolongan" name="golongan" placeholder="Golongan" value="<?= $User['Golongan']?>">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputPangkat" class="col-sm-4 col-form-label">Pangkat</label>
                                            <div class="col-sm-8">
                                            <input type="text" class="form-control" id="inputPangkat" name="pangkat" placeholder="Pangkat" value="<?= $User['Pangkat']?>">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputPendidikanTerakhir" class="col-sm-4 col-form-label">Pendidikan Terakhir</label>
                                            <div class="col-sm-8">
                                            <input type="text" class="form-control" id="inputPendidikanTerakhir" name="pendidikanTerakhir" placeholder="PendidikanTerakhir" value="<?= $User['PendidikanTerakhir']?>">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputLab" class="col-sm-4 col-form-label">Laboratorium</label>
                                            <div class="col-sm-8">
                                            <select name="laboratorium" id="inputLab" class="form-control">
                                                <option value="-" <?= ($User['Laboratorium'] == 1) ? "selected" : ""; ?>>-</option>
                                                <?php foreach($Lab as $lab) :?>
                                                    <option value="<?=$lab['Id']?>" <?= ($User['Laboratorium'] == 1) ? "selected" : ""; ?>><?=$lab['Nama']?></option>
                                                <?php endforeach;?>
                                            </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                            <button class="btn btn-warning" type="submit">Simpan</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>

            <!-- Upload Profile Photo Modal-->
            <div class="modal fade" id="uploadProfilePhoto"  role="dialog" aria-labelledby="exampleModalLabel"
                >
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Unggah Foto Profil</h5>
                            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                        <form method="post" action="<?= base_url('user/UploadProfilePhoto'); ?>" enctype="multipart/form-data">
                        <div class="form-group">
                            <div class="form-group row">
                                <div class="col-sm-2">Foto</div>
                                <div class="col-sm-10">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <img src="<?= base_url('assets/img/profiles/') . $User['ImageProfile'] ?>" class="img-thumbnail">
                                        </div>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="inputUserId" name="userId" value="<?=$User['Id']?>" hidden>
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="customFile" name="image" accept=".jpg,.png,.jpeg">
                                                <label class="custom-file-label" for="customFile">Pilih file</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                            <button class="btn btn-primary" type="submit">Unggah</button>
                        </div>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End of Main Content -->