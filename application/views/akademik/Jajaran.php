<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Filter -->
    <div class="card">
        <div class="row mt-3 ml-2 mb-0">
            <div class="col">
            <form method="post" action="<?= base_url('Akademik/Jajaran'); ?>" enctype="multipart/form-data">
                <div class="form-row">
                    <div class="form-group col-md-2">
                        <div class="form-row">
                            <div class="form-group col">
                            <select id="limit" name="limit" class="form-control">
                                <option value="">Tampil Data</option>
                                <option value="10">10</option>
                                <option value="25">25</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-2">
                        <div class="form-row">
                            <div class="form-group col">
                                <input type="text" class="form-control" id="search" name="search" placeholder="Nama Jajaran">
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-2">
                        <button type="submit" class="btn btn-secondary">Search</button>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>

    <!-- Table Jajaran -->
    <div class="card">
        <div class="row mt-3 ml-2 mr-2">
            <div class="col-4">
                <h4 class="text-white bg-dark">Data Jajaran</h4>
            </div>
            <div class="col-6">
            </div>
            <div class="col-2">
                <?php 
                if ($Jajaran == NULL):
                ?>
                <div class="btn-group">
                    <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa-brands fa-joomla"></i>
                        Tambah
                    </button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" data-toggle="modal" data-target="#tambahJajaran">Tambah Jajaran</a>
                    </div>
                </div>
                <?php endif; ?>
            </div>
        </div>
        <div class="row g-0 pb-3 pl-2 pr-2">
        <div class="card-body">
            <table class="table table-striped">
            <thead class="thead-dark">
                <tr>
                <th scope="col" width="50px">No</th>
                <th scope="col" width="500px">Kepala Departemen</th>
                <th scope="col" width="500px">Sekertaris Departemen</th>
                <th scope="col" width="500px">Ketua Program Studi Sarjana</th>
                <th scope="col" width="500px">Ketua Program Studi Magister</th>
                <th scope="col" width="500px">Ketua Program Studi Doktor</th>
                <th scope="col" width="500px">Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $i = 1;
                    foreach($Jajaran as $jajaran): 
                ?>
                <tr>
                <th scope="row" ><?=$i?></th>
                <td class="JajaranId" hidden><?=$jajaran['Id']?></td>
                <td>
                    <p><?=$jajaran["NamaKadep"]?></p>
                </td>
                <td>
                    <p><?=$jajaran["NamaSekdep"]?></p>
                </td>
                <td>
                    <p><?=$jajaran["NamaKaprodiS1"]?></p>
                </td>
                <td>
                    <p><?=$jajaran["NamaKaprodiS2"]?></p>
                </td>
                <td>
                    <p><?=$jajaran["NamaKaprodiS3"]?></p>
                </td>
                <td>
                    <button type="button" class="btn btn-warning editJajaran">Edit</button>
                </td>
                </tr>
                <?php 
                    $i++;
                    endforeach; 
                ?>
            </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Tambah Jajaran Modal-->
<div class="modal fade" id="tambahJajaran"  role="dialog" aria-labelledby="exampleModalLabel"
    >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Jajaran</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="<?= base_url('akademik/TambahJajaran'); ?>" >
                <div class="form-group row">
                    <label for="inputKadep" class="col-sm-4 col-form-label">Kepala Departemen</label>
                    <div class="col-sm-8">
                    <select id="Kadep" name="kadep">
                        <option value="">Select an option</option>
                        <?php foreach ($Dosen as $dsn): ?>
                            <option value="<?= $dsn['Id'] ?>"><?= $dsn['Name'] ?></option>
                        <?php endforeach; ?>
                    </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputSekdep" class="col-sm-4 col-form-label">Sekertaris Departemen</label>
                    <div class="col-sm-8">
                    <select id="Sekdep" name="sekdep">
                        <option value="">Select an option</option>
                        <?php foreach ($Dosen as $dsn): ?>
                            <option value="<?= $dsn['Id'] ?>"><?= $dsn['Name'] ?></option>
                        <?php endforeach; ?>
                    </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputSekdep" class="col-sm-4 col-form-label">Ketua Program Studi Sarjana</label>
                    <div class="col-sm-8">
                    <select id="KaprodiS1" name="kaprodiS1">
                        <option value="">Select an option</option>
                        <?php foreach ($Dosen as $dsn): ?>
                            <option value="<?= $dsn['Id'] ?>"><?= $dsn['Name'] ?></option>
                        <?php endforeach; ?>
                    </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputSekdep" class="col-sm-4 col-form-label">Ketua Program Studi Magister</label>
                    <div class="col-sm-8">
                    <select id="KaprodiS2" name="kaprodiS2">
                        <option value="">Select an option</option>
                        <?php foreach ($Dosen as $dsn): ?>
                            <option value="<?= $dsn['Id'] ?>"><?= $dsn['Name'] ?></option>
                        <?php endforeach; ?>
                    </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputSekdep" class="col-sm-4 col-form-label">Ketua Program Studi Doktor</label>
                    <div class="col-sm-8">
                    <select id="KaprodiS3" name="kaprodiS3">
                        <option value="">Select an option</option>
                        <?php foreach ($Dosen as $dsn): ?>
                            <option value="<?= $dsn['Id'] ?>"><?= $dsn['Name'] ?></option>
                        <?php endforeach; ?>
                    </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Tambah</button>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- Edit Jajaran Model -->
<div class="modal fade" id="editJajaran"  role="dialog" aria-labelledby="exampleModalLabel"
    >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Jajaran</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
            <form method="post" action="<?=base_url('Akademik/editJajaran')?>" >
            <div class="card">
                <div class="card-body">
                    <div class="form-group row">
                        <label for="inputKadep" class="col-sm-4 col-form-label">Kepala Departemen</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputJajaran" name="jajaranId" hidden>
                        <select id="KadepEdit" name="kadep">
                            <option value="">Select an option</option>
                            <?php foreach ($Dosen as $dsn): ?>
                                <option value="<?= $dsn['Id'] ?>"><?= $dsn['Name'] ?></option>
                            <?php endforeach; ?>
                        </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputSekdep" class="col-sm-4 col-form-label">Sekertaris Departemen</label>
                        <div class="col-sm-8">
                        <select id="SekdepEdit" name="sekdep">
                            <option value="">Select an option</option>
                            <?php foreach ($Dosen as $dsn): ?>
                                <option value="<?= $dsn['Id'] ?>"><?= $dsn['Name'] ?></option>
                            <?php endforeach; ?>
                        </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputKaprodiS1" class="col-sm-4 col-form-label">Kepala Program Studi S1</label>
                        <div class="col-sm-8">
                        <select id="KaprodiS1Edit" name="kaprodiS1">
                            <option value="">Select an option</option>
                            <?php foreach ($Dosen as $dsn): ?>
                                <option value="<?= $dsn['Id'] ?>"><?= $dsn['Name'] ?></option>
                            <?php endforeach; ?>
                        </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputKaprodiS2" class="col-sm-4 col-form-label">Kepala Program Studi S2</label>
                        <div class="col-sm-8">
                        <select id="KaprodiS2Edit" name="kaprodiS2">
                            <option value="">Select an option</option>
                            <?php foreach ($Dosen as $dsn): ?>
                                <option value="<?= $dsn['Id'] ?>"><?= $dsn['Name'] ?></option>
                            <?php endforeach; ?>
                        </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputKaprodiS3" class="col-sm-4 col-form-label">Kepala Program Studi S3</label>
                        <div class="col-sm-8">
                        <select id="KaprodiS3Edit" name="kaprodiS3">
                            <option value="">Select an option</option>
                            <?php foreach ($Dosen as $dsn): ?>
                                <option value="<?= $dsn['Id'] ?>"><?= $dsn['Name'] ?></option>
                            <?php endforeach; ?>
                        </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Edit</button>
            </div>
            </form>
        </div>
    </div>
    </div>
</div>

<!-- End of Main Content -->