<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Filter -->
    <div class="card">
        <div class="row mt-3 ml-2 mb-0">
            <div class="col">
            <form method="post" action="<?= base_url('Akademik/DateSkripsi'); ?>" enctype="multipart/form-data">
                <div class="form-row">
                    <div class="form-group col-md-2">
                        <div class="form-row">
                            <div class="form-group col">
                            <select id="limit" name="limit" class="form-control">
                                <option value="">Tampil Data</option>
                                <option value="10">10</option>
                                <option value="25">25</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-2">
                        <div class="form-row">
                            <div class="form-group col">
                                <input type="text" class="form-control" id="search" name="search" placeholder="Nama Ruangan">
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-2">
                        <button type="submit" class="btn btn-secondary">Search</button>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>

    <!-- Table Skripsi -->
    <div class="card">
        <div class="row mt-3 ml-2 mr-2">
            <div class="col-4">
                <h4 class="text-white bg-dark">Data Skripsi</h4>
            </div>
            <div class="col-6">
            </div>
            <div class="col-2">
                <div class="btn-group">
                    <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa-solid fa-school-flag"></i>
                        Tambah
                    </button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item tambahDateSkripsi">Tambah Skripsi</a>
                    </div>
            </div>
            </div>
        </div>
        <div class="row g-0 pb-3 pl-2 pr-2">
        <div class="card-body">
            <table class="table table-striped">
            <thead class="thead-dark">
                <tr>
                <th scope="col" width="50px">No</th>
                <th scope="col" width="500px">Semster</th>
                <th scope="col" width="500px">Maksimal Pendaftaran Sempro</th>
                <th scope="col" width="500px">Maksimal Pendaftaran Semju</th>
                <th scope="col" width="500px">Maksimal Pendaftaran Tugas Akhir</th>
                <th scope="col" width="500px">Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $i = 1;
                    foreach($Skripsi as $dateSkripsi): 
                ?>
                <tr <?php echo ($dateSkripsi['Status'] == 0) ? 'style="background-color:#FF7E62"':'style="background-color:#FFFFFF"' ?>>
                <th scope="row" ><?=$i?></th>
                <td class="DateSkripsiId" hidden><?=$dateSkripsi['Id']?></td>
                <td>
                    <p><?= $dateSkripsi['Semester'] ?> - <?= $dateSkripsi['TahunAkademik'] ?></p>
                </td>
                <td>
                    <p><?=$dateSkripsi['MaxDateSempro']?></p>
                </td>
                <td>
                    <p><?=$dateSkripsi['MaxDateSemju']?></p>
                </td>
                <td>
                    <p><?=$dateSkripsi['MaxDateSkripsi']?></p>
                </td>
                <td>
                    <button type="button" class="btn btn-warning editDateSkripsi">Edit</button>
                    <a href="<?= base_url("akademik/DeleteDateSkripsi/".$dateSkripsi['Id']);?>" type="button" class="btn btn-danger">Hapus</a>
                </td>
                </tr>
                <?php 
                    $i++;
                    endforeach; 
                ?>
            </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Tambah Semester Modal-->
<div class="modal fade" id="tambahDateSkripsi"  role="dialog" aria-labelledby="exampleModalLabel"
    >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Tanggal Skripsi</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="<?= base_url('akademik/TambahDateSkripsi'); ?>" >
                <div class="form-group row">
                    <label for="inputDosenPembimbing1" class="col-sm-4 col-form-label">Semester</label>
                    <div class="col-sm-8">
                    <select id="SemesterTambahDateSkripsi" name="semester" required>
                        <option value="">Select an option</option>
                    </select>
                    </div>
                </div>
                <?php $minimalDate = date("Y-m-d"); ?>
                <div class="form-group row">
                    <label for="inputTanggalUjianSempro" class="col-sm-4 col-form-label">Tanggal Seminar Proposal</label>
                    <div class="col-sm-8">
                    <input type="date" class="form-control" id="inputTanggalUjianSempro" name="tanggalUjianSempro" min="<?= $minimalDate; ?>" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputTanggalUjianSemju" class="col-sm-4 col-form-label">Tanggal Seminar Kemajuan</label>
                    <div class="col-sm-8">
                    <input type="date" class="form-control" id="inputTanggalUjianSemju" name="tanggalUjianSemju" min="<?= $minimalDate; ?>" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputTanggalUjianSkripsi" class="col-sm-4 col-form-label">Tanggal Ujian Tugas Akhir</label>
                    <div class="col-sm-8">
                    <input type="date" class="form-control" id="inputTanggalUjianSkripsi" name="tanggalUjianSkripsi" min="<?= $minimalDate; ?>" required>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Tambah</button>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- Edit Date Skripsi Model -->
<div class="modal fade" id="editDateSkripsi"  role="dialog" aria-labelledby="exampleModalLabel"
    >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Date Skripsi</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
            <form method="post" action="<?=base_url('Akademik/editDateSkripsi')?>" >
            <div class="card">
                <div class="card-body">
                    <div class="form-group row">
                        <label for="inputSemester" class="col-sm-4 col-form-label">Semester</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputSemester" name="dateSkripsiId" hidden>
                            <select id="SemesterEditDateSkripsi" name="semesterId" class="select2" required>
                                <option value="">Select an option</option>
                                <?php foreach ($Semester as $semester): ?>
                                    <option value="<?= $semester['Id'] ?>"><?= $semester['Semester'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                <?php $minimalDate = date("Y-m-d"); ?>
                <div class="form-group row">
                    <label for="inputTanggalUjianSempro" class="col-sm-4 col-form-label">Tanggal Seminar Proposal</label>
                    <div class="col-sm-8">
                    <input type="date" class="form-control" id="inputTanggalUjianSempro" name="tanggalUjianSempro" min="<?= $minimalDate; ?>" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputTanggalUjianSemju" class="col-sm-4 col-form-label">Tanggal Seminar Kemajuan</label>
                    <div class="col-sm-8">
                    <input type="date" class="form-control" id="inputTanggalUjianSemju" name="tanggalUjianSemju" min="<?= $minimalDate; ?>" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputTanggalUjianSkripsi" class="col-sm-4 col-form-label">Tanggal Ujian Tugas Akhir</label>
                    <div class="col-sm-8">
                    <input type="date" class="form-control" id="inputTanggalUjianSkripsi" name="tanggalUjianSkripsi" min="<?= $minimalDate; ?>" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputStatus" class="col-sm-4 col-form-label">Status</label>
                    <div class="col-sm-8">
                    <select id="inputStatus" name="status" class="form-control" required>
                        <option value="1">Aktif</option>
                        <option value="0">Tidak Aktif</option>
                    </select>
                    </div>
                </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Edit</button>
            </div>
            </form>
        </div>
    </div>
    </div>
</div>

<!-- End of Main Content -->