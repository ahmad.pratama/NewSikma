<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Card Bootstrap -->
    <div class="card mb-3" style="max-width: 1500px;">
        <div class="col-md-6 mt-3 ml-2">
            <h3>Menu Akademik</h3>
        </div>
        <div class="row g-0 pt-4 pb-3 pl-2 pr-2">

        <!-- Data Ruangan -->
        <div class="col-sm-3 pb-2">
            <a href="<?=base_url('akademik/ruangan')?>" >
                <div class="card bg-gradient-light" style="height: 18rem;">
                    <div class="card-body mx-auto">
                        <i class="fa-solid fa-fw fa-school fa-8x"></i>
                        <p></p>
                        <h4 class="card-text text-gray-800" style="text-align:center;">Data Ruangan</h4>
                    </div> 
                    <div class="card-footer bg-gray-400">
                    <button class="btn btn-primary btn-user btn-block">
                        <b>
                            Detail
                            <i class="fa-regular fa-fw fa-circle-right fa-1x"></i>
                        </b>
                    </button>
                    </div>
                </div>
            </a>
        </div>

        <!-- Data Semester -->
        <div class="col-sm-3 pb-2">
            <a href="<?=base_url('akademik/semester')?>">
                <div class="card bg-gradient-light" style="height: 18rem;">
                    <div class="card-body mx-auto">
                        <i class="fa-regular fa-fw fa-clipboard fa-8x"></i>
                        <p></p>
                        <h4 class="card-text text-gray-800" style="text-align:center;">Data Semester</h4>
                    </div> 
                    <div class="card-footer bg-gray-400">
                    <button class="btn btn-primary btn-user btn-block">
                        <b>
                            Detail
                            <i class="fa-regular fa-fw fa-circle-right fa-1x"></i>
                        </b>
                    </button>
                    </div>
                </div>
            </a>
        </div>

        <!-- Data Tanggal Skripsi -->
        <div class="col-sm-3 pb-2">
            <a href="<?=base_url('akademik/DateSkripsi')?>">
                <div class="card bg-gradient-light" style="height: 18rem;">
                    <div class="card-body mx-auto">
                        <i class="fa-regular fa-fw fa-calendar fa-8x"></i>
                        <p></p>
                        <h4 class="card-text text-gray-800" style="text-align:center;">Data Tanggal Skripsi</h4>
                    </div> 
                    <div class="card-footer bg-gray-400">
                    <button class="btn btn-primary btn-user btn-block">
                        <b>
                            Detail
                            <i class="fa-regular fa-fw fa-circle-right fa-1x"></i>
                        </b>
                    </button>
                    </div>
                </div>
            </a>
        </div>

        <!-- Data Instansi -->
        <div class="col-sm-3 pb-2">
            <a href="<?=base_url('akademik/Instansi')?>">
                <div class="card bg-gradient-light" style="height: 18rem;">
                    <div class="card-body mx-auto">
                        <i class="fa-solid fa-fw fa-building fa-8x"></i>
                        <p></p>
                        <h4 class="card-text text-gray-800" style="text-align:center;">Data Instansi</h4>
                    </div> 
                    <div class="card-footer bg-gray-400">
                    <button class="btn btn-primary btn-user btn-block">
                        <b>
                            Detail
                            <i class="fa-regular fa-fw fa-circle-right fa-1x"></i>
                        </b>
                    </button>
                    </div>
                </div>
            </a>
        </div>

        <!-- Data Laboratorium -->
        <div class="col-sm-3 pb-2">
            <a href="<?=base_url('akademik/Laboratorium')?>">
                <div class="card bg-gradient-light" style="height: 18rem;">
                    <div class="card-body mx-auto">
                        <i class="fa-solid fa-fw fa-building fa-8x"></i>
                        <p></p>
                        <h4 class="card-text text-gray-800" style="text-align:center;">Data Laboratorium</h4>
                    </div> 
                    <div class="card-footer bg-gray-400">
                    <button class="btn btn-primary btn-user btn-block">
                        <b>
                            Detail
                            <i class="fa-regular fa-fw fa-circle-right fa-1x"></i>
                        </b>
                    </button>
                    </div>
                </div>
            </a>
        </div>

        <!-- Data Jajaran -->
        <div class="col-sm-3 pb-2">
            <a href="<?=base_url('akademik/Jajaran')?>">
                <div class="card bg-gradient-light" style="height: 18rem;">
                    <div class="card-body mx-auto">
                        <i class="fa-solid fa-fw fa-people-roof fa-8x"></i>
                        <p></p>
                        <h4 class="card-text text-gray-800" style="text-align:center;">Data Jajaran</h4>
                    </div> 
                    <div class="card-footer bg-gray-400">
                    <button class="btn btn-primary btn-user btn-block">
                        <b>
                            Detail
                            <i class="fa-regular fa-fw fa-circle-right fa-1x"></i>
                        </b>
                    </button>
                    </div>
                </div>
            </a>
        </div>
        
        </div>
    </div>

    </div>
    <!-- /.container-fluid -->

</div>
<!-- End of Main Content -->