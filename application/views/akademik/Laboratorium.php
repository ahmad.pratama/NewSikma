<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Filter -->
    <div class="card">
        <div class="row mt-3 ml-2 mb-0">
            <div class="col">
            <form method="post" action="<?= base_url('Akademik/Laboratorium'); ?>" enctype="multipart/form-data">
                <div class="form-row">
                    <div class="form-group col-md-2">
                        <div class="form-row">
                            <div class="form-group col">
                            <select id="limit" name="limit" class="form-control">
                                <option value="">Tampil Data</option>
                                <option value="10">10</option>
                                <option value="25">25</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-2">
                        <div class="form-row">
                            <div class="form-group col">
                                <input type="text" class="form-control" id="search" name="search" placeholder="Nama Laboratorium">
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-2">
                        <button type="submit" class="btn btn-secondary">Search</button>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>

    <!-- Table Laboratorium -->
    <div class="card">
        <div class="row mt-3 ml-2 mr-2">
            <div class="col-4">
                <h4 class="text-white bg-dark">Data Laboratorium</h4>
            </div>
            <div class="col-6">
            </div>
            <div class="col-2">
                <div class="btn-group">
                    <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa-brands fa-joomla"></i>
                        Tambah
                    </button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" data-toggle="modal" data-target="#tambahLaboratorium">Tambah Lab</a>
                    </div>
            </div>
            </div>
        </div>
        <div class="row g-0 pb-3 pl-2 pr-2">
        <div class="card-body">
            <table class="table table-striped">
            <thead class="thead-dark">
                <tr>
                <th scope="col" width="50px">No</th>
                <th scope="col" width="500px">Laboratorium</th>
                <th scope="col" width="500px">Kepala Laboratorium</th>
                <th scope="col" width="500px">PLP</th>
                <th scope="col" width="500px">Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $i = 1;
                    foreach($Laboratorium as $lab): 
                ?>
                <tr <?php echo ($lab['Status'] == 0) ? 'style="background-color:#FF7E62"':'style="background-color:#FFFFFF"' ?>>
                <th scope="row" ><?=$i?></th>
                <td class="LaboratoriumId" hidden><?=$lab['Id']?></td>
                <td>
                    <p><?=$lab['Nama']?></p>
                </td>
                <td>
                    <p><?=$lab['Kalab']?></p>
                </td>
                <td>
                    <p><?=$lab['PLP']?></p>
                </td>
                <td>
                    <button type="button" class="btn btn-warning editLaboratorium">Edit</button>
                    <a href="<?= base_url("akademik/DeleteLaboratorium/".$lab['Id']);?>" type="button" class="btn btn-danger">Hapus</a>
                </td>
                </tr>
                <?php 
                    $i++;
                    endforeach; 
                ?>
            </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Tambah Laboratorium Modal-->
<div class="modal fade" id="tambahLaboratorium"  role="dialog" aria-labelledby="exampleModalLabel"
    >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Laboratorium</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="<?= base_url('akademik/TambahLaboratorium'); ?>" >

                <div class="form-group row">
                    <label for="inputLaboratorium" class="col-sm-4 col-form-label">Nama Laboratorium</label>
                    <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputLaboratorium" name="laboratorium" placeholder="Nama Laboratorium" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputKepalaLaboratorium" class="col-sm-4 col-form-label">Kepala Laboratorium</label>
                    <div class="col-sm-8">
                    <select id="Dosen1" name="dosen" required>
                        <option value="">Select an option</option>
                        <?php foreach ($Dosen as $dsn): ?>
                            <option value="<?= $dsn['Id'] ?>"><?= $dsn['Name'] ?></option>
                        <?php endforeach; ?>
                    </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Tambah</button>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- Edit Laboratorium Model -->
<div class="modal fade" id="editLaboratorium"  role="dialog" aria-labelledby="exampleModalLabel"
    >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Laboratorium</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
            <form method="post" action="<?=base_url('Akademik/editLaboratorium')?>" >
            <div class="card">
                <div class="card-body">
                    <div class="form-group row">
                        <label for="inputLaboratorium" class="col-sm-4 col-form-label">Nama Laboratorium</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputLaboratorium" name="laboratoriumId" hidden>
                        <input type="text" class="form-control" id="inputLaboratorium" name="laboratorium" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputKepalaLaboratorium" class="col-sm-4 col-form-label">Kepala Laboratorium</label>
                        <div class="col-sm-8">
                        <select id="Dosen2" name="dosen" required>
                            <option value="">Select an option</option>
                            <?php foreach ($Dosen as $dsn): ?>
                                <option value="<?= $dsn['Id'] ?>"><?= $dsn['Name'] ?></option>
                            <?php endforeach; ?>
                        </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputKepalaLaboratorium" class="col-sm-4 col-form-label">PLP</label>
                        <div class="col-sm-8">
                        <select id="PLP" name="plp" required>
                            <option value="">Select an option</option>
                            <?php foreach ($PLP as $plp): ?>
                                <option value="<?= $plp['Id'] ?>"><?= $plp['Name'] ?></option>
                            <?php endforeach; ?>
                        </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputStatus" class="col-sm-4 col-form-label">Status</label>
                        <div class="col-sm-8">
                        <select id="inputStatus" name="status" class="form-control" required>
                            <option value="1">Aktif</option>
                            <option value="0">Tidak Aktif</option>
                        </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Edit</button>
            </div>
            </form>
        </div>
    </div>
    </div>
</div>

<!-- End of Main Content -->