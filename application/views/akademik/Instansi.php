<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Filter -->
    <div class="card">
        <div class="row mt-3 ml-2 mb-0">
            <div class="col">
            <form method="post" action="<?= base_url('Akademik/Instansi'); ?>" enctype="multipart/form-data">
                <div class="form-row">
                    <div class="form-group col-md-2">
                        <div class="form-row">
                            <div class="form-group col">
                            <select id="limit" name="limit" class="form-control">
                                <option value="">Tampil Data</option>
                                <option value="10">10</option>
                                <option value="25">25</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-2">
                        <div class="form-row">
                            <div class="form-group col">
                                <input type="text" class="form-control" id="search" name="search" placeholder="Nama Instansi">
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-2">
                        <button type="submit" class="btn btn-secondary">Search</button>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>

    <!-- Table Instansi -->
    <div class="card">
        <div class="row mt-3 ml-2 mr-2">
            <div class="col-4">
                <h4 class="text-white bg-dark">Data Instansi</h4>
            </div>
            <div class="col-6">
            </div>
            <div class="col-2">
                <div class="btn-group">
                    <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa-solid fa-school-flag"></i>
                        Tambah
                    </button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" data-toggle="modal" data-target="#tambahInstansi">Tambah Instansi</a>
                        <a class="dropdown-item" data-toggle="modal" data-target="#importInstansi">Import Instansi</a>
                    </div>
            </div>
            </div>
        </div>
        <div class="row g-0 pb-3 pl-2 pr-2">
        <div class="card-body">
            <table class="table table-striped">
            <thead class="thead-dark">
                <tr>
                <th scope="col" width="50px">No</th>
                <th scope="col" width="500px">Nama</th>
                <th scope="col" width="500px">Alamat</th>
                <th scope="col" width="500px">Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $i = 1;
                    foreach($Instansi as $instansi): 
                ?>
                <tr <?php echo ($instansi['Status'] == 0) ? 'style="background-color:#FF7E62"':'style="background-color:#FFFFFF"' ?>>
                <th scope="row" ><?=$i?></th>
                <td class="InstansiId" hidden><?=$instansi['Id']?></td>
                <td>
                    <p><?=$instansi['Nama']?></p>
                </td>
                <td>
                    <p><?=$instansi['Alamat']?></p>
                </td>
                <td>
                    <button type="button" class="btn btn-warning editInstansi">Edit</button>
                    <a href="<?= base_url("akademik/DeleteInstansi/".$instansi['Id']);?>" type="button" class="btn btn-danger">Hapus</a>
                </td>
                </tr>
                <?php 
                    $i++;
                    endforeach; 
                ?>
            </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Tambah Instansi Modal-->
<div class="modal fade" id="tambahInstansi"  role="dialog" aria-labelledby="exampleModalLabel"
    >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Instansi</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="<?= base_url('akademik/TambahInstansi'); ?>" >
                <div class="form-group row">
                    <label for="inputNama" class="col-sm-4 col-form-label">Nama Instansi</label>
                    <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputNama" name="nama" placeholder="Nama" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputAlamat" class="col-sm-4 col-form-label">Alamat</label>
                    <div class="col-sm-8">
                    <textarea name="alamat" id="inputAlamat" cols="33" rows="3" required></textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputEmail" class="col-sm-4 col-form-label">Email</label>
                    <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputEmail" name="email" placeholder="Email" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputTelpon" class="col-sm-4 col-form-label">Telpon</label>
                    <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputTelpon" name="telpon" placeholder="Telpon" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputPimpinan" class="col-sm-4 col-form-label">Pimpinan</label>
                    <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputPimpinan" name="pimpinan" placeholder="Nama Pimpinan" required>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Tambah</button>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- Import Instansi Modal-->
<div class="modal fade" id="importInstansi"  role="dialog" aria-labelledby="exampleModalLabel"
    >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Import Instansi</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
            <form method="post" action="<?= base_url('akademik/ImportInstansi'); ?>" enctype="multipart/form-data">
            <div class="form-group">

                <div class="form-group row">
                    <label for="uploadexcel" class="col-sm-4 col-form-label">File Excel</label>
                    <div class="col-sm-6">
                        <input type="file" class="form-control-file" id="uploadexcel" name="importexcel" accept=".xlsx,.xls">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Tambah</button>
            </div>
            </form>
            </div>
        </div>
    </div>
</div>

<!-- Edit Instansi Model -->
<div class="modal fade" id="editInstansi"  role="dialog" aria-labelledby="exampleModalLabel"
    >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Instansi</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
            <form method="post" action="<?=base_url('Akademik/editInstansi')?>" >
            <div class="card">
                <div class="card-body">
                    <div class="form-group row">
                        <label for="inputNama" class="col-sm-4 col-form-label">Nama Instansi</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputSemester" name="instansiId" hidden>
                        <input type="text" class="form-control" id="inputNama" name="nama" placeholder="Nama" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputAlamat" class="col-sm-4 col-form-label">Alamat</label>
                        <div class="col-sm-8">
                        <textarea name="alamat" id="inputAlamat" cols="30" rows="3" required></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputEmail" class="col-sm-4 col-form-label">Email</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputEmail" name="email" placeholder="Email" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputTelpon" class="col-sm-4 col-form-label">Telpon</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputTelpon" name="telpon" placeholder="Telpon" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPimpinan" class="col-sm-4 col-form-label">Pimpinan</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputPimpinan" name="pimpinan" placeholder="Pimpinan" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputStatus" class="col-sm-4 col-form-label">Status</label>
                        <div class="col-sm-8">
                        <select id="inputStatus" name="status" class="form-control" required>
                            <option value="1">Aktif</option>
                            <option value="0">Tidak Aktif</option>
                        </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Edit</button>
            </div>
            </form>
        </div>
    </div>
    </div>
</div>

<!-- End of Main Content -->