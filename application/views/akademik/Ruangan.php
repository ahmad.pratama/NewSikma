<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Filter -->
    <div class="card">
        <div class="row mt-3 ml-2 mb-0">
            <div class="col">
            <form method="post" action="<?= base_url('Akademik/Ruangan'); ?>" enctype="multipart/form-data">
                <div class="form-row">
                    <div class="form-group col-md-2">
                        <div class="form-row">
                            <div class="form-group col">
                            <select id="limit" name="limit" class="form-control">
                                <option value="">Tampil Data</option>
                                <option value="10">10</option>
                                <option value="25">25</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-2">
                        <div class="form-row">
                            <div class="form-group col">
                                <input type="text" class="form-control" id="search" name="search" placeholder="Nama Ruangan">
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-2">
                        <button type="submit" class="btn btn-secondary">Search</button>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>

    <!-- Table Ruangan -->
    <div class="card">
        <div class="row mt-3 ml-2 mr-2">
            <div class="col-4">
                <h4 class="text-white bg-dark">Data Ruangan</h4>
            </div>
            <div class="col-6">
            </div>
            <div class="col-2">
                <div class="btn-group">
                    <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa-solid fa-school-flag"></i>
                        Tambah
                    </button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" data-toggle="modal" data-target="#tambahRuangan">Tambah Ruangan</a>
                        <a class="dropdown-item" data-toggle="modal" data-target="#importRuangan">Import Ruangan</a>
                    </div>
            </div>
            </div>
        </div>
        <div class="row g-0 pb-3 pl-2 pr-2">
        <div class="card-body">
            <table class="table table-striped">
            <thead class="thead-dark">
                <tr>
                <th scope="col" width="50px">No</th>
                <th scope="col" width="500px">Nama</th>
                <th scope="col" width="500px">Kapasitas</th>
                <th scope="col" width="500px">Keterangan</th>
                <th scope="col" width="500px">Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $i = 1;
                    foreach($Ruangan as $room): 
                ?>
                <tr <?php echo ($room['Status'] == 0) ? 'style="background-color:#FF7E62"':'style="background-color:#FFFFFF"' ?>>
                <th scope="row" ><?=$i?></th>
                <td class="RuanganId" hidden><?=$room['Id']?></td>
                <td>
                    <p><?=$room['Nama']?></p>
                </td>
                <td>
                    <p><?=$room['Kapasitas']?> Orang</p>
                </td>
                <td>
                    <p><?=$room['Keterangan']?></p>
                </td>
                <td>
                    <button type="button" class="btn btn-warning editRuangan">Edit</button>
                    <a href="<?= base_url("akademik/DeleteRuangan/".$room['Id']);?>" type="button" class="btn btn-danger">Hapus</a>
                </td>
                </tr>
                <?php 
                    $i++;
                    endforeach; 
                ?>
            </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Tambah Ruangan Modal-->
<div class="modal fade" id="tambahRuangan"  role="dialog" aria-labelledby="exampleModalLabel"
    >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Ruangan</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="<?= base_url('akademik/TambahRuangan'); ?>" >
                <div class="form-group row">
                    <label for="inputNama" class="col-sm-4 col-form-label">Nama Ruangan</label>
                    <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputNama" name="nama" placeholder="Nama" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputKapasitas" class="col-sm-4 col-form-label">Kapasitas</label>
                    <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputKapasitas" name="kapasitas" placeholder="Kapasitas">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputKeterangan" class="col-sm-4 col-form-label">Keterangan</label>
                    <div class="col-sm-8">
                    <textarea name="keterangan" id="inputKeterangan" cols="33" rows="3" required></textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputKategori" class="col-sm-4 col-form-label">Kategori</label>
                    <div class="col-sm-8">
                    <select id="inputKategori" name="kategori" class="form-control" required>
                        <option value="">Pilih Kategori</option>
                        <option value="1">Umum</option>
                        <option value="2">Laboratorium</option>
                        <option value="3">Khusus</option>
                        <option value="4">Other</option>
                    </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Tambah</button>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- Import Ruangan Modal-->
<div class="modal fade" id="importRuangan"  role="dialog" aria-labelledby="exampleModalLabel"
    >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Import Ruangan</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
            <form method="post" action="<?= base_url('akademik/ImportRuangan'); ?>" enctype="multipart/form-data">
            <div class="form-group">

                <div class="form-group row">
                    <label for="uploadexcel" class="col-sm-4 col-form-label">File Excel</label>
                    <div class="col-sm-6">
                        <input type="file" class="form-control-file" id="uploadexcel" name="importexcel" accept=".xlsx,.xls">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Tambah</button>
            </div>
            </form>
            </div>
        </div>
    </div>
</div>


<!-- View Ruangan Model -->
<div class="modal fade" id="viewRuangan"  role="dialog" aria-labelledby="exampleModalLabel"
    >
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="view_Ruangan">
            </div>
        </div>
    </div>
</div>

<!-- Edit Ruangan Model -->
<div class="modal fade" id="editRuangan"  role="dialog" aria-labelledby="exampleModalLabel"
    >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Ruangan</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
            <form method="post" action="<?=base_url('Akademik/editRuangan')?>" >
            <div class="card">
                <div class="card-body">
                    <div class="form-group row">
                        <label for="inputNama" class="col-sm-4 col-form-label">Nama Ruangan</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputSemester" name="RuanganId" hidden>
                        <input type="text" class="form-control" id="inputNama" name="nama" placeholder="Nama" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputKapasitas" class="col-sm-4 col-form-label">Kapasitas</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputKapasitas" name="kapasitas" placeholder="Kapasitas" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputKeterangan" class="col-sm-4 col-form-label">Keterangan</label>
                        <div class="col-sm-8">
                        <textarea name="keterangan" id="inputKeterangan" cols="30" rows="3" required></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputKategori" class="col-sm-4 col-form-label">Kategori</label>
                        <div class="col-sm-8">
                        <select id="inputKategori" name="kategori" class="form-control" required>
                            <option value="">Pilih Kategori</option>
                            <option value="1">Umum</option>
                            <option value="2">Laboratorium</option>
                            <option value="3">Khusus</option>
                            <option value="4">Other</option>
                        </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputStatus" class="col-sm-4 col-form-label">Status</label>
                        <div class="col-sm-8">
                        <select id="inputStatus" name="status" class="form-control" required>
                            <option value="1">Aktif</option>
                            <option value="0">Tidak Aktif</option>
                        </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button class="btn btn-primary" type="submit">Edit</button>
            </div>
            </form>
        </div>
    </div>
    </div>
</div>

<!-- End of Main Content -->